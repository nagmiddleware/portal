<%--
*******************************************************************************
                          Bank Organization Group Detail

  Description:
     This page is used to maintain bank organization group.  It supports
  insert, update and delete.  Errors found during a save are redisplayed on the 
  same page.  Successful updates return the user to the organizations home page.

*******************************************************************************
--%>

<%-- ************* import beans and use beans declarations****************  --%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
      import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*,com.ams.tradeportal.common.cache.*"%>
                         
<jsp:useBean  id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" 
             scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  BankOrganizationGroupWebBean bankGroup;
  PaymentReportingCode1WebBean pmtRptCode; //Suresh CR-655 03/03/2011 Add 
  PaymentReportingCode2WebBean pmtRpt2Code; //Suresh CR-655 03/03/2011 Add 
  String oid = null;
  DocumentHandler doc;
  QueryListView queryListView = null; //Suresh CR-655 03/03/2011 Add 
  
  boolean showButton =true; //Suresh IR-SAUL041337448 03/20/2011 Add
  boolean showSaveButton=true;
  boolean showSaveCloseButton=true;
  boolean showDeleteButton=true;
  boolean showCloseButton=true;
  boolean isReadOnly=false;
  String buttonPressed ="";
  boolean populateFromDB=false;
  boolean populateFromDocs=false;
  boolean insertMode=false;
  boolean defaultCheckBoxValue = false;
  boolean incldbnkDefaultCheckBoxValue = false;
  boolean errorFlag = false;
  String  helpSensitiveLink= null;
      
  String loginRights = userSession.getSecurityRights();
  String userLocale  = userSession.getUserLocale();
  String dropdownOptions = null;
 //Suresh CR-655 03/03/2011 Begin
  String sValue = null;
  String sValue2Pymt = null;
//MEer Rel 9.3.5 CR-1027
	String airWayBillPDFType = null;
	String approvalToPayPDFType = null;
	String directSendCollPDFType = null;
	String exportCollectionPDFType = null;
	String importLCPDFType = null;
	String loanRequestPDFType = null;
	String outgoingGuaranteePDFType = null;
	String outgoingStandbyLCPDFType = null;
	String requestToAdvisePDFType = null;
	String shippingGuaranteePDFType = null;
	boolean isBasicFormChecked = false;
	boolean isExpandedFormChecked = false;

	int numItems;
	int num2Pymt;
	int iLoop;
	int i2Loop;
	int PAYMENT_REPORTING_CODE_1_COUNT = 4;
	int PAYMENT_REPORTING_CODE_2_COUNT = 4;
	int rowCount = 0;
	int row2Count = 0;
	Vector vVector;
	Vector vVector2Pymt;
	List paymentReportingCode1List = new ArrayList(
			PAYMENT_REPORTING_CODE_1_COUNT);
	List paymentReportingCode2List = new ArrayList(
			PAYMENT_REPORTING_CODE_2_COUNT);

	if (request.getParameter("numberOfMultipleObjects") != null) {
		rowCount = Integer.parseInt(request
				.getParameter("numberOfMultipleObjects"));
	} else {
		rowCount = 8;
	}

	if (request.getParameter("numberOfMultipleObjects1") != null) {
		row2Count = Integer.parseInt(request
				.getParameter("numberOfMultipleObjects1"));
	} else {
		row2Count = 8;
	}
	//Suresh CR-655 03/03/2011 End

	// register the bean that has to be used
	beanMgr.registerBean(
			"com.ams.tradeportal.busobj.webbean.BankOrganizationGroupWebBean",
			"BankOrganizationGroup");
	bankGroup = (BankOrganizationGroupWebBean) beanMgr
			.getBean("BankOrganizationGroup");

	// Check for permissions on this page and redirect to OrganizationsHome if
	// can't view or maintain.
	if (SecurityAccess.hasRights(loginRights,
			SecurityAccess.MAINTAIN_BG_BY_CB)) {
		isReadOnly = false;
	} else if (SecurityAccess.hasRights(loginRights,
			SecurityAccess.VIEW_BG_BY_CB)) {
		isReadOnly = true;
	} else {

		// Don't have maintain or view access, send the user back.
		formMgr.setCurrPage("OrganizationsHome");
		String physicalPage = NavigationManager.getNavMan()
				.getPhysicalPage("OrganizationsHome", request);
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  }
      
  // this page may be called from the Organizations  Home page
  // or from this page when an error occurs
  // determine where the page is coming from and populate
  // the Bank Group accordingly
  doc = formMgr.getFromDocCache();
      buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
       Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");
  if (doc.getDocumentNode("/In/BankOrganizationGroup") == null) {
            //Suresh CR-655 03/03/2011 Begin
            if (request.getParameter("oid") != null) {
                  oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
            } else {
                  oid = doc.getAttribute("/In/bank_group_oid"); 
            }
			
            //Suresh CR-655 03/03/2011 End
     // there is no data from document cache - this 
     // page must come from Organizations Home page
     // if in insert mode, there is no need to get data
            if (oid == null || "0".equals(oid)) {
                  populateFromDocs = false; //Suresh CR-655 03/03/2011 Add
                  populateFromDB = false; //Suresh CR-655 03/03/2011 Add
                  insertMode = true;
                  oid = "0"; //Suresh CR-655 03/03/2011 Add
            }
            else {
                  // We have a good oid so we can retrieve.
                  populateFromDocs = false; //Suresh CR-655 03/03/2011 Add
                  populateFromDB = true;
                  insertMode = false; //Suresh CR-655 03/03/2011 Add
            }
  } else {
            //Suresh CR-655 03/03/2011 Begin
            // this must be returned from Bank Groups detail due to an error
            populateFromDocs = true;
            populateFromDB = false;
            errorFlag = true;
            
            //Suresh CR-655 03/03/2011 End
          String maxError = doc.getAttribute("/Error/maxerrorseverity");
          oid = doc.getAttribute("/Out/BankOrganizationGroup/organization_oid");
          if(oid == null || "0".equals(oid) || "".equals(oid)){
            oid = doc.getAttribute("/In/BankOrganizationGroup/organization_oid");
          }
            
          if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
             // We've returned from a save/update/delete that was successful
             // We shouldn't be here.  Forward to the OrganizationsHome page.
        	  populateFromDocs = false; 
              populateFromDB = true;
            }else{
            	// We've returned from a errors
                        oid = doc.getAttribute("/In/BankOrganizationGroup/organization_oid");
                        if ("0".equals(oid))
                        {
                      	    insertMode = true;
                        }
                        else
                        {
                         // Not in insert mode, use oid from output doc.
                        	insertMode = false;
                        }
            }
      }
      //Suresh CR-655 03/03/2011 Begin
      if (!populateFromDB && !populateFromDocs) {
            // Creating New Payment Report Code1
            for (int i = 0; i < PAYMENT_REPORTING_CODE_1_COUNT; i++) {
                  paymentReportingCode1List
                              .add(beanMgr.createBean(PaymentReportingCode1WebBean.class, "PaymentReportingCode1"));
            }
      }
      if (!populateFromDB && !populateFromDocs) {
            // Creating New Payment Report Code2
            for (int i = 0; i < PAYMENT_REPORTING_CODE_2_COUNT; i++) {
            paymentReportingCode2List.add(beanMgr.createBean(PaymentReportingCode2WebBean.class, "PaymentReportingCode2"));
            }
      }
      //Suresh CR-655 03/03/2011 End
//    else {
      if (populateFromDB) {
                  populateFromDocs = false;
                  bankGroup.getById(oid);
                  //Suresh CR-655 03/03/2011 Begin
                  if (bankGroup.getAttribute("organization_oid").equals(""))
                  {
                        insertMode        = true;
                     oid       = "0";
                     populateFromDocs = false;
                  }
                  else
                  {
                    insertMode = false;
                  }
                  //Suresh CR-655 03/03/2011 End
  }

            if (populateFromDocs) {
                  // Populate the bank organization group bean with the output doc.
                  try {
                        bankGroup.populateFromXmlDoc(doc.getFragment("/In"));
                  } catch (Exception e) {
                        System.out
                                    .println("Contact Administrator: Unable to get Bank Group attributes "
                                                + " for oid: "
                                                + oid
                                                + " "
                                                + e.toString());
                  }
        

                  //Suresh CR-655
                  // Populate PaymentReportingCode1 bean with PaymentReportingCode1 info from document handler
                  vVector = doc.getFragments("/In/BankOrganizationGroup/PaymentReportingCode1List");
                  numItems = vVector.size();
      
                  Debug.debug("Size = " + numItems);
                  Debug.debug(doc.toString());

                  for (iLoop = 0; iLoop < numItems; iLoop++) {
                        pmtRptCode = beanMgr.createBean(PaymentReportingCode1WebBean.class, "PaymentReportingCode1");
      
                        DocumentHandler pymtDoc = (DocumentHandler) vVector
                                    .elementAt(iLoop);
                        sValue = pymtDoc
                                    .getAttribute("/payment_reporting_code_1_oid");
                        pmtRptCode.setAttribute("payment_reporting_code_1_oid",
                                    sValue);
      
                        sValue = pymtDoc.getAttribute("/code");
                        pmtRptCode.setAttribute("code", sValue);
      
                        sValue = pymtDoc.getAttribute("/short_description");
                        pmtRptCode.setAttribute("short_description", sValue);
      
                        sValue = pymtDoc.getAttribute("/description");
                        pmtRptCode.setAttribute("description", sValue);
                        
                        
                        paymentReportingCode1List.add(pmtRptCode);
                  }
                  // Fill Empty Objects to make it Count 8.
                  if (numItems < PAYMENT_REPORTING_CODE_1_COUNT) {
                        for (iLoop = numItems; iLoop < PAYMENT_REPORTING_CODE_1_COUNT; iLoop++) {
                              paymentReportingCode1List
                                          .add(beanMgr.createBean(PaymentReportingCode1WebBean.class, "PaymentReportingCode1"));
                        }
                  }
            }
            
            if (populateFromDocs) {
                  // Populate the bank organization group bean with the output doc.
                  try {
                        bankGroup.populateFromXmlDoc(doc.getFragment("/In"));
                  } catch (Exception e) {
                        System.out
                                    .println("Contact Administrator: Unable to get Bank Group attributes "
                                                + " for oid: "
                                                + oid
                                                + " "
                                                + e.toString());
                  }
                  //Suresh CR-655 03/03/2011 Begin
                  // Populate PaymentReportingCode1 bean with PaymentReportingCode1 info from document handler
                  vVector2Pymt = doc.getFragments("/In/BankOrganizationGroup/PaymentReportingCode2List");
                  num2Pymt = vVector2Pymt.size();
      
                  Debug.debug("Size = " + num2Pymt);
                  Debug.debug(doc.toString());
				  
                  for (i2Loop = 0; i2Loop < num2Pymt; i2Loop++) {
                        pmtRpt2Code = beanMgr.createBean(PaymentReportingCode2WebBean.class, "PaymentReportingCode2");
      
                        DocumentHandler pymtDoc2 = (DocumentHandler) vVector2Pymt
                                    .elementAt(i2Loop);
                        sValue2Pymt = pymtDoc2
                                    .getAttribute("/payment_reporting_code_2_oid");
                        pmtRpt2Code.setAttribute("payment_reporting_code_2_oid",
                                    sValue2Pymt);
      
                        sValue2Pymt = pymtDoc2.getAttribute("/code");
                        pmtRpt2Code.setAttribute("code", sValue2Pymt);
      
                        sValue2Pymt = pymtDoc2.getAttribute("/short_description");
                        pmtRpt2Code.setAttribute("short_description", sValue2Pymt);
      
                        sValue2Pymt = pymtDoc2.getAttribute("/description");
                        pmtRpt2Code.setAttribute("description", sValue2Pymt);
                        
                        
                        paymentReportingCode2List.add(pmtRpt2Code);
                  }
                  // Fill Empty Objects to make it Count 8.
                  if (num2Pymt < PAYMENT_REPORTING_CODE_2_COUNT) {
                        for (i2Loop = num2Pymt; i2Loop < PAYMENT_REPORTING_CODE_2_COUNT; i2Loop++) {
                              paymentReportingCode2List
                                          .add(beanMgr.createBean(PaymentReportingCode2WebBean.class,  "PaymentReportingCode2"));
                        }
                  }
            }
            //Suresh CR-655 03/03/2011 End
             if (isReadOnly || insertMode)
               {
                  showDeleteButton = false;

                  if (isReadOnly)
                  {
                     showSaveButton = false;
                     showButton = false; //Suresh IR-SAUL041337448 03/20/2011 Add
                     showSaveCloseButton=false;
                  }
               }

      //Suresh CR-655 03/03/2011 Begin
      // Populate PAYMENT_REPORTING_CODE_1 bean with code info from the database
      StringBuilder sql = new StringBuilder();
      queryListView = null;
      try {
            if (populateFromDB) {
                  queryListView = (QueryListView) EJBObjectFactory
                              .createClientEJB(formMgr.getServerLocation(),
                                          "QueryListView");

                  sql.append("select payment_reporting_code_1_oid, code, short_description, description");
                  sql.append(" from payment_reporting_code_1");
                  sql.append(" where p_bank_group_oid = ?");
                  sql.append(" order by ");
                  sql.append(resMgr.localizeOrderBy("code"));
                  Debug.debug(sql.toString());

                  queryListView.setSQL(sql.toString(),new Object[]{oid});
                  queryListView.getRecords();

                  DocumentHandler pymtList = new DocumentHandler();
                  pymtList = queryListView.getXmlResultSet();
                  Debug.debug(pymtList.toString());

                  vVector = pymtList.getFragments("/ResultSetRecord");
                  numItems = vVector.size();

                  if (numItems > 8) {
                        rowCount = numItems;
                  }

                  for (iLoop = 0; iLoop < numItems; iLoop++) {
                        pmtRptCode = beanMgr.createBean(PaymentReportingCode1WebBean.class, "PaymentReportingCode1");
                        DocumentHandler pymtDoc = (DocumentHandler) vVector
                                    .elementAt(iLoop);
                        sValue = pymtDoc
                                    .getAttribute("/PAYMENT_REPORTING_CODE_1_OID");
                        pmtRptCode.setAttribute("payment_reporting_code_1_oid",
                                    sValue);

                        sValue = pymtDoc.getAttribute("/CODE");
                        pmtRptCode.setAttribute("code", sValue);

                        sValue = pymtDoc.getAttribute("/SHORT_DESCRIPTION");
                        pmtRptCode.setAttribute("short_description", sValue);

                        sValue = pymtDoc.getAttribute("/DESCRIPTION");
                        pmtRptCode.setAttribute("description", sValue);

                        paymentReportingCode1List.add(pmtRptCode);
                  }
                  // Fill Empty Objects to make it Count 8.
                  for (iLoop = numItems; iLoop < PAYMENT_REPORTING_CODE_1_COUNT; iLoop++) {
                        paymentReportingCode1List.add(beanMgr.createBean(PaymentReportingCode1WebBean.class,"PaymentReportingCode1"));
                  }
            }
      } catch (Exception e) {
            e.printStackTrace();
      } finally {
            try {
                  if (queryListView != null) {
                        queryListView.remove();
                  }
            } catch (Exception e) {
                  System.out.println("Error removing querylistview in BankGroupDetail.jsp!");
            }
      }
      StringBuilder sql2 = new StringBuilder();
      queryListView = null;
      try {
            if (populateFromDB) {
                  queryListView = (QueryListView) EJBObjectFactory
                              .createClientEJB(formMgr.getServerLocation(),
                                          "QueryListView");

                  sql2.append("select payment_reporting_code_2_oid, code, short_description, description");
                  sql2.append(" from payment_reporting_code_2");
                  sql2.append(" where p_bank_group_oid = ?");
                  sql2.append(" order by ");
                  sql2.append(resMgr.localizeOrderBy("code"));
                  Debug.debug(sql2.toString());

                  queryListView.setSQL(sql2.toString(),new Object[]{oid});
                  queryListView.getRecords();

                  DocumentHandler pymtList2 = new DocumentHandler();
                  pymtList2 = queryListView.getXmlResultSet();
                  Debug.debug(pymtList2.toString());

                  vVector2Pymt = pymtList2.getFragments("/ResultSetRecord");
                  num2Pymt = vVector2Pymt.size();
      
                  if (num2Pymt > 8) {
                        row2Count = num2Pymt;
                  }

                  for (i2Loop = 0; i2Loop < num2Pymt; i2Loop++) {
                        pmtRpt2Code = beanMgr.createBean(PaymentReportingCode2WebBean.class, "PaymentReportingCode2");
                        DocumentHandler pymtDoc2 = (DocumentHandler) vVector2Pymt
                                    .elementAt(i2Loop);
                        sValue2Pymt = pymtDoc2
                                    .getAttribute("/PAYMENT_REPORTING_CODE_2_OID");
                        pmtRpt2Code.setAttribute("payment_reporting_code_2_oid",
                                    sValue2Pymt);

                        sValue2Pymt = pymtDoc2.getAttribute("/CODE");
                        pmtRpt2Code.setAttribute("code", sValue2Pymt);

                        sValue2Pymt = pymtDoc2.getAttribute("/SHORT_DESCRIPTION");
                        pmtRpt2Code.setAttribute("short_description", sValue2Pymt);

                        sValue2Pymt = pymtDoc2.getAttribute("/DESCRIPTION");
                        pmtRpt2Code.setAttribute("description", sValue2Pymt);

                        paymentReportingCode2List.add(pmtRpt2Code);
                  }
                  // Fill Empty Objects to make it Count 8.
                  for (i2Loop = num2Pymt; i2Loop < PAYMENT_REPORTING_CODE_2_COUNT; i2Loop++) {
                        paymentReportingCode2List.add(beanMgr.createBean(PaymentReportingCode2WebBean.class,"PaymentReportingCode2"));
                  }
            }
      } catch (Exception e) {
            e.printStackTrace();
      } finally {
            try {
                  if (queryListView != null) {
                        queryListView.remove();
                  }
            } catch (Exception e) {
                  System.out.println("Error removing querylistview in BankGroupDetail.jsp!");
            }
      }
      //Suresh CR-655 03/03/2011 End
      //MEer Rel 9.3.5 CR-1027   
      airWayBillPDFType = bankGroup.getAttribute("airway_bill_pdf_type");
      approvalToPayPDFType = bankGroup.getAttribute("atp_pdf_type");
      directSendCollPDFType = bankGroup.getAttribute("exp_col_pdf_type");
      exportCollectionPDFType = bankGroup.getAttribute("exp_oco_pdf_type");
      importLCPDFType = bankGroup.getAttribute("imp_dlc_pdf_type");
      loanRequestPDFType = bankGroup.getAttribute("lrq_pdf_type");
      outgoingGuaranteePDFType = bankGroup.getAttribute("gua_pdf_type");
      outgoingStandbyLCPDFType = bankGroup.getAttribute("slc_pdf_type");
      requestToAdvisePDFType = bankGroup.getAttribute("rqa_pdf_type");
      shippingGuaranteePDFType = bankGroup.getAttribute("shp_pdf_type");
      
%>



<%-- ************** HTML begins here ****************  --%>

<%
  // onLoad is set to default the cursor to the Name field but only if the 
  // page is not in readonly mode.
  String onLoad = "";
  if (!isReadOnly) {
    onLoad = "document.BankGroupDetail.BankGroupName.focus();";
  }
%>

<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<%-- Begin inclusion of JavaScript functions, image rollovers, etc --%>
 
 <jsp:include page="/common/ButtonPrep.jsp" />

<%-- End inclusion of JavaScript functions, image rollovers, etc --%>

<%-- ************** Begin Form ******************************* --%>
<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="AdminCorpCust.BankGroup"/>
      <jsp:param name="helpUrl" value="admin/bank_group_detail.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (insertMode) {
    subHeaderTitle = resMgr.getText("BankGroups.NewBankGroup",TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = bankGroup.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>
      
      <form name="BankGroupDetail" id="BankGroupDetail" method="post" data-dojo-type="dijit.form.Form"
                        action="<%=formMgr.getSubmitAction(response)%>">
                              <input type=hidden value="" name=buttonName>

      <%
      
      // build parameters for inclusion to the BankGroupDetailForm
      // which is used by the mediator or bean, eg. security rights and locks
      
      Hashtable addlParms = new Hashtable();
      if (!insertMode)
            addlParms.put("organization_oid", oid);
      else
      {
            addlParms.put("organization_oid", "0");
            addlParms.put("activation_status", TradePortalConstants.ACTIVE);
      }
      
      addlParms.put("owner_org_oid", userSession.getOwnerOrgOid());
      addlParms.put("client_bank_oid", userSession.getOwnerOrgOid());
      addlParms.put("login_oid", userSession.getUserOid());
      addlParms.put("login_rights", userSession.getSecurityRights());
      addlParms.put("opt_lock", bankGroup.getAttribute("opt_lock"));
      
      %>
      <%  helpSensitiveLink = OnlineHelp.createContextSensitiveLink("admin/bank_group_detail.htm",  resMgr, userSession); %>
      
      <%= formMgr.getFormInstanceAsInputField("BankGroupDetailForm", addlParms) %>
      
            
        <%-- ******** Begin bank groups display/entry area ********** --%>
  <div class="formArea">
  <jsp:include page="/common/ErrorSection.jsp" />

  <div class="formContent">
                       

 
   
  <%= widgetFactory.createSectionHeader("1", "BankGroups.General") %>
 <%--  <div class="columnLeft"> --%>
        <%= widgetFactory.createTextField( "BankGroupName", "BankGroups.BankGroupName", bankGroup.getAttribute("name"), "35", isReadOnly, true, false, "style='width:275px'","regExp:'^[a-zA-Z0-9 \\\r\\\n]*$'","")%>
    
          
        <%= widgetFactory.createTextField( "BankGroupDescription", "BankGroups.Description",bankGroup.getAttribute("description"), "35", isReadOnly, true, false, "style='width:275px'","","")%>
  <%-- </div>        
  <div class="columnRight">  
       <%=widgetFactory.createTextArea("SupportInformation","BankGroups.SupportInformation",bankGroup.getAttribute("support_information"),isReadOnly, false, false,"style=\"min-height: 60px;\"", "", "")%>
  </div>      
        <div style="clear: both"></div> --%>
</div>
  <%-- ******** Begin email details display/entry area ********** --%>
 
  <%= widgetFactory.createSectionHeader("EmailDetails", "BankGroups.EmailDetails") %>

       <div class="columnLeft">

       
        <%
            dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE,bankGroup.getAttribute("email_language"),userLocale);
          
            out.println(widgetFactory.createSelectField( "emailLanguage","BankGroups.EmailLanguage1"," ", dropdownOptions, isReadOnly,false, false,"class='char10'", "", ""));
        %>
      
    
        <%=widgetFactory.createTextField("systemName","BankGroups.TradeSystemName",bankGroup.getAttribute("system_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
      
      
        <%=widgetFactory.createTextField("senderEmailAddress","BankGroups.SenderEmailAddress",bankGroup.getAttribute("sender_email_address"),"35",isReadOnly,false,false,"class='char30'","","" )%>
     
   <%=widgetFactory.createTextField("senderName","BankGroups.SenderName",bankGroup.getAttribute("sender_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
       
    
        <%=widgetFactory.createTextField("bankUrl","BankGroups.BankURL",bankGroup.getAttribute("bank_url"),"65",isReadOnly,false,false,"class='char30'","","" )%>
      </div>
      <div class="columnRight"> 
       
        <%
            dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE,bankGroup.getAttribute("email_language_2"),userLocale);
           
            out.println(widgetFactory.createSelectField( "emailLanguage2","BankGroups.EmailLanguage2"," ", dropdownOptions, isReadOnly,false, false,"class='char10'", "", ""));
        %>
       
      <%=widgetFactory.createTextField("systemName2","BankGroups.TradeSystemName",bankGroup.getAttribute("system_name_2"),"35",isReadOnly,false,false,"class='char30'","","" )%>   
           
     
        <%=widgetFactory.createTextField("senderEmailAddress2","BankGroups.SenderEmailAddress",bankGroup.getAttribute("sender_email_address_2"),"35",isReadOnly,false,false,"class='char30'","","" )%>
        
       <%=widgetFactory.createTextField("senderName2","BankGroups.SenderName",bankGroup.getAttribute("sender_name_2"),"35",isReadOnly,false,false,"class='char30'","","" )%>
       
        <%=widgetFactory.createTextField("bankUrl2","BankGroups.BankURL",bankGroup.getAttribute("bank_url_2"),"65",isReadOnly,false,false,"class='char30'","","" )%>
     </div>
     <div style="clear: both"></div>
  <%
  if ((!insertMode || errorFlag) && (bankGroup.getAttribute("do_not_reply_ind").equals(TradePortalConstants.INDICATOR_YES)))
  {
    defaultCheckBoxValue = true;
  }
  else
  {
    defaultCheckBoxValue = false;
  }
  if ((!insertMode || errorFlag) && (bankGroup.getAttribute("incld_bnk_url_trade").equals(TradePortalConstants.INDICATOR_YES)))
  {
    incldbnkDefaultCheckBoxValue = true;
  }
  else
  {
	  incldbnkDefaultCheckBoxValue = false;
  }
  
%>
<div style="clear:both">&nbsp;</div>
<div style="padding-left:14px;">
<%=widgetFactory.createCheckboxField("doNotReplyInd","",defaultCheckBoxValue,isReadOnly,false,"","","none")%>
<%=resMgr.getText("BankGroups.DoNotReplyTrade",TradePortalConstants.TEXT_BUNDLE)%>
</div>
<div style="clear:both">&nbsp;</div>
<div style="padding-left:14px;">
<%=widgetFactory.createCheckboxField("incldBnkURLTrade","",incldbnkDefaultCheckBoxValue,isReadOnly,false,"","","none")%>
<%=resMgr.getText("BankGroups.IncldBnkURLTrade",TradePortalConstants.TEXT_BUNDLE)%>
</div>

</div>
  <%-- ******** End email details display/entry area ********** --%>
  
<%-- ********SHILPAR CR-597 start Begin Payment Beneficiary Email Details/entry area ********** --%>
  
       
        <%= widgetFactory.createSectionHeader("BeneEmailDetails", "BankGroups.BeneEmailDetails") %>

  

     
  
       
        <%
            dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE,
                                                                  bankGroup.getAttribute("bene_email_language"),
                                                                userLocale);
           
            out.println(widgetFactory.createSelectField( "beneEmailLanguage","BankGroups.EmailLanguage"," ", dropdownOptions, isReadOnly,false, false,"class='char10'", "", ""));

        %>
      
       
        <%=widgetFactory.createTextField("beneSystemName","BankGroups.PaymentSystemName",bankGroup.getAttribute("bene_system_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
        
       
        <%=widgetFactory.createTextField("beneSenderName","BankGroups.SenderName",bankGroup.getAttribute("bene_sender_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
       
       
        <%=widgetFactory.createTextField("beneSenderEmailAddress","BankGroups.SenderEmailAddress",bankGroup.getAttribute("bene_sender_email_address"),"35",isReadOnly,false,false,"class='char30'","","" )%>
        
     
        
       
      
        
        <%=widgetFactory.createTextField("beneBankUrl","BankGroups.BankURL",bankGroup.getAttribute("bene_bank_url"),"65",isReadOnly,false,false,"class='char30'","","" )%>
                  
                        <%
                        if ((!insertMode || errorFlag) && (bankGroup.getAttribute("bene_do_not_reply_ind").equals(TradePortalConstants.INDICATOR_YES)))
                        {
                          defaultCheckBoxValue = true;
                        }
                        else
                        {
                          defaultCheckBoxValue = false;
                        }
                        if ((!insertMode || errorFlag) && (bankGroup.getAttribute("incld_bnk_url_pmnt_ben").equals(TradePortalConstants.INDICATOR_YES)))
                        {
                          incldbnkDefaultCheckBoxValue = true;
                        }
                        else
                        {
                      	  incldbnkDefaultCheckBoxValue = false;
                        }
                        %>
                        
                        <div style="clear:both"></div>
                        <div style="padding-left:14px;">
                        <%
                        out.print(widgetFactory.createCheckboxField("beneDoNotReplyInd","",defaultCheckBoxValue,isReadOnly,false,"","","none"));
                        %>
                        <%=resMgr.getText("BankGroups.DoNotReplyPayment",TradePortalConstants.TEXT_BUNDLE)%>
                        </div>
                        <div style="clear:both"></div>
                        <div style="padding-left:14px;">
                        <%
                        out.print(widgetFactory.createCheckboxField("incldBnkURLPmntBen","",incldbnkDefaultCheckBoxValue,isReadOnly,false,"","","none"));
                        %>
                        <%=resMgr.getText("BankGroups.IncldBnkURLPmntBen",TradePortalConstants.TEXT_BUNDLE)%>
 						</div>
                  
      </div>
    
    <%-- ********SHILPAR CR-597 end End Payment Beneficiary Email Details/entry area ********** --%>
	
	<%-- CR 821 - Rel 8.3 PMitnala START -- Repair and Panel Authorisations Email Details  --%>    
	
	   <%= widgetFactory.createSectionHeader("RepairEmailDetails", "BankGroups.RepairEmailDetails") %>
          <%
            dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE,
                                                                  bankGroup.getAttribute("repair_email_language"),
                                                                userLocale);
           
            out.println(widgetFactory.createSelectField( "repairEmailLanguage","BankGroups.EmailLanguage"," ", dropdownOptions, isReadOnly,false, false,"class='char10'", "", ""));

        %>
        <%=widgetFactory.createTextField("repairSystemName","BankGroups.RepairSystemName",bankGroup.getAttribute("repair_system_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
        <%=widgetFactory.createTextField("repairSenderName","BankGroups.SenderName",bankGroup.getAttribute("repair_sender_name"),"35",isReadOnly,false,false,"class='char30'","","" )%>
        <%=widgetFactory.createTextField("repairSenderEmailAddress","BankGroups.SenderEmailAddress",bankGroup.getAttribute("repair_sender_email_address"),"35",isReadOnly,false,false,"class='char30'","","" )%>
        <%=widgetFactory.createTextField("repairBankUrl","BankGroups.BankURL",bankGroup.getAttribute("repair_bank_url"),"65",isReadOnly,false,false,"class='char30'","","" )%>
                  
                        <%
                        if ((!insertMode || errorFlag) && (bankGroup.getAttribute("repair_do_not_reply_ind").equals(TradePortalConstants.INDICATOR_YES)))
                        {
                          defaultCheckBoxValue = true;
                        }
                        else
                        {
                          defaultCheckBoxValue = false;
                        }
                        if ((!insertMode || errorFlag) && (bankGroup.getAttribute("incld_bnk_url_repr_panel").equals(TradePortalConstants.INDICATOR_YES)))
                        {
                        	incldbnkDefaultCheckBoxValue = true;
                        }
                        else
                        {
                        	incldbnkDefaultCheckBoxValue = false;
                        }
                        %>
                        
                        <div style="clear:both"></div>
                        <div style="padding-left:14px;">
                        <%
                        out.print(widgetFactory.createCheckboxField("repairDoNotReplyInd","",defaultCheckBoxValue,isReadOnly,false,"","","none"));
                        %>
                        <%=resMgr.getText("BankGroups.DoNotReplyRepair",TradePortalConstants.TEXT_BUNDLE)%>
                        </div>
                        <div style="clear:both"></div>
                        <div style="padding-left:14px;">
                        <%
                        out.print(widgetFactory.createCheckboxField("incldBnkURLReprNPanel","",incldbnkDefaultCheckBoxValue,isReadOnly,false,"","","none"));
                        %>
                        <%=resMgr.getText("BankGroups.IncldBnkURLReprNPanel",TradePortalConstants.TEXT_BUNDLE)%>
 						</div>
                  
      </div>

<%-- CR 821 - Rel 8.3 PMitnala END -- Repair and Panel Authorisations Email Details  --%>  	

<%-- Narayan CR 913 rel9.0 29-Jan-2014 Begin - Payables Management Supplier Notification Email Section  --%>
   <%@ include file="fragments/BankGroupDetail-PayableEmail.frag"%>
<%-- Narayan CR 913 rel9.0 29-Jan-2014 End  --%>
 
  <%= widgetFactory.createSectionHeader("BrandingHeading", "BankGroups.BrandingHeading") %>



          <%
             String brandingDir;
             // If no branding directory exists, default to that of the bank
            // if(!bankGroup.getAttribute("branding_directory").equals(""))
             if(!insertMode || populateFromDocs)
                 brandingDir = bankGroup.getAttribute("branding_directory");
             else 
                // brandingDir = userSession.getBrandingDirectory();
                   brandingDir="blueyellow";
          %>
          
          <%=widgetFactory.createTextField("branding_directory","BankGroups.BrandingDirectory",brandingDir,"10",isReadOnly,true,false,"","","" )%>
  <%-------
          <%
             String brandButtonInd;
             if(!insertMode || populateFromDocs)
                 brandButtonInd = bankGroup.getAttribute("brand_button_ind");
             else if (userSession.getBrandButtonInd()) {
                 brandButtonInd = TradePortalConstants.INDICATOR_YES;
             }else {
                 brandButtonInd = TradePortalConstants.INDICATOR_NO;
             }
          %>
          <%= InputField.createCheckboxField("brand_button_ind", TradePortalConstants.INDICATOR_YES, 
                      resMgr.getText("BankGroups.BrandButtonInd", TradePortalConstants.TEXT_BUNDLE) ,
                      TradePortalConstants.INDICATOR_YES.equals(brandButtonInd), "ListText", "ListText", isReadOnly) %>
          ---------%>
         </div>

  <%-- ******** End bank groups display/entry area ********** --%>
  <%-- ******** DK CR-581/640  Cross Rate Calculation Rule area ******** --%> 
  <%= widgetFactory.createSectionHeader("CrossRateCalcRulesHeading","BankGroups.CrossRateCalcRulesHeading") %>
  <%

  String crossRateCalcRuleSql = "select cross_rate_calc_rule_oid, crcr.name as NAME from cross_rate_calc_rule crcr order by name";
  DocumentHandler crossRateCalcRuleList = DatabaseQueryBean
            .getXmlResultSet(crossRateCalcRuleSql.toString(), false,new ArrayList<Object>());
  String currCrossRateCalcRule = bankGroup.getAttribute("cross_rate_calc_oid");
  if (currCrossRateCalcRule == null)
      currCrossRateCalcRule = ""; 
  String pagOptions1 = ListBox.createOptionList(crossRateCalcRuleList,
            "CROSS_RATE_CALC_RULE_OID", "NAME", currCrossRateCalcRule);
  %>

   
            <%
   out.println(widgetFactory.createSelectField( "crossRateCalcRule","BankGroups.CrossRateCalcRule"," ", pagOptions1, isReadOnly,false, false,"class='char26'", "", ""));           %>
                        
      </div>
  <%-- ******** DK CR-581/640  Cross Rate Calculation Rule area ******** --%>

  
  <%= widgetFactory.createSectionHeader("FXOnlineHeading","BankGroups.FXOnlineHeading") %>
  <%-- ******** DK CR-640  Start of FX Online area ******** --%>
  <%
      Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
      DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
      String allowLiveMarketRates = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_LIVE_MARKET_RATES_REQ");
      if(allowLiveMarketRates.equals("Y")){
  %>
  
 
 
  <div class="columnLeft">
                                    <%
                                          String fxOnlineAvailableInd;
                                          boolean fxOnlineAvailableIndIndicator=false;                                        
                                          if (!insertMode || populateFromDocs)
                                                fxOnlineAvailableInd = bankGroup.getAttribute("fx_online_avail_ind");
                                          else {
                                                fxOnlineAvailableInd = TradePortalConstants.INDICATOR_NO;
                                                }
                                          //Added following condition for check box check
                                          if(fxOnlineAvailableInd.equals(TradePortalConstants.INDICATOR_YES))
                                                fxOnlineAvailableIndIndicator=true;
                                    %>
                                    <div style="padding-left:14px;">
                                    <%  out.print(widgetFactory.createCheckboxField("fxOnlineAvailable","BankGroups.FxOnlineAvailable",fxOnlineAvailableIndIndicator,isReadOnly,false,"","","none"));%>
                               
                                    </div>
            <br/>
       <%=widgetFactory.createTextField("defaultFxOnlineAcctId","BankGroups.DefaultFxOnlineAcctId",bankGroup.getAttribute("deflt_fx_online_acct_id"),"250",isReadOnly,false,false,"width","","" )%>
      </div>
      <div class="columnRight"> 
            <div class='formItem'></div>
           <div class='formItem'>
		<%=resMgr.getText("BankGroups.DomainOrgUrl1",TradePortalConstants.TEXT_BUNDLE)%><br/>
		<%=resMgr.getText("BankGroups.DomainOrgUrl2",TradePortalConstants.TEXT_BUNDLE)%><br/>
		<%=widgetFactory.createTextField("domainOrgUrl","",bankGroup.getAttribute("domain_org_url"),"30",isReadOnly,false,false,"style='width:275px'","","none" )%>
	</div>
      </div>
      
<%
      }
%>
	</div>
<%-- ******** DK CR-640  End of FX Online area ******** --%> 

<%-- Suresh CR-655  03/03/2011 Begin--%>
<%-- ******** Start of Payment Reporting Code1 area ******* --%>
<%= widgetFactory.createSectionHeader("PaymentReportingCode1","BankGroups.PaymentReportingCode1") %>
<%--Rpasupulati CR-1001 Start --%>
<%
				String repCode1Ach;
				boolean repCode1ForAch=false;                                        
				if (!insertMode || populateFromDocs)
				      repCode1Ach = bankGroup.getAttribute("reporting_code1_reqd_for_ach");
				else {
				      repCode1Ach = TradePortalConstants.INDICATOR_NO;
				      }
				
				if(repCode1Ach.equals(TradePortalConstants.INDICATOR_YES))
				      repCode1ForAch=true;		

				String repCode1Rtgs;
                boolean repCode1ForRtgs=false;                                        
                if (!insertMode || populateFromDocs)
                      repCode1Rtgs = bankGroup.getAttribute("reporting_code1_reqd_for_rtgs");
                else {
                      repCode1Rtgs = TradePortalConstants.INDICATOR_NO;
                      }
               
                if(repCode1Rtgs.equals(TradePortalConstants.INDICATOR_YES))
                      repCode1ForRtgs=true;

%>


 <%=widgetFactory.createCheckboxField("ReqACHPayMeth1","BankGroups.reqForACH",repCode1ForAch,isReadOnly,false,"","","none")%><br>
 <%=widgetFactory.createCheckboxField("ReqRTGPayMeth1","BankGroups.reqForRTGS",repCode1ForRtgs,isReadOnly,false,"","","none")%><br>
<%--Rpasupulati CR-1001 End --%>
<input type=hidden value="<%=rowCount%>" name="numberOfMultipleObjects" id="noOfRows1">
<table id="pymtCode1" class="formDocumentsTable" width="80%" border="0" cellspacing="0" cellpadding="0">
      <thead>
      <tr>
            
            <th width="15%" style="text-align:left">
            
            <b><%=resMgr.getText("BankGroups.ReportingCode",TradePortalConstants.TEXT_BUNDLE)%>      </b>
                        
                              
            </th>
            <th width="35%" style="text-align:left">
            
            <b><%=resMgr.getText("BankGroups.ShortDescription",TradePortalConstants.TEXT_BUNDLE)     %></b>
                                          
            </th>
            <th width="50%" style="text-align:left">
            
            <b><%=resMgr.getText("BankGroups.PaymentDescription",TradePortalConstants.TEXT_BUNDLE)%></b>
                        
            </th>
      </tr>
      </thead>
      <tbody>
      <%
            for (iLoop = 0; iLoop < paymentReportingCode1List.size(); iLoop++) {
                  pmtRptCode = (PaymentReportingCode1WebBean) paymentReportingCode1List.get(iLoop);
      %>
      <tr>
            
            <td>
                  
                   <%=widgetFactory.createTextField("code" + iLoop,"", pmtRptCode.getAttribute("code"),"3",isReadOnly,false,false,"onblur=\"this.value=this.value.toUpperCase()\"class='char8' onChange='checkBoxEnbDis1();'","" ,"none")%>
                  <% out.print("<INPUT TYPE=HIDDEN NAME='paymentReportingCode1Oid" + iLoop + "' VALUE='" + pmtRptCode.getAttribute("payment_reporting_code_1_oid") + "'>"); %>
            </td>
            
            <td>
            
             <%=widgetFactory.createTextField("shortDescription" + iLoop,"", pmtRptCode.getAttribute("short_description"),"15",isReadOnly,false,false,"class='char30'","" ,"none")%>
            </td>
            
            <td>
            
             <%=widgetFactory.createTextField("description" + iLoop,"", pmtRptCode.getAttribute("description"),"30",isReadOnly,false,false,"style='width: 28em;'","","none")%>
            </td>
      </tr>
            <%
                  }
            %>
      </tbody>
</table>


<div>
     <button data-dojo-type="dijit.form.Button" type="button" id="add4MoreRow" name="add4MoreRow"><%=resMgr.getText("BankGroups.Add4morerows",TradePortalConstants.TEXT_BUNDLE)%>
                        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                        add4MoreReportingCode1Rows();
                        </script>
                  </button> 
                   <%=widgetFactory.createHoverHelp("add4MoreRow", "Add4MoreRows") %>  
</div>            
</div>
<%-- ******** End of Payment Reporting Code1 area ******* --%>

<%-- ******** Start of Payment Reporting Code2 area ******* --%>
<%= widgetFactory.createSectionHeader("PaymentReportingCode2","BankGroups.PaymentReportingCode2") %>
<%--RPasupulati CR 1001 Start --%>
<%
				String repCode2Ach;
				boolean repCode2ForAch=false;                                        
				if (!insertMode || populateFromDocs)
				      repCode2Ach = bankGroup.getAttribute("reporting_code2_reqd_for_ach");
				else {
				      repCode2Ach = TradePortalConstants.INDICATOR_NO;
				      }
				
				if(repCode2Ach.equals(TradePortalConstants.INDICATOR_YES))
				      repCode2ForAch=true;		

				String repCode2Rtgs;
                boolean repCode2ForRtgs=false;                                        
                if (!insertMode || populateFromDocs)
                      repCode2Rtgs = bankGroup.getAttribute("reporting_code2_reqd_for_rtgs");
                else {
                      repCode2Rtgs = TradePortalConstants.INDICATOR_NO;
                      }
               
                if(repCode2Rtgs.equals(TradePortalConstants.INDICATOR_YES))
                      repCode2ForRtgs=true;

%>

<%=widgetFactory.createCheckboxField("ReqACHPayMeth2","BankGroups.reqForACH",repCode2ForAch,isReadOnly,false,"","","none")%><br>
 <%=widgetFactory.createCheckboxField("ReqRTGPayMeth2","BankGroups.reqForRTGS",repCode2ForRtgs,isReadOnly,false,"","","none")%>
 <%--RPasupulati CR 1001 End --%>
<table id="pymtCode2" class="formDocumentsTable" width="100%" border="0" cellspacing="0" cellpadding="0">
      <thead>
      <tr>
            
            <th width="15%" style="text-align:left">
            
            <b><%=resMgr.getText("BankGroups.ReportingCode",TradePortalConstants.TEXT_BUNDLE)%>      </b>
                                    
            </th>
            <th width="35%" style="text-align:left">
            
            <b><%=resMgr.getText("BankGroups.ShortDescription",TradePortalConstants.TEXT_BUNDLE)     %></b>
                                          
            </th>
            <th width="50%" style="text-align:left">
            
            <b><%=resMgr.getText("BankGroups.PaymentDescription",TradePortalConstants.TEXT_BUNDLE)%></b>
                        
            </th>
      </tr>
      </thead>
      <input type=hidden value="<%=row2Count%>" name="numberOfMultipleObjects1"
            id="noOfRows2">
            <tbody>
      <%
            for (i2Loop = 0; i2Loop < paymentReportingCode2List.size(); i2Loop++) {
                  pmtRpt2Code = (PaymentReportingCode2WebBean) paymentReportingCode2List.get(i2Loop);
      %>
      <tr>
            
            <td>
                  
                  <%=widgetFactory.createTextField("report2Code" + i2Loop,"",pmtRpt2Code.getAttribute("code"),"3",isReadOnly,false,false,"onblur=\"this.value=this.value.toUpperCase()\"class='char8'onChange='checkBoxEnbDis2();'","" ,"none")%>
                  <% out.print("<INPUT TYPE=HIDDEN NAME='paymentReportingCode2Oid" + i2Loop + "' VALUE='" + pmtRpt2Code.getAttribute("payment_reporting_code_2_oid") + "'>"); %>
            </td>
            
            <td>
             <%=widgetFactory.createTextField("report2ShortDescription" + i2Loop,"",  pmtRpt2Code.getAttribute("short_description"),"15",isReadOnly,false,false,"class='char30'","" ,"none")%>
            </td>
            
            <td>
            <%=widgetFactory.createTextField("report2Description" + i2Loop,"",pmtRpt2Code.getAttribute("description"),"30",isReadOnly,false,false,"style='width: 28em;'","" ,"none")%>
            </td>
      </tr>
            <%
                  }
            %>
      </tbody>
</table>
<div>
     <button data-dojo-type="dijit.form.Button" type="button" id="add4MoreRow2" name="add4MoreRow2"><%=resMgr.getText("BankGroups.Add4morerows",TradePortalConstants.TEXT_BUNDLE)%>
                        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                        add4MoreReportingCode2Rows();
                        </script>
                  </button>   
                  <%=widgetFactory.createHoverHelp("add4MoreRow2", "Add4MoreRows") %>
</div>             
			
</div>
<%-- //KMehta 10/15/2013 CR-910 Start --%>
<%-- SSikhakolli Rel 9.3.5 CR-1029 Adding enableAdminUpdateCentre, includeTTReimAllowed - Begin --%>
<%
	String expiryDateIndicator = bankGroup.getAttribute("slc_gua_expiry_date_required"); 
 	if(InstrumentServices.isBlank(expiryDateIndicator)){
		expiryDateIndicator = TradePortalConstants.INDICATOR_NO;
 	}
%>
	<%= widgetFactory.createSectionHeader("11","BankGroups.Setting") %>
		<div class="columnLeft ">
			<%= widgetFactory.createLabel("", "BankGroups.SlcGuarMsg", false, false, false, "") %>
                <div class="formItem">
	        <%= widgetFactory.createRadioButtonField("expiryDateInd", "ExpiryDate","BankGroups.ExpiryDate", TradePortalConstants.INDICATOR_YES, expiryDateIndicator.equals(TradePortalConstants.INDICATOR_YES), false) %>
			<br/>
			<%= widgetFactory.createRadioButtonField("expiryDateInd", "NoExpiryDate","BankGroups.NoExpiryDate", TradePortalConstants.INDICATOR_NO, expiryDateIndicator.equals(TradePortalConstants.INDICATOR_NO), false) %>
                </div>
                        <%= widgetFactory.createLabel("", "BankGroups.SSOSecurity", false, false, false, "") %>
			<%= widgetFactory.createCheckboxField("NoSSOInd","BankGroups.NoSSOInd",
					bankGroup.getAttribute("no_sso_ind").equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,"","","") %>
    	    </div>

		<div class="columnRight"> 
			<%= widgetFactory.createCheckboxField("includeTTReimAllowed","BankGroups.IncludeTTReimAllowed",
					bankGroup.getAttribute("include_tt_reim_allowed").equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,"","","") %>

<%		//ClientBank Web Bean
		ClientBankWebBean clientBank  = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
		clientBank.getById(userSession.getOwnerOrgOid());
		if(clientBank.getAttribute("enable_admin_update_centre").equals(TradePortalConstants.INDICATOR_YES)){
%>    
    		<%= widgetFactory.createCheckboxField("enableAdminUpdateCentre","BankGroups.EnableAdministrativeUpdateCentre",
    				bankGroup.getAttribute("enable_admin_update_centre").equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,"","","") %>
    		<%= widgetFactory.createCheckboxField("enableSessionSyncHTMLTag","BankGroups.EnableSessionSyncHTMLTag",
	    			bankGroup.getAttribute("enable_session_sync_html").equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,"","","") %>
<%
		}
%>		
		</div>	
                
	</div>	
<%-- SSikhakolli Rel 9.3.5 CR-1029 Adding enableAdminUpdateCentre, includeTTReimAllowed - End --%>
<%-- //KMehta 10/15/2013 CR-910 End --%>
<%--MEer CR-1027 06/10/2015 START  --%>
<%= widgetFactory.createSectionHeader("12","BankGroup.PDFApplicationsSetting") %>

<table id="pdfFormType" class="formDocumentsTable" width="80%" border="0" cellspacing="0" cellpadding="0">
     <thead>
      <tr>
          <th width="40%" style="text-align:left"></th>  
          <th width="30%" style="text-align:center">  
            <b><%=resMgr.getText("BankGroup.BasicPDFForm",TradePortalConstants.TEXT_BUNDLE)     %></b>                                
          </th>
          <th width="30%" style="text-align:center">
            <b><%=resMgr.getText("BankGroup.ExpandedPDFForm",TradePortalConstants.TEXT_BUNDLE)%></b>             
          </th>
       </tr>
      </thead>
      <tbody>
       <tr>     
           <td>
       			 <%= widgetFactory.createSubLabel("BankGroupPDFApplication.AirWaybills") %>
     	   </td>
         <%    
         if ((!insertMode || errorFlag) && (TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(airWayBillPDFType)) ) {
        	 isExpandedFormChecked = true;
         } else{
        	 isExpandedFormChecked = false;
         }
       %>
       <td> </td>
     <td align="center">     
          <%= widgetFactory.createCheckboxField("AirwayBillPDFType", "",  isExpandedFormChecked, isReadOnly, false, "", "", "none") %>
      </td>
      </tr>
       <tr>        
            <td>
       			 <%= widgetFactory.createSubLabel("BankGroupPDFApplication.ApprovalToPay") %>
     		</td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(approvalToPayPDFType))) {
        	 isBasicFormChecked = true;
         }else{
        	 isBasicFormChecked = false;
         }
       %>
       <td align="center">
        <%= widgetFactory.createCheckboxField("ApprovalToPayPDFType", "",  isBasicFormChecked, isReadOnly, false, "", "", "none") %>   
      </td>
      <td>  </td>
      </tr>
          <tr>        
            <td>
       			 <%= widgetFactory.createSubLabel("BankGroupPDFApplication.DirectSendColl") %>
     		</td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(directSendCollPDFType)) ) {
        	 isBasicFormChecked = true;
         }else{
        	 isBasicFormChecked = false;
         }
       %>
       <td align="center">     
        <%= widgetFactory.createCheckboxField("DirectSendCollectionPDFType", "", isBasicFormChecked, isReadOnly, false,  "", "", "none") %>
       
      </td>
      <td>   </td>
      </tr>
          <tr>        
            <td>
       			 <%= widgetFactory.createSubLabel("BankGroupPDFApplication.ExportCollection") %>
     		</td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(exportCollectionPDFType)) )  {
        	 isBasicFormChecked = true;
         }else{
        	 isBasicFormChecked = false;
         }
       %>
       <td align="center">     
        <%= widgetFactory.createCheckboxField("ExportCollectionPDFType", "",isBasicFormChecked, isReadOnly, false,  "", "", "none") %>   
      </td>
      <td> </td>
      </tr>
         <tr>   
            <td>
        		<%= widgetFactory.createSubLabel("BankGroupPDFApplication.ImportLC") %>
  		   </td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(importLCPDFType)) ) {
        	 isBasicFormChecked = true;
         }else{
        	 isBasicFormChecked = false;
         }
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(importLCPDFType)) ) {
        	 isExpandedFormChecked = true;
         } else{
        	 isExpandedFormChecked = false;
         }
		if ((!insertMode || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/In/BankOrganizationGroup/imp_dlc_bas_pdf_type")) && TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/In/BankOrganizationGroup/imp_dlc_exp_pdf_type"))) ){  
       	 		isExpandedFormChecked = true;
       	 		isBasicFormChecked = true;
        }	
        
       %>
        <td align="center">     
        <%= widgetFactory.createCheckboxField("ImportLCBasicPDFType", "", isBasicFormChecked, isReadOnly, false, "", "", "none") %>   
      </td>
      <td align="center">     
          <%= widgetFactory.createCheckboxField("ImportLCExpandedPDFType", "",  isExpandedFormChecked, isReadOnly, false,  "", "", "none") %>
      </td>
      </tr>
       <tr>        
            <td>
       			 <%= widgetFactory.createSubLabel("BankGroupPDFApplication.LoanRequest") %>
     		</td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(loanRequestPDFType)) ) {
        	 isExpandedFormChecked = true;
         }else{
        	 isExpandedFormChecked = false;
         }
       %>
       <td></td>
        <td align="center">     
        <%= widgetFactory.createCheckboxField("LoanRequestPDFType", "", isExpandedFormChecked, isReadOnly, false,   "", "", "none") %>    
      </td>
      </tr>
        <tr>   
            <td>
        <%= widgetFactory.createSubLabel("BankGroupPDFApplication.OutgoingGuarantee") %>
     </td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(outgoingGuaranteePDFType)) ) {
        	 isBasicFormChecked = true;
         } else {
        	 isBasicFormChecked = false;
         }
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(outgoingGuaranteePDFType)) ) {
        	 isExpandedFormChecked = true;
         }  else {
        	 isExpandedFormChecked = false;
         }
         if ((!insertMode || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/In/BankOrganizationGroup/gua_bas_pdf_type")) && TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/In/BankOrganizationGroup/gua_exp_pdf_type"))) ){  
           	 	isExpandedFormChecked = true;
           	 	isBasicFormChecked = true;
         }
         
       %>
        <td align="center">     
        <%= widgetFactory.createCheckboxField("OutgoingGuaranteeBasicPDFType", "",	isBasicFormChecked, isReadOnly, false,  "", "", "none") %>
       
      </td>
       <td align="center">     
               <%= widgetFactory.createCheckboxField("OutgoingGuaranteeEXPPDFType", "", isExpandedFormChecked, isReadOnly, false,  "", "", "none") %>
      </td>
      </tr>
        <tr>   
           <td>
        		<%= widgetFactory.createSubLabel("BankGroupPDFApplication.OutgoingSLC") %>
     	</td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(outgoingStandbyLCPDFType))){
        	 isBasicFormChecked = true;
         }else {
        	 isBasicFormChecked = false;
         }
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(outgoingStandbyLCPDFType))){
        	 isExpandedFormChecked = true;
         } else {
        	 isExpandedFormChecked = false;
         }
         if ((!insertMode || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/In/BankOrganizationGroup/slc_bas_pdf_type")) && TradePortalConstants.INDICATOR_YES.equals(doc.getAttribute("/In/BankOrganizationGroup/slc_exp_pdf_type"))) ){  
           	 	isExpandedFormChecked = true;
           	 	isBasicFormChecked = true;
         }
       %>
       <td align="center">     
        <%= widgetFactory.createCheckboxField("OutgoingStandbyLCBasicPDFType", "", isBasicFormChecked, isReadOnly, false,   "", "", "none") %>
       
      </td>
       <td align="center">      
          <%= widgetFactory.createCheckboxField("OutgoingStandbyLCEXPPDFType", "",  isExpandedFormChecked, isReadOnly, false,   "", "", "none") %>
      </td>
      </tr> 
       <tr>        
            <td>
       			 <%= widgetFactory.createSubLabel("BankGroupPDFApplication.RequestToAdvise") %>
     		</td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_BASIC_TYPE.equals(requestToAdvisePDFType))) {
        	 isBasicFormChecked = true;
         }else{
        	 isBasicFormChecked = false;
         }
       %>
       <td align="center">     
        <%= widgetFactory.createCheckboxField("RequestToAdvisePDFType", "",	isBasicFormChecked, isReadOnly, false, "", "", "none") %>
       
      </td>
      <td>   </td>
      </tr>
      <tr>        
            <td>
       			 <%= widgetFactory.createSubLabel("BankGroupPDFApplication.ShippingGuarantee") %>
     		</td>
         <%
         if ((!insertMode || errorFlag) && ( TradePortalConstants.APPLICATION_PDF_EXPANDED_TYPE.equals(shippingGuaranteePDFType))) {
        	 isExpandedFormChecked = true;
         }else{
        	 isExpandedFormChecked = false;
         }
       %>
       <td></td>
        <td align="center">     
        <%= widgetFactory.createCheckboxField("ShippingGuaranteePDFType", "", isExpandedFormChecked, isReadOnly, false, "", "", "none") %>   
      </td>
     </tr>
     </tbody>
    </table>
    </div>

<%--MEer CR-1027 06/10/2015 END  --%>


</div><%--formContent--%>
</div><%--formArea--%>
<%-- ******** End of Payment Reporting Code2 area ******* --%> 
<%-- ******** Suresh CR-655 03/03/2011 End ******** --%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form:'BankGroupDetail'">
                         <jsp:include page="/common/RefDataSidebar.jsp">      
                                   <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />      
                                   <jsp:param name="saveOnClick" value="saveOnClick" />
                                   <jsp:param name="showDeleteButton" value="<%= showDeleteButton %>" /> 
                                   <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButton %>" />
                                   <jsp:param name="saveCloseOnClick" value="saveCloseOnClick" />
                                   <jsp:param name="cancelAction" value="goToOrganizationsHome" />    
                                   <jsp:param name="showHelpButton" value="false" />
                                   <jsp:param name="showLinks" value="true" />
                                   <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />    
                                   <jsp:param name="error" value="<%=error%>" />
                         </jsp:include>        
                </div> <%--closes sidebar area--%>
      <%--
// Suresh CR-655 03/03/2011 Begin
 --%>
<script language="JavaScript">


function add4MoreReportingCode1Rows() {
	<%-- so that user can only add 2 rows --%>
	var tbl = document.getElementById('pymtCode1');<%-- to identify the table in which the row will get insert --%>
	var lastElement = tbl.rows.length;
	var NewRow="";
		 
	for(i=lastElement;i<lastElement+4;i++){
		
   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
		newRow.id = "newRowRep1_"+i;
		j = i-1;
		var name = "paymentReportingCode1Oid"+j;
        cell0.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"code'+j+'\" id=\"code'+j+'\" onblur=\"this.value=this.value.toUpperCase()\" class=\'char8\'  maxLength=\"3\">'
        cell1.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"shortDescription'+j+'\" id=\"shortDescription'+j+'\" onblur=\"this.value=this.value.toUpperCase()\" class=\'char30\'  maxLength=\"15\">'
        cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"description'+j+'\" id=\"description'+j+'\" onblur=\"this.value=this.value.toUpperCase()\" style=\"width:28em;\"  maxLength=\"30\"> <INPUT TYPE=HIDDEN NAME=\"'+ name + '\" VALUE=\"\">'
      
        require(["dojo/parser"], function(parser) {
         	parser.parse(newRow.id);
    	 });
	}
	document.getElementById("noOfRows1").value = eval(lastElement+3);   
}


function add4MoreReportingCode2Rows() {
	<%-- so that user can only add 2 rows --%>
	var tbl = document.getElementById('pymtCode2');<%-- to identify the table in which the row will get insert --%>
	var lastElement = tbl.rows.length;
		 
	for(i=lastElement;i<lastElement+4;i++){
   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
		newRow.id = "newRowRep2+"+i;
		j = i-1;
		var name = "paymentReportingCode2Oid"+j;
		cell0.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"report2Code'+j+'\" id=\"report2Code'+j+'\" onblur=\"this.value=this.value.toUpperCase()\" class=\'char8\'  maxLength=\"3\">'
		<%-- jgadela - 10/27/2014 R9.1 IR T36000033668  - corrected the missmatch report2Description and report2ShortDescription --%>
		cell1.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"report2ShortDescription'+j+'\" id=\"report2ShortDescription'+j+'\" onblur=\"this.value=this.value.toUpperCase()\" class=\'char30\'  maxLength=\"15\">'
        cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"report2Description'+j+'\" id=\"report2Description'+j+'\" onblur=\"this.value=this.value.toUpperCase()\" style=\"width:28em;\"  maxLength=\"30\"> <INPUT TYPE=HIDDEN NAME=\"'+ name + '\" VALUE=\"\">'
      
        require(["dojo/parser"], function(parser) {
         	parser.parse(newRow.id);
    	 });
	}
	document.getElementById("noOfRows2").value = eval(lastElement+3); 
}

</script>
<%--
// Suresh CR-655 03/03/2011 End
 --%>         
                    
</form>
</div>
</div>
</div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- Kiran  03/15/2013  Rel8.2 IR-T36000014710  Start --%> 
<%
  //save behavior defaults to without hsm
  //Kiran Added document.forms[0].submit(); for the two lines in order to save the form details
  
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  //String saveOnClick = "common.setButtonPressed('SaveTrans','0');document.forms[0].submit();";
  //String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0');document.forms[0].submit();";
%>
<%-- Kiran  03/15/2013  Rel8.2 IR-T36000014710  End --%>   
  

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
  

<%-- ************** End Form ******************************* --%>
</html>
<%-- ******************* Code Clean-up ********************* --%>

<%
beanMgr.unregisterBean("BankOrganizationGroup");

// Finally, reset the cached document to eliminate carryover of 
// information to the next visit of this page.
formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>
<script type="text/javascript">

require(["dijit/registry", "dojo/ready"],
	    function(registry, ready ) {
	      ready(function() {
var rep2Code0 = document.getElementById('report2Code0').value;

if(rep2Code0 != ""){
	dijit.byId('ReqACHPayMeth2').setAttribute("disabled",false);
	dijit.byId('ReqRTGPayMeth2').setAttribute("disabled",false);
}else{
	
	dijit.byId('ReqACHPayMeth2').setAttribute("disabled",true);
	dijit.byId('ReqRTGPayMeth2').setAttribute("disabled",true);
	}
var rep1Code0 = document.getElementById('code0').value;

if(rep1Code0 != ""){
	 dijit.byId('ReqACHPayMeth1').setAttribute("disabled",false);
	 dijit.byId('ReqRTGPayMeth1').setAttribute("disabled",false);
}else{
	
	 dijit.byId('ReqACHPayMeth1').setAttribute("disabled",true);
	 dijit.byId('ReqRTGPayMeth1').setAttribute("disabled",true);
		
	}

	      });
});
function checkBoxEnbDis1(){
	
	var report1Code0 = document.getElementById('code0').value;
	var report1Code1 = document.getElementById('code1').value;
	var report1Code2 = document.getElementById('code2').value;
	var report1Code3 = document.getElementById('code3').value;
	var ReqACHPayMeth1 = dijit.byId('ReqACHPayMeth1');
	var ReqRTGPayMeth1 = dijit.byId('ReqRTGPayMeth1');
	
	
	if(report1Code0 != ""||report1Code1 != "" ||report1Code2 != ""||report1Code3 != ""){
		ReqACHPayMeth1.setAttribute("disabled",false);
		ReqRTGPayMeth1.setAttribute("disabled",false);
	}else{
	ReqACHPayMeth1.setAttribute("disabled",true);
	ReqRTGPayMeth1.setAttribute("disabled",true);
	ReqACHPayMeth1.setAttribute("checked",false);
	ReqRTGPayMeth1.setAttribute("checked",false);
	}
}
function checkBoxEnbDis2(){
	
	var report2Code0 = document.getElementById('report2Code0').value;
	var report2Code1 = document.getElementById('report2Code1').value;
	var report2Code2 = document.getElementById('report2Code2').value;
	var report2Code3 = document.getElementById('report2Code3').value;
	var ReqACHPayMeth2 = dijit.byId('ReqACHPayMeth2');
	var ReqRTGPayMeth2 = dijit.byId('ReqRTGPayMeth2');
	
	if(report2Code0 != ""||report2Code1 != "" ||report2Code2 != ""||report2Code3 != ""){
		ReqACHPayMeth2.setAttribute("disabled",false);
		ReqRTGPayMeth2.setAttribute("disabled",false);
	}else{
	
		ReqACHPayMeth2.setAttribute("disabled",true);
		ReqRTGPayMeth2.setAttribute("disabled",true);
		ReqACHPayMeth2.setAttribute('checked',false);
		ReqRTGPayMeth2.setAttribute('checked',false);
	}
}
</script>