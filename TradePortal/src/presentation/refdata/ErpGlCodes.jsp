<%--
 *
 *     AAlubala - Rel8.2 CR741 - 01/04/2013                        
 *     ERP GL CODES - Refdata
 *     
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
   
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   boolean isNew = false;
   boolean showSaveAs = true;
   //
   //start - 

  String 	loginLocale 		= userSession.getUserLocale();
  String 	loginTimezone		= userSession.getTimeZone();
  String 	loginRights 		= userSession.getSecurityRights();
  String parentOrgID        = StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid());
  String 	tabPressed		= request.getParameter("tabPressed");	// This is a local attribute from the webBean
 
  DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  Hashtable 	secureParams		= new Hashtable();
  boolean	hasEditRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ERP_GL_CODES );
  boolean	isReadOnly		= true;
  boolean  insertMode      = true;
  boolean  showDelete      = true;
  boolean  showSave        = true;
  boolean  showSaveClose        = true;
  boolean       getDataFromDoc          = false;   
   //end
   String oid =  StringFunction.xssCharsToHtml(request.getParameter("oid"));
   //String loginRights;  
  
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.CustomerErpGlCodeWebBean", "CustomerErpGlCode");
   CustomerErpGlCodeWebBean customerErpGlCode = (CustomerErpGlCodeWebBean)beanMgr.getBean("CustomerErpGlCode");

   DocumentHandler doc      = null;
   
   //Get WidgetFactory instance..
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   String   refDataHome       = "goToRefDataHome";                //used for the close button and Breadcrumb link
   String buttonPressed ="";
   //Get security rights
   //loginRights = userSession.getSecurityRights();
   //IR T36000011362 - isReadOnly was being set to false
   //isReadOnly = false;

   doc = formMgr.getFromDocCache();
   buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
   
   Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");
   Debug.debug("doc from cache is " + doc.toString());

  

    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).
    String onLoad = "";
	//out.print("DEFAULTDOC: ["+defaultDoc.getFragment("/")+"]<br>");
	  if( hasEditRights ) 			//If the user has the right to Maintain/edit data SET Readonly to False.
		isReadOnly = false;
	
	  if( InstrumentServices.isBlank(oid) ) {
		oid = defaultDoc.getAttribute("/Out/oid");
	  }
	  //out.print("<br>oid from DEFAULTDOC: ["+oid+"]");
	  if( InstrumentServices.isNotBlank(parentOrgID) )
	  	customerErpGlCode.setAttribute("p_owner_oid", parentOrgID);
	  if( InstrumentServices.isNotBlank(oid) ) {
	     insertMode = false;
	     oid = EncryptDecrypt.decryptStringUsingTripleDes( oid, userSession.getSecretKey() );
	     Debug.debug("***** customer erp oid == " + oid);
	     customerErpGlCode.setAttribute("customer_erp_gl_code_oid", oid);
	     
	     customerErpGlCode.getDataFromAppServer();
	     
	     //out.print("<br>here> "+customerErpGlCode.getOidFieldName());
	     //out.print("<br>here2 "+customerErpGlCode.getAttribute(""));
	  }else {
	      oid = defaultDoc.getAttribute("/Out/CustomerErpGlCode/customer_erp_gl_code_oid");
	    
	      if( InstrumentServices.isNotBlank(oid) ) {
	    	  customerErpGlCode.setAttribute("customer_erp_gl_code_oid", oid);
	    	  customerErpGlCode.getDataFromAppServer();
	      }
	      else {
	         oid = "0";
	         insertMode = true;
	         isNew = true;
	          showSaveAs = true; 	         
	      }
	
	  }    
    
    
    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

    if (isReadOnly)
    {
        showSave = false;
        showDelete = false;
        showSaveClose = false;
        showSaveAs = false;
    }
    else if (isNew){
        showDelete = false;
        showSaveAs = false;
    }

   secureParams.put("loginRights", loginRights);
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">        <%-- Page Main Start --%>
  <div class="pageContent">   <%--  Page Content Start --%>


    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="ERP GL Codes"/>
      <jsp:param name="helpUrl" value="customer/paydiscount_detail.htm"/>
    </jsp:include>

<% 
  String subHeaderTitle = "";

%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      

<form name="ErpGlCodesDetailForm" id="ErpGlCodesDetailForm" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">

  <input type=hidden value="" name=buttonName>
  <input type=hidden value="" name=saveAsFlag>

<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />
<%


StringBuffer options = Dropdown.createGLCategoryOptions(resMgr, customerErpGlCode.getAttribute("erp_gl_category"));

%>
<div class="formContent">           <%--  Form Content Start --%>
   <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
      <div class="dijitTitlePaneContentInner"> 
   
	<table id="codesIDTable" width=100%>
		<thead>
			<tr style="height:35px">
				<th width=20% align=left><%= widgetFactory.createLabel("ERPGLCode","ErpGlCodes.ERPGLCode",isReadOnly,true,false,"")%></th>
				<th width=25% align=left><%= widgetFactory.createLabel("ERPGLCategory","ErpGlCodes.ERPGLCategory",isReadOnly,true,false,"")%></th>
				<th width=30% align=left><%= widgetFactory.createLabel("Description","ErpGlCodes.Description",isReadOnly,true,false,"")%></th>
				<th width=15% align=left><%= widgetFactory.createSubLabel("ErpGlCodes.DefaultGlInd") %></th>
				<th width=10% align=left><%= "&nbsp;&nbsp;"+widgetFactory.createSubLabel("ErpGlCodes.Deactivate") %></th>			
			</tr>
		</thead>	
		<tr id="erpglcode1">
		<input type=hidden value="" name="erpglcode" id="erpglcode">
			<td>      
			  <%=widgetFactory.createTextField("erpGlCode1","",
				customerErpGlCode.getAttribute("erp_gl_code") ,"17",isReadOnly,true,false, "style='width: 75px'", "", "none") %>
			</td> 
			<td>
			 <%= widgetFactory.createSelectField("erpGlCat1", "", 
			    " ", options.toString(), isReadOnly, true, false,"style='width: 150px'", "", "none") %>      
			</td> 			
			<td>      
			  <%=widgetFactory.createTextField("desc1","",
				customerErpGlCode.getAttribute("erp_gl_description") ,"30",isReadOnly,true,false, "style='width: 175px'", "", "none") %>
			</td>
			<td>
			<%=widgetFactory.createCheckboxField("defaultGlInd1", "", 
			customerErpGlCode.getAttribute("default_gl_code_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%>
			</td>			 
			<td>         
			<%=widgetFactory.createCheckboxField("deactivate1", "", 
			customerErpGlCode.getAttribute("deactivate_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%> 
			</td>		                                                
		</tr>
		<%if (isNew){ 
			for(int i=2; i<9; i++){
		%>

			<tr id="erpglcode"<%=i%>>
				<td>      
				  <%=widgetFactory.createTextField("erpGlCode"+i,"",
					customerErpGlCode.getAttribute("erp_gl_code") ,"17",isReadOnly,true,false, "style='width: 75px'", "", "none") %>
				</td> 
				<td>
				 <%= widgetFactory.createSelectField("erpGlCat"+i, "", 
				    " ", options.toString(), isReadOnly, true, false,"style='width: 150px'", "", "none") %>      
				</td> 			
				<td>      
				  <%=widgetFactory.createTextField("desc"+i,"",
					customerErpGlCode.getAttribute("erp_gl_description") ,"30",isReadOnly,true,false, "style='width: 175px'", "", "none") %>
				</td>
				<td>
				<%=widgetFactory.createCheckboxField("defaultGlInd"+i, "", 
				customerErpGlCode.getAttribute("default_gl_code_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%>
				</td>			 
				<td>         
				<%=widgetFactory.createCheckboxField("deactivate"+i, "", 
				customerErpGlCode.getAttribute("deactivate_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "none")%> 
				</td>		                                                
			</tr>		
		
		<%
			} 
		
		}
	
		%>
	
	</table> 
<br/>
	<%
	//IR T36000015498
if(isNew){
	if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="submit" id="add4MoreCodes">
				<%=resMgr.getText("common.Add4MoreGLCodesText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					add4MoreCodes();
				</script>
			</button>
		<% } else {		%>
					&nbsp;
		<% 
		}
}		
	
  if(isNew){
  for(int ii=1; ii<9; ii++){ %>
	  <input type=hidden value="<%=oid %>" name=oid<%=ii%>>
	  <input type=hidden value="<%=parentOrgID %>" name=powner<%=ii%>>
  <%}
  }else{
  %>
	  <input type=hidden value="<%=oid %>" name=oid1>
	  <input type=hidden value="<%=parentOrgID %>" name=powner1>		
 <%} %>
 

   </div>
  </div>
  </div>          <%--  Form Content End --%>
</div><%--formArea--%>

 <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'ErpGlCodesDetailForm'">
<jsp:include page="/common/RefDataSidebar.jsp">
      <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
      <jsp:param name="showSaveButton" value="<%= showSave %>" />
      <jsp:param name="saveOnClick" value="none" />
      <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
      <jsp:param name="saveCloseOnClick" value="none" />
      <jsp:param name="showHelpButton" value="true" />
      <jsp:param name="cancelAction" value="goToRefDataHome" />
      <jsp:param name="helpLink" value="" />
      <jsp:param name="saveAsDialogId" value="saveAsDialogId" />
      <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
      <jsp:param name="error" value="<%=error%>" />
      <jsp:param name="showLinks" value="false" />
</jsp:include>
</div> <%-- Side Bar End--%>

  <%= formMgr.getFormInstanceAsInputField("ErpGlCodesDetailForm", secureParams) %>
</form>

</div>            <%--  Page Content End --%> 
</div>            <%--  Page Main End --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%
  //save behavior defaults to without hsm
  
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />
  
 <div id="saveAsDialogId" style="display: none;"></div>
 
<%
  String focusField = "nameField";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          if(focusField){ 
                  focusField.focus();
            }
        }
      });
  });
  
  function submitSaveAs() 
  {
          if(window.document.saveAsForm.newProfileName.value == ""){
            return false;
          }
            document.ErpGlCodesDetailForm.nameField.value=window.document.saveAsForm.newProfileName.value;
            document.ErpGlCodesDetailForm.oid.value="0";
            <%-- Sets a hidden field with the 'button' that was pressed.  This --%>
            <%-- value is passed along to the mediator to determine what --%>
            <%-- action should be done. --%>
            document.ErpGlCodesDetailForm.saveAsFlag.value = "Y";
            document.ErpGlCodesDetailForm.buttonName.value = "SaveAndClose";
            document.ErpGlCodesDetailForm.submit();
            hideDialog("saveAsDialogId");
   }
  
  
	function add4MoreCodes() {
		<%--  IR T36000016909 Starts Rpasupulati --%>
		<%-- so that user can only add rows --%>
		var tbl = document.getElementById('codesIDTable');<%-- to identify the table in which the row will get insert --%>
		var lastElement = tbl.rows.length;
		var NewRow="";
			 
		for(i=lastElement;i<lastElement+4;i++){
			
	   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
	   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
			var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
			var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
			var cell3 = newRow.insertCell(3);<%-- fourth cell in the row --%>
			var cell4 = newRow.insertCell(4);<%-- fifth cell in the row --%>
			newRow.id = "newRowRep_"+i;
			j = i;
			var name = "erpglcode"+j;
	        cell0.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"erpGlCode'+j+'\" id=\"erpGlCode'+j+'\"  class=\'char15\'  style=\"width:75px;\"  maxLength=\"17\">'
	        cell1.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"erpGlCat'+j+'\"  style=\"width:150px;\" id=\"erpGlCat'+j+'\" ></select><INPUT TYPE=HIDDEN NAME=\"UserAuthorizedTemplateGroupOid'+(j*2+1)+'\" VALUE=\"\">'
	       	cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"desc'+j+'\" id=\"desc'+j+'\" style=\"width:175px;\"  maxLength=\"30\">'
	        cell3.innerHTML ='<input data-dojo-type=\'dijit.form.CheckBox\' name=\"defaultGlInd'+j+'\" id=\"defaultGlInd'+j+'\"  value=\'Y\'>'
	        cell4.innerHTML ='<input data-dojo-type=\'dijit.form.CheckBox\' name=\"deactivate'+j+'\" id=\"deactivate'+j+'\"  value=\'Y\'>'
			require(["dojo/parser", "dijit/registry"], function(parser, registry) {
         	parser.parse(newRow.id);
         	var erpGlCat = registry.byId("erpGlCat1");
            if(erpGlCat!=null){
              var theSelData = erpGlCat.store.data;
              for(var i=0; i<theSelData.length;i++){
                      registry.byId('erpGlCat'+(j)).store.data.push({name:theSelData[i].name,value:theSelData[i].value, id:theSelData[i].value});
              }
              registry.byId('erpGlCat'+(j)).attr('displayedValue', "");
            }
    	 });
		}
		document.getElementById("erpglcode").value = eval(lastElement+3);  
		<%--  IR T36000016909 Ends Rpasupulati --%>
	}

   function preSaveAndClose(){
  	 	 
 	  document.forms[0].buttonName.value = "SaveAndClose";
 	  return true;

	}
</script>
<%
  }
%>

</body>

</html>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("CustomerErpGlCode");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
