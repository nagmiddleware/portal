<%--
*******************************************************************************
  Announcement Transactions Home Page

  Description:  
     This page is used as the payment transactions main page.
   
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 

<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  Hashtable         secureParms           = new Hashtable();
  String            current2ndNav      = "";
  String            formName              = null;

  String  userOrgOid	 = userSession.getOwnerOrgOid();
  String            userOid               = null;
  String            userSecurityRights    = null;
  String            userSecurityType      = null;
  boolean 			maintainAccess		  = false;
  userSecurityRights = userSession.getSecurityRights();
  maintainAccess = SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_ANNOUNCEMENT);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  String newLink = formMgr.getLinkAsUrl("announcementDetail",response);

  userSession.setCurrentPrimaryNavigation("AdminNavigationBar.UsersAndSecurity");
  Map searchQueryMap =  userSession.getSavedSearchQueryData();
  Map savedSearchQueryData =null;
  if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("AnnouncementDataView");
	}
%>
<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="Announce"/>
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="Announcement.Home" />
      <jsp:param name="helpUrl"  value="customer/announcement_listview.htm"/>
    </jsp:include>

    <jsp:include page="/common/ErrorSection.jsp" />

    <div class="formContentNoSidebar">
      <%-- Komal PR ANZ Incident Report_Issue61 Starts --%>
      <div class="searchHeader">
        <div class="searchHeaderActions">
          <%--cquinton 10/8/2012 include gridShowCounts --%>
          <jsp:include page="/common/gridShowCount.jsp">
            <jsp:param name="gridId" value="announceGrid" />
          </jsp:include>
<%
  // if there is maintain access, display the Create button
  if (maintainAccess) {
%>
          <button data-dojo-type="dijit.form.Button" type="button" id="newButton">
            <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
            <script type="dojo/method" data-dojo-args="evt">
            </script>
          </button>	
          <%= widgetFactory.createHoverHelp("newButton","NewHoverText") %>
<%
  }
%>
        </div>
        <div style="clear:both"></div>
      </div>
      <%-- Komal PR ANZ Incident Report_Issue61 Ends --%>

<%
   String gridHtml = dgFactory.createDataGrid("announceGrid","AnnouncementDataGrid",null);
   String gridLayout = dgFactory.createGridLayout("AnnouncementDataGrid");
%>

      <%= gridHtml %>
    </div>

  </div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<form id="AnnouncementDeleteForm" name="AnnouncementDeleteForm"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
	<input type="hidden" name="buttonName" value="Delete">
    <%= formMgr.getFormInstanceAsInputField("AnnouncementDeleteForm", 
                                        secureParms) %>
</form>

<%@ include file="/common/GetSavedSearchQueryData.frag" %>



<script type="text/javascript">
  <%--- SureshL  5/23/2014 Rel8.4  IR-T36000028608 -start  ---%>  
var mAnnouncement = false;

   <% if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_ANNOUNCEMENT)) {%>
      mAnnouncement = true;
     <%
       } if(SecurityAccess.hasRights(userSecurityRights,
                                      SecurityAccess.VIEW_ANNOUNCEMENT)) {%>
      mAnnouncement = false;
      <% }
       %>

 function initParms(){
  require(["dojo/dom","dijit/registry","dojo/dom-style"],
		      function(dom,registry,domStyle){	
		     
	 var gridLayout = <%= gridLayout %>;
      var initSearchParms = "userOrgOid=<%=StringFunction.xssCharsToHtml(userOrgOid)%>";
      var savedSort = savedSearchQueryData["sort"];  
     	if("" == savedSort || null == savedSort || "undefined" == savedSort){
          savedSort ='-StartDate';
       }
       if(mAnnouncement){
    domStyle.set(dojo.byId(Announcements_Delete), 'display', 'block');
       }else{
    domStyle.set(dojo.byId(Announcements_Delete), 'display', 'none');
       }
        
       var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AnnouncementDataView",userSession.getSecretKey())%>"; <%--Rel9.2 IR T36000032596 --%> 
       var announceGrid = createDataGrid("announceGrid", viewName,
        gridLayout, initSearchParms, savedSort);
		      
		});      
		      
	}	      

 <%---  SureshL IR-T36000036654 04/17/2015 Start ---%> 

 function deleteAnnouncement(){

         var rowKeys =getSelectedGridRowKeys("announceGrid");

         if(rowKeys == ""){
           alert("Please pick record(s) and click 'Delete'");
           return;
         }

         var confirmDelete = confirm('<%=resMgr.getText("Announcements.ConfirmDeleteMessage",TradePortalConstants.TEXT_BUNDLE)%>');
         if(confirmDelete == true){
           var formName = 'AnnouncementDeleteForm';
           var buttonName = 'Delete';
           var rowKeys = getSelectedGridRowKeys("announceGrid");
          submitFormWithParms(formName, buttonName, "announcement_oid", rowKeys);
         }
      
     }

<%---  SureshL IR-T36000036654 04/17/2015 End ---%> 

  <%--provide a variable for page callbacks--%>
  var thisPage = {};

  require(["dijit/registry", "dojo/on", "t360/common", "t360/datagrid", "dojo/dom", "dojo/ready"],
    function(registry, on, common, t360grid, dom, ready) {
       initParms();
      ready(function() {
		    var newButton = registry.byId("newButton");
        if ( newButton ) {
          newButton.on("click", function() {
            window.location = "<%= newLink %>";
          });
        }
		});  
		});  
		  
        

<%--- SureshL  5/23/2014 Rel8.4  IR-T36000028608 -End  ---%>  
      

</script>

<%--cquinton 10/8/2012 include gridShowCounts --%>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="announceGrid" />
</jsp:include>

</body>
</html>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
