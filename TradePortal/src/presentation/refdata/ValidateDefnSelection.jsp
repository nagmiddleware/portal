<%--
*******************************************************************************
                           Validate Definition Selection

  Description:
     This page is an internal navigation page.  Based on the po upload
  definition selected by the user on the POUploadDefnSelection page, we validate
  that a selection was actually made.  If so, forward to the LC Creation Rule
  Detail page with the encrypted oid for the selection definition.  Otherwise
  issue an error and return to the selection page.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.frame.*,
                 com.amsinc.ecsg.util.*,com.ams.tradeportal.common.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<%
   DocumentHandler doc  = formMgr.getFromDocCache();
   String searchType    = "";
   String definition    = "";
   String nextPage      = null;
   String errorPage     = null;
   String parmName      = null;

   // Determine which PO Definition type to validate; different parameters will
   // be set depending on the selection type
   if (doc.getDocumentNode("/In/SearchForPO") != null)
   {
     searchType = doc.getAttribute("/In/SearchForPO/searchType");
   }

   if (searchType.equals(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL))
   {
     definition = request.getParameter("PODefinition");
     nextPage  = "PODetailsEntry";
     errorPage = "PODefinitionSelection";
     parmName  = "PODefinition";
   }
   else
   {
     definition = request.getParameter("UploadDefinition");
     nextPage  = "LCCreationRuleDetail";
     errorPage = "POUploadDefnSelection";
     parmName  = "UploadDefinition";
   }

   // If no selection was made, issue and error and return to the selection page
   if (definition.equals("")) {
      MediatorServices medService = new MediatorServices();
      medService.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      medService.getErrorManager().issueError(
                                    TradePortalConstants.ERR_CAT_1,
                                    TradePortalConstants.SELECT_PO_DEFNITION);
      medService.addErrorInfo();
      doc.setComponent("/Error", medService.getErrorDoc());
      formMgr.storeInDocCache("default.doc", doc);
      nextPage = errorPage;
   }

   formMgr.setCurrPage(nextPage);
   String physicalPage = NavigationManager.getNavMan().getPhysicalPage(nextPage, request);

%>
   <jsp:forward page='<%= physicalPage %>'>
     <jsp:param name="parmName" value="<%=definition%>" />
   </jsp:forward>
