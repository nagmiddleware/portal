<%--
**********************************************************************************
                              Edit All FX Rates

  Description:
     This page is used to maintain all foreign exchange rates.  It supports 
     insert, update, and delete.  Errors found during a save are redisplayed on 
     the same page. Successful updates return the user to the ref data home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   DocumentHandler   fxRatesDoc            = null;
   DocumentHandler   fxRateDoc             = null;
   DocumentHandler   xmlDoc                = null;
   QueryListView     queryListView         = null;
   StringBuffer      updateText            = null;
   StringBuffer      sqlQuery              = new StringBuffer();
   Hashtable         secureParms           = new Hashtable();
   boolean           showSaveButtonFlag    = true;
   boolean           showSaveAndCloseButtonFlag = true;
   boolean           isReadOnly            = false;
   boolean           errorFlag             = false;
   Vector            fxRateList            = null;
   String            userSecurityRights    = null;
   String            ownerOrgOid       = null;
   String            dropdownOptions       = null;
   String            classAttribute        = null;
   String            defaultValue          = null;
   String            userOrgOid            = null;
   String            userLocale            = null;
   String            buttonPressed = "";
   int               numberOfFxRates       = 0;

   // Retrieve the user's locale, corporate organization oid, and security rights
   userSecurityRights = userSession.getSecurityRights();
   userLocale         = userSession.getUserLocale();
   ownerOrgOid    = userSession.getOwnerOrgOid();

   isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_FX_RATE);

   // Determine whether the Delete and Save buttons should be displayed
   if (isReadOnly)
   {
      showSaveButtonFlag = false;
      showSaveAndCloseButtonFlag = false;
   }

   // Get the document from the cache.  We'll use it to repopulate the screen if the
   // user was entering data with errors.
   xmlDoc = formMgr.getFromDocCache();
   buttonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed");
	Vector error = null;
	error = xmlDoc.getFragments("/Error/errorlist/error");

   // If data exists in the xml doc, then we're returning to the page with errors; otherwise
   // we're coming from the listview
   if (xmlDoc.getDocumentNode("/In/FXRate(0)") != null)
   {
      errorFlag = true;
   }

   try
   {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 

      userOrgOid = userSession.getOwnerOrgOid();

      sqlQuery.append("select a.fx_rate_oid, a.currency_code as currency_code, ");
      sqlQuery.append("a.rate, a.multiply_indicator as calculation_method, ");
      sqlQuery.append("a.base_currency_code as base_currency, ");
      //CM IAZ 10/14/08 Begin
      sqlQuery.append("a.buy_rate, a.sell_rate, ");
      //CM IAZ 10/14/08 End  
      sqlQuery.append("a.last_updated_date as last_modified_date, a.opt_lock ");
      sqlQuery.append("from fx_rate a ");
      //sqlQuery.append("where a.p_corporate_org_oid = ");
      sqlQuery.append("where a.p_owner_org_oid = ?"); //AAlubala Rel7000 CR610 03/2011 - Modify to use p_owner_org_oid

      // Since the description for currencies is the code - description, sorting
      // by the code also sorts by the description
	sqlQuery.append(" order by ");
	  sqlQuery.append(resMgr.localizeOrderBy("base_currency"));
	  sqlQuery.append(" , ");
      sqlQuery.append(resMgr.localizeOrderBy("currency_code"));

      queryListView.setSQL(sqlQuery.toString(), new Object[]{userOrgOid});
      queryListView.getRecords();

      numberOfFxRates = queryListView.getRecordCount();

      fxRatesDoc = queryListView.getXmlResultSet();

      fxRateList = fxRatesDoc.getFragments("/ResultSetRecord");
   }
   catch (Exception e)
   {
      e.printStackTrace();
   }
   finally
   {
      try
      {
         if (queryListView != null)
         {
            queryListView.remove();
         }
      }
      catch (Exception e)
      {
         System.out.println("Error removing QueryListView in EditAllFXRates.jsp!");
      }
   }
%>

<%-- ********************* HTML for page begins here *********************  --%>



<%
  // onLoad is set to default the cursor to the UserId field but only if the 
  // page is not in readonly mode.
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad"         value="document.EditAllFxRatesForm.FxRateRate0.focus();" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="FXRatesDetail.ForeignExchangeRates" />
      <jsp:param name="helpUrl" value="customer/edit_all_fx_rates.htm" />
    </jsp:include>

    <%--cquinton 11/1/2012 ir#7015 remove return link--%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="titleKey" value="AllFXRates.AllFXRates" />
    </jsp:include>

<form name="EditAllFxRatesForm" id="EditAllFxRatesForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">

<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />
<div class="formContent">
  <input type=hidden value="" name=buttonName>
<%
   secureParms.put("UserOid",                 userSession.getUserOid());
   secureParms.put("numberOfMultipleObjects", String.valueOf(numberOfFxRates));
%>

<%= formMgr.getFormInstanceAsInputField("EditAllFxRatesForm", secureParms) %>

<%
   // Store several FX rate (if there is one) and user attributes in a secure hashtable for the form
   for (int i = 0; i < numberOfFxRates; i++)
   {
      fxRateDoc = (DocumentHandler) fxRateList.elementAt(i);

      // Retrieve the text to use in the error message and audit log
      updateText = new StringBuffer();

      updateText.append(resMgr.getText("error.FxRate", TradePortalConstants.TEXT_BUNDLE));
      updateText.append(": ");
      updateText.append(ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CURRENCY_CODE,
											     fxRateDoc.getAttribute("/CURRENCY_CODE"), 
                                                                       userSession.getUserLocale()));

      out.print("<input type=\"hidden\" name=\"FxRateOid" + i + "\" value=\"" + 
                EncryptDecrypt.encryptStringUsingTripleDes(fxRateDoc.getAttribute("/FX_RATE_OID"), userSession.getSecretKey()) + "\" />");
      out.print("<input type=\"hidden\" name=\"FxRateCurrencyDescr" + i + "\" value=\"" + updateText.toString() + "\" />");
      out.print("<input type=\"hidden\" name=\"FxRateLockId" + i + "\" value=\"" + fxRateDoc.getAttribute("/OPT_LOCK") + 
                "\" />");
      out.print("<input type=\"hidden\" name=\"FxRateOwnerOrgOid" + i + "\" value=\"" + 
                EncryptDecrypt.encryptStringUsingTripleDes(ownerOrgOid, userSession.getSecretKey()) + "\" />");
   }
%>

  
  
  <%
  //AAlubala - Rel 7.0.0.0 CR610 - bug fix - Only display the base currency info to corporate customers
  if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE)){
      %>   
        &nbsp;&nbsp;&nbsp;<%=widgetFactory.createSubLabel("AllFXRates.CurrencyText") %> 
        <br />       
          &nbsp;&nbsp;&nbsp; <b><%=userSession.getBaseCurrencyCode()%></b>        
        <br />
        <br />   
  <%}%>
  		  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
			<thead>
				<tr>
					<th width="5%"><%=resMgr.getText("AllFXRates.Currency", TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="20%"><span class="asterisk ">*</span>&nbsp;<%=resMgr.getText("AllFXRates.CalculationMethod", TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="10%"><%=resMgr.getText("AllFXRates.BaseCurrency", TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="15%"><%=resMgr.getText("AllFXRates.BuyRate", TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="15%"><span class="asterisk ">*</span>&nbsp;<%=resMgr.getText("AllFXRates.MidRate", TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="15%"><%=resMgr.getText("AllFXRates.SellRate", TradePortalConstants.TEXT_BUNDLE)%></th>
					<th width="15%"><%=resMgr.getText("AllFXRates.LastModified", TradePortalConstants.TEXT_BUNDLE)%></th>
				</tr>
			 </thead>
			 <tbody>
			 <%
		       for (int j = 0; j < numberOfFxRates; j++)
		       {
		          fxRateDoc = (DocumentHandler) fxRateList.elementAt(j);
		
		          if ((j % 2) == 0)
		          {
		             classAttribute = "";
		          }
		          else
		          {
		             classAttribute = "class=\"ColorGreySize\"";
		          }
		     %>
			     <tr>
			       <td align="left">              
		                <%= fxRateDoc.getAttribute("/CURRENCY_CODE")%>              
		            </td>
		            <td align="left">              
		              <%
		                 if (errorFlag)
		                 {
		                    defaultValue = xmlDoc.getAttribute("/In/FXRate(" + j + ")/multiply_indicator");
		                 }
		                 else
		                 {
		                    defaultValue = fxRateDoc.getAttribute("/CALCULATION_METHOD");
		                 }
						 
						 out.print(widgetFactory.createRadioButtonField("FxRateMultiplyIndicator" + j,"TradePortalConstants.DIVIDE"+ j,"AllFXRates.Divide",TradePortalConstants.DIVIDE,defaultValue.equals(TradePortalConstants.DIVIDE), isReadOnly));
						%>
						
						<%
						 out.print(widgetFactory.createRadioButtonField("FxRateMultiplyIndicator" + j,"TradePortalConstants.MULTIPLY"+ j,"AllFXRates.Multiply",TradePortalConstants.MULTIPLY,defaultValue.equals(TradePortalConstants.MULTIPLY), isReadOnly));
		                %>            
		            </td>
		            <td align="left">
		            	 <%
		                /*if (errorFlag){
		                    defaultValue = xmlDoc.getAttribute("/In/FXRate(" + j + ")/base_currency");
		                	}
		                else{
		                    defaultValue = fxRateDoc.getAttribute("/BASE_CURRENCY");
			                }*/
		            	 out.print(fxRateDoc.getAttribute("/BASE_CURRENCY"));
		                 %>&nbsp;
		            </td>
		            <%-- CM IAZ 10/14/08 Begin - add Sell and Buy rates --%>
		            <td align="left"> 
		              <%
		                 String displayRate;
		
		                 if (errorFlag)
		                 {
		                    defaultValue = xmlDoc.getAttribute("/In/FXRate(" + j + ")/buy_rate");
		                    displayRate = defaultValue;
		                 }
		                 else
		                 {
		                    defaultValue = fxRateDoc.getAttribute("/BUY_RATE");
		                    displayRate = 
		                         TPCurrencyUtility.getDisplayAmount(fxRateDoc.getAttribute("/BUY_RATE"), "", userLocale);
		                 }
		                
		                 out.print(widgetFactory.createTextField( "FxRateBuyRate" + j, "", displayRate, "14", isReadOnly, false, false, "class='char10'", "regExp:'((([0-9]{1,3}(,[0-9]{3})*|([0-9]{1,5}))(.[0-9]{1,8}))|([0-9]{1,5})|(.[0-9]{1,8})|([0-9]{1,5}\\.))'", "none" ));
		                 out.print(widgetFactory.createHoverHelp("FxRateBuyRate" + j,"BuyRateHoverText"));
		              %>
		            </td>            
		            <td align="left"> 
		              <%
		
		                 if (errorFlag)
		                 {
		                    defaultValue = xmlDoc.getAttribute("/In/FXRate(" + j + ")/rate");
		                    displayRate = defaultValue;
		                 }
		                 else
		                 {
		                    defaultValue = fxRateDoc.getAttribute("/RATE");
		                    displayRate = 
		                         TPCurrencyUtility.getDisplayAmount(fxRateDoc.getAttribute("/RATE"), "", userLocale);
		                 }
		               
		                 out.print(widgetFactory.createTextField( "FxRateRate" + j, "", displayRate, "14", isReadOnly, false, false, "class='char10'", "regExp:'((([0-9]{1,3}(,[0-9]{3})*|([0-9]{1,5}))(.[0-9]{1,8}))|([0-9]{1,5})|(.[0-9]{1,8})|([0-9]{1,5}\\.))'", "none" ));
		                 out.print(widgetFactory.createHoverHelp("FxRateRate" + j,"MidRateHoverText"));
		              %>
		            </td>
		            <td align="left"> 
		              <%
		
		                 if (errorFlag)
		                 {
		                    defaultValue = xmlDoc.getAttribute("/In/FXRate(" + j + ")/sell_rate");
		                    displayRate = defaultValue;
		                 }
		                 else
		                 {
		                    defaultValue = fxRateDoc.getAttribute("/SELL_RATE");
		                    displayRate = 
		                         TPCurrencyUtility.getDisplayAmount(fxRateDoc.getAttribute("/SELL_RATE"), "", userLocale);
		                 }
		
		                 out.print(widgetFactory.createTextField( "FxRateSellRate" + j, "", displayRate, "14", isReadOnly, false, false, "class='char10'", "regExp:'((([0-9]{1,3}(,[0-9]{3})*|([0-9]{1,5}))(.[0-9]{1,8}))|([0-9]{1,5})|(.[0-9]{1,8})|([0-9]{1,5}\\.))'", "none" ));
		                 out.print(widgetFactory.createHoverHelp("FxRateSellRate" + j,"SellRateHoverText"));
		              %>
		            </td>
		            <%-- CM IAZ 10/14/08 End --%>
		            <td align="left"> 
		              
		                <%
		
					out.print(TPDateTimeUtility.convertGMTDateForTimezoneAndFormat(fxRateDoc.getAttribute("/LAST_MODIFIED_DATE"),
												   TPDateTimeUtility.SHORT,
												   resMgr.getResourceLocale(),
												   userSession.getTimeZone()) );
		                   // NEED TO PUT THIS IN THE CORRECT FORMAT!
		                   //out.print(fxRateDoc.getAttribute("/LAST_MODIFIED_DATE"));
		                %>
		              
		            </td> 
			     </tr>
			     <%
		       }
		    	%>
			 </tbody>	 	             
		  </table>
  
  </div>
  </div><%--formArea--%>
        <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'EditAllFxRatesForm'">
        <jsp:include page="/common/RefDataSidebar.jsp">
           <jsp:param name="showSaveButton"   value="<%=showSaveButtonFlag%>" />
           <jsp:param name="saveOnClick" value="none" />
           <jsp:param name="showDeleteButton" value="false" />
           <jsp:param name="showSaveCloseButton" value="<%=showSaveAndCloseButtonFlag%>" />
           <jsp:param name="saveCloseOnClick" value="none" />
           <jsp:param name="showHelpButton"   value="true" />
	       <jsp:param name="helpLink"         value="customer/edit_all_fx_rates.htm" />
           <jsp:param name="cancelAction"     value="goToRefDataHome" />
           <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
           <jsp:param name="error" value="<%= error%>" /> 
        </jsp:include>
      </div>
      
</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%



	//(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp

	//String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
	//String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />
	 

<jsp:include page="/common/ButtonPrep.jsp" />

<script type="text/javascript">
  require(["dojo/ready","dojo/dom","dojo/domReady!" ], function(ready,dom) {
	  ready(function() { 
	  var i= 0, j = <%= numberOfFxRates %>;
	  for ( i = 0; i < j; i++)
	  {
		  dom.byId("FxRateBuyRate"+i).style.textAlign ="right";
		  dom.byId("FxRateRate"+i).style.textAlign ="right";
		  dom.byId("FxRateSellRate"+i).style.textAlign ="right";
      } 
	  });	
  });

 function preSaveAndClose(){
  	 	 
 	  document.forms[0].buttonName.value = "SaveAndClose";
 	  return true;

}



</script>

</body>
</html>


<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
