
<%--
*******************************************************************************
       			Notification Rule Detail Row
  
Description:    Creates html tags that display a row in the Notification Rule
		Detail Page

Parameters:
	
  	transactionTypes	
  	instrCategory
  	criterionRuleOid		
 	criterionRuleSetting		
	criterionRuleType
	criterionRuleCount
	isReadOnly		

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>

<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.html.*, com.ams.tradeportal.common.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String transactionTypes 	= StringFunction.xssCharsToHtml(request.getParameter("transactionTypes"));
  String instrCategory		= StringFunction.xssCharsToHtml(request.getParameter("instrCategory"));
  String criterionRuleOid	= request.getParameter("criterionRuleOid");
  String criterionRuleSetting	= request.getParameter("criterionRuleSetting");
  String criterionRuleType	= request.getParameter("criterionRuleType");
  int    criterionRuleCount	= Integer.valueOf(request.getParameter("criterionRuleCount")).intValue();
  boolean isReadOnly		= request.getParameter("isReadOnly").equals("true")? true:false;

  if (criterionRuleType.equals(TradePortalConstants.NOTIF_RULE_NOTIFICATION))
  {
%>    
<%--IR3009 26/07/2012 by dillip  --%> 
            <td align="center">             
              <%=widgetFactory.createRadioButtonField("setting" + criterionRuleCount,"TradePortalConstants.NOTIF_RULE_ALWAYS" + criterionRuleCount,"",TradePortalConstants.NOTIF_RULE_ALWAYS,criterionRuleSetting.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                         
            </td>
             
            <td align="center">           
              <%=widgetFactory.createRadioButtonField("setting" + criterionRuleCount,"TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY" + criterionRuleCount,"",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,criterionRuleSetting.equals(TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY),isReadOnly)%>                          
            </td>
            
            <td align="center">             
              <%=widgetFactory.createRadioButtonField("setting" + criterionRuleCount,"TradePortalConstants.NOTIF_RULE_NONE" + criterionRuleCount,"",TradePortalConstants.NOTIF_RULE_NONE,criterionRuleSetting.equals(TradePortalConstants.NOTIF_RULE_NONE), isReadOnly)%>                           
            </td>                     
      	  <% out.print("<input type=hidden name='criterionOid" + criterionRuleCount + "' value='" + EncryptDecrypt.encryptStringUsingTripleDes(criterionRuleOid, userSession.getSecretKey()) + "'>"); %>  
       	  <% out.print("<input type=hidden name='transactionType" + criterionRuleCount + "' value='" + transactionTypes + "'>"); %>
       	  <% out.print("<input type=hidden name='instrNotifyCategory" + criterionRuleCount + "' value='" + instrCategory + "'>"); %>
       	  <% out.print("<input type=hidden name='criterionType" + criterionRuleCount + "' value='" + TradePortalConstants.NOTIF_RULE_NOTIFICATION + "'>"); %>
       	  <%--Ended Here  --%>        	  
<% 
  }
  else
  {
%> 
            <td align="center">             
              <%=widgetFactory.createRadioButtonField("setting" + criterionRuleCount,"TradePortalConstants.NOTIF_RULE_ALWAYS" + criterionRuleCount,"",TradePortalConstants.NOTIF_RULE_ALWAYS,criterionRuleSetting.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                         
            </td>
             
            <td align="center">           
              <%=widgetFactory.createRadioButtonField("setting" + criterionRuleCount,"TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY" + criterionRuleCount,"",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,criterionRuleSetting.equals(TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY), isReadOnly)%>                          
            </td>
            
            <td align="center">             
              <%=widgetFactory.createRadioButtonField("setting" + criterionRuleCount,"TradePortalConstants.NOTIF_RULE_NONE" + criterionRuleCount,"",TradePortalConstants.NOTIF_RULE_NONE,criterionRuleSetting.equals(TradePortalConstants.NOTIF_RULE_NONE), isReadOnly)%>                           
            </td>             
          </tr>
       	  <% out.print("<input type=hidden name='criterionOid" + criterionRuleCount + "' value='" + EncryptDecrypt.encryptStringUsingTripleDes(criterionRuleOid, userSession.getSecretKey()) + "'>"); %>  
       	  <% out.print("<input type=hidden name='transactionType" + criterionRuleCount + "' value='" + transactionTypes + "'>"); %>
       	  <% out.print("<input type=hidden name='instrNotifyCategory" + criterionRuleCount + "' value='" + instrCategory + "'>"); %>
       	  <% out.print("<input type=hidden name='criterionType" + criterionRuleCount + "' value='" + TradePortalConstants.NOTIF_RULE_EMAIL + "'>"); %>
<% 
  }
%>
