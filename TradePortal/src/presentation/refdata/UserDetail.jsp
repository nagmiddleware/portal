<%--
*******************************************************************************
                                    User Detail

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, com.ams.tradeportal.common.cache.*, javax.crypto.SecretKey" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%
  boolean isAdminUser = false;
  // Check if this TradePortal instance is configured for HSM encryption / decryption
  boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
  //for debugging hsm in dev comment out above and comment in below
  //boolean instanceHSMEnabled = true;
%>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  CorporateOrganizationBean corpOrgBean = new CorporateOrganizationBean(); //CR 821
  String panelAliasesOptions = ""; //CR 821
  Hashtable panelAliasesMap = new Hashtable(); //CR 821
  boolean isReadOnly = false;
  boolean isViewInReadOnly = false;
  boolean isWorkGroupReadOnly = false;

  String options;
  String defaultText;
  String link;
  String links = "";
  String certAuthURL = "";
  String oid = "";
//Suresh CR-603 03/11/2011 Begin
  UserWebBean					  user							  = null;
  StringBuffer                    sqlQuery                        = null;
  String						  userDetailReptCateg1Oid		  = null;
  String						  userDetailReptCateg2Oid		  = null;
  String						  userDetailReptCateg3Oid		  = null;
  String						  userDetailReptCateg4Oid		  = null;
  String						  userDetailReptCateg5Oid		  = null;
  String						  userDetailReptCateg6Oid		  = null;
  String						  userDetailReptCateg7Oid		  = null;
  String						  userDetailReptCateg8Oid		  = null;
  String						  userDetailReptCateg9Oid		  = null;
  String						  userDetailReptCateg10Oid	      = null;
  String 						  pSql							  = null;
  String                          dropdownOptions                 = null;
  String 						  allowPayByAnotherAccnt          = "";
  int numTotalAccounts = 0;
  int totAccounts = 0;
  int DEFAULT_ACCOUNT_COUNT = 4;
  String buttonPressed = "";
  Vector error = null;
  UserAuthorizedAccountWebBean account[]=null;
  String onLoad = "";

    int numTotalTemplateGroups = 0;
    int totTemplateGroups = 0;
    final int DEFAULT_ROWS = 4;
    int DEFAULT_TEMPLATE_COUNT = 4;
    UserAuthorizedTemplateGroupWebBean templateGroup[]=null;

  // These booleans help determine the mode of the JSP.
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;
//jgadela rel8.3 CR501 [START]
  boolean showApprove = false;
  boolean showReject = false;
//jgadela rel8.3 CR501 [END]

  boolean showSave = true;
  boolean showDelete = true;

  boolean showSaveCloseButton=false;
  boolean showDeleteButton=false;
  boolean showSaveButton=false;

  String password1 = "";
  String password2 = "";
//MEer Rel 8.3 T36000021992 
  boolean ignoreDefaultPanelValue = false;

  DocumentHandler doc;

  // Must initialize these 2 doc, otherwise get a compile error.
  DocumentHandler thresholdGroupDoc = new DocumentHandler();
  DocumentHandler secProfileDoc = new DocumentHandler();
  DocumentHandler workGroupDoc = new DocumentHandler();
  DocumentHandler accountDoc = new DocumentHandler();
  DocumentHandler templateGroupDoc = new DocumentHandler();

  String multipleTransactionPartsInd;
  String displayExtendedListviewInd;

  boolean defaultRadioButtonValue1 = true;
  boolean defaultRadioButtonValue2 = false;

  if(request.getParameter("numberOfMultipleObjects")!=null)
     numTotalAccounts = Integer.parseInt(request.getParameter("numberOfMultipleObjects"));
  else
     numTotalAccounts =DEFAULT_ROWS;

  if(request.getParameter("numberOfMultipleObjects1")!=null)
     numTotalTemplateGroups = Integer.parseInt(request.getParameter("numberOfMultipleObjects1"));
  else
     numTotalTemplateGroups =DEFAULT_ROWS;

  user = beanMgr.createBean(UserWebBean.class, "User");

  CorporateOrganizationWebBean corporateOrg = null;

//jgadela Rel 8.4 CR-854 [BEGIN]- Adding reporting language option
  DocumentHandler				  reportCategsDocEn				  = null;
  DocumentHandler				  reportCategsDocFr				  = null;
//jgadela R90 IR T36000026319 - SQL INJECTION FIX
  Object sqlParams[] = new Object[10];
  sqlParams[0] = userSession.getOwnerOrgOid();
  sqlParams[1] = userSession.getOwnerOrgOid();
  sqlParams[2] = userSession.getOwnerOrgOid();
  sqlParams[3] = userSession.getOwnerOrgOid();
  sqlParams[4] = userSession.getOwnerOrgOid();
  sqlParams[5] = userSession.getOwnerOrgOid();
  sqlParams[6] = userSession.getOwnerOrgOid();
  sqlParams[7] = userSession.getOwnerOrgOid();
  sqlParams[8] = userSession.getOwnerOrgOid();
  sqlParams[9] = userSession.getOwnerOrgOid();
  pSql = "select CODE, DESCR from refdata where table_type = 'REPORTING_CATEGORY' and locale_name is NULL and CODE IN ( " +
		  "select first_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select second_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select third_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select fourth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select fifth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select sixth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select seventh_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select eighth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select nineth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select tenth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  ")";
  
  reportCategsDocEn = DatabaseQueryBean.getXmlResultSet(pSql, false, sqlParams);
  pSql = "select CODE, DESCR from refdata where table_type = 'REPORTING_CATEGORY' and locale_name = 'fr_CA' and CODE IN ( " +
		  "select first_rept_categ from CORPORATE_ORG where organization_oid=? " +
		  "UNION ALL select second_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select third_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select fourth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select fifth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select sixth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select seventh_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select eighth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select nineth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select tenth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  ")";
  reportCategsDocFr = DatabaseQueryBean.getXmlResultSet(pSql, false, sqlParams);
  //Suresh CR-603 03/14/2011 End
  Vector repVectorEn = reportCategsDocEn!=null ? reportCategsDocEn.getFragments("/ResultSetRecord"): null;
  Vector repVectorFr = reportCategsDocFr!=null ? reportCategsDocFr.getFragments("/ResultSetRecord"): null;
  String repCatEn = getReportCatData(repVectorEn, userSession.getSecretKey());
  String repCatFr = getReportCatData(repVectorFr, userSession.getSecretKey());
//jgadela Rel 8.4 CR-854 - [END]Adding reporting language option

  String loginLocale;
  String loginRights;
  String viewInReadOnly;
  String cbMobileBankingInd = "";
  String cbReportingLangOptionInd = null;// jgadela Rel 8.4 CR-854  T36000024797

  viewInReadOnly = request.getParameter("fromHomePage");

  //If we got to this page from the Home Page, then we need to explicitly be in Read only mode.
  //Also, the close button action needs to take us back to the home page.  This flag will help
  //us keep track of where to go.

  if( (!InstrumentServices.isBlank(viewInReadOnly)) &&
      (viewInReadOnly.equals("true")) )
  {
	isViewInReadOnly = true;
  }

  String         userSecurityType        = null;

  // Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
  if (userSession.getSavedUserSession() == null) {
  		userSecurityType = userSession.getSecurityType();
   }else {
	   userSecurityType = userSession.getSavedUserSessionSecurityType();
   }
  Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
  // W Zhu 9/22/2015 Rel 9.4 T36000044046 check CAN_EDIT_ID_IND only if the current login user is coporate user.
  String canViewIDFields = TradePortalConstants.INDICATOR_YES;
  if (userSession.getSecurityType().equals(TradePortalConstants.NON_ADMIN)
          && (!userSession.hasSavedUserSession() || userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.NON_ADMIN))) {
     canViewIDFields = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_ID_IND");
  }
  String canViewPaymentTemplateGroups; //IR-VUK090959969

  if (InstrumentServices.isBlank(userSession.getClientBankOid())){
  	    //canViewIDFields = "";
  	    canViewPaymentTemplateGroups = ""; //IR-VUK090959969
  }else{
  	   canViewPaymentTemplateGroups = CBCResult.getAttribute("/ResultSetRecord(0)/TEMPLATE_GROUPS_IND"); //IR-VUK090959969
  	   allowPayByAnotherAccnt = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT"); // RKazi IR HRUL062032745
  	   cbMobileBankingInd = CBCResult.getAttribute("/ResultSetRecord(0)/MOBILE_BANKING_ACCESS_IND"); // MEerupula Rel 8.4  CR-934A
  	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // jgadela Rel 8.4 CR-854 T36000024797 
  }
  if (InstrumentServices.isBlank(allowPayByAnotherAccnt)){
  		String clientBankOid = userSession.getClientBankOid();
  		QueryListView queryListView   = null;
  		sqlQuery = new StringBuffer();

  		sqlQuery.append("select allow_pay_by_another_accnt");
  		sqlQuery.append(" from client_bank");
  		sqlQuery.append(" where organization_oid = ?");

  		queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
  		//out.println("QUERY: "+sqlQuery.toString());
  		queryListView.setSQL(sqlQuery.toString(),new Object[]{clientBankOid});
  		queryListView.getRecords();

  		DocumentHandler result = queryListView.getXmlResultSet();
  		allowPayByAnotherAccnt = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
  	}
	// RKazi IR HRUL062032745 Start
	
	
%>

<%
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

     corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
     corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
     corporateOrg.getDataFromAppServer();
     //MEerupula CR 934A
     String corpCustMobileBankingInd = corporateOrg.getAttribute("mobile_banking_access_ind");
    

  if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_USERS)) {
     isReadOnly = false;
     // If the user's corporate organization requires certificate authentication for users
     // that can maintain user data and the user's authentication method is password,
     // display the page in read-only mode and display the information message.

      // NAR-SAUJ060247671 24-SEP-2010
      //if user is admin, force certificate condition should not be check for admin user.if admin has user maintinance access reghts,
      // he will be able to edit customer user data and can create user without checking whether he has 2FA and Certificate rights.
     if(!userSecurityType.equals(TradePortalConstants.ADMIN)){
     if (corporateOrg.getAttribute("force_certs_for_user_maint").equals(TradePortalConstants.INDICATOR_YES)) { // Corporate Org requires cert for users that maintain users.
        UserWebBean thisUser = beanMgr.createBean(UserWebBean.class,  "User");
        thisUser.setAttribute("user_oid", userSession.getUserOid());
        thisUser.getDataFromAppServer();
     // T36000013154 Rel 8.2 07/11/2013 - Start
        String authMethod = StringFunction.isNotBlank(thisUser.getAttribute("authentication_method")) ?
       		 thisUser.getAttribute("authentication_method") : corporateOrg.getAttribute("authentication_method");
             

        if (!(TradePortalConstants.AUTH_CERTIFICATE.equals(authMethod)
          || TradePortalConstants.AUTH_2FA.equals(authMethod))) { // This user is not using certificate
        	// T36000013154 Rel 8.2 07/11/2013 - End
           // Use a MediatorService to add these errors to the default.doc XML so that
           // it can be accessed by the error page
           MediatorServices mediatorServices = new MediatorServices();
           mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
           mediatorServices.getErrorManager().issueError("UserDetail.jsp",
                                   TradePortalConstants.SECURITY_NEED_CERT_MAINT_USER);
           mediatorServices.addErrorInfo();
           DocumentHandler defaultDoc = new DocumentHandler();
           defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
	       defaultDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
           formMgr.storeInDocCache("default.doc", defaultDoc);

		   isReadOnly = true;
        }
       }
     }
  } else if (SecurityAccess.hasRights(loginRights,
                                         SecurityAccess.VIEW_USERS) || isViewInReadOnly) {
     isReadOnly = true;
  } else {
     // Don't have maintain or view access, send the user back.
        formMgr.setCurrPage("RefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  }

  // We should only be in this page for corporate users.  If the logged in
  // user's level is not corporate, send back to ref data page.  This
  // condition should never occur but is caught just in case.
  String ownerLevel = userSession.getOwnershipLevel();

  if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
        formMgr.setCurrPage("RefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  }

  if( isViewInReadOnly)
  {
 	isReadOnly = true;
  }

  Debug.debug("login_rights is " + loginRights);

  // Get the document from the cache.  We'll use it to repopulate the screen
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();
  Debug.debug("doc from cache is " + doc.toString());
//jgadela rel8.3 CR501 [START]
  String pendingReferenceDataOid = "";
  String pendingChangedObjectOid		 = null;
//jgadela 10/10/2013 - Rel 8.3 IR T36000021772  - Corrected the CORP_USER_DUAL_CTRL_REQD_IND
  String adminUserDualCtrlReqdInd = userSecurityType.equals(TradePortalConstants.ADMIN) ?
          CBCResult.getAttribute("/ResultSetRecord(0)/CORP_USER_DUAL_CTRL_REQD_IND"): corporateOrg.getAttribute("corp_user_dual_ctrl_reqd_ind");

 //jgadela rel8.3 CR501 [END]

  if (doc.getDocumentNode("/In/Update/ButtonPressed") != null)
  {
      buttonPressed  = doc.getAttribute("/In/Update/ButtonPressed");
      error = doc.getFragments("/Error/errorlist/error");
  }

  if (doc.getDocumentNode("/In/User") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item or creating
     // a new one.)
     if(request.getParameter("oid") != null)
      {
        oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
        pendingChangedObjectOid = oid;
      }
     else
      {
	  oid = null;
      }

     if (oid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       oid = "0";
     } else if (oid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
     }

  } else {

     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.
     oid = doc.getAttribute("/Out/User/user_oid");
     if(oid == null)
		 oid = doc.getAttribute("/In/User/user_oid");

     pendingChangedObjectOid = oid;

	 retrieveFromDB = false;
     getDataFromDoc = true;
     String maxError = doc.getAttribute("/Error/maxerrorseverity");
     //jgadela rel8.3 CR501 [START]
     pendingReferenceDataOid = doc.getAttribute("/Out/PendingReferenceData/oid");
     if(pendingReferenceDataOid == null || "".equals(pendingReferenceDataOid) || "null".equals(pendingReferenceDataOid)){
     	  pendingReferenceDataOid = doc.getAttribute("/In/PendingReferenceData/oid");
       }
     //jgadela rel8.3 CR501 [END]

     if ((maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0 )) {
        // We've returned from a save/update/delete that was successful
        // We shouldn't be here.  Forward to the RefDataHome page.
    	 insertMode = false;
        //T36000031504
    	 if(pendingReferenceDataOid == null || "".equals(pendingReferenceDataOid) || "null".equals(pendingReferenceDataOid)){
    		 getDataFromDoc = false;
    	     retrieveFromDB = true;
          }
    	 
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.
        String newOid = doc.getAttribute("/In/User/user_oid");

        if (newOid.equals("0")) {
           insertMode = true;
           oid = "0";
        } else {
           // Not in insert mode, use oid from input doc.
           insertMode = false;
           oid = doc.getAttribute("/In/User/user_oid");
           pendingChangedObjectOid = oid;
        }
     }
  }
  String onlineHelpFileName="";
  onlineHelpFileName = "customer/customer/user_detail.htm";
  // Make sure that users cannot delete themselves
  String thisUserOid;
  thisUserOid = userSession.getUserOid();
  //if users of this corp (as driven by Client Bank) cannot modify thier own profiles,
  //make sure the mode isReadOnly if user access his/her own profile
  //if ( thisUserOid.equals(oid) ) showDelete = false;
  try {
  if ( thisUserOid.equals(oid) )
  {
  	showDelete = false;
  	if (!isReadOnly)
  	{
  	   	if (InstrumentServices.isNotBlank(userSession.getClientBankOid()))
   		{
   			String canModifyProfile = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_OWN_PROFILE_IND");
  			if (TradePortalConstants.INDICATOR_NO.equals(canModifyProfile)){
  				isReadOnly = true;
  			}

  	 	}
  	}
  }
  }
  catch (Exception any_e)
  {
		System.out.println("General exception " + any_e.toString() + "verifying if user can edit own profile.  Will use default mode.");
  }
  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Set to
     // insert mode and display error.
     user.setAttribute("user_oid", oid);
    // user.getDataFromAppServer();

   //jgadela rel8.3 CR501 [START]
     if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction"))){
    	 showApprove = true;
    	 showReject = true;
     }
     String ownerShipLevel = userSession.getOwnershipLevel();
     if(TradePortalConstants.OWNER_GLOBAL.equals(ownerShipLevel) || TradePortalConstants.OWNER_BOG.equals(ownerShipLevel)){
    	 ownerShipLevel = TradePortalConstants.OWNER_BANK;
     }
     ReferenceDataPendingWebBean pendingObj = beanMgr.createBean(ReferenceDataPendingWebBean.class,"ReferenceDataPending");
	 user = (UserWebBean) pendingObj.getReferenceData(user, userSession.getUserOid(),showApprove);

	//If changed by same user then do not show approve/reject buttons
	 if (userSession.getUserOid().equals(pendingObj.getAttribute("change_user_oid")) || isReadOnly ||
			 !ownerShipLevel.equals(pendingObj.getAttribute("ownership_level")) || !userSecurityType.equals(pendingObj.getAttribute("changed_user_security_type"))){
		 showApprove = false;
    	 showReject = false;
	 }

	 pendingReferenceDataOid = pendingObj.getAttribute("pending_data_oid");
	//IR 21408 - if user has maintain access then show error alert
	 if (pendingObj.isReadOnly() && !isReadOnly) {
	     isReadOnly = true;
	     if(showApprove == false){
		     DocumentHandler errorDoc = new DocumentHandler();
		     String errorMessage = resMgr.getText(TradePortalConstants.PENDING_USER_ALREADY_EXISTS2,"ErrorMessages");
		     String textM = resMgr.getText("PendingRefdata.User","TextResources");
		     errorMessage = errorMessage.replace("{0}",textM);
		     errorDoc.setAttribute("/severity", "5");
		     errorDoc.setAttribute("/message", errorMessage);
		     doc.addComponent("/Error/errorlist/error(0)", errorDoc);
	     }
	 }

	 if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction"))) {
		 getDataFromDoc = true;
	 	 String xmlData = StringFunction.xssHtmlToChars(pendingObj.getAttribute("changed_object_data"));
	 	 doc =  new DocumentHandler(xmlData,false);
     }else{
    	 getDataFromDoc = false;
     }

	//jgadela rel8.3 CR501 [END]

     if (user.getAttribute("user_oid").equals("")) {
       insertMode = true;
       oid = "0";
       getDataFromDoc = false;
     } else {
    	//jgadela rel8.3 CR501 -- Added if condition if the case of approving newly added pending record
    	 String change_type = pendingObj.getAttribute("change_type");
    	 if("C".equalsIgnoreCase(change_type)){
    	 	insertMode = true;
    	 	oid = "0";
    	 }else{
     		  insertMode = false;
    	 }
     }
  }


  if (getDataFromDoc) {
	//jgadela rel8.3 CR501 [START]  - IR T36000018219
	  com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean pendingBean = null;
	  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean", "ReferenceDataPending");

	  pendingBean = (ReferenceDataPendingWebBean) beanMgr.getBean("ReferenceDataPending");
	  pendingBean.getById(pendingReferenceDataOid);

	  String change_type = pendingBean.getAttribute("change_type");
	  if("C".equalsIgnoreCase(change_type)){
		  insertMode = true;
		  oid = "0";
	  }
	   //jgadela rel8.3 CR501 [END]
     // Populate the user bean with the input doc.
     try {
    	//jgadela rel8.3 CR501 [START] - Modified the statement and added object check condition
        user.populateFromXmlDoc((doc.getComponent("/In") != null ) ? doc.getComponent("/In"):doc);
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get User attributes "
             + " for oid: " + oid + " " + e.toString());
     }


       // Populate account[] bean with account info from document handler
       //jgadela rel8.3 CR501 [START] - Modified the statement and added object check condition
       DocumentHandler doc2 =  (doc.getComponent("/In") != null ) ? doc.getComponent("/In"):doc;
       Vector vVector = doc2.getFragments("/User/UserAuthorizedAccountList");
	   totAccounts = vVector.size();
	   String  sValue;
	   Debug.debug("UserAuthorizedAccountList Size = "+numTotalAccounts);
	   numTotalAccounts=(totAccounts > DEFAULT_ROWS)?totAccounts:DEFAULT_ROWS;

	   account = new UserAuthorizedAccountWebBean[numTotalAccounts];

	   for (int iLoop=0; iLoop<totAccounts; iLoop++)
	   {
 	     account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
	     DocumentHandler acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		 try { sValue = acctDoc.getAttribute("/authorized_account_oid"); }
		 catch (Exception e) { sValue = ""; }
		 account[iLoop].setAttribute("authorized_account_oid", sValue);

		 acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		 try { sValue = acctDoc.getAttribute("/account_oid"); }
		 catch (Exception e) { sValue = ""; }
		 account[iLoop].setAttribute("account_oid", sValue);

		 acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		 try { sValue = acctDoc.getAttribute("/account_description"); }
		 catch (Exception e) { sValue = ""; }
		 account[iLoop].setAttribute("account_description", sValue);
	   }

	   DocumentHandler templategroupDoc = doc.getComponent("/In");
	   Vector tgVector = doc2.getFragments("/User/UserAuthorizedTemplateGroupList");
	   totTemplateGroups = tgVector.size();
	   String  tgValue;
	   Debug.debug("UserAuthorizedTemplateGroupList Size = " + numTotalTemplateGroups);
	   numTotalTemplateGroups=(totTemplateGroups > DEFAULT_ROWS)?totTemplateGroups:DEFAULT_ROWS;

		  templateGroup = new UserAuthorizedTemplateGroupWebBean[numTotalTemplateGroups];
	   for (int iLoop=0; iLoop<totTemplateGroups; iLoop++)
	   {
 	     templateGroup[iLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
	     DocumentHandler templateGrpDoc = (DocumentHandler) tgVector.elementAt(iLoop);
		 try { tgValue = templateGrpDoc.getAttribute("/authorized_template_group_oid"); }
		 catch (Exception e) { tgValue = ""; }
		 templateGroup[iLoop].setAttribute("authorized_template_group_oid", tgValue);

		 templateGrpDoc = (DocumentHandler) tgVector.elementAt(iLoop);
		 try { tgValue = templateGrpDoc.getAttribute("/payment_template_group_oid"); }
		 catch (Exception e) { tgValue = ""; }
		 templateGroup[iLoop].setAttribute("payment_template_group_oid", tgValue);


	   }
  }else {
	   // Populate account[] bean with account info from document handler
	   QueryListView queryListView=null;
	   try
		{
		 String  sValue;
		 Vector vVector =null;
		 if (retrieveFromDB) {
		  StringBuilder sql = new StringBuilder();
		  List<Object> sqlParamsUser = new ArrayList();
		  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		  sql.append("select authorized_account_oid, account_description, a_account_oid,  othercorp_customer_indicator from USER_AUTHORIZED_ACCT, ACCOUNT ");
		  sql.append(" where p_user_oid = ?" );
		  sql.append(" and a_account_oid = account_oid and deactivate_indicator != ?" );
		  sqlParamsUser.add(user.getAttribute("user_oid"));
		  sqlParamsUser.add(TradePortalConstants.INDICATOR_YES);
		  if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccnt)){
    		sql.append(" and othercorp_customer_indicator != ?");
	    	 sqlParamsUser.add(TradePortalConstants.INDICATOR_YES);
		  }
		  sql.append(" order by ");
		  sql.append(resMgr.localizeOrderBy("authorized_account_oid"));
		  Debug.debug(sql.toString());

		  queryListView.setSQL(sql.toString(),sqlParamsUser);
		  queryListView.getRecords();

		  DocumentHandler acctList = new DocumentHandler();
		  acctList = queryListView.getXmlResultSet();
		  vVector = acctList.getFragments("/ResultSetRecord");
		  totAccounts = vVector.size();
		 }
		 else {
			 totAccounts=0;
		 }
		 numTotalAccounts=(totAccounts > DEFAULT_ROWS)?totAccounts:DEFAULT_ROWS;
	      Debug.debug("UserAuthorizedAccountList Size = "+numTotalAccounts);
	    //if add button is pressed increment Accounts by 4
/*	      if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_4_MORE_ACCOUNTS)) {
	    	  numTotalAccounts += 4;
	          onLoad += "location.hash='#BottomOfAccounts" + "';";
	      }		 */
	      account = new UserAuthorizedAccountWebBean[numTotalAccounts];

		  for (int iLoop=0;iLoop<totAccounts;iLoop++)
	      {
		    DocumentHandler acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		    account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
		    try { sValue = acctDoc.getAttribute("/AUTHORIZED_ACCOUNT_OID"); }
		    catch (Exception e) { sValue = ""; }
		    account[iLoop].setAttribute("authorized_account_oid", sValue);

		    try { sValue = acctDoc.getAttribute("/A_ACCOUNT_OID"); }
		    catch (Exception e) { sValue = ""; }
		    account[iLoop].setAttribute("account_oid", sValue);

		    try { sValue = acctDoc.getAttribute("/ACCOUNT_DESCRIPTION"); }
		    catch (Exception e) { sValue = ""; }
		    account[iLoop].setAttribute("account_description", sValue);
		  }

		  if(totAccounts < DEFAULT_ACCOUNT_COUNT) {
			  for(int iLoop = totAccounts; iLoop < DEFAULT_ACCOUNT_COUNT; iLoop++) {
			  	account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
			  }
		  }

		  String  tgValue;
		  Vector tgVector =null;
		  if (retrieveFromDB) {
		  	  StringBuffer sql = new StringBuffer();
		  	  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		  	  sql.append("select authorized_template_group_oid, a_template_group_oid from USER_AUTHORIZED_TEMPLATE_GROUP, PAYMENT_TEMPLATE_GROUP ");
		  	  sql.append(" where p_user_oid = ?" );
		  	  sql.append(" and a_template_group_oid = payment_template_group_oid " );
		  	  sql.append(" order by ");
		  	  sql.append(resMgr.localizeOrderBy("authorized_template_group_oid"));
		  	  Debug.debug(sql.toString());
		  	  queryListView.setSQL(sql.toString(),new Object[]{user.getAttribute("user_oid")});
		  	  queryListView.getRecords();

		  	  DocumentHandler templateGrpList = new DocumentHandler();
		  	  templateGrpList = queryListView.getXmlResultSet();
		  	  tgVector = templateGrpList.getFragments("/ResultSetRecord");
		  	  totTemplateGroups = tgVector.size();
		  }
		  else {
		          totTemplateGroups=0;
		  }

		  numTotalTemplateGroups=(totTemplateGroups > DEFAULT_ROWS)?totTemplateGroups:DEFAULT_ROWS;
		  Debug.debug("UserAuthorizedTemplateGroupList Size = " + numTotalTemplateGroups);
		  templateGroup = new UserAuthorizedTemplateGroupWebBean[numTotalTemplateGroups];
		  //Rel 9.2 IR 36234 start
		  if( ((numTotalTemplateGroups%2) != 0) ){
				numTotalTemplateGroups = numTotalTemplateGroups + 1;
				totTemplateGroups = numTotalTemplateGroups;
			}
		  //Rel 9.2 IR 36234 end
		  for (int iLoop=0;iLoop<totTemplateGroups;iLoop++)
		  {
		      DocumentHandler templateGrpDoc = (DocumentHandler) tgVector.elementAt(iLoop);
		      templateGroup[iLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
		      try { tgValue = templateGrpDoc.getAttribute("/AUTHORIZED_TEMPLATE_GROUP_OID"); }
		      catch (Exception e) { tgValue = ""; }
		      templateGroup[iLoop].setAttribute("authorized_template_group_oid", tgValue);

		      try { tgValue = templateGrpDoc.getAttribute("/A_TEMPLATE_GROUP_OID"); }
		      catch (Exception e) { tgValue = ""; }
		      templateGroup[iLoop].setAttribute("payment_template_group_oid", tgValue);


		  }

		  if(totTemplateGroups < DEFAULT_TEMPLATE_COUNT) {
		    for(int iLoop = totTemplateGroups; iLoop < DEFAULT_TEMPLATE_COUNT; iLoop++) {
		    	templateGroup[iLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
		    }
		  }
		}
		catch (Exception e) { e.printStackTrace(); }
		finally {
		try {
		  if (queryListView != null) {
		    queryListView.remove();
		  }
		} catch (Exception e) { System.out.println("Error removing queryListView in UserDetail.jsp"); }
		}



  }

  //UserAuthorizedAccountWebBean account[] = new UserAuthorizedAccountWebBean[numTotalAccounts];
  for (int iLoop=totAccounts; iLoop<numTotalAccounts; iLoop++)
  {
    account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
  }

  for (int jLoop=totTemplateGroups; jLoop<numTotalTemplateGroups; jLoop++)
  {
     templateGroup[jLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
  }
    // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " insertMode: " + insertMode + " oid: "
              + oid);

  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly) showSave = false;
// IR T36000049532 Rel9.5.0.3
  if (!insertMode || isReadOnly)
  {
	  userDetailReptCateg1Oid		  = user.getAttribute("first_rept_categ");
	  userDetailReptCateg2Oid		  = user.getAttribute("second_rept_categ");
	  userDetailReptCateg3Oid		  = user.getAttribute("third_rept_categ");
	  userDetailReptCateg4Oid		  = user.getAttribute("fourth_rept_categ");
	  userDetailReptCateg5Oid		  = user.getAttribute("fifth_rept_categ");
	  userDetailReptCateg6Oid		  = user.getAttribute("sixth_rept_categ");
	  userDetailReptCateg7Oid		  = user.getAttribute("seventh_rept_categ");
	  userDetailReptCateg8Oid		  = user.getAttribute("eighth_rept_categ");
	  userDetailReptCateg9Oid		  = user.getAttribute("nineth_rept_categ");
	  userDetailReptCateg10Oid	      = user.getAttribute("tenth_rept_categ");
  }
  isWorkGroupReadOnly = (isReadOnly || userSession.isUsingSubsidiaryAccess()); //WorkGroup not available for subsidiary access
%>

<%
  // Retrieve the data used to populate some of the dropdowns.

  StringBuilder sql = new StringBuilder();

  QueryListView queryListView = null;

  try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView");

      // Get all the thrshold groups that the user may be assigned to
      sql.append("select threshold_group_oid, threshold_group_name ");
      sql.append(" from threshold_group");
      sql.append(" where p_corp_org_oid = ?" );
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("threshold_group_name"));
      Debug.debug(sql.toString());

      queryListView.setSQL(sql.toString(),new Object[]{userSession.getOwnerOrgOid()});

      queryListView.getRecords();

      thresholdGroupDoc = queryListView.getXmlResultSet();
      Debug.debug(thresholdGroupDoc.toString());

      // Get all the security profiles that the user may be assigned to
      sql = new StringBuilder();
      List<Object> sqlParamsSp = new ArrayList();
      sql.append("select security_profile_oid, name ");
      sql.append(" from security_profile");
      sql.append(" where security_profile_type = 'NON_ADMIN'");
      sql.append(" and (p_owner_org_oid = ?");
      sqlParamsSp.add(userSession.getOwnerOrgOid());

      // Also include templates from the user's actual organization if using subsidiary access
      if(userSession.showOrgDataUnderSubAccess())
       {
          sql.append(" or p_owner_org_oid = ?");
          sqlParamsSp.add(userSession.getSavedUserSession().getOwnerOrgOid());
       }

      sql.append(" or p_owner_org_oid = ?");
      sql.append(" or p_owner_org_oid = ?");
      sql.append(" or p_owner_org_oid = ?" );
      sql.append(" )");
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
	  Debug.debug(sql.toString());
	  sqlParamsSp.add(userSession.getGlobalOrgOid());
	  sqlParamsSp.add( userSession.getBogOid());
	  sqlParamsSp.add(userSession.getClientBankOid());
      queryListView.setSQL(sql.toString(),sqlParamsSp);

      queryListView.getRecords();

      secProfileDoc = queryListView.getXmlResultSet();
      Debug.debug(secProfileDoc.toString());

      // Get all the Work Groups that the user may be assigned to
      sql = new StringBuilder();
      sql.append("select work_group_oid, work_group_name ");
      sql.append(" from work_group");
      sql.append(" where p_corp_org_oid = ?");
	  sql.append(" and activation_status = ?");
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("work_group_name"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),new Object[]{userSession.getOwnerOrgOid(),TradePortalConstants.ACTIVE});

      queryListView.getRecords();

      workGroupDoc = queryListView.getXmlResultSet();
      Debug.debug(workGroupDoc.toString());

   // Get all the Account defined in corporate customer that the user may be assigned to
      sql = new StringBuilder();
      sql.append("select account_oid, account_number || ' ' || account_name as account_number");
//Added this field as it is needed for filtering account dropdown
      sql.append(" from account");
      sql.append(" where p_owner_oid = ?");
      sql.append(" and deactivate_indicator != ?");
      List<Object> sqlParamsAcc = new ArrayList();
      sqlParamsAcc.add(userSession.getOwnerOrgOid());
      sqlParamsAcc.add(TradePortalConstants.INDICATOR_YES);
	  if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccnt)){
    	  sql.append(" and othercorp_customer_indicator != ?");
    	  sqlParamsAcc.add(TradePortalConstants.INDICATOR_YES);
      }
	  sql.append(" order by ");
	  sql.append(resMgr.localizeOrderBy("account_number"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),sqlParamsAcc);

      queryListView.getRecords();

      accountDoc = queryListView.getXmlResultSet();
      Debug.debug(accountDoc.toString());

      sql = new StringBuilder();
      sql.append("select payment_template_group_oid, name");
      sql.append(" from payment_template_group");
      sql.append(" where P_OWNER_ORG_OID in (?,?,?,?)");
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      Debug.debug(sql.toString());
      List<Object> sqlParamsPayT = new ArrayList();
      sqlParamsPayT.add(userSession.getOwnerOrgOid());
      sqlParamsPayT.add(userSession.getClientBankOid());
      sqlParamsPayT.add(userSession.getGlobalOrgOid());
      sqlParamsPayT.add(userSession.getBogOid());
      queryListView.setSQL(sql.toString(),sqlParamsPayT);

      queryListView.getRecords();

      templateGroupDoc = queryListView.getXmlResultSet();
      Debug.debug(templateGroupDoc.toString());
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in UserDetail.jsp");
      }
  }  // try/catch/finally block


  // Determine if the bank requires passwords for authentication.  This
  // boolean gets passed to a fragment when creating the security section.
  
//jgadela R90 IR T36000026319 - SQL INJECTION FIX
  String passwordSql = "select authentication_method "
                     + "from corporate_org where organization_oid = ?";

  DocumentHandler result = DatabaseQueryBean.getXmlResultSet(passwordSql, false, new Object[]{userSession.getOwnerOrgOid()});

  String corpAuthMethod = result.getAttribute("/ResultSetRecord(0)/AUTHENTICATION_METHOD");
  String userAuthMethod = user.getAttribute("authentication_method");
  String additionalAuthType = user.getAttribute("additional_auth_type");
  String userProfileMobileBankingInd = user.getAttribute("mobile_banking_access_ind");
  Debug.debug("Mobile Banking Indicator ["+ userProfileMobileBankingInd +"]");
  
  String additionalAuth2FAType = user.getAttribute("auth2fa_subtype");
  String stricterPasswordSetting = null;
  String minimumPasswordLength = null;

	if(instanceHSMEnabled)
	{
		String ownershipLevel = user.getAttribute("ownership_level");
		if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		{
			Debug.debug("[UserDetail.jsp] User's ownership level is OWNER_GLOBAL");
			stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
			Debug.debug("[UserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
			minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
			Debug.debug("[UserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
		} // if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		else
		{
			ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
			String clientBankOid = null;
			if(InstrumentServices.isNotBlank(userSession.getClientBankOid()))
			{
				clientBankOid = userSession.getClientBankOid();
				Debug.debug("[UserDetail.jsp] Got clientBankOid from userSession.getClientBankOid");
			} // if(InstrumentServices.isNotBlank(userSession.getClientBankOid()))
			else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
			{
				clientBankOid = doc.getAttribute("/In/User/user_oid");
				Debug.debug("[UserDetail.jsp] Got clientBankOid from doc.getAttribute(\"/In/User/user_oid\")");
			} // else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
			else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))
			{
				clientBankOid = user.getAttribute("client_bank_oid");
				Debug.debug("[UserDetail.jsp] Got clientBankOid from user.getAttribute(\"client_bank_oid\")");
			} // else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))

			if(clientBankOid != null)
			{
				Debug.debug("[UserDetail.jsp] Getting client bank with oid = "+clientBankOid);
				clientBank.getById(clientBankOid);

				if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
				{
					Debug.debug("[UserDetail.jsp] User's ownership level is OWNER_CORPORATE");
					stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
					Debug.debug("[UserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
					minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
					Debug.debug("[UserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
				} // if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
				else
				{
					stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
					Debug.debug("[UserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
					minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
					Debug.debug("[UserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
				} // else
			} // if(clientBankOid != null)
		} // else

		// Default values for stricterPasswordSetting and minimumPasswordLength
		if((stricterPasswordSetting == null) || (stricterPasswordSetting.trim().length() == 0))
		{
			stricterPasswordSetting = TradePortalConstants.INDICATOR_YES;
		}
		if((minimumPasswordLength == null) || (minimumPasswordLength.trim().length() == 0))
		{
			minimumPasswordLength = "8";
		}
	}
%>


<%
	//determine which buttons to show
	if (!isReadOnly)
	{
		showSaveButton=true;
		showSaveCloseButton = true;
		if (!insertMode)
			showDeleteButton=true;
	}

  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
%>


<%-- ********************* HTML for page begins here *********************  --%>

<%-- Body tag included as part of common header --%>
	<jsp:include page="/common/Header.jsp">
		<jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
		<jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
		<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
	</jsp:include>

<input type=hidden value="<%=additionalAuthType%>" name="additionalAuthType" id="additionalAuthType">



<%--cquinton 10/8/2012 ir#5988 move hsm encryption javascript to footer area--%>

<div class="pageMain">
  <div class="pageContent">
<%
 	String pageTitleKey = resMgr.getText("UserDetail.Users",TradePortalConstants.TEXT_BUNDLE);
	String helpUrl = "customer/user_detail.htm";
%>

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="item1Key" value="" />
      <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
    </jsp:include>
<%
  //cquinton 10/30/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if ( insertMode) {
    subHeaderTitle = resMgr.getText("UserDetail.NewUser",TradePortalConstants.TEXT_BUNDLE);
  } else {
    subHeaderTitle = user.getAttribute("user_identifier");
  }

  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToRefDataHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToRefDataHome", parms.toString(), response);
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%=subHeaderTitle%>" />
    </jsp:include>

<form id="UserDetail" name="UserDetail"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>

<% if (instanceHSMEnabled)
	{
%>
  <input type=hidden name="protectedNewPassword" value="">
    <input type=hidden name="encryptedRetypedNewPassword" value="">
  
  <input type=hidden name="passwordValidationError" value="">
<%
	}
%>
<%--jgadela rel8.3 CR501 [START] --%>
<input type=hidden name="PendingReferenceDataOid" value="<%=pendingReferenceDataOid %>">
<input type=hidden name="pendingChangedObjectOid" value="<%=pendingChangedObjectOid %>">
<input type=hidden name="UserType" value="CORPORATE">
<%
if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(adminUserDualCtrlReqdInd)){
%>
  <input type=hidden name="isDualControlRequired" value="Y">
<%
}else{
%>
  <input type=hidden name="isDualControlRequired" value="N">
<%}%>
<%--jgadela rel8.3 CR501 [END] --%>
<%
  // Store values such as the userid, his security rights, and his org in a secure
  // hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);
  secureParms.put("opt_lock",  user.getAttribute("opt_lock"));
  secureParms.put("ownership_level", "CORPORATE");
  secureParms.put("user_oid", oid);
  secureParms.put("RegisteredSSOId", user.getAttribute("registered_sso_id"));//BSL 09/06/11 CR663 Rel 7.1 Add
%>

<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent"> <%-- Form Content Area starts here --%>
	<%= widgetFactory.createSectionHeader("1", "UserDetail.General") %>
         <%@ include file="fragments/UserDetail-General.frag"%>
	</div> <%-- General Tab Ends Here--%>
	
	<%= widgetFactory.createSectionHeader("2", "UserDetail.Security") %>
	  <%@ include file="fragments/UserSecuritySection.frag"%>
          <%--cquinton 11/15/2012 moved additional authentication to UserSecuritySection
              so it commonly used by user preferences--%>
              <% if(TradePortalConstants.INDICATOR_YES.equals(cbMobileBankingInd) && TradePortalConstants.INDICATOR_YES.equals(corpCustMobileBankingInd)){%>
  				<div id="userProfileMBInd">
 					 <%= widgetFactory.createLabel("", "UserProfile.EnableMobileBanking", false, false, false, "") %>
 					 <div class="formItemWithIndent1">
 					 <%=widgetFactory.createRadioButtonField("MobileBankingInd","userProfileMobileBankingInd" + "Yes", "common.Yes", TradePortalConstants.INDICATOR_YES,
  						TradePortalConstants.INDICATOR_YES.equals(userProfileMobileBankingInd),isReadOnly, "", "") %>	
 						 <br>
 					 <%=widgetFactory.createRadioButtonField("MobileBankingInd", "userProfileMobileBankingInd"+ "No", "common.No", TradePortalConstants.INDICATOR_NO,
  					TradePortalConstants.INDICATOR_NO.equals(userProfileMobileBankingInd),isReadOnly, "", "") %>	
  					</div>
				 </div>
		<% 
  				}
 		 %>            
        </div> <%-- Security Tab Ends Here --%>

	<%= widgetFactory.createSectionHeader("3", "UserDetail.AssignedTo") %>
		<%@ include  file="fragments/UserDetail-AssignedTo.frag"%>
	</div> <%-- AssignedTo Tab Ends Here --%>

	<%= widgetFactory.createSectionHeader("4", "UserDetail.PanelAuthoritySection") %>
		<%@ include  file="fragments/UserDetail-PanelAuthority.frag"%>
	</div> <%-- Panel Authority Tab Ends Here --%>

	<%= widgetFactory.createSectionHeader("5", "UserDetail.TransactionProcessingSetting") %>
		<%@ include  file="fragments/UserDetail-TransactionProcessingSetting.frag"%>
	</div> <%-- Transaction Processing Setting Tab Ends Here --%>

	<%= widgetFactory.createSectionHeader("6", "UserDetail.TemplateGroups") %>
		<%@ include  file="fragments/UserDetail-TemplateGroups.frag"%>
	</div> <%-- Template Groups Tab Ends Here --%>

	<%= widgetFactory.createSectionHeader("7", "UserDetail.AccountAvailableforMakingPayments") %>
		<%=widgetFactory.createSubLabel("UserDetail.AcountstobeUsedforFundsTransfer")%>
		<%@ include  file="fragments/UserDetail-AccountAccessforFundsTransfer.frag"%>
	</div> <%-- Account Access for FundsTransfer Tab Ends Here --%>

	<%= widgetFactory.createSectionHeader("8", "UserDetail.ReportCategs") %>
		<%@ include  file="fragments/UserDetail-ReportCategories.frag"%>
	</div> <%-- Report Categs Tab Ends Here --%>

	<%= widgetFactory.createSectionHeader("9", "UserDetail.SubsidiaryAccess") %>
		<%@ include  file="fragments/UserDetail-SubsidiaryAccess.frag"%>
	</div>

<%
  // ONLY SHOW THIS SECTION IF THE USER'S CORP ORG HAS DOC PREP ACCESS
  if (corporateOrg.getAttribute("doc_prep_ind").equals(TradePortalConstants.INDICATOR_YES)) {
%>
	<%= widgetFactory.createSectionHeader("10", "UserDetail.DocumentPreparation") %>
		<%@ include  file="fragments/UserDetail-CorporateServices.frag"%>
	</div>
<%
  }
  else {
    // Default this to N for users that don't do doc prep
    if(insertMode)
      secureParms.put("DocPrepInd", TradePortalConstants.INDICATOR_NO);
  }
%>
</div> <%-- Form Content End Here --%>
</div><%--formArea--%>
<%//jgadela rel8.3 CR501 [START] %>
<%
String cancelAction = "goToRefDataHome";
 if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction")) || "ApproveButton".equalsIgnoreCase(buttonPressed) || "RejectButton".equalsIgnoreCase(buttonPressed)){
	 cancelAction = "goToRefDataApproval";
 }
%>
<%//jgadela rel8.3 CR501 [END] %>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'UserDetail'">
                   <jsp:include page="/common/RefDataSidebar.jsp">
                             <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
                             <jsp:param name="saveOnClick" value="none" />
                             <jsp:param name="showDeleteButton" value="<%= showDeleteButton %>" />
                             <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButton %>" />
                             <jsp:param name="saveCloseOnClick" value="none" />
                             <jsp:param name="cancelAction" value="<%= cancelAction %>" />
                             <jsp:param name="showHelpButton" value="true" />
			            	<jsp:param name="showLinks" value="true" />
			            	<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
			            	<jsp:param name="error" value="<%= error%>" />
                             <jsp:param name="helpLink" value="customer/user_detail.htm" />
                             <jsp:param name="showApproveButton" value="<%=showApprove%>" />
                             <jsp:param name="showRejectButton" value="<%=showReject%>" />
                             
                   </jsp:include>
          </div> <%--closes sidebar area--%>
<%= formMgr.getFormInstanceAsInputField("UserDetailForm", secureParms) %>

<%--cquinton 10/12/2012 move closing form tag here to make html valid so javascript works--%>
</form>
</div>
</div>


<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>


<%--cquinton 10/8/2012 ir#5988 start
    move hsm encryption javascript to footer area--%>
<%
  //save behavior defaults to without hsm
  String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";

  // Include javascript functions for encryption
  if (instanceHSMEnabled) {
%>
<script src="/portal/js/crypto/pidcrypt.js"></script>
<script src="/portal/js/crypto/pidcrypt_util.js"></script>
<script src="/portal/js/crypto/asn1.js"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="/portal/js/crypto/jsbn.js"></script><%-- needed for rsa math operations --%>
<script src="/portal/js/crypto/rng.js"></script><%-- needed for rsa key generation --%>
<script src="/portal/js/crypto/prng4.js"></script><%-- needed for rsa key generation --%>
<script src="/portal/js/crypto/rsa.js"></script><%-- needed for rsa en-/decryption --%>
<script src="/portal/js/crypto/md5.js"></script><%-- needed for key and iv generation --%>
<script src="/portal/js/crypto/aes_core.js"></script><%-- needed block en-/decryption --%>
<script src="/portal/js/crypto/aes_cbc.js"></script><%-- needed for cbc mode --%>
<script src="/portal/js/crypto/sha256.js"></script>
<script src="/portal/js/crypto/tpprotect.js"></script><%-- needed for cbc mode --%>

<script>
  function encryptHsmFields() {
    var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
    var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
    <%-- for debugging hsm in dev comment out above and comment in below --%>
    <%-- var public_key = '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvmMiU1irZ6aejIm0B+cKL9foNO+iDdftWwFS0eUYmu8A4IJWxoq66hgJ1+z84TuKrkHYhWGYz1iK1sxsf5CgbN86UumXX99c3KhfP3YJrgHzohvpU0yt/HAlSLa7JgnuSTNYcQiaGW/y1lkuS8bzSw2Hb/yeXUI1yt7KhwTPjE9hJnh5Jk7YERebRXP7MCOUxWJBuEjidmYIRjRkqzJ3FDEG1WByLjco5cmMaWZYdz9GUUEF3lIW7/Zb0kt40B2Ct0b3sIPhH5g6UF+nCzXxzBy5FOMMdUxh23re2ob88Ec+9AfYcD3qL9HS9IQ3ztAHTPZbhcwj8++8b9sXrfo/+QIDAQAB\n-----END PUBLIC KEY-----'; --%>
    <%-- var iv = 'zhXaSTr+/981U4TPJSu/Yg=='; --%>

    var protector = new TPProtect();
    document.UserDetail.passwordValidationError.value = "";

    if((document.UserDetail.Password1.value != "") || (document.UserDetail.Password2.value != "")) <%-- Run the following logic only if the password fields are not blank --%>
    {

      <%-- Ensure that the new password and retyped password match --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.MatchingEntries({errorCode: "<%= TradePortalConstants.ENTRIES_DONT_MATCH %>", newPassword: document.UserDetail.Password1.value, retypePassword: document.UserDetail.Password2.value});
      }

      <%-- Ensure that the new password is not the same as the user_identifier --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.UserDetail.Password1.value, sameAsString: "<%= user.getAttribute("user_identifier") %>"});
      }

      <%-- Ensure that the new password is not the same as the first_name --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.UserDetail.Password1.value, sameAsString: "<%= user.getAttribute("first_name") %>"});
      }

      <%-- Ensure that the new password is not the same as the last_name --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.UserDetail.Password1.value, sameAsString: "<%= user.getAttribute("last_name") %>"});
      }

      <%-- Ensure that the new password is not the same as the login_id --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: document.UserDetail.Password1.value, sameAsString: "<%= user.getAttribute("login_id") %>"});
      }

      <%-- Ensure that the new password does not start or end with a space --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.StartEndWithSpace({errorCode: "<%= TradePortalConstants.PASSWORD_SPACE %>", newPassword: document.UserDetail.Password1.value});
      }

      <%-- Ensure that the new password is the required length --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.IsRequiredLength({errorCode: "<%= TradePortalConstants.PASSWORD_TOO_SHORT %>", newPassword: document.UserDetail.Password1.value, reqLength: <%= minimumPasswordLength %>});
      }

      <%-- Ensure that the new password meets the consecutive characters requirements --%>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.CheckConsecutiveChars({errorCode: "<%= TradePortalConstants.PASSWORD_CONSECUTIVE %>", newPassword: document.UserDetail.Password1.value});
      }

<%  if(stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) { %>
      if (document.UserDetail.passwordValidationError.value == "") {
        document.UserDetail.passwordValidationError.value = protector.CheckPasswordComplexity({errorCode: "<%= TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT %>", newPassword: document.UserDetail.Password1.value});
      }
<%  } %>
      <%-- [START] CR-543 --%>
      <%-- Encrypt the MAC of the new password --%>
      <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- START --%>
      
      var passwordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: document.UserDetail.Password1.value, challenge: iv});
      var combinedPassword = passwordMac + "|" + document.UserDetail.Password1.value;
      document.UserDetail.protectedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: combinedPassword});
      <%-- [END] CR-543 --%>
      
      if (document.UserDetail.Password2) {
          document.UserDetail.encryptedRetypedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.UserDetail.Password2.value});  
       }    
      <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- END --%>
      <%-- Blank out the password fields --%>
      document.UserDetail.Password1.value = "";
      document.UserDetail.Password2.value = "";
    } <%--  if(document.UserDetail.Password1 && document.UserDetail.Password2) --%>
  }
</script>
<%
    
    //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
    // saveOnClick = "encryptHsmFields();" + saveOnClick;
    // saveCloseOnClick = "encryptHsmFields();" + saveCloseOnClick;
  }
%>

<jsp:include page="/common/RefDataSidebarFooter.jsp" />

<%--cquinton 10/8/2012 ir#5988 end--%> 

<%
  String focusField = "UserId";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<%-- ********************* JavaScript for page begins here *********************  --%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
        <%-- CR 821 - Rel 8.3--%>
        enablePanelSelection();
      });
  });

  function setSubAccessRadioButton() {
    if (document.forms[0].SubAccessViewParentOrgData.checked == true) {
      document.forms[0].SubsidiaryAccess[1].checked = true;
    }
  }

</script>
<%
  }
%>

<%--cquinton 10/12/2012 move closing form tag above to make html valid so javascript works--%>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

<%-- UserDetail Page Ends Komal --%>
<%-- BSL 09/01/11 CR663 Rel 7.1 Begin --%>
<form name="UserDetailForm-UnregisterUserAccount" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
  <input type=hidden value="" name=buttonName>
<%
	Hashtable addlUnregisterParms = new Hashtable();

	// The remote user will be registered to the userOID
	addlUnregisterParms.put("newRegisteredSSOId", "");
	addlUnregisterParms.put("userOID", oid);
	addlUnregisterParms.put("changingRegisteredSSOIdFlag", "true");
%>
<%= formMgr.getFormInstanceAsInputField("UserDetailForm-UnregisterUserAccount",
		addlUnregisterParms, "unregisterSSO") %>
</form>
<%-- BSL 09/01/11 CR663 Rel 7.1 End --%>

<%--cquinton 2/19/2013 ir#11753 modify javascript so we fields on change of radio selection, etc.
    note that we MUST defensively check these because when auth method is not PERUSER, the radio
    buttons are not created.
    Also: javascript should be just before closing body tag.--%>
<script>

  var local = {};
  var insertMode = <%=insertMode%>;

  require(["dojo/dom", "dojo/dom-class", "dojo/on", "dijit/registry", "dojo/ready", "dojo/domReady!"],
      function(dom, domClass, on, registry, ready) {

    <%-- make a widget required or not --%>
    <%-- manipulates both the widget validation and the formItem required class --%>
    local.setRequired = function(widget, requiredness) {
      if ( widget ) {
        var parentNode = widget.domNode.parentNode;
        if ( requiredness ) {
          widget.required = true;
          domClass.add(parentNode, "required");
        }
        else {
          widget.required = false;
          domClass.remove(parentNode, "required");
        }
      }
    };

<%
  //only do the security field manipulation if auth is per user
  if ( TradePortalConstants.AUTH_PERUSER.equals(corpAuthMethod) ) {
%>
    ready(function(){
      <%-- get the various authentication checkboxes --%>
      var authPass = registry.byId("TradePortalConstants.AUTH_PASSWORD");
      var auth2fa = registry.byId("TradePortalConstants.AUTH_2FA");
      var authSso = registry.byId("TradePortalConstants.AUTH_SSO");
      var authRegisteredSso = registry.byId("TradePortalConstants.AUTH_REGISTERED_SSO");
      var authCert = registry.byId("TradePortalConstants.AUTH_CERTIFICATE");

      <%-- if peruser, the potentially required fields are --%>
      <%--  initially set as required so they are validation textboxes --%>
      <%-- here set them not required on page load --%>

      <%-- determine whether loginID should be required or not --%>
      if((authSso && authSso.checked) ||
         (authRegisteredSso && authRegisteredSso.checked) ||
         (authCert && authCert.checked) ) {
        <%-- loginId not required --%>
        var loginId = registry.byId("loginID");
        local.setRequired(loginId, false);
      }
      <%-- if nothing checked nothing is required --%>
      if((authPass && !authPass.checked) &&
         (auth2fa && !auth2fa.checked) &&
         (authCert && !authCert.checked) &&
         (authSso && !authSso.checked) &&
         (authRegisteredSso && !authRegisteredSso.checked) ) {
        var loginId = registry.byId("loginID");
        local.setRequired(loginId, false);
      }

      <%-- now set onchange events so requiredness is set appropriately --%>
      if ( authPass ) {
        on(authPass, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_PASSWORD").checked){
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, true);
          }
        });
      }
      if ( auth2fa ) {
        on(auth2fa, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_2FA").checked){
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, true);
          }
        });
      }
      if ( authSso ) {
        on(authSso, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_SSO").checked){
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, false);
          }
        });
      }
      if ( authRegisteredSso ) {
        on(authRegisteredSso, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_REGISTERED_SSO").checked){
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, false);
          }
        });
      }
      if ( authCert ) {
        on(authCert, "change", function(){
          if(dom.byId("TradePortalConstants.AUTH_CERTIFICATE").checked){
            <%-- set loginId required = false --%>
            var loginId = registry.byId("loginID");
            local.setRequired(loginId, false);
          }
        });
      }

    });
<%
  } //if auth per user
%>
  });
<%-- CR 821 - Rel 8.3--%>
  function enablePanelSelection(){
	  if(document.forms[0].useSubsidiaryProfile[0].checked == true){
		  dijit.byId("SubsidiaryPanelAuthority").set('disabled',true);
		  dijit.byId("SubsidiaryAuthorizeOwnInputInd").set('disabled',true);
	  }else if(document.forms[0].useSubsidiaryProfile[1].checked == true){
		  dijit.byId("SubsidiaryPanelAuthority").set('disabled',false);
		  dijit.byId("SubsidiaryAuthorizeOwnInputInd").set('disabled',false);
	  }
  }
  function setOptions()
  {
     if (document.forms[0].AdditionalAuthType[0].checked== true)
     {
     	  dijit.byId("AdditionalAuthType-N").set('checked',false);
  	  dijit.byId("TradePortalConstants.AUTH_2FA_SUBTYPE_OTP").set('checked',false);
  	  dijit.byId("TradePortalConstants.AUTH_2FA_SUBTYPE_SIG").set('checked',false);
      }
     else
     {
  	   if (document.forms[0].AdditionalAuthType[1].checked== true)
        {
        dijit.byId("AdditionalAuthType-Y").set('checked',false);
  	dijit.byId("TradePortalConstants.AUTH_2FA_SUBTYPE_OTP").set('checked',true);
  	 }
     }
  }
  
<%-- KMehta on 16 Dec 2015 @ Rel 9200 for IR T36000034128 Start --%>
  function clear2FA()
  {
	  dijit.byId("TradePortalConstants.AUTH_2FA_SUBTYPE_OTP").set('checked',false);
	  dijit.byId("TradePortalConstants.AUTH_2FA_SUBTYPE_SIG").set('checked',false);
	  dijit.byId("AdditionalAuthType-Y").set('checked',false);
	  dijit.byId("AdditionalAuthType-N").set('checked',false);
  }
  <%-- KMehta on 16 Dec 2015 @ Rel 9200 for IR T36000034128 End --%>

  
  <%--  jgadela Rel 8.4 CR-854 [BEGIN]- Dynamic population of ReportingCategory dropdowns based on ReportingLanguage selection --%>
   var repCatEn = <%=(!StringFunction.isBlank(repCatEn))?repCatEn:"''"%>; 
   var repCatFr = <%=(!StringFunction.isBlank(repCatFr))?repCatFr:"''"%>; 
   var userDetailReptCateg1Oid = '<%=userDetailReptCateg1Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg1Oid, userSession.getSecretKey()):""%>'; 
   var userDetailReptCateg2Oid = '<%=userDetailReptCateg2Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg2Oid, userSession.getSecretKey()):""%>'; 
   var userDetailReptCateg3Oid = '<%=userDetailReptCateg3Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg3Oid, userSession.getSecretKey()):""%>'; 
   var userDetailReptCateg4Oid = '<%=userDetailReptCateg4Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg4Oid, userSession.getSecretKey()):""%>';
   var userDetailReptCateg5Oid = '<%=userDetailReptCateg5Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg5Oid, userSession.getSecretKey()):""%>'; 
   var userDetailReptCateg6Oid = '<%=userDetailReptCateg6Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg6Oid, userSession.getSecretKey()):""%>';
   var userDetailReptCateg7Oid = '<%=userDetailReptCateg7Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg7Oid, userSession.getSecretKey()):""%>'; 
   var userDetailReptCateg8Oid = '<%=userDetailReptCateg8Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg8Oid, userSession.getSecretKey()):""%>';
   var userDetailReptCateg9Oid = '<%=userDetailReptCateg9Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg9Oid, userSession.getSecretKey()):""%>'; 
   var userDetailReptCateg10Oid = '<%=userDetailReptCateg10Oid!=null ? EncryptDecrypt.encryptStringUsingTripleDes(userDetailReptCateg10Oid, userSession.getSecretKey()):""%>';
   
   function onChangeRepLang() {
 	 
 	  var newStore;
 	  require(["dojo/_base/array", "dojo/dom","dojo/query",
 	           "dijit/registry", "dojo/store/Memory","dojo/ready"], 
 	      function(baseArray, dom, query,
 	               registry, Memory, ready ) {
 		  
 		  var reportingLanguage = registry.byId("ReportingLanguage");
 		  for(var i=1;i<=10;i++){
 		  
 			  var ReportingCategory = registry.byId("ReportingCategory"+i);
 			  
 			  if("fr_CA" == reportingLanguage){
 				  newStore = new Memory({ data: repCatFr });
 			  }else{
 				  newStore = new Memory({ data: repCatEn });
 			  }
 			  ReportingCategory.set('store',newStore);
 		  }
 		registry.byId("ReportingCategory1").set('value',userDetailReptCateg1Oid);
 		registry.byId("ReportingCategory2").set('value',userDetailReptCateg2Oid);
 		registry.byId("ReportingCategory3").set('value',userDetailReptCateg3Oid);
 		registry.byId("ReportingCategory4").set('value',userDetailReptCateg4Oid);
 		registry.byId("ReportingCategory5").set('value',userDetailReptCateg5Oid);
 		registry.byId("ReportingCategory6").set('value',userDetailReptCateg6Oid);
 		registry.byId("ReportingCategory7").set('value',userDetailReptCateg7Oid);
 		registry.byId("ReportingCategory8").set('value',userDetailReptCateg8Oid);
 		registry.byId("ReportingCategory9").set('value',userDetailReptCateg9Oid);
 		registry.byId("ReportingCategory10").set('value',userDetailReptCateg10Oid);
 	 
 	  });
   }
 <%-- jgadela Rel 8.4 CR-854 [END]- Dynamic population of ReportingCategory dropdowns based on ReportingLanguage selection --%>


</script>

<%--
  //for debugging
  out.println("corpAuthMethod="+corpAuthMethod);
--%>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>


<%
	if (instanceHSMEnabled){
%>

		<script>
			
			var preSave = function(){
 				encryptHsmFields();
 				return true;
			}
			
			var preSaveAndClose = function(){
  	 	 
 				encryptHsmFields();
 				return true;

			 }
	
		</script>
<%
	}
%>

</body>
</html>



<%!
//jgadela Rel 8.4 CR-854 [BEGIN]- Dynamic population of ReportingCategory dropdowns based on ReportingLanguage selection
String getReportCatData(Vector aVector, SecretKey secretKey)
{
   StringBuffer data = new StringBuffer();
   if(aVector == null){
		   return data.toString();
   }
   
   int numItems = aVector.size();
   
   for (int iLoop=0;iLoop<numItems;iLoop++) {
       DocumentHandler acctDoc = (DocumentHandler) aVector.elementAt(iLoop);
       String code = acctDoc.getAttribute("/CODE") != null ? acctDoc.getAttribute("/CODE") : acctDoc.getAttribute("/code");
       String descr = acctDoc.getAttribute("/DESCR") != null ? acctDoc.getAttribute("/DESCR") : acctDoc.getAttribute("/descr");
       
       
       code = EncryptDecrypt.encryptStringUsingTripleDes(code, secretKey);
       
         data.append("{ ");
         data.append("id:'").append(code).append("'");
         data.append(", ");
         data.append("name:'").append(descr).append("'");
         data.append(" }");
         if ( iLoop < numItems-1) {
           data.append(", ");
         }
       }
   return "["+data.toString()+"]";
}
//jgadela Rel 8.4 CR-854 [END]- Dynamic population of ReportingCategory dropdowns based on ReportingLanguage selection
%>
