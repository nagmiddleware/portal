
<%--
*
*     Copyright   2001                         
*     American Management Systems, Incorporated 
*     All rights reserved
--%>
<%@ page
	import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
com.amsinc.ecsg.frame.*,com.ams.tradeportal.busobj.util.*"%>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session"></jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session"></jsp:useBean>

<%
WidgetFactory widgetFactory = new WidgetFactory(resMgr);

Debug.debug("***START***********CLIENT*BANK*DETAIL*******************START***");
//CR-501 IR-17647 Rel8.3 MEerupula
if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
	userSession.setCurrentPrimaryNavigation("AdminNavigationBar.Organizations");
}else{
   userSession.setCurrentPrimaryNavigation("NavigationBar.Organizations");
}

/****************************************************************
* THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A CLIENT BANK
* CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
* VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
* AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
* TO THE FORM MANAGER.
****************************************************************/

/*********\
* GLOBALS *
\*********/
boolean isNew          = false; //is this a new client bank
boolean readOnly       = false; //is the page read only
boolean getDataFromDoc = false; //do we need to refresh from the doc cache
boolean showSave       = true;  //do we show the save button
boolean showSaveClose       = true;  //do we show the save button
boolean showDelete     = true;  //do we show the delete button
boolean defaultCheckBoxValue = false;

String oid          = null; //the oid of the client bank being edited
String loginRights  = null; //login rights of the user viewing the page

Hashtable secureParams = new Hashtable();//stores the secure parameters

DocumentHandler doc = null;//the doc we will use to get data from cache
DocumentHandler queryDoc = null; //this doc will be use to get data from a query

//ClientBank Web Bean
beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.ClientBankWebBean", "ClientBank");
ClientBankWebBean editClient = (ClientBankWebBean)beanMgr.getBean("ClientBank");

//Get security rights
loginRights = userSession.getSecurityRights();
readOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CB_BY_PX);
Debug.debug("MAINTAIN_CB_BY_PX == "+SecurityAccess.hasRights(loginRights,
SecurityAccess.MAINTAIN_CB_BY_PX));

// Get the document from the cache.  We'll use it to repopulate the screen if the
// user was entering data with errors.
doc = formMgr.getFromDocCache();

Vector error = null;
error = doc.getFragments("/Error/errorlist/error");

String buttonClicked = request.getParameter("buttonName");

Debug.debug("doc from cache is " + doc.toString());
// The logic is modified for the TP refresh, - 24/11/2012
if (doc.getDocumentNode("/In/ClientBank") != null )
{
getDataFromDoc = true;

oid = doc.getAttribute("/In/ClientBank/organization_oid");
if("0".equals(oid) || oid==null){
oid = doc.getAttribute("/Out/ClientBank/organization_oid");
}

String maxError = doc.getAttribute("/Error/maxerrorseverity");
if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY))>=0) {
			// We've returned from a save/update/delete but have errors.
			// We will display data from the cache.  Determine if we're
			// in insertMode by looking for a '0' oid in the input section
			// of the doc.
			oid = doc.getAttribute("/In/ClientBank/organization_oid");
			if(oid.equals("0"))
			{
				Debug.debug("Error occered while CREATING a Client Bank");
				isNew = true;//else it stays false
			}
			else {// Not in insert mode, use oid from input doc.
				Debug.debug("Error occured while UPDATING a party");
				oid = doc.getAttribute("/In/ClientBank/organization_oid");
			}
} else {
// We've returned from a save/update/delete that was successful
// We shouldn't be here.  Forward to the AdminRefDataHome page.
// This normally should be handled by jPylon and Navigation.xml.

}
}

if (getDataFromDoc)
{
	editClient.populateFromXmlDoc(doc,"/In");
	if (!isNew)
		editClient.setAttribute("organization_oid",oid);
}
else if (request.getParameter("oid") != null) //EDIT PARTY
	{
	isNew = false;
	//This getParameter is done twice to prevent overwriting a valid oid
	oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
	Debug.debug("OID -> " + oid);
	editClient.getById(oid);
	Debug.debug("Editing party with oid: " + editClient.getAttribute("organization_oid")); //DEBUG
}
else 
{
	oid = null;
	isNew = true;
}

InstrNumberSequenceWebBean instrNumSeq = null;

if(!isNew)
{
// Retrieve the last used instrument id for this client bank
instrNumSeq = beanMgr.createBean(InstrNumberSequenceWebBean.class,  "InstrNumberSequence");

instrNumSeq.getById(editClient.getAttribute("c_InstrumentNumberSequence"));

DocumentHandler xml = new DocumentHandler();

instrNumSeq.populateXmlDoc(xml);
}

// onLoad is set to default the cursor to the partyName field but only if the
// page is not in readonly mode.
String onLoad = "";
if (!readOnly) {
onLoad = "document.ClientBankDetail.clientName.focus();";
}

if (readOnly)
{
showSave = false;
showDelete = false;
showSaveClose = false;

}
else if (isNew)
{
oid = "0";
editClient.setAttribute("organization_oid",oid);
showDelete = false;
}

//START SECUREPARAMS
secureParams.put("oid",oid);
secureParams.put("login_oid",userSession.getUserOid());
secureParams.put("login_rights",loginRights);
secureParams.put("opt_lock", editClient.getAttribute("opt_lock"));
secureParams.put("parentOid", userSession.getGlobalOrgOid());

if (isNew)
secureParams.put("activation_status", TradePortalConstants.ACTIVE);
//END SECUREPARAMS


// Retrieve the data used to populate some of the dropdowns.
// This data will be used in the default users dropdown

StringBuilder sql = new StringBuilder();

QueryListView queryListView = null;

try {
queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
formMgr.getServerLocation(), "QueryListView");

// Get a list of the users for the client bank
sql = new StringBuilder();
sql.append("select USER_OID, USER_IDENTIFIER");
sql.append(" from users");
sql.append(" where P_OWNER_ORG_OID = ?");
sql.append(" order by ");
sql.append(resMgr.localizeOrderBy("USER_IDENTIFIER"));
Debug.debug(sql.toString());
queryListView.setSQL(sql.toString(),new Object[]{ oid});

queryListView.getRecords();

queryDoc = queryListView.getXmlResultSet();
Debug.debug("queryDoc -- > " + queryDoc.toString());

} catch (Exception e) {
e.printStackTrace();
} finally {
try {
if (queryListView != null) {
queryListView.remove();
}
} catch (Exception e) {
System.out.println("error removing querylistview in ClientBankDetail.jsp");
}
}  // try/catch/finally block

//cquinton cr498 get instrument authentication details
String requireTranAuth = editClient.getAttribute("require_tran_auth");

//CR ANZ 501 Rel 8.3 04-04-2013 jgadela  Beign

String corpOrgDualCtrlReqdInd = editClient.getAttribute("corp_org_dual_ctrl_reqd_ind");
String adminUserDualCtrlReqdInd = editClient.getAttribute("admin_user_dual_ctrl_reqd_ind");
String corpUserDualCtrlReqdInd = editClient.getAttribute("corp_user_dual_ctrl_reqd_ind");
//CR ANZ 501 Rel 8.3 04-04-2013 jgadela  End

%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />

<div class="pageMain">
	<div class="pageContent">

		<jsp:include page="/common/PageHeader.jsp">
			<jsp:param name="titleKey" value="ClientBankDetail.ClientBanks" />
			<jsp:param name="helpUrl" value="admin/client_bank_detail.htm" />
		</jsp:include>

		<% 
//cquinton 10/29/2012 ir#7015 remove return link
String subHeaderTitle = "";
if (!isNew) {
subHeaderTitle = editClient.getAttribute("name");
}
else {
subHeaderTitle = resMgr.getText("ClientBankDetail.NewClientBank", TradePortalConstants.TEXT_BUNDLE);
}
%>
		<jsp:include page="/common/PageSubHeader.jsp">
			<jsp:param name="title" value="<%= subHeaderTitle %>" />
		</jsp:include>

		<form id="ClientBankDetail" name="ClientBankDetail" method="post"
			data-dojo-type="dijit.form.Form"
			action="<%=formMgr.getSubmitAction(response)%>">
			<input type=hidden value="" name=buttonName>

			<div class="formArea">
				<jsp:include page="/common/ErrorSection.jsp" />

				<div class="formContent">


					<%
/**==========================
*======== General =========
*==========================*/
%>
					<%= widgetFactory.createSectionHeader("1", "ClientBankDetail.General") %>
					<br>
					<div class="columnLeft">
						<%= widgetFactory.createTextField("name", "ClientBankDetail.ClientBank", editClient.getAttribute("name"), "35", 
readOnly, true, false, "class='char30'", "","") %>
						<%= widgetFactory.createTextField("address1", "ClientBankDetail.AddressLine1", editClient.getAttribute("address_line_1"), "35", 
readOnly, true, false, "class='char30'", "","") %>
						<%= widgetFactory.createTextField("address2", "ClientBankDetail.AddressLine2", editClient.getAttribute("address_line_2"), "35", 
readOnly, false, false, "class='char30'", "","") %>
						<%= widgetFactory.createTextField("city", "ClientBankDetail.City", editClient.getAttribute("address_city"), "23", 
readOnly, true, false, "class='char30'", "","") %>

						<div>
							<%= widgetFactory.createTextField("state", "ClientBankDetail.ProvinceState", editClient.getAttribute("address_state_province"), "8", 
readOnly, false, false, "style='width:70px'", "","inline") %>
							<%= widgetFactory.createTextField("postalCode", "ClientBankDetail.PostalCode", editClient.getAttribute("address_postal_code"), "8", 
readOnly, false, false, "style='width:70px'", "","inline") %>
							<div style="clear: both;"></div>
						</div>

						<%
String options = Dropdown.createSortedRefDataOptions("COUNTRY", editClient.getAttribute("address_country"), resMgr.getResourceLocale());
%>
						<%-- ANZ Issue 211 / IR T36000006463 Fixed by Komal BEGINS --%>
						<%-- <%= widgetFactory.createSelectField("country", "ClientBankDetail.Country", " ", options, 
readOnly, true, false, "", "", "") %> --%>
						<%= widgetFactory.createSelectField("country", "ClientBankDetail.Country", " ", options, 
readOnly, true, false, "class='char30'", "", "") %>
						<%-- ANZ Issue 211 / IR T36000006463 Fixed by Komal ENDS --%>
					</div>

					<div class="columnRight">
						<%= widgetFactory.createTextField("phoneNumber", "ClientBankDetail.GeneralPhoneNumber", editClient.getAttribute("phone_number"), "20", readOnly,false,false,"class='char15'","regExp:'[0-9]+'","") %>



						<%= widgetFactory.createTextField("swift1", "ClientBankDetail.SwiftAddress", editClient.getAttribute("swift_address_part1"), "8", 
  readOnly, false, false, "style='width:70px'", "","inline") %>
						<%= widgetFactory.createTextField("swift2", "&nbsp;", editClient.getAttribute("swift_address_part2"), "3", 
  readOnly, false, false, "style='width:23px'", "","none") %>
						<div style="clear: both;"></div>

						<%= widgetFactory.createTextField("fax1", "ClientBankDetail.FaxNumber1", editClient.getAttribute("fax_1"), "20", 
readOnly,false, false, "class='char15'", "regExp:'[0-9]+'","inline") %>
						<%= widgetFactory.createTextField("fax2", "ClientBankDetail.FaxNumber2", editClient.getAttribute("fax_2"), "20", 
readOnly,false, false, "class='char15'", "regExp:'[0-9]+'","inline") %>
						<div style="clear: both;"></div>


						<div>
							<%= widgetFactory.createTextField("OTLId", "ClientBankDetail.OTLCustomerId", editClient.getAttribute("OTL_id"), "3", 
  readOnly, true, false, "class='char15'", "","inline") %>

							<% if (!isNew) { 
options = Dropdown.createSortedOptions(queryDoc,"USER_OID", "USER_IDENTIFIER", editClient.getAttribute("default_user_oid"), userSession.getSecretKey(), resMgr.getResourceLocale());
%>
							<%= widgetFactory.createSelectField("defaultUser", "ClientBankDetail.DefaultUser", " ", options, 
  readOnly, false, false, "class='char15'", "", "inline") %>
							<% } //end if %>
							<div style="clear: both;"></div>
						</div>

						<%//= widgetFactory.createTextField("telexNumber", "ClientBankDetail.TelexNumber", editClient.getAttribute("telex_1"), "20", 
//            readOnly, true, false, "", "","") %>
						<%//= widgetFactory.createTextField("telexAnswer", "ClientBankDetail.TelexAnswerBack", editClient.getAttribute("telex_answer_back_1"), "20", 
//            readOnly, false, false, "", "","") %>
					</div>
					<div style="clear: both;"></div>
				</div>
				<%-- end of titlepane General--%>

				<%
/**==========================
*======== Branding =========
*==========================*/
%>
				<%= widgetFactory.createSectionHeader("2", "ClientBankDetail.BrandingHeading") %>
				<%//= widgetFactory.createLabel("", "ClientBankDetail.BrandingNote", false, false, false, "") %>
				<%//= widgetFactory.createLabel("", "ClientBankDetail.BrandingNoteText", false, false, false, "") %>
				<%= widgetFactory.createTextField("branding_directory", "ClientBankDetail.BrandingDirectory", editClient.getAttribute("branding_directory"), "10", 
readOnly, true, false, "", "","") %>

				<%//= widgetFactory.createCheckboxField("brand_button_ind", "ClientBankDetail.BrandButtonInd", 
//                editClient.getAttribute("brand_button_ind").equals(TradePortalConstants.INDICATOR_YES),readOnly) %>
			</div>
			<%-- end of titlepane Branding--%>

			<%
/**==========================
*======== Corporate Services =========
*==========================*/
%>
			<%= widgetFactory.createSectionHeader("3", "ClientBankDetail.CorporateServicesHeading") %>
			<% 
   				String imgServicePassword = editClient.getAttribute("image_service_password");
   		    %>
   			<%-- MEer 02/25/2015 Rel 9.2 XSS IR-36837 Add image password from web bean to Secure parameters Map 
   			and set password widget value to empty so that it will not be shown on page source once the page loads. --%>
   		<% 
   			secureParams.put("secureImagePassword", editClient.getAttribute("image_service_password"));
   			
   			//IValavala CR 742. Adding new text fields for image servies
        %>
			<table>
				<tr>
					<td align="left"><%= widgetFactory.createTextField("image_service_endpoint", "ClientBankDetail.ImageServicesURL", editClient.getAttribute("image_service_endpoint"),"2000", readOnly,false,false,"","","") %>
					</td>
					<td align="left"><%= widgetFactory.createTextField("image_service_userid", "ClientBankDetail.ImageServicesUserID", editClient.getAttribute("image_service_userid"),"2000", readOnly,false,false,"","","") %>
					</td>
					<td align="left"><%= widgetFactory.createTextField("image_service_password", "ClientBankDetail.ImageServicesPassword", "", "2000", readOnly,false,false,"type=\"password\" AutoComplete=\"off\"","","") %>
					</td>
				</tr>
			</table>
			<%= widgetFactory.createTextField("doc_prep_url", "ClientBankDetail.CorporateServicesURL", editClient.getAttribute("doc_prep_url"),"2000", readOnly,false,false,"","","") %>
	</div>
	<%-- end of titlepane Corporate Services--%>

	<%
/**==========================
*======== Options =========
*==========================*/
String purgeType = editClient.getAttribute("instrument_purge_type");
String supressDocLinkInd = editClient.getAttribute("suppress_doc_link_ind");
String allowPaymentByAnotherAccount = editClient.getAttribute("allow_pay_by_another_accnt");
String allowLiveMarketRates = editClient.getAttribute("allow_live_market_rates_req");
String overrideSwiftLengthInd = editClient.getAttribute("override_swift_length_ind");
String overrideSwiftValidationInd = editClient.getAttribute("override_swift_validation_ind");
String allowOnlyISO8859CharInd = editClient.getAttribute("allow_only_iso8859_ind");
String cbMobileBankingAccessInd = editClient.getAttribute("mobile_banking_access_ind");
//jgadela R8.4 CR 854 T36000024797
String reportingLangOptionInd = editClient.getAttribute("reporting_lang_option_ind");
String preDebitOptionInd = editClient.getAttribute("pre_debit_funding_option_ind");
//SSikhakolli Rel 9.3.5 CR-1029 
String enableAdminUpdateCentre = editClient.getAttribute("enable_admin_update_centre");
String utilizeAdditionalBankFields = editClient.getAttribute("utilize_additional_bank_fields");
String controlOfNotifRulesbyAdmin = editClient.getAttribute("control_NotifRule_byAdmin");//Rel9.5 CR-927B
String allowFilteringofInstruments = editClient.getAttribute("allow_filtering_of_instruments");//Rel9.5 CR-927B
if(StringFunction.isBlank(enableAdminUpdateCentre)){
	enableAdminUpdateCentre = TradePortalConstants.INDICATOR_YES;
}
%>
	<%= widgetFactory.createSectionHeader("4", "ClientBankDetail.Options") %>
	<div class="formItem">
		<table width="100%" border="1" cellspacing="0" cellpadding="0"
			class="formDocumentsTable">
			<tr>
				<th nowrap width="90%" align="left"><%= widgetFactory.createLabel("", " ", false, false, false, "none") %>&nbsp;
				</th>
				<th nowrap width="5%" align="center"><%= widgetFactory.createLabel("", "common.Yes", false, false, false, "none") %>
				</th>
				<th nowrap width="5%" align="center"><%= widgetFactory.createLabel("", "common.No", false, false, false, "none") %>
				</th>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.DeleteInstruments", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("purgeType", "TradePortalConstants.PURGE_DELETE_INSTRUMENT", 
"", TradePortalConstants.PURGE_DELETE_INSTRUMENT, 
(purgeType.equals("") ||purgeType.equals(TradePortalConstants.PURGE_DELETE_INSTRUMENT)), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("purgeType", "TradePortalConstants.PURGE_KEEP_INSTRUMENT", 
"", TradePortalConstants.PURGE_KEEP_INSTRUMENT, 
purgeType.equals(TradePortalConstants.PURGE_KEEP_INSTRUMENT), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.SupressPDFDocuments", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("supressDocLinkInd", "supressDocLinkInd_YES", 
"", TradePortalConstants.INDICATOR_YES, 
supressDocLinkInd.equals(TradePortalConstants.INDICATOR_YES), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("supressDocLinkInd", "supressDocLinkInd_NO", 
"", TradePortalConstants.INDICATOR_NO, 
(supressDocLinkInd.equals("") ||supressDocLinkInd.equals(TradePortalConstants.INDICATOR_NO)), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.AllowPayByAnotherAccnt", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowPaymentByAnotherAccount", "allowPaymentByAnotherAccount_YES", 
"", TradePortalConstants.INDICATOR_YES, 
allowPaymentByAnotherAccount.equals(TradePortalConstants.INDICATOR_YES), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowPaymentByAnotherAccount", "allowPaymentByAnotherAccount_NO", 
"", TradePortalConstants.INDICATOR_NO, 
(allowPaymentByAnotherAccount.equals("")||allowPaymentByAnotherAccount.equals(TradePortalConstants.INDICATOR_NO)), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.AllowLiveMarketRates", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowLiveMarketRates", "allowLiveMarketRates_YES", 
"", TradePortalConstants.INDICATOR_YES, 
allowLiveMarketRates.equals(TradePortalConstants.INDICATOR_YES), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowLiveMarketRates", "TallowLiveMarketRates_NO", 
"", TradePortalConstants.INDICATOR_NO, 
(allowLiveMarketRates.equals("")||allowLiveMarketRates.equals(TradePortalConstants.INDICATOR_NO)), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.OverrideSwiftLengthIndYesChoiceText", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("overrideSwiftLengthInd", "overrideSwiftLengthInd_YES", 
"", TradePortalConstants.INDICATOR_YES, 
									  overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_YES), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("overrideSwiftLengthInd", "overrideSwiftLengthInd_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  (overrideSwiftLengthInd.equals("")||overrideSwiftLengthInd.equals(TradePortalConstants.INDICATOR_NO)), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.OverrideSwiftValidationIndYesChoiceText", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("overrideSwiftValidationInd", "overrideSwiftValidationInd_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(overrideSwiftValidationInd), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("overrideSwiftValidationInd", "overrideSwiftValidationInd_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  (StringFunction.isBlank(overrideSwiftValidationInd) ||TradePortalConstants.INDICATOR_NO.equals(overrideSwiftValidationInd)), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.AllowOnlyISO8859Chars", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowOnlyISO8859CharInd", "allowOnlyISO8859CharInd_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(allowOnlyISO8859CharInd), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowOnlyISO8859CharInd", "allowOnlyISO8859CharInd_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  (StringFunction.isBlank(allowOnlyISO8859CharInd) ||TradePortalConstants.INDICATOR_NO.equals(allowOnlyISO8859CharInd)), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.AllowMobileBankingforCorpCustomers", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("MobileBankingInd", "cbMobileBankingInd_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(cbMobileBankingAccessInd), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("MobileBankingInd", "cbMobileBankingInd_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  TradePortalConstants.INDICATOR_NO.equals(cbMobileBankingAccessInd), readOnly, "", "") %>
				</td>
			</tr>
			
			<%// jgadela R8.4 CR-854 T36000024797 [START]- Added new indicator for reporting language option %>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.ReportingLangOptionInd", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("reportingLangOptionInd", "reportingLangOptionInd_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(reportingLangOptionInd), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("reportingLangOptionInd", "reportingLangOptionInd_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  TradePortalConstants.INDICATOR_NO.equals(reportingLangOptionInd), readOnly, "", "") %>
				</td>
			</tr>
						<%// jgadela R8.4 CR-854 T36000024797 [END]%>
						
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.PreDebitOptionInd", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("preDebitOptionInd", "preDebitOptionInd_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(preDebitOptionInd), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("preDebitOptionInd", "preDebitOptionInd_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  TradePortalConstants.INDICATOR_NO.equals(preDebitOptionInd), readOnly, "", "") %>
				</td>
			</tr>
			<%//SSikhakolli Rel 9.3.5 CR-1029 Adding new row %>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.EnableAdministrativeUpdateCentre", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("enableAdminUpdateCentre", "enableAdminUpdateCentre_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(enableAdminUpdateCentre), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("enableAdminUpdateCentre", "enableAdminUpdateCentre_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  TradePortalConstants.INDICATOR_NO.equals(enableAdminUpdateCentre), readOnly, "", "") %>
				</td>
			</tr>
			<%-- Nar Rel9500 CR-1132 Add- Begin --%>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.UtilizeAddtionBankField", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("utilizeAdditionalBankFields", "utilizeAdditionalBankFields_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(utilizeAdditionalBankFields), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("utilizeAdditionalBankFields", "utilizeAdditionalBankFields_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  ("".equals(utilizeAdditionalBankFields) || TradePortalConstants.INDICATOR_NO.equals(utilizeAdditionalBankFields)), readOnly, "", "") %>
				</td>
			</tr>
			<%-- Nar Rel9500 CR-1132 Add- End --%>
			<%-- Rel9500 CR-927B Add- Begin --%>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.ControlOfNotifRuleByAdmin", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("controlOfNotifRulesByAdmin", "controlOfNotifRulesByAdmin_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  TradePortalConstants.INDICATOR_YES.equals(controlOfNotifRulesbyAdmin), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("controlOfNotifRulesByAdmin", "controlOfNotifRulesByAdmin_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  ("".equals(controlOfNotifRulesbyAdmin) || TradePortalConstants.INDICATOR_NO.equals(controlOfNotifRulesbyAdmin)), readOnly, "", "") %>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.AllowFilteringofInstruments", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowFilteringofInstruments", "allowFilteringofInstruments_YES", 
									  "", TradePortalConstants.INDICATOR_YES, 
									  ("".equals(allowFilteringofInstruments) || TradePortalConstants.INDICATOR_YES.equals(allowFilteringofInstruments)), readOnly, "", "") %>
				</td>
				<td align="center"><%= widgetFactory.createRadioButtonField("allowFilteringofInstruments", "allowFilteringofInstruments_NO", 
									  "", TradePortalConstants.INDICATOR_NO, 
									  TradePortalConstants.INDICATOR_NO.equals(allowFilteringofInstruments), readOnly, "", "") %>
				</td>
			</tr>
			<%-- Rel9500 CR-927B Add- End --%>
		</table>
	</div>
</div>
<%-- end of titlepane Options--%>

<%
/**==========================
*======== Admin User Authentication =========
*==========================*/

String authMethod = editClient.getAttribute("authentication_method");
%>
<%= widgetFactory.createSectionHeader("5", "ClientBankDetail.AdminUserAuthentication") %>
<%= widgetFactory.createLabel("", "ClientBankDetail.SpecifyAdminAuthenticationMethod", false, false, false, "") %>

<div class="formItem">
	<%= widgetFactory.createRadioButtonField("authenticationMethod", "authenticationMethod_AUTH_CERTIFICATE", 
			  "ClientBankDetail.UseCertificatesForAuthentication", TradePortalConstants.AUTH_CERTIFICATE, 
			  authMethod.equals(TradePortalConstants.AUTH_CERTIFICATE), readOnly, "", "") %>
	<br />
	<%= widgetFactory.createRadioButtonField("authenticationMethod", "authenticationMethod_AUTH_2FA", 
			  "ClientBankDetail.Use2FAForAuthentication", TradePortalConstants.AUTH_2FA, 
			  authMethod.equals(TradePortalConstants.AUTH_2FA), readOnly, "", "") %>
	<br />
	<%= widgetFactory.createRadioButtonField("authenticationMethod", "authenticationMethod_AUTH_SSO", 
			  "ClientBankDetail.UseSSOForAuthentication", TradePortalConstants.AUTH_SSO, 
			  authMethod.equals(TradePortalConstants.AUTH_SSO), readOnly, "", "") %>
	<br />

	<%          
if(isNew){
%>
	<%= widgetFactory.createRadioButtonField("authenticationMethod", "authenticationMethod_AUTH_PASSWORD", 
			  "ClientBankDetail.UsePasswordsForAuthentication", TradePortalConstants.AUTH_PASSWORD, 
			  (authMethod.equals("")||authMethod.equals(TradePortalConstants.AUTH_PASSWORD)), readOnly, "", "") %>
	<%
}else{
%>
	<%= widgetFactory.createRadioButtonField("authenticationMethod", "authenticationMethod_AUTH_PASSWORD", 
			  "ClientBankDetail.UsePasswordsForAuthentication", TradePortalConstants.AUTH_PASSWORD, 
			  authMethod.equals(TradePortalConstants.AUTH_PASSWORD), readOnly, "", "") %>
	<%
}
%>
    <br />
    <%-- Nar CR-1071 Rel9350 -  Add option Individual Configuration for Admin user. --%>
	<%= widgetFactory.createRadioButtonField("authenticationMethod", "authenticationMethod_AUTH_PERUSER", 
			  "ClientBankDetail.ConfiguredForEither", TradePortalConstants.AUTH_PERUSER, 
			  authMethod.equals(TradePortalConstants.AUTH_PERUSER), readOnly, "", "") %>
    <br />
    <div class="formItemWithIndent3 inline">
      <%= widgetFactory.createSubLabel("ClientBankDetail.PwdOrIndividualSelection") %>
    </div>
</div>
<div class="formItemWithIndent4 inline">

	<%= widgetFactory.createLabel("", "ClientBankDetail.MustCreatePasswords", true, false, false, "formItem inline") %>
	<%= widgetFactory.createNumberField("bankPasswordLength", "", editClient.getAttribute("bank_min_password_len"), "2", 
			  readOnly, false, false, "", "constraints:{ min:8, max:32 }","none") %>

	<%=resMgr.getText(" ("+SecurityRules.getBankPasswordMinimumLengthLimit()+"", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.CharactersinLengthMinimumIs", TradePortalConstants.TEXT_BUNDLE)%>


</div>
<div style="clear: both;"></div>
<div class="formItemWithIndent4 inline">
	<%= widgetFactory.createLabel("", "ClientBankDetail.CanHaveaMaximum", true, false, false, "formItem inline") %>
	<%= widgetFactory.createNumberField("bankFailedAttempts", "", editClient.getAttribute("bank_max_failed_attempts"), "3", 
			  readOnly, false, false, "", "constraints:{ min:1, max:300 }","none") %>
	<%//= widgetFactory.createSubLabel(" ("+SecurityRules.getBankFailedLoginAttemptsLimit()+" ") %>
	<%=resMgr.getText(" (300 ", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.FailedLoginAttempts", TradePortalConstants.TEXT_BUNDLE)%>

</div>
<div style="clear: both;"></div>
<div class="formItemWithIndent4 inline">
	<%= widgetFactory.createLabel("", "ClientBankDetail.MustChangePassword", true, false, false, "formItem inline") %>
	<%= widgetFactory.createNumberField("bankChangePassword", "", editClient.getAttribute("bank_change_password_days"), "3", 
			  readOnly, false, false, "", "constraints:{ min:1, max:180 }","none") %>
	<%//= widgetFactory.createSubLabel(" ("+SecurityRules.getBankChangePasswordDaysLimit()+" ") %>
	<%=resMgr.getText(" (180", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.DaysMaximumis", TradePortalConstants.TEXT_BUNDLE)%>
</div>
<div style="clear: both;"></div>
<div class="formItemWithIndent1">
	<%= widgetFactory.createCheckboxField("bankPasswordAddlCriteria", "",
	  editClient.getAttribute("bank_password_addl_criteria").equals(TradePortalConstants.INDICATOR_YES), readOnly,false,"","","none") %><%=resMgr.getText("ClientBankDetail.PasswordsMustMeet",TradePortalConstants.TEXT_BUNDLE) %>
</div>
<div class="formItemWithIndent1 inline">
	<%= widgetFactory.createCheckboxField("bankPasswordHistoryCheckbox", "ClientBankDetail.NewPasswordsDifferent1",
			  editClient.getAttribute("bank_password_history").equals(TradePortalConstants.INDICATOR_YES), readOnly, 
			  false, "", "", "none") %>
	<%= widgetFactory.createNumberField("bankPasswordHistoryCount", "", editClient.getAttribute("bank_password_history_count"), "2", 
			  readOnly, false, false, "", "","none") %>
	<%=resMgr.getText("ClientBankDetail.NewPasswordsDifferent2", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("(99 ", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.NewPasswordsDifferent3", TradePortalConstants.TEXT_BUNDLE)%>

</div>
<div style="clear: both;"></div>
</div>
<%-- end of titlepane Admin User Authentication--%>

<%
/**==========================
*======== Corporate User Authentication =========
*==========================*/

String corpAuthMethod = editClient.getAttribute("corp_auth_method");
%>
<%= widgetFactory.createSectionHeader("6", "ClientBankDetail.CorporateUserAuthentication") %>
<%= widgetFactory.createLabel("", "ClientBankDetail.SpecifyCorpCustAuthenticationMethod", false, false, false, "") %>

<div class="formItem">
	<%= widgetFactory.createRadioButtonField("corpAuthMethod", "corpAuthMethod_AUTH_CERTIFICATE", 
			  "ClientBankDetail.CorporateCustomersUseCertificates", TradePortalConstants.AUTH_CERTIFICATE, 
			  corpAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE), readOnly, "", "") %>
	<br />
	<%= widgetFactory.createRadioButtonField("corpAuthMethod", "corpAuthMethod_AUTH_2FA", 
			  "ClientBankDetail.CorporateCustomersUse2FA", TradePortalConstants.AUTH_2FA, 
			  corpAuthMethod.equals(TradePortalConstants.AUTH_2FA), readOnly, "", "") %>
	<br />
	<%= widgetFactory.createRadioButtonField("corpAuthMethod", "corpAuthMethod_AUTH_SSO", 
			  "ClientBankDetail.CorporateCustomersUseSSO", TradePortalConstants.AUTH_SSO, 
			  corpAuthMethod.equals(TradePortalConstants.AUTH_SSO), readOnly, "", "") %>
	<br />
	<%= widgetFactory.createRadioButtonField("corpAuthMethod", "corpAuthMethod_AUTH_REGISTERED_SSO", 
			  "ClientBankDetail.CorporateCustomersUseSSOByRegistration", TradePortalConstants.AUTH_REGISTERED_SSO, 
			  TradePortalConstants.AUTH_REGISTERED_SSO.equals(corpAuthMethod), readOnly, "", "") %>
</div>
<div class="formItemWithIndent4">
	<%= widgetFactory.createTextArea("registrationHeader", "ClientBankDetail.RegistrationHeader", 
	  editClient.getAttribute("registration_header"), readOnly, false, false, "", "", "formItem") %>
	<%= widgetFactory.createTextArea("registrationText", "ClientBankDetail.RegistrationText", 
	  editClient.getAttribute("registration_text"), readOnly, false, false, "", "", "formItem") %>
</div>
<br />

<div class="formItem nowrap">
	<%= widgetFactory.createRadioButtonField("corpAuthMethod", "corpAuthMethod_AUTH_PERUSER", 
			  "ClientBankDetail.SpecifiedForindividualCorporateCustomer", TradePortalConstants.AUTH_PERUSER, 
			  corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER), readOnly, "", "") %>
</div>
<div style="clear: both;"></div>

<div class="formItemWithIndent4 inline">
	<%= widgetFactory.createLabel("", "ClientBankDetail.MustCreatePasswords", true, false, false, "formItem inline") %>
	<%= widgetFactory.createNumberField("corpPasswordLength", "", editClient.getAttribute("corp_min_password_length"), "2", 
			  readOnly, false, false, "", "constraints:{ min:8, max:32 }","none") %>
	<%=resMgr.getText(" ("+SecurityRules.getCorporatePasswordMinimumLengthLimit()+" ", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.CharactersinLengthMinimumIs", TradePortalConstants.TEXT_BUNDLE)%>

</div>
<div style="clear: both;"></div>
<div class="formItemWithIndent4 inline">
	<%= widgetFactory.createLabel("", "ClientBankDetail.CanHaveaMaximum", true, false, false, "formItem inline") %>
	<%= widgetFactory.createNumberField("corpFailedAttempts", "", editClient.getAttribute("corp_max_failed_attempts"), "3", 
			  readOnly, false, false, "", "constraints:{ min:1, max:300 }","none") %>
	<%//= widgetFactory.createSubLabel(" ("+SecurityRules.getCorporateFailedLoginAttemptsLimit()+" ") %>
	<%=resMgr.getText(" (300 ", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.FailedLoginAttempts", TradePortalConstants.TEXT_BUNDLE)%>

</div>
<div style="clear: both;"></div>
<div class="formItemWithIndent4 inline">
	<%= widgetFactory.createLabel("", "ClientBankDetail.MustChangePassword", true, false, false, "formItem inline") %>
	<%= widgetFactory.createNumberField("corpChangePassword", "", editClient.getAttribute("corp_change_password_days"), "3", 
			  readOnly, false, false, "", "constraints:{ min:1, max:180 }","none") %>
	<%//= widgetFactory.createSubLabel(" ("+SecurityRules.getCorporateChangePasswordDaysLimit()+" ") %>
	<%=resMgr.getText(" (180 ", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.DaysMaximumis", TradePortalConstants.TEXT_BUNDLE)%>

</div>
<div style="clear: both;"></div>
<div class="formItemWithIndent1">
	<%= widgetFactory.createCheckboxField("corpPasswordAddlCriteria", "",
	  editClient.getAttribute("corp_password_addl_criteria").equals(TradePortalConstants.INDICATOR_YES), readOnly,false,"","","none") %><%=resMgr.getText("ClientBankDetail.PasswordsMustMeet",TradePortalConstants.TEXT_BUNDLE) %>
</div>
<div class="formItemWithIndent1 inline">
	<%= widgetFactory.createCheckboxField("corpPasswordHistoryCheckbox", "ClientBankDetail.NewPasswordsDifferent1",
			  editClient.getAttribute("corp_password_history").equals(TradePortalConstants.INDICATOR_YES), readOnly, 
			  false, "", "", "none") %>

	<%= widgetFactory.createNumberField("corpPasswordHistoryCount", "", editClient.getAttribute("corp_password_history_count"), "2", 
			  readOnly, false, false, "", "","none") %>
	<%=resMgr.getText("ClientBankDetail.NewPasswordsDifferent2", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText(" (99 ", TradePortalConstants.TEXT_BUNDLE)%>
	<%=resMgr.getText("ClientBankDetail.NewPasswordsDifferent3", TradePortalConstants.TEXT_BUNDLE)%>

</div>
<div style="clear: both;"></div>
</div>
<%-- end of titlepane Corporate User Authentication--%>

<%
/**==========================
*======== User Security Maintenance Setting =========
*==========================*/
%>
<%= widgetFactory.createSectionHeader("7", "ClientBankDetail.UserSecurityMaintSetting") %>
<div class="formItem">
	<%    
String canEditOwnProfileInd = null;
canEditOwnProfileInd = editClient.getAttribute("can_edit_own_profile_ind");
//out.println(canEditOwnProfileInd);

if ((!isNew) && (TradePortalConstants.INDICATOR_YES.equals(canEditOwnProfileInd))){
defaultCheckBoxValue = true;
}else{
	    defaultCheckBoxValue = false;
}
%>
	<%= widgetFactory.createCheckboxField("canEditOwnProfile", "", 
	  editClient.getAttribute("can_edit_own_profile_ind").equals(TradePortalConstants.INDICATOR_YES),readOnly,false,"","","none") %><%=resMgr.getText("ClientBankDetail.canEditOwnProfile", TradePortalConstants.TEXT_BUNDLE)%>
	<br />
	<br />
	<%    
String canEditIDInd = null;
canEditIDInd = editClient.getAttribute("can_edit_id_ind");

if ((!isNew) && (TradePortalConstants.INDICATOR_YES.equals(canEditIDInd))){
defaultCheckBoxValue = true;
}else{
	    defaultCheckBoxValue = false;
} 
%>
	<%= widgetFactory.createCheckboxField("canEditID", "", 
	  editClient.getAttribute("can_edit_id_ind").equals(TradePortalConstants.INDICATOR_YES),readOnly,false,"","","none") %><%=resMgr.getText("ClientBankDetail.canEditID", TradePortalConstants.TEXT_BUNDLE)%>
	<br />
	<br />
	<%    
String canAssociateTemplateGrps = null;
canAssociateTemplateGrps = editClient.getAttribute("template_groups_ind");

if ((!isNew) && (TradePortalConstants.INDICATOR_YES.equals(canAssociateTemplateGrps))){
defaultCheckBoxValue = true;
}else{
	    defaultCheckBoxValue = false;
}
%>
	<%= widgetFactory.createCheckboxField("allowTemplateGroups", "", 
editClient.getAttribute("template_groups_ind").equals(TradePortalConstants.INDICATOR_YES),readOnly,false,"","","none") %><%=resMgr.getText("ClientBankDetail.allowTemplateGroups", TradePortalConstants.TEXT_BUNDLE)%>
	<br />
	<br />
	<%    
String canReportCategs = null;
canReportCategs = editClient.getAttribute("report_categories_ind");

if ((!isNew) && (TradePortalConstants.INDICATOR_YES.equals(canReportCategs))){
defaultCheckBoxValue = true;
}else{
	    defaultCheckBoxValue = false;
}
%>
	<%= widgetFactory.createCheckboxField("allowReportCategories", "", 
editClient.getAttribute("report_categories_ind").equals(TradePortalConstants.INDICATOR_YES),readOnly,false,"","","none") %><%=resMgr.getText("ClientBankDetail.allowReportCategories", TradePortalConstants.TEXT_BUNDLE)%>
	<br>
</div>
</div>
<%-- end of titlepane User Security Maintenance Setting--%>

<%
/**==========================
*======== Innstrument Authentication Details =========
*==========================*/
%>
<%= widgetFactory.createSectionHeader("8", "ClientBankDetail.InstrumentAuthDetails") %>
<%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthText", false, false, false, "") %>

<% 
/**
*-----------------------------------
*=========== Trade ===========
*-----------------------------------
*/
%>
<div class="formItem">
	<table width="100%" border="1" cellspacing="0" cellpadding="0"
		class="formDocumentsTable">
		<thead>
			<th width="30%"><%= widgetFactory.createLabel("", "        ", false, false, false, "none") %>&nbsp;
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthEXP_COL-ISS", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthEXP_DLC-TRN", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthATP-AMD", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthEXP_DLC-ATR", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthAIR-REL", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthEXP_DLC-ASN", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthATP-APR", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%= resMgr.getText("ClientBankDetail.InstrumentAuthIMP_DLC-DCR", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%= resMgr.getText("ClientBankDetail.InstrumentAuthSettleInstr", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
		</thead>
		<tr class="tableSubHeader">
			<td colspan="10" align="left"><b> <%=       resMgr.getText("common.trade", TradePortalConstants.TEXT_BUNDLE) %></b>
			</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthAIR", false, false, false, "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthAirRel", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__AIR_REL),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthATP", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthAtpIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__ATP_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthAtpAmd", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__ATP_AMD),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthAtpApr", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__ATP_APR),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthEXP_COL", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpColIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_COL_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpColAmd", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_COL_AMD),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthEXP_OCO", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpOcoIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_OCO_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpOcoAmd", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_OCO_AMD),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthEXP_DLC", false, false, false, "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpDlcTrn", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_TRN),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpDlcAtr", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ATR),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpDlcAsn", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_ASN),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthExpDlcDcr", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__EXP_DLC_DCR),
									  readOnly, false, "", "", "none") %></td>
		    <td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthIMP_DLC", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthImpDlcIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMP_DLC_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthImpDlcAmd", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMP_DLC_AMD),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthImpDlcDcr", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMP_DLC_DCR),
									  readOnly, false, "", "", "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthImpDlcSit", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMP_DLC_SIT),
									  readOnly, false, "", "", "none") %></td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "SecondaryNavigation.Instruments.ImportCollection", false, false, false, "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthImcSit", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__IMC_SIT),
									  readOnly, false, "", "", "none") %></td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthINC_SLC", false, false, false, "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthIncSlcDcr", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__INC_SLC_DCR),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthLRQ", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthLrqIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__LRQ_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthLrqSit", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__LRQ_SIT),
									  readOnly, false, "", "", "none") %></td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthGUAR", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthGuarIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__GUAR_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthGuarAmd", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__GUAR_AMD),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthSLC", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthSlcIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SLC_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthSlcAmd", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SLC_AMD),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthSlcDcr", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SLC_DCR),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthRQA", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthRqaIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RQA_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthRqaAmd", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RQA_AMD),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthRqaDcr", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RQA_DCR),
									  readOnly, false, "", "", "none") %></td>
		    <td>&nbsp;</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthSHP", false, false, false, "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthShpIss", "", 
									  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__SHP_ISS),
									  readOnly, false, "", "", "none") %></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
<%-- end of Trade table div --%>

<% 
/**
*-----------------------------------
*=========== Payment ===========
*-----------------------------------
*/
%>
<div class="formItem">
	<div class="columnLeft">
		<table width="100%" border="1" cellspacing="0" cellpadding="0"
			class="formDocumentsTable">
			<tr>
				<th>&nbsp;</th>
				<th><b> <%=                resMgr.getText("ClientBankDetail.InstrumentAuthATP-ISS", TradePortalConstants.TEXT_BUNDLE) %></b>

				</th>
			</tr>
			<tr class="tableSubHeader">
				<td colspan="2"><b> <%=                resMgr.getText("common.payment", TradePortalConstants.TEXT_BUNDLE) %></b>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthFTRQ", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthFtrqIss", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__FTRQ_ISS),
											  readOnly, false, "", "", "none") %></td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthFTDP", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthFtdpIss", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__FTDP_ISS),
											  readOnly, false, "", "", "none") %></td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthFTBA", false, false, false, "none") %>
				</td>
				<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthFtbaIss", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__FTBA_ISS),
											  readOnly, false, "", "", "none") %></td>
			</tr>
		</table>
	</div>
	<%-- end of Payment table div --%>

	<% 
	  /**
	    *-----------------------------------
	    *=========== Direct Debit Instructions ===========
	    *-----------------------------------
	    */
%>
	<div class="columnRight">
		<table width="100%" border="1" cellspacing="0" cellpadding="0"
			class="formDocumentsTable">
			<tr>
				<th width="60%">&nbsp;</th>
				<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthATP-ISS", TradePortalConstants.TEXT_BUNDLE) %></b>
				</th>
			</tr>
			<tr class="tableSubHeader">
				<td colspan="2"><b> <%= resMgr.getText("ClientBankDetail.InstrumentAuthDDI", TradePortalConstants.TEXT_BUNDLE) %></b>
				</td>
			</tr>
			<tr>
				<td><%= widgetFactory.createLabel("", "ClientBankDetail.DirectDebit", false, false, false, "none") %>

				</td>
				<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthDdiIss", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__DDI_ISS),
											  readOnly, false, "", "", "none") %></td>
			</tr>

		</table>
		<%-- end of Direct Debit Instructions table div --%>
		<br />
	</div>
	<div style="clear: both;"></div>

</div>

<div class="formItem">
<% 
      /**
        *-----------------------------------
        *=========== Rel 9.0 CR 913 START===========
        *-----------------------------------
        */
      %>
        <table width="100%" border="1" cellspacing="0" cellpadding="0"
            class="formDocumentsTable">
            <tr>
                <th width="20%">&nbsp;</th>
                <th width="20%"><b> <%=resMgr.getText("ClientBankDetail.FinanceInvoice", TradePortalConstants.TEXT_BUNDLE) %></b>
                </th>
                <th width="20%"><b> <%=resMgr.getText("ClientBankDetail.ProcessInvoice", TradePortalConstants.TEXT_BUNDLE) %></b>
                </th>
                <th width="50%">&nbsp;</th>
            </tr>
            <tr class="tableSubHeader">
                <td colspan="4"><b> <%= resMgr.getText("common.UploadedInvoices", TradePortalConstants.TEXT_BUNDLE) %></b>
                </td>
            </tr>
            <tr>
                <td width="20%"><%= widgetFactory.createLabel("", "ClientBankDetail.RecInv", false, false, false, "none") %>

                </td>
                <td align="center" width="20%"><%= widgetFactory.createCheckboxField("instrumentAuthInvoiceMgntFinance", "", 
                                              InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__INVOICE_MGNT_FIN_INVOICE),
                                              readOnly, false, "", "", "none") %></td>
                <td width="20%">&nbsp;</td>    
                <td width="50%">&nbsp;</td>                                           
            </tr>
            <tr>
                <td width="20%"><%= widgetFactory.createLabel("", "ClientBankDetail.PayInv", false, false, false, "none") %>

                </td>
                <td width="20%">&nbsp;</td>
                <td align="center" width="20%"><%= widgetFactory.createCheckboxField("instrAuthInvoiceProcessFinance", "", 
                                              InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__PAYABLES_UPLOAD_INV_PROCESS),
                                              readOnly, false, "", "", "none") %></td>
                <td width="50%">&nbsp;</td>                                               
            </tr>

        </table>
        <%-- end of Invoice Management table --%>
</div>
<% 
      /**
        *-----------------------------------
        *=========== Rel 9.0 CR 913 END===========
        *-----------------------------------
        */
      %>


<% 
/**
*-----------------------------------
*=========== Receivables ===========
*-----------------------------------
*/
%>
<div class="formItem">
	<table width="100%" border="1" cellspacing="0" cellpadding="0"
		class="formDocumentsTable">
		<tr>
			<th>&nbsp;</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthReceivablesMatchResponse", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthReceivablesApproveDiscount", TradePortalConstants.TEXT_BUNDLE) %></b>
			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthReceivablesCloseInvoice", TradePortalConstants.TEXT_BUNDLE) %></b>

			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthReceivablesFinInvoice", TradePortalConstants.TEXT_BUNDLE) %></b>

			</th>
			<th><b> <%=resMgr.getText("ClientBankDetail.InstrumentAuthReceivablesDisputeInvoice", TradePortalConstants.TEXT_BUNDLE) %></b>

			</th>
		</tr>
		<tr class="tableSubHeader">
			<td colspan="6"><b> <%= resMgr.getText("common.receivables", TradePortalConstants.TEXT_BUNDLE) %></b>

			</td>
		</tr>
		<tr>
			<td><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthReceivables", false, false, false, "none") %>
			</td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthReceivablesMatchResponse", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_MATCH_RESPONSE),
											  readOnly, false, "", "", "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthReceivablesApproveDiscount", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_APPROVE_DISCOUNT),
											  readOnly, false, "", "", "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthReceivablesCloseInvoice", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_CLOSE_INVOICE),
											  readOnly, false, "", "", "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthReceivablesFinInvoice", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_FIN_INVOICE),
											  readOnly, false, "", "", "none") %></td>
			<td align="center"><%= widgetFactory.createCheckboxField("instrumentAuthReceivablesDisputeInvoice", "", 
											  InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__RECEIVABLES_DISPUTE_INVOICE),
											  readOnly, false, "", "", "none") %></td>
		</tr>
	</table>
</div>
<%-- end of Receivables table div --%>
<% 
/**
* Rel 9.0 CR 913 START
*-----------------------------------
*=========== Payables ===========
*-----------------------------------
*/
%>
<div class="formItem">
    <table width="100%" border="1" cellspacing="0" cellpadding="0"
        class="formDocumentsTable">
        <tr>
            <th width="20%">&nbsp;</th>
            <th width="27%"><b> <%=resMgr.getText("ClientBankDetail.InstrAuthPayablesManagementUpdates", TradePortalConstants.TEXT_BUNDLE) %></b>
            </th>
            <th width="53%">&nbsp;</th>
        </tr>
        <tr class="tableSubHeader">
            <td colspan="3"><b> <%= resMgr.getText("common.payables", TradePortalConstants.TEXT_BUNDLE) %></b>

            </td>
        </tr>
        <tr>
            <td width="20%"><%= widgetFactory.createLabel("", "ClientBankDetail.InstrumentAuthPayables", false, false, false, "none") %>
            </td>
            <td align="center" width="27%"><%= widgetFactory.createCheckboxField("instrAuthPayablesMgmtUpdates", "", 
                                              InstrumentAuthentication.requireTransactionAuthentication(requireTranAuth, InstrumentAuthentication.TRAN_AUTH__PAYABLES_MGMT_INV_UPDATES),
                                              readOnly, false, "", "", "none") %></td>
            <td width="53%">&nbsp;</td>                                              
        </tr>
    </table>
</div>
<%-- end of Payables table div --%>
<%--  Rel 9.0 CR 913 END --%>
</div>
<%-- end of titlepane Innstrument Authentication Details--%>

<%
/**==========================
*======== Overall Instrument ID Range =========
*==========================*/

String lastInstrIdUsed = (instrNumSeq != null) ? instrNumSeq.getAttribute("last_instrument_id_used") : "";
%>
<%= widgetFactory.createSectionHeader("9", "ClientBankDetail.InstrumentIDRange") %>
<div>
	<%= widgetFactory.createNumberField("startsAt", "ClientBankDetail.StartsAt", editClient.getAttribute("instr_id_range_start"), "16", readOnly, true, false, "", "constraints: {pattern: '###############'}","inline") %>
	<%= widgetFactory.createNumberField("endsAt", "ClientBankDetail.EndsAt", editClient.getAttribute("instr_id_range_end"), "16",  readOnly, true, false, "", "constraints: {pattern: '###############'}","inline") %>
	<%= widgetFactory.createTextField("LastInstrumentIDUsed", "ClientBankDetail.LastInstrumentIDUsed", lastInstrIdUsed, "50", true, false, false, "", "","inline") %>
	<div style="celar: both;"></div>
</div>
<br>   
</div><%-- end of titlepane Overall Instrument ID Range--%>

<%-- CR ANZ 501 Rel 8.3 03-24-2013 jgadela  Beign--%>
<%= widgetFactory.createSectionHeader("10", "ClientBankDetail.BankGroupsAuthorisationBlock") %>
<div>
<table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 25%;">
      <col span="1" style="width: 25%;">
      <col span="1" style="width: 25%;">
    </colgroup>
    <thead>
        <th align="left"></th>
        <th align="left"><%=resMgr.getText("common.Yes",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="left"><%=resMgr.getText("common.No",TradePortalConstants.TEXT_BUNDLE)%></th>
      </tr>
    </thead>
  <tbody>
  
  <%
  boolean defaultRadioButtonValue1 = false;
  boolean defaultRadioButtonValue2 = true;
  // 09/19/2014  R 9.1 IR T36000032331    - Corrected the logic to keep entered values after validation fails
  if (corpOrgDualCtrlReqdInd != null )
  {
     	if (TradePortalConstants.INDICATOR_YES.equals(corpOrgDualCtrlReqdInd)){
            defaultRadioButtonValue1 = true;
            defaultRadioButtonValue2 = false;
         }
    	 	else if (TradePortalConstants.INDICATOR_NO.equals(corpOrgDualCtrlReqdInd)){
            defaultRadioButtonValue1 = false;
            defaultRadioButtonValue2 = true;
        }
   }
   else
   {
 	   defaultRadioButtonValue1 = false;
       defaultRadioButtonValue2 = true;
   }
%>

<tr class="tableSubHeader"> 
			  <td colspan="3">
					  <b>        <%= resMgr.getText("ClientBankDetail.BankGroupAdminUsersRequireDualControl", TradePortalConstants.TEXT_BUNDLE) %></b>

			  </td>
	  </tr>
    <tr>
      <td>
            <%= widgetFactory.createSubLabel("AdminCorpCust.TabName") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("corpOrgDualCtrlReqdInd",
                "corpOrgDualCtrlReqdInd" + "Yes", "", TradePortalConstants.INDICATOR_YES, defaultRadioButtonValue1,readOnly, "", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("corpOrgDualCtrlReqdInd",
                "corpOrgDualCtrlReqdInd" + "No", "", TradePortalConstants.INDICATOR_NO, defaultRadioButtonValue2, readOnly,"", ""));
        %>
      </td>
      </tr>
      <%
   		// 09/19/2014  R 9.1 IR T36000032331    - Corrected the logic to keep entered values after validation fails
		if (adminUserDualCtrlReqdInd != null )
    	{
          	if (TradePortalConstants.INDICATOR_YES.equals(adminUserDualCtrlReqdInd)){
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
              }
         	 	else if (TradePortalConstants.INDICATOR_NO.equals(adminUserDualCtrlReqdInd)){
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
             }
         }
         else
         {
      	   defaultRadioButtonValue1 = false;
            defaultRadioButtonValue2 = true;
		}
%>
   <tr class="tableSubHeader"> 
			  <td colspan="3">
					  <b>        <%= resMgr.getText("ClientBankDetail.AdminRequireDualControl", TradePortalConstants.TEXT_BUNDLE) %></b>

			  </td>
	  </tr>
	  
    <tr>
      <td>
            <%= widgetFactory.createSubLabel("AdminSecProfileDetail.AdminUsers") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("adminUserDualCtrlReqdInd",
                "adminUserDualCtrlReqdInd" + "Yes", "", TradePortalConstants.INDICATOR_YES, defaultRadioButtonValue1,readOnly, "", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("adminUserDualCtrlReqdInd",
                "adminUserDualCtrlReqdInd" + "No", "", TradePortalConstants.INDICATOR_NO, defaultRadioButtonValue2, readOnly,"", ""));
        %>
      </td>
      </tr>
      
       <%
    // 09/19/2014  R 9.1 IR T36000032331    - Corrected the logic to keep entered values after validation fails
	if (corpUserDualCtrlReqdInd != null )
    {
          	 if (TradePortalConstants.INDICATOR_YES.equals(corpUserDualCtrlReqdInd)){
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
              }else if (TradePortalConstants.INDICATOR_NO.equals(corpUserDualCtrlReqdInd)){
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
              }
    }
    else
    {
      	    defaultRadioButtonValue1 = false;
            defaultRadioButtonValue2 = true;
	}
%>
    <tr class="tableSubHeader"> 
			  <td colspan="3">
					  <b>        <%= resMgr.getText("ClientBankDetail.CustomerReferenceDataRequireDualControl", TradePortalConstants.TEXT_BUNDLE) %></b>

			  </td>
	  </tr>
    <tr>
      <td>
            <%= widgetFactory.createSubLabel("ClientBankDetail.CorporateUsers") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("corpUserDualCtrlReqdInd",
                "corpUserDualCtrlReqdInd" + "Yes", "", TradePortalConstants.INDICATOR_YES, defaultRadioButtonValue1,readOnly, "", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("corpUserDualCtrlReqdInd",
                "corpUserDualCtrlReqdInd" + "No", "", TradePortalConstants.INDICATOR_NO, defaultRadioButtonValue2, readOnly,"", ""));
        %>
      </td>
      </tr>
  </tbody>
  </table><div style="celar: both;"></div>
</div>
<br>    
</div><%-- end of titlepane 10 th section--%>
<%-- CR ANZ 501 Rel 8.3 03-24-2013 jgadela  end--%>

<%-- end of titlepane Overall Instrument ID Range--%>
</div>
<%--formContent--%>
</div>
<%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar"
	data-dojo-props="form: 'ClientBankDetail'">
	<jsp:include page="/common/RefDataSidebar.jsp">
		<jsp:param name="showSaveButton" value="<%= showSave %>" />
		<jsp:param name="saveOnClick" value="none" />
		<jsp:param name="showSaveCloseButton" value="<%= showSaveClose %>" />
		<jsp:param name="saveCloseOnClick" value="none" />
		<jsp:param name="showDeleteButton" value="<%= showDelete %>" />
		<jsp:param name="showCloseButton" value="true" />
		<jsp:param name="showHelpButton" value="false" />
		<jsp:param name="cancelAction" value="ClientBankCancel" />
		<jsp:param name="showLinks" value="true" />
		<jsp:param name="buttonPressed" value="<%=buttonClicked%>" />
		<jsp:param name="error" value="<%=error%>" />
	</jsp:include>
</div>
<%--closes sidebar area--%>

<%= formMgr.getFormInstanceAsInputField("ClientBankDetailForm", secureParams) %>
</form>
</div>
<%--closes pageContent area--%>
</div>
<%--closes pageMain area--%>

<jsp:include page="/common/Footer.jsp">
	<jsp:param name="displayFooter"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_NO%>" />
</jsp:include>

<%

	//(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0');document.forms[0].submit(); ";
  // String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0');document.forms[0].submit(); ";%>

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
 

<script type="text/javascript">
		require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
		ready(function(){
		var pwd1box = registry.byId("bankPasswordHistoryCount");
		var pwd2box = registry.byId("corpPasswordHistoryCount");
		var pwd1chbox = registry.byId("bankPasswordHistoryCheckbox");
		var pwd2chbox = registry.byId("corpPasswordHistoryCheckbox");
		on(pwd1box, "change", function(){
		if(pwd1box.value)
		pwd1chbox.set("checked", true);
		else
		pwd1chbox.set("checked", false);
		});
		on(pwd2box, "change", function(){
		if(pwd2box.value)
		pwd2chbox.set("checked", true);
		else
		pwd2chbox.set("checked", false);
		});
		});
		});

 function preSaveAndClose(){
  	 	 
 	  document.forms[0].buttonName.value = "SaveAndClose";
 	  return true;

}



</script>


</body>
</html>

<%
/**********
* CLEAN UP
**********/

beanMgr.unregisterBean("ClientBank");

// Finally, reset the cached document to eliminate carryover of
// information to the next visit of this page.
formMgr.storeInDocCache("default.doc", new DocumentHandler());
Debug.debug("***END********************CLIENT*BANK*DETAIL**************************END***");
%>