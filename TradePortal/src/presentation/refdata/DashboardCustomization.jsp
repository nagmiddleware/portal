<%--
*******************************************************************************
  Dashboard Customization

  Description:

  Provides ability for user to customize their dashboard (i.e. home page).
  The page header shows Dashboard Preferences, but the subheader
  is Dashboard Customization.  Assumption is Dashboard Preferences home page
  can have other pages in the future.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"    	 scope="session"></jsp:useBean>
<jsp:useBean id="condDisplay" class="com.ams.tradeportal.html.ConditionalDisplay" scope="request">
  <jsp:setProperty name="condDisplay" property="userSession" value="<%=userSession%>" />
  <jsp:setProperty name="condDisplay" property="beanMgr" value="<%=beanMgr%>" />
</jsp:useBean>

<%
  Debug.debug("***START******** Dashboard Customization Page ************START***");

  String userOid = userSession.getUserOid();
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  Hashtable secureParms = new Hashtable();
  secureParms.put("UserOid", userSession.getUserOid());
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("login_rights", userSession.getSecurityRights());
  secureParms.put("ownership_level", userSession.getOwnershipLevel());
  secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
  secureParms.put("corp_org_oid", userSession.getOwnerOrgOid());

  // Get the document from the cache.  We'll use it to repopulate the screen if the
  // user was entering data with errors.
  DocumentHandler doc = formMgr.getFromDocCache();
  String buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  Vector error = doc.getFragments("/Error/errorlist/error");

  //clear primary navigation as we are in none
  userSession.setCurrentPrimaryNavigation("");

  com.ams.tradeportal.common.cache.Cache reCertCache = (com.ams.tradeportal.common.cache.Cache)com.ams.tradeportal.common.cache.TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());	
  String isDisplayPreDebitNotify = CBCResult.getAttribute("/ResultSetRecord(0)/PRE_DEBIT_FUNDING_OPTION_IND");
  
  CorporateOrganizationWebBean org1 =null;
  String preDebitEnabledAtCorpLevel = TradePortalConstants.INDICATOR_NO;
  if(TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)) {
     org1 = beanMgr.createBean(CorporateOrganizationWebBean.class,  "CorporateOrganization");
    org1.getById(userSession.getOwnerOrgOid());
    preDebitEnabledAtCorpLevel = org1.getAttribute("pre_debit_funding_option_ind");
  }
  
		    
  String homeLink = formMgr.getLinkAsUrl("goToTradePortalHome", response);
  String focusField = "";

  //get the user's dashboard preferences
  //if no dashboard sections saved, then go with the default

  //determine current user preferences
  List<String> sectionOids = new ArrayList<String>();
  List<String> sectionIds = new ArrayList<String>();
  List<String> sectionOptLocks = new ArrayList<String>();
//jgadela R90 IR T36000026319 - SQL INJECTION FIX
  String dashSql = "select dashboard_section_oid, section_id, opt_lock from dashboard_section "+
  					"where a_user_oid = ? order by display_order";
  DocumentHandler dashListDoc = DatabaseQueryBean.getXmlResultSet(dashSql.toString(), false, new Object[]{userOid}); 
  if ( dashListDoc != null ) {
    Vector dashList = dashListDoc.getFragments("/ResultSetRecord");
    for( int i=0; i<dashList.size(); i++ ) {
      DocumentHandler dashDoc = (DocumentHandler) dashList.get(i);
      String sectionOidAttr = dashDoc.getAttribute("/DASHBOARD_SECTION_OID");     
      sectionOids.add(sectionOidAttr);
      String sectionIdAttr = dashDoc.getAttribute("/SECTION_ID");    
      sectionIds.add(sectionIdAttr);    
      String optLockAttr = dashDoc.getAttribute("/OPT_LOCK");
      sectionOptLocks.add(optLockAttr);
    }
  }
  
  /* Start Vsarkary Rel 9.2 IR#T36000034829 -- commenting out as the IR says if no Dashboard display preferences section is selected it should not display anything on home page
  As these are loaded as default if no  Dashboard display preferences section is selected. Changes not migrated properly doing it again*/
  /* MEer Rel 9.2 BaseCMActivity#T36000036626- Reverting changes made towards IR#T36000034829 and fix the IR#34829 as per Design appropriately.  */
 
  //default if necessary T36000006896 30-Oct-2012 dillip
  if ( sectionIds.size() <= 0 ) {
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
   	 		sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__MAIL);
	  }
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NOTIFICATIONS ) ) {
    		sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS);
	   }
	   //Rel 9.0 - CR 9.3 - vdesingu - start
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DEBIT_FUNDING_MAIL ) 
			   && TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
			   &&  TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel) ) {
    		sectionIds.add(TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL);
	   }
	 //Rel 9.0 - CR 9.3 - vdesingu - End
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERED ) ) {
    		sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED);
	   }
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS ) ) {
    		sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS);
    		//sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS); //CR 821 Rel 8.3
	   }
	   //jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 <START>
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS ) ) {
   			sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS); //CR 821 Rel 8.3
	   }
	   //jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 <END>
	   
	   if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
    		sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES);
	   }
	 //IR 23015 Start - visibility condition should be same as Accounts on menu bar
	   // //Nar IR# T36000011763 should display only if user has CM transaction (Payment and Transaction Between Amount.
	   if ( sectionIds.contains( ConditionalDisplay.DISPLAY_ACCOUNTS ) ) {
    	sectionIds.add(TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES);
	   } 
	   //Ended Here
  } 
  /* MEer Rel 9.2 BaseCMActivity#T36000036626  */
  // End Vsarkary Rel 9.2 IR#T36000034829 

  // a variable to determine whether section should be displayed
  boolean displaySection = false;
%>
<%-- *****************   HTML starts here   ****************** --%>

<jsp:include page="/common/Header.jsp" />

<%--ctq portal refresh begin --%>
<div class="pageMain dashboardPrefs"> <%--include a page specific reference to allow for page specific styling--%>
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="DashboardPreferences.Title" />
      <jsp:param name="helpUrl" value="/customer/dashboard_customisation.htm" />
    </jsp:include>

    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="titleKey" value="DashboardCustomization.Title" />
      <jsp:param name="returnUrl" value="<%= homeLink %>" />
    </jsp:include>

    <form name="DashboardCustomizationForm" id="DashboardCustomizationForm"
          data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">
      <input type=hidden name="buttonName" value="" />

      <%--main content --%>
      <div class="formArea">
        <jsp:include page="/common/ErrorSection.jsp" />

        <div class="formContent formBorder">
          <span class="customizationColumn1">
            <div class="instruction"><%= resMgr.getText("DashboardCustomization.Step1", TradePortalConstants.TEXT_BUNDLE) %></div>
           
	<%--MEer IR-34829 Added NO Sections to Display check box for customization on Dashboard STARTS HERE.--%>
<% 
	boolean defaultNoSectionsValue = false;
	 if ( sectionIds.contains(TradePortalConstants.DASHBOARD_NO_SECTIONS_DISPLAY)){
		 defaultNoSectionsValue = true;
	 }else{		  
		  defaultNoSectionsValue = false;
	 }
%>  
 <div id="noSectionsCheckbox" class="instruction">
   
              <%= widgetFactory.createCheckboxField("displayNoSections", "Dashboard.No.Sections.Display", defaultNoSectionsValue ,
                      false, false, "onChange=\"deselectOtherSections();\"", "", "none") %> 
</div>  
                               
 <%-- MEer IR-34829 Added NO Sections to Display check box for customization on Dashboard ENDS HERE --%>  
 <div class="customizationItemsHeader"><%= resMgr.getText("DashboardCustomization.Sections", TradePortalConstants.TEXT_BUNDLE) %></div>

<%
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MESSAGES ) ) {
%>
            <div data-dojo-type="dijit.TitlePane" id="MessagesSection" class="availItemsSection doNotShowOnSidebar"
                 title='<%= resMgr.getText("DashboardCustomization.Messages", TradePortalConstants.TEXT_BUNDLE) %>'>
<%
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
%>
              <div id="<%=TradePortalConstants.DASHBOARD_SECTION__MAIL%>_avail" class="availItem">
                <span class="availItemSelect">
<%
      displaySection = false;
      if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION__MAIL ) ) {
        displaySection = true;
      }
%>
                  <%= widgetFactory.createCheckboxField(
                        TradePortalConstants.DASHBOARD_SECTION__MAIL+"_checkbox", "", displaySection,
                        false, false, "", "", "none") %>
                </span>
                <span class="availItemDesc">
                  <%= resMgr.getText("DashboardCustomization.MailMessages", TradePortalConstants.TEXT_BUNDLE) %>
                </span>
              </div>
<%
    }
//Rel 9.0 CR 913 vdesingu start
	if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DEBIT_FUNDING_MAIL ) 
			&& TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
			   &&  TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel) ) {
	%>
	              <div id="<%=TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL%>_avail" class="availItem">
	                <span class="availItemSelect">
	<%
	      displaySection = false;
	      if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL ) ) {
	        displaySection = true;
	      }
	%>
	                  <%= widgetFactory.createCheckboxField(
	                        TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL+"_checkbox", "", displaySection,
	                        false, false, "", "", "none") %>
	                </span>
	                <span class="availItemDesc">
	                  <%= resMgr.getText("DashboardCustomization.DebitFundingMailMessages", TradePortalConstants.TEXT_BUNDLE) %>
	                </span>
	              </div>
	<%
	    }
	//Rel 9.0 CR 913 vdesingu End
    if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NOTIFICATIONS ) ) {
%>
              <div id="<%=TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS%>_avail" class="availItem">
                <span class="availItemSelect">
<%
      displaySection = false;
      if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS ) ) {
        displaySection = true;
      }
%>
                  <%= widgetFactory.createCheckboxField(
                        TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS+"_checkbox", "", displaySection,
                        false, false, "", "", "none") %>
                </span>
                <span class="availItemDesc">
                  <%= resMgr.getText("DashboardCustomization.Notifications", TradePortalConstants.TEXT_BUNDLE) %>
                </span>
              </div>
<%
    }    
%>
            </div>
 <%-- Ravindra - CR-708B Start --%>
<%
  }
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERS ) ) {
%>
			  <div data-dojo-type="dijit.TitlePane" id="InvOffersSection" class="availItemsSection doNotShowOnSidebar"
                 title='<%= resMgr.getText("DashboardCustomization.SpInvoicesOffered", TradePortalConstants.TEXT_BUNDLE) %>'>
              <div id="<%=TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED%>_avail" class="availItem">
                <span class="availItemSelect">
<%
    displaySection = false;
    if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED ) ) {
      displaySection = true;
    }
%>     
				<%= widgetFactory.createCheckboxField(
                        TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED+"_checkbox", "", displaySection,
                        false, false, "", "", "none") %>
                </span>
                <span class="availItemDesc">
                  <%= resMgr.getText("DashboardCustomization.SpInvoicesOffered", TradePortalConstants.TEXT_BUNDLE) %>
                </span>
              </div>
            </div>           
<%-- Ravindra - CR-708B End --%>         
<%
  }
	//jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 Refactored the whole trasactions block conditions <START> 
  boolean isTransSectonDiv = false;
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS ) || condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS )) {
	  isTransSectonDiv = true;
%>
            <div data-dojo-type="dijit.TitlePane" id="TransactionSection" class="availItemsSection doNotShowOnSidebar"
                 title='<%= resMgr.getText("DashboardCustomization.Transactions", TradePortalConstants.TEXT_BUNDLE) %>'>
              <div id="<%=TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS%>_avail" class="availItem">
                <span class="availItemSelect">
 <% }             

    displaySection = false;
   		if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS )){
		    if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS ) ) {
		      displaySection = true;
		    }
%>
                <%= widgetFactory.createCheckboxField(
                        TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS+"_checkbox", "", displaySection,
                        false, false, "", "", "none") %>
                </span>
                <span class="availItemDesc">
                  <%= resMgr.getText("DashboardCustomization.AllTransactions", TradePortalConstants.TEXT_BUNDLE) %>
                </span>
              </div>
  
			<%}
			displaySection = false;
			if(condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS )){
			    if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS ) ) {
			      displaySection = true;
			    }
			%>
<%-- CR 821 Rel 8.3 STARTS --%> 

              <div id="<%=TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS%>_avail" class="availItem">
                <span class="availItemSelect">
                  <%= widgetFactory.createCheckboxField(
                        TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS+"_checkbox", "", displaySection,
                        false, false, "", "", "none") %>
                </span>
                <span class="availItemDesc">
                  <%= resMgr.getText("DashboardCustomization.PanelAuthTransactions", TradePortalConstants.TEXT_BUNDLE) %>
                </span>
              </div>
              <%}%>
       
<%if(isTransSectonDiv){%>  
</div>
<%}
//jgadela 08/09/2013 REL 8.3 CR-821 - T36000019559 Refactored the whole trasactions block conditions <END> 
%>
  
  
  
  
  
 <%//cquinton 10/18/2012 ir#6417 receivables matching should be its own section
  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
%>
            <div data-dojo-type="dijit.TitlePane" id="ReceivablesSection" class="availItemsSection doNotShowOnSidebar"
                 title='<%= resMgr.getText("DashboardCustomization.ReceivablesMatching", TradePortalConstants.TEXT_BUNDLE) %>'>
              <div id="<%=TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES%>_avail" class="availItem">
                <span class="availItemSelect">
<%
    displaySection = false;
    if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES ) ) {
      displaySection = true;
    }
%>
                  <%= widgetFactory.createCheckboxField(
                        TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES+"_checkbox", "", displaySection,
                        false, false, "", "", "none") %>
                </span>
                <span class="availItemDesc">
                  <%= resMgr.getText("DashboardCustomization.ReceivablesMatching", TradePortalConstants.TEXT_BUNDLE) %>
                </span>
              </div>
            </div>
<%
  }

 
 //IR 23015 Start - visibility condition should be same as Accounts on menu bar
  //Nar IR# T36000011763 should display only if user has CM transaction (Payment and Transaction Between Amount)access.
  //if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS ) ) {
	  
	  if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_ACCOUNTS ) ) {
 //IR 23015 End
%>
            <div data-dojo-type="dijit.TitlePane" id="AccountsSection"   class="availItemsSection doNotShowOnSidebar"
                 title='<%= resMgr.getText("DashboardCustomization.Accounts", TradePortalConstants.TEXT_BUNDLE) %>'>
              <div id="<%=TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES%>_avail" class="availItem">
                <span class="availItemSelect">
<%
  displaySection = false;
  if ( sectionIds.contains( TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES ) ) {
    displaySection = true;
  }
%>
                  <%= widgetFactory.createCheckboxField(
                        TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES+"_checkbox", "", displaySection,
                        false, false, "", "", "none") %>
                </span>
                <span class="availItemDesc">
                  <%= resMgr.getText("DashboardCustomization.AccountBalances", TradePortalConstants.TEXT_BUNDLE) %>
                </span>
              </div>
            </div>
<%  } %>
      
          </span>
          <span class="customizationColumn2">
            <div class="instruction"><%= resMgr.getText("DashboardCustomization.Step2", TradePortalConstants.TEXT_BUNDLE) %></div>

            <div class="customizationItemsHeader"><%= resMgr.getText("DashboardCustomization.SelectedItems", TradePortalConstants.TEXT_BUNDLE) %></div>

            <div data-dojo-type="dijit.TitlePane" id="SelectedItems" class="selectedItemsSection doNotShowOnSidebar"
                 title='<%= resMgr.getText("DashboardCustomization.Order", TradePortalConstants.TEXT_BUNDLE) %>'>
              <%--a dom element that is direct parent of individual selected items, so we can easily access--%>
              <div id="selectedItems">
<%
	int selectedItemIdx = 0;
  String displayKey = "";
  for( int i=0; i<sectionIds.size(); i++) {
    String sectionId = sectionIds.get(i);
    //cquinton 1/22/2013 ir#xxx only display selected if can be displayed
    boolean displaySelected = false;
    if (TradePortalConstants.DASHBOARD_SECTION__MAIL.equals(sectionId)) {
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_MAIL_MESSAGES ) ) {
        displaySelected = true;
      }
      displayKey = "DashboardCustomization.MailMessages";
    }
    else if (TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS.equals(sectionId)) {
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NOTIFICATIONS ) ) {
        displaySelected = true;
      }
      displayKey = "DashboardCustomization.Notifications";
    }
  //Rel 9.0 CR 913 vdesingu start
    else if (TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL.equals(sectionId) 
    		&& TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
	   &&  TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel) ) {
        if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_DEBIT_FUNDING_MAIL ) ) {
          displaySelected = true;
        }
        displayKey = "DashboardCustomization.DebitFundingMailMessages";
      }
  //Rel 9.0 CR 913 vdesingu End
    else if (TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS.equals(sectionId)) {
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_TRANSACTIONS ) ) {
        displaySelected = true;
      }
      displayKey = "DashboardCustomization.AllTransactions";
    }
    else if (TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS.equals(sectionId)) {
        if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_PANEL_AUTHORISATION_TRANSACTIONS ) ) { //Rel9.1 IR T36000032379
          displaySelected = true;
        }
        displayKey = "DashboardCustomization.PanelAuthTransactions";
      }
    else if (TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES.equals(sectionId)) {
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_RECEIVABLE_TRANSACTIONS ) ) {
        displaySelected = true;
      }
      displayKey = "DashboardCustomization.ReceivablesMatching";
    }
    else if (TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES.equals(sectionId)) {
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_NEW_PAYMENT_INSTRUMENTS ) ) {
         displaySelected = true; 
      }   
      displayKey = "DashboardCustomization.AccountBalances";
    }
    else if (TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED.equals(sectionId)) {
      if (condDisplay.shouldDisplay( ConditionalDisplay.DISPLAY_INVOICE_OFFERS ) ) {
         displaySelected = true; 
      }   
      displayKey = "DashboardCustomization.SpInvoicesOffered";
    } 

if (displaySelected) {
	String sectionOid = "0"; //default to new
	String sectionOptLock = "";
	if (sectionIds.size() == sectionOids.size()) { //these are not default sections
		sectionOid = StringFunction.xssCharsToHtml(sectionOids.get(i));
		sectionOptLock = StringFunction.xssCharsToHtml(sectionOptLocks.get(i));
	}
%>
                <%--cquinton 9/13/2012 ir#t36000004200 use selectedItem id with included section id is
                    problematic.  remove id and add some classes to subitems, then use query().children()
                    to get the sectionId off the hidden field--%>
                <div class="selectedItem">
                  <input id="sectionOid<%=selectedItemIdx%>" name="sectionOid<%=selectedItemIdx%>" type="hidden" value="<%=sectionOid%>" />
                  <span class="selectedItemOrder">
                    <input id="display_order<%=selectedItemIdx%>" name="display_order<%=selectedItemIdx%>" data-dojo-type="dijit.form.TextBox"
                           data-dojo-props="intermediateChanges: true" 
                           maxLength="3" class="selectedItemOrderTextBox" value="<%=selectedItemIdx+1%>" required="true">
                  </span>
                  <input id="sectionId<%=selectedItemIdx%>" name="sectionId<%=selectedItemIdx%>" class="selectedItemId" type="hidden" value="<%=sectionId%>" />
                  <span id="selectedDesc<%=selectedItemIdx%>" class="selectedItemDesc">
                    <%= resMgr.getText(displayKey, TradePortalConstants.TEXT_BUNDLE) %>
                  </span>
                  <input id="sectionOptLock<%=selectedItemIdx%>" name="sectionOptLock<%=selectedItemIdx%>" type="hidden" value="<%=sectionOptLock%>" />
                  <span class="deleteSelectedItem"></span>
                  <div style="clear:both;"></div>
                </div>
<%
      //set focus field to first order item display order, if there is one
      if (i==0) {
        focusField = "display_order0";
      }
      selectedItemIdx++;
    } //displaySelected
  }
%>
              </div>
              <div class="selectedItemsFooter">
                <div class="selectedItemsTotalCount">
                  <span><%= resMgr.getText("DashboardCustomization.totalCount", TradePortalConstants.TEXT_BUNDLE) %>&nbsp;</span>
                  <span id="selectedItems_totalCount"><%=selectedItemIdx%></span>
                </div>
                <div class="clear:both;"></div>
              </div>
            </div>
            <%--cquinton 10/17/2012 ir#6640 use button type so form does not submit--%>
            <button id="updateButton" data-dojo-type="dijit.form.Button"
                    type="button" class="customizationAction" >
              <%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
            </button>
            <%--cquinton 12/03/2012 add hover help--%>
            <%=widgetFactory.createHoverHelp("updateButton","DashboardCustomization.Update") %>
          </span>
          <div style="clear:both;"></div>
        </div>
      </div>

      <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'DashboardCustomizationForm'">
        <jsp:include page="/common/RefDataSidebar.jsp">
          <jsp:param name="showSaveButton" value="true" />
          <jsp:param name="saveOnClick" value="none" />
          <jsp:param name="showSaveCloseButton" value="true" />
          <jsp:param name="saveCloseOnClick" value="none" />
          <jsp:param name="showPreviewButton" value="true" />
          <jsp:param name="cancelAction" value="goToTradePortalHome" />
          <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
          <jsp:param name="error" value="<%= error%>" />
        </jsp:include>
        <%=widgetFactory.createHoverHelp("PreviewButton","DashboardCustomization.Preview") %>
      </div>

      <%= formMgr.getFormInstanceAsInputField("DashboardCustomizationForm", secureParms) %>

    </form>
  </div>
</div>

  <%--ctq portal refresh end --%>


<%
   Debug.debug("***END******************** DASHBOARD CUSTOMIZATION Page *********************END***");

   formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<div id="previewDialog" ></div>

<script>
  <%-- define a local variable for functions --%>
  var local = {
    <%-- a property that keep track of the last order field changed so that it sorts at the top --%>
    lastOrderChanged: undefined
  };

  <%-- define the set of field ids - this is used for adding/removing rows and updating row order --%>
  <%-- the array of field ids must be: 0=oid, 1=id, 2=order input, 3+ other fields --%>
  var selectedItemFieldIds = [ "sectionOid", "sectionId", "display_order", "selectedDesc", "sectionOptLock" ];

  require(["dojo/dom",
           "dojo/dom-construct", "dojo/dom-class",
           "dojo/dom-style", "dojo/query",
           "dijit/registry", "dojo/on",
           "t360/common", "t360/dialog",
           "t360/itemselect",
           "dojo/ready", "dojo/NodeList-traverse"],
      function(dom,
               domConstruct, domClass,
               domStyle, query,
               registry, on,
               common, dialog,
               itemSelect,
               ready ) {

    <%-- selectItem is page specific, so we do it here rather than commonly in itemselect --%>
    local.selectItem = function(checkboxWidget, selectedItemsId) {

      <%-- cquinton 12/14/2012 do not tally counts, at the end just --%>
      <%--  set based on current conditions --%>
      <%-- itemSelect.increaseTotalCount(selectedItemsId); --%>

      <%-- get selectItemId --%>
      var checkboxId = checkboxWidget.id;
      var idx = checkboxId.indexOf("_checkbox");
      var addSelectItemId = checkboxId.substring(0,idx);

      <%-- get the text to add --%>
      var description = "";
      <%-- only expect one but we use query for simplicity --%>
      query("#"+addSelectItemId+"_avail").children(".availItemDesc").forEach( function(entry, i) {
        description = entry.innerHTML;
      });

      var selectItems = dom.byId(selectedItemsId); <%-- get the selectedItem parent --%>

      <%-- get index for next item --%>
      <%--  the fields must be sequential, zero based for submit to work --%>
      var nextIdx = itemSelect.getSelectedItemCount(selectedItemsId);

      <%-- get the next order # in sequence --%>
      var nextOrderValue = itemSelect.getMaxOrderValue(selectedItemsId) + 1;

      <%-- add the selected item --%>
      var newSelectItem = domConstruct.create("div", null, selectItems);
      domClass.add(newSelectItem,"selectedItem");
      var newOid = domConstruct.create("input",
        { id:"sectionOid"+nextIdx, name:"sectionOid"+nextIdx, type:"hidden", value:"0" },
        newSelectItem);
      var newSelectItemOrder = domConstruct.create("span", null, newSelectItem);
      domClass.add(newSelectItemOrder,"selectedItemOrder");
      var newOrderInput = domConstruct.create("input",
        {id:"display_order"+nextIdx, name:"display_order"+nextIdx},
        newSelectItemOrder);
      var newOrderTextBox =
        new dijit.form.TextBox({ 
          name:"display_order"+nextIdx, 
          value: nextOrderValue,
          <%-- event handler --%>
          intermediateChanges: true, <%-- after each keystroke ie doesn't fire on button click --%>
          onChange: function() {
            local.lastOrderChanged = "display_order"+nextIdx
          }
        }, newOrderInput);
      domClass.add(newOrderTextBox.domNode,"selectedItemOrderTextBox");
      var newSelectItemId = domConstruct.create("input",
        { id:"sectionId"+nextIdx, name:"sectionId"+nextIdx, type:"hidden", value:addSelectItemId },
        newSelectItem);
      domClass.add(newSelectItemId,"selectedItemId");
      var newSelectItemDesc = domConstruct.create("span",
        {id:"selectedDesc"+nextIdx, innerHTML:description},
        newSelectItem);
      domClass.add(newSelectItemDesc,"selectedItemDesc");
      var newOptLock = domConstruct.create("input",
        { id:"sectionOptLock"+nextIdx, name:"sectionOptLock"+nextIdx, type:"hidden", value:"" },
        newSelectItem);
      var newSelectItemDel = domConstruct.create("span", null, newSelectItem);
      domClass.add(newSelectItemDel,"deleteSelectedItem");
      var newSelectItemClear = domConstruct.create("div", null, newSelectItem);
      domStyle.set(newSelectItemClear, "clear", "both");

      <%-- cquinton 12/14/2012 do not tally counts, at the end just --%>
      <%--  set based on current conditions --%>
      itemSelect.setTotalCount(selectedItemsId);
    };

    <%-- delete selected item event handler --%>
    <%-- cquinton 9/13/2012 ir#t36000004200 --%>
    <%-- use event delegation to get all interaction on any --%>
    <%--  delete selected item within selectedItems --%>
    <%-- this removes need to add/remove event handles as selected items are --%>
    <%--  added/removed --%>
    query('#selectedItems').on(".deleteSelectedItem:click", function(evt) {
        itemSelect.deleteEventHandler(evt.target);
    });

    <%-- cquinton 12/20/2012 event delegation on change does not work in ie 7,8 --%>
    <%--  we have to do it individually. ie9+ and other browsers all do --%>
    <%-- todo: when ie8 support drops, go back to event delegation... --%>
    <%-- put an onchange event handler on the order fields so we can keep track of --%>
    <%-- which one was touched last --%>
    <%-- query('#selectedItems').on(".selectedItemOrderTextBox:change", function(evt) { --%>
    <%--   local.lastOrderChanged = evt.target.id; --%>
    <%-- }); --%>
      

    <%-- now load the stuff that requires dojo to be loaded --%>
    ready(function() {
	
      var focusFieldId = '<%= focusField %>';
      if ( focusFieldId ) {
        var focusField = registry.byId(focusFieldId);
        focusField.focus();
      }

      <%-- event handlers for selecting/unselecting items --%>
      var mailCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__MAIL%>_checkbox");
      if ( mailCheckBox ) {
        mailCheckBox.on("change", function(checkValue) {
          if ( checkValue ) {
            local.selectItem(this, "selectedItems");
          }
          else {
            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
            setNoSectionsOnLastUncheck();
          }
        });
      }
      var notifsCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS%>_checkbox");
      if ( notifsCheckBox ) {
        notifsCheckBox.on("change", function(checkValue) {
          if ( checkValue ) {
            local.selectItem(this, "selectedItems");
          }
          else {
            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
            setNoSectionsOnLastUncheck();
          }
        });
      }
      <%-- Rel 9.0 - CR 913 - vdesingu - Start --%>
      var debFundCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL%>_checkbox");
      if ( debFundCheckBox ) {
    	  debFundCheckBox.on("change", function(checkValue) {
          if ( checkValue ) {
            local.selectItem(this, "selectedItems");
          }
          else {
            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
            setNoSectionsOnLastUncheck();
          }
        });
      }
      <%-- Rel 9.0 - CR 913 - vdesingu - End --%>
    <%-- Ravindra - CR-708B - Start --%>
      <%-- event handlers for selecting/unselecting items --%>
      var invOfferedCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED%>_checkbox");
      if ( invOfferedCheckBox ) {
    	  invOfferedCheckBox.on("change", function(checkValue) {
	          if ( checkValue ) {
	            local.selectItem(this, "selectedItems");
	          }
	          else {
	            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
	            setNoSectionsOnLastUncheck();
	          }
	        });
      }
    <%-- Ravindra - CR-708B - End --%>
      var allTranCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS%>_checkbox");
      if ( allTranCheckBox ) {
        allTranCheckBox.on("change", function(checkValue) {
          if ( checkValue ) {
            local.selectItem(this, "selectedItems");
          }
          else {
            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
            setNoSectionsOnLastUncheck();
            
          }
        });
      }
      <%-- CR 821 Rel 8.3 START --%>
      var panelAuthTranCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS%>_checkbox");
      if ( panelAuthTranCheckBox ) {
        panelAuthTranCheckBox.on("change", function(checkValue) {
          if ( checkValue ) {
            local.selectItem(this, "selectedItems");
          }
          else {
            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
            setNoSectionsOnLastUncheck();
          }
        });
      }
      <%-- CR 821 Rel 8.3 END --%>
      var rcvMatchNoticesCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES%>_checkbox");
      if ( rcvMatchNoticesCheckBox ) {
        rcvMatchNoticesCheckBox.on("change", function(checkValue) {
          if ( checkValue ) {
            local.selectItem(this, "selectedItems");
          }
          else {
            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
            setNoSectionsOnLastUncheck();
          }
        });
      }
      var acctCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES%>_checkbox");
      if ( acctCheckBox ) {
        acctCheckBox.on("change", function(checkValue) {
          if ( checkValue ) {
            local.selectItem(this, "selectedItems");
          }
          else {
            itemSelect.unselectItem(this, "selectedItems", selectedItemFieldIds);
            setNoSectionsOnLastUncheck();
          }
        });
      }
	 <%-- Disable other checkboxes --%>
	 if( registry.byId("displayNoSections").checked == true){
    	deselectOtherSections();
	 }	
	 
      <%-- update button --%>
      var updateButton = registry.byId("updateButton");
      if ( updateButton ) {
        updateButton.on("click", function() {
          <%-- cquinton 10/17/2012 ir#6640 update orders the selected items, does not save --%>
          itemSelect.updateOrder("selectedItems", selectedItemFieldIds, local.lastOrderChanged);
        });
      }

      <%-- preview button - this is very page specific --%>
      var previewButton = registry.byId("PreviewButton");
      if ( previewButton ) {
        previewButton.on("click", function() {
          <%-- get the selected sections --%>
          var sections = "";
          var selectItemCount = itemSelect.getSelectedItemCount("selectedItems");
          if( selectItemCount>0){
          	for (var idx=0; idx<selectItemCount; idx++) {
            	if ( idx>0 ) {
              	sections += ",";
            	}
            	var sectionId = dom.byId("sectionId"+idx);
            	sections += sectionId.value;
          	}
          }else if(registry.byId("displayNoSections").checked == true){
        	  sections = "<%=TradePortalConstants.DASHBOARD_NO_SECTIONS_DISPLAY%>";
          }
          dialog.open('previewDialog', 'Dashboard Preview',
            'dashboardPreview.jsp',
            'sections', sections);
        });
      }

      <%-- cquinton 12/20/2012 eventhandlers for order change --%>
      <%-- todo: would like to do eventdelegation, need to wait for ie7,8 support to expire --%>
      var orderWidget = registry.byId('display_order0');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order0";
        });
      }
      orderWidget = registry.byId('display_order1');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order1";
        });
      }
      orderWidget = registry.byId('display_order2');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order2";
        });
      }
      orderWidget = registry.byId('display_order3');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order3";
        });
      }
      orderWidget = registry.byId('display_order4');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order4";
        });
      }
      orderWidget = registry.byId('display_order5');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order5";
        });
      }
      orderWidget = registry.byId('display_order6');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order6";
        });
      }
      orderWidget = registry.byId('display_order7');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order7";
        });
      }
      orderWidget = registry.byId('display_order8');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order8";
        });
      }
      orderWidget = registry.byId('display_order9');
      if ( orderWidget ) {
        orderWidget.on("change", function(evt) {
          local.lastOrderChanged = "display_order9";
        });
      }
   
  
    });	
  }); 

			 
<%--MEer IR-34829 If No Sections check box is selected then deselect all other sections to display on home page. --%>			 
  function deselectOtherSections() {
    	 require(["dijit/registry"],   function(registry) {
		     	var mailCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__MAIL%>_checkbox");
      			var notifsCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__NOTIFICATIONS%>_checkbox");
      			var debFundCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION_DEBIT_FUNDING_MAIL%>_checkbox");
     			var invOfferedCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__INVOICE_OFFERED%>_checkbox");
      			var allTranCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__ALL_TRANSACTIONS%>_checkbox");
     			var panelAuthTranCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__PANEL_AUTH_TRANSACTIONS%>_checkbox");
     			var rcvMatchNoticesCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__RCV_MATCH_NOTICES%>_checkbox");
      			var acctCheckBox = registry.byId("<%=TradePortalConstants.DASHBOARD_SECTION__ACCOUNT_BALANCES%>_checkbox");
	   			var updateButton = registry.byId("updateButton");
    			if( registry.byId("displayNoSections").checked == true) {
	 	 			deselectDisableCheckbox(mailCheckBox);
	 				deselectDisableCheckbox(notifsCheckBox);
	  				deselectDisableCheckbox(debFundCheckBox);
	 			 	deselectDisableCheckbox(invOfferedCheckBox);
	  				deselectDisableCheckbox(allTranCheckBox);
	  				deselectDisableCheckbox(panelAuthTranCheckBox);
	  				deselectDisableCheckbox(rcvMatchNoticesCheckBox);
	  				deselectDisableCheckbox(acctCheckBox);	  
	  				updateButton.set('disabled', true);
    			}else {
	  				enableCheckbox(mailCheckBox);
	 				enableCheckbox(notifsCheckBox);
	  				enableCheckbox(debFundCheckBox);
	  				enableCheckbox(invOfferedCheckBox);
	  				enableCheckbox(allTranCheckBox);
	  				enableCheckbox(panelAuthTranCheckBox);
	  				enableCheckbox(rcvMatchNoticesCheckBox);
	  				enableCheckbox(acctCheckBox);
	  				updateButton.set('disabled', false);
    			}
		 });
  }
	 
    function deselectDisableCheckbox(chkBox) {
		if(chkBox != null){
			chkBox.set('checked', false);
    		chkBox.set('disabled', true);
		}
    }
    
	function enableCheckbox(chkBox) {
		if(chkBox != null){
     		chkBox.set('disabled', false);
		}
	}	
	<%-- IR-34829  Select NoSections check box on unchecking the last section i.e., selected sections count is 0.--%>
  function setNoSectionsOnLastUncheck(){	  
	   require(["dojo/dom", "dijit/registry"],   function(dom, registry) {
	   var totalCount = dom.byId("selectedItems_totalCount").innerHTML;		  
	 	 if(totalCount ==0 || totalCount ==""){
		  	registry.byId("displayNoSections").set('checked', true);
	 	 }
	   });	   
  }
 
</script>

</body>
</html>