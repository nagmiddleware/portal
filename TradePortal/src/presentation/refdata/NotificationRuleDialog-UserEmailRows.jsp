
<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.ArrayList, java.util.Hashtable" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
String defaultSelection = "";
int DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 1;
  String parmValue1 = parmValue1 = request.getParameter("notifEmailIndex");
  int notifEmailIndex = 0;
  if ( parmValue1 != null ) {
    try {
    	notifEmailIndex = (new Integer(parmValue1)).intValue();
    } 
    catch (Exception ex ) {
      //todo
    }
  }

  String parmValue2 = parmValue2 = request.getParameter("rowIndex");
  int rowIndex = 0;
  if ( parmValue2 != null ) {
    try {
    	rowIndex = (new Integer(parmValue2)).intValue();
    } 
    catch (Exception ex ) {
      //todo
    }
  }
  
  String parmValue3 = parmValue3 = request.getParameter("myDivIdIdx");
  int selectedGridIndex = 0;
  if ( parmValue3 != null ) {
	    try {
	    	selectedGridIndex = (new Integer(parmValue3)).intValue();
	    } 
	    catch (Exception ex ) {
	      //todo
	    }
  }
  
  String loginLocale = userSession.getUserLocale();
  boolean isReadOnly = Boolean.parseBoolean(request.getParameter("isReadOnly"));
  String notificationRuleTemplateOid = request.getParameter("notificationRuleTemplateOid");
  
//other necessaries
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String notifyRuleUsersSql = "select USER_OID, LOGIN_ID from users where activation_status=? and p_owner_org_oid = ? and email_addr is not null";
  DocumentHandler notifyRuleUsersList = DatabaseQueryBean.getXmlResultSet(notifyRuleUsersSql, false, new Object[]{TradePortalConstants.ACTIVE, userSession.getOwnerOrgOid()});
	
	String userEmailsOptions = "<option  value=\"\"></option>";
	userEmailsOptions += ListBox.createOptionList(notifyRuleUsersList, "USER_OID", "LOGIN_ID", "", null);
  
	  
	  String notifyEmail = "";
	  if(null != request.getParameter("notifyEmail")){
		  notifyEmail = request.getParameter("notifyEmail");
	  }
	  
	  String tableRowIdPrefix = "";
	  if(null != request.getParameter("tableRowIdPrefix")){
		  tableRowIdPrefix = request.getParameter("tableRowIdPrefix");
	  }
	  
	  
  %>


<%@ include file="fragments/NotificationRuleDialog-UserEmailRows.frag" %>

