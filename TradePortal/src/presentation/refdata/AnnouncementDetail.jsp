<%--
*******************************************************************************
                          Announcement Detail

  Description:
     This page is used to maintain announcements.  It supports
  insert, update and delete.  Errors found during a save are redisplayed on the 
  same page.  Successful updates return the user to the admin ref data home page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.Vector, java.util.Hashtable , java.util.Locale, java.util.Date, java.text.*, com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.cache.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  String links="";
  String certAuthURL = "";
  
  
  boolean isViewInReadOnly = false;
  boolean showSave=true;
  boolean showSaveAsDraft=true;
  boolean showDeactivate=true;
  boolean showPreview = true;
  boolean all_bogs_indicator = false;
  int availableBankGroups = 0;
  
  // even if form is opened for an existing announcement, it should always be editable
  boolean isReadOnly = false;
  boolean debug = false;
  String buttonPressed = "";
  Vector error = null;
  String pattern = "";
  String oid = null;
  String announcementDialogTitle = 
	      resMgr.getText("announcementDialog.title", TradePortalConstants.TEXT_BUNDLE);
  String announcementStatus = "DRAFT"; 
  Vector<String> bogIds = new Vector<String> ();
 
  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);

  String startDate_val = "";
  String endDate_val = "";
  String isCritical_val = "";
  String subject_val = "";
  String title_val = "";
 boolean isAdmin = false;
  // These booleans help determine the mode of the JSP.  
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;
  Vector<DocumentHandler> nodes = new Vector<DocumentHandler>();
  Vector <DocumentHandler> preSelectedBG = new Vector <DocumentHandler> ();

  // Constants for ref data access.
  String NO_ACCESS = "N";
  String VIEW_ONLY = "V";
  String MAINTAIN  = "M";

  DocumentHandler doc;
  Debug.debug("***START********************ANNUONCEMENT*DETAIL**************************START***");
  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.AnnouncementWebBean",
                        "Announcement");
  AnnouncementWebBean announcementBean =
                   (AnnouncementWebBean)beanMgr.getBean("Announcement");
   // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
 //   SureshL IR-T36000028608 05/30/2014 Start 
 
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();
 
  if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_ANNOUNCEMENT)) {
     isReadOnly = false;
  } else if (SecurityAccess.hasRights(loginRights,
                                      SecurityAccess.VIEW_ANNOUNCEMENT)) {
     isReadOnly = true;
  } else {
     // Don't have maintain or view access, send the user back.
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage =
                 NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>'/>
<%
        return;
  }

  // We should only be in this page for non-corporate users.  If the logged in
  // user's level is corporate, send back to ref data page.  This condition
  // should never occur but is caught just in case.
  String ownerLevel = userSession.getOwnershipLevel();
  if (ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
        formMgr.setCurrPage("AdminRefDataHome");
        String physicalPage =
                 NavigationManager.getNavMan().getPhysicalPage("AdminRefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>'/>
<%
        return;
  }

  if( isViewInReadOnly )
      isReadOnly = true;

  Debug.debug("login_rights is " + loginRights);
  
//   SureshL IR-T36000028608 05/30/2014 End 

  // Get the document from the cache.  We'll use it to repopulate the screen if
  // the user was entering data with errors.
  doc = formMgr.getFromDocCache();
  //out.println("doc="+doc.toString());
  Debug.debug("doc from cache is " + doc.toString());
  
  if (doc.getDocumentNode("/In/Announcement") == null ) {
    // The document does not exist in the cache.  We are entering the page from
    // the listview page (meaning we're opening an existing item or creating 
    // a new one.)

    if (request.getParameter("announcementOid") != null) {
      oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("announcementOid"), userSession.getSecretKey());
    }

    if (oid == null) {
      getDataFromDoc = false;
      retrieveFromDB = false;
      insertMode = true;
      oid = "0";
    } else if (oid.equals("0")) {
      getDataFromDoc = false;
      retrieveFromDB = false;
      insertMode = true;
    } else {
      // We have a good oid so we can retrieve.
      getDataFromDoc = false;
      retrieveFromDB = true;
      insertMode = false;
    }

  } else {

    //we've returned from save or some other action (i.e. save as draft/deactivate)
    getDataFromDoc = true;
    retrieveFromDB = false;

    //set some items
    if (doc.getDocumentNode("/In/Update/ButtonPressed") != null) {
      buttonPressed  = doc.getAttribute("/In/Update/ButtonPressed");
      error = doc.getFragments("/Error/errorlist/error");
    }

    String maxError = doc.getAttribute("/Error/maxerrorseverity");
    if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) <= 0) {
      //no errors, get data from db
      getDataFromDoc = false;
      retrieveFromDB = true;
      oid = doc.getAttribute("/Out/Announcement/announcement_oid");
      //if this was an update not an insert, we need to get from in
      if (oid == null ) {
        oid = doc.getAttribute("/In/Announcement/announcement_oid");
      }
      insertMode = false;

    } 
    else { //we have an error
      //Determine if we're in insertMode by looking for a '0' oid in the input section
      // of the doc.
      oid = doc.getAttribute("/In/Announcement/announcement_oid");

      if ("0".equals(oid))
      {
        insertMode = true;
      }
      else
      {
        // Not in insert mode, use oid from output doc.
        insertMode = false;
      }
    }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll 
     // check for a blank oid -- this indicates record not found.  Set to insert
     // mode and display error.

     getDataFromDoc = false;

     announcementBean.setAttribute("announcement_oid", oid);
     announcementBean.getDataFromAppServer();
     oid = announcementBean.getAttribute("announcement_oid");

     //get some values for debugging
     startDate_val = announcementBean.getAttribute("start_date");
     endDate_val = announcementBean.getAttribute("end_date");
     subject_val = announcementBean.getAttribute("subject");
     title_val = announcementBean.getAttribute("title");
     isCritical_val = announcementBean.getAttribute("critical_ind");
     announcementStatus = announcementBean.getAttribute("announcement_status");
     //Debug.debug("retrieveFromDB bog indicator="+announcementBean.getAttribute("all_bogs_ind"));
     all_bogs_indicator = announcementBean.getAttribute("all_bogs_ind").equalsIgnoreCase("Y") ? true: false;
     //Debug.debug("retrieveFromDB: oid="+ oid + ": startDate_val="+ startDate_val +
     // 			 ": endDate_val="+endDate_val + ": subject_val="+ subject_val +
     // 			 ": title_val="+ title_val + ": isCritical_val="+isCritical_val);
	 
     if (announcementBean.getAttribute("announcement_oid").equals("")) {
       //insertMode = true;
       oid = "0";
       //getDataFromDoc = false;
     } else {
     	// Update mode is set
     	if (!all_bogs_indicator) {
     		//jgadela R91 IR T36000026319 - SQL INJECTION FIX
     		String sql = "select a_bog_oid from announcement_bank_group where p_announcement_oid = ? ";
     		DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, new Object[]{oid});
     		
     		if(result != null){
     			Debug.debug("result="+result.toString());
     			preSelectedBG = result.getFragments("/ResultSetRecord");
         		Debug.debug("selections="+preSelectedBG.size());
         		 for (int i =0; i < preSelectedBG.size(); i++){
             		String bog = preSelectedBG.get(i).getAttribute("/A_BOG_OID");
             		Debug.debug("bog="+bog);
             		if (bog != null && bog.trim().length() > 0)
             		 	bogIds.add(bog);
             	 }	
     		}
     		
     	}
     	//insertMode = false;
     }
  } 

  if (getDataFromDoc) {
     // Populate the announcement bean with the input doc.
     try {
    	 announcementBean.populateFromXmlDoc(doc.getComponent("/In"));
    	 //out.println(announcementBean.getAttribute("start_date"));
    	 //out.println(announcementBean.getAttribute("subject"));
    	 Debug.debug("doc="+doc.toString());
    	 preSelectedBG = doc.getFragments("/In/Announcement/AnnouncementBankGroupList");
    	 oid = announcementBean.getAttribute("announcement_oid");
    	 startDate_val = announcementBean.getAttribute("start_date");
    	 endDate_val = announcementBean.getAttribute("end_date");
    	 subject_val = announcementBean.getAttribute("subject");
    	 title_val = announcementBean.getAttribute("title");
    	 isCritical_val = announcementBean.getAttribute("critical_ind");
    	 announcementStatus = announcementBean.getAttribute("announcement_status");
     	 Debug.debug("bog indicator="+announcementBean.getAttribute("all_bogs_ind"));
    	 all_bogs_indicator = announcementBean.getAttribute("all_bogs_ind").equalsIgnoreCase("Y") ? true: false;
    	 for (int i =0; i < preSelectedBG.size(); i++){
    		String bog = preSelectedBG.get(i).getAttribute("bog_oid");
    		if (bog != null && bog.trim().length() > 0) {
    		   //out.println("bog="+bog + " found");
    			bogIds.add(bog);
    		}
    	 }
    	 
     	 if (announcementBean.getAttribute("announcement_oid").equals("") ||
			announcementBean.getAttribute("announcement_oid").equals("0")) {
     	       //insertMode = true;
     	       oid = "0";
     	       //getDataFromDoc = false;
     	     } else {
     	     }
    	 
     	 Debug.debug("bogIds="+bogIds);
     	 Debug.debug("getDataFromDoc: oid="+ oid + ": startDate_val="+ startDate_val +
     			 ": endDate_val="+endDate_val + ": subject_val="+ subject_val +
     			 ": title_val="+ title_val + ": isCritical_val="+isCritical_val);
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get Announcement "
             + "attributes for oid: " + oid + " " + e.toString());
     }
  }

  //cquinton 3/1/2013 #14375 if the user's org is not the owner of the announcement, make it read only
  if ( !insertMode && announcementBean!=null ) {
    String announcementOwnerOid = announcementBean.getAttribute("owner_org_oid");
    if (announcementOwnerOid!=null && !announcementOwnerOid.equals(userSession.getOwnerOrgOid())) {
      isReadOnly = true;
    }
  }

  if (isReadOnly) { 
    showSave = false; showSaveAsDraft = false; showPreview = true; showDeactivate = false;
  }
  else if (insertMode) {
    showSave = true; showSaveAsDraft = true; showPreview = true; showDeactivate = false;
  } 
  else { 
    showSave = true; showSaveAsDraft = false; showPreview = true; showDeactivate = true;
  }
%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
%>

<%-- Body tag included as part of common header --%>
<%-- ********************* HTML for page begins here *********************  --%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>


<div class="pageMain">
  <div class="pageContent">
<%
  String pageTitleKey = resMgr.getText("Announcement.Announcement",TradePortalConstants.TEXT_BUNDLE);
%>
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="helpUrl"  value="customer/announcement_form.htm" />
    </jsp:include>
<%// --------------------- Sub header start --------------------- %>
<%  
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if ( insertMode) {
    subHeaderTitle = resMgr.getText("Announcement.New",TradePortalConstants.TEXT_BUNDLE);
    oid = "0";
  } else {
    subHeaderTitle = announcementBean.getAttribute("title") + " - (" +announcementBean.getAttribute("announcement_status") + ")";
  }

  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("selectAnnouncement", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("selectAnnouncement", parms.toString(), response);
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include> 		
<%// --------------------- Sub header end --------------------- %>

    <form id="AnnouncementDetailForm" name="AnnouncementDetailForm"
      method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  //Debug.debug("oid="+oid);
  Hashtable secureParms = new Hashtable();
  secureParms.put("UserOid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("announcement_oid", oid);
  if (oid != null && !oid.equalsIgnoreCase("0")) 
	  secureParms.put("opt_lock", announcementBean.getAttribute("opt_lock"));
  secureParms.put("ownership_level", userSession.getOwnershipLevel());
  //IR#T36000038731 pgedupudi - Rel-9.3 CR-976A 06/18/2015
  secureParms.put("announcement_status", announcementStatus);
  // Used for the "Added By" column
  if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
	secureParms.put("OwnershipType", TradePortalConstants.OWNER_TYPE_ADMIN);
  else
	secureParms.put("OwnershipType", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
%>

      <div class="formArea">

        <jsp:include page="/common/ErrorSection.jsp" />

        <div class="formContent formBorder">
          <input type=hidden value="" name=buttonName>
          <%-- 	<input type=hidden value="0" name=announcement_oid> --%>
          <%-- 	<input type=hidden value="" name=owner_org_oid> --%>
          <%-- 	<input type=hidden value="" name=opt_lock> --%>
          <%-- 	<input type=hidden value="Y" name=all_bogs_ind> --%>

          <br/>
          <div class="formItem">
            <%= widgetFactory.createNote("Announcement.InstructionText") %>
          </div>
<%
  String sql = null;
  if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_BOG)){
    //cquinton 2/28/2013 ir#14375 when ownership level is brank group, user can
    // only view their bank group
    //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    sql = "select distinct organization_oid, name " +
      " from bank_organization_group" +
      " where organization_oid = ? "+ 
      //cquinton 12/5/2012 only include active bank orgs
      " and activation_status = ?";
  } else if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_BANK)) {
    sql ="select organization_oid, name "+ 
      "from bank_organization_group" +
      " where p_client_bank_oid = ?" +
      //cquinton 12/5/2012 only include active bank orgs
      " and activation_status = ? ";
  }

  boolean hasMultipleBOG = true;
  try{		
    DocumentHandler result = DatabaseQueryBean.getXmlResultSet(sql, false, userSession.getOwnerOrgOid(), TradePortalConstants.ACTIVE );
    //Debug.debug("result="+result.toString());
    nodes = result.getFragments("/ResultSetRecord");
    availableBankGroups = nodes.size();
    //Debug.debug(result.toString());
    //Debug.debug("size="+nodes.size());

    if (nodes.size() == 1) {
      availableBankGroups = 1;
      //Debug.debug("single node");
      hasMultipleBOG = false;
    }
  } catch (Exception ex) {
    ex.printStackTrace();
  }
  Debug.debug("hasMultipleBOG="+hasMultipleBOG);

  boolean allBogsIndIsReadOnly = isReadOnly;
  if ( TradePortalConstants.OWNER_BOG.equals( userSession.getOwnershipLevel() )) {
    //force read only if user is a bank group user
    allBogsIndIsReadOnly = true;
  }
%>
              <div>
                <%= widgetFactory.createLabel("groupName", "Announcement.BankGroups", true, false, false, "inline") %>
                <%= widgetFactory.createCheckboxField("all_bogs_ind", "Announcement.AllCheckBoxes",
                      all_bogs_indicator, allBogsIndIsReadOnly, false, "onClick='setAllCheckBoxes("+nodes.size()+");'", "", "inline")%>
              </div>
              <div class="scrollableArea" id= "scrollableArea">
<%
  if (hasMultipleBOG) {

    Debug.debug("bogIds="+ bogIds.size());
    for(int i =0; i < nodes.size(); i++){ 
      DocumentHandler nodeDoc = nodes.get(i); 
      String name = nodeDoc.getAttribute("/NAME");
      String orgid = nodeDoc.getAttribute("/ORGANIZATION_OID");
      String hiddenfieldname = "bankgroupOid"+i;
      String bankgroup = "groupChecked"+i;
      Debug.debug("orgid="+orgid);
      String orgIdEncode = StringFunction.xssCharsToHtml(orgid);
%>
                <%-- 	<input type="hidden" name="<%=hiddenfieldname%>" id="<%=hiddenfieldname%>" value=<%=orgid %>> --%>
		
                <%= widgetFactory.createCheckboxField(hiddenfieldname, name, orgid,
                     !bogIds.isEmpty() ? bogIds.contains(orgid) : false,
                     isReadOnly, false, "onClick='setCheckBoxValue(\""+hiddenfieldname+"\",\""+orgIdEncode+"\");'", "", "none")%>
                <br/>
<%
    }

  } 
  else {
    // retrieve the only BOG
    DocumentHandler nodeDoc = nodes.get(0);
    String name = nodeDoc.getAttribute("/NAME");
    String orgid = nodeDoc.getAttribute("/ORGANIZATION_OID"); 
%>
                <%= widgetFactory.createCheckboxField("groupChecked0", name, orgid,
                      true, //yes its checked
                      true, //yes its readonly
                      false, "", "", "none")%>
                <br/>
<%
  } 
%>
              </div>

              <div>
                <%= widgetFactory.createTextField("title", "Announcement.Title", title_val, "100", isReadOnly, false, 
                      false, "width=\"FormWidth\"", "", "") %>
                <%= widgetFactory.createTextArea("subject", "Announcement.Subject", subject_val, isReadOnly, false, false, 
                      "maxLength='4000' class=\"textAreaFormWidth\" rows=\"5\"", "", "") %>
                <%= widgetFactory.createLabel("startdate","Announcement.StartDate", false, false, false, "none inline") %>		
                <%= widgetFactory.createDateField("startdate", "", announcementBean.getAttribute("start_date"),
                      isReadOnly, false, false, "", "placeHolder:'"+datePatternDisplay+"'", "inline")%>
                <%= widgetFactory.createLabel("enddate","Announcement.EndDate", false, false, false, "inline none") %>
                <%= widgetFactory.createDateField("enddate", "", announcementBean.getAttribute("end_date"),
                      isReadOnly, false, false, "", "placeHolder:'"+datePatternDisplay+"'", "inline")%>
                <%= widgetFactory.createCheckboxField("critical_ind", "Announcement.Critical", 
                      isCritical_val.equalsIgnoreCase("Y") ? true: false, isReadOnly, false, "", "", "inline") %>		
              </div>
              <div style="clear:both;"></div>
              <br/>

        </div> <%--end formContent--%>
      </div> <%--end formArea--%>
  
      <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'AnnouncementDetailForm'">
        <jsp:include page="/common/RefDataSidebar.jsp">
          <jsp:param value="<%=links %>" name="links" />
          <jsp:param name="showSaveButton" value="<%= showSave %>" />
          <jsp:param name="showSaveAsDraftButton" value="<%= showSaveAsDraft %>" />
          <jsp:param name="showSaveCloseButton" value="<%= showSave %>" />
          <jsp:param name="showPreviewButton" value="<%= showPreview %>" />
          <jsp:param name="showDeactivateButton" value="<%= showDeactivate %>" />
          <jsp:param name="cancelAction" value="goToAnnouncementsHome" />
          <jsp:param name="showHelpButton" value="true" />  
          <jsp:param name="helpLink" value="admin/admin_sec_profile_detail.htm" />                                                        
          <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
          <jsp:param name="showLinks" value="false" />
          <jsp:param name="error" value="<%= error%>" />
          <jsp:param name="formName" value="0" />
        </jsp:include>        
        <%=widgetFactory.createHoverHelp("SaveDraftButton","Announcement.SaveAsDraft") %>
        <%=widgetFactory.createHoverHelp("PreviewButton","Announcement.Preview") %>
        <%=widgetFactory.createHoverHelp("DeactivateButton","Announcement.Deactivate") %>
      </div>
      <%= formMgr.getFormInstanceAsInputField("AnnouncementDetailForm", secureParms) %>
    </form>
  </div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />

<script type="text/javascript">
  <%--  on page load/refresh, if all_bogs_ind is true, then check all chcekboxes. --%>
  var count = 0;

  require(["dijit/registry", "dojo/on", "dojo/dom", "dojo/ready", "t360/dialog"],
	      function(registry, on, dom, ready, dialog) {

    ready(function() {
      registry.byId("PreviewButton").on("click", function() {
<%
  //cquinton 3/1/2013 ir#14375 when readonly use value from original bean
  if ( !isReadOnly ) {
%>
      var title = registry.byId('title').value;
      var subject = registry.byId('subject').value;
<%
  }
  else { //isReadOnly
%>
      var title = "<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(title_val))%>";
      var subject = "<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(subject_val))%>";
<%
  }
%>
      var status=  "<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(announcementStatus))%>";
      dialog.open('announcementDialog', 'Announcement - '+ '('+status+')',
        'announcementDialog.jsp',
        ['announcementOid', 'title', 'subject', 'status','critical'], 
        [<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(oid))%>, title, subject, status,registry.byId('critical_ind').checked ], 
        registry.byId('critical_ind').checked);  <%-- callbacks and critical flag --%>
      });
    });
  });

  function setCheckBoxValue(group,value) {
	var allcBox = dijit.byId('all_bogs_ind');
	console.log('invoked for '+group + ", " + value);
	console.log('total bank groups ='+<%=availableBankGroups%>);
	var cBox = dijit.byId(group);
	if (cBox.checked) {
		cBox.set('value',value);	
		cBox.set('checked',true);
		count = count + 1;
	} else{
		cBox.set('value','');
		cBox.set('checked',false);
		count = count -1;
	}
	console.log('count='+count);
}

  function setAllCheckBoxes(size) {
	var allcBox = dijit.byId('all_bogs_ind');
	for (i =0; i < size; i++) {
	var cBox = dijit.byId('bankgroupOid'+i);
	console.log('cbox ='+cBox.name);
	console.log('all_bogs_ind is '+dijit.byId('all_bogs_ind').checked);
	if (dijit.byId('all_bogs_ind').checked) {
		cBox.set('disabled',true);
		cBox.set('checked',true);
		cBox.set('value','');		
	} else{
		cBox.set('value','');
		cBox.set('checked',false);
		cBox.set('disabled',false);
	}
	}
}  
</script>
<div id="announcementDialog" ></div>
</body>

<%
beanMgr.unregisterBean("Announcement");  
formMgr.storeInDocCache("default.doc", new DocumentHandler());

%>

</html>
