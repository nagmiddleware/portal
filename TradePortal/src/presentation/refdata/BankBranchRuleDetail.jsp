<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,
                 com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("***START********************BANK*BRANCH*DETAIL**************************START***");
  /*  userSession.setGlobalNavigationTab(resMgr.getText("NavigationBar.RefData", TradePortalConstants.TEXT_BUNDLE)); */
 
   /****************************************************************
    * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A BankBranchBranch
    * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
    * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
    * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
    * TO THE FORM MANAGER.
    ****************************************************************/

    /*********\
    * GLOBALS *
    \*********/
   boolean isNew = false; //tells if this is a new bank branch rule
   boolean isReadOnly = false; //tell if the page is read only
   boolean getDataFromDoc = false; 
   boolean showSaveButton = false;
   boolean showSaveCloseButton = false;
   boolean showDeleteButton = false;
   boolean showCloseButton = true;
   
   String oid = "";
   String loginRights = null;
   Vector error = null;
   String buttonPressed ="";
   
   //Create  WidgetFactory object 

   WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
     

   //BankBranchRule Web Bean
   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.BankBranchRuleWebBean", "BankBranchRule");
   BankBranchRuleWebBean editBankBranchRule = (BankBranchRuleWebBean)beanMgr.getBean("BankBranchRule");
   
   Hashtable secureParams = new Hashtable();

   DocumentHandler doc = null;

   //Get security rights
   loginRights = userSession.getSecurityRights();
   isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_BANK_BRANCH_RULES);
   Debug.debug("MAINTAIN_BANK_BRANCH_RULES == " + SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_BANK_BRANCH_RULES));

     // Get the document from the cache.  We'll use it to repopulate the screen if the
   // user was entering data with errors.
  
   
   doc = formMgr.getFromDocCache();
   buttonPressed =doc.getAttribute("/In/Update/ButtonPressed");
      
      error = doc.getFragments("/Error/errorlist/error");
   
      Debug.debug("doc from cache is " + doc.toString());

   if (doc.getDocumentNode("/In/BankBranchRule") != null ) {
   // The doc does exist so check for errors.

       String maxError = doc.getAttribute("/Error/maxerrorseverity");
       oid = doc.getAttribute("/Out/BankBranchRule/bank_branch_rule_oid");
       
       if(oid == null || "0".equals(oid) || "".equals(oid)){
       oid =  doc.getAttribute("/In/BankBranchRule/bank_branch_rule_oid");
       }

       if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
       // We've returned from a save/update/delete that was successful
       // We shouldn't be here.  Forward to the RefDataHome page.
         getDataFromDoc = true;
       } else {
       // We've returned from a save/update/delete but have errors.
       // We will display data from the cache.  Determine if we're
       // in insertMode by looking for a '0' oid in the input section
       // of the doc.
           getDataFromDoc = true;
           oid =  doc.getAttribute("/In/BankBranchRule/bank_branch_rule_oid");
           if(oid.equals("0"))
           {
               Debug.debug("Create BankBranchRule Error");
               isNew = true;//else it stays false
           }

           if (!isNew)
           // Not in insert mode, use oid from input doc.
           {
               Debug.debug("Update BankBranchRule Error");
               oid = doc.getAttribute("/In/BankBranchRule/bank_branch_rule_oid");
           }
       }
   }
   if (getDataFromDoc)
   {
       // We create a new docHandler to work around an aparent bug in AMSEntityWebbean
       // This hurts performance and should be removed as soon as populateFromXmlDoc(doc,path) works
       DocumentHandler doc2 = doc.getComponent("/In");
       Debug.debug("Populating bank branch with:\n" + doc2.toString(true));
       editBankBranchRule.populateFromXmlDoc(doc2);
       if (!isNew)
           editBankBranchRule.setAttribute("bank_branch_rule_oid",oid);
           oid = editBankBranchRule.getAttribute("bank_branch_rule_oid");
   }
   else if (request.getParameter("oid") != null) //EDIT BankBranchRule
   {
       isNew = false;
       //This getParameter is done twice to prevent overwriting a valid oid
       oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
       Debug.debug("Editing bankbranch with oid -> " + oid);
       editBankBranchRule.getById(oid);
       Debug.debug("Editing bank branch with oid: " + editBankBranchRule.getAttribute("bank_branch_rule_oid")); //DEBUG
       Debug.debug("isReadOnly == " + isReadOnly);
         }
   else 
    {
       isNew = true;
         oid = null;
    }

   // onLoad is set to default the cursor to the Country field but only if the
   // page is not in isReadOnly mode.
   // Auto save the form when time-out if not readonly.  
   // (Header.jsp will check for auto-save setting of the corporate org).
   String onLoad = "";
   if (!isReadOnly) {
     onLoad = "document.BankBranchRuleDetail.AddressCountry.focus();";
     showSaveButton = true;
     showDeleteButton = true;
     showSaveCloseButton = true;
     showCloseButton = true;
     
   }

   String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

   if (isReadOnly)
   {
      

      
   }
   else if (isNew)
   {
       oid = "0";
       editBankBranchRule.setAttribute("bank_branch_rule_oid",oid);
      

       showSaveButton = true;
       showDeleteButton = false;
       showSaveCloseButton = true;
       showCloseButton = false;
       
   }
%>

<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
  <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="BankBranchRuleDetail.BankBranchRule" />
      <jsp:param name="helpUrl"  value="admin/bank_branch_rules_overview.htm" /> 
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (!isNew) {
    ReferenceDataManager ref = ReferenceDataManager.getRefDataMgr (formMgr.getServerLocation());
    subHeaderTitle = ref.getDescr("PAYMENT_METHOD", editBankBranchRule.getAttribute("payment_method"), userSession.getUserLocale()) + ", " + editBankBranchRule.getAttribute("bank_branch_code");
  }
  else {
    subHeaderTitle = resMgr.getText("BankBranchRuleDetail.NewBankBranchRule", TradePortalConstants.TEXT_BUNDLE);
  } 
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

      <jsp:include page="/common/ErrorSection.jsp" />
            <form name="BankBranchRuleForm" id="BankBranchRuleForm" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">


  <input type=hidden value="" name=buttonName>
  
  <%
           /*******************
            * START SECUREPARAMS
            ********************/
                secureParams.put("BankBranchRuleOid",oid);

            secureParams.put("login_oid",userSession.getUserOid());
            secureParams.put("login_rights",loginRights);
            secureParams.put("opt_lock", editBankBranchRule.getAttribute("opt_lock"));
            /*****************
            * END SECUREPARAMS
            ******************/
            
   %>
  <div class="formArea">
   <div class="formContent">
      <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
      <div class="dijitTitlePaneContentInner">             
                        <div class="columnLeft">
                              <%--   START BankBranchRule TYPE DROPDOWN--%>
                              <%
                              Vector codesToExclude = new Vector();
                              codesToExclude.add(TradePortalConstants.PAYMENT_METHOD_IACC);
               String options2 = Dropdown.createSortedRefDataOptions("PAYMENT_METHOD", editBankBranchRule.getAttribute("payment_method"), resMgr.getResourceLocale(),codesToExclude);
               out.println(widgetFactory.createSelectField("PaymentMethod", "BankBranchRuleDetail.PaymentMethod", " ", options2, isReadOnly,true,false,"class='char35'","","" ));  
                       %>
                              <%-- END BankBranchRule TYPE DROPDOWN --%>
                              
                              <%--   START BANK BRANCH CODE INPUT FIELD--%>
                              <%= widgetFactory.createTextField( "BankBranchCode", "BankBranchRuleDetail.BankBranchCode", editBankBranchRule.getAttribute("bank_branch_code"), "35",isReadOnly, true, false, "class='char35'", "", "" ) %>
                              <%--   END BANK BRANCH CODE INPUT FIELD--%>

                              <%--  START Bank Name INPUT FIELD--%>
                              <%= widgetFactory.createTextField( "BankName", "BankBranchRuleDetail.BankName", editBankBranchRule.getAttribute("bank_name"), "120",isReadOnly, true, false, "class='char35'", "", "" ) %>
                              <%-- END Bank Name INPUT FIELD --%>

                              <%-- START Branch Name INPUT FIELD--%>
                              <%= widgetFactory.createTextField( "BranchName", "BankBranchRuleDetail.BranchName", editBankBranchRule.getAttribute("branch_name"),"70",isReadOnly, false, false, "class='char35'", "", "" ) %>
                              <%--  END Branch Name INPUT FIELD --%>
                        </div>

                        <div class=columnRight>
                              <%-- START ADDRESS1 INPUT FIELD --%>
                              <%= widgetFactory.createTextField( "AddressLine1", "BankBranchRuleDetail.AddressLine1", editBankBranchRule.getAttribute("address_line_1"),"35",isReadOnly, false, false, "", "", "" ) %>
                              <%--  END ADDRESS1 INPUT FIELD--%>


                              <%-- START ADDRESS2 INPUT FIELD--%>
                              <%= widgetFactory.createTextField( "AddressLine2", "BankBranchRuleDetail.AddressLine2", editBankBranchRule.getAttribute("address_line_2"),"35",isReadOnly, false, false, "", "", "" ) %>
                    <%-- END ADDRESS2 INPUT FIELD--%>

                              <%-- START CITY INPUT FIELD--%>
                              <%= widgetFactory.createTextField( "AddressCity", "BankBranchRuleDetail.City", editBankBranchRule.getAttribute("address_city"),"35",isReadOnly, false, false, "", "", "" ) %>
                              <%--  END CITY INPUT FIELD--%>

                              <%-- START PROVINCE INPUT FIELD--%>
                              <%=widgetFactory.createTextField( "AddressStateProvince", "BankBranchRuleDetail.ProvinceState", editBankBranchRule.getAttribute("address_state_province"),"8",isReadOnly, false, false, "", "", "" ) %>
                              <%--  END PROVINCE INPUT FIELD--%>


                              <%-- START COUNTRY DROPDOWN BOX--%>
                              <%
              String options3 = Dropdown.createSortedRefDataOptions("COUNTRY", editBankBranchRule.getAttribute("address_country"),
                                                              resMgr.getResourceLocale());
                  //Debug.debug(options3);
                  out.print(widgetFactory.createSelectField("AddressCountry", "BankBranchRuleDetail.Country", " ", options3, isReadOnly,false,false,"class='char35'","",""));  
                        %>
                              <%--END COUNTRY DROPDOWN BOX--%>
 
 
 


                        </div>
                        </div>
                        </div>
                  </div>  <%-- End FormContent  --%>
                  </div>    <%-- End Formarea  --%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar"
                        data-dojo-props="form: 'BankBranchRuleForm'">
                        <jsp:include page="/common/RefDataSidebar.jsp">
                              <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
                              <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButton %>" />
                              <jsp:param name="showDeleteButton"  value="<%= showDeleteButton %>" />
                              <jsp:param name="cancelAction" value="goToRefDataHome" />
                              <jsp:param name="showHelpButton" value="false" />
                              
                                    <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
                       <jsp:param name="error" value="<%= error%>" /> 

                        </jsp:include>
                  </div>
<%= formMgr.getFormInstanceAsInputField("BankBranchRuleForm", secureParams) %>
</form>




</div>
</div>


<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

</body>
</html>
<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("BankBranchRule");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   Debug.debug("***END********************BankBranchRule*DETAIL**************************END***");
%>
