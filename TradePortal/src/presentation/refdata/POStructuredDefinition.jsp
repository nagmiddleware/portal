
<%--
*******************************************************************************
                       PO Upload Definition Detail Page
  
  Description: This jsp sets up the data being used for the PO Upload Defn.
  The idea is that this jsp will call the jsps that create the 3 tabs associated
  with the PO upload definition.  This jsp will set up the data that is used in 
  the called jsp(s).  The only way a user can get to this page is via Refdata 
  home. 
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>



<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%

Debug.debug("*********************** START: Purchase Order Structure Definition Detail Page **************************");

  String 	poDefinitionOid		= request.getParameter("oid");
  String 	loginLocale 		= userSession.getUserLocale();
  String 	loginTimezone		= userSession.getTimeZone();
  String 	loginRights 		= userSession.getSecurityRights();
  String 	tabPressed			= request.getParameter("tabPressed");	// This is a local attribute from the webBean
  String	refDataHome			= "goToRefDataHome";    				//used for the close button and Breadcrumb link
  String    onLoad				= "";
 
  DocumentHandler defaultDoc		= formMgr.getFromDocCache();
  Hashtable 	secureParms		= new Hashtable();
  boolean	hasEditRights		= SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_PO_UPLOAD_DEFN );
  boolean	isReadOnly			= true;
  boolean  insertMode			= false;
  boolean  showDelete			= true;
  boolean  showSave				= true;
  boolean  showSaveClose        = true;
  boolean showSaveAs            = false;
  boolean saveClose             = true;
  boolean showCreationRule      = SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_ATP_CREATE_RULES);
  boolean  getDataFromDoc		= false;
  boolean  retrieveFromDB       = false;
  String   partName				= "";
  
  
  HashSet poLineItemOptinalVal = new HashSet();
  poLineItemOptinalVal.add("line_item_num");
  poLineItemOptinalVal.add("unit_price");
  poLineItemOptinalVal.add("quantity");


  final String  general			= TradePortalConstants.PO_GENERAL;
  final String  file			= TradePortalConstants.PO_FILE;

  //WebBeans to access data
                             		    
  PurchaseOrderDefinitionWebBean poDefinitionRef = beanMgr.createBean(PurchaseOrderDefinitionWebBean.class, "PurchaseOrderDefinition");
                             		                               		     
//Initialize WidgetFactory.
  WidgetFactory widgetFactory = new WidgetFactory(resMgr); 

  String maxError = defaultDoc.getAttribute("/Error/maxerrorseverity");
  Vector error = null;
  error = defaultDoc.getFragments("/Error/errorlist/error");

String links = "";

  if( hasEditRights ) 			//If the user has the right to Maintain/edit data SET Readonly to False.
	isReadOnly = false;
  
  
  
  if (defaultDoc.getDocumentNode("/In/PurchaseOrderDefinition") == null ) {
	     // The document does not exist in the cache.  We are entering the page from
	     // the listview page (meaning we're opening an existing item or creating
	     // a new one.)
  
     if(poDefinitionOid != null)
      {
    	 poDefinitionOid = EncryptDecrypt.decryptStringUsingTripleDes(poDefinitionOid,userSession.getSecretKey());
      }
     else
      {
    	 poDefinitionOid = null;
      }

     if (poDefinitionOid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       poDefinitionOid = "0";
     } else if (poDefinitionOid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
       showSaveAs = true;
     }    
  }else{
	// The doc does exist so check for errors.  If highest severity >=
	     // WARNING, we had errors.
	     
	     poDefinitionOid = defaultDoc.getAttribute("/In/PurchaseOrderDefinition/purchase_order_definition_oid");
	       if(poDefinitionOid == null || "0".equals(poDefinitionOid) || "".equals(poDefinitionOid)){
	    	   poDefinitionOid = defaultDoc.getAttribute("/Out/PurchaseOrderDefinition/purchase_order_definition_oid");
	       }else if(poDefinitionOid != null && !"0".equals(poDefinitionOid) && !"".equals(poDefinitionOid))
	       {
	    	   getDataFromDoc = false;
	    	   insertMode = false;
	           retrieveFromDB = true;
	           showSaveAs = true;
	           showDelete=true;
	       }
	     
	     if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
	       
	    	 getDataFromDoc = false;
	  	   	 insertMode = false;
	         retrieveFromDB = true;
	         showSaveAs = true;
	         showDelete=true;
	       
	       
	         // We've returned from a save/update/delete that was successful
	         // We shouldn't be here.  Forward to the RefDataHome page.
	         // Should never get here: jPylon and Navigation.xml should take care
	         // of this situation.
	      
	     } else {
	    	 //out.println("error");
	            // We've returned from a save/update/delete but have errors.
	            // We will display data from the cache.  Determine if we're
	            // in insertMode by looking for a '0' oid in the input section
	            // of the doc.
	            retrieveFromDB = false;
	            getDataFromDoc = true;
	            String newOid =
	            	defaultDoc.getAttribute("/In/PurchaseOrderDefinition/purchase_order_definition_oid");
	            
	            if (newOid.equals("0")) {
	               insertMode = true;
	               poDefinitionOid = "0";
	            } else {
	               // Not in insert mode, use oid from output doc.
	               insertMode = false;
	               poDefinitionOid = newOid;
	            }
	      }
	 }
  
  
  if (retrieveFromDB) {
	     // Attempt to retrieve the item.  It should always work.  Still, we'll
	     // check for a blank oid -- this indicates record not found.  Set to insert
	     // mode and display error.

	     getDataFromDoc = false;

	     poDefinitionRef.setAttribute("purchase_order_definition_oid", poDefinitionOid);
	     poDefinitionRef.getDataFromAppServer();

	     if (poDefinitionRef.getAttribute("purchase_order_definition_oid").equals("")) {
	       insertMode = true;
	       poDefinitionOid = "0";
	       getDataFromDoc = false;
	     } else {
	       insertMode = false;
	     }
	}

    if (getDataFromDoc) {
	     // Populate the securityProfile bean with the output doc.
	     try {
	    	 poDefinitionRef.populateFromXmlDoc(defaultDoc.getComponent("/In"));
	     } catch (Exception e) {
	        out.println("Contact Administrator: Unable to get Purchase Order Definition "
	             + "attributes for oid: " + StringFunction.xssCharsToHtml(poDefinitionOid) + " " + e.toString());
	     }
	 }
    //IR T36000018158 start- make prod_chars_ud1_label default to Description if its new definition
    boolean isNew = false;
    String prodCharUD1Label ="";
    if(StringFunction.isNotBlank(poDefinitionOid) && !"0".equals(poDefinitionOid)){
    	prodCharUD1Label =poDefinitionRef.getAttribute("prod_chars_ud1_label");
    	if(resMgr.getText("InvoiceDefinition.Description",TradePortalConstants.TEXT_BUNDLE).equalsIgnoreCase(prodCharUD1Label))
    		isNew =true;
    }
    else{
    	prodCharUD1Label=resMgr.getText("InvoiceDefinition.Description",TradePortalConstants.TEXT_BUNDLE);
    	isNew =true;
    }
    //IR T36000018158 end
  if(InstrumentServices.isBlank( tabPressed )) {
	tabPressed = "General";
  }

  String buttonPressed = defaultDoc.getAttribute("/In/Update/ButtonPressed");

  // TLE - 01/23/07 - IR#FRUH012138122 - Add Begin 
  //secureParms.put("owner_oid",userSession.getOwnerOrgOid());
  String existingOrgOid = poDefinitionRef.getAttribute("owner_org_oid");
  String CurrentOrgOid = userSession.getOwnerOrgOid();

  if(InstrumentServices.isNotBlank( existingOrgOid )) {
      secureParms.put("owner_oid",existingOrgOid);
      if (!CurrentOrgOid.equals(existingOrgOid)) {
         isReadOnly = true;
      }
  } else {
      secureParms.put("owner_oid",userSession.getOwnerOrgOid()); 
  }
  // TLE - 01/23/07 - IR#FRUH012138122 - Add End 

  secureParms.put("oid",poDefinitionOid);
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("login_rights",loginRights);
  secureParms.put("opt_lock", poDefinitionRef.getAttribute("opt_lock"));

  // Auto save the form when time-out if not readonly.  
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;
  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly){
	  showSave = false;
	  showSaveClose = false;
	  showSaveAs = false;
	  showCreationRule = false;
  }
  

%>


<%-- ============================== HTML Starts HERE =================================== --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />
      
<div class="pageMain">		<%-- Page Main Start --%>
  <div class="pageContent">	<%--  Page Content Start --%>

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="POStructureDefinition.POStructureDefinition"/>
      <jsp:param name="helpUrl" value="customer/creating_po_definitions.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if( InstrumentServices.isBlank(poDefinitionOid) || 
		  poDefinitionOid.equals("0")) {
    subHeaderTitle = resMgr.getText("POUploadDefinitionDetail.NewPOUploadDef", 
      TradePortalConstants.TEXT_BUNDLE); 
  }
  else {
    subHeaderTitle = poDefinitionRef.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

<form id="POStructureDefinitionForm" name="POStructureDefinitionForm" data-dojo-type="dijit.form.Form" method="post" 
		action="<%=formMgr.getSubmitAction(response)%>">
	<input type=hidden value="" name=saveAsFlag>
		
  <div class="formArea">
	<jsp:include page="/common/ErrorSection.jsp" />


	<div class="formContent">		<%--  Form Content Start --%>
		<%@ include file="fragments/POStructuredDefinitionDetail-General.frag" %>
		
		
		
		<%-- General Title Panel details start --%>
		<%= widgetFactory.createSectionHeader("3", "POUploadDefinitionDetail.PODefinitionFieldOrder") %>
		
			<%@ include file="fragments/POStructuredDefinitionDetail-Line.frag" %>
		
		</div>
 	</div>		<%--  Form Content End --%>

</div> <%--  Form Area End --%>

	<input type=hidden name ="part_to_validate" value="<%=TradePortalConstants.INV_GENERAL%>">
        <input type="hidden" name="buttonName" value="">
   
   
<%	    

  Debug.debug("*********************** END: PO Upload Definition Detail Page **************************");
%>

   <%= formMgr.getFormInstanceAsInputField("POStructureDefinitionForm", secureParms) %>
<%
	defaultDoc.removeAllChildren("/Error");
        formMgr.storeInDocCache("default.doc", defaultDoc);
%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'POStructureDefinitionForm'">

        <jsp:include page="/common/RefDataSidebar.jsp">
			<jsp:param name="showSaveButton" value="<%= showSave %>" />
			<jsp:param name="showDeleteButton" value="<%= showDelete%>" />
			<jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
			<jsp:param name="showSaveAsButton" value="<%= showSaveAs%>" />
			<jsp:param name="cancelAction" value="goToRefDataHome" />
			<jsp:param name="showHelpButton" value="false" />
			<jsp:param name="helpLink" value="customer/thresh_groups_detail.htm" />
			<jsp:param name="showLinks" value="true" /> 
			<jsp:param name="showCreateCreationRule" value="<%=showCreationRule%>" />
			<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
			<jsp:param name="error" value="<%=error%>" />
            <jsp:param name="saveOnClick" value="none" />
            <jsp:param name="saveCloseOnClick" value="none" />
			<jsp:param name="saveAsDialogPageName" value="poDefinitionSaveAs.jsp" />
            <jsp:param name="saveAsDialogId" value="saveAsDialogId" />				
		</jsp:include>
      
</div> <%--closes sidebar area--%>

  
      
</form>

</div>		<%--  Page Content End --%> 
</div>		<%--  Page Main End --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<div id="saveAsDialogId" style="display: none;"></div>

<script>

function add4MoreDefined(tableToIncrease){
	require(["dojo/dom","dojo/domReady!" ], function(dom) {
	var bType = "seller_users_def";
	var toalUserDef = 10;
	if (tableToIncrease == "poStructBuyerDefinedTableId")
		bType = "buyer_users_def";
	if (tableToIncrease =="poUserDefinitionLineTableId"){
		bType = "prod_chars_ud";
	    toalUserDef = 7;
	}
	
	var table = dom.byId(tableToIncrease);
		
	<%-- get index# for field naming --%>
	var insertRowIdx = table.rows.length;
	var cnt = 0;	  
	  
	for(cnt = 0; cnt<4; cnt++){
	  	<%-- k is used to check the total data item rows. Maximum = 10. --%>
	  	<%-- we need to subtract 2 to remove header lines --%>
	  	var k=insertRowIdx+1-2;
	  	if(k > toalUserDef){
			  break;
	    }
	  	
	    <%-- add the table row --%>
	  	var row = table.insertRow(insertRowIdx);
	  	row.id = bType+(insertRowIdx-1);
   	 	var cell0 = row.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = row.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = row.insertCell(2);<%-- third  cell in the row --%>
		cell2.align="center";

	          cell0.innerHTML ='<input data-dojo-type=\"dijit.form.CheckBox\" name=\"'+bType+k+'_req\" id=\"'+bType+k+'_req\" value=\"Y\">'        
	          cell1.innerHTML ='<input data-dojo-type="dijit.form.ValidationTextBox" name=\"'+bType+k+'_label\" id=\"'+bType+k+'_label\"   maxLength=\"35\" class=\'char25\' value=\"\">'        
			  if(bType == "prod_chars_ud"){
	            cell2.innerHTML ='<label>35</label>'
			  }else{
		        cell2.innerHTML ='<label>140</label>'	
			  }
	            
	        require(["dojo/parser"], function(parser) {
	         	parser.parse(row.id);
	    	 });
		   	insertRowIdx = insertRowIdx+1;
  }  
});
}

function updateDefOrder(gIndex, filedName)
{
	var temp = "";
	var index = 0;
	var temp_upload_order_value = "";
	gIndex = gIndex-1;		
	<%-- Validate text box --%>
	for( i=1; i < gIndex; i++ ){
		textValue = document.getElementById('text'+i).value;
		<%-- Check for in-correct value --%>
		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < 1) || (textValue >= gIndex) ) {
				document.getElementById('text'+i).focus();
				return false;
		}else{
			document.getElementById('text'+i).value = textValue ;
		}

	}

	for(var i=1; i < gIndex; i++){
		<%-- check the entered text box value with the i value  --%>
		if( i != document.getElementById('text'+(i)).value ){
			<%--  check if textbox value's are repeated or not --%>
			
			if(document.getElementById('text'+i).value == document.getElementById('text'+(document.getElementById('text'+i).value)).value){
				document.getElementById('text'+i).focus();
				return false;
			}
			
			<%-- Swap fieldname data --%>
			temp = document.getElementById('fileFieldName'+i).innerHTML;
			document.getElementById('fileFieldName'+ i).innerHTML = document.getElementById('fileFieldName'+document.getElementById('text'+(i)).value).innerHTML;
			document.getElementById("fileFieldName"+document.getElementById('text'+(i)).value).innerHTML = temp;
			
			<%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("upload_order_"+i).value;
			document.getElementById("upload_order_"+i).value = document.getElementById("upload_order_"+(document.getElementById('text'+i).value) ).value;
			document.getElementById("upload_order_"+(document.getElementById('text'+i).value) ).value = temp_upload_order_value;

			<%-- Swap the index value ie, textbox value --%>
			index = document.getElementById('text'+(i)).value
			document.getElementById('text'+(i)).value =  document.getElementById('text'+index).value;
			document.getElementById('text'+index).value = index;
			
		}
		
	}	
	
}

function updateSumDefOrder(gIndex, sIndex, filedName)
{
	var temp = "";
	var index = 0;
	var temp_upload_order_value = "";
	
	sIndex = sIndex - 1;
	gIndex = gIndex -1;
	totalOrder = sIndex + gIndex -2;
	var isSwaped = false;

	for( var j = 1; j < gIndex; j++ ){

		textValue = document.getElementById('userDefText'+(j)).value;
		<%-- Check for in-correct value --%>
		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < sIndex) || (textValue > totalOrder) ) {
				document.getElementById('userDefText'+j).focus();
				return false;
		}else{
			document.getElementById('userDefText'+j).value = textValue ;
		}

	}

	for(var i=1; i < gIndex; i++){
		<%-- check the entered text box value with the i value  --%>
		var userDefTextVal = document.getElementById('userDefText'+(i)).value;
		var invSummGoodsStart = sIndex + i -1;
		if( invSummGoodsStart != userDefTextVal ){
			<%--  check if textbox value's are repeated or not --%>

		   var swapFieldPosition = userDefTextVal - sIndex +1;
			if(document.getElementById('userDefText'+(i)).value == document.getElementById('userDefText'+swapFieldPosition).value){
				document.getElementById('userDefText'+i).focus();
				return false;
			}
			
			<%-- Swap fieldname data --%>
			temp = document.getElementById('userDefFileFieldName'+(i)).innerHTML;					
			document.getElementById('userDefFileFieldName'+ i).innerHTML = document.getElementById('userDefFileFieldName'+ swapFieldPosition).innerHTML;
			document.getElementById("userDefFileFieldName"+ swapFieldPosition).innerHTML = temp;
			
			<%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("upload_userdef_order_"+i).value;
			document.getElementById("upload_userdef_order_"+i).value = document.getElementById("upload_userdef_order_"+ swapFieldPosition).value;
			document.getElementById("upload_userdef_order_"+ swapFieldPosition).value = temp_upload_order_value;

			<%-- Swap the index value ie, textbox value --%>
			index = (document.getElementById('userDefText'+(i)).value);
			document.getElementById('userDefText'+(i)).value =  document.getElementById('userDefText'+ swapFieldPosition).value;
			document.getElementById('userDefText'+ swapFieldPosition).value = index;
			
			isSwaped = true;
		}
	}	
	<%--  update row is according to data after swapping --%>
	if(isSwaped){
		
		var totalPOSummGoodOrderDara = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2); <%--  two row for header --%>
		var rowToModify = "";
		var spanId="";
		var idx;
		for(var row=0; row<totalPOSummGoodOrderDara; row++){
			rowToModify = document.getElementById("poStructSumDataOrderTableId").rows[row+2]; <%--  two for header --%>
			spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
			idx = spanId.indexOf("_label");
			if( idx > -1){
				spanId = spanId.substring(0,idx)
			}
			rowToModify.id = spanId + "_req_order"
		}
	}
}

function updateSumLineItemOrder(gIndex, sIndex, filedName)
{
	var temp = "";
	var index = 0;
	var temp_upload_order_value = "";
	var isSwaped = false;	
	
	sIndex = sIndex - 3;
	gIndex = gIndex -1;
	totalOrder = sIndex + gIndex -2;
	
	for( var j = 1; j < gIndex; j++ ){        
		textValue = document.getElementById('userDefLineText'+j).value;
		<%-- Check for in-correct value --%>
		if( textValue == "" || textValue.length == 0 || isNaN(textValue) || (textValue < sIndex) || (textValue > totalOrder) ) {
				document.getElementById('userDefLineText'+j).focus();
				return false;
		}else{
			document.getElementById('userDefLineText'+j).value = textValue ;
		}

	}

	for(var i=1; i < gIndex; i++){
		<%-- check the entered text box value with the i value  --%>
		var userDefTextVal = document.getElementById('userDefLineText'+(i)).value;
		var invSummLineItemStart = sIndex + i -1;
		if( invSummLineItemStart != userDefTextVal ){
			<%--  check if textbox value's are repeated or not --%>
            var swapFieldPosition = userDefTextVal - sIndex +1;
			if(document.getElementById('userDefLineText'+(i)).value == document.getElementById('userDefLineText'+swapFieldPosition).value){
				document.getElementById('userDefLineText'+i).focus();
				return false;
			}
			
			<%-- Swap fieldname data --%>
			temp = document.getElementById('userDefLineFileFieldName'+(i)).innerHTML;					
			document.getElementById('userDefLineFileFieldName'+ i).innerHTML = document.getElementById('userDefLineFileFieldName'+swapFieldPosition).innerHTML;
			document.getElementById("userDefLineFileFieldName"+swapFieldPosition).innerHTML = temp;
			
			<%-- swap even the ids --%>
            temp_upload_order_value = document.getElementById("upload_userDefLine_order_"+i).value;
			document.getElementById("upload_userDefLine_order_"+i).value = document.getElementById("upload_userDefLine_order_"+swapFieldPosition).value;
			document.getElementById("upload_userDefLine_order_"+swapFieldPosition ).value = temp_upload_order_value;

			<%-- Swap the index value ie, textbox value --%>
			index = (document.getElementById('userDefLineText'+(i)).value);
			document.getElementById('userDefLineText'+(i)).value =  document.getElementById('userDefLineText'+swapFieldPosition).value;
			document.getElementById('userDefLineText'+swapFieldPosition).value = index;
			
			isSwaped = true;
		}
	}	
	
	<%--  update row is according to data after swapping --%>
	if(isSwaped){
		
		var totalPOLineItemOrderDara = (document.getElementById("poStructLineItemOrderTableId").rows.length) - (2); <%--  two row for header --%>
		var rowToModify = "";
		var spanId="";
		var idx;
		for(var row=0; row<totalPOLineItemOrderDara; row++){
			rowToModify = document.getElementById("poStructLineItemOrderTableId").rows[row+2]; <%--  two for header --%>
			spanId = rowToModify.cells[1].getElementsByTagName("span")[0].id;
			idx = spanId.indexOf("_label");
			if( idx > -1){
				spanId = spanId.substring(0,idx)
			}
			rowToModify.id = spanId + "_req_order"
		}
	}
}

function reorderPOSummLineItemOrder(){
	
	var invSummLineItemOrderCount = (document.getElementById("poStructLineItemOrderTableId").rows.length) - (2);
	if(invSummLineItemOrderCount > 0){
		var invSummDataOrderCount = (document.getElementById("poStructDataFileOrderTableId").rows.length) - (2);
		var invSummGoodsOrderCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
		for(var i=1; i <= invSummLineItemOrderCount; i++){
			document.getElementById("userDefLineText" + i).value = (invSummDataOrderCount) + (invSummGoodsOrderCount) + (i);
		}		
	}	
}

require(["dojo/_base/array", "dojo/dom",
         "dojo/dom-construct", "dojo/dom-class", 
         "dojo/dom-style", "dojo/query",
         "dijit/registry", "dojo/on", 
         "t360/common",
         "dojo/ready", "dojo/NodeList-traverse"],
    function(arrayUtil, dom,
             domConstruct, domClass,
             domStyle, query,
             registry, on, 
             common, 
             ready ) {
	
	
	 function deleteOrderField(deleteRow, table, idName){
			
			var tabSize = (table.rows.length)- (2); <%--  two row for header --%>
			var replaceRow = deleteRow.rowIndex;
			for(var row= replaceRow; row <= tabSize; row++){
				table.rows[ row ].id = table.rows[ row + 1 ].id;
				table.rows[ row ].cells[0].childNodes[1].value = table.rows[ row+1 ].cells[0].childNodes[1].value;
				table.rows[ row ].cells[1].innerHTML = table.rows[ row+1 ].cells[1].innerHTML;
			}
			table.deleteRow(tabSize+1);	
			var idNumberToDestroy = (table.rows.length) - (1);
			registry.byId(idName + idNumberToDestroy).destroy();
			
             if ("userDefText" == idName){
				 reorderPOSummLineItemOrder();
			 }
		  }
		
		     <%-- add most functions to thisPage for convenience of access --%>
	         var thisPage = {
		    		 	        		 
	        		 selectPOSummGoodsItem: function(checkboxWidget) {
	         			
	        			 var invSummGoodsDataTable = document.getElementById("poStructSumDataOrderTableId");
	        			 var rowCount = invSummGoodsDataTable.rows.length;
	        			 var row = invSummGoodsDataTable.insertRow(rowCount);
	        			 
	                     var invSummOrderDataCount = (document.getElementById("poStructDataFileOrderTableId").rows.length) - (2);
	        			 
	        			 var cell1 = row.insertCell(0);
	        			 var cell2 = row.insertCell(1);
	        			
	        			 var invSummGoodsOrderCount = rowCount-1;
	        			 
	        			 var totalOrder = invSummOrderDataCount + invSummGoodsOrderCount;        			 
	        			 var DefCharCheckbox = new dijit.form.TextBox({
	        			        name: "",
	        			        id: "userDefText"+ invSummGoodsOrderCount,
	        			        style:"width: 25px;",
	        			        value: totalOrder
	        			 	});

	        			    cell1.appendChild(DefCharCheckbox.domNode);
	        			    cell1.align="center";
	        			    
	        			    var val1 = document.createElement("input"); 
	        			    val1.type = 'hidden';
	        			    val1.id = 'upload_userdef_order_'+invSummGoodsOrderCount;
	        			    val1.name = 'po_summary_field'+invSummGoodsOrderCount;
	        			           			            			            			   
	        			    cell2.id = "userDefFileFieldName"+invSummGoodsOrderCount;
	        			    cell2.align = "left";
	        			    cell2.className = "formItem";
	        			    
	        			    var checkboxId = checkboxWidget.id;
	        			    row.id = checkboxId + "_order";
	        		        var idx = checkboxId.indexOf("_req");
	        			    
	        			    var val2 = document.createElement("span"); 
	        			    val2.className = "deleteSelectedPOStructureDefinitipnItem";

	        		        
	        			    if( idx != -1){   
	        			    	 var addSectionId = checkboxId.substring(0,idx);
	        			    	 var userdef = checkboxId.indexOf("_users_def");
	        			    	 if(userdef != -1){
	        			    	   var DBValue =  addSectionId.replace("users_def", "user_def");
	        			    	   val1.value = DBValue + "_label";
	        			    	   val2.id =  addSectionId + "_label";
	          			    	   cell1.appendChild(val1);
	          			    	   cell2.innerHTML = '<label>' + document.getElementById(addSectionId + "_label").value + '</label>';  
	          			    	   cell2.appendChild(val2);          			    	 
	        			    	 }else{
	        			    		 var DBValue = addSectionId;
	        			    		 if("incoterm_loc" == addSectionId){
	        			    			 DBValue = "incoterm_location";
		          			    	 }else if("partial_ship_allowed" == addSectionId){
		          			    		 DBValue = "partial_shipment_allowed";  	          			    		 
		          			    	 }else if("goods_desc" == addSectionId){
		          			    		 DBValue = "goods_description";  
		          			        }
	          			    	   val1.value = DBValue;	          			    	   
	          			    	   val2.id = addSectionId;
	        			    	   cell1.appendChild(val1);
	        			    	   cell2.innerHTML = '<label>' + document.getElementById(addSectionId).value + '</label>';  
	        			    	   cell2.appendChild(val2);        			    		 
	        			    	 }
	        			    	 reorderPOSummLineItemOrder();
	        			    }
	        		    
	        		 },
	        		 
	        		 selectPOSummLineItem: function(checkboxWidget) {
	          			
	        			 var invSummLineDataTable = document.getElementById("poStructLineItemOrderTableId");
	        			 var rowCount = invSummLineDataTable.rows.length;
	        			 var row = invSummLineDataTable.insertRow(rowCount);
	        			 
	                     var invSummOrderDataCount = (document.getElementById("poStructDataFileOrderTableId").rows.length) - (2);
	                     var invSummGoodsrderDataCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
	        			 
	        			 var cell1 = row.insertCell(0);
	        			 var cell2 = row.insertCell(1);
	        			
	        			 var invSummLineOrderCount = rowCount-1;
	        			 
	        			 var totalOrder = invSummOrderDataCount + invSummGoodsrderDataCount + invSummLineOrderCount;        			 
	        			 var DefCharCheckbox = new dijit.form.TextBox({
	        			        name: "",
	        			        id: "userDefLineText"+ invSummLineOrderCount,
	        			        style:"width: 25px;",
	        			        value: totalOrder
	        			 	});

	        			    cell1.appendChild(DefCharCheckbox.domNode);
	        			    cell1.align="center";
	        			    
	        			    var val1 = document.createElement("input"); 
	        			    val1.type = 'hidden';
	        			    val1.id = 'upload_userDefLine_order_'+invSummLineOrderCount;
	        			    val1.name = 'po_line_item_field'+invSummLineOrderCount;
	        			           			            			            			   
	        			    cell2.id = "userDefLineFileFieldName"+invSummLineOrderCount;
	        			    cell2.align = "left";
	        			    cell2.className = "formItem";
	        			    
	        			    var checkboxId = checkboxWidget.id;
	        			    row.id = checkboxId + "_order";
	        		        var idx = checkboxId.indexOf("_req");
	        			    
	        			    var val2 = document.createElement("span"); 
	        			    val2.className = "deleteSelectedPOStructureDefinitipnItem";
	        		        
	        			    if( idx != -1){   
	        			    	   var addSectionId = checkboxId.substring(0,idx);
	        			    	   var userdef = checkboxId.indexOf("prod_chars");
	        			    	   if(userdef != -1){
		        			    	   val1.value = addSectionId + "_label";
		        			    	   val2.id = addSectionId + "_label";
		          			    	   cell1.appendChild(val1);
		          			    	   cell2.innerHTML = '<label>' + document.getElementById(addSectionId + "_label").value + '</label>';  
		          			    	   cell2.appendChild(val2);          			    	 
		        			    	 }else{
		        			    		var DBValue = addSectionId;
		        			    		if("quantity_var_plus" == addSectionId){
		        			    			DBValue = "quantity_variance_plus";
			          			        }
		          			    	   val1.value = DBValue;
		          			    	   val2.id = addSectionId;
		        			    	   cell1.appendChild(val1);
		        			    	   cell2.innerHTML = '<label>' + document.getElementById(addSectionId).value + '</label>';  
		        			    	   cell2.appendChild(val2);        			    		 
		        			    	 }
	        			    }	        			    	        			    	        			    	        			    	        			    	        			    
	        		    
	        		 },
	        		 
	        		 deleteEventHandler: function(deleteButton, tableID, orderId) {

	        		        <%-- uncheck the available item --%>
	        		        <%-- this automatically does the onchange mechanism there --%>
	        		        <%-- including removing this item and decreasing the total count --%>
	        		       <%--  var deleteSelectItem = deleteButton.parentNode; --%>
	        		        var deleteSelectItemId = deleteButton.id;
	   			    	    var userdef = deleteSelectItemId.indexOf("_label");
	   			    	    if(userdef != -1){
	   			    	    	deleteSelectItemId = deleteSelectItemId.substring(0, userdef);
	   			    	    }   
	        		        var checkbox = registry.byId(deleteSelectItemId+'_req');   
	        		        if ( checkbox ) {
	        		          checkbox.set('checked',false);
	        		        }
	        		      
							var row = document.getElementById(deleteSelectItemId + '_req_order');
						    deleteOrderField(row, tableID, orderId);	
	        		 }
		     
		     
		}
	
      ready(function() {
    	  
    	  if(document.getElementById("line_item_detail_provided").checked){
    		  setPOLineItemCheckBox(true); 
    	  }
    	  
	       query('#poStructDataDefinitionTableId').on("#incoterm_req:click", function(checkValue) {
	 	          if ( this.checked ) {
	 	   	       thisPage.selectPOSummGoodsItem(this);
	 	           }
	 	          else {
	 	   	       var row = document.getElementById("incoterm_req_order");
	 	   	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
	 	          }
	 	        });
	       
 	       query('#poStructDataDefinitionTableId').on("#incoterm_loc_req:click", function(checkValue) {
  	          if ( this.checked ) {
  	   	       thisPage.selectPOSummGoodsItem(this);
  	           }
  	          else {
  	   	       var row = document.getElementById("incoterm_loc_req_order");
  	   	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
  	          }
  	        });
 	       
 	       query('#poStructDataDefinitionTableId').on("#latest_shipment_date_req:click", function(checkValue) {
  	          if ( this.checked ) {
  	   	       thisPage.selectPOSummGoodsItem(this);
  	           }
  	          else {
  	   	       var row = document.getElementById("latest_shipment_date_req_order");
  	   	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
  	          }
  	        });
 	       
 	       query('#poStructDataDefinitionTableId').on("#partial_ship_allowed_req:click", function(checkValue) {
  	          if ( this.checked ) {
  	   	       thisPage.selectPOSummGoodsItem(this);
  	           }
  	          else {
  	   	       var row = document.getElementById("partial_ship_allowed_req_order");
  	   	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
  	          }
  	        });
 	       
 	       query('#poStructDataDefinitionTableId').on("#goods_desc_req:click", function(checkValue) {
  	          if ( this.checked ) {
  	   	       thisPage.selectPOSummGoodsItem(this);
  	           }
  	          else {
  	   	       var row = document.getElementById("goods_desc_req_order");
  	   	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
  	          }
  	        });
 		   
 	       query('#poStructBuyerDefinedTableId').on("#buyer_users_def1_req:click", function(checkValue) {
 	          if ( this.checked ) {
	        	  if(checkLabelVal(document.getElementById("buyer_users_def1_label").value, document.POStructureDefinitionForm.buyer_users_def1_req,
    			  'Buyer User Def Label 1')){
	        		  thisPage.selectPOSummGoodsItem(this);
	        	  }	
 	           }
 	          else {
 	   	       var row = document.getElementById("buyer_users_def1_req_order");
 	   	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 	          }
 	        });
 	       
 	       query('#poStructBuyerDefinedTableId').on("#buyer_users_def2_req:click", function(checkValue) {
 	          if ( this.checked ) {
	        	  if(checkLabelVal(document.getElementById("buyer_users_def2_label").value, document.POStructureDefinitionForm.buyer_users_def2_req,
    			  'Buyer User Def Label 2')){
	        		  thisPage.selectPOSummGoodsItem(this);
	        	  }	
 	           }
 	          else {
 	   	       var row = document.getElementById("buyer_users_def2_req_order");
 	   	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 	          }
 	        });
 		   		  	     
        query('#poStructBuyerDefinedTableId').on("#buyer_users_def3_req:click", function(checkValue) {
          if ( this.checked ) {
        	  if(checkLabelVal(document.getElementById("buyer_users_def3_label").value, document.POStructureDefinitionForm.buyer_users_def3_req,
			  'Buyer User Def Label 3')){
        		  thisPage.selectPOSummGoodsItem(this);
        	  }	
         }
         else {
  	       var row = document.getElementById("buyer_users_def3_req_order");
  	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
         }
       });
        

       query('#poStructBuyerDefinedTableId').on("#buyer_users_def4_req:click", function(checkValue) {
         if ( this.checked ) {
       	  if(checkLabelVal(document.getElementById("buyer_users_def4_label").value, document.POStructureDefinitionForm.buyer_users_def4_req,
		  'Buyer User Def Label 4')){
    		  thisPage.selectPOSummGoodsItem(this);
    	  }	
          }
         else {
  	       var row = document.getElementById("buyer_users_def4_req_order");
  	       deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
         }
       });

     query('#poStructBuyerDefinedTableId').on("#buyer_users_def5_req:click", function(checkValue) {
       if ( this.checked ) {
     	  if(checkLabelVal(document.getElementById("buyer_users_def5_label").value, document.POStructureDefinitionForm.buyer_users_def5_req,
		  'Buyer User Def Label 5')){
    		  thisPage.selectPOSummGoodsItem(this);
    	  }	
        }
      else {
  	    var row = document.getElementById("buyer_users_def5_req_order");
  	    deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
       }
     });

     query('#poStructBuyerDefinedTableId').on("#buyer_users_def6_req:click", function(checkValue) {
       if ( this.checked ) {
     	  if(checkLabelVal(document.getElementById("buyer_users_def6_label").value, document.POStructureDefinitionForm.buyer_users_def6_req,
		  'Buyer User Def Label 6')){
    		  thisPage.selectPOSummGoodsItem(this);
    	  }	
       }
       else {
  	     var row = document.getElementById("buyer_users_def6_req_order");
  	     deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
        }
      });

     query('#poStructBuyerDefinedTableId').on("#buyer_users_def7_req:click", function(checkValue) {
       if ( this.checked ) {
     	  if(checkLabelVal(document.getElementById("buyer_users_def7_label").value, document.POStructureDefinitionForm.buyer_users_def7_req,
		  'Buyer User Def Label 7')){
    		  thisPage.selectPOSummGoodsItem(this);
    	  }	
       }
      else {
  	  var row = document.getElementById("buyer_users_def7_req_order");
  	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
      }
     });

    query('#poStructBuyerDefinedTableId').on("#buyer_users_def8_req:click", function(checkValue) {
      if ( this.checked ) {
    	  if(checkLabelVal(document.getElementById("buyer_users_def8_label").value, document.POStructureDefinitionForm.buyer_users_def8_req,
		  'Buyer User Def Label 8')){
    		  thisPage.selectPOSummGoodsItem(this);
    	  }	
       }
      else {
  	     var row = document.getElementById("buyer_users_def8_req_order");
  	    deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
      }
    });

    query('#poStructBuyerDefinedTableId').on("#buyer_users_def9_req:click", function(checkValue) {
      if ( this.checked ) {
    	  if(checkLabelVal(document.getElementById("buyer_users_def9_label").value, document.POStructureDefinitionForm.buyer_users_def9_req,
		  'Buyer User Def Label 9')){
    		  thisPage.selectPOSummGoodsItem(this);
    	  }	
      }
      else {
  	    var row = document.getElementById("buyer_users_def9_req_order");
  	    deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
      }
    });

   query('#poStructBuyerDefinedTableId').on("#buyer_users_def10_req:click", function(checkValue) {
      if ( this.checked ) {
    	  if(checkLabelVal(document.getElementById("buyer_users_def10_label").value, document.POStructureDefinitionForm.buyer_users_def10_req,
		  'Buyer User Def Label 10')){
    		  thisPage.selectPOSummGoodsItem(this);
    	  }	
      }
     else {
  	    var row = document.getElementById("buyer_users_def10_req_order");
  	    deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
     }
    });


		  query('#poStructSellerDefinedTableId').on("#seller_users_def1_req:click", function(checkValue) {
		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def1_label").value, document.POStructureDefinitionForm.seller_users_def1_req,
        			  'Seller User Def Label 1')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
		          }
		          else {
	        	 var row = document.getElementById("seller_users_def1_req_order");
	        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
		          }
		        });
		  
			  query('#poStructSellerDefinedTableId').on("#seller_users_def2_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def2_label").value, document.POStructureDefinitionForm.seller_users_def2_req,
        			  'Seller User Def Label 2')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def2_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def3_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def3_label").value, document.POStructureDefinitionForm.seller_users_def3_req,
        			  'Seller User Def Label 3')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def3_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
 			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def4_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def4_label").value, document.POStructureDefinitionForm.seller_users_def4_req,
        			  'Seller User Def Label 4')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	 		        	  
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def4_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
 			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def5_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def5_label").value, document.POStructureDefinitionForm.seller_users_def5_req,
        			  'Seller User Def Label 5')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def5_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
 			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def6_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def6_label").value, document.POStructureDefinitionForm.seller_users_def6_req,
        			  'Seller User Def Label 6')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def6_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
 			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def7_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def7_label").value, document.POStructureDefinitionForm.seller_users_def7_req,
        			  'Seller User Def Label 7')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def7_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
 			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def8_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def8_label").value, document.POStructureDefinitionForm.seller_users_def8_req,
        			  'Seller User Def Label 8')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def8_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
 			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def9_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def9_label").value, document.POStructureDefinitionForm.seller_users_def9_req,
        			  'Seller User Def Label 9')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def9_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });
 			  
 			  query('#poStructSellerDefinedTableId').on("#seller_users_def10_req:click", function(checkValue) {
 		          if ( this.checked ) {
		        	  if(checkLabelVal(document.getElementById("seller_users_def10_label").value, document.POStructureDefinitionForm.seller_users_def10_req,
        			  'Seller User Def Label 10')){
		        		  thisPage.selectPOSummGoodsItem(this);
		        	  }	
 		          }
 		          else {
		        	 var row = document.getElementById("seller_users_def10_req_order");
		        	  deleteOrderField(row, document.getElementById("poStructSumDataOrderTableId"), "userDefText");	
 		          }
 		        });

			  query('#poSysDefinitionLineTableId').on("#unit_of_measure_req:click", function(checkValue) {
 		          if ( this.checked ) {
						        thisPage.selectPOSummLineItem(this);
						    }
						    else {
						         var row = document.getElementById("unit_of_measure_req_order");
						        deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
						    }
					});
			  
			  query('#poSysDefinitionLineTableId').on("#quantity_var_plus_req:click", function(checkValue) {
 		          if ( this.checked ) {
						        thisPage.selectPOSummLineItem(this);
						    }
						    else {
						         var row = document.getElementById("quantity_var_plus_req_order");
						        deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
						    }
					});
			  
				  query('#poUserDefinitionLineTableId').on("#prod_chars_ud1_req:click", function(checkValue) {
	 		          if ( this.checked ) {
	 		        	  if(checkLabelVal(document.getElementById("prod_chars_ud1_label").value, document.POStructureDefinitionForm.prod_chars_ud1_req,
	 	    			  'Product Chars UD 1 Type/Value Label')){
	 		        		 thisPage.selectPOSummLineItem(this);
	 		        	  }	
				     }
					else {
					      var row = document.getElementById("prod_chars_ud1_req_order");
						  deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
				     }
				});
				  
				  query('#poUserDefinitionLineTableId').on("#prod_chars_ud2_req:click", function(checkValue) {
	 		          if ( this.checked ) {
	 		        	  if(checkLabelVal(document.getElementById("prod_chars_ud2_label").value, document.POStructureDefinitionForm.prod_chars_ud2_req,
	 	    			  'Product Chars UD 2 Type/Value Label')){
	 		        		 thisPage.selectPOSummLineItem(this);
	 		        	  }	
					   }
					   else {
							 var row = document.getElementById("prod_chars_ud2_req_order");
							 deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
				       }
			       });
				  
				  query('#poUserDefinitionLineTableId').on("#prod_chars_ud3_req:click", function(checkValue) {
	 		          if ( this.checked ) {
	 		        	  if(checkLabelVal(document.getElementById("prod_chars_ud3_label").value, document.POStructureDefinitionForm.prod_chars_ud3_req,
	 	    			  'Product Chars UD 3 Type/Value Label')){
	 		        		 thisPage.selectPOSummLineItem(this);
	 		        	  }	
					   }
				      else {
							 var row = document.getElementById("prod_chars_ud3_req_order");
							 deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
				     }
			     });
				  
			  query('#poUserDefinitionLineTableId').on("#prod_chars_ud4_req:click", function(checkValue) {
 		          if ( this.checked ) {
 		        	  if(checkLabelVal(document.getElementById("prod_chars_ud4_label").value, document.POStructureDefinitionForm.prod_chars_ud4_req,
 	    			  'Product Chars UD 4 Type/Value Label')){
 		        		 thisPage.selectPOSummLineItem(this);
 		        	  }	
			      }
				   else {
						 var row = document.getElementById("prod_chars_ud4_req_order");
						 deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
				}
			   });
 
			  query('#poUserDefinitionLineTableId').on("#prod_chars_ud5_req:click", function(checkValue) {
 		          if ( this.checked ) {
 		        	  if(checkLabelVal(document.getElementById("prod_chars_ud5_label").value, document.POStructureDefinitionForm.prod_chars_ud5_req,
 	    			  'Product Chars UD 5 Type/Value Label')){
 		        		 thisPage.selectPOSummLineItem(this);
 		        	  }	
				   }
			      else {
				     var row = document.getElementById("prod_chars_ud5_req_order");
				     deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
				  }
			});
							   							   
		  query('#poUserDefinitionLineTableId').on("#prod_chars_ud6_req:click", function(checkValue) {
		          if ( this.checked ) {
 		        	  if(checkLabelVal(document.getElementById("prod_chars_ud6_label").value, document.POStructureDefinitionForm.prod_chars_ud6_req,
 	    			  'Product Chars UD 6 Type/Value Label')){
 		        		 thisPage.selectPOSummLineItem(this);
 		        	  }	
				   }
				   else {
				         var row = document.getElementById("prod_chars_ud6_req_order");
					     deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
				  }
		});
								   
		  query('#poUserDefinitionLineTableId').on("#prod_chars_ud7_req:click", function(checkValue) {
		          if ( this.checked ) {
 		        	  if(checkLabelVal(document.getElementById("prod_chars_ud7_label").value, document.POStructureDefinitionForm.prod_chars_ud7_req,
 	    			  'Product Chars UD 7 Type/Value Label')){
 		        		 thisPage.selectPOSummLineItem(this);
 		        	  }	
				}
			      else {
						var row = document.getElementById("prod_chars_ud7_req_order");
					    deleteOrderField(row, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");	
					}
		 });							   
 		   
 		<%-- delete selected item event handler --%>
		    query('#poStructSumDataOrderTableId').on(".deleteSelectedPOStructureDefinitipnItem:click", function(evt) {
		      thisPage.deleteEventHandler(this, document.getElementById("poStructSumDataOrderTableId"), "userDefText");
		      
		    });
 		
 		
		  <%-- delete selected item event handler --%>
		    query('#poStructLineItemOrderTableId').on(".deleteSelectedPOStructureDefinitipnItem:click", function(evt) {
		      thisPage.deleteEventHandler(this, document.getElementById("poStructLineItemOrderTableId"), "userDefLineText");
		    }); 
		  
			   var lineItemNotRequiredCheckBox = registry.byId("line_item_detail_provided");
			      if ( lineItemNotRequiredCheckBox ) {
			    	  lineItemNotRequiredCheckBox.on("click", function(checkValue) {
			          if ( this.checked ) {
			        	  
			        	  var tableId = document.getElementById("poStructLineItemOrderTableId");
			        	  var OrderId = "userDefLineText";
			        	  
			        	  var unitOfMsr = document.getElementById("unit_of_measure_req_order");
			        	  if(unitOfMsr){
			        		dijit.getEnclosingWidget(document.getElementById("unit_of_measure_req")).set('checked',false);
						    deleteOrderField(unitOfMsr, tableId, OrderId);
			        	  }
			        	  
			        	  var qtyVaPls = document.getElementById("quantity_var_plus_req_order");
			        	  if(qtyVaPls){
			        		  dijit.getEnclosingWidget(document.getElementById("quantity_var_plus_req")).set('checked',false);
							  deleteOrderField(qtyVaPls, tableId, OrderId);
				          }
			        	  
			        	  var prodChar1 = document.getElementById("prod_chars_ud1_req_order");
			        	  if(prodChar1){
			        		    dijit.getEnclosingWidget(document.getElementById("prod_chars_ud1_req")).set('checked',false);
							    deleteOrderField(prodChar1, tableId, OrderId);
				        	}
			        	  
			        	  var prodChar2 = document.getElementById("prod_chars_ud2_req_order");
			        	  if(prodChar2){
			        		    dijit.getEnclosingWidget(document.getElementById("prod_chars_ud2_req")).set('checked',false);
							    deleteOrderField(prodChar2, tableId, OrderId);
				          }
			        	  
			        	  var prodChar3 = document.getElementById("prod_chars_ud3_req_order");
			        	  if(prodChar3){
			        		    dijit.getEnclosingWidget(document.getElementById("prod_chars_ud3_req")).set('checked',false);
							    deleteOrderField(prodChar3, tableId, OrderId);
				        	  }
			        	  
			        	  var prodChar4 = document.getElementById("prod_chars_ud4_req_order");
			        	  if(prodChar4){
			        		    dijit.getEnclosingWidget(document.getElementById("prod_chars_ud4_req")).set('checked',false);
							    deleteOrderField(prodChar4, tableId, OrderId);
				           }
			        	  
			        	  var prodChar5 = document.getElementById("prod_chars_ud5_req_order");
			        	  if(prodChar5){
			        		    dijit.getEnclosingWidget(document.getElementById("prod_chars_ud5_req")).set('checked',false);
							    deleteOrderField(prodChar5, tableId, OrderId);
				        	  }
			        	  
			        	  var prodChar6 = document.getElementById("prod_chars_ud6_req_order");
			        	  if(prodChar6){
			        		    dijit.getEnclosingWidget(document.getElementById("prod_chars_ud6_req")).set('checked',false);
							    deleteOrderField(prodChar6, tableId, OrderId);
				        	  }
			        	  
			        	  var prodChar7 = document.getElementById("prod_chars_ud7_req_order");
			        	  if(prodChar7){
			        		    dijit.getEnclosingWidget(document.getElementById("prod_chars_ud7_req")).set('checked',false);
							    deleteOrderField(prodChar7, tableId, OrderId);
				        	  }	
			        	  
			        	  document.getElementById("line_item_num_req_order").style.display = "none";
			              document.getElementById("unit_price_req_order").style.display = "none";
			              document.getElementById("quantity_req_order").style.display = "none"	;
			              
			              setPOLineItemCheckBox(true);
			          }
			          else {
			        	  if(document.getElementById("line_item_num_req_order")){
			        	      document.getElementById("line_item_num_req_order").style.display = "table-row";
			                  document.getElementById("unit_price_req_order").style.display = "table-row";
			                  document.getElementById("quantity_req_order").style.display = "table-row"	;
			                  
			                  setPOLineItemCheckBox(false);
				        		
			        	  }else{
			        		  
			        		   setPOLineItemCheckBox(false);			        		  
			        		   var invSummLineDataTable = document.getElementById("poStructLineItemOrderTableId");
			        		   var rowCount = invSummLineDataTable.rows.length;
			        		   
			        		   var invSummOrderDataCount = (document.getElementById("poStructDataFileOrderTableId").rows.length) - (2);
			                   var invSummGoodsrderDataCount = (document.getElementById("poStructSumDataOrderTableId").rows.length) - (2);
			                   
			                   var totalOrder = invSummOrderDataCount + invSummGoodsrderDataCount;
			                   
			                   var row;
		        		       var cell1;
			        		   var cell2;
			        		   
			        		   
			        		   var defaultVal = ["line_item_num", "unit_price", "quantity"];
			        		   var defaultLabel = [ '<%=resMgr.getTextEscapedJS("POStructureDefinition.LineItemNumber", TradePortalConstants.TEXT_BUNDLE)%>',
			        		                        '<%=resMgr.getTextEscapedJS("POStructureDefinition.LineItemUnitPrice", TradePortalConstants.TEXT_BUNDLE)%>',
			        		                        '<%=resMgr.getTextEscapedJS("POStructureDefinition.LineItemDetailQuantity", TradePortalConstants.TEXT_BUNDLE)%>'];
			                     
			        		   for(var i=1; i<=3; i++){
			        		       row = invSummLineDataTable.insertRow(rowCount+i-1);
			        		       row.id = defaultVal[i-1] + "_req_order";
			        		       cell1 = row.insertCell(0);
			        		       cell1.align="center";
			        		       
				        		   cell2 = row.insertCell(1);
				        		  
				        		   cell2.id = "userDefLineFileFieldName" + i;
				        		   cell2.align = "left"
				        		   
				        		   cell1.innerHTML =  '<input type=hidden id=\"upload_userDefLine_order_'+i+'\" name=\"po_line_item_field'+i+'\" value=\"'+defaultVal[i-1]+'\"><input type=\"text\" name=\"\" value=\"'+(totalOrder + i)+'\" data-dojo-type=\"dijit.form.TextBox\" data-dojo-props=\"trim:true\" id=\"userDefLineText'+i+'\"  style=\"width: 25px;\" >';
				        		   cell2.innerHTML = '<label>'+defaultLabel[i-1]+'</label> <span class=\"\" id=\"'+defaultVal[i-1]+'\"></span>';
			        		   
				        		   require(["dojo/parser"], function(parser) {
					       	         	parser.parse(row.id);
					       	    	 });
			        		   }    
			        	  
			        	  }
			          }
			       });
			    }
     
      });
  
	  function checkLabelVal(labelVal, reqCheck, errorText) {
	      if(labelVal.replace(/^\s+|\s+$/g, '') == ""){		        		  
	          alert("'" + errorText + "' is required");
	          var checkedObj = dijit.getEnclosingWidget(reqCheck)
	          checkedObj.set('checked',false);
	          return false;
	       }          
	     return true;
	   }
	         
  });
  
function submitSaveAs() 
{
        if(window.document.saveAsForm.newDefinitionName.value == ""){
          return false;
        }
          document.POStructureDefinitionForm.name.value=window.document.saveAsForm.newDefinitionName.value
          <%--  Sets a hidden field with the 'button' that was pressed.  This
          value is passed along to the mediator to determine what
          action should be done.  --%>
          document.POStructureDefinitionForm.saveAsFlag.value = "Y";            
          require(["dijit/registry","t360/common"],function(registry,common) {
                common.setButtonPressed("SaveCloseTrans",'0');
                document.forms[0].setAttribute("method","post");              
                document.forms[0].submit();
          });               
          hideDialog("saveAsDialogId");             
 } 
 
function setPOLineItemCheckBox(isChecked) 
{
    document.POStructureDefinitionForm.unit_of_measure_req.disabled = isChecked;  
	document.POStructureDefinitionForm.quantity_var_plus_req.disabled = isChecked;  
	document.POStructureDefinitionForm.prod_chars_ud1_req.disabled = isChecked;  
	document.POStructureDefinitionForm.prod_chars_ud2_req.disabled = isChecked;  
	document.POStructureDefinitionForm.prod_chars_ud3_req.disabled = isChecked; 
	if(document.POStructureDefinitionForm.prod_chars_ud4_req)
	document.POStructureDefinitionForm.prod_chars_ud4_req.disabled = isChecked; 
	if(document.POStructureDefinitionForm.prod_chars_ud5_req)
	document.POStructureDefinitionForm.prod_chars_ud5_req.disabled = isChecked; 
	if(document.POStructureDefinitionForm.prod_chars_ud6_req)
	document.POStructureDefinitionForm.prod_chars_ud6_req.disabled = isChecked;  
	if(document.POStructureDefinitionForm.prod_chars_ud7_req)
	document.POStructureDefinitionForm.prod_chars_ud7_req.disabled = isChecked;   
	
	dijit.byId("add4MoreLineDefined").set("disabled", isChecked);
	dijit.byId("updateUserLineOrder").set("disabled", isChecked);
 }
 
</script>


</body>
</html>
