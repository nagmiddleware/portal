
<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
  String parmValue = parmValue = request.getParameter("marginIndex");
  int marginIndex = 0;
  if ( parmValue != null ) {
    try {
    	marginIndex = (new Integer(parmValue)).intValue();
    } 
    catch (Exception ex ) {
      //todo
    }
  }

  String loginLocale = userSession.getUserLocale();
  boolean isReadOnly = false;
  boolean isFromExpress = false;

  //other necessaries
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  //when included per ajax, the business objects will be blank
  // we want 4 of them
  List<CustomerMarginRuleWebBean> marginRuleList = new ArrayList<CustomerMarginRuleWebBean>();
  marginRuleList.add(beanMgr.createBean(CustomerMarginRuleWebBean.class,"CustomerMarginRule") );
  marginRuleList.add(beanMgr.createBean(CustomerMarginRuleWebBean.class,"CustomerMarginRule") );
  marginRuleList.add(beanMgr.createBean(CustomerMarginRuleWebBean.class,"CustomerMarginRule") );
  marginRuleList.add(beanMgr.createBean(CustomerMarginRuleWebBean.class,"CustomerMarginRule") );
%>

<%@ include file="fragments/CorporateCustomerMarginRows.frag" %>
