<%--
***************************************************************************************************
Threshold Groups

Description:
      Displays Threshold Groups based on the Corporate Institution user.
      This page may be retrieved from several actions:
                  Creating New Threshold Group from main Reference Data page
                  Selecting a Threshold Group in the Threshold Group listview
                  Errors resulting from Saving and Deleting the Threshold Group
            
Request Parameters:
      Oid - Oid of Threshold Group selected from listview
      /In/ThresholdGroup - ThresholdGroup node if returning from transaction
      

***************************************************************************************************
--%>

<%-- ****** Begin Page imports, bean imports declaration ******* --%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.cache.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ****** End Page imports, bean imports declaration  ******* --%>


<%--  ****** Begin data retrieval/display logic ****** --%>
<%
      WidgetFactory widgetFactory = new WidgetFactory(resMgr);
      
  DocumentHandler doc;
  DocumentHandler errorDoc = new DocumentHandler();
  ThresholdGroupWebBean thresholdGroup;
  String thresholdGroupOid = null;
  
  boolean insertMode=false;
  boolean getFromDB=false;
  boolean getFromDoc=false;
      
  boolean isReadOnly=false;
  boolean displayErrors=false;
  
  boolean showCloseButton=true;
  boolean showSaveCloseButton=false;
  boolean showDeleteButton=false;
  boolean showSaveButton=false;
  boolean showSaveAsButton=false;
   
  boolean nullValue=true;
  boolean debugMode=true;
  
  String loginLocale;
  String loginRights;
  
  String formattedAmount="";
  String baseCurrency;
  
  String buttonPressed = "";
  Vector error = null;

  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

  // retrieve user's corporate base currency

  baseCurrency = userSession.getBaseCurrencyCode();

  Debug.debug("Login locale is " + loginLocale);
  isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_THRESH_GRP);
  
  // if user does not have access to view this page, return back to Reference
  // Home page
  if (!SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_OR_MAINTAIN_THRESH_GRP))
  {
      formMgr.setCurrPage("RefDataHome");
      String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request); 
  %>
      <jsp:forward page='<%= physicalPage %>' />
  <%
  }

  // register Threshold Group bean

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.ThresholdGroupWebBean", "ThresholdGroup");
  thresholdGroup = (ThresholdGroupWebBean) beanMgr.getBean("ThresholdGroup");
      
  doc = formMgr.getFromDocCache();
 
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  error = doc.getFragments("/Error/errorlist/error");
      
  if (doc.getDocumentNode("/In/ThresholdGroup") == null)
  {
      // This must come from the Reference Data page 
      if(request.getParameter("oid") != null)
       {
            thresholdGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());  
       }
      else
       {
            thresholdGroupOid = null;
       }  

      if (thresholdGroupOid == null )
      {
            insertMode=true;
      }
      else if (thresholdGroupOid.equals("0"))
      {
            insertMode=true;
      }
      else
      {
            getFromDB=true;   
      }
  }
  else
  {
         // The doc does exist so check for errors.
             String maxError = doc.getAttribute("/Error/maxerrorseverity");
             thresholdGroupOid = doc.getAttribute("/Out/ThresholdGroup/threshold_group_oid");
             
             if(thresholdGroupOid == null || "0".equals(thresholdGroupOid) || "".equals(thresholdGroupOid)){
               thresholdGroupOid =  doc.getAttribute("/In/ThresholdGroup/threshold_group_oid");
             }

             if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
             // We've returned from a save/update/delete that was successful
             // We shouldn't be here.  Forward to the RefDataHome page.
               getFromDB = false; 
               getFromDoc = true;
             } else {
             // We've returned from a save/update/delete but have errors.
             // We will display data from the cache.  Determine if we're
             // in insertMode by looking for a '0' oid in the input section
             // of the doc.
                 getFromDB = false; 
                 displayErrors = true;
                   getFromDoc = true;
                   thresholdGroupOid =  doc.getAttribute("/In/ThresholdGroup/threshold_group_oid");
                 if(thresholdGroupOid.equals("0"))
                 {
                     Debug.debug("Create BankBranchRule Error");
                     insertMode = true;//else it stays false
                 }

                 if (!insertMode)
                 // Not in insert mode, use oid from input doc.
                 {
                     Debug.debug("Update BankBranchRule Error");
                     thresholdGroupOid = doc.getAttribute("/In/ThresholdGroup/threshold_group_oid");
                 }
             }
      /*if(!getFromDB){
          displayErrors = true;
          getFromDoc = true;
            
          thresholdGroupOid = doc.getAttribute("/In/ThresholdGroup/threshold_group_oid");
      
          if (thresholdGroupOid == null)
          {
              insertMode=true;
          }
          else if (thresholdGroupOid.equals("0"))
          {
              insertMode=true;
            }
      }*/
  }

  if (getFromDB)
  {
      thresholdGroup.getById(thresholdGroupOid);      
  }

  if (getFromDoc)
  {
      thresholdGroup.populateFromXmlDoc(doc.getComponent("/In"));
  }

  if (insertMode)
  {
       
      thresholdGroupOid="0";
  }
  
  //IAZ CR-475 06/21/09 Begin
  //if users of this corp (as driven by Client Bank) cannot modify thier own profiles,
  //make sure the mode isReadOnly if user access his/her own profile  
  Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());      
  String canModifyProfile = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_OWN_PROFILE_IND");
  if (TradePortalConstants.INDICATOR_NO.equals(canModifyProfile))
  {
      try
      {
            UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");      
            thisUser.setAttribute("user_oid", userSession.getUserOid());
            thisUser.getDataFromAppServer();
            if (thisUser.getAttribute("threshold_group_oid").equals(thresholdGroupOid))
            {
                  isReadOnly = true;
            }
      }
      catch (Exception any)
      {
             // we can't detarmine if user is trying to chnage his own security profile - we must not allow
             // user to do any updates
             System.out.println("Exception verifying if user can edit threshold profile. Will set readOnly mode.");
             isReadOnly = true;       
      }
  }  
  //IAZ CR-475 06/21/09 End

  // determine which buttons to show
  if (!isReadOnly)
  {
      showSaveButton=true;
      showSaveCloseButton=true;
      if (!insertMode)
            showDeleteButton=true;
            showSaveAsButton=true;
  }
  if(insertMode){
        showSaveAsButton=false;
  }
  
Debug.debug(" getFromDoc: " + getFromDoc + " getFromDB: " + getFromDB +
                              " insertMode " + insertMode + 
                              "thresholdGroupOid: " + thresholdGroupOid);
      
    
%>
<%--  ****** End data retrieval/display logic  ****** --%>

<%--  ****** Begin HTML  ****** --%>

<%
      String onLoad="";
    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).

    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
  <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>


<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="SecondaryNavigation.ThresholdGroup" />
      <jsp:param name="helpUrl"  value="customer/thresh_groups_detail.htm" />
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (insertMode) {
    subHeaderTitle = resMgr.getText("ThresholdGroupDetail.NewThresholdGroup", TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = thresholdGroup.getAttribute("threshold_group_name");
  }
%>

    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

                  <form id="ThresholdGroupDetail" name="ThresholdGroupDetail" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
                        <input type=hidden value="" name=buttonName>
                        <input type=hidden value="" name=saveAsFlag>
                        <input type=hidden value="<%=thresholdGroupOid %>" name=threshold_group_oid>
<%

// build parameters for inclusion to the ThresholdGroupDetailForm
// which is used by the mediator, eg. security rights and locks

Hashtable addlParms = new Hashtable();
if (!insertMode)
      addlParms.put("threshold_group_oid", thresholdGroupOid);
else
      addlParms.put("threshold_group_oid", "0");


addlParms.put("owner_org_oid", userSession.getOwnerOrgOid());
addlParms.put("corp_org_oid", userSession.getOwnerOrgOid());
addlParms.put("login_oid", userSession.getUserOid());
addlParms.put("login_rights", userSession.getSecurityRights());
addlParms.put("opt_lock", thresholdGroup.getAttribute("opt_lock"));
%>

<%= formMgr.getFormInstanceAsInputField("ThresholdGroupDetailForm", addlParms) %>

  <div class="formArea">
  <jsp:include page="/common/ErrorSection.jsp" />

  <div class="formContent">
      <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
      <div class="dijitTitlePaneContentInner">             
      <%= widgetFactory.createTextField("ThresholdGroupName", "ThresholdGroupDetail.ThresholdGroupName", 
            thresholdGroup.getAttribute("threshold_group_name"), "35",isReadOnly, true, false, "", "","") %>
    
    <div class="formItem">    
          <i> <%= widgetFactory.createSubLabel("ThresholdGroupDetail.BlankEntryNote") %><br/>
            <%= widgetFactory.createSubLabel("ThresholdGroupDetail.ZeroEntryNote") %>      </i>
      </div>
      
  
  <%-- Begin Table with Threshold Group Amounts Entry --%>
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <thead>
      <th style="text-align:left;">
            <%=resMgr.getText("ThresholdGroupDetail.InstrumentType",TradePortalConstants.TEXT_BUNDLE)%>
        </th>
      <th  style="text-align:left;">
            <%=resMgr.getText("ThresholdGroupDetail.TransactionType",TradePortalConstants.TEXT_BUNDLE)%>
        </th>
      <th  style="text-align:left;">
            <%=resMgr.getText("ThresholdGroupDetail.Currency",TradePortalConstants.TEXT_BUNDLE)%>
        </th>
      <th  style="text-align:left;">
            <%=resMgr.getText("ThresholdGroupDetail.ThresholdAmount",TradePortalConstants.TEXT_BUNDLE)%>
        </th>
      <th  style="text-align:left;">
            <%=resMgr.getText("ThresholdGroupDetail.DailyLimit",TradePortalConstants.TEXT_BUNDLE)%>
        </th>
    </thead>
    
    <tr> 
      <td colspan="6" class="tableSubHeader">
            <%=resMgr.getText("common.trade",TradePortalConstants.TEXT_BUNDLE)%>
      </td>
    </tr>
    
    <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.AirwayBill") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.AirwayBillReleaseIss") %>' />    
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='AirwayBillIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("airwaybill_issue_thold")%>' />
            <jsp:param name="limitName" value='AirwayBillIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("airwaybill_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />           
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />          
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ApprovaltoPay") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ApprovalToPayIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("approval_to_pay_issue_thold")%>' />
            <jsp:param name="limitName" value='ApprovalToPayIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("approval_to_pay_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />           
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="3" />          
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Amend") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ApprovalToPayAmendThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("approval_to_pay_amend_thold")%>' />
            <jsp:param name="limitName" value='ApprovalToPayAmendDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("approval_to_pay_amend_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ATPResponse") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ApprovalToPayDiscrRespThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("approval_to_pay_discr_thold")%>' />
            <jsp:param name="limitName" value='ApprovalToPayDiscrRespDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("approval_to_pay_discr_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ExportCollection") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ExportCollectionIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("export_collection_issue_thold")%>' />
            <jsp:param name="limitName" value='ExportCollectionIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("export_collection_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="2" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value="" /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Amend") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ExportCollectionAmendThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("export_collection_amend_thold")%>' />
            <jsp:param name="limitName" value='ExportCollectionAmendDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("export_collection_amend_dlimit")%>' /> 
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />    
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />          
      </jsp:include>
      <%-- Vasavi CR 524 03/31/2010 Begin --%>
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.NewExportCollection") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='NewExportCollectionIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("new_export_coll_issue_thold")%>' />
            <jsp:param name="limitName" value='NewExportCollectionIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("new_export_coll_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="2" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value="" /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Amend") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='NewExportCollectionAmendThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("new_export_coll_amend_thold")%>' />
            <jsp:param name="limitName" value='NewExportCollectionAmendDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("new_export_coll_amend_dlimit")%>' /> 
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />                
      </jsp:include>
      
            <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ExportLC") %>' />   
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.IssueTransfer") %>' />     
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="ExportLCIssueThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("export_LC_issue_thold")%>' /> 
            <jsp:param name="limitName" value="ExportLCIssueDailyLimit" /> 
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("export_LC_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="4" />                
      </jsp:include>
      <%-- ANZ UAT IR6266 BEGIN --%>
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.AmendTransfer") %>' />     
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="ExportLCAmendTransferThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("export_LC_amend_transfer_thold")%>' />
            <jsp:param name="limitName" value="ExportLCAmendTransferDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("export_LC_amend_transfer_dlim")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />    
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />          
      </jsp:include>
<%-- ANZ UAT IR6266 END --%>
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Assignment") %>' />  
            <jsp:param name="currency"    value='<%=baseCurrency%>' />
            <jsp:param name="locale"      value='<%=loginLocale%>' />
            <jsp:param name="thresholdName"     value="ExportLCIssueAssignThreshold" /> 
            <jsp:param name="thresholdValue"    value='<%=thresholdGroup.getAttribute("export_LC_issue_assign_thold")%>' />
            <jsp:param name="limitName"         value="ExportLCIssueAssignDailyLimit" />
            <jsp:param name="limitValue"        value='<%=thresholdGroup.getAttribute("export_LC_issue_assign_dlimit")%>' /> 
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor"         value='false' />  
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />          
      </jsp:include>

      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DiscrepancyResponse") %>' />     
            <jsp:param name="currency"    value='<%=baseCurrency%>' />
            <jsp:param name="locale"      value='<%=loginLocale%>' />
            <jsp:param name="thresholdName"     value="ExportLCDiscrRespThreshold" /> 
            <jsp:param name="thresholdValue"    value='<%=thresholdGroup.getAttribute("export_LC_discr_thold")%>' />
            <jsp:param name="limitName"         value="ExportLCDiscrRespDailyLimit" />
            <jsp:param name="limitValue"        value='<%=thresholdGroup.getAttribute("export_LC_discr_dlimit")%>' /> 
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor"         value='false' />  
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />          
      </jsp:include>
      
      <%-- SSikhakolli - Rel-9.4 CR-818 - Begin --%>
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ImportCollection") %>' />
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.SettlementInstructions") %>' />
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="ImportColThreshold" />
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("imp_col_set_instr_thold")%>' />
            <jsp:param name="limitName" value="ImportColDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("imp_col_set_instr_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value="true" />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />          
      </jsp:include>
      <%-- SSikhakolli - Rel-9.4 CR-818 - End --%>
      
      <% String value = widgetFactory.createSubLabel("ThresholdGroupDetail.ImportLC"); %>
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ImportLC") %>' />
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' />
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="ImportLCIssueThreshold" />
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("import_LC_issue_thold")%>' />
            <jsp:param name="limitName" value="ImportLCIssueDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("import_LC_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value="true" />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="3" />          
      </jsp:include>

      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Amend") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="ImportLCAmendThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("import_LC_amend_thold")%>' />
            <jsp:param name="limitName" value="ImportLCAmendDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("import_LC_amend_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DiscrepancyResponseSettlementInstructions") %>' />     
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="ImportLCDiscrRespThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("import_LC_discr_thold")%>' />
            <jsp:param name="limitName" value="ImportLCDiscrRespDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("import_LC_discr_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.IncomingStandbyLC") %>' />   
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DiscrepancyResponse") %>' />     
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="IncomingSLCDiscrRespThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("inc_standby_LC_discr_thold")%>' /> 
            <jsp:param name="limitName" value="IncomingSLCDiscrRespDailyLimit" /> 
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("inc_standby_LC_discr_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.LoanRequest") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='LoanRequestIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("loan_request_issue_thold")%>' />
            <jsp:param name="limitName" value='LoanRequestIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("loan_request_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="2" />                
      </jsp:include>
		
		<%-- SSikhakolli - Rel-9.4 CR-818 - Begin --%>      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.SettlementInstructions") %>' />     
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="LoanReqSetInstrThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("loan_request_set_instr_thold")%>' />
            <jsp:param name="limitName" value="LoanReqSetInstrDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("loan_request_set_instr_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      <%-- SSikhakolli - Rel-9.4 CR-818 - End --%>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Guarantee") %>' />   
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="GuaranteeIssueThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("guarantee_issue_thold")%>' />
            <jsp:param name="limitName" value="GuaranteeIssueDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("guarantee_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="2" />                
      </jsp:include>

      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Amend") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='GuaranteeAmendThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("guarantee_amend_thold")%>' />
            <jsp:param name="limitName" value='GuaranteeAmendDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("guarantee_amend_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>

      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.StandbyLC") %>' />
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="StandbyLCIssueThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("standby_LC_issue_thold")%>' />
            <jsp:param name="limitName" value="StandbyLCIssueDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("standby_LC_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />           
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="3" />                      
      </jsp:include>

      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Amend") %>' />       
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="StandbyLCAmendThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("standby_LC_amend_thold")%>' />
            <jsp:param name="limitName" value="StandbyLCAmendDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("standby_LC_amend_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=""/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DiscrepancyResponse") %>' />           
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value="StandbyLCDiscrRespThreshold" /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("standby_LC_discr_thold")%>' />
            <jsp:param name="limitName" value="StandbyLCDiscrRespDailyLimit" />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("standby_LC_discr_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>

      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ShippingGuarantee") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ShipGuaranteeIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("ship_guarantee_issue_thold")%>' />
            <jsp:param name="limitName" value='ShipGuaranteeIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("ship_guarantee_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />           
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />          
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.RequestAdvise") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='RequestAdviseIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("request_advise_issue_thold")%>' />
            <jsp:param name="limitName" value='RequestAdviseIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("request_advise_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="3" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Amend") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='RequestAdviseAmendThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("request_advise_amend_thold")%>' />
            <jsp:param name="limitName" value='RequestAdviseAmendDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("request_advise_amend_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DiscrepancyResponse") %>' />     
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='RequestAdviseDiscrRespThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("request_advise_discr_thold")%>' />
            <jsp:param name="limitName" value='RequestAdviseDiscrRespDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("request_advise_discr_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>

      <tr> 
      <td colspan="6" class="tableSubHeader">
            <%=resMgr.getText("common.payment",TradePortalConstants.TEXT_BUNDLE)%>
      </td>
    </tr>
    
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.FundsTransferRequest") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='FundsTransferIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("funds_transfer_issue_thold")%>' />
            <jsp:param name="limitName" value='FundsTransferIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("funds_transfer_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DomesticPayment") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='DomesticPaymentThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("domestic_payment_issue_thold")%>' />
            <jsp:param name="limitName" value='DomesticPaymentDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("domestic_payment_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.TransferBetweenAccounts") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.Issue") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='TransferBetweenAccountsThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("transfer_btw_accts_issue_thold")%>' />
            <jsp:param name="limitName" value='TransferBetweenAccountsDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("transfer_btw_accts_issue_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />                
      </jsp:include>
      
      <%-- Narayan CR913 04-Feb-2014 Rel9.0 ADD -  Begin --%>
      <tr> 
       <td colspan="6" class="tableSubHeader">
            <%=resMgr.getText("common.UploadedInvoices",TradePortalConstants.TEXT_BUNDLE)%>
       </td>
      </tr>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.RecInvoices") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='UploadRecInvThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("upload_rec_inv_thold")%>' />
            <jsp:param name="limitName" value='UploadRecInvDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("upload_rec_inv_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="3" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.PayInvoices") %>' />   
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='UploadPayInvThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("upload_pay_inv_thold")%>' />
            <jsp:param name="limitName" value='UploadPayInvDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("upload_pay_inv_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      <%-- Narayan CR913 04-Feb-2014 Rel9.0 ADD -  End --%>
      <%--vdesingu Rel 9.2 CR-914A Start --%>
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.PayCreditNotes") %>' />   
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='PayCreditNoteThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("pay_credit_note_thold")%>' />
            <jsp:param name="limitName" value='PayCreditNoteDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("pay_credit_note_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      <%--vdesingu Rel 9.2 CR-914A End --%>
      
      <tr> 
      <td colspan="6" class="tableSubHeader">
            <%=resMgr.getText("common.receivables",TradePortalConstants.TEXT_BUNDLE)%>
      </td>
    </tr>
    
    <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ReceivablesManagement") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ARMMatchResponse") %>' />  
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ARMatchResponseThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("ar_match_response_thold")%>' />
            <jsp:param name="limitName" value='ARMatchResponseDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("ar_match_response_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="5" />                
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ARMApproveDiscount") %>' />      
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ARApproveDiscountThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("ar_approve_discount_thold")%>' />
            <jsp:param name="limitName" value='ARApproveDiscountDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("ar_approve_discount_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ARMCloseInvoice") %>' />   
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ARCloseInvoiceThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("ar_close_invoice_thold")%>' />
            <jsp:param name="limitName" value='ARCloseInvoiceDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("ar_close_invoice_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
            
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ARMFinanceInvoice") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ARFinanceInvoiceThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("ar_finance_invoice_thold")%>' />
            <jsp:param name="limitName" value='ARFinanceInvoiceDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("ar_finance_invoice_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />    
      </jsp:include>
      <%-- Chandrakanth CR 451 CM 10/21/2008 Begin --%>
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value=''/>
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.ARMDisputeOrUnDisputeInvoice") %>' />  
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='ARDisputeOrUnDisputeInvoiceThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("ar_dispute_invoice_thold")%>' />
            <jsp:param name="limitName" value='ARDisputeOrUnDisputeInvoiceDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("ar_dispute_invoice_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='false' />
            <jsp:param name="span" value="false" />   
            <jsp:param name="rowSpan" value="0" />          
      </jsp:include>
      <%-- Narayan CR913 04-Feb-2014 Rel9.0 ADD -  Begin --%>
      <tr> 
       <td colspan="6" class="tableSubHeader">
            <%=resMgr.getText("common.payables",TradePortalConstants.TEXT_BUNDLE)%>
       </td>
      </tr>
      
      <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.PayablesMgmt") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.PayablesInvoices") %>' /> 
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='PybMgmInvThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("pyb_mgm_inv_thold")%>' />
            <jsp:param name="limitName" value='PybMgmInvDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("pyb_mgm_inv_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />     
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />                
      </jsp:include>
      <%-- Narayan CR913 04-Feb-2014 Rel9.0 ADD -  End --%>
      
    <tr> 
      <td colspan="6" class="tableSubHeader">
            <%=resMgr.getText("common.directdebit",TradePortalConstants.TEXT_BUNDLE)%>
      </td>
    </tr>
    
    <jsp:include page="/refdata/ThresholdGroupRow.jsp">
            <jsp:param name="instrumentType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DirectDebit") %>' /> 
            <jsp:param name="transactionType" value='<%= widgetFactory.createSubLabel("ThresholdGroupDetail.DirectDebitIssue") %>' />  
            <jsp:param name="currency" value='<%=baseCurrency%>' />
            <jsp:param name="locale" value='<%=loginLocale%>' />
            <jsp:param name="thresholdName" value='DirectDebitIssueThreshold' /> 
            <jsp:param name="thresholdValue" value='<%=thresholdGroup.getAttribute("direct_debit_thold")%>' />
            <jsp:param name="limitName" value='DirectDebitIssueDailyLimit' />
            <jsp:param name="limitValue" value='<%=thresholdGroup.getAttribute("direct_debit_dlimit")%>' />
            <jsp:param name="isReadOnly" value='<%=isReadOnly%>' />
            <jsp:param name="displayErrors" value='<%=displayErrors%>' />
            <jsp:param name="greyColor" value='true' />           
            <jsp:param name="span" value="true" />    
            <jsp:param name="rowSpan" value="1" />          
      </jsp:include>
  </table>
  <%-- End Table with Threshold Group Amounts Entry --%>
  </div> 
  </div> 
  </div> <%--closes formContent area--%>   
  </div><%--formArea--%>

      <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'ThresholdGroupDetail'">
            <jsp:include page="/common/RefDataSidebar.jsp">
                  <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
                   <jsp:param name="saveOnClick" value="none" />
              <jsp:param name="showDeleteButton" value="<%= showDeleteButton %>" />
              <jsp:param name="showSaveAsButton" value="<%= showSaveAsButton %>" />
              <jsp:param name="showSaveCloseButton" value="<%= showSaveCloseButton %>" />
              <jsp:param name="saveCloseOnClick" value="none" />
              <jsp:param name="cancelAction" value="goToRefDataHome" />
              <jsp:param name="showHelpButton" value="false" />
                  <jsp:param name="helpLink" value="customer/thresh_groups_detail.htm" />
            <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
            <jsp:param name="error" value="<%= error%>" /> 
            <jsp:param name="saveAsDialogPageName" value="ThresholdGroupSaveAs.jsp" />
                  <jsp:param name="saveAsDialogId" value="thresholdGroupSaveAsDialogId" />
                  <jsp:param name="showTop" value="true" />  
                  <jsp:param name="showTips" value="true" />   
            </jsp:include>    
      </div> <%--closes sidebar area--%>
</form>
</div> <%--closes pageContent area--%>
</div> <%--closes pageMain area--%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%
  //save behavior defaults to without hsm
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
   //String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";

 
%>
  

<jsp:include page="/common/RefDataSidebarFooter.jsp" />


<div id="thresholdGroupSaveAsDialogId" style="display: none;"></div>

<%
  String focusField = "ThresholdGroupName";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
  
  
  function submitSaveAs() 
  {
          if(document.thresholdSaveAsForm.newThresholdName.value == ""){
            return false;
          }
            document.ThresholdGroupDetail.ThresholdGroupName.value=window.document.thresholdSaveAsForm.newThresholdName.value
            <%-- Sets a hidden field with the 'button' that was pressed.  This --%>
            <%-- value is passed along to the mediator to determine what --%>
            <%-- action should be done. --%>
            document.ThresholdGroupDetail.buttonName.value = "SaveCloseTrans";
            document.ThresholdGroupDetail.threshold_group_oid.value = "0";
            console.log("New ThresholdGroupName ="+document.ThresholdGroupDetail.ThresholdGroupName.value);
            console.log("New threshold_group_oid ="+document.ThresholdGroupDetail.threshold_group_oid.value);
            document.ThresholdGroupDetail.submit();
            hideDialog("thresholdGroupSaveAsDialogId");
   }
  
 
  function cancelOne(){
      
      require(["t360/dialog"], 
                  function(dialog) {
      dialog.hide('thresholdGroupSaveAsDialogId');

  });
      }
 
</script>
<%
  }
%>

</body>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("ThresholdGroup");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>
<%--  ****** End HTML  ****** --%>