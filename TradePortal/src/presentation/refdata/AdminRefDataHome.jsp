<%--
*******************************************************************************
                                  Admin Ref Data Home

  Description:
     Display a tab metaphor for Admin Users, Admin Security Profiles, and Locked
  Out Users.  
  The default tab is the Admin Users.  Clicking either tab displays a listview of
  the appropriate data.
     Clicking the tab send the user back to this page.  A parameter ('tab')
  indicates which tab to display.
*******************************************************************************
--%>   

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated UsersAndSecurityHome
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
	if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN)){
		userSession.setCurrentPrimaryNavigation("AdminNavigationBar.UsersAndSecurity");
	}else{
	   userSession.setCurrentPrimaryNavigation("NavigationBar.AdminRefData");
	}
	
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  	String parentOrgID    = userSession.getOwnerOrgOid();
  	String bogID          = userSession.getBogOid();
  	String clientBankID   = userSession.getClientBankOid();
  	String globalID       = userSession.getGlobalOrgOid();
  	String ownershipLevel = userSession.getOwnershipLevel();
	//Getting the resources for Dialog Titles
  	String UnlockUser = resMgr.getText("UnlockUserPopUp.title", TradePortalConstants.TEXT_BUNDLE);
  	String newButtonText = "";
  	String newLink;
  	String buttonImageName = "";
  	String current2ndNav;
   	String userOn;
  	String profileOn;
  	String lockedUsersOn;
	DocumentHandler selectOrgDoc = null;
	String dropdownOptions = null;
	String selectedBankOid = null;
	String filterTextFirst = null;
	String filterTextLast = null;
	StringBuffer searchCriteria = new StringBuffer();
	String loginRights = userSession.getSecurityRights();
  	boolean viewAccess = false;
  	boolean maintainAccess = false;
  	//Added for all the options
  	String OrganisationSelector =resMgr.getTextEscapedJS("AdminUserSearch.OrganisationSelector",TradePortalConstants.TEXT_BUNDLE);
  	String initSearchParms="";
  	String searchParams="";
  	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  	DGridFactory dGridFactory = new DGridFactory(resMgr, userSession, formMgr, beanMgr, response);
  	String gridHtml = "";
  	String gridLayout = "";	
  		String titleKey ="";
  	String helpUrl="";
  	//cquinton 10/8/2012 include gridShowCounts
  	String gridId = "";
  	
 // jgadela 10/01/2014 R9.1 T36000032989  -- Vara code task - Encrypt the parameters.
  //'tab' will be 'user', 'profile', 'lockedUsers' or nothing.  'user' or nothing is Admin User,
    // 'profile' is Security  Profile.  Based on the tab, set up variables that 
    // drive the listview.
    if (request.getParameter("current2ndNav") != null) //a type of refdata has been selected
    {
        //Rel 9.0 IR#T36000030061
        current2ndNav = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("current2ndNav"), userSession.getSecretKey());
        session.setAttribute("adminRefData2ndNav", current2ndNav);
    }
    else {
  	     // We've either come to this page the first time or have returned from else
  	    // where (e.g., Cancel from Admin Security Profile Detail).  Check the session
  	    // to see if we store the tab there.  If so, use it.  Otherwise, default to user.
  	    current2ndNav = (String) session.getAttribute("adminRefData2ndNav");
  	    if (current2ndNav == null) current2ndNav = "user";
    }

  	String helpLink = "";
  	// Add context to the performance statistic so that we can determine which tab the user is on
  	PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

  	if((perfStat != null) && (perfStat.isLinkAction())){
        perfStat.setContext(current2ndNav);
  	}

  	Map searchQueryMap =  userSession.getSavedSearchQueryData();
   	Map savedSearchQueryData =null; 
  	if (current2ndNav.equals("user")) {
	    gridId="AdminUserDataGridId";
	    gridHtml = dGridFactory.createDataGrid(gridId,"AdminUserDataGrid", null);
	    gridLayout = dGridFactory.createGridLayout(gridId,"AdminUserDataGrid");
	    helpLink = "admin/admin_users.htm";
	    current2ndNav = "user";
	    viewAccess = SecurityAccess.hasRights(loginRights,
	                                 SecurityAccess.VIEW_OR_MAINTAIN_ADM_USERS);
	    maintainAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.MAINTAIN_ADM_USERS);
	    newLink = formMgr.getLinkAsUrl("goToAdminUserOrgSelection",response);
	    newButtonText = "RefDataHome.CreateNewUser";
	    buttonImageName = "common.CreateNewAdminUserImg";
    	// The contents of the listview is different based on the organizational 
	    // level of the user.
	    //    Global org users can see all global org users and all client bank users
	    //    Client bank users can see all user for that client bank and all
	    //         BOG users of that bank
	    //    BOG users can only see other users of that BOG
 
	    userOn = TradePortalConstants.INDICATOR_YES;
	    profileOn = TradePortalConstants.INDICATOR_NO;
	    lockedUsersOn = TradePortalConstants.INDICATOR_NO; 
	
		selectedBankOid = request.getParameter("BankOid");
		Debug.debug("Selected Bank Oid in state "	+ selectedBankOid);
		filterTextFirst = request.getParameter("FilterTextFirst");
		filterTextLast = request.getParameter("FilterTextLast");
	
		// Now, look at the state for the listview (if it exists) and get the search
		// criteria the user entered.  If we found it, extract the user's values 
		// into the search parms and redisplay on the page.
	
		if (StringFunction.isNotBlank(selectedBankOid)) {
			// Store this value in the search criteria
			searchCriteria.append("BankOid=");
			searchCriteria.append(selectedBankOid);
			searchCriteria.append("|");
		}
		if (StringFunction.isNotBlank(filterTextFirst) || StringFunction.isNotBlank(filterTextLast)) {
			// Store this value in the search criteria
			searchCriteria.append("FilterTextFirst=");
			searchCriteria.append(filterTextFirst);
			searchCriteria.append("|");
			searchCriteria.append("FilterTextLast=");
			searchCriteria.append(filterTextLast);
		}  
	 StringBuffer sql = new StringBuffer();
 	 List<Object> sqlParams = new ArrayList<Object>();

     	if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
	         // For global org users, the list of orgs is the global org plus all
	         // client banks
	         sql.append("select organization_oid, name from global_organization");
	         sql.append(" where activation_status='ACTIVE'");
	         sql.append(" union ");
	         sql.append("select organization_oid, name from client_bank");
	         sql.append(" where activation_status='ACTIVE'");
      	} else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
	         // For Client Bank users, the list of orgs is the user's client bank
	         // plus all BOGs for that client bank
	         sql.append("select organization_oid, name from client_bank");
	         sql.append(" where organization_oid = ?");
	         sql.append(" and activation_status='ACTIVE'");
	         sql.append(" union ");
	         sql.append("select organization_oid, name from bank_organization_group");
	         sql.append(" where p_client_bank_oid = ?");
	         sql.append(" and activation_status='ACTIVE'");
	         sqlParams.add(userSession.getClientBankOid());
	         sqlParams.add(userSession.getClientBankOid());
      	} else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
	         // For BOG users, the list of orgs is only the user's bog.  (Note
	         // this is provided only for consistency.  The org field is not a
	         // visible field for BOG users.)
	         sql.append("select organization_oid, name from bank_organization_group");
	         sql.append(" where organization_oid = ?");
	         sqlParams.add(userSession.getBogOid());
      	}
     	
      	sql.append(" order by name");
 	  	Debug.debug("Checking SQL "	+ sql.toString());
      	selectOrgDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), false, sqlParams);
      
      	if (StringFunction.isNotBlank(selectedBankOid)) {
			selectedBankOid = EncryptDecrypt.decryptStringUsingTripleDes(selectedBankOid, userSession.getSecretKey());
		}
		
		Debug.debug("(decrypted) bankoid " + selectedBankOid);
   		Debug.debug("filtertext " + filterTextFirst + filterTextLast);
   		if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 	savedSearchQueryData = (Map)searchQueryMap.get("AdminUserDataView");
		}
  	} else if (current2ndNav.equals("lockedUsers")) {

    gridId="LockedUsersDataGridId";
    helpLink = "admin/locked_out_users_listing.htm";
    viewAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.VIEW_OR_MAINTAIN_LOCKED_OUT_USERS);
    maintainAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.MAINTAIN_LOCKED_OUT_USERS);
   
    // There's no "New" button the unlocked users listview, so null these out
    newLink = null;
    buttonImageName = null;
    newButtonText = null;
    gridHtml = dgFactory.createDataGrid(gridId,"LockedUsersDataGrid", null);
    gridLayout = dgFactory.createGridLayout(gridId,"LockedUsersDataGrid");
   


    userOn = TradePortalConstants.INDICATOR_NO;
    profileOn = TradePortalConstants.INDICATOR_NO;
    lockedUsersOn = TradePortalConstants.INDICATOR_YES; 
  }
  else {

    gridId="AdminSecurityProfileDataGridId";

    //Initializing for datagrid
    gridHtml = dGridFactory.createDataGrid(gridId,"AdminSecurityProfileDataGrid", null);
    gridLayout = dGridFactory.createGridLayout(gridId,"AdminSecurityProfileDataGrid");
	  
    helpLink = "admin/admin_sec_profiles.htm";
    viewAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.VIEW_OR_MAINTAIN_ADM_SEC_PROF);
    maintainAccess = SecurityAccess.hasRights(loginRights,
                                 SecurityAccess.MAINTAIN_ADM_SEC_PROF);

    newLink = formMgr.getLinkAsUrl("selectAdminSecurityProfile",response);
    buttonImageName = "common.CreateNewAdminSecurityProfileImg";
    newButtonText = "RefDataHome.CreateNewSecurityProfile";

    
    userOn = TradePortalConstants.INDICATOR_NO;
    profileOn = TradePortalConstants.INDICATOR_YES;
    lockedUsersOn = TradePortalConstants.INDICATOR_NO; 
    if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
		 savedSearchQueryData = (Map)searchQueryMap.get("AdminSecurityProfileDataView");
	}
  }

  String searchNav = "adminRefHome."+current2ndNav;
%>


<%-- ********************* HTML for page begins here *********************  --%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" 
              value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>
<%
// Add Javascript to validate that a radio button was selected
// This is only needed on the Locked Out Users tab
if (current2ndNav.equals("lockedUsers"))
   {
%>

<script LANGUAGE="JavaScript">

  function getCheckedRadioButton()
  {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>
    numElements = document.AdminUserListView.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.AdminUserListView.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.AdminUserListView.selection.length;
        <%--
        //Handle the case where there is only one radio button
        //Handle the case where there are two or more radio buttons
        --%>
        for (i=0; i<size; i++)
        {
            if (document.AdminUserListView.selection[i].checked)
            {
                return document.AdminUserListView.selection[i].value;
            }
        }
        if (document.AdminUserListView.selection.checked)
        {
            return document.AdminUserListView.selection.value;
        }

    }
    return 0;
  }

  function confirmSelection()
  {
    if (getCheckedRadioButton()==0)
    {
        alert('<%=resMgr.getText("LockedOutUsers.SelectUserWarning",TradePortalConstants.TEXT_BUNDLE)%>');
        return false;
    }
    return true;
  }

</script>

<%
    }
%>

<div class="pageMainNoSidebar">
  <div class="pageContent">		
<%  if (current2ndNav.equals("user")){
      /* Initializing for secondary nav */
      titleKey="UsersAndSecurityMenu.AdminUsers";
      helpUrl="admin/admin_users.htm";
    }
    else if(current2ndNav.equals("lockedUsers")){
      titleKey="UsersAndSecurityMenu.LockedOutUsers";
      helpUrl="admin/locked_out_users_listing.htm";
    }
    else{
      /* Initializing for secondary nav */
      titleKey="UsersAndSecurityMenu.AdminSecurityProfiles";
      helpUrl="admin/admin_sec_profiles.htm";
    }
%>
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=titleKey %>"/>
      <jsp:param name="helpUrl"  value="<%=helpUrl %>"/>
    </jsp:include>

<form name="AdminUserListView" method="post" action="<%=formMgr.getSubmitAction(response)%>">
  <input type="hidden" name="NewSearch"  value="<%=TradePortalConstants.INDICATOR_YES%>">	
  <jsp:include page="/common/ErrorSection.jsp" />
  <div class="formContentNoSidebar">
    <div class="searchHeader">
      <div class="searchHeaderActions">

<%
	if(current2ndNav.equals("lockedUsers")){
%>
      	<jsp:include page="/common/gridShowCount.jsp">
        	<jsp:param name="gridId" value="<%=gridId%>" />
      	</jsp:include>
<%
	} else {
%>
		<jsp:include page="/common/dGridShowCount.jsp">
        	<jsp:param name="gridId" value="<%=gridId%>" />
      	</jsp:include>
<%
	}
%>
<%
    // New Button is added only if we have maintain rights and if a
    // "New" button is appropriate for the tab that we're on
    if ((newLink != null) && maintainAccess) {

        //Vasavi CR 519 03/11/2010 Begin
        if (current2ndNav.equals("user")) {
%>

        <button data-dojo-type="dijit.form.Button" type="button" id="newAdminUser" class="searchHeaderButton">
          <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">AddUserOrgSearch();return false;
          </script>
        </button>	
        <%=widgetFactory.createHoverHelp("newAdminUser","NewHoverText") %>
        

<%      } else { %>
         
        <button data-dojo-type="dijit.form.Button" type="button" id="newAdminSecProfile" class="searchHeaderButton">
          <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			var newLink = '<%=newLink%>';
        	if (isActive =='Y') {
				if (newLink != '' && newLink.indexOf("javascript:") == -1) {
		        	var cTime = (new Date()).getTime();
		        	newLink = newLink + "&cTime=" + cTime;
		        	newLink = newLink + "&prevPage=" + context;
				}
		    }
        	window.location = newLink;
			return true;
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("newAdminSecProfile","NewHoverText") %>
         
<%
        }
      } 
%>
         
      </div>
      <div style="clear:both;"></div>
    </div>
  
<%
      //Vasavi CR 519 03/11/2010 Begin
      if (current2ndNav.equals("user")) {
%>
    <div class="searchDivider"></div>

    <div class="searchDetail lastSearchDetail">
      <div class="searchCriteria">
 
<%
        dropdownOptions = ListBox.createOptionList(selectOrgDoc,
          "ORGANIZATION_OID", "NAME", selectedBankOid, userSession.getSecretKey());
%>
 
      <%= widgetFactory.createSearchSelectField("selectedBankOid", "AdminUserSearch.OwningOrg", " ", dropdownOptions.toString()) %>
      <%= widgetFactory.createSearchTextField("filterTextFirst", "AdminUserSearch.FirstName", "35","onKeydown='Javascript: filterAdminUsersOnEnter(\"filterTextFirst\");' class='char15'") %>
      <%= widgetFactory.createSearchTextField("filterTextLast", "AdminUserSearch.LastName", "35","onKeydown='Javascript: filterAdminUsersOnEnter(\"filterTextLast\");' class='char15'")%>

       </div>
       <div class="searchActions for2LineLabelCriteria"> 
        <button data-dojo-type="dijit.form.Button" type="button" id="adminUserSearch"><%=resMgr.getText("CorpCustSearch.Filter",TradePortalConstants.TEXT_BUNDLE)%> 
          <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            searchAdminUser();return false;
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("adminUserSearch","SearchHoverText") %>
        <%= widgetFactory.createHoverHelp("AdminUserSearchButton","SearchHoverText")%>
     </div>
      <div style="clear:both;"></div>
    </div>
<%
      }
      //Vasavi CR 519 03/11/2010 End

      // Specific logic for the Locked Out Users tab
      // Add the form instance hidden field
      // and also add the button to choose a user from the list
      if (maintainAccess && current2ndNav.equals("lockedUsers")) {

        out.print(formMgr.getFormInstanceAsInputField("LockedUserChooseForm", new Hashtable()));

      } 
%>

  <%= gridHtml %>
   

  </div>
</form>

<%--cquinton Portal Refresh - start--%>
<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%--cquinton Portal Refresh - end--%>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>

<script type="text/javascript">
	var initSearchParms="";
	
	function initializeDataGrid(){
		require(["t360/OnDemandGrid", "dojo/dom-construct"], function(onDemandGrid, domConstruct){
			<%--get grid layout from the data grid factory--%>
			var gridLayout = <%=gridLayout%>;
		
			<%--set the initial search parms--%>
			initSearchParms = "";
		
			var savedSort = savedSearchQueryData["sort"];  
		  	if("" == savedSort || null == savedSort || "undefined" == savedSort){
		       savedSort = '0';
		    }
		  	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AdminSecurityProfileDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
			<%--create the grid, attaching it to the div id specified created in dataGridDataGridFactory.createDataGrid.jsp()--%>
			onDemandGrid.createOnDemandGrid("AdminSecurityProfileDataGridId",viewName,gridLayout, initSearchParms,savedSort);
		});
	}

function AddUserOrgSearch(){
require(["t360/dialog"], function(dialog ) {
	dialog.open('adminUserOrgSelectionDialogId', '<%=OrganisationSelector%>',
	'AdminUserOrgSelectionDialog.jsp',
	null,null, <%-- parameters --%>
	'select', this.createAdminUserSelected);
});
}
function createAdminUserSelected(orgOid) {
    if ( document.getElementsByName('AdminUserOrgSelection').length > 0 ) {
      var theForm = document.getElementsByName('AdminUserOrgSelection')[0];
      theForm.OrgAndLevel.value=orgOid;
      theForm.submit();      					
    }
  }
</script>

 <% if (current2ndNav.equals("user")){%>
		<script type="text/javascript">
		
		function filterAdminUsersOnEnter(fieldId){
			<%-- 
		  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
		  	 --%>
		  	require(["dojo/on","dijit/registry"],function(on, registry) {
			    on(registry.byId(fieldId), "keypress", function(event) {
			        if (event && event.keyCode == 13) {
			        	dojo.stopEvent(event);
			        	searchAdminUser();
			        }
			    });
			});
		}
		
		var gridLayout = "";
		var initSearchParms = "";
 		var savedSort = savedSearchQueryData["sort"]; 
  		require(["dojo/dom","dijit/registry", "dojo/on", "t360/OnDemandGrid", "dojo/dom-construct", "dojo/ready"], 
  				function(dom,registry, on, onDemandGrid, domConstruct, ready ) {
  		 	ready(function() {
	  			var filterTextFirst = savedSearchQueryData["filterTextFirst"];   
	  			var filterTextLast = savedSearchQueryData["filterTextLast"];   
	  			var selectedBankOid = savedSearchQueryData["selectedBankOid"];   
    		 	if("" == filterTextFirst || null == filterTextFirst || "undefined" == filterTextFirst)
    				filterTextFirst=(dom.byId("filterTextFirst").value).toUpperCase();
    		 	else
    				dom.byId("filterTextFirst").value = filterTextFirst;
    	 	
    	 	 	if("" == filterTextLast || null == filterTextLast || "undefined" == filterTextLast)
    	 			filterTextLast=(dom.byId("filterTextLast").value).toUpperCase();
    		 	else
    				dom.byId("filterTextLast").value = filterTextLast;
    	 	 
    	 		if("" == selectedBankOid || null == selectedBankOid || "undefined" == selectedBankOid)
    	 			selectedBankOid=registry.byId("selectedBankOid").get('value');
    		 	else
    				registry.byId("selectedBankOid").set('value',selectedBankOid);
    	 	
    	 	 	if("" == savedSort || null == savedSort || "undefined" == savedSort){
       		  		savedSort = '0';
       	 		}
    	 	 	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("AdminUserDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    	 		initSearchParms = initSearchParms+"&filterTextFirst="+filterTextFirst+"&filterTextLast="+filterTextLast+"&selectedBankOid="+selectedBankOid;
    	 		gridLayout = <%=gridLayout%>;
    	 		onDemandGrid.createOnDemandGrid("AdminUserDataGridId",viewName,gridLayout,initSearchParms,savedSort);
  		 	});
  		 });
		   		 
		function searchAdminUser() {
		   	require(["dijit/registry", "t360/OnDemandGrid"], function(registry, onDemandGrid){
				removeErrorSection();
				var filterTextFirst=(registry.byId("filterTextFirst").get('value')).toUpperCase();
				var filterTextLast=(registry.byId("filterTextLast").get('value')).toUpperCase();
				var selectedBankOid=dijit.byId("selectedBankOid").value;
		    	var searchParams = "filterTextFirst="+filterTextFirst+"&filterTextLast="+filterTextLast+"&selectedBankOid="+selectedBankOid;
		    	console.log("SearchParmas: " + searchParams);
		    	onDemandGrid.searchDataGrid("AdminUserDataGridId", "AdminUserDataView", searchParams);
			});
	     }
</script>
		
		 
	<%} else if(current2ndNav.equals("lockedUsers")){ %>
	<script type="text/javascript">
	
			var gridLayout = <%=gridLayout%>;
			initSearchParms = "";
			var userId;
			var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("LockedUsersDataView",userSession.getSecretKey())%>"; <%--Rel9.2 IR T36000032596 --%>
		    createDataGrid("LockedUsersDataGridId", viewName,gridLayout, initSearchParms);
			
			function resetUser(){
				var formName = 'AdminUserListView';
			    var buttonName = '<%=TradePortalConstants.BUTTON_SEARCH%>';
			    

			    <%--get array of rowkeys from the grid--%>
			    var rowKeys = getSelectedGridRowKeys("LockedUsersDataGridId");
			    
			    if (rowKeys.length==0)
			    {
			        alert('<%=resMgr.getText("LockedOutUsers.SelectUserWarning",TradePortalConstants.TEXT_BUNDLE)%>');
			        return false;
			    }
			    
			    require(["t360/dialog","dijit/registry","dojo/_base/array"], function(dialog,registry,baseArray) {
			    	myGrid = registry.byId("LockedUsersDataGridId");
			    	items = myGrid.selection.getSelected();
					var selection = getSelectedGridRowKeys('LockedUsersDataGridId');
					userId=items[0].i.UserId;
					
					myGrid.on("SelectionChanged", function(){
				        <%--   this will be an array of dojo/data items --%>
				        items = myGrid.selection.getSelected();
				        baseArray.forEach(items, function(item){
				        userId = myGrid.store.getValue(item, "UserId");
				        console.log(userId);
  
				        }, myGrid);
				       
				      }, true);
					
					dialog.open('adminUserOrgSelectionDialogId',"<%=UnlockUser%>"+" - [ " + userId+" ]",
                                'UnlockUser.jsp',
                                ['selection','divId'],[rowKeys[0],'adminUserOrgSelectionDialogId'], <%-- selection is UserOId, DivId for Cancel link --%>
                                'none', 'none');
					var dialogName=dijit.byId("adminUserOrgSelectionDialogId");
					var dialogCloseButton=dialogName.closeButtonNode;
					dialogCloseButton.setAttribute("id","adminUserDialogCloseButton");
					
                  });
			    <%--submit the form with rowkeys
			        becuase rowKeys is an array, the parameter name for
			        each will be checkbox + an iteration number -
			        i.e. checkbox0, checkbox1, checkbox2, etc --%>
			    <%-- submitFormWithParms(formName, buttonName, "checkbox", rowKeys); --%>
			    dijit.byId("adminUserOrgSelectionDialogId").set('title',"<%=UnlockUser%>"+" - [ " + userId+" ]");
			}
		</script>
	
	<%}else{ %>
	    <script type="text/javascript">
			initializeDataGrid();
		</script>
	<%} %>

<%
	if(current2ndNav.equals("lockedUsers")){
%>
		<jsp:include page="/common/gridShowCountFooter.jsp">
		  <jsp:param name="gridId" value="<%=gridId%>" />
		</jsp:include>
<%
	} else {
%>		
		<jsp:include page="/common/dGridShowCountFooter.jsp">
		  <jsp:param name="gridId" value="<%=gridId%>" />
		</jsp:include>
<%
	}
%>		

<div id="adminUserOrgSelectionDialogId" ></div>
<%=widgetFactory.createHoverHelp("LockedOutUsers_ResetSelectedUser", "AdminRefdataUnlockUser.ResetListButton") %>


</body>

</html>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

  session.setAttribute("adminRefData2ndNav", current2ndNav);
  
%>
<script type="text/javascript">
<%-- 
 * Remove Error or Info Section on Search button click
  --%>
function removeErrorSection(){
	if(document.getElementById('infoSection')){
		 document.getElementById('infoSection').style.display = 'none'; 
	}
}
</script>
