<%--
***************************************************************************************************
Generic Message Categories

Description:
	Displays Generic Message Categories reference data.
	This page may be retrieved from several actions:
			Creating New Message Category from main Reference Data page
			Selecting a Generic Message Category in the Generic Message Categoriesp listview
			Errors resulting from Saving and Deleting the Generic Message Categories
		
Request Parameters:
	Oid - Oid of Message Category selected from listview	

***************************************************************************************************
--%>

<%-- ****** Begin Page imports, bean imports declaration ******* --%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ****** End Page imports, bean imports declaration  ******* --%>


<%--  ****** Begin data retrieval/display logic ****** --%>
<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	
  DocumentHandler doc;
  
  String buttonPressed ="";
  Vector error = null;
	
  DocumentHandler errorDoc = new DocumentHandler();
  GenericMessageCategoryWebBean genericMsgCategory;
  String genMsgCategoryOid = null;
  
  boolean insertMode=false;
  boolean getFromDB=false;
  boolean getFromDoc=false;
	
  boolean isReadOnly=false;
  boolean displayErrors=false;
  
  boolean showCloseButton=true;
  boolean showDeleteButton=false;
  boolean showSaveButton=false;
  boolean showSaveClose=false;
  
  boolean nullValue=true;
  boolean debugMode=true;
  
  String loginLocale;
  String loginRights;

  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

  Debug.debug("Login locale is " + loginLocale);
  isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_GM_CAT_GRP);
  
  // if user does not have access to view this page, return back to Reference
  // Home page
  if (!SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_OR_MAINTAIN_GM_CAT_GRP))
  {
	formMgr.setCurrPage("RefDataHome");
      String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request); 
  %>
  	<jsp:forward page='<%= physicalPage %>' />
  <%
  }

  // register Generic Msg Categories bean

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.GenericMessageCategoryWebBean", "GenericMessageCategory");
  genericMsgCategory = (GenericMessageCategoryWebBean) beanMgr.getBean("GenericMessageCategory");
	
  doc = formMgr.getFromDocCache();
  
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
	error = doc.getFragments("/Error/errorlist/error");
  
  if (doc.getDocumentNode("/In/GenericMessageCategory") == null)
  {
	// This must come from the Reference Data page 
      if(request.getParameter("oid") != null)
       {
		genMsgCategoryOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());	
       }
      else
       {
		genMsgCategoryOid = null;
       }  

	if (genMsgCategoryOid == null )
	{
		insertMode=true;
	}
	else if (genMsgCategoryOid.equals("0"))
	{
		insertMode=true;
	}
	else
	{
		getFromDB=true;	
	}
  }// Logic is re-written for TP redfresh - 12-13-2012
  else
  {
	// This must be returned from Generic Message Category with errors
    // Check that maxerrorseverity is valid, redirect otherwise
	
	errorDoc.addComponent("/Error",doc.getComponent("/Error"));
	String maxError = errorDoc.getAttribute("/Error/maxerrorseverity");
	Debug.debug("Error Fragment is: " + errorDoc.toString());
	genMsgCategoryOid = doc.getAttribute("/Out/GenericMessageCategory/gen_message_category_oid");
    if (genMsgCategoryOid == null ) {
    	genMsgCategoryOid = doc.getAttribute("/In/GenericMessageCategory/gen_message_category_oid");
    }
    
    getFromDoc = true;
    getFromDB = false;
	// check for message severity and decide whether the tranaction is sucessfull ot not.
	if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0)
	{
			// Save is sucessfull
	}else{
		// We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.
	    displayErrors = true;
	    genMsgCategoryOid = doc.getAttribute("/In/GenericMessageCategory/gen_message_category_oid");
	    if (genMsgCategoryOid == null)
	    {
		  insertMode = true;
	    }
	    else if (genMsgCategoryOid.equals("0"))
	    {
		  insertMode = true;
	    }else{
	    	insertMode = false;
	    }
	}
  }

  if (getFromDB)
  {
  	genericMsgCategory.getById(genMsgCategoryOid);	
  }

  if (getFromDoc)
  {
	genericMsgCategory.populateFromXmlDoc(doc.getComponent("/In"));
  }

  if (insertMode)
  {
	genMsgCategoryOid="0";
  }
  
  // determine which buttons to show
  if (!isReadOnly)
  {
	showSaveButton=true;
	showSaveClose=true;
	showDeleteButton=false;
  }
  if (!insertMode && !isReadOnly)
		showDeleteButton=true;
  
Debug.debug(" getFromDoc: " + getFromDoc + " getFromDB: " + getFromDB +
					" insertMode " + insertMode + 
					"genMsgCategoryOid: " + genMsgCategoryOid);
	
    
%>
<%--  ****** End data retrieval/display logic  ****** --%>

<%--  ****** Begin HTML  ****** --%>

<%
	String onLoad="";
	// where to focus on
    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).

	if (!isReadOnly) {
		onLoad = "document.GenericMessageCategoryDetail.MsgCatName.focus();";
    }

    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

%>

<%-- Begin HTML header declarations (this includes the body tag) --%>

<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
  <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<%-- End HTML header declarations --%>



<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="GenericMessageCategoryDetail.GenMsgCatLabel" />
      <jsp:param name="helpUrl"  value="admin/Generic_Message_Category_Form.htm" />
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if ( insertMode) {
    subHeaderTitle = resMgr.getText("GenericMessageCategoryDetail.NewGenMsgCat", TradePortalConstants.TEXT_BUNDLE);
  } 
  else {
    subHeaderTitle = genericMsgCategory.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

				<form id="GenericMessageCategoryDetail" name="GenericMessageCategoryDetail" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
  					<input type=hidden value="" name=buttonName>

<%

// build parameters for inclusion to the GenericMessageCategoryDetailForm
// which is used by the mediator, eg. security rights and locks

Hashtable addlParms = new Hashtable();
if (!insertMode)
	addlParms.put("gen_message_category_oid", genMsgCategoryOid);
else
	addlParms.put("gen_message_category_oid", "0");


addlParms.put("p_owner_org_oid", userSession.getOwnerOrgOid());
addlParms.put("opt_lock", genericMsgCategory.getAttribute("opt_lock"));

//Vshah - IR#LUUL120240799 - Rel7.1 - 12/14/2011 - <BEGIN>
//addlParms.put("ownership_level", genericMsgCategory.getAttribute("ownership_level"));
//addlParms.put("ownership_type", genericMsgCategory.getAttribute("ownership_type"));
addlParms.put("ownership_level", userSession.getOwnershipLevel());
if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
  addlParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
else
  addlParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
//Vshah - IR#LUUL120240799 - Rel7.1 - 12/14/2011 - <END>


//addlParms.put("login_oid", userSession.getUserOid());
//addlParms.put("login_rights", userSession.getSecurityRights());
%>

<%= formMgr.getFormInstanceAsInputField("GenericMessageCategoryDetailForm", addlParms) %>

 <div class="formArea">
 <jsp:include page="/common/ErrorSection.jsp" />

 <div class="formContent">
 	<div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
	<div class="dijitTitlePaneContentInner">		     
<br>
	<%= widgetFactory.createTextField("MsgCatName", "GenericMessageCategoryDetail.MsgCatName", genericMsgCategory.getAttribute("name"), 
			"20",isReadOnly, true, false, "", "","") %>
			
	<%= widgetFactory.createTextField("MsgCatDescription", "GenericMessageCategoryDetail.MsgCatDescription", genericMsgCategory.getAttribute("description"), 
			"35",isReadOnly, true, false, "char35", "","") %>
			
	<%= widgetFactory.createTextField("MsgCatShortDescription", "GenericMessageCategoryDetail.MsgCatShortDescription", genericMsgCategory.getAttribute("short_description"), 
			"25",isReadOnly, true, false, "class='char35'", "","") %>
<br>
</div>
</div>
  </div><%--formContent--%>
  </div><%--formArea--%>

	<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'GenericMessageCategoryDetail'">
		<jsp:include page="/common/RefDataSidebar.jsp">
	        <jsp:param name="showSaveButton" value='<%= showSaveButton %>' />
	        <jsp:param name="saveOnClick" value="none" />
	        <jsp:param name="showDeleteButton" value='<%= showDeleteButton %>' />
	        <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
	        <jsp:param name="saveCloseOnClick" value="none" />
			<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
	        <jsp:param name="error" value="<%= error%>" />
	        <jsp:param name="showLinks" value="false" />  
			<jsp:param name="showTop" value="false" />
           	<jsp:param name="showTips" value="true" />
		</jsp:include>	
	</div> <%--closes sidebar area--%>

</form>

</div> <%--closes pageContent area--%>
</div> <%--closes pageMain area--%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>

<%
  //save behavior defaults to without hsm
  
   //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";

 
%>
  

<jsp:include page="/common/RefDataSidebarFooter.jsp"/>
   
</body>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("GenericMessageCategory");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</html>
<%--  ****** End HTML  ****** --%>
