<%--
***************************************************************************************************
Cross Rate Calculation Rules

Description:
	Displays Cross Rate Calculation Rule reference data.
	This page may be retrieved from several actions:
			Creating New Cross Rate Calculation Rule from main Reference Data page
			Selecting a Cross Rate Calculation Rule in the Generic Message Categoriesp listview
			Errors resulting from Saving and Deleting the Cross Rate Calculation Rules
		
Request Parameters:
	Oid - Oid of Cross Rate Calculation Rule selected from listview	

***************************************************************************************************
--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>


<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>


<%-- ****** End Page imports, bean imports declaration  ******* --%>


<%--  ****** Begin data retrieval/display logic ****** --%>
<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String links="";
  String certAuthURL = "";
  String templateFlag = "";
  DocumentHandler doc;
  DocumentHandler errorDoc = new DocumentHandler();
  CrossRateCalcRuleWebBean crossRateCalcRule;
  String crossRateCalcRuleOid = null;
  String buttonPressed = "";
  Vector error = null;
  boolean insertMode=false;
  boolean getFromDB=false;
  boolean getFromDoc=false;
	
  boolean isReadOnly=false;
  boolean displayErrors=false;
  
  boolean showSaveClose=false;
  boolean showDelete=false;
  boolean showSave=false;
   
  boolean nullValue=true;
  boolean debugMode=true;
  boolean isErrorFlag=false;
 
  String loginLocale;
  String loginRights;
  String frmCcyCode  = null;
  String toCcyCode  = null;
  String frmCcyOptions = null;
  String toCcyOptions = null;
  String frmCcyDefaultText = null;
  String toCcyDefaultText = null;
  String multiplyIndicator = null;
  String dropdownOptions = null;
  String multiDivValue= null;
  String maxError=null;
  int DEFAULT_ROW_COUNT = 4;
  List ruleList = new ArrayList(DEFAULT_ROW_COUNT);
  int numRow;
  int rowCount = 0;
  int iLoop;
  //cquinton 1/5/2012 Rel 7.1 ir#daul122861639 start
  //parameter name for number of rows must be "numberOfMultipleObjects"
  if(request.getParameter("numberOfMultipleObjects")!=null) {
	rowCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects"));
  }
  else {
	rowCount = DEFAULT_ROW_COUNT;
  }
  //cquinton 1/5/2012 Rel 7.1 ir#daul122861639 end
  QueryListView rowQueryListView = null;

  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

  Debug.debug("Login locale is " + loginLocale);
  isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_CROSS_RATE_RULE);
  
  // if user does not have access to view this page, return back to Reference
  // Home page
  if (!SecurityAccess.hasRights(loginRights, SecurityAccess.VIEW_OR_MAINTAIN_CROSS_RATE_RULE))
  {
	  formMgr.setCurrPage("RefDataHome");
      String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request); 
  %>
  	<jsp:forward page='<%= physicalPage %>' />
  <%
  }

  // register Cross Rate Calc Rule bean

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.CrossRateCalcRuleWebBean", "CrossRateCalcRule");
  crossRateCalcRule = (CrossRateCalcRuleWebBean) beanMgr.getBean("CrossRateCalcRule");
	
  doc = formMgr.getFromDocCache();
  if (doc.getDocumentNode("/In/Update/ButtonPressed") != null) 
  {
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");  
  error = doc.getFragments("/Error/errorlist/error");
  }
  
  if (doc.getDocumentNode("/In/CrossRateCalcRule") == null)
  {
	// This must come from the Reference Data page 
      if(request.getParameter("oid") != null)
       {
		crossRateCalcRuleOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());	
       }
      else
       {
		crossRateCalcRuleOid = null;
       }  

	if (crossRateCalcRuleOid == null )
	{
		insertMode=true;
		getFromDB=false;
	}
	else if (crossRateCalcRuleOid.equals("0"))
	{
		insertMode=true;
		getFromDB=false;
	}
	else
	{
		getFromDB=true;	
	}
  }
  else
  {
	// This must be returned from Cross Rate Calc Rule with errors
    // Check that maxerrorseverity is valid, redirect otherwise
	
	errorDoc.addComponent("/Error",doc.getComponent("/Error"));
	 maxError = errorDoc.getAttribute("/Error/maxerrorseverity");
	Debug.debug("Error Fragment is: " + errorDoc.toString());
	
	if(Integer.parseInt(maxError)==5){
		isErrorFlag=true;
		String oid = doc.getAttribute("/In/CrossRateCalcRule/cross_rate_calc_rule_oid");
        if (oid==null || "0".equals(oid))
        {
      	    insertMode = true;
      	    
        }
        else
        {
         // Not in insert mode, use oid from output doc.
        	insertMode = false;
        }
	}
	// maxerrorseverity should be more then 3, otherwise redirect back to RefDataHome
	//out.println("MaxxErrro..."+maxError+"Comparee too..."+maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)));
	if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) > 0)
	{
		    
		// We've returned from a errors
        
		    formMgr.setCurrPage("RefDataHome");
            String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
            
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
		return;
	}
	
    displayErrors = true;
    getFromDoc = true;
    crossRateCalcRuleOid = doc.getAttribute("/In/CrossRateCalcRule/cross_rate_calc_rule_oid");  
    if(!insertMode){
    if(crossRateCalcRuleOid==null || "0".equals(crossRateCalcRuleOid)){
    	crossRateCalcRuleOid = doc.getAttribute("/Out/CrossRateCalcRule/cross_rate_calc_rule_oid");
    }

    if (crossRateCalcRuleOid == null)
    {
	  insertMode=true;
    }
    else if (crossRateCalcRuleOid.equals("0"))
    {
	  insertMode=true;
    }
    }//end if !insertMode
  }

  if(!getFromDB && !getFromDoc) {
	// Creating New Corporate Customer
	for (int i = 0; i < DEFAULT_ROW_COUNT; i++) {
		ruleList.add(beanMgr.createBean(CrossRateRuleCriterionWebBean.class, "CrossRateRuleCriterion"));
	}
  }
  
  if (getFromDoc && !isErrorFlag){
	  
	  getFromDB =true;
	  getFromDoc=false;
	  errorDoc.setAttribute("/Error/maxerrorseverity","0");
	  maxError="0";
	  isErrorFlag=false;
	  errorDoc.removeComponent("/Error");
  }
  
   
  	Vector ruleList1= new Vector();
  	List sqlParams = new ArrayList();
  	
    crossRateCalcRule.getById(crossRateCalcRuleOid);
  	rowQueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");

	StringBuffer ruleSql= new StringBuffer("SELECT criterion_oid, FROM_CCY,  TO_CCY,  CALC_METHOD,  LOWER_VARIANCE,  UPPER_VARIANCE,  P_CROSS_RATE_CALC_RULE_OID");
  	ruleSql.append(" from CROSS_RATE_RULE_CRITERION");
  	ruleSql.append(" where P_CROSS_RATE_CALC_RULE_OID = ?");
  	ruleSql.append(" order by ");
  	ruleSql.append(resMgr.localizeOrderBy("CRITERION_OID"));
	Debug.debug(ruleSql.toString());
	sqlParams.add(crossRateCalcRuleOid);
	rowQueryListView.setSQL(ruleSql.toString(), sqlParams);
	rowQueryListView.getRecords();
	HashMap ruleMap= new HashMap();
	DocumentHandler result = rowQueryListView.getXmlResultSet();
	Debug.debug(result.toString());

	Vector vVector = result.getFragments("/ResultSetRecord");

        //cquinton 1/5/2012 Rel 7.1 ir#daul122861639 start
        //set rowCount to number retrieved from db
        if ( vVector.size() > DEFAULT_ROW_COUNT ) {
            rowCount = vVector.size();
        }
        //cquinton 1/5/2012 Rel 7.1 ir#daul122861639 end
    //out.println("rowCount..."+rowCount);
	for (iLoop = 0; iLoop < vVector.size(); iLoop++) {
		 CrossRateRuleCriterionWebBean rule= beanMgr.createBean(CrossRateRuleCriterionWebBean.class, "CrossRateRuleCriterion");
	   
		 DocumentHandler ruleDoc = (DocumentHandler) vVector.elementAt(iLoop);
		 rule.setAttribute("criterion_oid",ruleDoc.getAttribute("/CRITERION_OID"));
		 rule.setAttribute("from_ccy",ruleDoc.getAttribute("/FROM_CCY"));
		 rule.setAttribute("to_ccy",ruleDoc.getAttribute("/TO_CCY"));
		 rule.setAttribute("calc_method",ruleDoc.getAttribute("/CALC_METHOD"));
		 rule.setAttribute("lower_variance",ruleDoc.getAttribute("/LOWER_VARIANCE"));
		 rule.setAttribute("upper_variance",ruleDoc.getAttribute("/UPPER_VARIANCE"));
		 rule.setAttribute("cross_rate_calc_rule_oid",ruleDoc.getAttribute("/CROSS_RATE_CALC_RULE_OID"));
		 ruleMap.put(""+iLoop,rule);
		 ruleList1.add(rule);
	} 
	 if(vVector.size() < DEFAULT_ROW_COUNT) {
		  for(iLoop = vVector.size(); iLoop < DEFAULT_ROW_COUNT; iLoop++) {
			  ruleList1.add(beanMgr.createBean(CrossRateRuleCriterionWebBean.class,	"CrossRateRuleCriterion"));
		  }
	  }
	
  	if (rowQueryListView != null) {
  		rowQueryListView.remove();
	  }
  	
    if(getFromDB){
    	//out.println("RuleList.."+ruleList1.size());
    	ruleList= new Vector();
    	for(int i=0;i<ruleList1.size();i++){
    		 ruleList.add(ruleList1.get(i));
    	}
    	//ruleList ruleList1;
  }//get data from DB
  
  if (getFromDoc)
  {
	crossRateCalcRule.populateFromXmlDoc(doc.getComponent("/In"));
	   Debug.debug(doc.toString());
	   ruleList= new Vector();
	   vVector = doc.getFragments("/In/CrossRateCalcRule/CrossRateRuleCriterionList");
	   //out.println("vvector"+vVector.size());
		 for (iLoop=0; iLoop<vVector.size(); iLoop++)
		   {
			 CrossRateRuleCriterionWebBean rule= beanMgr.createBean(CrossRateRuleCriterionWebBean.class, "CrossRateRuleCriterion");
		   
			 DocumentHandler ruleDoc = (DocumentHandler) vVector.elementAt(iLoop);
			 rule.populateFromXmlDoc(ruleDoc,"/");
			 CrossRateRuleCriterionWebBean ruleDoc1 =  (CrossRateRuleCriterionWebBean)ruleMap.get(""+iLoop);
			 if(ruleDoc1!=null)
			 //out.println("Criterion poid"+ruleDoc1.getAttribute("criterion_oid"));
			 if(ruleDoc1!=null && ruleDoc1.getAttribute("criterion_oid")!=null){
				// out.println("in not null");
			 if(ruleDoc1.getAttribute("criterion_oid").length()>0){
				 rule.setAttribute("criterion_oid",ruleDoc1.getAttribute("criterion_oid"));
			 }}
			 ruleList.add(rule);
		   } 		 
		 if(vVector.size() < DEFAULT_ROW_COUNT) {
			  for(iLoop = vVector.size(); iLoop < DEFAULT_ROW_COUNT; iLoop++) {
				  ruleList.add(beanMgr.createBean(CrossRateRuleCriterionWebBean.class, "CrossRateRuleCriterion"));
			  }
		  }
	
  }

  if (insertMode)
  {
	crossRateCalcRuleOid="0";
  }
  
  // determine which buttons to show
  if (!isReadOnly)
  {
	showSave=true;
	showSaveClose=true;
	if (!insertMode)
		showDelete=true;
  }
  
Debug.debug(" getFromDoc: " + getFromDoc + " getFromDB: " + getFromDB +
					" insertMode " + insertMode + 
					"crossRateCalcRuleOid: " + crossRateCalcRuleOid);
					
  frmCcyDefaultText = resMgr.getText("FXRatesDetail.SelectCurrency", TradePortalConstants.TEXT_BUNDLE);
  toCcyDefaultText = resMgr.getText("FXRatesDetail.SelectCurrency", TradePortalConstants.TEXT_BUNDLE);

  
%>
<%--  ****** End data retrieval/display logic  ****** --%>

<%--  ****** Begin HTML  ****** --%>

<%
		//The navigation bar is only shown when editing templates.  For transactions
		// it is not shown ti minimize the chance of leaving the page without properly
		// unlocking the transaction.
		String showNavBar = TradePortalConstants.INDICATOR_YES;
	
    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

%>

<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag" value="<%=showNavBar%>" />
	<jsp:param name="includeErrorSectionFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />	
	<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
	<jsp:param name="templateFlag" value="<%=templateFlag%>" />
</jsp:include>


<div class="pageMain">
  <div class="pageContent">
<%
 	String pageTitleKey = resMgr.getText("CrossRateCalculationRuleDetail.CrossRateCalculationRuleLabel",TradePortalConstants.TEXT_BUNDLE);
	String helpUrl = "customer/Creating_Cross_Rate_Calculation_Rules.htm";
%>

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="item1Key" value="" />
      <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
    </jsp:include>
<%--------------------- Sub header start ---------------------%>
<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if ( insertMode) {
    subHeaderTitle = resMgr.getText("CrossRateCalculationRuleDetail.NewCrossRateCalculationRule",TradePortalConstants.TEXT_BUNDLE);
  } else {
    subHeaderTitle = crossRateCalcRule.getAttribute("name");
  }

  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes("goToRefDataHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToRefDataHome", parms.toString(), response);
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>
<%--------------------- Sub header end ---------------------%>

<form id="CrossRateCalculationRuleDetailForm" name="CrossRateCalculationRuleDetailForm"
	method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
	<input type=hidden value="" name=buttonName>
	 <div class="formMainHeader">        
 
  	</div>
	<div class="formArea">
		<jsp:include page="/common/ErrorSection.jsp" />
  		<div class="formContent"> <%-- Form Content Area starts here --%>
			<div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
	        <div class="dijitTitlePaneContentInner">		     
 
<%

// build parameters for inclusion to the CrossRateCalculationRuleDetailForm
// which is used by the mediator, eg. security rights and locks

Hashtable addlParms = new Hashtable();
if (!insertMode)
	addlParms.put("cross_rate_calc_rule_oid", crossRateCalcRuleOid);
else
	addlParms.put("cross_rate_calc_rule_oid", "0");


addlParms.put("owner_org_oid", userSession.getOwnerOrgOid());
addlParms.put("opt_lock", crossRateCalcRule.getAttribute("opt_lock"));
addlParms.put("ownership_level", userSession.getOwnershipLevel());
addlParms.put("login_oid", userSession.getUserOid());
addlParms.put("login_rights", userSession.getSecurityRights());

// Used for the "Added By" column
if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
  addlParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
else
  addlParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
rowCount  =ruleList.size();
%>

 
  <%=widgetFactory.createTextField("name",
					"CrossRateCalculationRuleDetail.CrossRateCalcuRuleName", crossRateCalcRule.getAttribute("name"), "35", isReadOnly, true, false,
					"", "", "")%>
  <input type=hidden value="<%=rowCount%>" name="numberOfMultipleObjects"
         id="noOfRows"> 
  <%--  //cquinton 1/5/2012 Rel 7.1 ir#daul122861639 end --%>
  <table id="rulesTable" class="formDocumentsTable" style="width:97%">
		<thead align="left">
				<tr>
					<th>&nbsp;</th>
					<th align="center"><span class="asterisk">* </span><%=widgetFactory.createLabel("","CrossRateCalculationRuleDetail.FromCcy",isReadOnly, true, false, "none")%></th>
					<th align="center"><span class="asterisk">* </span><%=widgetFactory.createLabel("","CrossRateCalculationRuleDetail.ToCcy",isReadOnly, true, false, "none")%></th>
					<th align="center"><span class="asterisk">* </span><%=widgetFactory.createLabel("","CrossRateCalculationRuleDetail.CalcMethod",isReadOnly, true, false, "none")%></th>
					<th align="center"><span class="asterisk">* </span><%=widgetFactory.createLabel("","CrossRateCalculationRuleDetail.LowerVar",isReadOnly, true, false, "none")%></th>
					<th align="center"><span class="asterisk">* </span><%=widgetFactory.createLabel("","CrossRateCalculationRuleDetail.UpperVar",isReadOnly, true, false, "none")%></th>
					<th align="center"><%=widgetFactory.createLabel("","PanelAuthorizationGroupDetail.ClearRow",isReadOnly, false, false, "none")%></th>				
		
			</tr>
		</thead>
		
		<tbody>
		<%
			CrossRateRuleCriterionWebBean crossRateRule = null;
			//System.out.println("Cross Rate Cal Rule List.size() = "+ruleList.size());
			for (int crossRateCalcIndex = 0; crossRateCalcIndex < ruleList.size(); crossRateCalcIndex++) {
				crossRateRule = (CrossRateRuleCriterionWebBean) ruleList.get(crossRateCalcIndex); 
				
	     %>
	     	<input type=hidden value='<%=crossRateRule%>' name="crossRateRule" id="crossRateRule">
	     	<%@ include file="fragments/CrossRateCalculationRuleRow.frag"%>	   
	     <%}%>
		</tbody>
  </table>
  <%-- End Table with Cross Rate Calculation Rule Name --%>
  <div>
  <% if (!(isReadOnly)) 
		{     %>	 
				<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreRow">Add 4 More
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					add4MoreRow();
				</script>
			</button>	
			<%=widgetFactory.createHoverHelp("add4MoreRow", "RefData.add4More") %>	
	<% 
		}else{  %>
			&nbsp;
		<% 
		} %>  		
  	</div>
	</div>
	</div>
  </div><%--formContent--%>
</div><%--formArea--%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'CrossRateCalculationRuleDetailForm'">
                   <jsp:include page="/common/RefDataSidebar.jsp">
	                        <jsp:param name="showSaveButton" value="<%=showSave%>" />
							<jsp:param name="showSaveCloseButton" value="<%=showSaveClose%>" />
					        <jsp:param name="showDeleteButton" value="<%=showDelete%>" />
                             <jsp:param name="cancelAction" value="goToRefDataHome" />
                             <jsp:param name="showHelpButton" value="false" />
                             <jsp:param name="helpLink" value="customer/work_group_detail.htm" />    
			            	<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
			            	<jsp:param name="error" value="<%=error%>" />
                   </jsp:include>        
          </div> <%--closes sidebar area--%>
          
<%= formMgr.getFormInstanceAsInputField("CrossRateCalculationRuleDetailForm", addlParms) %>
</form>
</div><%--formContent--%>
</div><%--formArea--%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<%-- ********************* JavaScript for page begins here *********************  --%>

<script language="JavaScript">
	function add4MoreRow(){
	    var tbl = document.getElementById("rulesTable");
	    <%-- get index# for field naming --%>
	    var insertRowIdx = tbl.rows.length;
	    var cnt = 0;
	    for(cnt = insertRowIdx; cnt<(insertRowIdx+4); cnt++){
	    <%-- add the table row --%>
	    var newRow = tbl.insertRow(cnt);
	    newRow.id = 'crossRateCalcIndex'+cnt;
	    document.getElementById('noOfRows' ).value = eval(cnt);
      	
    	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
      	var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
      	var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
      	var cell3 = newRow.insertCell(3);<%-- fourth  cell in the row --%>
      	var cell4 = newRow.insertCell(4);<%-- fifth  cell in the row --%>
		var cell5 = newRow.insertCell(5);<%-- sixth  cell in the row --%>
		var cell6 = newRow.insertCell(6);<%-- seventh  cell in the row --%>
		
			
        j=cnt-1;
		
		var optFromCCYInnerHTML=  '<select data-dojo-type=\"t360.widget.FilteringSelect\" name=\"from_ccy'+j+'\"" id=\"from_ccy'+j+'\" style=\"width: 75px;\" ></select>';
		var optToCCYInnerHTML='<select data-dojo-type=\"t360.widget.FilteringSelect\" name=\"to_ccy'+j+'\"" id=\"to_ccy'+j+'\" style=\"width: 75px;\" ></select>';
		var multiDivInnerHTML = '<input data-dojo-type=\"dijit.form.RadioButton\" name=\"calc_method'+j+'\" id=\"TradePortalConstants.DIVIDE'+j+'\" value=\"DIVIDE\"   />';
		multiDivInnerHTML=multiDivInnerHTML+'<label for=\"TradePortalConstants.DIVIDE'+j+'\" class=\"radioButtonLabel\">Divide</label>'
		multiDivInnerHTML=multiDivInnerHTML+'<input data-dojo-type=\"dijit.form.RadioButton\" name=\"calc_method'+j+'\" id=\"TradePortalConstants.MULTIPLY'+j+'\" value=\"MULTIPLY\" /> <label for=\"TradePortalConstants.MULTIPLY'+j+'\" class=\"radioButtonLabe\">Multiply</label>';
		var lowerVarInnerHTML= '<input data-dojo-type=\"dijit.form.TextBox\" name=\"lower_variance'+j+'\" id=\"lower_variance'+j+'\" style=\"width: 100px;\" maxlength=\"14\" class=\"char14\" value=\"\" />'
		var upperVarInnerHTML= '<input data-dojo-type=\"dijit.form.TextBox\" name=\"upper_variance'+j+'\" id=\"upper_variance'+j+'\" style=\"width: 100px;\" maxlength=\"14\" class=\"char14\" value=\"\" />'
		var clearTextHTML ='<div id=\"clear\"><a href="javascript:clearText('+j+', \'&lt;Select A Currency&gt;\', \'&lt;Select A Currency&gt;\');\" >Clear</a></div> <span data-dojo-type=\"t360.widget.Tooltip\" data-dojo-props=\"connectId:\'clear\'\">Select to clear the data in this row.</span>';	
		var creationOidHTML ='<INPUT TYPE=HIDDEN NAME=\'criterion_oid'+j+'\' VALUE=\'\'>	<input type=hidden value=\'\' name=\"crossRateRule\" id=\"crossRateRule\">';
		  
		cell0.innerHTML =(j+1)+".";
		cell1.innerHTML =optFromCCYInnerHTML;
		cell2.innerHTML =optToCCYInnerHTML;
		cell3.innerHTML =multiDivInnerHTML;
		cell4.innerHTML =lowerVarInnerHTML;
		cell5.innerHTML =upperVarInnerHTML;
		cell6.innerHTML =clearTextHTML+creationOidHTML;
		
			
		require(["dojo/parser", "dijit/registry"], function(parser, registry) {
	         parser.parse(newRow.id);
	       <%-- from CCY 	  --%>
	         var selFromCCY = registry.byId("from_ccy0");
	             if(selFromCCY!=null){
	               var fromCCY = selFromCCY.store.data;
	              for(var i=0; i<fromCCY.length;i++){
	                      registry.byId('from_ccy'+(j)).store.data.push({name:fromCCY[i].name,value:fromCCY[i].value, id:fromCCY[i].value});
	              }
	              registry.byId('from_ccy'+(j)).attr('displayedValue', "<Select A Currency>");
	            }
	     <%-- TOCCY --%>
	        var selToCCY = registry.byId("to_ccy0");
		    if(selToCCY!=null){
		    	var toCCY = selToCCY.store.data;
		        for(var k=0; k<toCCY.length;k++){
		             registry.byId('to_ccy'+(j)).store.data.push({name:toCCY[k].name,value:toCCY[k].value, id:toCCY[k].value});
              	}
              	registry.byId('to_ccy'+(j)).attr('displayedValue', "<Select A Currency>");
			}
	    });
			
    }
}<%-- end of add4MoreRows function --%>

	
</script>
<script type = 'text/javascript' src='/portal/js/common.js'></script>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("CrossRateCalcRule");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
<%--  ****** End HTML  ****** --%>
