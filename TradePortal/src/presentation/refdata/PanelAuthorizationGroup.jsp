<%--
**********************************************************************************
                               Panel Authrorization Group

  Description:
     This page is used to maintain Panel Authrorization Group.  It supports
     insert, update, and delete.  Errors found during a save are redisplayed on
     the same page. Successful updates return the user to the ref data home page.

**********************************************************************************
--%>

<%--
*
*     Copyright   2001
*     American Management Systems, Incorporated
*     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*, java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
DocumentHandler                xmlDoc                          = null;
DocumentHandler errorDoc = new DocumentHandler();
//PanelAuthorizationRangeWebBean   panelAuthRange                    = null;
CorporateOrganizationWebBean corporateOrg                                  = null;
Hashtable                      secureParms                     = new Hashtable();
boolean                        getDataFromXmlDocFlag           = false;
boolean                        retrieveFromDBFlag              = false;
boolean                        insertModeFlag                  = true;
boolean                        isReadOnly                      = false;
boolean showSaveClose = false;
boolean showDelete = false;
boolean showSave = false;
//jgadela rel8.3 CR501 [START]
boolean showApprove = false;
boolean showReject = false;
//jgadela rel8.3 CR501 [END]
boolean                        showDeleteButton                = false;
boolean                        showSaveAs                = false;

boolean                        showSaveButton                  = false;
boolean                        defaultCheckBoxValue            = false;
boolean                        errorFlag                       = false;
String                         corporateOrgOid                = null;
String                         userSecurityRights              = null;
String                         defaultText                     = null;
String                         dropdownOptions                 = null;
String                         userOrgOid                      = null;
String                         userLocale                      = null;
String                         userType                        = null;
String                         onLoad                          = null;
String                         sValue                          = null;
String                         clientBankOid                   = null;
String                         PAGroupOid                      = null;
QueryListView                  queryListView                   = null;

Vector                         vVector=null;
boolean                              transferBtwAcctIndicator = false;
boolean                            domesticPaymtIndicator = false;
boolean                              intlPaymentIndicator = false;
String                                    allowTBAIndicator = null;
String                                    allowDomesticPmtIndicator     = null;
String                                    allowInternationalPmtInd      = null;
String buttonPressed = "";
Vector error = null;
String panelRangeMinAmount = null;
String panelRangeMaxAmount = null;
String baseCurrency = "";
StringBuffer panelrange0 = new StringBuffer();
StringBuffer panelrange1 = new StringBuffer();
StringBuffer panelrange2 = new StringBuffer();
StringBuffer panelrange3 = new StringBuffer();
StringBuffer panelrange4 = new StringBuffer();
StringBuffer panelrange5 = new StringBuffer();
String brandingDirectory = userSession.getBrandingDirectory();
String corpOrgOid = null;
boolean ignoreDefaultPanelValue = false;
String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

if (brandingDirectory == null || brandingDirectory.equals("")) {
    brandingDirectory = TradePortalConstants.DEFAULT_BRANDING;
}else if( "blueyellow".equals(brandingDirectory) ) {
    brandingDirectory = TradePortalConstants.DEFAULT_BRANDING;
}else if ( "anztransac".equals(brandingDirectory) ) {
  brandingDirectory = "anz";
}

String         userSecurityType        = null;

// Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
if (userSession.getSavedUserSession() == null) {
		userSecurityType = userSession.getSecurityType();
 }else {
	   userSecurityType = userSession.getSavedUserSessionSecurityType();
 }

String version = resMgr.getText("version", "TradePortalVersion");
if (version.equals("version")) {
  version = resMgr.getText("Footer.UnknownVersion",
                           TradePortalConstants.TEXT_BUNDLE);
}
version = EncryptDecrypt.encryptStringUsingTripleDes(version, userSession.getSecretKey());
version = version.substring(0, 4);

WidgetFactory widgetFactory = new WidgetFactory(resMgr);
int countLoop;


   beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.PanelAuthorizationRangeWebBean", "PanelAuthRange");
   beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.PanelAuthorizationRuleWebBean", "PanelAuthRule");
   beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
   corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
   corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
   corporateOrg.getDataFromAppServer();
   //MEer Rel 8.3 IR-T36000021992
   corpOrgOid = userSession.getOwnerOrgOid();
   //jgadela rel8.3 CR501 [START]
   String pendingReferenceDataOid = "";
   String pendingChangedObjectOid = "";
   String panelAuthDualCtrlReqdInd = corporateOrg.getAttribute("panel_auth_dual_ctrl_reqd_ind");
   //jgadela rel8.3 CR501 [END]

   PanelAuthorizationGroupWebBean_Base paGroup = beanMgr.createBean(PanelAuthorizationGroupWebBean_Base.class, "PanelAuthorizationGroup");

   xmlDoc = formMgr.getFromDocCache();
   /* out.println(xmlDoc); */

   if (xmlDoc.getDocumentNode("/In/Update/ButtonPressed") != null) {
            buttonPressed = xmlDoc.getAttribute("/In/Update/ButtonPressed");
            error = xmlDoc.getFragments("/Error/errorlist/error");
      }

   if (xmlDoc.getDocumentNode("/In/PanelAuthorizationGroup") == null)
   {
         // This must come from the Reference Data page.
         //It may be update or insert mode (in which case, we set oid to 0)
            if(request.getParameter("oid") != null)
                 PAGroupOid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
            else
                 PAGroupOid = "0";
            //jgadela rel8.3 CR501
            pendingChangedObjectOid = PAGroupOid;

           if (PAGroupOid == null )
           {
               PAGroupOid = "0";
                 insertModeFlag = true;
           }
           else if (PAGroupOid.equals("0"))
           {
                 insertModeFlag = true;
           }
           else
           {
                 retrieveFromDBFlag = true;
                 insertModeFlag = false;
           }
   }
   else
   {
	   // jgadela T36000019754 R8.3 CR-501/Cr821 - redefined the initial display logic in this else block.
	   		 PAGroupOid = xmlDoc.getAttribute("/In/PanelAuthorizationGroup/panel_auth_group_oid");
	     	if(PAGroupOid == null || PAGroupOid.equals("0")){
	    	 	PAGroupOid = xmlDoc.getAttribute("/Out/PanelAuthorizationGroup/panel_auth_group_oid");
	     	}
	     	pendingChangedObjectOid = PAGroupOid;

	   		getDataFromXmlDocFlag = true;
	   		retrieveFromDBFlag = false;
            // This must be returned from ThresholdGroup with errors
       		// Check that maxerrorseverity is valid, redirect otherwise
            errorDoc.addComponent("/Error", xmlDoc.getComponent("/Error"));
            String maxError = errorDoc.getAttribute("/Error/maxerrorseverity");
            Debug.debug("Error Fragment is: " + errorDoc.toString());
            //jgadela rel8.3 CR501 [START]
     		pendingReferenceDataOid = xmlDoc.getAttribute("/Out/PendingReferenceData/oid");
     	    if(pendingReferenceDataOid == null || "".equals(pendingReferenceDataOid) || "null".equals(pendingReferenceDataOid)){
     	     	  pendingReferenceDataOid = xmlDoc.getAttribute("/In/PendingReferenceData/oid");
     	    }
     		//jgadela rel8.3 CR501 [END]

			if ((maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0 )) {
        		// We've returned from a save/update/delete that was successful
        		// We shouldn't be here.  Forward to the RefDataHome page.
    	 		insertModeFlag = false;
    	 		//jgadela Rel-8.3 09/10/2013 IR T36000020603 CR 501  [BEGIN] - Moved below code in to this block to correct error message
	  			  if((pendingReferenceDataOid == null || "".equals(pendingReferenceDataOid)) && !insertModeFlag) {
	  		    	  getDataFromXmlDocFlag = false;
	  		    	  retrieveFromDBFlag = true;
	  		      }
	  			//jgadela Rel-8.3 09/10/2013 IR T36000020603 CR 501 [END]

    		 } else {
    		        // We've returned from a save/update/delete but have errors.
    		        // We will display data from the cache.  Determine if we're
    		        // in insertMode by looking for a '0' oid in the input section
    		        // of the doc.
    		        String newOid = xmlDoc.getAttribute("/In/PanelAuthorizationGroup/panel_auth_group_oid");
    		        if (newOid.equals("0")) {
    		        	insertModeFlag = true;
    		            PAGroupOid = "0";
    		        } else {
    		           // Not in insert mode, use oid from input doc.
    		           insertModeFlag = false;
    		           //PAGroupOid = xmlDoc.getAttribute("/In/PanelAuthorizationGroup/panel_auth_group_oid");;
    		           //pendingChangedObjectOid = PAGroupOid;
    		        }
    		        errorFlag = true;
    		     }
   }

   if (retrieveFromDBFlag)
   {
    // paGroup.getById(PAGroupOid);
     paGroup.setAttribute("panel_auth_group_oid", PAGroupOid);
   //jgadela rel8.3 CR501 [START]
     if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction"))){
    	 showApprove = true;
    	 showReject = true;
     }
     String ownerShipLevel = userSession.getOwnershipLevel();
     if(TradePortalConstants.OWNER_GLOBAL.equals(ownerShipLevel) || TradePortalConstants.OWNER_BOG.equals(ownerShipLevel)){
    	 ownerShipLevel = TradePortalConstants.OWNER_BANK;
     }
     ReferenceDataPendingWebBean pendingObj = beanMgr.createBean(ReferenceDataPendingWebBean.class,"ReferenceDataPending");
     paGroup = (PanelAuthorizationGroupWebBean_Base) pendingObj.getReferenceData(paGroup, userSession.getUserOid(),showApprove);

	 //If changed by same user then do not show approve/reject buttons
     if (userSession.getUserOid().equals(pendingObj.getAttribute("change_user_oid")) || isReadOnly ||
			 !ownerShipLevel.equals(pendingObj.getAttribute("ownership_level")) || !userSecurityType.equals(pendingObj.getAttribute("changed_user_security_type"))){
		 showApprove = false;
    	 showReject = false;
	 }

	 pendingReferenceDataOid = pendingObj.getAttribute("pending_data_oid");
	 if (pendingObj.isReadOnly() && !isReadOnly) {
	     isReadOnly = true;
	    //jgadela Rel-8.3 09/10/2013 IR T36000020603  [BEGIN] - Verifying whether the doc is having any errors,
	    //if we have errord do not replace the error (Skip below next step)
	     boolean hasExistingErrDoc = false;
	     Vector  errorList1     = xmlDoc.getFragments("/Error/errorlist/error");
	     int errorLstSize = errorList1.size();
	     if (errorLstSize > 0) {
	    	    for (int i = 0; i < errorLstSize; i++) {
	    	      DocumentHandler errorDocu = (DocumentHandler) errorList1.elementAt(i);
	    	      int sevLevel = errorDocu.getAttributeInt("/severity");
	    	      if (sevLevel == ErrorManager.ERROR_SEVERITY) {
	    	    	  hasExistingErrDoc = true;
	    	      }
	     		}
	     }
	     //gadela Rel-8.3 09/10/2013 IR T36000020603  [END]

	     if(showApprove == false && !hasExistingErrDoc){
		     DocumentHandler errDoc = new DocumentHandler();
		     String errorMessage = resMgr.getText(TradePortalConstants.PENDING_USER_ALREADY_EXISTS2,"ErrorMessages");
		     String textM = resMgr.getText("PendingRefdata.PanelAuth","TextResources");
		     errorMessage = errorMessage.replace("{0}",textM);
		     errDoc.setAttribute("/severity", "5");
		     errDoc.setAttribute("/message", errorMessage);
		     xmlDoc.addComponent("/Error/errorlist/error", errDoc);
	     }
	 }

	 if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction"))) {
		 getDataFromXmlDocFlag = true;
		 retrieveFromDBFlag = false;
	 	 String xmlData = StringFunction.xssHtmlToChars(pendingObj.getAttribute("changed_object_data"));
	 	 xmlDoc = new DocumentHandler();
	 	 xmlDoc.setComponent("/In", new DocumentHandler(xmlData,false));  ;

	 	//jgadela rel8.3 CR501 -- Added if condition if the case of approving newly added pending record
    	 String change_type = pendingObj.getAttribute("change_type");
    	 if("C".equalsIgnoreCase(change_type)){
    		 insertModeFlag = true;
    		 PAGroupOid = "0";
    	 }
     }
	//jgadela rel8.3 CR501 [END]
   }

   if (getDataFromXmlDocFlag)
   {
	 	//jgadela rel8.3 CR501 [START]  - IR T36000018219
		  com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean pendingBean = null;
		  beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.ReferenceDataPendingWebBean", "ReferenceDataPending");

		  pendingBean = (ReferenceDataPendingWebBean) beanMgr.getBean("ReferenceDataPending");
		  pendingBean.getById(pendingReferenceDataOid);

		  String change_type = pendingBean.getAttribute("change_type");
		  if("C".equalsIgnoreCase(change_type)){
			  insertModeFlag = true;
	    		 PAGroupOid = "0";
		  }
		   //jgadela rel8.3 CR501 [END]
            paGroup.populateFromXmlDoc(xmlDoc.getComponent("/In"));
   }

   if (insertModeFlag)
            PAGroupOid="0";

   // Retrieve the user's locale, security rights, and organization oid
   userSecurityRights = userSession.getSecurityRights();
   userLocale         = userSession.getUserLocale();
   userOrgOid         = userSession.getOwnerOrgOid();
   clientBankOid      = userSession.getClientBankOid();
   // Retrieve the type of user so that we can get the appropriate read only rights; we can only be on this
   // page if we're a client bank user or a BOG user
   userType = userSession.getOwnershipLevel();


   corporateOrgOid = userOrgOid;
   corporateOrg.getById(corporateOrgOid );
   baseCurrency = corporateOrg.getAttribute("base_currency_code");
   //out.println("cooid " + corporateOrgOid + " currency "+ baseCurrency + "<br>");
	//jgadela rel8.3 CR501
   if(isReadOnly == false){
   		isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_PANEL_AUTH_GRP);
   }
	if(InstrumentServices.isNotBlank(paGroup.getAttribute("corp_org_oid"))){
	if(Integer.parseInt(userOrgOid)!=Integer.parseInt(paGroup.getAttribute("corp_org_oid")))
		isReadOnly=true;
	}
   showSaveButton = !isReadOnly;
   showDeleteButton = !isReadOnly;
   showSaveAs=!isReadOnly;
   showSaveClose = !isReadOnly;
   //out.println("ZInsertMode...."+insertModeFlag);
   if (insertModeFlag){
            showDeleteButton = false;
   			showSaveAs=false;
   	}

    PanelAuthorizationRangeWebBean panelAuthRange[] = new PanelAuthorizationRangeWebBean[6];
    PanelAuthorizationRuleWebBean forpanelAuthRange[][]=new PanelAuthorizationRuleWebBean[6][10];

      for (int iLoop=0; iLoop<6; iLoop++)
      {
            panelAuthRange[iLoop] = beanMgr.createBean(PanelAuthorizationRangeWebBean.class, "PanelAuthorizationRange");
            for(int jLoop=0;jLoop<10;jLoop++){
                forpanelAuthRange[iLoop][jLoop] = beanMgr.createBean(PanelAuthorizationRuleWebBean.class, "PanelAuthorizationRule");
            }
      }

      if (getDataFromXmlDocFlag) {
    	for(int iLoop=0; iLoop<6; iLoop++){
      	  DocumentHandler Doc = new DocumentHandler();
      	  Doc=xmlDoc.getComponent("/In/PanelAuthorizationGroup/panel_range_"+(iLoop+1));


      	if(Doc!=null){
      	  panelAuthRange[iLoop].setAttribute("panel_auth_range_oid", Doc.getAttribute("/panel_auth_range_oid"));
      	  panelAuthRange[iLoop].setAttribute("panel_auth_range_min_amt", Doc.getAttribute("/panel_auth_range_min_amt"));
      	  panelAuthRange[iLoop].setAttribute("panel_auth_range_max_amt", Doc.getAttribute("/panel_auth_range_max_amt"));

      	Vector  rulelist = Doc.getFragments("/PanelAuthorizationRuleList");
      	DocumentHandler rule = null;
        for(int jLoop=0;jLoop<rulelist.size();jLoop++){
      	  rule = (DocumentHandler) rulelist.elementAt(jLoop);

      	if(rule!=null){
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("panel_auth_rule_oid", rule.getAttribute("/panel_auth_rule_oid"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_sequence", rule.getAttribute("/approver_sequence"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_1", rule.getAttribute("/approver_1"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_2", rule.getAttribute("/approver_2"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_3", rule.getAttribute("/approver_3"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_4", rule.getAttribute("/approver_4"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_5", rule.getAttribute("/approver_5"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_6", rule.getAttribute("/approver_6"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_7", rule.getAttribute("/approver_7"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_8", rule.getAttribute("/approver_8"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_9", rule.getAttribute("/approver_9"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_10", rule.getAttribute("/approver_10"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_11", rule.getAttribute("/approver_11"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_12", rule.getAttribute("/approver_12"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_13", rule.getAttribute("/approver_13"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_14", rule.getAttribute("/approver_14"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_15", rule.getAttribute("/approver_15"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_16", rule.getAttribute("/approver_16"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_17", rule.getAttribute("/approver_17"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_18", rule.getAttribute("/approver_18"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_19", rule.getAttribute("/approver_19"));
      	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_20", rule.getAttribute("/approver_20"));
      	}
        }
        }
    	}

      try
        {
           corporateOrg.populateFromXmlDoc(xmlDoc.getFragment("/In"));

        }
        catch (Exception e)
        {
           System.out.println("Contact Administrator: Unable to get CorporateOrganization attributes " +
                              "for oid: " + corporateOrgOid + " " + e.toString());
        }
    }

    if (!insertModeFlag || errorFlag)
    {
      allowTBAIndicator = corporateOrg.getAttribute("allow_xfer_btwn_accts_panel");
      allowDomesticPmtIndicator = corporateOrg.getAttribute("allow_domestic_payments_panel");
      allowInternationalPmtInd = corporateOrg.getAttribute("allow_funds_transfer_panel");
    }
            StringBuffer sql = new StringBuffer();
            queryListView = null;

      // Retrieve Panel ranges for all the Instrument Types
      if(retrieveFromDBFlag) {
            //try {
            insertModeFlag = false;

              queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
              sql.append("select * from panel_auth_range");
              sql.append(" where panel_auth_range_oid in( ");
              List<Object> sqlParamsPanel = new ArrayList();
              List rangeoids=new ArrayList();
              if(!InstrumentServices.isBlank(paGroup.getAttribute("panel_range_1_oid")))
            	  rangeoids.add(paGroup.getAttribute("panel_range_1_oid"));
              if(!InstrumentServices.isBlank(paGroup.getAttribute("panel_range_2_oid")))
            	  rangeoids.add(paGroup.getAttribute("panel_range_2_oid"));
              if(!InstrumentServices.isBlank(paGroup.getAttribute("panel_range_3_oid")))
            	  rangeoids.add(paGroup.getAttribute("panel_range_3_oid"));
              if(!InstrumentServices.isBlank(paGroup.getAttribute("panel_range_4_oid")))
            	  rangeoids.add(paGroup.getAttribute("panel_range_4_oid"));
              if(!InstrumentServices.isBlank(paGroup.getAttribute("panel_range_5_oid")))
            	  rangeoids.add(paGroup.getAttribute("panel_range_5_oid"));
              if(!InstrumentServices.isBlank(paGroup.getAttribute("panel_range_6_oid")))
            	  rangeoids.add(paGroup.getAttribute("panel_range_6_oid"));

              for(int i=0;i<rangeoids.size();i++){
            	  sql.append("?");
            	  sqlParamsPanel.add((String)rangeoids.get(i));
            	  if(i!=rangeoids.size()-1){
            		  sql.append(" , ");
            	  }
              }

              sql.append(")");
              sql.append(" order by ");
              sql.append("panel_auth_range_min_amt");

              int numOfItems=0;
              Debug.debug("Prateep Gedupudi"+sql.toString());
			  if(rangeoids.size()>0){
	              queryListView.setSQL(sql.toString(),sqlParamsPanel);
	              queryListView.getRecords();

	              DocumentHandler panelAuthRangeList = new DocumentHandler();
	              panelAuthRangeList = queryListView.getXmlResultSet();
	              Debug.debug(panelAuthRange.toString());


	              vVector = panelAuthRangeList.getFragments("/ResultSetRecord");
	              numOfItems = vVector.size();
	              Debug.debug("Record Count for Transfer Between Accounts "+numOfItems);
			  }
              
              DocumentHandler panelAuthDoc = null;
              for(int iLoop=0; iLoop<numOfItems; iLoop++){
            	  if(vVector!=null)
            	  panelAuthDoc = (DocumentHandler) vVector.elementAt(iLoop);
            	  /* Doc=xmlDoc.getComponent("/In/PanelAuthorizationGroup/panel_range_"+(iLoop+1)); */
            	  if(panelAuthDoc!=null){
            	  panelAuthRange[iLoop].setAttribute("panel_auth_range_oid", panelAuthDoc.getAttribute("/PANEL_AUTH_RANGE_OID"));
            	  panelAuthRange[iLoop].setAttribute("panel_auth_range_min_amt", panelAuthDoc.getAttribute("/PANEL_AUTH_RANGE_MIN_AMT"));
            	  panelAuthRange[iLoop].setAttribute("panel_auth_range_max_amt", panelAuthDoc.getAttribute("/PANEL_AUTH_RANGE_MAX_AMT"));

            	   if(panelAuthDoc.getAttribute("/PANEL_AUTH_RANGE_OID")!=null){
            	  StringBuffer sqlrule=new StringBuffer();
            	//Rel 8.3 IR T36000020412 - Order by panel_auth_rule_oid added, to display the rules in the order in which they were entered.
            	  sqlrule.append("select * from panel_auth_rule");
            	//MEerupula - 02 Sept 2013 - Rel8.3.0 IR-T36000020449 ADD BEGIN
            	// The sql query was modified to do order by on panel rule oid instead of approver sequence.
				  sqlrule.append(" where approver_sequence is not null and p_panel_auth_range_oid= ? order by panel_auth_rule_oid asc");
            	//MEerupula - 02 Sept 2013 - Rel8.3.0 IR-T36000020449 ADD END

            	  Debug.debug("Prateep Gedupudi rule"+sqlrule.toString());
                  queryListView.setSQL(sqlrule.toString(),new Object[]{ panelAuthDoc.getAttribute("/PANEL_AUTH_RANGE_OID")});
                  queryListView.getRecords();

                  DocumentHandler panelAuthRuleList = new DocumentHandler();
                  panelAuthRuleList = queryListView.getXmlResultSet();

                  Vector rulelist = panelAuthRuleList.getFragments("/ResultSetRecord");
                  /* out.println(rulelist.size()); */
                  DocumentHandler rule = null;
                  for(int jLoop=0;jLoop<rulelist.size();jLoop++){
                	  rule = (DocumentHandler) rulelist.elementAt(jLoop);
                	  /* out.print(rule);
                	  out.print(rule.getAttribute("/APPROVER_SEQUENCE")); */
                	  if(rule!=null){
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("panel_auth_rule_oid", rule.getAttribute("/PANEL_AUTH_RULE_OID"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_sequence", rule.getAttribute("/APPROVER_SEQUENCE"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_1", rule.getAttribute("/APPROVER_1"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_2", rule.getAttribute("/APPROVER_2"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_3", rule.getAttribute("/APPROVER_3"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_4", rule.getAttribute("/APPROVER_4"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_5", rule.getAttribute("/APPROVER_5"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_6", rule.getAttribute("/APPROVER_6"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_7", rule.getAttribute("/APPROVER_7"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_8", rule.getAttribute("/APPROVER_8"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_9", rule.getAttribute("/APPROVER_9"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_10", rule.getAttribute("/APPROVER_10"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_11", rule.getAttribute("/APPROVER_11"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_12", rule.getAttribute("/APPROVER_12"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_13", rule.getAttribute("/APPROVER_13"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_14", rule.getAttribute("/APPROVER_14"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_15", rule.getAttribute("/APPROVER_15"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_16", rule.getAttribute("/APPROVER_16"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_17", rule.getAttribute("/APPROVER_17"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_18", rule.getAttribute("/APPROVER_18"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_19", rule.getAttribute("/APPROVER_19"));
	                	  forpanelAuthRange[iLoop][jLoop].setAttribute("approver_20", rule.getAttribute("/APPROVER_20"));
                	  }
                  }
            	  }

              }
              }



            if (queryListView != null) {
                queryListView.remove();
              }
      }
/* Get sequence and approver description from refdata if panel aliases are described  in corporate org then get from it and merge */
      Hashtable refDataPanelSequences = new Hashtable();
    //MEer Rel 8.3 IR-19699- Updated method call with locale as parameter
      refDataPanelSequences = ReferenceDataManager.getRefDataMgr().getAllCodeAndDescr("PANEL_APPROVE_SEQUENCE", userSession.getUserLocale());
      List keyseqList=new ArrayList(refDataPanelSequences.keySet());
		StringBuffer refsequences=new StringBuffer();
		refsequences.append("{id: '', name: '',value:''},");

		Collections.sort(keyseqList);

		for(Iterator i=keyseqList.iterator();i.hasNext();){
			String key=(String)i.next();
			refsequences.append("{");
			refsequences.append("id:'").append(key).append("'");
			refsequences.append(", ");
			refsequences.append("name:'").append(refDataPanelSequences.get(key)).append("'");
			refsequences.append(", ");
			refsequences.append("value:'").append(key).append("'");
			refsequences.append("} ");
				if(i.hasNext()){
					refsequences.append(" , ");
				}
			}
%>

<%-- ********************* HTML for page begins here *********************  --%>
<%-- <jsp:include page="/common/ButtonPrep.jsp" /> --%>

<jsp:include page="/common/Header.jsp">
      <jsp:param name="includeNavigationBarFlag"
            value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="additionalOnLoad" value="" />
      <jsp:param name="autoSaveFlag" value="<%=true%>" />
</jsp:include>
<div class="pageMain">
      <div class="pageContent">
      <%-- PageHeader starts --%>
            <%
                  String pageTitleKey = resMgr.getText("RefDataHome.PanelAuthGroup",
                              TradePortalConstants.TEXT_BUNDLE);
                  String helpUrl = "/customer/panel_auth_group_overview.htm";
            %>

            <jsp:include page="/common/PageHeader.jsp">
                  <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
                  <jsp:param name="item1Key" value="" />
                  <jsp:param name="helpUrl" value="<%=helpUrl%>" />
            </jsp:include>
      <%-- PageHeader end --%>
      <%-- PageSubHeader starts --%>
<%
   // jgadela R8.3 IR - T36000019754. - To show the panel name in the title when newly saved/created
   String subHeaderTitle = paGroup.getAttribute("name");;
   if (subHeaderTitle == null || InstrumentServices.isBlank(subHeaderTitle)) {
	    subHeaderTitle = resMgr.getText( "PanelAuthorizationGroupDetail.NewPanelAuthGroup",
	    TradePortalConstants.TEXT_BUNDLE);
   }

  // figures out how to return to the calling page
  StringBuffer parms = new StringBuffer();
  parms.append("&returnAction=");
  parms.append(EncryptDecrypt.encryptStringUsingTripleDes(
    "goToRefDataHome", userSession.getSecretKey()));
  String returnLink = formMgr.getLinkAsUrl("goToRefDataHome",
    parms.toString(), response);
%>
            <jsp:include page="/common/PageSubHeader.jsp">
              <jsp:param name="title" value="<%= subHeaderTitle %>" />
            </jsp:include>

            <%-- PageSubHeader ends --%>

<form id="PanelAuthGroupForm" name="PanelAuthGroupForm" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
  <input type=hidden value="" name=saveAsFlag>

  <%--jgadela rel8.3 CR501 [START] --%>
  <input type=hidden name="PendingReferenceDataOid" value="<%=pendingReferenceDataOid %>">
	<input type=hidden name="pendingChangedObjectOid" value="<%=pendingChangedObjectOid %>">
	<input type=hidden name="UserType" value="CORPORATE">
	<%
	if(TradePortalConstants.INDICATOR_YES.equalsIgnoreCase(panelAuthDualCtrlReqdInd)){
	%>
	  <input type=hidden name="isDualControlRequired" value="Y">
	<%
	}else{
	%>
	  <input type=hidden name="isDualControlRequired" value="N">
	<%}%>
<%--jgadela rel8.3 CR501 [END] --%>
<%
   // Store the Corporate Org oid (if there is one) and other necessary info in a secure hashtable for the form
   //secureParms.put("AddressOid", addressOid);
   secureParms.put("UserOid",                  userSession.getUserOid());
   secureParms.put("login_oid",                userSession.getUserOid());
   secureParms.put("login_rights",             userSession.getSecurityRights());
   secureParms.put("ownership_level",userSession.getOwnershipLevel());
   secureParms.put("panel_auth_group_oid", PAGroupOid);
   secureParms.put("opt_lock", paGroup.getAttribute("opt_lock"));
   secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
   secureParms.put("corp_org_oid", userSession.getOwnerOrgOid());


   if (insertModeFlag)
   {
      secureParms.put("ClientBankOid", clientBankOid);

      if (userType.equals(TradePortalConstants.OWNER_BOG))
      {
         secureParms.put("BankOrgGroupOid", EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()) );
      }
   }
   else
   {
      //secureParms.put("opt_lock", address.getAttribute("opt_lock"));
   }
%>

<%= formMgr.getFormInstanceAsInputField("PanelAuthGroupForm", secureParms) %>
<%-- formArea starts --%>
<div class="formArea">
                        <jsp:include page="/common/ErrorSection.jsp" />
                        <%-- formContent starts --%>
                        <div class="formContent">


				  <%
                  if(!insertModeFlag) {%>
                        <input type=hidden name="panel_auth_group_oid" value="<%=EncryptDecrypt.encryptStringUsingTripleDes( PAGroupOid, userSession.getSecretKey())%>">
                  <% }else {%>
                        <input type=hidden name="panel_auth_group_oid" value="<%=EncryptDecrypt.encryptStringUsingTripleDes( "0", userSession.getSecretKey())%>">
                  <%} %>
                  <input type=hidden name="owner_org_oid" value="<%=StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid())%>">

                  <div data-dojo-type="dojo.store.Memory"
    data-dojo-id="seqStore"
    data-dojo-props="data: [<%= refsequences%>]">
</div>


<%@ include file="/transactions/fragments/LoadPanelLevelAliases.frag" %>
<%
StringBuffer stores[]=new StringBuffer[6];
for(int i=0;i<6;i++){
	StringBuffer store = new StringBuffer(7000);
	for(int j=0;j<10;j++){
		store.append("{ ");
		store.append("oid:'");
		if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("panel_auth_rule_oid")))
			//store.append(EncryptDecrypt.encryptStringUsingTripleDes( "0", userSession.getSecretKey())).append("'");
			store.append(EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey())).append("'");
		else
			store.append(EncryptDecrypt.encryptStringUsingTripleDes( forpanelAuthRange[i][j].getAttribute("panel_auth_rule_oid"), userSession.getSecretKey())).append("'");
	      store.append(", ");
	      store.append("approver_sequence:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_sequence")))
	    	store.append("'");
	    else
	    	store.append(forpanelAuthRange[i][j].getAttribute("approver_sequence")).append("'");
	    store.append(", ");
	      store.append("approver_sequence_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_sequence")))
	    	store.append("'");
	    else
	    	store.append(refDataPanelSequences.get(forpanelAuthRange[i][j].getAttribute("approver_sequence"))).append("'");
	      store.append(", ");
	      store.append("approver_1:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_1")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_1")).append("'");
	    store.append(", ");
	      store.append("approver_1_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_1")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_1")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_2:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_2")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_2")).append("'");
	    store.append(", ");
	      store.append("approver_2_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_2")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_2")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_3:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_3")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_3")).append("'");
	    store.append(", ");
	      store.append("approver_3_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_3")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_3")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_4:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_4")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_4")).append("'");
	    store.append(", ");
	      store.append("approver_4_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_4")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_4")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_5:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_5")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_5")).append("'");
	    store.append(", ");
	      store.append("approver_5_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_5")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_5")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_6:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_6")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_6")).append("'");
	    store.append(", ");
	      store.append("approver_6_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_6")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_6")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_7:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_7")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_7")).append("'");
	    store.append(", ");
	      store.append("approver_7_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_7")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_7")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_8:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_8")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_8")).append("'");
	    store.append(", ");
	      store.append("approver_8_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_8")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_8")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_9:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_9")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_9")).append("'");
	    store.append(", ");
	      store.append("approver_9_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_9")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_9")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_10:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_10")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_10")).append("'");
	    store.append(", ");
	      store.append("approver_10_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_10")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_10")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_11:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_11")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_11")).append("'");
	    store.append(", ");
	      store.append("approver_11_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_11")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_11")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_12:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_12")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_12")).append("'");
	    store.append(", ");
	      store.append("approver_12_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_12")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_12")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_13:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_13")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_13")).append("'");
	    store.append(", ");
	      store.append("approver_13_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_13")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_13")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_14:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_14")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_14")).append("'");
	    store.append(", ");
	      store.append("approver_14_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_14")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_14")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_15:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_15")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_15")).append("'");
	    store.append(", ");
	      store.append("approver_15_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_15")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_15")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_16:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_16")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_16")).append("'");
	    store.append(", ");
	      store.append("approver_16_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_16")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_16")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_17:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_17")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_17")).append("'");
	    store.append(", ");
	      store.append("approver_17_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_17")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_17")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_18:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_18")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_18")).append("'");
	    store.append(", ");
	      store.append("approver_18_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_18")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_18")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_19:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_19")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_19")).append("'");
	    store.append(", ");
	      store.append("approver_19_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_19")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_19")).toString())).append("'");
	    store.append(", ");
	      store.append("approver_20:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_20")))
		  	store.append("'");
		else
		    store.append(forpanelAuthRange[i][j].getAttribute("approver_20")).append("'");
	    store.append(", ");
	      store.append("approver_20_dsc:'");
	    if(InstrumentServices.isBlank(forpanelAuthRange[i][j].getAttribute("approver_20")))
		  	store.append("'");
		else
		    store.append(StringFunction.escapeQuotesforJS(corporgpanelAliases.get(forpanelAuthRange[i][j].getAttribute("approver_20")).toString())).append("'");


	      store.append("}");

	      if (j < 9) {
	        store.append(", ");
	      }
	}
	stores[i]=store;
}

panelrange0=stores[0];
panelrange1=stores[1];
panelrange2=stores[2];
panelrange3=stores[3];
panelrange4=stores[4];
panelrange5=stores[5];
//out.println(panelrange1);
%>

  <%-- General Tab Starts Here--%>
  <%=widgetFactory.createSectionHeader("1","PanelAuthorizationGroupDetail.General")%>
            <div>
            <div class="columnLeftPanel">
           		 <%--MEer Rel 8.3 IR-22567  Increased PanelGroupId size from 4 to 5--%>
                  <%=widgetFactory.createTextField("PanelAuthorizationGroupId","PanelAuthorizationGroupDetail.PanelAuthorizationGroupID",paGroup.getAttribute("panel_auth_group_id"), "5",isReadOnly, true, false, "", "", "")%>
                  <%=widgetFactory.createNote("PanelAuthorizationGroupDetail.PanelAuthorizationGroupIdComment","formItem")%>
                  <%=widgetFactory.createTextField("Name","PanelAuthorizationGroupDetail.PanelAuthorizationGroupName",paGroup.getAttribute("name"), "25", isReadOnly,true, false, "", "", "")%>

                  <%
            	   dropdownOptions = Dropdown.createSortedRefDataOptions("CURRENCY_BASIS", paGroup.getAttribute("ccy_basis"), userLocale);
            	   out.print(widgetFactory.createSelectField( "ccy_basis", "PanelAuthorizationGroupDetail.currbasis", " ", dropdownOptions, isReadOnly,true,false,"class='char15'","",""));
             	%>
            </div>

            <div class="columnRightPanel">
            		<%= widgetFactory.createLabel("instrument_group_type","PanelAuthorizationGroupDetail.instrumenttype",false,true,false,"","") %>
            		<%
					  String instrument_group_type = paGroup.getAttribute("instrument_group_type");

					 /*  if(InstrumentServices.isBlank(instrument_group_type))
						  instrument_group_type = "PYMTS"; */
					%>
            		<%= widgetFactory.createRadioButtonField("instrument_group_type", "payments",
				         "PanelAuthorizationGroupDetail.payments", "PYMTS", instrument_group_type.equals("PYMTS"), isReadOnly, "", "")%>

				         <div id="payments_section" class="radiomarginleft" style="display:none">
				         	<%
							  String payment_type = paGroup.getAttribute("payment_type");

							  /* if(InstrumentServices.isBlank(payment_type))
								  payment_type = "INT_PYMT"; */
							%>
				         	<%= widgetFactory.createRadioButtonField("payment_type", "int_payment",
				         	"PanelAuthorizationGroupDetail.intpayment", "INT_PYMT", payment_type.equals("INT_PYMT"), isReadOnly, "", "")%>
				         	<div id="intpaymentconfind"></div>

					         <%= widgetFactory.createRadioButtonField("payment_type", "payment",
					         "PanelAuthorizationGroupDetail.payment", "PYMT", payment_type.equals("PYMT"), isReadOnly, "", "")%>
					         <div id="payment_type_section" style="display:none">
					         	<div class="PanelAuthGroupLeft">
					         		<%
					         		defaultCheckBoxValue = false;
					         		String ACH_pymt_method_ind=paGroup.getAttribute("ACH_pymt_method_ind");
					     		   if (ACH_pymt_method_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
					         		<%=widgetFactory.createCheckboxField("ACH_pymt_method_ind", "PanelAuthorizationGroupDetail.achgiro", defaultCheckBoxValue, isReadOnly,false,"","","") %>
					         		<%
					         		defaultCheckBoxValue = false;
					         		String BCHK_pymt_method_ind=paGroup.getAttribute("BCHK_pymt_method_ind");
					     		   if (BCHK_pymt_method_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
					         		<%=widgetFactory.createCheckboxField("BCHK_pymt_method_ind", "PanelAuthorizationGroupDetail.bankchq", defaultCheckBoxValue, isReadOnly,false,"","","") %>
					         		<%
					         		defaultCheckBoxValue = false;
					         		String BKT_pymt_method_ind=paGroup.getAttribute("BKT_pymt_method_ind");
					     		   if (BKT_pymt_method_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
					         		<%=widgetFactory.createCheckboxField("BKT_pymt_method_ind", "PanelAuthorizationGroupDetail.booktrans", defaultCheckBoxValue, isReadOnly,false,"","","") %>
					         		<%
					         		defaultCheckBoxValue = false;
					         		String CBFT_pymt_method_ind=paGroup.getAttribute("CBFT_pymt_method_ind");
					     		   if (CBFT_pymt_method_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
					         		<%=widgetFactory.createCheckboxField("CBFT_pymt_method_ind", "PanelAuthorizationGroupDetail.crossbrft", defaultCheckBoxValue, isReadOnly,false,"","","") %>
					         	</div>
					         	<div class="PanelAuthGroupRight">
					         		<%
					         		defaultCheckBoxValue = false;
					         		String CCHK_pymt_method_ind=paGroup.getAttribute("CCHK_pymt_method_ind");
					     		   if (CCHK_pymt_method_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
					         		<%=widgetFactory.createCheckboxField("CCHK_pymt_method_ind", "PanelAuthorizationGroupDetail.customerchq", defaultCheckBoxValue, isReadOnly,false,"","","") %>
					         		<%
					         		defaultCheckBoxValue = false;
					         		String RTGS_pymt_method_ind=paGroup.getAttribute("RTGS_pymt_method_ind");
					     		   if (RTGS_pymt_method_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("RTGS_pymt_method_ind", "PanelAuthorizationGroupDetail.rtgs", defaultCheckBoxValue, isReadOnly,false,"","","") %>
						         	<%
					         		defaultCheckBoxValue = false;
					         		String FTBA_pymt_method_ind=paGroup.getAttribute("FTBA_pymt_method_ind");
					     		   if (FTBA_pymt_method_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("FTBA_pymt_method_ind", "PanelAuthorizationGroupDetail.transferbwacc", defaultCheckBoxValue, isReadOnly,false,"","","") %>
					         	</div>
					         	<div style="clear: both;"></div>
					         	<%--
					         		Madhuri - Rel 8.3 SysTest IR# T36000020742 09/13/2013 - Begin
					         		Reverted the changes made for IR 20411 as this is causing potential problem --%>

					         	 </div>
					         	 <%--  Madhuri - Rel 8.3 SysTest IR# T36000020742 09/13/2013 - End	--%>
					         	<%--
					         		Sandeep - Rel 8.3 SysTest IR# T36000020411 09/05/2013 - Begin
					         		Moved the below div form out side to this main div,
					         		Reason is 'panelgroupconfpaymentinst' checkbox should display only on selection of 'Payment' radiobutton.
					         		Changes are made in panelauthgroup.js file in few lines of code, added comment in those lines.
					         	--%>
					         	<div id="paymentconfind"></div>
					         	<%-- Sandeep - Rel 8.3 SysTest IR# T36000020411 09/05/2013 - End --%>


					         <div id="panelgroupconfpaymentinstsection">
			             		<%=widgetFactory.createCheckboxField("panelgroupconfpaymentinst", "PanelAuthorizationGroupDetail.panelgroupconfpaymentinst", paGroup.getAttribute("panelgroupconfpaymentinst").equals(TradePortalConstants.INDICATOR_YES), isReadOnly) %>
			             	</div>
				         </div>

				    <div style="clear: both;"></div>
				    <%--Rel9.0 CR 913 Start --%>
				    <%= widgetFactory.createRadioButtonField("instrument_group_type", "payables",
                          "PanelAuthorizationGroupDetail.payables", "PAYB", instrument_group_type.equals("PAYB"), isReadOnly, "", "")%>
                          <div id="payables_section" style="display:none" class = "radiomarginleft">
                                <%
                                    defaultCheckBoxValue = false;
                                    String pay_mgm_inv_auth_type=paGroup.getAttribute("pay_mgm_inv_auth_ind");
                                    if (TradePortalConstants.INDICATOR_YES.equals(pay_mgm_inv_auth_type))
                                    {
                                       defaultCheckBoxValue = true;
                                    }
                                 %>
                                <%=widgetFactory.createCheckboxField("pay_mgm_inv_auth_ind", "PanelAuthorizationGroupDetail.PayableManagementInvoiceUpdates", defaultCheckBoxValue, isReadOnly,false,"","","") %>
                                <%
                                    defaultCheckBoxValue = false;
                                    String pay_upl_inv_auth_type=paGroup.getAttribute("pay_upl_inv_auth_ind");
                                    if (TradePortalConstants.INDICATOR_YES.equals(pay_upl_inv_auth_type))
                                    {
                                       defaultCheckBoxValue = true;
                                    }
                                 %>
                                <%=widgetFactory.createCheckboxField("pay_upl_inv_auth_ind", "PanelAuthorizationGroupDetail.UploadedInvoiceAuthorisation", defaultCheckBoxValue, isReadOnly,false,"","","") %>    
                         		<%--Rel9.2 CR 914A Start --%>
                         		<%
                                    defaultCheckBoxValue = false;
                                    String pay_credit_note_auth_ind=paGroup.getAttribute("pay_credit_note_auth_ind");
                                    if (TradePortalConstants.INDICATOR_YES.equals(pay_credit_note_auth_ind))
                                    {
                                       defaultCheckBoxValue = true;
                                    }
                                 %>
                                <%=widgetFactory.createCheckboxField("pay_credit_note_auth_ind", "PanelAuthorizationGroupDetail.PayablesCreditNoteAuthorisation", defaultCheckBoxValue, isReadOnly,false,"","","") %> 
                         		<%--Rel9.2 CR 914A End --%>
                         </div>
                    <div style="clear: both;"></div>
                    <%--Rel9.0 CR 913 End --%>
				    <%= widgetFactory.createRadioButtonField("instrument_group_type", "receivables",
				          "PanelAuthorizationGroupDetail.receivables", "RECV", instrument_group_type.equals("RECV"), isReadOnly, "", "")%>
				          <div id="receivables_section" style="display:none">
				          		<%
					         		defaultCheckBoxValue = false;
					         		String credit_auth_ind=paGroup.getAttribute("credit_auth_ind");
					     		   if (credit_auth_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				         		<%=widgetFactory.createCheckboxField("credit_auth_ind", "PanelAuthorizationGroupDetail.creditnoteauth", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				         		<%
					         		defaultCheckBoxValue = false;
					         		String inv_auth_ind=paGroup.getAttribute("inv_auth_ind");
					     		   if (inv_auth_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
					         	<%=widgetFactory.createCheckboxField("inv_auth_ind", "PanelAuthorizationGroupDetail.invauth", defaultCheckBoxValue, isReadOnly,false,"","","") %>
					         	<%
					         		defaultCheckBoxValue = false;
					         		String matchin_approval_ind=paGroup.getAttribute("matchin_approval_ind");
					     		   if (matchin_approval_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
					         	<%=widgetFactory.createCheckboxField("matchin_approval_ind", "PanelAuthorizationGroupDetail.matcappr", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				         </div>
				    <div style="clear: both;"></div>
				    <%= widgetFactory.createRadioButtonField("instrument_group_type", "trade",
				          "PanelAuthorizationGroupDetail.trade", "TRADE", instrument_group_type.equals("TRADE"), isReadOnly, "", "")%>
				          <div id="trade_section" style="display:none">
				          		<div class="PanelAuthGroupLeft">
				          			<%
					         		defaultCheckBoxValue = false;
					         		String AIR_ind=paGroup.getAttribute("AIR_ind");
					     		   if (AIR_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("AIR_ind", "PanelAuthorizationGroupDetail.airwaybills", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%
					         		defaultCheckBoxValue = false;
					         		String ATP_ind=paGroup.getAttribute("ATP_ind");
					     		   if (ATP_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("ATP_ind", "PanelAuthorizationGroupDetail.aprtopay", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%
					         		defaultCheckBoxValue = false;
					         		String EXP_COL_ind=paGroup.getAttribute("EXP_COL_ind");
					     		   if (EXP_COL_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("EXP_COL_ind", "PanelAuthorizationGroupDetail.dscoll", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%
					         		defaultCheckBoxValue = false;
					         		String EXP_DLC_ind=paGroup.getAttribute("EXP_DLC_ind");
					     		   if (EXP_DLC_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("EXP_DLC_ind", "PanelAuthorizationGroupDetail.explc", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%
					         		defaultCheckBoxValue = false;
					         		String EXP_OCO_ind=paGroup.getAttribute("EXP_OCO_ind");
					     		   if (EXP_OCO_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("EXP_OCO_ind", "PanelAuthorizationGroupDetail.expcoll", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%
					         		defaultCheckBoxValue = false;
					     		   if (paGroup.getAttribute("import_col_ind").equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("ImportCollectionsInd", "PanelAuthorizationGroupDetail.ImportCollections", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%
					         		defaultCheckBoxValue = false;
					         		String IMP_DLC_ind=paGroup.getAttribute("IMP_DLC_ind");
					     		   if (IMP_DLC_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("IMP_DLC_ind", "PanelAuthorizationGroupDetail.implc", defaultCheckBoxValue, isReadOnly,false,"","","") %>
									<%-- SSikhakolli - Rel-8.3 Sys Test IR# T36000020411 on 10/21/2013 - Begin--%>
				          		<%
					         		defaultCheckBoxValue = false;
					         		String LRQ_ind=paGroup.getAttribute("LRQ_ind");
					     		   if (LRQ_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("LRQ_ind", "PanelAuthorizationGroupDetail.lr", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%-- SSikhakolli - Rel-8.3 Sys Test IR# T36000020411 on 10/21/2013 - End--%>
				          		</div>
				          		<div class="PanelAuthGroupRight">
				          			<%
					         		defaultCheckBoxValue = false;
					         		String GUA_ind=paGroup.getAttribute("GUA_ind");
					     		   if (GUA_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
				          			<%=widgetFactory.createCheckboxField("GUA_ind", "PanelAuthorizationGroupDetail.outguarant", defaultCheckBoxValue, isReadOnly,false,"","","") %>
				          			<%
					         		defaultCheckBoxValue = false;
					         		String SLC_ind=paGroup.getAttribute("SLC_ind");
					     		   if (SLC_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("SLC_ind", "PanelAuthorizationGroupDetail.outstandlc", defaultCheckBoxValue, isReadOnly,false,"","","") %>
						         	<%
					         		defaultCheckBoxValue = false;
					         		String RQA_ind=paGroup.getAttribute("RQA_ind");
					     		   if (RQA_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("RQA_ind", "PanelAuthorizationGroupDetail.reqtoadv", defaultCheckBoxValue, isReadOnly,false,"","","") %>
						         	<%
					         		defaultCheckBoxValue = false;
					         		String SHP_ind=paGroup.getAttribute("SHP_ind");
					     		   if (SHP_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("SHP_ind", "PanelAuthorizationGroupDetail.shippgua", defaultCheckBoxValue, isReadOnly,false,"","","") %>
						         	<%
					         		defaultCheckBoxValue = false;
					         		String SP_ind=paGroup.getAttribute("SP_ind");
					     		   if (SP_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("SP_ind", "PanelAuthorizationGroupDetail.supplpor", defaultCheckBoxValue, isReadOnly,false,"","","") %>
									<%-- SSikhakolli - Rel-8.3 Sys Test IR# T36000020411 on 10/21/2013 - Begin --%>
				         		<%
					         		defaultCheckBoxValue = false;
					         		String DCR_ind=paGroup.getAttribute("DCR_ind");
					     		   if (DCR_ind.equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("DCR_ind", "PanelAuthorizationGroupDetail.descrepancyres", defaultCheckBoxValue, isReadOnly,false,"labelClass=\"shortWidth\"","","") %>
						         	<%-- SSikhakolli - Rel-8.3 Sys Test IR# T36000020411 on 10/21/2013 - End--%>
						         	<%-- SSikhakolli - Rel-9.4 CR-818 --%>
						         	<%
					         		defaultCheckBoxValue = false;
					     		    if (paGroup.getAttribute("settlement_instr_ind").equals(TradePortalConstants.INDICATOR_YES))
					                {
					                   defaultCheckBoxValue = true;
					                }
					         		%>
						         	<%=widgetFactory.createCheckboxField("SettlementInstrMsgResponse", "PanelAuthorizationGroupDetail.SettlementInstrMsgResponse", defaultCheckBoxValue, isReadOnly,false,"labelClass=\"shortWidth\"","","") %>
				          		</div>
				         		<div style="clear: both;"></div>
				         		
					     </div>
				    	<div style="clear: both;"></div>
            	</div>
            </div>
  </div><%-- General Tab Ends--%>

  <%=widgetFactory.createSectionHeader("2","PanelAuthorizationGroupDetail.PanelAmtRanges")%>
  			<%/* Getting values for Panel range min and max amount starts*/
  			for(int i=0;i<6;i++){
                       if(!InstrumentServices.isBlank(panelAuthRange[i].getAttribute("panel_auth_range_min_amt"))) {
                              panelRangeMinAmount = TPCurrencyUtility.getDisplayAmountWithoutCommaSeperator(panelAuthRange[i].getAttribute("panel_auth_range_min_amt"), baseCurrency, userLocale);
                        } else {
                              panelRangeMinAmount = panelAuthRange[i].getAttribute("panel_auth_range_min_amt");
                        }
                        if(!InstrumentServices.isBlank(panelAuthRange[i].getAttribute("panel_auth_range_max_amt"))) {
                              panelRangeMaxAmount = TPCurrencyUtility.getDisplayAmountWithoutCommaSeperator(panelAuthRange[i].getAttribute("panel_auth_range_max_amt"), baseCurrency, userLocale);
                        } else {
                              panelRangeMaxAmount = panelAuthRange[i].getAttribute("panel_auth_range_max_amt");
                        }


                        if(InstrumentServices.isNotBlank(panelAuthRange[i].getAttribute("panel_auth_range_oid"))) {
                              out.print("<INPUT TYPE=HIDDEN NAME='panel_range"+i+"_oid' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( panelAuthRange[i].getAttribute("panel_auth_range_oid"), userSession.getSecretKey()) + "'>");
                        } else {
                            //out.print("<INPUT TYPE=HIDDEN NAME='panel_range"+i+"_oid' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( "0", userSession.getSecretKey()) + "'>");
							  out.print("<INPUT TYPE=HIDDEN NAME='panel_range"+i+"_oid' VALUE=''>");
                        }
            %>
            <div id="hiddenvarsrange<%=i%>" style="display: none;"></div>
            <div class="gridSearch">
            	<div class="searchDetail">
	            	<span class="searchCriteria">
	            		<div class="searchItem">
	            			<b><%=i+1 %>.</b>
	            			<%=resMgr.getText("PanelAuthorizationGroupDetail.PmntAmtRange", TradePortalConstants.TEXT_BUNDLE) %>
	            			<%=widgetFactory.createAmountField("panel_range"+i+"_MinAmt","",panelRangeMinAmount, "", isReadOnly, false, false,"class='char10'", "", "none")%>
	                        <%=resMgr.getText("PanelAuthorizationGroupDetail.to", TradePortalConstants.TEXT_BUNDLE) %>
	                        <%=widgetFactory.createAmountField("panel_range"+i+"_MaxAmt","",panelRangeMaxAmount, "", isReadOnly, false, false,"class='char10'", "", "none")%>
	            		</div>
	            	</span>
	            	<span class="searchActions">
		      			<%
						  if (!isReadOnly) {
						%>
					      <button data-dojo-type="dijit.form.Button" type="button" id="AddModifyButtonRange<%=i%>">
					        <%=resMgr.getText("PanelAuthorizationGroupDetail.addormodify",TradePortalConstants.TEXT_BUNDLE)%>
					        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								var titleforpopup='<%=resMgr.getText("PanelAuthorizationGroupDetail.PmntAmtRange", TradePortalConstants.TEXT_BUNDLE)%> '+document.getElementById('panel_range<%=i%>_MinAmt').value+' <%=resMgr.getText("PanelAuthorizationGroupDetail.to", TradePortalConstants.TEXT_BUNDLE)%>'+' '+document.getElementById('panel_range<%=i%>_MaxAmt').value;
								panelauthgroup.openDialog(titleforpopup,"datagridrange<%=i%>","hiddenvarsrange<%=i%>",<%=i%>);
							</script>
					      </button>
						<%
						  }
						%>
	            	</span>
            	</div>
            </div>
            <div style="clear: both;"></div>
            <div id="datagridrange<%=i%>"></div>
            <div style="margin-top: 20px;"></div>
            <%} %>

  </div><%-- PaymentAmtRanges Tab Ends --%>

      </div><%--formContent ends--%>
</div><%--formArea ends--%>
<%-- Form sidebar starts --%>
<%//jgadela rel8.3 CR501 [START] %>
<%
String cancelAction = "goToRefDataHome";
 if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction")) || "ApproveButton".equalsIgnoreCase(buttonPressed) || "RejectButton".equalsIgnoreCase(buttonPressed)){
	 cancelAction = "goToRefDataApproval";
 }
%>
<%//jgadela rel8.3 CR501 [END] %>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PanelAuthGroupForm'">
      <jsp:include page="/common/RefDataSidebar.jsp">
            <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
            <jsp:param name="showSaveCloseButton" value="<%= showSaveClose %>" />
            <jsp:param name="showSaveAsButton" value="<%=showSaveAs%>" />
            <jsp:param name="showDeleteButton" value="<%= showDeleteButton %>" />
            <jsp:param name="saveOnClick" value="none" />
         	<jsp:param name="saveCloseOnClick" value="none" />
            <jsp:param name="cancelAction" value="<%= cancelAction%>" />
            <jsp:param name="saveAsDialogPageName" value="PanelAuthorizationGroupSaveAs.jsp" />
      		<jsp:param name="saveAsDialogId" value="saveAsDialogId" />

            <jsp:param name="showHelpButton" value="true" />
            <jsp:param name="showLinks" value="true" />
            <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
            <jsp:param name="error" value="<%= error%>" />
            <jsp:param name="formName" value="0" />
            <jsp:param name="showApproveButton" value="<%=showApprove%>" />
            <jsp:param name="showRejectButton" value="<%=showReject%>" />
            
      </jsp:include>
</div>
<%-- Form sidebar Ends --%>
</form>
</div> <%-- pageMain --%>
</div> <%-- pageContent --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>


<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<div id="saveAsDialogId" style="display: none;"></div>
<div id="editablegriddialog"></div>
<div id="editabledailog" data-dojo-type="dijit.Dialog" style="display: none;">
	<div class="dialogContent fullwidth">
		<div class="gridSearch">
        	<div class="searchDetail">
         	<span class="searchCriteria">
         		<div class="searchItem">
         		    <%=widgetFactory.createNote("PanelAuthorizationGroupDetail.addormodifydialogtext","formItem")%>
                  </div>
         	</span>
         	<span class="searchActions">
    			<%
		  if (!isReadOnly) {
		%>
	      <button data-dojo-type="dijit.form.Button" type="button" id="DeleteAll" data-dojo-props="iconClass:'delete'">
	        <%=resMgr.getText("PanelAuthorizationGroupDetail.deleteall",TradePortalConstants.TEXT_BUNDLE)%>
	        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">panelauthgroup.deleteallClick();</script>
	      </button>
		<%
		  }
		%>
         	</span>
        	</div>
        </div>
        <div style="clear: both;"></div>
		<div id="editabledatagrid"></div>
		<div style="clear: both;"></div>
		<%
		  if (!isReadOnly) {
		%>
	      <button data-dojo-type="dijit.form.Button" type="button" id="SaveAndClose">
	        <%=resMgr.getText("PanelAuthorizationGroupDetail.saveandclose",TradePortalConstants.TEXT_BUNDLE)%>
	        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">panelauthgroup.saveandcloseClick();</script>
	      </button>
		<%
		  }
		%>
		<%
		  if (!isReadOnly) {
		%>
	      <button data-dojo-type="dijit.form.Button" type="button" id="Cancel">
	        <%=resMgr.getText("PanelAuthorizationGroupDetail.cancel",TradePortalConstants.TEXT_BUNDLE)%>
	        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">panelauthgroup.cancelClick();</script>
	      </button>
		<%
		  }
		%>
	</div>
</div>
<script type="text/javascript">
var brandingDirectory='<%=StringFunction.xssCharsToHtml(brandingDirectory)%>';
</script>
<script type="text/javascript" src="/portal/js/page/panelauthgroup.js?<%= version %>"></script>
<%-- CR-821 Prateep Gedupudi Java Script Start --%>
<script type="text/javascript">

require(["dijit/registry",
         "dojo/data/ObjectStore",
         "dojo/store/Memory",
         "dojox/grid/DataGrid",
         "dojo/dom",
         "dojo/ready",
         "dijit/form/FilteringSelect",
         "dojox/grid/cells/dijit",
         "dojo/domReady!"],
function(registry,ObjectStore,Memory,DataGrid,dom,ready,select) {
	ready(function(){
		<%-- T36000031541 --%>
		var panelrange0=[<%=panelrange0.toString()%>];
		var panelrange1=[<%=panelrange1.toString()%>];
		var panelrange2=[<%=panelrange2.toString()%>];
		var panelrange3=[<%=panelrange3.toString()%>];
		var panelrange4=[<%=panelrange4.toString()%>];
		var panelrange5=[<%=panelrange5.toString()%>];
		var panelauthrulelayout=[
								 {noscroll:true,
									 cells:[{name:"oid", field:"oid",hidden:true} ,

								 new dojox.grid.cells.RowIndex({ name:"",width: "10px" }),

								 {name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.sequence",TradePortalConstants.TEXT_BUNDLE)%>",
									 field:"approver_sequence",type:dojox.grid.cells._Widget,widgetClass:select,
									 widgetProps: {store: seqStore},formatter:panelauthgroup.sequenceformatter,width:"110px"},

									 {name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.TotalAuthorisers",TradePortalConstants.TEXT_BUNDLE)%>",
		                        	 fields:["approver_1","approver_2","approver_3","approver_4","approver_5","approver_6","approver_7","approver_8","approver_9","approver_10",
		                        	         "approver_11","approver_12","approver_13","approver_14","approver_15","approver_16","approver_17","approver_18","approver_19","approver_20"],
		                        	 formatter:panelauthgroup.totalauthformatter,width:"80px"} ]},

		                        	 {cells:[

		                        	 {name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
				                        	field:"approver_1",type:dojox.grid.cells._Widget,widgetClass:select,
				                            widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
			                        		 field:"approver_2",type:dojox.grid.cells._Widget,widgetClass:select,
			                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

			                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
				                        		 field:"approver_3",type:dojox.grid.cells._Widget,widgetClass:select,
				                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
			                        		 field:"approver_4",type:dojox.grid.cells._Widget,widgetClass:select,
			                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
			                        		 field:"approver_5",type:dojox.grid.cells._Widget,widgetClass:select,
			                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 		                        	field:"approver_6",type:dojox.grid.cells._Widget,widgetClass:select,
		 		                            widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 	                        		 field:"approver_7",type:dojox.grid.cells._Widget,widgetClass:select,
		 	                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		 	                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 		                        		 field:"approver_8",type:dojox.grid.cells._Widget,widgetClass:select,
		 		                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 	                        		 field:"approver_9",type:dojox.grid.cells._Widget,widgetClass:select,
		 	                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 	                        		 field:"approver_10",type:dojox.grid.cells._Widget,widgetClass:select,
		 	                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
				                        	field:"approver_11",type:dojox.grid.cells._Widget,widgetClass:select,
				                            widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
			                        		 field:"approver_12",type:dojox.grid.cells._Widget,widgetClass:select,
			                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

			                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
				                        		 field:"approver_13",type:dojox.grid.cells._Widget,widgetClass:select,
				                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
			                        		 field:"approver_14",type:dojox.grid.cells._Widget,widgetClass:select,
			                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
			                        		 field:"approver_15",type:dojox.grid.cells._Widget,widgetClass:select,
			                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 		                        	field:"approver_16",type:dojox.grid.cells._Widget,widgetClass:select,
		 		                            widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 	                        		 field:"approver_17",type:dojox.grid.cells._Widget,widgetClass:select,
		 	                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		 	                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 		                        		 field:"approver_18",type:dojox.grid.cells._Widget,widgetClass:select,
		 		                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 	                        		 field:"approver_19",type:dojox.grid.cells._Widget,widgetClass:select,
		 	                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ,

		                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		 	                        		 field:"approver_20",type:dojox.grid.cells._Widget,widgetClass:select,
		 	                         		 widgetProps: {store: panelStore},formatter:panelauthgroup.approvformatter,width:"100px"} ]}
		                          ];

		var panelauthruleeditlayout=[
								 {noscroll:true,width:"302px",cells:[{name:"oid", field:"oid",hidden:true} ,
								 new dojox.grid.cells.RowIndex({ name:"",width: "15px" }),

								 {name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.sequence",TradePortalConstants.TEXT_BUNDLE)%>",
									 field:"approver_sequence",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
									 widgetProps: {store: seqStore, required:false},formatter:panelauthgroup.sequenceformatter,width:"160px"} ,

		                         {name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.TotalAuthorisers",TradePortalConstants.TEXT_BUNDLE)%>",
		                        	 fields:["approver_1","approver_2","approver_3","approver_4","approver_5","approver_6","approver_7","approver_8","approver_9","approver_10",
		                        	         "approver_11","approver_12","approver_13","approver_14","approver_15","approver_16","approver_17","approver_18","approver_19","approver_20"],
		                        	 formatter:panelauthgroup.totalauthformatter,width:"90px"}]} ,
		                         {width:"695px",cells:[
		                        {name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		                        	field:"approver_1",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
		                            widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
	                        		 field:"approver_2",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

	                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		                        		 field:"approver_3",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
		                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
	                        		 field:"approver_4",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
	                        		 field:"approver_5",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 		                        	field:"approver_6",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 		                            widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 	                        		 field:"approver_7",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

 	                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 		                        		 field:"approver_8",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 		                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 	                        		 field:"approver_9",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 	                        		 field:"approver_10",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		                        	field:"approver_11",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
		                            widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
	                        		 field:"approver_12",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

	                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
		                        		 field:"approver_13",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
		                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
	                        		 field:"approver_14",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
	                        		 field:"approver_15",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                         		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 		                        	field:"approver_16",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 		                            widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 	                        		 field:"approver_17",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

 	                         	{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 		                        		 field:"approver_18",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 		                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 	                        		 field:"approver_19",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ,

                          		{name:"<%=resMgr.getText("PanelAuthorizationGroupDetail.approver",TradePortalConstants.TEXT_BUNDLE)%>",
 	                        		 field:"approver_20",editable: true,type:dojox.grid.cells._Widget,widgetClass:select,
 	                         		 widgetProps: {store: panelStore, required:false},formatter:panelauthgroup.approvformatter,width:"100px"} ]},
                         		 {noscroll:true,width:"25px",cells:[

		                         {formatter:panelauthgroup.deleteformatter,width:"25px"}]}
		                          ];

		panelauthgroup.createDataGrid(panelauthrulelayout,panelrange0,"datagridrange0");
		panelauthgroup.createDataGrid(panelauthrulelayout,panelrange1,"datagridrange1");

		setTimeout(function(){
			panelauthgroup.createDataGrid(panelauthrulelayout,panelrange2,"datagridrange2");
			panelauthgroup.createDataGrid(panelauthrulelayout,panelrange3,"datagridrange3");
		},0);

		setTimeout(function(){
			panelauthgroup.createDataGrid(panelauthrulelayout,panelrange4,"datagridrange4");
			panelauthgroup.createDataGrid(panelauthrulelayout,panelrange5,"datagridrange5");
		},0);

		setTimeout(function(){
			panelauthgroup.createhiddenfieldsfromstore(registry.byId("datagridrange0").store.objectStore,"hiddenvarsrange0",0);
			panelauthgroup.createhiddenfieldsfromstore(registry.byId("datagridrange1").store.objectStore,"hiddenvarsrange1",1);
			panelauthgroup.createhiddenfieldsfromstore(registry.byId("datagridrange2").store.objectStore,"hiddenvarsrange2",2);
			panelauthgroup.createhiddenfieldsfromstore(registry.byId("datagridrange3").store.objectStore,"hiddenvarsrange3",3);
			panelauthgroup.createhiddenfieldsfromstore(registry.byId("datagridrange4").store.objectStore,"hiddenvarsrange4",4);
			panelauthgroup.createhiddenfieldsfromstore(registry.byId("datagridrange5").store.objectStore,"hiddenvarsrange5",5);
			panelauthgroup.createEditableDataGrid(panelauthruleeditlayout,"","editabledatagrid");
		},0);
	});
    });

	function submitSaveAs(){
        if(window.document.saveAsForm.newPanelAuthorizationGroupId.value == "" && window.document.saveAsForm.newName.value == ""){
          return false;
        }
          document.PanelAuthGroupForm.PanelAuthorizationGroupId.value=window.document.saveAsForm.newPanelAuthorizationGroupId.value;
          document.PanelAuthGroupForm.Name.value=window.document.saveAsForm.newName.value;
          panelauthgroup.setValuesforMatched("input[name*='_oid']",'<%=EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey())%>');
          panelauthgroup.setEmtyValues('<%=EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey())%>');
          document.PanelAuthGroupForm.owner_org_oid.value="<%=StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid())%>";
          document.PanelAuthGroupForm.panel_auth_group_oid.value="<%=EncryptDecrypt.encryptStringUsingTripleDes( "0", userSession.getSecretKey())%>";

          document.PanelAuthGroupForm.saveAsFlag.value = "Y";
          document.PanelAuthGroupForm.buttonName.value = "SaveAndClose";
          document.PanelAuthGroupForm.submit();
          hideDialog("saveAsDialogId");
 	}
</script>
<%-- CR-821 Prateep Gedupudi Java Script End --%>
</body>
</html>
<%
   beanMgr.unregisterBean("PanelAuthRange");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
<%--  ****** End HTML  ****** --%>