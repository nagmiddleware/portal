<%--
*******************************************************************************
                              Security Profile Detail

  Description:
     This page is used to maintain customer security profiles.  It supports
  insert, update and delete.  Errors found during a save are redisplayed on the
  same page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page
      import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.Vector,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.Hashtable,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.cache.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
      class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
      scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
  boolean isReadOnly = false;


  String securityProfileRights = "";

  String oid = "";

  // These booleans help determine the mode of the JSP.
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;

  boolean showSave = true;
  boolean showDelete = true;
  boolean showSaveAs = false;
  boolean saveClose = true;
  String buttonPressed ="";
  boolean isAdmin = false;
  

  

  // Constants for ref data access.
  String NO_ACCESS = "N";
  String VIEW_ONLY = "V";
  String MAINTAIN  = "M";

  DocumentHandler doc;

  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.SecurityProfileWebBean",
                        "SecurityProfile");

  SecurityProfileWebBean securityProfile =
                   (SecurityProfileWebBean)beanMgr.getBean("SecurityProfile");

  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  isReadOnly = !SecurityAccess.hasRights(loginRights,
                                         SecurityAccess.MAINTAIN_SEC_PROF);

  // if user is not ADMIN, then invoice definition related radios should be readonly
  // IR T36000016192 

  String userSecurityType1 = "";
  if (userSession.getSavedUserSession() == null) {
    userSecurityType1 = userSession.getSecurityType();
  }else {
    userSecurityType1 = userSession.getSavedUserSessionSecurityType();
  }          
  
  if(userSecurityType1.equals(TradePortalConstants.ADMIN)){
	isAdmin = true;	  
   }
 
  // If the user can't even access ref data, we shouldn't be here.
  // Return to ref data home page.
  if (!SecurityAccess.hasRights(loginRights,
                                SecurityAccess.ACCESS_REFDATA_AREA)) {
     // If the user can't access ref data, we shouldn't even be here.
     formMgr.setCurrPage("RefDataHome");
     String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
<jsp:forward page='<%= physicalPage %>' />
<%
     return;
  }


  // Get the document from the cache.  We'll use it to repopulate the screen if
  // the user was entering data with errors.
  doc = formMgr.getFromDocCache();
  buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");

  if (doc.getDocumentNode("/In/SecurityProfile") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item or creating
     // a new one.)

     if(request.getParameter("oid") != null)
      {
         oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"),userSession.getSecretKey());
      }
     else
      {
     oid = null;
      }

     if (oid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       oid = "0";
     } else if (oid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
       showSaveAs = true;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity >=
     // WARNING, we had errors.
     String maxError = doc.getAttribute("/Error/maxerrorseverity");
     
       oid = doc.getAttribute("/In/SecurityProfile/security_profile_oid");
       if(oid == null || "0".equals(oid) || "".equals(oid)){
              oid = doc.getAttribute("/Out/SecurityProfile/security_profile_oid");
       }else if(oid != null && !"0".equals(oid) && !"".equals(oid))
       {
    	   getDataFromDoc = false;
    	   insertMode = false;
           retrieveFromDB = true;
           showSaveAs = true;
           showDelete=true;
       }
     
     if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
       
    	 getDataFromDoc = false;
  	   	 insertMode = false;
         retrieveFromDB = true;
         showSaveAs = true;
         showDelete=true;
       
       
         // We've returned from a save/update/delete that was successful
         // We shouldn't be here.  Forward to the RefDataHome page.
         // Should never get here: jPylon and Navigation.xml should take care
         // of this situation.
      
     } else {
    	 //out.println("error");
            // We've returned from a save/update/delete but have errors.
            // We will display data from the cache.  Determine if we're
            // in insertMode by looking for a '0' oid in the input section
            // of the doc.
            retrieveFromDB = false;
            getDataFromDoc = true;
            String newOid =
                   doc.getAttribute("/In/SecurityProfile/security_profile_oid");
            
            if (newOid.equals("0")) {
               insertMode = true;
               oid = "0";
            } else {
               // Not in insert mode, use oid from output doc.
               insertMode = false;
               oid = newOid;
            }
      }
  }

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Set to insert
     // mode and display error.

     getDataFromDoc = false;

     securityProfile.setAttribute("security_profile_oid", oid);
     securityProfile.getDataFromAppServer();

     if (securityProfile.getAttribute("security_profile_oid").equals("")) {
       insertMode = true;
       oid = "0";
       getDataFromDoc = false;
     } else {
       insertMode = false;
     }
  }

  if (getDataFromDoc) {
     // Populate the securityProfile bean with the output doc.
     try {
        securityProfile.populateFromXmlDoc(doc.getComponent("/In"));
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get SecurityProfile "
             + "attributes for oid: " + oid + " " + e.toString());
     }
  }


  // This value gets used all over the place.  Get it once and then refer to the
  // local variable.
  securityProfileRights = securityProfile.getAttribute("security_rights");

  // Make the page readonly if the security profile does not belong to the user's org
  String ownerOid = securityProfile.getAttribute("owner_org_oid");
  if (!insertMode && !ownerOid.equals(userSession.getOwnerOrgOid()))
            isReadOnly = true;

  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " insertMode: " + insertMode + " oid: "
              + oid);

  //IAZ CR-475 06/21/09 and 06/28/09 Begin
  //if users of this corp (as driven by Client Bank) cannot modify thier own security profiles,
  //make sure the mode isReadOnly if user access his/her own security profiles
  if (InstrumentServices.isNotBlank(userSession.getClientBankOid())) 
  {
      try
      {
            Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
            DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());     
            String canModifyProfile = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_OWN_PROFILE_IND");
            if (TradePortalConstants.INDICATOR_NO.equals(canModifyProfile))
            {
                  try
                  {
                  UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");      
                  thisUser.setAttribute("user_oid", userSession.getUserOid());
                  thisUser.getDataFromAppServer();
                  if (thisUser.getAttribute("security_profile_oid").equals(oid))
                  {
                        isReadOnly = true;
                  }
                  Debug.debug("csp " + thisUser.getAttribute("security_profile_oid") + " " + thisUser.getAttribute("cust_access_sec_profile_oid") + " " + oid);
                  String casprofile = thisUser.getAttribute("cust_access_sec_profile_oid");
                  if (InstrumentServices.isNotBlank(casprofile) && thisUser.getAttribute("cust_access_sec_profile_oid").equals(oid))
                  {
                        isReadOnly = true;
                  }           
            
            }
            catch (Exception any)
            {
                   // we can't detarmine if user is trying to chnage his own security profile - we must not allow
                   // user to do any updates
                   System.out.println("Exception verifying if user can edit security profile. Will set readOnly mode.");
                   isReadOnly = true;       
            }
        }
      }
      catch (Exception any_e)
      {
            System.out.println("General exception " + any_e.toString() + "verifying if user can edit security profile.  Will use default mode");  
      }
  }
  //IAZ CR-475 06/21/09 and 06/28/09 End
    
  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly){ 
      showSave = false;   
      showSaveAs = false; 
      saveClose = false;
      }
  






%>

<%-- ********************* HTML for page begins here *********************  --%>

<%
  // Auto save the form when time-out if not readonly.
  // (Header.jsp will check for auto-save setting of the corporate org).
  String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

%>

<%-- Body tag included as part of common header --%>

<jsp:include page="/common/Header.jsp">
      <jsp:param name="includeNavigationBarFlag"
            value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>

<script language="JavaScript">
  if (document.images) {
    <%-- define rollover button images --%>
    SelectButton = new Image();
    SelectButton.src = "<%=resMgr.getText("common.SelectImg",
                                        TradePortalConstants.TEXT_BUNDLE)%>";
    DeselectButton = new Image();
    DeselectButton.src = "<%=resMgr.getText("common.DeselectImg",
                                        TradePortalConstants.TEXT_BUNDLE)%>";

    SelectButtonRO = new Image();
    SelectButtonRO.src = "<%=resMgr.getText("common.SelectImgRO",
                                        TradePortalConstants.TEXT_BUNDLE)%>";
    DeselectButtonRO = new Image();
    DeselectButtonRO.src = "<%=resMgr.getText("common.DeselectImgRO",
                                        TradePortalConstants.TEXT_BUNDLE)%>";
  }

  function selectInstrumentRights(selectAll)
  {
    <%--
    // Select or deselect all the instrument checkboxes based on the
    // passed in parameter.
    --%>

    CheckTheObj(document.SecurityProfileDetail.DLCCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DLCDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DLCRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DLCAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DLCProcessPO,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SLCCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SLCDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SLCRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SLCAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpDLCCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpDLCDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpDLCRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpDLCAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpCollCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpCollDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpCollRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ExpCollAuthorise,selectAll);
    <%-- Vasavi CR 524 03/31/2010 Begin --%>
    CheckTheObj(document.SecurityProfileDetail.NewExpCollCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.NewExpCollDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.NewExpCollRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.NewExpCollAuthorise,selectAll);
    <%-- Vasavi CR 524 03/31/2010 End --%>
    CheckTheObj(document.SecurityProfileDetail.GUARCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.GUARDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.GUARRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.GUARAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.AWRCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.AWRDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.AWRRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.AWRAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SGCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SGDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SGRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SGAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DRCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DRDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DRRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DRAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.FTCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.FTDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.FTRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.FTAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.LRCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.LRDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.LRRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.LRAuthorise,selectAll);
    <%-- Krishna CR 375-D ATP 07/19/2007 Begin --%>
    CheckTheObj(document.SecurityProfileDetail.ATPCreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ATPDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ATPRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ATPAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ATPProxyAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ATPProcessPO,selectAll);
    <%-- Krishna CR 375-D ATP 07/19/2007 End --%>

<%-- // crhodes CR451 10/15/2008 Begin --%>
      CheckTheObj(document.SecurityProfileDetail.RACreateMod,selectAll);
      CheckTheObj(document.SecurityProfileDetail.RADelete,selectAll);
      CheckTheObj(document.SecurityProfileDetail.RARoute,selectAll);
      CheckTheObj(document.SecurityProfileDetail.RAAuthorise,selectAll);
      CheckTheObj(document.SecurityProfileDetail.RAProxyAuthorise,selectAll);
      CheckTheObj(document.SecurityProfileDetail.TFCreateMod,selectAll);
      CheckTheObj(document.SecurityProfileDetail.TFDelete,selectAll);
      CheckTheObj(document.SecurityProfileDetail.TFRoute,selectAll);
      CheckTheObj(document.SecurityProfileDetail.TFAuthorise,selectAll);
      CheckTheObj(document.SecurityProfileDetail.TFProxyAuthorise,selectAll);
      CheckTheObj(document.SecurityProfileDetail.DPCreateMod,selectAll);
      CheckTheObj(document.SecurityProfileDetail.DPDelete,selectAll);
      CheckTheObj(document.SecurityProfileDetail.DPRoute,selectAll);
      CheckTheObj(document.SecurityProfileDetail.DPAuthorise,selectAll);
      CheckTheObj(document.SecurityProfileDetail.DRProxyAuthorise,selectAll);
      CheckTheObj(document.SecurityProfileDetail.DPUploadBulkFile,selectAll);
<%-- // crhodes CR451 10/15/2008 End --%>

<%-- CR 821 - Rel 8.3 START --%>
	  CheckTheObj(document.SecurityProfileDetail.ATPChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.ATPRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.DPChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.DPRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.DLCChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.DLCRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.SLCChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.SLCRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.ExpDLCChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.ExpDLCRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.ExpCollChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.ExpCollRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.GUARChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.GUARRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.NewExpCollChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.NewExpCollRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.SGChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.SGRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.AWRChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.AWRRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.LRChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.LRRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.TFChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.TFRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.RAChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.RARepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.FTChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.FTRepair,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.DRChecker,selectAll);
	  CheckTheObj(document.SecurityProfileDetail.DRRepair,selectAll);
<%-- CR 821 - Rel 8.3 END --%>

	<%-- SSikhakolli - Rel-9.4 CR-818 --%>
	CheckTheObj(document.SecurityProfileDetail.SICreateMod,selectAll);
	CheckTheObj(document.SecurityProfileDetail.SIDelete,selectAll);
	CheckTheObj(document.SecurityProfileDetail.SIRoute,selectAll);
	CheckTheObj(document.SecurityProfileDetail.SIAuthorise,selectAll);
	CheckTheObj(document.SecurityProfileDetail.SIProxyAuthorise,selectAll);
	CheckTheObj(document.SecurityProfileDetail.SIChecker,selectAll);
	CheckTheObj(document.SecurityProfileDetail.SIRepair,selectAll);

    <%-- Now reset the instrument access buttons --%>
    pickInstrumentAccess();
  }

  <%--CR 821 - Rel 8.3 --%>
  function selectRepairAccess(){
	  if(document.SecurityProfileDetail.ATPChecker.checked == true){
	  	CheckTheObj(document.SecurityProfileDetail.ATPRepair,true);
	  }
	  if(document.SecurityProfileDetail.DPChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.DPRepair,true);
	  }
	  if(document.SecurityProfileDetail.DLCChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.DLCRepair,true);
	  }
	  if(document.SecurityProfileDetail.SLCChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.SLCRepair,true);
	  }
	  if(document.SecurityProfileDetail.ExpDLCChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.ExpDLCRepair,true);
	  }
	  if(document.SecurityProfileDetail.ExpCollChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.ExpCollRepair,true);
	  }
	  if(document.SecurityProfileDetail.GUARChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.GUARRepair,true);
	  }
	  if(document.SecurityProfileDetail.NewExpCollChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.NewExpCollRepair,true);
	  }
	  if(document.SecurityProfileDetail.SGChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.SGRepair,true);
	  }
	  if(document.SecurityProfileDetail.AWRChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.AWRRepair,true);
	  }
	  if(document.SecurityProfileDetail.LRChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.LRRepair,true);
	  }
	  if(document.SecurityProfileDetail.TFChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.TFRepair,true);
	  }
	  if(document.SecurityProfileDetail.RAChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.RARepair,true);
	  }
	  if(document.SecurityProfileDetail.FTChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.FTRepair,true);
	  }
	  if(document.SecurityProfileDetail.DRChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.DRRepair,true);
	  }
	  <%-- SSikhakolli - Rel-9.4 CR-818 --%>
	  if(document.SecurityProfileDetail.SIChecker.checked == true){
		CheckTheObj(document.SecurityProfileDetail.SIRepair,true);
	  }
  }
  
  function setCheckerAccess(){
	  if(document.SecurityProfileDetail.ATPRepair.checked == false){
	  	CheckTheObj(document.SecurityProfileDetail.ATPChecker,false);
	  }
	  if(document.SecurityProfileDetail.DPRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.DPChecker,false);
	  }
	  if(document.SecurityProfileDetail.DLCRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.DLCChecker,false);
	  }
	  if(document.SecurityProfileDetail.SLCRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.SLCChecker,false);
	  }
	  if(document.SecurityProfileDetail.ExpDLCRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.ExpDLCChecker,false);
	  }
	  if(document.SecurityProfileDetail.ExpCollRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.ExpCollChecker,false);
	  }
	  if(document.SecurityProfileDetail.GUARRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.GUARChecker,false);
	  }
	  if(document.SecurityProfileDetail.NewExpCollRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.NewExpCollChecker,false);
	  }
	  if(document.SecurityProfileDetail.SGRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.SGChecker,false);
	  }
	  if(document.SecurityProfileDetail.AWRRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.AWRChecker,false);
	  }
	  if(document.SecurityProfileDetail.LRRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.LRChecker,false);
	  }
	  if(document.SecurityProfileDetail.TFRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.TFChecker,false);
	  }
	  if(document.SecurityProfileDetail.RARepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.RAChecker,false);
	  }
	  if(document.SecurityProfileDetail.FTRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.FTChecker,false);
	  }
	  if(document.SecurityProfileDetail.DRRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.DRChecker,false);
	  }
	  <%-- SSikhakolli - Rel-9.4 CR-818 --%>
	  if(document.SecurityProfileDetail.SIRepair.checked == false){
		CheckTheObj(document.SecurityProfileDetail.SIChecker,false);
	  }
  }
      function deselectInvoiceMgmtRights(){    
        if(document.SecurityProfileDetail.InvoiceMgtView.checked = true){
              CheckTheObj(document.SecurityProfileDetail.SelectAllInv[1],true);
                selectAllInvoiceMgmtRights(false); 
              
        }
        }
        
        function selectAllInvoiceMgmtRights(selectAll)
        {
          <%--
          // Select or deselect all the invoice management checkboxes based on the
          // passed in parameter.
          --%>

          <%-- document.SecurityProfileDetail.ViewUploadedInvoice.checked = selectAll; --%>
          <%-- document.SecurityProfileDetail.UploadInvoice.checked = selectAll; --%>
          <%-- document.SecurityProfileDetail.RemoveUploadedFiles.checked = selectAll; --%>
          
          CheckTheObj(document.SecurityProfileDetail.ViewUploadedInvoice,selectAll);
          CheckTheObj(document.SecurityProfileDetail.UploadInvoice,selectAll);
          CheckTheObj(document.SecurityProfileDetail.RemoveUploadedFiles,selectAll);
            
        }
        
        function pickInvoiceMgmtViewAccess() {
          <%--
          // Based on whether any of the Direct Debit access rights are checked,
          // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
          // for instruments.
          --%>
          
          <%-- document.SecurityProfileDetail.ViewUploadedInvoice.checked=true; --%>
          <%-- document.SecurityProfileDetail.UploadInvoice.checked=true; --%>
                var result=true;

          document.SecurityProfileDetail.InvoiceMgtView[1].checked = result;
          document.SecurityProfileDetail.InvoiceMgtView[0].checked = !result;    
        }

        function pickInvoiceMgmtAccess() {
          <%--
          // Based on whether any of the Direct Debit access rights are checked,
          // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
          // for instruments.
          --%>
         
          result = document.SecurityProfileDetail.ViewUploadedInvoice.checked
                || document.SecurityProfileDetail.UploadInvoice.checked
                || document.SecurityProfileDetail.RemoveUploadedFiles.checked; 
                   
                
             var check =  document.SecurityProfileDetail.InvoiceMgtView;
                  
             var checkObj1 = dijit.getEnclosingWidget(check);
             check.checked = !result;
             checkObj1.set('checked',!result); 
             CheckTheObj(document.SecurityProfileDetail.SelectAllInv[1],!result);
          
          <%-- document.SecurityProfileDetail.InvoiceMgtView[1].checked = result; --%>
          <%-- document.SecurityProfileDetail.InvoiceMgtView[0].checked = !result;     --%>
        }
        
        function selectAllUploadInvRights() {
               
                 radio0 = document.SecurityProfileDetail.SelectAllInv[0];
                   radio1 = document.SecurityProfileDetail.SelectAllInv[1];
                    
                    var checkedFlag;
                    
                    if (radio0.checked == true ){
                          checkedFlag = true;
                          var check =  document.SecurityProfileDetail.InvoiceMgtView;

                        var checkObj1 = dijit.getEnclosingWidget(check);
                          check.checked = false;
                          checkObj1.set('checked',false);
                    
                    }else if (radio1.checked == true){
                          checkedFlag = false;
                    }
                    
                  CheckTheObj(document.SecurityProfileDetail.ViewUploadedInvoice,checkedFlag);
                  CheckTheObj(document.SecurityProfileDetail.UploadInvoice,checkedFlag);
                  CheckTheObj(document.SecurityProfileDetail.RemoveUploadedFiles,checkedFlag); 
              }
  
   <%-- VSHAH 12/14/09 CR#509 Begin --%>
  
  <%--  function deselectDirectDebitRights(){    
      CheckTheObj(document.SecurityProfileDetail.ViewChildOrgDirectDebit,false);
    selectAllDirectDebitRights(false);    
  }  --%>
  
  <%-- function selectAllDirectDebitRights(selectAll)
  {
    
    // Select or deselect all the Direct Debits checkboxes based on the
    // passed in parameter.
   

    CheckTheObj(document.SecurityProfileDetail.DDICreateMod,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DDIDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DDIRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DDIAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DDIUploadFile,selectAll);
        
    Now reset the access buttons
    pickDirectDebitAccess();
  }
  
  function pickDirectDebitAccess() {
    
    // Based on whether any of the Direct Debit access rights are checked,
    // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
    // for instruments.
   
    Vasavi 12/31/09 SSUJ122237332
    result = document.SecurityProfileDetail.ViewChildOrgDirectDebit.checked
          ||document.SecurityProfileDetail.DDICreateMod.checked
          || document.SecurityProfileDetail.DDIDelete.checked
          || document.SecurityProfileDetail.DDIRoute.checked
          || document.SecurityProfileDetail.DDIAuthorise.checked
          || document.SecurityProfileDetail.DDIUploadFile.checked ;
          

    //document.SecurityProfileDetail.AccessDirectDebit[1].checked = result;
   // document.SecurityProfileDetail.AccessDirectDebit[0].checked = !result;  
    
       var check =  document.SecurityProfileDetail.AccessDirectDebit;

     var checkObj1 = dijit.getEnclosingWidget(check);
       check.checked = !result;
       checkObj1.set('checked',!result);
  } --%>
  
 <%-- VSHAH 12/14/09 CR#509 End --%>      

<%-- // crhodes CR434 10/20/2008 Begin --%>

  function deselectReceivablesRights(){
      
	  
	
      if(document.SecurityProfileDetail.AccessReceivables.checked = true){
      
    	  var checkObj1 = dijit.getEnclosingWidget( document.SecurityProfileDetail.ViewChildOrgReceivables);
          document.SecurityProfileDetail.ViewChildOrgReceivables.checked = false;
          checkObj1.set('checked',false);
          selectAllReceivablesRights(false); 
        
          CheckTheObj(document.SecurityProfileDetail.SelectAllReceivables[1],true);
          CheckTheObj(document.SecurityProfileDetail.SelectAllInvoice[1],true);
          CheckTheObj(document.SecurityProfileDetail.SelectAllUploadInvoice[1],true);
          CheckTheObj(document.SecurityProfileDetail.SelectAllATPInvoice[1],true);
          CheckTheObj(document.SecurityProfileDetail.SelectAllSupplierPortal[1],true);
          CheckTheObj(document.SecurityProfileDetail.SelectAllSupplierPortal[1],true);
          CheckTheObj(document.SecurityProfileDetail.SelectAllCrNotes[1],true);
          
          
        }
  }


  function selectAllReceivablesRights(selectAll)
  {
    <%--
    // Select or deselect all the receivables checkboxes based on the
    // passed in parameter.
    --%>
    CheckTheObj(document.SecurityProfileDetail.ReceivablesMatch,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesRoute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesAuthorize,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesProxyAuthorize,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesApproveDiscountAuthorize,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesApproveDiscountProxyAuthorize,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesAddBuyerToPayment,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesDisputeUndispute,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesClose,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesFinance,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ApproveFinancing,selectAll);
    CheckTheObj(document.SecurityProfileDetail.DeleteInvoice,selectAll);
    CheckTheObj(document.SecurityProfileDetail.RemovableInvoice,selectAll);
    CheckTheObj(document.SecurityProfileDetail.AuthorizeInvoice,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ProxyAuthorizeInvoice,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ApplyPaymentDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.CreateATP,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayableDeleteInvoice,selectAll);
    <%-- DK CR-709 Rel8.2 Begins --%>
    CheckTheObj(document.SecurityProfileDetail.ReceivablesApplyPaymentDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesClearPaymentDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesAssignInstrType,selectAll);
    CheckTheObj(document.SecurityProfileDetail.ReceivablesAssignLoanType,selectAll);	
    CheckTheObj(document.SecurityProfileDetail.ReceivablesCreateLoanReq,selectAll);	
    CheckTheObj(document.SecurityProfileDetail.PayClearPaymentDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayableDeleteInvoice,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayAssignInstrType,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayAssignLoanType,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayCreateLoanReq,selectAll);
	<%-- DK CR-709 Rel8.2 Ends --%>
	CheckTheObj(document.SecurityProfileDetail.ReceivablesDeclineInvoice,selectAll); <%-- #RKAZI CR1006 04-24-2015  - Add  Decline Invoice --%>
    CheckTheObj(document.SecurityProfileDetail.SpInvoicesOffered,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpHistory,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpAcceptOffer,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpAuthoriseOffer,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpDeclineOffer,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpAssignFutureValueDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpRemoveFutureValueDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpRemoveInvoice,selectAll);
    CheckTheObj(document.SecurityProfileDetail.SpResetToOffered,selectAll);

    <%--Rel9.0 CR 913 Start--%>
    CheckTheObj(document.SecurityProfileDetail.PayAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayAuthoriseOffline,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayApplyEarlyPaymentAmt,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayResetEarlyPaymentAmt,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayApplySendtoSupplierDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayResetSendtoSupplierDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesAuthorise,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesAuthoriseOffline,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesApprovePayment,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesClearPaymentDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesApplyEarlyPaymentAmt,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesResetEarlyPaymentAmt,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesApplySendtoSupplierDate,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesResetSendtoSupplierDate,selectAll);
    <%--Rel9.0 CR 913 End --%>
    
    CheckTheObj(document.SecurityProfileDetail.PayDeclineInvoice,selectAll);<%-- #RKAZI CR1006 04-24-2015  - Add  Decline Invoice --%>
    
    CheckTheObj(document.SecurityProfileDetail.PayCrNoteAuthorize,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayCrNoteAuthorizeOffline,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayCrNoteDelete,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayCrNoteClose,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayCrNoteApplyManual,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayCrNoteUnApplyManual,selectAll);
    CheckTheObj(document.SecurityProfileDetail.PayablesDeclineCreditNote,selectAll);<%-- #RKAZI CR1006 04-24-2015  - Add  Decline CreditNote --%>
    CheckTheObj(document.SecurityProfileDetail.PayablesApproveCreditNote,selectAll);<%-- #PGedupudi CR1006 05-14-2015  - Add  Approve CreditNote --%>
    
    
    <%-- Now reset the instrument access buttons --%>
    pickReceivablesAccess();
  }

  function pickReceivablesAccess() {
    <%--
    // Based on whether any of the receivables access rights are checked,
    // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
    // for instruments.
    --%>
    result = document.SecurityProfileDetail.ViewChildOrgReceivables.checked
          || document.SecurityProfileDetail.ReceivablesMatch.checked
          || document.SecurityProfileDetail.ReceivablesRoute.checked
          || document.SecurityProfileDetail.ReceivablesAuthorize.checked
          || document.SecurityProfileDetail.ReceivablesProxyAuthorize.checked
          || document.SecurityProfileDetail.ReceivablesApproveDiscountAuthorize.checked
          || document.SecurityProfileDetail.ReceivablesApproveDiscountProxyAuthorize.checked
          || document.SecurityProfileDetail.ReceivablesAddBuyerToPayment.checked
          || document.SecurityProfileDetail.ReceivablesDisputeUndispute.checked
          || document.SecurityProfileDetail.ReceivablesClose.checked
          || document.SecurityProfileDetail.ReceivablesFinance.checked
          || document.SecurityProfileDetail.ApproveFinancing.checked
          || document.SecurityProfileDetail.DeleteInvoice.checked
          || document.SecurityProfileDetail.RemovableInvoice.checked
          || document.SecurityProfileDetail.AuthorizeInvoice.checked
          || document.SecurityProfileDetail.ProxyAuthorizeInvoice.checked
          || document.SecurityProfileDetail.ApplyPaymentDate.checked
          || document.SecurityProfileDetail.CreateATP.checked
          || document.SecurityProfileDetail.SpInvoicesOffered.checked
    	  || document.SecurityProfileDetail.SpHistory.checked
    	  || document.SecurityProfileDetail.SpAcceptOffer.checked
    	  || document.SecurityProfileDetail.SpAuthoriseOffer.checked
    	  || document.SecurityProfileDetail.SpDeclineOffer.checked
    	  || document.SecurityProfileDetail.SpAssignFutureValueDate.checked
    	  || document.SecurityProfileDetail.SpRemoveFutureValueDate.checked
    	  || document.SecurityProfileDetail.SpRemoveInvoice.checked
    	  || document.SecurityProfileDetail.SpResetToOffered.checked
          || document.SecurityProfileDetail.ReceivablesApplyPaymentDate.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.ReceivablesClearPaymentDate.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.ReceivablesAssignInstrType.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.ReceivablesAssignLoanType.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.ReceivablesCreateLoanReq.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.PayClearPaymentDate.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.PayableDeleteInvoice.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.PayAssignInstrType.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.PayAssignLoanType.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.PayCreateLoanReq.checked <%--  DK CR709 Rel8.2 Added --%>
          || document.SecurityProfileDetail.ReceivablesDeclineInvoice.checked <%-- #RKAZI CR1006 04-24-2015  - Add  Decline Invoice --%>
          <%--Rel 9.0 CR 913 Starts --%>
          || document.SecurityProfileDetail.PayablesAuthorise.checked 
          || document.SecurityProfileDetail.PayablesAuthoriseOffline.checked 
          || document.SecurityProfileDetail.PayablesApprovePayment.checked 
          || document.SecurityProfileDetail.PayablesClearPaymentDate.checked
          || document.SecurityProfileDetail.PayablesApplyEarlyPaymentAmt.checked
          || document.SecurityProfileDetail.PayablesResetEarlyPaymentAmt.checked
          || document.SecurityProfileDetail.PayablesApplySendtoSupplierDate.checked 
          || document.SecurityProfileDetail.PayablesResetSendtoSupplierDate.checked 
          || document.SecurityProfileDetail.PayAuthorise.checked 
          || document.SecurityProfileDetail.PayAuthoriseOffline.checked 
          || document.SecurityProfileDetail.PayApplyEarlyPaymentAmt.checked
          || document.SecurityProfileDetail.PayResetEarlyPaymentAmt.checked
          || document.SecurityProfileDetail.PayApplySendtoSupplierDate.checked
          || document.SecurityProfileDetail.PayResetSendtoSupplierDate.checked
          
          
          <%--Rel 9.0 CR 913 Ends --%>
          || document.SecurityProfileDetail.PayDeclineInvoice.checked <%-- #RKAZI CR1006 04-24-2015  - Add  Decline Invoice  --%>
          || document.SecurityProfileDetail.PayCrNoteAuthorize.checked
          || document.SecurityProfileDetail.PayCrNoteAuthorizeOffline.checked
          || document.SecurityProfileDetail.PayCrNoteDelete.checked
          || document.SecurityProfileDetail.PayCrNoteClose.checked
          || document.SecurityProfileDetail.PayCrNoteApplyManual.checked
          || document.SecurityProfileDetail.PayCrNoteUnApplyManual.checked
          || document.SecurityProfileDetail.PayablesDeclineCreditNote.checked <%-- #RKAZI CR1006 04-24-2015  - Add  Decline Credit Note --%>
          || document.SecurityProfileDetail.PayablesApproveCreditNote.checked; <%-- #PGedupudi CR1006 05-14-2015  - Add  Approve CreditNote --%>
          
          
   <%--  document.SecurityProfileDetail.AccessReceivables[1].checked = result; --%>
    <%-- document.SecurityProfileDetail.AccessReceivables[0].checked = !result;     --%>
    
       var check =  document.SecurityProfileDetail.AccessReceivables;

    	 var checkObj1 = dijit.getEnclosingWidget(check);
       check.checked = !result;
       checkObj1.set('checked',!result);
       
       CheckTheObj(document.SecurityProfileDetail.SelectAllReceivables[1],!result);
       CheckTheObj(document.SecurityProfileDetail.SelectAllInvoice[1],!result);
       CheckTheObj(document.SecurityProfileDetail.SelectAllUploadInvoice[1],!result);
       CheckTheObj(document.SecurityProfileDetail.SelectAllATPInvoice[1],!result);
       CheckTheObj(document.SecurityProfileDetail.SelectAllPayInvoice[1],!result);<%--Rel9.0 CR 913 --%>
       
       CheckTheObj(document.SecurityProfileDetail.SelectAllSupplierPortal[1],!result);
       result = document.SecurityProfileDetail.SpAuthoriseOffer.checked
    	  || document.SecurityProfileDetail.SpDeclineOffer.checked
    	  || document.SecurityProfileDetail.SpAssignFutureValueDate.checked
    	  || document.SecurityProfileDetail.SpRemoveFutureValueDate.checked
    	  || document.SecurityProfileDetail.SpResetToOffered.checked;
      if (result ) {
          check =  document.SecurityProfileDetail.SpInvoicesOffered;
          checkObj1 = dijit.getEnclosingWidget(check);
          check.checked = result;
          checkObj1.set('checked',result);
      }

  }
  
  
<%-- // crhodes CR434 10/20/2008 End --%>
<%-- // crhodes CR434 10/20/2008 End --%>
<%-- Srini_D 11/10/11 CR#710 Begin  --%>
 function pickReceivablesUploadAccess() {
<%--
// Based on whether any of the receivables access rights are checked,
// check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
// for instruments.
--%>
             document.SecurityProfileDetail.AccessReceivables[1].checked =true;

  }
<%-- Srini_D 11/10/11 CR#710 End  --%>

  function pickRefDataAccess() {
    <%--
    // Based on whether any of the ref data access rights are checked,
    // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
    // for ref data.
    --%>
<%-- [START] IR VIUK090944011 09092010 --%>
      if(!document.SecurityProfileDetail.MaintainTemplate[2].checked && document.SecurityProfileDetail.CreateFixedPaymentTemplate.checked){
      <%-- document.SecurityProfileDetail.CreateFixedPaymentTemplate.checked = false; --%>
            var spCreateFixedPayTem =  document.SecurityProfileDetail.CreateFixedPaymentTemplate;
            var spCreateFixedPayTemObj = dijit.getEnclosingWidget(spCreateFixedPayTem);
            <%-- check.checked = !result; --%>
            spCreateFixedPayTemObj.set('checked',false);
      }

      if(!document.SecurityProfileDetail.MaintainParty[2].checked && document.SecurityProfileDetail.CreateNewTransactionParty.checked){
            <%-- document.SecurityProfileDetail.CreateFixedPaymentTemplate.checked = false; --%>
                  var spCreatePartyem =  document.SecurityProfileDetail.CreateNewTransactionParty;
                  var spCreateFixedPartyTemObj = dijit.getEnclosingWidget(spCreatePartyem);
                  spCreateFixedPartyTemObj.set('checked',false);
      }

<%-- [END] IR VIUK090944011 09092010 --%>
    result = document.SecurityProfileDetail.MaintainFXRate[1].checked
          || document.SecurityProfileDetail.MaintainFXRate[2].checked
          || document.SecurityProfileDetail.MaintainTemplate[1].checked
          || document.SecurityProfileDetail.MaintainTemplate[2].checked
          || document.SecurityProfileDetail.MaintainNotificationRule[1].checked<%--Rel9.5 CR 927B --%>
          || document.SecurityProfileDetail.MaintainNotificationRule[2].checked<%--Rel9.5 CR 927B --%>
          || document.SecurityProfileDetail.MaintainParty[1].checked
          || document.SecurityProfileDetail.MaintainParty[2].checked
          || document.SecurityProfileDetail.MaintainPhrase[1].checked
          || document.SecurityProfileDetail.MaintainPhrase[2].checked
          || document.SecurityProfileDetail.MaintainSecProf[1].checked
          || document.SecurityProfileDetail.MaintainSecProf[2].checked
          || document.SecurityProfileDetail.MaintainThreshGrp[1].checked
          || document.SecurityProfileDetail.MaintainThreshGrp[2].checked
          || document.SecurityProfileDetail.MaintainUser[1].checked
          || document.SecurityProfileDetail.MaintainUser[2].checked
          || document.SecurityProfileDetail.MaintainLCCreateRule[1].checked
          || document.SecurityProfileDetail.MaintainLCCreateRule[2].checked
          || document.SecurityProfileDetail.MaintainPOUploadDefn[1].checked
          || document.SecurityProfileDetail.MaintainPOUploadDefn[2].checked
          || document.SecurityProfileDetail.MaintainWorkGroups[1].checked
          || document.SecurityProfileDetail.MaintainWorkGroups[2].checked
          || document.SecurityProfileDetail.MaintainPanelAuthGroup[1].checked <%-- // iaz CR-511 11/09/2009 --%>
          || document.SecurityProfileDetail.MaintainPanelAuthGroup[2].checked <%-- // iaz CR-511 11/09/2009 --%>
    || document.SecurityProfileDetail.CreateNewTransactionParty.checked <%-- //rbhaduri 18th Jan 06 CR239 --%>
    || document.SecurityProfileDetail.MaintainReceivablesMatchRule[1].checked <%-- // crhodes CR434 10/20/2008 --%>
    || document.SecurityProfileDetail.MaintainReceivablesMatchRule[2].checked <%-- // crhodes CR434 10/20/2008 --%>
    || document.SecurityProfileDetail.MaintainPaymentTemplateGroup[1].checked <%-- // CR-586 --%>
    || document.SecurityProfileDetail.MaintainPaymentTemplateGroup[2].checked <%-- // CR-586 --%>
    || document.SecurityProfileDetail.CreateFixedPaymentTemplate.checked <%-- // CR-586 --%>
    || document.SecurityProfileDetail.MaintainInvoiceDefinitions[1].checked
    || document.SecurityProfileDetail.MaintainInvoiceDefinitions[2].checked
    || document.SecurityProfileDetail.MaintainErpGlCodes[1].checked <%-- //AAlubala Rel8.2 CR741 ERP GL Codes --%>
    || document.SecurityProfileDetail.MaintainErpGlCodes[2].checked  <%-- //AAlubala Rel8.2 CR741 ERP GL Codes --%>
    || document.SecurityProfileDetail.MaintainDiscountCodes[1].checked <%-- //AAlubala Rel8.2 CR741 Discount Codes --%>
    || document.SecurityProfileDetail.MaintainDiscountCodes[2].checked <%-- //AAlubala Rel8.2 CR741 Discount Codes --%>    
    || document.SecurityProfileDetail.MaintainReceivablesMatchRule[1].checked
    || document.SecurityProfileDetail.MaintainReceivablesMatchRule[2].checked
    || document.SecurityProfileDetail.MaintainOrgProfile[1].checked <%-- //MEerupula  04/18/2013 Rel8.3 CR776 ERP  --%>
    || document.SecurityProfileDetail.MaintainOrgProfile[2].checked
	|| document.SecurityProfileDetail.MaintainPaymentFileDef[1].checked <%-- // Srinivasu_D CR-599 Rel8.4 11/23/2013 --%>
    || document.SecurityProfileDetail.MaintainPaymentFileDef[2].checked;
    
    <%-- document.SecurityProfileDetail.AccessRefData[1].checked = result; --%>
   <%--  document.SecurityProfileDetail.AccessRefData[0].checked = !result; --%>
    
    var check =  document.SecurityProfileDetail.AccessRefData;
       var checkObj1 = dijit.getEnclosingWidget(check);
       check.checked = !result;
       checkObj1.set('checked',!result);
  }
  
<%-- Srinivasu_D CR#269 Rel8.4 09/02/2013 - start --%>

 function pickConvCenterAccess() {
	  <%--
	  // Based on whether any of the Direct Debit access rights are checked,
	  // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
	  // for instruments.
	  --%>
		  
	  result = document.SecurityProfileDetail.CCCreateMod.checked
			|| document.SecurityProfileDetail.CCDelete.checked
			|| document.SecurityProfileDetail.CCRoute.checked
		    || document.SecurityProfileDetail.CCAuthorise.checked; 			   
			
		 var check =  document.SecurityProfileDetail.AccessConverCenter;
			  
		 var checkObj1 = dijit.getEnclosingWidget(check);
		 check.checked = !result;
		 checkObj1.set('checked',!result); 
		 CheckTheObj(document.SecurityProfileDetail.SelectAllCC[1],!result);
	  
	}

  function deselectConvCenterRights(){    
        if(document.SecurityProfileDetail.AccessConverCenter.checked == true){
              CheckTheObj(document.SecurityProfileDetail.SelectAllCC[1],true);
                selectAllConvCenterRights(false);               
        }
  }
        
  function selectAllConvCenterRights(selectAll)
  {
          <%--
          // Select or deselect all the invoice management checkboxes based on the
          // passed in parameter.
          --%>     
          
          CheckTheObj(document.SecurityProfileDetail.CCCreateMod,selectAll);
          CheckTheObj(document.SecurityProfileDetail.CCDelete,selectAll);
          CheckTheObj(document.SecurityProfileDetail.CCRoute,selectAll);
          CheckTheObj(document.SecurityProfileDetail.CCAuthorise,selectAll);  
  }  

   function selectAllConversionCenterRights() {
               
		 radio0 = document.SecurityProfileDetail.SelectAllCC[0];
		   radio1 = document.SecurityProfileDetail.SelectAllCC[1];
			
			var checkedFlag;
			
			if (radio0.checked == true ){
				  checkedFlag = true;
				  var check =  document.SecurityProfileDetail.AccessConverCenter;

				var checkObj1 = dijit.getEnclosingWidget(check);
				  check.checked = false;
				  checkObj1.set('checked',false);
			
			}else if (radio1.checked == true){
				  checkedFlag = false;
			}
			
		  CheckTheObj(document.SecurityProfileDetail.CCCreateMod,checkedFlag);
		  CheckTheObj(document.SecurityProfileDetail.CCDelete,checkedFlag);
		  CheckTheObj(document.SecurityProfileDetail.CCRoute,checkedFlag); 
		  CheckTheObj(document.SecurityProfileDetail.CCAuthorise,checkedFlag); 
   }

 <%-- Srinivasu_D CR#269 Rel8.4 09/02/2013 -  --%>

  function deselectRefDataRights() {
          <%-- Select all the "cannot view" options for refdata --%>
          CheckTheObj(document.SecurityProfileDetail.MaintainFXRate[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainTemplate[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainNotificationRule[0],true);<%--Rel9.5 CR 927B --%>
          CheckTheObj(document.SecurityProfileDetail.MaintainParty[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainPhrase[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainSecProf[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainThreshGrp[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainUser[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainLCCreateRule[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainPaymentTemplateGroup[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainPOUploadDefn[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainWorkGroups[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainReceivablesMatchRule[0],true);
			<%-- AAlubala Rel 8.2 12/17/2012 - CR741 ERP and Discount Codes BEGIN--%>
			CheckTheObj(document.SecurityProfileDetail.MaintainErpGlCodes[0],true);
			CheckTheObj(document.SecurityProfileDetail.MaintainDiscountCodes[0],true);
			<%-- CR741 END--%>          
          <%-- // Vshah IR - SUK021854519 03/24/2010 Begin --%>
          CheckTheObj(document.SecurityProfileDetail.MaintainPanelAuthGroup[0],true);
            <%-- // Vshah IR - SUK021854519 03/24/2010  End --%>
            CheckTheObj(document.SecurityProfileDetail.MaintainInvoiceDefinitions[0],true);
            <%-- M Eerupula 04/18/2013 Rel8.3 CR776 --%>
            CheckTheObj(document.SecurityProfileDetail.MaintainOrgProfile[0],true);
			  <%-- Srinivasu_D CR-599 Rel8.4 11/23/2013 --%>
           CheckTheObj(document.SecurityProfileDetail.MaintainPaymentFileDef[0],true);
            
            var spCreatePartyem =  document.SecurityProfileDetail.CreateNewTransactionParty;
            var spCreateFixedPartyTemObj = dijit.getEnclosingWidget(spCreatePartyem);
            spCreateFixedPartyTemObj.set('checked',false);
            
            var spCreateFixedPayTem =  document.SecurityProfileDetail.CreateFixedPaymentTemplate;
            var spCreateFixedPayTemObj = dijit.getEnclosingWidget(spCreateFixedPayTem);
            spCreateFixedPayTemObj.set('checked',false);
        }

  <%-- Suresh CR-603 03/23/2011 Begin --%>
  function pickReportsAccess() {
       result =  document.SecurityProfileDetail.MaintainStandardReport[1].checked
            || document.SecurityProfileDetail.MaintainStandardReport[2].checked
            || document.SecurityProfileDetail.MaintainCustomReport[1].checked
            || document.SecurityProfileDetail.MaintainCustomReport[2].checked;
       
       var check =  document.SecurityProfileDetail.AccessReports;
      <%--  var radio0 =  document.SecurityProfileDetail.AccessReports[0]; --%>
      
       var checkObj1 = dijit.getEnclosingWidget(check);
       check.checked = !result;
       checkObj1.set('checked',!result);

      <%--  document.SecurityProfileDetail.AccessReports[1].checked = result; --%>
       <%-- document.SecurityProfileDetail.AccessReports[0].checked = !result;      --%>
  }
  <%-- Suresh CR-603 03/23/2011 End --%>
  
  function deselectReportRights() {
          <%-- Select all the "cannot view" options for refdata --%>
          CheckTheObj(document.SecurityProfileDetail.MaintainStandardReport[0],true);
          CheckTheObj(document.SecurityProfileDetail.MaintainCustomReport[0],true);
 }
  
  

  function pickMessageAccess() {
    <%--
    // Based on whether any of the message access rights are checked,
    // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
    // for messages.
    --%>

    result = document.SecurityProfileDetail.SendMessage.checked
          || document.SecurityProfileDetail.CreateMessage.checked
          || document.SecurityProfileDetail.DeleteMessage.checked
          || document.SecurityProfileDetail.RouteMessage.checked
          || document.SecurityProfileDetail.RouteDiscrepancyMsg.checked
          || document.SecurityProfileDetail.DeleteDiscrepancyMsg.checked
          || document.SecurityProfileDetail.DeleteNotify.checked
          || document.SecurityProfileDetail.ViewChildOrgMessages.checked
          || document.SecurityProfileDetail.RoutePreDebitFundingNotif.checked <%-- Rel9.0 CR 913 Added --%>
          || document.SecurityProfileDetail.DeletePreDebitFundingNotif.checked;<%-- Rel9.0 CR 913 Added --%>


   <%--  document.SecurityProfileDetail.AccessMessage[1].checked = result; --%>
    <%--  document.SecurityProfileDetail.AccessMessage[0].checked = !result; --%>
    var check =  document.SecurityProfileDetail.AccessMessage;

    var checkObj1 = dijit.getEnclosingWidget(check);
       check.checked = !result;
       checkObj1.set('checked',!result);
       
       if(!check.checked){
            <%--  document.SecurityProfileDetail.AccessMessage.value = 'Y'; --%>
       }
  }
  
  <%-- crhodes CR434 10/20/2008 Begin --%>
  function pickReceivableNoticesAccess() {
    <%--
    // Based on whether any of the message access rights are checked,
    // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
    // for messages.
    --%>
    result = document.SecurityProfileDetail.RecNoticesRoute.checked;
   <%--  document.SecurityProfileDetail.AccessReceivableNotices[1].checked = result; --%>
    <%-- document.SecurityProfileDetail.AccessReceivableNotices[0].checked = !result; --%>
    var check =  document.SecurityProfileDetail.AccessReceivableNotices;

     var checkObj1 = dijit.getEnclosingWidget(check);
       check.checked = result;
       checkObj1.set('checked',!result);
  }
  
  function deselectReceivableNoticesRights(){
        CheckTheObj(document.SecurityProfileDetail.RecNoticesRoute, false);
  }
  
  <%-- crhodes CR434 10/20/2008 Begin --%>

  function deselectMessageRights() {
    <%-- Deselect all the message rights. --%>
    CheckTheObj(document.SecurityProfileDetail.SendMessage, false);
    CheckTheObj(document.SecurityProfileDetail.CreateMessage,false);
    CheckTheObj(document.SecurityProfileDetail.DeleteMessage,false);
    CheckTheObj(document.SecurityProfileDetail.RouteMessage, false);
    CheckTheObj(document.SecurityProfileDetail.RouteDiscrepancyMsg, false);
    CheckTheObj(document.SecurityProfileDetail.DeleteDiscrepancyMsg, false);
    CheckTheObj(document.SecurityProfileDetail.DeleteNotify, false);
    CheckTheObj(document.SecurityProfileDetail.ViewChildOrgMessages, false);
    CheckTheObj(document.SecurityProfileDetail.MessageAttachDocument, false);
    CheckTheObj(document.SecurityProfileDetail.MessageDeleteDocument, false);
    CheckTheObj(document.SecurityProfileDetail.RoutePreDebitFundingNotif, false); <%-- Rel9.0 CR 913 Added --%>
    CheckTheObj(document.SecurityProfileDetail.DeletePreDebitFundingNotif, false);<%-- Rel9.0 CR 913 Added --%>
    
  }

  function pickInstrumentAccess() {
    <%--
    // Based on whether any of the instrument access rights are checked,
    // check and uncheck the HAS ACCESS ([1]) and NO ACCESS ([0]) buttons
    // for instruments.
    --%>

    result = document.SecurityProfileDetail.DLCCreateMod.checked
          || document.SecurityProfileDetail.DLCDelete.checked
          || document.SecurityProfileDetail.DLCRoute.checked
          || document.SecurityProfileDetail.DLCAuthorise.checked
          || document.SecurityProfileDetail.DLCProxyAuthorise.checked
          || document.SecurityProfileDetail.DLCProcessPO.checked
          || document.SecurityProfileDetail.SLCCreateMod.checked
          || document.SecurityProfileDetail.SLCDelete.checked
          || document.SecurityProfileDetail.SLCRoute.checked
          || document.SecurityProfileDetail.SLCAuthorise.checked
          || document.SecurityProfileDetail.SLCProxyAuthorise.checked
          || document.SecurityProfileDetail.ExpDLCCreateMod.checked
          || document.SecurityProfileDetail.ExpDLCDelete.checked
          || document.SecurityProfileDetail.ExpDLCRoute.checked
          || document.SecurityProfileDetail.ExpDLCAuthorise.checked
          || document.SecurityProfileDetail.ExpDLCProxyAuthorise.checked
          || document.SecurityProfileDetail.ExpCollCreateMod.checked
          || document.SecurityProfileDetail.ExpCollDelete.checked
          || document.SecurityProfileDetail.ExpCollRoute.checked
          || document.SecurityProfileDetail.ExpCollAuthorise.checked
          || document.SecurityProfileDetail.ExpCollProxyAuthorise.checked
          || document.SecurityProfileDetail.NewExpCollCreateMod.checked <%-- Vasavi CR 524 03/31/2010 Begin --%>
          || document.SecurityProfileDetail.NewExpCollDelete.checked
          || document.SecurityProfileDetail.NewExpCollRoute.checked
          || document.SecurityProfileDetail.NewExpCollAuthorise.checked <%-- Vasavi CR 524 03/31/2010 End --%>
          || document.SecurityProfileDetail.NewExpCollProxyAuthorise.checked
          || document.SecurityProfileDetail.GUARCreateMod.checked
          || document.SecurityProfileDetail.GUARDelete.checked
          || document.SecurityProfileDetail.GUARRoute.checked
          || document.SecurityProfileDetail.GUARAuthorise.checked
          || document.SecurityProfileDetail.GUARProxyAuthorise.checked
          || document.SecurityProfileDetail.AWRCreateMod.checked
          || document.SecurityProfileDetail.AWRDelete.checked
          || document.SecurityProfileDetail.AWRRoute.checked
          || document.SecurityProfileDetail.AWRAuthorise.checked
          || document.SecurityProfileDetail.AWRProxyAuthorise.checked
          || document.SecurityProfileDetail.SGCreateMod.checked
          || document.SecurityProfileDetail.SGDelete.checked
          || document.SecurityProfileDetail.SGRoute.checked
          || document.SecurityProfileDetail.SGAuthorise.checked
          || document.SecurityProfileDetail.SGProxyAuthorise.checked
          || document.SecurityProfileDetail.FTCreateMod.checked
          || document.SecurityProfileDetail.FTDelete.checked
          || document.SecurityProfileDetail.FTRoute.checked
          || document.SecurityProfileDetail.FTAuthorise.checked
          || document.SecurityProfileDetail.FTProxyAuthorise.checked
          || document.SecurityProfileDetail.LRCreateMod.checked
          || document.SecurityProfileDetail.LRDelete.checked
          || document.SecurityProfileDetail.LRRoute.checked
          || document.SecurityProfileDetail.LRAuthorise.checked
          || document.SecurityProfileDetail.LRProxyAuthorise.checked
          || document.SecurityProfileDetail.ATPCreateMod.checked <%-- Krishna CR 375-D ATP 07/19/2007 Begin --%>
          || document.SecurityProfileDetail.ATPDelete.checked
          || document.SecurityProfileDetail.ATPRoute.checked
          || document.SecurityProfileDetail.ATPAuthorise.checked
          || document.SecurityProfileDetail.ATPProxyAuthorise.checked
          || document.SecurityProfileDetail.ATPProcessPO.checked <%-- Krishna CR 375-D ATP 07/19/2007 End --%>
          || document.SecurityProfileDetail.ProcessATPInvoices.checked
    	  || document.SecurityProfileDetail.DRCreateMod.checked
      	  || document.SecurityProfileDetail.DRCreateMod.checked
          || document.SecurityProfileDetail.DRDelete.checked
          || document.SecurityProfileDetail.DRRoute.checked
          || document.SecurityProfileDetail.DRAuthorise.checked
          || document.SecurityProfileDetail.DRProxyAuthorise.checked
          || document.SecurityProfileDetail.WorkSubsidiary.checked
          || document.SecurityProfileDetail.RACreateMod.checked <%-- crhodes CR451 10/15/2008 Begin --%>
          || document.SecurityProfileDetail.RADelete.checked
          || document.SecurityProfileDetail.RARoute.checked
          || document.SecurityProfileDetail.RAAuthorise.checked
          || document.SecurityProfileDetail.RAProxyAuthorise.checked
          || document.SecurityProfileDetail.TFCreateMod.checked
          || document.SecurityProfileDetail.TFDelete.checked
          || document.SecurityProfileDetail.TFRoute.checked
          || document.SecurityProfileDetail.TFAuthorise.checked
          || document.SecurityProfileDetail.TFProxyAuthorise.checked
          || document.SecurityProfileDetail.DPCreateMod.checked
          || document.SecurityProfileDetail.DPDelete.checked
          || document.SecurityProfileDetail.DPRoute.checked
          || document.SecurityProfileDetail.DPAuthorise.checked
          || document.SecurityProfileDetail.DPProxyAuthorise.checked
          || document.SecurityProfileDetail.DPUploadBulkFile.checked  <%-- crhodes CR451 10/15/2008 End --%>
          <%-- CR 821 - Rel 8.3 START --%>
    	  || document.SecurityProfileDetail.ATPChecker.checked
    	  || document.SecurityProfileDetail.ATPRepair.checked
    	  || document.SecurityProfileDetail.DPChecker.checked
    	  || document.SecurityProfileDetail.DPRepair.checked
    	  || document.SecurityProfileDetail.DLCChecker.checked
    	  || document.SecurityProfileDetail.DLCRepair.checked
    	  || document.SecurityProfileDetail.SLCChecker.checked
    	  || document.SecurityProfileDetail.SLCRepair.checked
    	  || document.SecurityProfileDetail.ExpDLCChecker.checked
    	  || document.SecurityProfileDetail.ExpDLCRepair.checked
    	  || document.SecurityProfileDetail.ExpCollChecker.checked
    	  || document.SecurityProfileDetail.ExpCollRepair.checked
    	  || document.SecurityProfileDetail.GUARChecker.checked
    	  || document.SecurityProfileDetail.GUARRepair.checked
    	  || document.SecurityProfileDetail.NewExpCollChecker.checked
    	  || document.SecurityProfileDetail.NewExpCollRepair.checked
    	  || document.SecurityProfileDetail.DLCChecker.checked
    	  || document.SecurityProfileDetail.DLCRepair.checked
    	  || document.SecurityProfileDetail.SGChecker.checked
    	  || document.SecurityProfileDetail.SGRepair.checked
    	  || document.SecurityProfileDetail.AWRChecker.checked
    	  || document.SecurityProfileDetail.AWRRepair.checked
    	  || document.SecurityProfileDetail.LRChecker.checked
    	  || document.SecurityProfileDetail.LRRepair.checked
    	  || document.SecurityProfileDetail.TFChecker.checked
    	  || document.SecurityProfileDetail.TFRepair.checked
    	  || document.SecurityProfileDetail.RAChecker.checked
    	  || document.SecurityProfileDetail.RARepair.checked
    	  || document.SecurityProfileDetail.FTChecker.checked
    	  || document.SecurityProfileDetail.FTRepair.checked
    	  || document.SecurityProfileDetail.DRChecker.checked
    	  || document.SecurityProfileDetail.DRRepair.checked
    <%-- CR 821 - Rel 8.3 END --%>
    	  <%-- SSikhakolli - Rel-9.4 CR-818 - Begin --%>
    	  || document.SecurityProfileDetail.SICreateMod.checked
          || document.SecurityProfileDetail.SIDelete.checked
          || document.SecurityProfileDetail.SIRoute.checked
          || document.SecurityProfileDetail.SIAuthorise.checked
          || document.SecurityProfileDetail.SIProxyAuthorise.checked
          || document.SecurityProfileDetail.SIChecker.checked
    	  || document.SecurityProfileDetail.SIRepair.checked;
    	  <%-- SSikhakolli - Rel-9.4 CR-818 - End --%>
        
          var check =  document.SecurityProfileDetail.AccessInstrument;
          var checkObj1 = dijit.getEnclosingWidget(check);
          check.checked = !result;
          checkObj1.set('checked',!result);
          CheckTheObj(document.SecurityProfileDetail.SelectAllInst[1],false);
  }
  
  var radio0 ;
  var radio1 ;
  
  function CheckTheObj(obj,checkedFlag) {
	    var checkedObj = dijit.getEnclosingWidget(obj);
        obj.checked = checkedFlag;
        checkedObj.set('checked',checkedFlag);
 }
  
function selectAllInstruments() { 
      
  radio0 = document.SecurityProfileDetail.SelectAllInst[0];
  radio1 = document.SecurityProfileDetail.SelectAllInst[1];
  
  var checkedFlag;  
  if (radio0.checked == true ){
        checkedFlag = true;
      var checkObj1 = dijit.getEnclosingWidget(document.SecurityProfileDetail.AccessInstrument);
      document.SecurityProfileDetail.AccessInstrument.checked = false;
        checkObj1.set('checked',false);      
      }else if (radio1.checked == true){
        checkedFlag = false;
  }
  
  selectAllInstrumentsObj(checkedFlag);
}


function noAccessAllInstruments() { 
        var check = document.SecurityProfileDetail.AccessInstrument;
        
        var checkObj1 = dijit.getEnclosingWidget(document.SecurityProfileDetail.WorkSubsidiary);
        document.SecurityProfileDetail.WorkSubsidiary.checked = false;
        checkObj1.set('checked',false);
        
      var checkObj2 = dijit.getEnclosingWidget(document.SecurityProfileDetail.TransactionAttachDocument);
        document.SecurityProfileDetail.TransactionAttachDocument.checked = false;
        checkObj2.set('checked',false);

        
        var checkObj3 = dijit.getEnclosingWidget(document.SecurityProfileDetail.TransactionDeleteDocument);
        document.SecurityProfileDetail.TransactionDeleteDocument.checked = false;
        checkObj3.set('checked',false);
      
        CheckTheObj(document.SecurityProfileDetail.SelectAllInst[1],true);
       
        if (check.checked == true ){
              checkedFlag = false;
              selectAllInstrumentsObj(checkedFlag);
        }else {
        }
}


function selectAllInstrumentsObj(checkedFlag) { 
        
  CheckTheObj(document.SecurityProfileDetail.AWRCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.AWRDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.AWRRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.AWRAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.AWRProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCProcessPO,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SLCCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SLCDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SLCRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SLCAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SLCProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpDLCCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpDLCDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpDLCRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpDLCAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpDLCProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.NewExpCollCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.NewExpCollDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.NewExpCollRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.NewExpCollAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.NewExpCollProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.GUARCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.GUARDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.GUARRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.GUARAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.GUARProxyAuthorise,checkedFlag);
   
  CheckTheObj(document.SecurityProfileDetail.SGCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SGDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SGRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SGAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SGProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.FTCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.FTDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.FTRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.FTAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.FTProxyAuthorise,checkedFlag);
  
  CheckTheObj(document.SecurityProfileDetail.LRCreateMod,checkedFlag);  
  CheckTheObj(document.SecurityProfileDetail.LRDelete,checkedFlag);  
  CheckTheObj(document.SecurityProfileDetail.LRRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.LRAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.LRProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.LRProcessInvoices,checkedFlag); <%-- DK CR709 Rel8.2 10/31/2012 --%>
  CheckTheObj( document.SecurityProfileDetail.ATPCreateMod,checkedFlag); 
  CheckTheObj(document.SecurityProfileDetail.ATPDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ATPRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ATPAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ATPProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ProcessATPInvoices,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ATPProcessPO,checkedFlag); <%-- Krishna CR 375-D ATP 07/19/2007 End --%>
  CheckTheObj(document.SecurityProfileDetail.DRCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DRCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DRDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DRRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DRAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DRProxyAuthorise,checkedFlag);
  <%-- CheckTheObj(document.SecurityProfileDetail.WorkSubsidiary,checkedFlag); --%>
  CheckTheObj(document.SecurityProfileDetail.RACreateMod,checkedFlag); <%-- crhodes CR451 10/15/2008 Begin --%>
  CheckTheObj(document.SecurityProfileDetail.RADelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.RARoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.RAAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.RAProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.TFCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.TFDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.TFRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.TFAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.TFProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPCreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPUploadBulkFile,checkedFlag);
  
  <%--  CheckTheObj(document.SecurityProfileDetail.DDICreateMod,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DDIDelete,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DDIRoute,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DDIAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DDIProxyAuthorise,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DDIUploadFile,checkedFlag);  --%>
  <%-- CR 821 - Rel 8.3 START --%>
  CheckTheObj(document.SecurityProfileDetail.ATPChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ATPRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DPRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SLCChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SLCRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpDLCChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpDLCRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.ExpCollRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.GUARChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.GUARRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.NewExpCollChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.NewExpCollRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DLCRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SGChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.SGRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.AWRChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.AWRRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.LRChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.LRRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.TFChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.TFRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.RAChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.RARepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.FTChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.FTRepair,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DRChecker,checkedFlag);
  CheckTheObj(document.SecurityProfileDetail.DRRepair,checkedFlag);
<%-- CR 821 - Rel 8.3 END --%>
<%-- SSikhakolli - Rel-9.4 CR-818 --%>
CheckTheObj(document.SecurityProfileDetail.SICreateMod,checkedFlag);
CheckTheObj(document.SecurityProfileDetail.SIDelete,checkedFlag);
CheckTheObj(document.SecurityProfileDetail.SIRoute,checkedFlag);
CheckTheObj(document.SecurityProfileDetail.SIAuthorise,checkedFlag);
CheckTheObj(document.SecurityProfileDetail.SIProxyAuthorise,checkedFlag);
CheckTheObj(document.SecurityProfileDetail.SIChecker,checkedFlag);
CheckTheObj(document.SecurityProfileDetail.SIRepair,checkedFlag);
  }
 

  
function selectAllReceivables() { 
      
       radio0 = document.SecurityProfileDetail.SelectAllReceivables[0];
       radio1 = document.SecurityProfileDetail.SelectAllReceivables[1];
        
        var checkedFlag;
        
        if (radio0.checked == true ){
              checkedFlag = true;
              var check =  document.SecurityProfileDetail.AccessReceivables;

            var checkObj1 = dijit.getEnclosingWidget(check);
              check.checked = false;
              checkObj1.set('checked',false);
        
        }else if (radio1.checked == true){
              checkedFlag = false;
        }
        
      CheckTheObj(document.SecurityProfileDetail.ReceivablesMatch,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesRoute,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesProxyAuthorize,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesAuthorize,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesApproveDiscountAuthorize,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesApproveDiscountProxyAuthorize,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesAddBuyerToPayment,checkedFlag);
}

function selectAllInvoice1() { 
       radio0 = document.SecurityProfileDetail.SelectAllInvoice[0];
       radio1 = document.SecurityProfileDetail.SelectAllInvoice[1];
        
        var checkedFlag;
        
        if (radio0.checked == true ){
              checkedFlag = true;
              var check =  document.SecurityProfileDetail.AccessReceivables;

            var checkObj1 = dijit.getEnclosingWidget(check);
              check.checked = false;
              checkObj1.set('checked',false);
        
        }else if (radio1.checked == true){
              checkedFlag = false;
        }
        
      CheckTheObj(document.SecurityProfileDetail.ReceivablesDisputeUndispute,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesClose,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesFinance,checkedFlag);
}

function selectAllUploadInvoice() { 
      radio0 = document.SecurityProfileDetail.SelectAllUploadInvoice[0];
       radio1 = document.SecurityProfileDetail.SelectAllUploadInvoice[1];
        
        var checkedFlag;
        
        if (radio0.checked == true ){
              checkedFlag = true;
              var check =  document.SecurityProfileDetail.AccessReceivables;

            var checkObj1 = dijit.getEnclosingWidget(check);
              check.checked = false;
              checkObj1.set('checked',false);
        
        }else if (radio1.checked == true){
              checkedFlag = false;
        }
        
      CheckTheObj(document.SecurityProfileDetail.ApproveFinancing,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.DeleteInvoice,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.RemovableInvoice,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.AuthorizeInvoice,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ProxyAuthorizeInvoice,checkedFlag);
      <%-- DK CR709 Rel8.2 Begins --%>
      CheckTheObj(document.SecurityProfileDetail.ReceivablesApplyPaymentDate,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesClearPaymentDate,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesAssignInstrType,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesAssignLoanType,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesCreateLoanReq,checkedFlag);
      CheckTheObj(document.SecurityProfileDetail.ReceivablesDeclineInvoice,checkedFlag); <%-- #RKAZI CR1006 04-24-2015  - Add  Decline Invoice --%>
      <%-- DK CR709 Rel8.2 Ends --%>
}

function selectAllATPInvoice() { 
     radio0 = document.SecurityProfileDetail.SelectAllATPInvoice[0];
     radio1 = document.SecurityProfileDetail.SelectAllATPInvoice[1];
      
      var checkedFlag;
      
      if (radio0.checked == true ){
            checkedFlag = true;
            var check =  document.SecurityProfileDetail.AccessReceivables;

          var checkObj1 = dijit.getEnclosingWidget(check);
            check.checked = false;
            checkObj1.set('checked',false);
      
      }else if (radio1.checked == true){
            checkedFlag = false;
      }
      
    CheckTheObj(document.SecurityProfileDetail.ApplyPaymentDate,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.CreateATP,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayableDeleteInvoice,checkedFlag);
    <%--  DK CR709 Rel8.2 Begins --%>
    CheckTheObj(document.SecurityProfileDetail.PayClearPaymentDate,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayAssignInstrType,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayAssignLoanType,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayCreateLoanReq,checkedFlag);
    <%--  DK CR709 Rel8.2 Ends --%>
    <%--Rel 9.0 CR 913 Start--%>
    CheckTheObj(document.SecurityProfileDetail.PayAuthorise,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayAuthoriseOffline,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayApplyEarlyPaymentAmt,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayResetEarlyPaymentAmt,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayApplySendtoSupplierDate,checkedFlag);
    CheckTheObj(document.SecurityProfileDetail.PayResetSendtoSupplierDate,checkedFlag);
    <%--Rel 9.0 CR 913 Ends--%>
}


function selectAllCreditNoteOptions(checkedFlag){
	
	CheckTheObj(document.SecurityProfileDetail.PayCrNoteAuthorize,checkedFlag);
	CheckTheObj(document.SecurityProfileDetail.PayCrNoteAuthorizeOffline,checkedFlag);
	CheckTheObj(document.SecurityProfileDetail.PayCrNoteDelete,checkedFlag);
	CheckTheObj(document.SecurityProfileDetail.PayCrNoteClose,checkedFlag);
	CheckTheObj(document.SecurityProfileDetail.PayCrNoteApplyManual,checkedFlag);
	CheckTheObj(document.SecurityProfileDetail.PayCrNoteUnApplyManual,checkedFlag);
	CheckTheObj(document.SecurityProfileDetail.PayablesDeclineCreditNote,checkedFlag); <%-- #RKAZI CR1006 04-24-2015  - Add  Decline Invoice --%>
	CheckTheObj(document.SecurityProfileDetail.PayablesApproveCreditNote,checkedFlag); <%-- #PGedupudi CR1006 05-14-2015  - Add  Approve CreditNote --%>
	
}

function selectAllSupplierPortal() { 
    radio0 = document.SecurityProfileDetail.SelectAllSupplierPortal[0];
    radio1 = document.SecurityProfileDetail.SelectAllSupplierPortal[1];
     
     var checkedFlag;
     
     if (radio0.checked == true ){
           checkedFlag = true;
           var check =  document.SecurityProfileDetail.AccessReceivables;

         var checkObj1 = dijit.getEnclosingWidget(check);
           check.checked = false;
           checkObj1.set('checked',false);
     
     }else if (radio1.checked == true){
           checkedFlag = false;
     }
   CheckTheObj(document.SecurityProfileDetail.SpInvoicesOffered,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpHistory,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpAcceptOffer,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpAuthoriseOffer,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpDeclineOffer,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpAssignFutureValueDate,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpRemoveFutureValueDate,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpRemoveInvoice,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.SpResetToOffered,checkedFlag);
}

<%-- Rel 9.0 CR 913 Start--%>
function selectAllPayInvoice() { 
    radio0 = document.SecurityProfileDetail.SelectAllPayInvoice[0];
    radio1 = document.SecurityProfileDetail.SelectAllPayInvoice[1];
     
     var checkedFlag;
     
     if (radio0.checked == true ){
           checkedFlag = true;
           var check =  document.SecurityProfileDetail.AccessReceivables;

         var checkObj1 = dijit.getEnclosingWidget(check);
           check.checked = false;
           checkObj1.set('checked',false);
     
     }else if (radio1.checked == true){
           checkedFlag = false;
     }
     
   CheckTheObj(document.SecurityProfileDetail.PayablesAuthorise,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayablesAuthoriseOffline,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayablesApprovePayment,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayablesClearPaymentDate,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayablesApplyEarlyPaymentAmt,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayablesResetEarlyPaymentAmt,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayablesApplySendtoSupplierDate,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayablesResetSendtoSupplierDate,checkedFlag);
   CheckTheObj(document.SecurityProfileDetail.PayDeclineInvoice,checkedFlag); <%-- #RKAZI CR1006 04-24-2015  - Add  Decline invoice --%>
}
<%-- Rel 9.0 CR 913 End--%>
</script>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/ButtonPrep.jsp"></jsp:include>
<%
      String pageTitleKey = resMgr.getText("SecurityProfileDetail.SecurityProfiles",TradePortalConstants.TEXT_BUNDLE);
      String helpUrl = "customer/security_profile_detail.htm";
%> 
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
      <jsp:param name="item1Key" value="" />
      <jsp:param name="helpUrl" value="<%=helpUrl%>" />
    </jsp:include> 
<%// --------------------- Sub header start --------------------- %>
<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if ( insertMode) {
    subHeaderTitle = resMgr.getText("SecurityProfileDetail.NewSecurityProfile",TradePortalConstants.TEXT_BUNDLE);
  } else {
    subHeaderTitle = securityProfile.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      
<%// --------------------- Sub header end --------------------- %>

<form id="SecurityProfileDetail" name="SecurityProfileDetail"
      method="post" data-dojo-type="dijit.form.Form"
      action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden value="" name=buttonName>
<input type=hidden value="" name=saveAsFlag>
 
<div class="formArea"><jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">



<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);
  secureParms.put("opt_lock", securityProfile.getAttribute("opt_lock"));


  secureParms.put("security_profile_oid", oid);
  secureParms.put("Type", TradePortalConstants.NON_ADMIN);
  secureParms.put("OwnershipLevel", userSession.getOwnershipLevel());

  // Used for the "Added By" column
  if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
  secureParms.put("OwnershipType", TradePortalConstants.OWNER_TYPE_ADMIN);
  else
  secureParms.put("OwnershipType", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
  String ownershipLevel     = userSession.getOwnershipLevel();            
  if (ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
        showSaveAs = false;  
  }
%> <% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %> <%= formMgr.getFormInstanceAsInputField("SecurityProfileDetailForm",
                                        secureParms) %> 
<% // ----------- 1. General starts here -------------- %>


<%= widgetFactory.createSectionHeader( "1","SecurityProfileDetail.1General") %>

<%= widgetFactory.createTextField( "Name", "SecurityProfileDetail.SecurityProfileName", securityProfile.getAttribute("name"), "25", isReadOnly, true, false,  "", "","" ) %>
</div>


<%// ----------- 2. Messages starts here -------------- %> 

<%= widgetFactory.createSectionHeader( "2","SecurityProfileDetail.2Messages") %>

<div>
<%=widgetFactory.createCheckboxField("AccessMessage","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.ACCESS_MESSAGES_AREA),isReadOnly,false,"onClick=\"deselectMessageRights();\"","","")%>
</div>
<div class="formItem">
<%=widgetFactory.createCheckboxField("ViewChildOrgMessages","SecurityProfileDetail.MessageSubBranches",SecurityAccess.hasRights(securityProfileRights,
          SecurityAccess.VIEW_CHILD_ORG_MESSAGES),isReadOnly,false,"onClick=\"pickMessageAccess();\" labelClass=\"formWidth\"","","")%>
</div>


<%String col1 = resMgr.getText("SecurityProfileDetail.DeleteFromInbox", TradePortalConstants.TEXT_BUNDLE); //widgetFactory.createLabel("", "SecurityProfileDetail.DeleteFromInbox", false, false, false, "");
String col2 = resMgr.getText("SecurityProfileDetail.CreateReply", TradePortalConstants.TEXT_BUNDLE); //widgetFactory.createLabel("", "SecurityProfileDetail.CreateReply", false, false, false, "");
String col4 = resMgr.getText("SecurityProfileDetail.MessageSend", TradePortalConstants.TEXT_BUNDLE); //widgetFactory.createLabel("", "SecurityProfileDetail.MessageSend", false, false, false, "");
String col3 = resMgr.getText("SecurityProfileDetail.MessageRoute", TradePortalConstants.TEXT_BUNDLE); //widgetFactory.createLabel("", "SecurityProfileDetail.MessageRoute", false, false, false, "");

String col5 = resMgr.getText("SecurityProfileDetail.AttachDocumentHdr", TradePortalConstants.TEXT_BUNDLE); //widgetFactory.createLabel("", "SecurityProfileDetail.AttachDocumentHdr", false, false, false, "");
String col6 = resMgr.getText("SecurityProfileDetail.DeleteDocumentHdr", TradePortalConstants.TEXT_BUNDLE); //widgetFactory.createLabel("", "SecurityProfileDetail.DeleteDocumentHdr", false, false, false, "");

//Row1
String mailMessages = resMgr.getText("SecurityProfileDetail.MailMessages", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.MailMessages", false, false, false, "");

String deleteMessage = widgetFactory.createCheckboxField("DeleteMessage","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DELETE_MESSAGE),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");


String createMessage = widgetFactory.createCheckboxField("CreateMessage","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.CREATE_MESSAGE),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");

String sendMessage = widgetFactory.createCheckboxField("SendMessage","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SEND_MESSAGE),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");

String routeMessage = widgetFactory.createCheckboxField("RouteMessage","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.ROUTE_MESSAGE),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");

String messageAttachDocument = widgetFactory.createCheckboxField("MessageAttachDocument","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.ATTACH_DOC_MESSAGE),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");

String messageDeleteDocument = widgetFactory.createCheckboxField("MessageDeleteDocument","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DELETE_DOC_MESSAGE),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");


//Rel9.0 CR 913 - New row added - Start
String preDebitFundingNotifications = resMgr.getText("SecurityProfileDetail.PreDebitFundingNotifications", TradePortalConstants.TEXT_BUNDLE);
String deletePreDebitFundingNotif = widgetFactory.createCheckboxField("DeletePreDebitFundingNotif","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DELETE_PREDEBIT_FUNDING_NOTIFICATION),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");
String routePreDebitFundingNotif = widgetFactory.createCheckboxField("RoutePreDebitFundingNotif","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.ROUTE_PREDEBIT_FUNDING_NOTIFICATION),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");
//Rel9.0 CR 913 - Ends

//Row2 
String discrepancies = resMgr.getText("SecurityProfileDetail.Discrepancies", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.Discrepancies", false, false, false, "");
String deleteDiscrepancyMsg = widgetFactory.createCheckboxField("DeleteDiscrepancyMsg","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DELETE_DISCREPANCY_MSG),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");
String routeDiscrepancyMsg = widgetFactory.createCheckboxField("RouteDiscrepancyMsg","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.ROUTE_DISCREPANCY_MSG),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");


//Row3
String notifications = resMgr.getText("SecurityProfileDetail.Notifications", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.Notifications", false, false, false, "");
String deleteNotify = widgetFactory.createCheckboxField("DeleteNotify","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DELETE_NOTIFICATION),isReadOnly,false,"onClick=\"pickMessageAccess();\"","","none");
 %>


<table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="30%">&nbsp;</th>
            <th width="10%"><%=col1%></th>
            <th width="10%"><%=col2%></th>
            <th width="10%"><%=col4%></th>
            <th width="10%"><%=col3%></th>
            <th width="15%"><%=col5%></th>
            <th width="15%"><%=col6%></th>
      </tr>
      </thead>
      <%// ----------------- ROWS  ------------------------ %>
      <tr>
            <td><%=mailMessages%></td>
            <td align="center"><%=deleteMessage%></td>
            <td align="center"><%=createMessage%></td>
            
            <td align="center"><%=sendMessage%></td>
            <td align="center"><%=routeMessage%></td>
            <td align="center"><%=messageAttachDocument%></td>
            <td align="center"><%=messageDeleteDocument%></td>
      </tr>
      <%--Rel9.0 CR 913 Start --%>
      <tr>
            <td width="30%"><%=preDebitFundingNotifications%></td>
            <td align="center"><%=deletePreDebitFundingNotif%></td>
            
            <td>&nbsp;</td>
            
            <td>&nbsp;</td>
            <td align="center"><%=routePreDebitFundingNotif%></td>
            
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <%--Rel9.0 CR 913 End --%>
      <tr>
            <td width="30%"><%=discrepancies%></td>
            <td align="center"><%=deleteDiscrepancyMsg%></td>
            
            <td>&nbsp;</td>
            
            <td>&nbsp;</td>
            <td align="center"><%=routeDiscrepancyMsg%></td>
            
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td align="left"><%=notifications%></td>
            <td align="center"><%=deleteNotify%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
</table>

</div>

<%----------- 2. Messages ends here --------------%> 

<%-- 3. Instruments starts here --%>
<%= widgetFactory.createSectionHeader( "3","SecurityProfileDetail.3Instruments") %>
<div>
<%=widgetFactory.createCheckboxField("AccessInstrument","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
         SecurityAccess.ACCESS_INSTRUMENTS_AREA),isReadOnly,false,"onClick=\"noAccessAllInstruments();\"","","")%>
</div>
<div class="formItem">

<%=widgetFactory.createCheckboxField("WorkSubsidiary","SecurityProfileDetail.InstrumentsWorkSubsidiaires",SecurityAccess.hasRights(securityProfileRights,
          SecurityAccess.VIEW_CHILD_ORG_WORK),isReadOnly,false,"onClick=\"pickInstrumentAccess();\" labelClass=\"formWidth\"","","")%>
</div>

<%String inCol1 = resMgr.getText("SecurityProfileDetail.InstrumentsCreateModify", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsCreateModify", false, false, false, "");
String inCol2 = resMgr.getText("SecurityProfileDetail.InstrumentsDelete", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsDelete", false, false, false, "");
String inCol3 = resMgr.getText("SecurityProfileDetail.InstrumentsRoute", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsRoute", false, false, false, "");
String inCol4 = resMgr.getText("SecurityProfileDetail.InstrumentsAuthorise", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsAuthorise", false, false, false, "");
String inCol5 = resMgr.getText("SecurityProfileDetail.AuthoriseOffline", TradePortalConstants.TEXT_BUNDLE);
String inCol6 = resMgr.getText("SecurityProfileDetail.ProcessPurchaseOrders", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ProcessPurchaseOrders", false, false, false, "");
String inCol7 = resMgr.getText("SecurityProfileDetail.InstrumentsUploadBulkFile", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsUploadBulkFile", false, false, false, "");
String inCol8 = resMgr.getText("SecurityProfileDetail.InstrumentProcessInvoices", TradePortalConstants.TEXT_BUNDLE);
//CR 821 - Rel 8.3 BEGIN
String inCol9 = resMgr.getText("SecurityProfileDetail.InstrumentsChecker", TradePortalConstants.TEXT_BUNDLE);
String inCol10 = resMgr.getText("SecurityProfileDetail.InstrumentsSendForRepair", TradePortalConstants.TEXT_BUNDLE);
//CR 821 - Rel 8.3 END
String inCol11 = resMgr.getText("SecurityProfileDetail.ConvertedInstConvert", TradePortalConstants.TEXT_BUNDLE);
//Approval to Pay Row1 -
String instrumentsATP = resMgr.getText("SecurityProfileDetail.InstrumentsATP", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsATP", false, false, false, "");

String aTPCreateMod = widgetFactory.createCheckboxField("ATPCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.APPROVAL_TO_PAY_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String aTPDelete = widgetFactory.createCheckboxField("ATPDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.APPROVAL_TO_PAY_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String aTPRoute = widgetFactory.createCheckboxField("ATPRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.APPROVAL_TO_PAY_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String aTPAuthorise = widgetFactory.createCheckboxField("ATPAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.APPROVAL_TO_PAY_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String aTPProcessPO = widgetFactory.createCheckboxField("ATPProcessPO","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.APPROVAL_TO_PAY_PROCESS_PO),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String aTPProxyAuthorise = widgetFactory.createCheckboxField("ATPProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_ATP_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String aTPProcessInvoice = widgetFactory.createCheckboxField("ProcessATPInvoices","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.ATP_PROCESS_INVOICES),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

//CR 821 - Rel 8.3
String aTPChecker = widgetFactory.createCheckboxField("ATPChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.APPROVAL_TO_PAY_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String aTPRepair = widgetFactory.createCheckboxField("ATPRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.APPROVAL_TO_PAY_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");             


//Ditect Pay Row2 -
String instrumentsDP = resMgr.getText("SecurityProfileDetail.InstrumentsDP", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsDP", false, false, false, "");

String DPCreateMod = widgetFactory.createCheckboxField("DPCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DOMESTIC_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
String DPDelete = widgetFactory.createCheckboxField("DPDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DOMESTIC_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
String DPRoute = widgetFactory.createCheckboxField("DPRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DOMESTIC_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
String DPAuthorise = widgetFactory.createCheckboxField("DPAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DOMESTIC_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
String DPUploadBulkFile = widgetFactory.createCheckboxField("DPUploadBulkFile","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DOMESTIC_UPLOAD_BULK_FILE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
String DPProxyAuthorise = widgetFactory.createCheckboxField("DPProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.PROXY_DP_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String DPChecker = widgetFactory.createCheckboxField("DPChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DOMESTIC_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String DPRepair = widgetFactory.createCheckboxField("DPRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DOMESTIC_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  

//DLC  Row -
String InstrumentsDLC = resMgr.getText("SecurityProfileDetail.InstrumentsDLC", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsDLC", false, false, false, "");

String DLCCreateMod = widgetFactory.createCheckboxField("DLCCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DLC_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DLCDelete = widgetFactory.createCheckboxField("DLCDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DLC_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DLCRoute = widgetFactory.createCheckboxField("DLCRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DLC_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DLCAuthorise = widgetFactory.createCheckboxField("DLCAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DLC_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DLCProcessPO = widgetFactory.createCheckboxField("DLCProcessPO","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DLC_PROCESS_PO),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DLCProxyAuthorise = widgetFactory.createCheckboxField("DLCProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_DLC_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String DLCChecker = widgetFactory.createCheckboxField("DLCChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DLC_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String DLCRepair = widgetFactory.createCheckboxField("DLCRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DLC_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  

//SLC Row -
String InstrumentsSLC = resMgr.getText("SecurityProfileDetail.InstrumentsSLC", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsSLC", false, false, false, "");

String SLCCreateMod = widgetFactory.createCheckboxField("SLCCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SLC_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SLCDelete = widgetFactory.createCheckboxField("SLCDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SLC_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SLCRoute = widgetFactory.createCheckboxField("SLCRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SLC_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SLCAuthorise = widgetFactory.createCheckboxField("SLCAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SLC_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SLCProxyAuthorise = widgetFactory.createCheckboxField("SLCProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_SLC_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String SLCChecker = widgetFactory.createCheckboxField("SLCChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SLC_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String SLCRepair = widgetFactory.createCheckboxField("SLCRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SLC_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  


//InstrumentsExpDLC -
String InstrumentsExpDLC = resMgr.getText("SecurityProfileDetail.InstrumentsExpDLC", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsExpDLC", false, false, false, "");

String ExpDLCCreateMod = widgetFactory.createCheckboxField("ExpDLCCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_LC_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpDLCDelete = widgetFactory.createCheckboxField("ExpDLCDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_LC_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpDLCRoute = widgetFactory.createCheckboxField("ExpDLCRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_LC_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpDLCAuthorise = widgetFactory.createCheckboxField("ExpDLCAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_LC_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpDLCProxyAuthorise = widgetFactory.createCheckboxField("ExpDLCProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_EXP_DLC_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String ExpDLCChecker = widgetFactory.createCheckboxField("ExpDLCChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_LC_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String ExpDLCRepair = widgetFactory.createCheckboxField("ExpDLCRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_LC_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  

//InstrumentsEC -
String InstrumentsEC = resMgr.getText("SecurityProfileDetail.InstrumentsEC", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsEC", false, false, false, "");

String ExpCollCreateMod = widgetFactory.createCheckboxField("ExpCollCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_COLL_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpCollDelete = widgetFactory.createCheckboxField("ExpCollDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_COLL_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpCollRoute = widgetFactory.createCheckboxField("ExpCollRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_COLL_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpCollAuthorise = widgetFactory.createCheckboxField("ExpCollAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_COLL_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String ExpCollProxyAuthorise = widgetFactory.createCheckboxField("ExpCollProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_EXP_COLL_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String ExpCollChecker = widgetFactory.createCheckboxField("ExpCollChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_COLL_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String ExpCollRepair = widgetFactory.createCheckboxField("ExpCollRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.EXPORT_COLL_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  

//InstrumentsGUAR -
String InstrumentsGUAR = resMgr.getText("SecurityProfileDetail.InstrumentsGUAR", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsGUAR", false, false, false, "");

String GUARCreateMod = widgetFactory.createCheckboxField("GUARCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.GUAR_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String GUARDelete = widgetFactory.createCheckboxField("GUARDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.GUAR_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String GUARRoute = widgetFactory.createCheckboxField("GUARRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.GUAR_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String GUARAuthorise = widgetFactory.createCheckboxField("GUARAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.GUAR_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String GUARProxyAuthorise = widgetFactory.createCheckboxField("GUARProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_GUAR_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String GUARChecker = widgetFactory.createCheckboxField("GUARChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.GUAR_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String GUARRepair = widgetFactory.createCheckboxField("GUARRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.GUAR_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  

//InstrumentsNEC -
String InstrumentsNEC = resMgr.getText("SecurityProfileDetail.InstrumentsNEC", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsNEC", false, false, false, "");

String NewExpCollCreateMod = widgetFactory.createCheckboxField("NewExpCollCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.NEW_EXPORT_COLL_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String NewExpCollDelete = widgetFactory.createCheckboxField("NewExpCollDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.NEW_EXPORT_COLL_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String NewExpCollRoute = widgetFactory.createCheckboxField("NewExpCollRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.NEW_EXPORT_COLL_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String NewExpCollAuthorise = widgetFactory.createCheckboxField("NewExpCollAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.NEW_EXPORT_COLL_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String NewExpCollProxyAuthorise = widgetFactory.createCheckboxField("NewExpCollProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_NEW_EXP_COLL_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String NewExpCollChecker = widgetFactory.createCheckboxField("NewExpCollChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.NEW_EXPORT_COLL_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String NewExpCollRepair = widgetFactory.createCheckboxField("NewExpCollRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.NEW_EXPORT_COLL_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  
 
        
//InstrumentsSG -
String InstrumentsSG = resMgr.getText("SecurityProfileDetail.InstrumentsSG", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsSG", false, false, false, "");

String SGCreateMod = widgetFactory.createCheckboxField("SGCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SHIP_GUAR_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SGDelete = widgetFactory.createCheckboxField("SGDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SHIP_GUAR_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SGAuthorise = widgetFactory.createCheckboxField("SGAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SHIP_GUAR_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SGRoute = widgetFactory.createCheckboxField("SGRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SHIP_GUAR_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SGProxyAuthorise = widgetFactory.createCheckboxField("SGProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_SG_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String SGChecker = widgetFactory.createCheckboxField("SGChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SHIP_GUAR_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String SGRepair = widgetFactory.createCheckboxField("SGRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SHIP_GUAR_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  


//InstrumentsAWR -
String InstrumentsAWR = resMgr.getText("SecurityProfileDetail.InstrumentsAWR", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsAWR", false, false, false, "");

String AWRCreateMod = widgetFactory.createCheckboxField("AWRCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.AIR_WAYBILL_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String AWRDelete = widgetFactory.createCheckboxField("AWRDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.AIR_WAYBILL_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String AWRRoute = widgetFactory.createCheckboxField("AWRRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.AIR_WAYBILL_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String AWRAuthorise = widgetFactory.createCheckboxField("AWRAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.AIR_WAYBILL_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String AWRProxyAuthorise = widgetFactory.createCheckboxField("AWRProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_AWR_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String AWRChecker = widgetFactory.createCheckboxField("AWRChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.AIR_WAYBILL_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String AWRRepair = widgetFactory.createCheckboxField("AWRRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.AIR_WAYBILL_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  
                      
                          
 //InstrumentsLR -
String InstrumentsLR = resMgr.getText("SecurityProfileDetail.InstrumentsLR", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsLR", false, false, false, "");

String LRCreateMod = widgetFactory.createCheckboxField("LRCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.LOAN_RQST_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String LRDelete  = widgetFactory.createCheckboxField("LRDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.LOAN_RQST_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String LRRoute = widgetFactory.createCheckboxField("LRRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.LOAN_RQST_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String LRAuthorise = widgetFactory.createCheckboxField("LRAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.LOAN_RQST_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String LRProxyAuthorise = widgetFactory.createCheckboxField("LRProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_LR_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

// DK CR709 Rel8.2 10/31/2012 Added
String LRProcessInvoices = widgetFactory.createCheckboxField("LRProcessInvoices","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.LOAN_RQST_PROCESS_INVOICES),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String LRChecker = widgetFactory.createCheckboxField("LRChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.LOAN_RQST_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String LRRepair = widgetFactory.createCheckboxField("LRRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.LOAN_RQST_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");     

//InstrumentsTF-
String InstrumentsTF = resMgr.getText("SecurityProfileDetail.InstrumentsTF", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsTF", false, false, false, "");

String TFCreateMod = widgetFactory.createCheckboxField("TFCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.TRANSFER_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String TFDelete  = widgetFactory.createCheckboxField("TFDelete","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.TRANSFER_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String TFRoute = widgetFactory.createCheckboxField("TFRoute","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.TRANSFER_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String TFAuthorise = widgetFactory.createCheckboxField("TFAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.TRANSFER_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String TFProxyAuthorise = widgetFactory.createCheckboxField("TFProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.PROXY_TF_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");
//CR 821 - Rel 8.3
String TFChecker = widgetFactory.createCheckboxField("TFChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.TRANSFER_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String TFRepair = widgetFactory.createCheckboxField("TFRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.TRANSFER_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  
      
     
//InstrumentsRA-
String InstrumentsRA = resMgr.getText("SecurityProfileDetail.InstrumentsRA", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsRA", false, false, false, "");

String RACreateMod = widgetFactory.createCheckboxField("RACreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.REQUEST_ADVISE_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String RADelete  = widgetFactory.createCheckboxField("RADelete","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.REQUEST_ADVISE_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

      
String RARoute = widgetFactory.createCheckboxField("RARoute","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.REQUEST_ADVISE_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String RAAuthorise = widgetFactory.createCheckboxField("RAAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.REQUEST_ADVISE_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none"); 

String RAProxyAuthorise = widgetFactory.createCheckboxField("RAProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.PROXY_RA_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");  
//CR 821 - Rel 8.3
String RAChecker = widgetFactory.createCheckboxField("RAChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.REQUEST_ADVISE_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String RARepair = widgetFactory.createCheckboxField("RARepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.REQUEST_ADVISE_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  


//InstrumentsFT-
String InstrumentsFT = resMgr.getText("SecurityProfileDetail.InstrumentsFT", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InstrumentsFT", false, false, false, "");

String FTCreateMod = widgetFactory.createCheckboxField("FTCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.FUNDS_XFER_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String FTDelete = widgetFactory.createCheckboxField("FTDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.FUNDS_XFER_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String FTRoute = widgetFactory.createCheckboxField("FTRoute","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.FUNDS_XFER_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String FTAuthorise = widgetFactory.createCheckboxField("FTAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.FUNDS_XFER_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");  

String FTProxyAuthorise = widgetFactory.createCheckboxField("FTProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.PROXY_FT_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none"); 
//CR 821 - Rel 8.3
String FTChecker = widgetFactory.createCheckboxField("FTChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.FUNDS_XFER_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String FTRepair = widgetFactory.createCheckboxField("FTRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.FUNDS_XFER_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  
     
//DiscRespTransaction-
String DiscRespTransaction = resMgr.getText("SecurityProfileDetail.DiscRespTransaction", TradePortalConstants.TEXT_BUNDLE);

String DRCreateMod = widgetFactory.createCheckboxField("DRCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DISCREPANCY_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DRDelete = widgetFactory.createCheckboxField("DRDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DISCREPANCY_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DRRoute = widgetFactory.createCheckboxField("DRRoute","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.DISCREPANCY_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String DRAuthorise = widgetFactory.createCheckboxField("DRAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.DISCREPANCY_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none"); 

String DRProxyAuthorise = widgetFactory.createCheckboxField("DRProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.PROXY_DP_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");  
//CR 821 - Rel 8.3
String DRChecker = widgetFactory.createCheckboxField("DRChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DISCREPANCY_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String DRRepair = widgetFactory.createCheckboxField("DRRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DISCREPANCY_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none");  

//SSikhakolli - Rel-9.4 CR-818 - SettlementInstructionTransaction
String SettlementInstructionTransaction = resMgr.getText("SecurityProfileDetail.SettlementInstructionTransaction", TradePortalConstants.TEXT_BUNDLE);

String SICreateMod = widgetFactory.createCheckboxField("SICreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SIT_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SIDelete = widgetFactory.createCheckboxField("SIDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SIT_DELETE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SIRoute = widgetFactory.createCheckboxField("SIRoute","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.SIT_ROUTE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");

String SIAuthorise = widgetFactory.createCheckboxField("SIAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.SIT_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none"); 

String SIProxyAuthorise = widgetFactory.createCheckboxField("SIProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.PROXY_SIT_AUTHORIZE),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","none");  

String SIChecker = widgetFactory.createCheckboxField("SIChecker","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SIT_CHECKER),isReadOnly,false,"onClick=\"pickInstrumentAccess();selectRepairAccess();\"","","none");

String SIRepair = widgetFactory.createCheckboxField("SIRepair","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SIT_REPAIR),isReadOnly,false,"onClick=\"pickInstrumentAccess();setCheckerAccess();\"","","none"); 
%>

<table width="100%" class="formDocumentsTable">
      <thead>
      <tr>
            <th width="30%">&nbsp;</th>
            <th width="10%"><%=inCol1%></th>
            <th width="10%"><%=inCol2%></th>
            <th width="10%"><%=inCol3%></th>
            <th width="10%"><%=inCol9%></th>
            <th width="10%"><%=inCol10%></th>
            <th width="10%"><%=inCol4%></th>
            <th width="10%"><%=inCol5%></th>
            <th width="10%"><%=inCol6%></th>
            <th width="10%"><%=inCol7%></th>
            <th width="10%"><%=inCol8%></th>
      </tr>
    </thead>
	
      <tr >
            <th colspan=11" style="text-align: left;">
			<%=widgetFactory.createRadioButtonField("SelectAllInst", "Y", 
            "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllInstruments('A');\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; <%=widgetFactory.createRadioButtonField("SelectAllInst", "N", 
            "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllInstruments('N');\"","")%>
			</th>
      </tr>
      
      <tr class="tableSubHeader">
            <td colspan="11"><%=resMgr.getText("SecurityProfileDetail.Instruments", TradePortalConstants.TEXT_BUNDLE)%></td>
      </tr>

      <tr>
            <td><%=InstrumentsAWR%></td>  
            <td align="center"><%=AWRCreateMod%></td>  
            <td align="center"><%=AWRDelete%></td> 
            <td align="center"><%=AWRRoute%></td> 
            <td align="center"><%=AWRChecker%></td> 
            <td align="center"><%=AWRRepair%></td> 
            <td align="center"><%=AWRAuthorise%></td>
            <td align="center"><%=AWRProxyAuthorise%></td> 
            <td>&nbsp;</td> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=instrumentsATP%></td>
            <td align="center"><%=aTPCreateMod%></td>
            <td align="center"><%=aTPDelete%></td>
            <td align="center"><%=aTPRoute%></td>
            <td align="center"><%=aTPChecker%></td> 
            <td align="center"><%=aTPRepair%></td> 
            <td align="center"><%=aTPAuthorise%></td>
            <td align="center"><%=aTPProxyAuthorise%></td>
            <td align="center"><%=aTPProcessPO%></td>
            <td align="center">&nbsp;</td>
            <td align="center"><%=aTPProcessInvoice%></td>
      </tr>

      


      <tr>
            <td><%=InstrumentsEC%></td>
            <td align="center"><%=ExpCollCreateMod%></td>
            <td align="center"><%=ExpCollDelete%></td>
            <td align="center"><%=ExpCollRoute%></td>
            <td align="center"><%=ExpCollChecker%></td> 
            <td align="center"><%=ExpCollRepair%></td> 
            <td align="center"><%=ExpCollAuthorise%></td>
            <td align="center"><%=ExpCollProxyAuthorise%></td>
            <td >&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=InstrumentsNEC%></td>
            <td align="center"><%=NewExpCollCreateMod%></td>
            <td align="center"><%=NewExpCollDelete%></td>
            <td align="center"><%=NewExpCollRoute%></td>
            <td align="center"><%=NewExpCollChecker%></td> 
            <td align="center"><%=NewExpCollRepair%></td> 
            <td align="center"><%=NewExpCollAuthorise%></td>
            <td align="center"><%=NewExpCollProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=InstrumentsExpDLC%></td>
            <td align="center"><%=ExpDLCCreateMod%></td>
            <td align="center"><%=ExpDLCDelete%></td>
            <td align="center"><%=ExpDLCRoute%></td>
            <td align="center"><%=ExpDLCChecker%></td> 
            <td align="center"><%=ExpDLCRepair%></td> 
            <td align="center"><%=ExpDLCAuthorise%></td>
            <td align="center"><%=ExpDLCProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=InstrumentsDLC%></td>
            <td align="center"><%=DLCCreateMod%></td>
            <td align="center"><%=DLCDelete%></td>
            <td align="center"><%=DLCRoute%></td>
            <td align="center"><%=DLCChecker%></td> 
            <td align="center"><%=DLCRepair%></td> 
            <td align="center"><%=DLCAuthorise%></td>
            <td align="center"><%=DLCProxyAuthorise%></td>
            <td align="center"><%=DLCProcessPO%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=InstrumentsFT%></td>
            <td align="center"><%=FTCreateMod%></td>
            <td align="center"><%=FTDelete%></td>
            <td align="center"><%=FTRoute%></td>
            <td align="center"><%=FTChecker%></td> 
            <td align="center"><%=FTRepair%></td> 
            <td align="center"><%=FTAuthorise%></td>
            <td align="center"><%=FTProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=InstrumentsLR%></td>
            <td align="center"><%=LRCreateMod%></td>
            <td align="center"><%=LRDelete%></td>
            <td align="center"><%=LRRoute%></td>
            <td align="center"><%=LRChecker%></td> 
            <td align="center"><%=LRRepair%></td> 
            <td align="center"><%=LRAuthorise%></td>
            <td align="center"><%=LRProxyAuthorise%></td>            
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <%-- DK CR709 Rel8.2 10/31/2012 BEGINS --%>
            <td align="center"><%=LRProcessInvoices%></td>
            <%-- DK CR709 Rel8.2 10/31/2012 ENDS --%>
      </tr>

      <tr>
            <td><%=InstrumentsGUAR%></td>
            <td align="center"><%=GUARCreateMod%></td>
            <td align="center"><%=GUARDelete%></td>
            <td align="center"><%=GUARRoute%></td>
            <td align="center"><%=GUARChecker%></td> 
            <td align="center"><%=GUARRepair%></td> 
            <td align="center"><%=GUARAuthorise%></td>
            <td align="center"><%=GUARProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>



      <tr>
            <td><%=InstrumentsSLC%></td>
            <td align="center"><%=SLCCreateMod%></td>
            <td align="center"><%=SLCDelete%></td>
            <td align="center"><%=SLCRoute%></td>
            <td align="center"><%=SLCChecker%></td> 
            <td align="center"><%=SLCRepair%></td> 
            <td align="center"><%=SLCAuthorise%></td>
            <td align="center"><%=SLCProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=instrumentsDP%></td>
            <td align="center"><%=DPCreateMod%></td>
            <td align="center"><%=DPDelete%></td>
            <td align="center"><%=DPRoute%></td>
            <td align="center"><%=DPChecker%></td> 
            <td align="center"><%=DPRepair%></td> 
            <td align="center"><%=DPAuthorise%></td>
            <td align="center"><%=DPProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td align="center"><%=DPUploadBulkFile%></td>
            <td>&nbsp;</td>
      </tr>
      <tr>
            <td><%=InstrumentsRA%></td>
            <td align="center"><%=RACreateMod%></td>
            <td align="center"><%=RADelete%></td>
            <td align="center"><%=RARoute%></td>
            <td align="center"><%=RAChecker%></td> 
            <td align="center"><%=RARepair%></td> 
            <td align="center"><%=RAAuthorise%></td>
            <td align="center"><%=RAProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=InstrumentsSG%></td>
            <td align="center"><%=SGCreateMod%></td>
            <td align="center"><%=SGDelete%></td>
            <td align="center"><%=SGRoute%></td>
            <td align="center"><%=SGChecker%></td> 
            <td align="center"><%=SGRepair%></td> 
            <td align="center"><%=SGAuthorise%></td>
            <td align="center"><%=SGProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr>
            <td><%=InstrumentsTF%></td>
            <td align="center"><%=TFCreateMod%></td>
            <td align="center"><%=TFDelete%></td>
            <td align="center"><%=TFRoute%></td>
            <td align="center"><%=TFChecker%></td> 
            <td align="center"><%=TFRepair%></td> 
            <td align="center"><%=TFAuthorise%></td>
            <td align="center"><%=TFProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <tr class="tableSubHeader">
            <td colspan="11"><%=resMgr.getText("SecurityProfileDetail.Transactions", TradePortalConstants.TEXT_BUNDLE)%></td>
      </tr>
      <tr>
            <td><%=DiscRespTransaction%></td>
            <td align="center"><%=DRCreateMod%></td>
            <td align="center"><%=DRDelete%></td>
            <td align="center"><%=DRRoute%></td>
            <td align="center"><%=DRChecker%></td> 
            <td align="center"><%=DRRepair%></td> 
            <td align="center"><%=DRAuthorise%></td>
            <td align="center"><%=DRProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
      <%-- SSikhakolli - Rel-9.4 CR-818 --%>
      <tr>
            <td><%=SettlementInstructionTransaction%></td>
            <td align="center"><%=SICreateMod%></td>
            <td align="center"><%=SIDelete%></td>
            <td align="center"><%=SIRoute%></td>
            <td align="center"><%=SIChecker%></td> 
            <td align="center"><%=SIRepair%></td> 
            <td align="center"><%=SIAuthorise%></td>
            <td align="center"><%=SIProxyAuthorise%></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>
</table>
<br>
<%=widgetFactory.createCheckboxField("TransactionAttachDocument","SecurityProfileDetail.AttachDocument",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.ATTACH_DOC_TRANSACTION),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","")%>

<%=widgetFactory.createCheckboxField("TransactionDeleteDocument","SecurityProfileDetail.DeleteDocument",SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.DELETE_DOC_TRANSACTION),isReadOnly,false,"onClick=\"pickInstrumentAccess();\"","","")%>
</div>




<%-- 4. ReceivablesPayablesManagement starts here --%> <%= widgetFactory.createSectionHeader( "4","SecurityProfileDetail.4ReceivablesPayablesManagement") %>
<div>
<%=widgetFactory.createCheckboxField("AccessReceivables","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
               SecurityAccess.ACCESS_RECEIVABLES_AREA),isReadOnly,false,"onClick=\"deselectReceivablesRights();\"","","")%>
</div>
<div class="formItem">
<%=widgetFactory.createCheckboxField("ViewChildOrgReceivables","SecurityProfileDetail.InstrumentsWorkSubsidiaires",SecurityAccess.hasRights(securityProfileRights,
          SecurityAccess.VIEW_CHILD_ORG_RECEIVABLES),isReadOnly,false,"onClick=\"pickReceivablesAccess();\" labelClass=\"formWidth\"","","")%>
</div>

<%
String recCol1 = resMgr.getText("SecurityProfileDetail.ReceivablesMatch", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesMatch", false, false, false, "");
String recCol2 = resMgr.getText("SecurityProfileDetail.ReceivablesRoute", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesRoute", false, false, false, "");
String recCol3 = resMgr.getText("SecurityProfileDetail.ReceivablesAuthorize", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesAuthorize", false, false, false, "");
String recCol4 = resMgr.getText("SecurityProfileDetail.AuthoriseOffline", TradePortalConstants.TEXT_BUNDLE);
String recCol5 = resMgr.getText("SecurityProfileDetail.ReceivablesApproveDiscountAuthorize", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesApproveDiscountAuthorize", false, false, false, "");
String recCol6 = resMgr.getText("SecurityProfileDetail.ReceivablesApproveDiscountAuthorizeOffline", TradePortalConstants.TEXT_BUNDLE);
String recCol7 = resMgr.getText("SecurityProfileDetail.ReceivablesAddBuyerToPayment", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesAddBuyerToPayment", false, false, false, "");



//Approval to Pay Row1 -
String ReceivablesMgmtMatchNotice = resMgr.getText("SecurityProfileDetail.ReceivablesMgmtMatchNotice", TradePortalConstants.TEXT_BUNDLE);


String ReceivablesMatch = widgetFactory.createCheckboxField("ReceivablesMatch","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_MATCH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesRoute = widgetFactory.createCheckboxField("ReceivablesRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_ROUTE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesAuthorize = widgetFactory.createCheckboxField("ReceivablesAuthorize","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_AUTHORIZE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesProxyAuthorize = widgetFactory.createCheckboxField("ReceivablesProxyAuthorize","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_RECEIVABLES_AUTHORIZE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesApproveDiscountAuthorize = widgetFactory.createCheckboxField("ReceivablesApproveDiscountAuthorize","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_APPROVE_DISCOUNT_AUTHORIZE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesApproveDiscountProxyAuthorize = widgetFactory.createCheckboxField("ReceivablesApproveDiscountProxyAuthorize","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_PROXY_APPROVE_DISCOUNT_AUTHORIZE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesAddBuyerToPayment = widgetFactory.createCheckboxField("ReceivablesAddBuyerToPayment","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_ADD_BUYER_TO_PAYMENT),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

%>

<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="37%">&nbsp;</th>
            <th width="7%"><%=recCol1%></th>
            <th width="6%"><%=recCol2%></th>
            <th width="10%"><%=recCol3%></th>
            <th width="10%"><%=recCol4%></th>
            <th width="10%"><%=recCol5%></th>
            <th width="10%"><%=recCol6%></th>
            <th width="10%"><%=recCol7%></th>
      </tr>
      </thead>
      

      <tr align="left">
            <th colspan="8" style="text-align: left;"><%=widgetFactory.createRadioButtonField("SelectAllReceivables", "", "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllReceivables();\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; <%=widgetFactory.createRadioButtonField("SelectAllReceivables", "",      "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllReceivables();\"","")%>
            </th>
      </tr>

      <tr>
            <td ><%=ReceivablesMgmtMatchNotice%></td>
            <td align="center"><%=ReceivablesMatch%></td>
            <td align="center"><%=ReceivablesRoute%></td>
            <td align="center"><%=ReceivablesAuthorize%></td>
            <td align="center"><%=ReceivablesProxyAuthorize%></td>
            <td align="center"><%=ReceivablesApproveDiscountAuthorize%></td>
            <td align="center"><%=ReceivablesApproveDiscountProxyAuthorize%></td>
            <td align="center"><%=ReceivablesAddBuyerToPayment%></td>
      </tr>

</table>
<br>

<%
String recInCol1 = resMgr.getText("SecurityProfileDetail.ReceivablesDisputeUndispute", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesDisputeUndispute", false, false, false, "");
String recInCol2 = resMgr.getText("SecurityProfileDetail.ReceivablesClose", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesClose", false, false, false, "");
String recInCol3 = resMgr.getText("SecurityProfileDetail.ReceivablesFinance", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ReceivablesFinance", false, false, false, "");

//ReceivablesInvoices
String ReceivablesInvoices = resMgr.getText("SecurityProfileDetail.ReceivablesInvoices", TradePortalConstants.TEXT_BUNDLE); 


String ReceivablesDisputeUndispute = widgetFactory.createCheckboxField("ReceivablesDisputeUndispute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_DISPUTE_UNDISPUTE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesClose = widgetFactory.createCheckboxField("ReceivablesClose","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_CLOSE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String ReceivablesFinance = widgetFactory.createCheckboxField("ReceivablesFinance","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_FINANCE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
%>


<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="37%">&nbsp;</th>
            <th width="22%"><%=recInCol1%></th>
            <th width="21%"><%=recInCol2%></th>
            <th width="20%"><%=recInCol3%></th>
      </tr>
      </thead>
      <tr align="left">
            <th colspan="4" style="text-align: left;"><%=widgetFactory.createRadioButtonField("SelectAllInvoice", "", 
            "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllInvoice1();\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; <%=widgetFactory.createRadioButtonField("SelectAllInvoice", "", "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllInvoice1();\"","")%>
            </th>
      </tr>

      
      <tr>
            <td><%=ReceivablesInvoices%></td>
            <td align="center"><%=ReceivablesDisputeUndispute%></td>
            <td align="center"><%=ReceivablesClose%></td>
            <td align="center"><%=ReceivablesFinance%></td>

      </tr>
</table>
<br>
<%
	//Rel 9.0 CR 913 - New section Starts
String payInCol1 = resMgr.getText("SecurityProfileDetail.Authorise", TradePortalConstants.TEXT_BUNDLE);
String payInCol2 = resMgr.getText("SecurityProfileDetail.AuthoriseOffline", TradePortalConstants.TEXT_BUNDLE);
String payInCol3 = resMgr.getText("SecurityProfileDetail.ApproveForPayment", TradePortalConstants.TEXT_BUNDLE);
String payInCol4 = resMgr.getText("SecurityProfileDetail.PayClearPaymentDate", TradePortalConstants.TEXT_BUNDLE);
String payInCol5 = resMgr.getText("SecurityProfileDetail.ApplyEarlyPayAmt", TradePortalConstants.TEXT_BUNDLE);
String payInCol6 = resMgr.getText("SecurityProfileDetail.ResetEarlyPayAmt", TradePortalConstants.TEXT_BUNDLE);
String payInCol7 = resMgr.getText("SecurityProfileDetail.ApplySendtoSuppDate", TradePortalConstants.TEXT_BUNDLE);
String payInCol8 = resMgr.getText("SecurityProfileDetail.ResetSendtoSuppDate", TradePortalConstants.TEXT_BUNDLE);

//PayableInvoices
String PayablesInvoices = resMgr.getText("SecurityProfileDetail.PayableInvoices", TradePortalConstants.TEXT_BUNDLE); 


String PayablesAuthorise = widgetFactory.createCheckboxField("PayablesAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayablesAuthoriseOffline = widgetFactory.createCheckboxField("PayablesAuthoriseOffline","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_OFFLINE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayablesApprovePayment = widgetFactory.createCheckboxField("PayablesApprovePayment","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_APPLY_PAYMENT_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayablesClearPaymentDate = widgetFactory.createCheckboxField("PayablesClearPaymentDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_CLEAR_PAYMENT_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayablesApplyEarlyPaymentAmt = widgetFactory.createCheckboxField("PayablesApplyEarlyPaymentAmt","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_APPLY_EARLY_PAY_AMOUNT),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayablesResetEarlyPaymentAmt = widgetFactory.createCheckboxField("PayablesResetEarlyPaymentAmt","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_RESET_EARLY_PAY_AMOUNT),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayablesApplySendtoSupplierDate = widgetFactory.createCheckboxField("PayablesApplySendtoSupplierDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_APPLY_SUPPLIER_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayablesResetSendtoSupplierDate = widgetFactory.createCheckboxField("PayablesResetSendtoSupplierDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_MGM_RESET_SUPPLIER_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
%>


<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="37%">&nbsp;</th>
            <th width="10%"><%=payInCol1%></th>
            <th width="10%"><%=payInCol2%></th>
            <th width="10%"><%=payInCol3%></th>
            <th width="10%"><%=payInCol4%></th>
            <th width="10%"><%=payInCol5%></th>
            <th width="10%"><%=payInCol6%></th>
            <th width="10%"><%=payInCol7%></th>
            <th width="10%"><%=payInCol8%></th>
      </tr>
      </thead>
      <tr align="left">
            <th colspan="9" style="text-align: left;"><%=widgetFactory.createRadioButtonField("SelectAllPayInvoice", "", 
            "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllPayInvoice();\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; <%=widgetFactory.createRadioButtonField("SelectAllPayInvoice", "", "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllPayInvoice();\"","")%>
            </th>
      </tr>

      
      <tr>
            <td><%=PayablesInvoices%></td>
            <td align="center"><%=PayablesAuthorise%></td>
            <td align="center"><%=PayablesAuthoriseOffline%></td>
            <td align="center"><%=PayablesApprovePayment%></td>
            <td align="center"><%=PayablesClearPaymentDate%></td>
            <td align="center"><%=PayablesApplyEarlyPaymentAmt%></td>
            <td align="center"><%=PayablesResetEarlyPaymentAmt%></td>
            <td align="center"><%=PayablesApplySendtoSupplierDate%></td>
            <td align="center"><%=PayablesResetSendtoSupplierDate%></td>

      </tr>
</table>
<br>
<%--Rel 9.0 CR 913 - New section Ends --%>
<%
String InvCol1 = resMgr.getText("SecurityProfileDetail.ApproveFinance", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ApproveFinance", false, false, false, "");
String InvCol2 = resMgr.getText("SecurityProfileDetail.Authorise", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.Authorise", false, false, false, "");
String InvCol3 = resMgr.getText("SecurityProfileDetail.AuthoriseOffline", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.OfflineAuthorise", false, false, false, "");
String InvCol4 = resMgr.getText("SecurityProfileDetail.RemoveFromGroup", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.RemoveFromGroup", false, false, false, "");
String InvCol5 = resMgr.getText("SecurityProfileDetail.Delete", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.Delete", false, false, false, "");

// DK CR-709 Rel8.2 Begins
String InvCol6 = resMgr.getText("SecurityProfileDetail.RecAssignInstrType", TradePortalConstants.TEXT_BUNDLE); 
String InvCol7 = resMgr.getText("SecurityProfileDetail.RecAssignLoanType", TradePortalConstants.TEXT_BUNDLE);
String InvCol8 = resMgr.getText("SecurityProfileDetail.RecCreateLoanReq", TradePortalConstants.TEXT_BUNDLE);
String InvCol9 = resMgr.getText("SecurityProfileDetail.RecApplyPaymentDate", TradePortalConstants.TEXT_BUNDLE);
String InvCol10 = resMgr.getText("SecurityProfileDetail.RecClearPaymentDate", TradePortalConstants.TEXT_BUNDLE);
//DK CR-709 Rel8.2 Ends

String InvCol11 = resMgr.getText("SecurityProfileDetail.DeclineInvoice", TradePortalConstants.TEXT_BUNDLE);//#RKAZI CR1006 04-24-2015  - Add  Decline
//ReceivablesInvoices
String uploadInvoiceMgmt = resMgr.getText("SecurityProfileDetail.uploadInvRecMgmt", TradePortalConstants.TEXT_BUNDLE);


String invApproveFinancing = widgetFactory.createCheckboxField("ApproveFinancing","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_APPROVE_FINANCING),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAuthorise = widgetFactory.createCheckboxField("AuthorizeInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_AUTHORIZE_INVOICE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invOfflineAuthorise = widgetFactory.createCheckboxField("ProxyAuthorizeInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_RECEIVABLES_AUTHORIZE_INVOICE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invRemoveFromGroup = widgetFactory.createCheckboxField("RemovableInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_REMOVE_INVOICE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invDelete = widgetFactory.createCheckboxField("DeleteInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_DELETE_INVOICE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

//DK CR-709 Rel8.2 Begins
String recApplyPaymentDate = widgetFactory.createCheckboxField("ReceivablesApplyPaymentDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_APPLY_PAYMENT_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String recClearPaymentDate = widgetFactory.createCheckboxField("ReceivablesClearPaymentDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_CLEAR_PAYMENT_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String recAssignInstrType = widgetFactory.createCheckboxField("ReceivablesAssignInstrType","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_ASSIGN_INSTR_TYPE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String recAssignLoanType = widgetFactory.createCheckboxField("ReceivablesAssignLoanType","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_ASSIGN_LOAN_TYPE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String recCreateLoanReq = widgetFactory.createCheckboxField("ReceivablesCreateLoanReq","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.RECEIVABLES_CREATE_LOAN_REQ),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
//DK CR-709 Rel8.2 Ends

String recDeclineInvoice = widgetFactory.createCheckboxField("ReceivablesDeclineInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.REC_INVOICE_DECLINE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none"); //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice
%>

<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="37%">&nbsp;</th>
            <th width="14%"><%=InvCol1 %></th>
            <th width="14%"><%=InvCol2 %></th>
            <th width="13%"><%=InvCol3 %></th>
            <th width="11%"><%=InvCol4 %></th>
            <th width="11%"><%=InvCol5 %></th>
            <%--DK CR-709 Rel8.2 Begins --%>
			<%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes - BEGIN --%>
            <th width="11%"><%=InvCol6 %></th>
            <th width="11%"><%=InvCol7 %></th>
      </tr>
      </thead>
		<%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes END --%>
      <tr align="left">
            <th colspan="11" style="text-align: left;">
            <%=widgetFactory.createRadioButtonField("SelectAllUploadInvoice", "", 
                     "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllUploadInvoice();\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; 
            <%=widgetFactory.createRadioButtonField("SelectAllUploadInvoice", "", 
                        "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllUploadInvoice();\"","")%>
            </th>
      </tr>
	  <%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes - BEGIN --%>
      <tr>
            <td rowspan="3"><%=uploadInvoiceMgmt%></td>
            <td align="center"><%=invApproveFinancing%></td>
            <td align="center"><%=invAuthorise%></td>
            <td align="center"><%=invOfflineAuthorise%></td>
            <td align="center"><%=invRemoveFromGroup%></td>
            <td align="center"><%=invDelete%></td>
            <td align="center"><%=recAssignInstrType%></td>
            <td align="center"><%=recAssignLoanType%></td>
      
      </tr>
      <tr>
        <th width="11%"><%=InvCol8 %></th>
        <th width="11%"><%=InvCol9 %></th>
        <th width="11%"><%=InvCol10 %></th>
        <%--DK CR-709 Rel8.2 Ends --%>
        <th width="11%"><%=InvCol11 %></th>
        <th width="56%">&nbsp;</th>
        <th width="56%">&nbsp;</th>
        <th width="56%">&nbsp;</th>
      </tr>
      <tr>
            <%--DK CR-709 Rel8.2 Begins --%>
	    <td align="center"><%=recCreateLoanReq%></td>
	    <td align="center"><%=recApplyPaymentDate%></td>
	    <td align="center"><%=recClearPaymentDate%></td>
	    <%--DK CR-709 Rel8.2 Ends --%>
        <td align="center"><%=recDeclineInvoice%></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
	<%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes - END --%>
		</tr> 
</table>
<br>

<%
String InvAtpCol1 = resMgr.getText("SecurityProfileDetail.Authorise", TradePortalConstants.TEXT_BUNDLE);//Rel 9.0 CR 913 Added
String InvAtpCol2 = resMgr.getText("SecurityProfileDetail.AuthoriseOffline", TradePortalConstants.TEXT_BUNDLE);//Rel 9.0 CR 913 Added
String InvAtpCol3 = resMgr.getText("SecurityProfileDetail.Delete", TradePortalConstants.TEXT_BUNDLE);
String InvAtpCol4 = resMgr.getText("SecurityProfileDetail.PayAssignInstrType", TradePortalConstants.TEXT_BUNDLE); // DK CR-709 Rel8.2 Added
String InvAtpCol5 = resMgr.getText("SecurityProfileDetail.PayCreateLoanReq", TradePortalConstants.TEXT_BUNDLE); // DK CR-709 Rel8.2 Added
String InvAtpCol6 = resMgr.getText("SecurityProfileDetail.PayAssignLoanType", TradePortalConstants.TEXT_BUNDLE); // DK CR-709 Rel8.2 Added
String InvAtpCol7 = resMgr.getText("SecurityProfileDetail.CreateATP", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.Authorise", false, false, false, "");
String InvAtpCol8 = resMgr.getText("SecurityProfileDetail.ApproveForPayment", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.ApproveFinance", false, false, false, "");
String InvAtpCol9 = resMgr.getText("SecurityProfileDetail.PayClearPaymentDate", TradePortalConstants.TEXT_BUNDLE); // DK CR-709 Rel8.2 Added
String InvAtpCol10 = resMgr.getText("SecurityProfileDetail.ApplyEarlyPayAmt", TradePortalConstants.TEXT_BUNDLE);//Rel 9.0 CR 913 Added
String InvAtpCol11 = resMgr.getText("SecurityProfileDetail.ResetEarlyPayAmt", TradePortalConstants.TEXT_BUNDLE);//Rel 9.0 CR 913 Added
String InvAtpCol12 = resMgr.getText("SecurityProfileDetail.ApplySendtoSuppDate", TradePortalConstants.TEXT_BUNDLE);//Rel 9.0 CR 913 Added
String InvAtpCol13 = resMgr.getText("SecurityProfileDetail.ResetSendtoSuppDate", TradePortalConstants.TEXT_BUNDLE);//Rel 9.0 CR 913 Added
String InvAtpCol14 = resMgr.getText("SecurityProfileDetail.DeclineInvoice", TradePortalConstants.TEXT_BUNDLE);//#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes
 

//Receivables ATP Invoices
String uploadInvoiceAtpMgmt = resMgr.getText("SecurityProfileDetail.uploadInvAtpRecMgmt", TradePortalConstants.TEXT_BUNDLE);


String invAtpApplyPayDt = widgetFactory.createCheckboxField("ApplyPaymentDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAYABLES_APPLY_PAYDATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAtpCreateAuth = widgetFactory.createCheckboxField("CreateATP","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAYABLES_CREATE_ATP),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAtpDelete = widgetFactory.createCheckboxField("PayableDeleteInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAYABLES_DELETE_INVOICE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

//DK CR-709 Rel8.2 Begins
String invATPClearPaymentDate = widgetFactory.createCheckboxField("PayClearPaymentDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CLEAR_PAYMENT_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invATPAssignInstrType = widgetFactory.createCheckboxField("PayAssignInstrType","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_ASSIGN_INSTR_TYPE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invATPAssignLoanType = widgetFactory.createCheckboxField("PayAssignLoanType","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_ASSIGN_LOAN_TYPE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invATPCreateLoanReq = widgetFactory.createCheckboxField("PayCreateLoanReq","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREATE_LOAN_REQ),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
//DK CR-709 Rel8.2 Ends

//Rel 9.0 CR 913 Start
String invAtpAuthorise = widgetFactory.createCheckboxField("PayAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.UPLOAD_PAY_MGM_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAtpAuthoriseOffline = widgetFactory.createCheckboxField("PayAuthoriseOffline","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.UPLOAD_PAY_MGM_OFFLINE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAtpApplyEarlyPaymentAmt = widgetFactory.createCheckboxField("PayApplyEarlyPaymentAmt","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.UPLOAD_PAY_MGM_APPLY_EARLY_PAY_AMOUNT),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAtpResetEarlyPaymentAmt = widgetFactory.createCheckboxField("PayResetEarlyPaymentAmt","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.UPLOAD_PAY_MGM_RESET_EARLY_PAY_AMOUNT),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAtpApplySendtoSupplierDate = widgetFactory.createCheckboxField("PayApplySendtoSupplierDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.UPLOAD_PAY_MGM_APPLY_SUPPLIER_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String invAtpResetSendtoSupplierDate = widgetFactory.createCheckboxField("PayResetSendtoSupplierDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.UPLOAD_PAY_MGM_RESET_SUPPLIER_DATE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");


String PayDeclineInvoice = widgetFactory.createCheckboxField("PayDeclineInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_INVOICE_DECLINE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none"); //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes 


//Rel 9.0 CR 913 End
%>

<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="37%">&nbsp;</th>
            <th width="23%"><%=InvAtpCol1%></th>
            <th width="20%"><%=InvAtpCol2%></th>
            <th width="20%"><%=InvAtpCol3%></th>
            <th width="23%"><%=InvAtpCol4%></th>
            <th width="20%"><%=InvAtpCol5%></th>
            <th width="20%"><%=InvAtpCol6%></th>
            <th width="20%"><%=InvAtpCol7%></th>
            
      </tr>
      </thead>
      <tr align="left">
            <th colspan="14" style="text-align: left;">
            <%=widgetFactory.createRadioButtonField("SelectAllATPInvoice", "", 
                     "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllATPInvoice();\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; 
            <%=widgetFactory.createRadioButtonField("SelectAllATPInvoice", "", 
                        "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllATPInvoice();\"","")%>
            </th>
      </tr>

      <tr>
            <td rowspan="3" valign="top"><%=uploadInvoiceAtpMgmt%></td>
            <td align="center"><%=invAtpAuthorise%></td>
            <td align="center"><%=invAtpAuthoriseOffline%></td>
            <td align="center"><%=invAtpDelete%></td>     
            <td align="center"><%=invATPAssignInstrType%></td>  
            <td align="center"><%=invATPCreateLoanReq%></td> 
            <td align="center"><%=invATPAssignLoanType%></td>
            <td align="center"><%=invAtpCreateAuth%></td>
            
      </tr> 
      <tr>
            <th width="23%"><%=InvAtpCol8%></th>
            <th width="20%"><%=InvAtpCol9%></th>
            <th width="20%"><%=InvAtpCol10%></th>
            <th width="23%"><%=InvAtpCol11%></th>
            <th width="20%"><%=InvAtpCol12%></th>
            <th width="20%"><%=InvAtpCol13%></th>
            <th width="20%"><%=InvAtpCol14%></th> <%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes --%>
            
      </tr>
      <tr>
            <td align="center"><%=invAtpApplyPayDt%></td>
            <td align="center"><%=invATPClearPaymentDate%></td>
            <td align="center"><%=invAtpApplyEarlyPaymentAmt%></td>
            <td align="center"><%=invAtpResetEarlyPaymentAmt%></td>
            <td align="center"><%=invAtpApplySendtoSupplierDate%></td>
            <td align="center"><%=invAtpResetSendtoSupplierDate%></td>
            <td align="center"><%=PayDeclineInvoice%></td> <%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Invoice Changes  --%>
            
      </tr>
      
</table>
            <br>


 <%// -----------------R9.2 CR914  ------------------------ %>

<%
	String paybleCreditNotes = resMgr.getText("SecurityProfileDetail.paybleCreditNotes", TradePortalConstants.TEXT_BUNDLE); 
String PayCrNoteCol1 = resMgr.getText("SecurityProfileDetail.Authorise", TradePortalConstants.TEXT_BUNDLE); 
String PayCrNoteCol2 = resMgr.getText("SecurityProfileDetail.AuthoriseOffline", TradePortalConstants.TEXT_BUNDLE); 
String PayCrNoteCol3 = resMgr.getText("SecurityProfileDetail.Delete", TradePortalConstants.TEXT_BUNDLE);
String PayCrNoteCol31 = resMgr.getText("SecurityProfileDetail.paybleCreditNotesClose", TradePortalConstants.TEXT_BUNDLE);
String PayCrNoteCol4 = resMgr.getText("SecurityProfileDetail.applyManualPayableCrNote", TradePortalConstants.TEXT_BUNDLE);
String PayCrNoteCol5 = resMgr.getText("SecurityProfileDetail.unApplyManualPayableCrNote", TradePortalConstants.TEXT_BUNDLE);
String PayCrNoteCol6 = resMgr.getText("SecurityProfileDetail.PayablesDeclineCreditNote", TradePortalConstants.TEXT_BUNDLE); //#RKAZI CR1006 04-24-2015  - Add  Decline Credit Notes Changes
String PayCrNoteCol7 = resMgr.getText("SecurityProfileDetail.PayablesApproveCreditNote", TradePortalConstants.TEXT_BUNDLE); //#PGedupudi CR1006 05-14-2015  - Add  Approve CreditNote


String PayCrNoteAuthorize = widgetFactory.createCheckboxField("PayCrNoteAuthorize","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayCrNoteAuthorizeOffline = widgetFactory.createCheckboxField("PayCrNoteAuthorizeOffline","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_OFFLINE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayCrNoteDelete = widgetFactory.createCheckboxField("PayCrNoteDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_DELETE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayCrNoteClose = widgetFactory.createCheckboxField("PayCrNoteClose","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_CLOSE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayCrNoteApplyManual = widgetFactory.createCheckboxField("PayCrNoteApplyManual","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_MANUAL_APPLY),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

String PayCrNoteUnApplyManual = widgetFactory.createCheckboxField("PayCrNoteUnApplyManual","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_MANUAL_UNAPPLY),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");

//Rel 9.0 CR 913 End
String PayablesDeclineCreditNote = widgetFactory.createCheckboxField("PayablesDeclineCreditNote","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_DECLINE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none"); //#RKAZI CR1006 04-24-2015  - Add  Decline Credit Notes Changes 

String PayablesApproveCreditNote = widgetFactory.createCheckboxField("PayablesApproveCreditNote","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PAY_CREDIT_NOTE_APPROVE_AUTH),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none"); //#PGedupudi CR1006 05-14-2015  - Add  Approve CreditNote 
%>


  
<table width="100%" class="formDocumentsTable">

      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="37%">&nbsp;</th>
            <th width="17%"><%=PayCrNoteCol1%></th>
            <th width="17%"><%=PayCrNoteCol2%></th>
            <th width="17%"><%=PayCrNoteCol3%></th>
            <th width="17%"><%=PayCrNoteCol31%></th>
            <th width="17%"><%=PayCrNoteCol4%></th>
            <th width="17%"><%=PayCrNoteCol5%></th>
            <th width="17%"><%=PayCrNoteCol7%></th> <%-- //#PGedupudi CR1006 05-14-2015  - Add  Approve CreditNote --%>
            <th width="17%"><%=PayCrNoteCol6%></th> <%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Credit Notes Changes --%>
           
      </tr>
      </thead>
      <tr align="left">
            <th colspan="14" style="text-align: left;">
            <%=widgetFactory.createRadioButtonField("SelectAllCrNotes", "", 
                     "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllCreditNoteOptions(true);\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; 
            <%=widgetFactory.createRadioButtonField("SelectAllCrNotes", "", 
                        "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllCreditNoteOptions(false);\"","")%>
            </th>
      </tr>

      <tr>
            <td rowspan="3" valign="top"><%=paybleCreditNotes%></td>
            <td align="center"><%=PayCrNoteAuthorize%></td>
            <td align="center"><%=PayCrNoteAuthorizeOffline%></td>
            <td align="center"><%=PayCrNoteDelete%></td>   
            <td align="center"> <%=PayCrNoteClose%> </td>  
            <td align="center"><%=PayCrNoteApplyManual%></td>  
            <td align="center"><%=PayCrNoteUnApplyManual%></td> 
            <td align="center"><%=PayablesApproveCreditNote%></td> <%-- //#PGedupudi CR1006 05-14-2015  - Add  Approve CreditNote  --%>
            <td align="center"><%=PayablesDeclineCreditNote%></td> <%-- //#RKAZI CR1006 04-24-2015  - Add  Decline Credit Notes Changes  --%>
         
      </tr> 
      
</table>
            <br>

<%
String SpCol1 = resMgr.getText("SecurityProfileDetail.SpInvoicesOffered", TradePortalConstants.TEXT_BUNDLE);
String SpCol2 = resMgr.getText("SecurityProfileDetail.SpHistory", TradePortalConstants.TEXT_BUNDLE);
String SpCol3 = resMgr.getText("SecurityProfileDetail.SpAcceptOffer", TradePortalConstants.TEXT_BUNDLE);
String SpCol4 = resMgr.getText("SecurityProfileDetail.SpAuthoriseOffer", TradePortalConstants.TEXT_BUNDLE);
String SpCol5 = resMgr.getText("SecurityProfileDetail.SpDeclineOffer", TradePortalConstants.TEXT_BUNDLE);
String SpCol6 = resMgr.getText("SecurityProfileDetail.SpAssignFutureValueDate", TradePortalConstants.TEXT_BUNDLE);
String SpCol7 = resMgr.getText("SecurityProfileDetail.SpRemoveFutureValueDate", TradePortalConstants.TEXT_BUNDLE);
String SpCol8 = resMgr.getText("SecurityProfileDetail.SpRemoveInvoice", TradePortalConstants.TEXT_BUNDLE);
String SpCol9 = resMgr.getText("SecurityProfileDetail.SpResetToOffered", TradePortalConstants.TEXT_BUNDLE);

//Receivables Supplier Portal
String supplierPortalLbl = resMgr.getText("SecurityProfileDetail.SupplierPortal", TradePortalConstants.TEXT_BUNDLE);

String spInvoicesOffered = widgetFactory.createCheckboxField("SpInvoicesOffered","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_INVOICE_OFFERED),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spHistory = widgetFactory.createCheckboxField("SpHistory","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_HISTORY),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spAcceptOffer = widgetFactory.createCheckboxField("SpAcceptOffer","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_ACCEPT_OFFER),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spAuthoriseOffer = widgetFactory.createCheckboxField("SpAuthoriseOffer","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_AUTHORIZE_OFFER),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spDeclineOffer = widgetFactory.createCheckboxField("SpDeclineOffer","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_DECLINE_OFFER),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spAssignFutureValueDate = widgetFactory.createCheckboxField("SpAssignFutureValueDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_ASSIGN_FVD),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spRemoveFutureValueDate = widgetFactory.createCheckboxField("SpRemoveFutureValueDate","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_REMOVE_FVD),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spRemoveInvoice = widgetFactory.createCheckboxField("SpRemoveInvoice","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_REMOVE_INVOICE),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
String spResetToOffered = widgetFactory.createCheckboxField("SpResetToOffered","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.SP_RESET_TO_OFFERED),isReadOnly,false,"onClick=\"pickReceivablesAccess();\"","","none");
%>

<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th width="37%">&nbsp;</th>
            <th width="23%"><%=SpCol1%></th>
            <th width="20%"><%=SpCol2%></th>
            <th width="20%"><%=SpCol3%></th>
            <th width="23%"><%=SpCol4%></th>
            <th width="20%"><%=SpCol5%></th>
            <th width="20%"><%=SpCol6%></th>
            <th width="23%"><%=SpCol7%></th>
            <th width="20%"><%=SpCol8%></th>
            <th width="20%"><%=SpCol9%></th>
      </tr>
      </thead>
      <tr align="left">
            <th colspan="10" style="text-align: left;">
            <%=widgetFactory.createRadioButtonField("SelectAllSupplierPortal", "", 
                     "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllSupplierPortal();\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; 
            <%=widgetFactory.createRadioButtonField("SelectAllSupplierPortal", "", 
                        "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllSupplierPortal();\"","")%>
            </th>
      </tr>

      <tr>
            <td><%=supplierPortalLbl%></td>
            <td align="center"><%=spInvoicesOffered%></td>
            <td align="center"><%=spHistory%></td>
            <td align="center"><%=spAcceptOffer%></td>
            <td align="center"><%=spAuthoriseOffer%></td>
            <td align="center"><%=spDeclineOffer%></td>
            <td align="center"><%=spAssignFutureValueDate%></td>
            <td align="center"><%=spRemoveFutureValueDate%></td>
            <td align="center"><%=spRemoveInvoice%></td>
            <td align="center"><%=spResetToOffered%></td>
      </tr> 
</table>

</div>
<%-- END  4. ReceivablesPayablesManagement  --%>

<%-- START  5. Direct Debits  --%>
<%= widgetFactory.createSectionHeader("5", "SecurityProfileDetail.5DDI")%>

<div>
<%=widgetFactory.createCheckboxField("AccessDirectDebit","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
		SecurityAccess.ACCESS_DIRECTDEBIT_AREA),isReadOnly,false,"","","")%>
</div>
<div class="formItem">
<%=widgetFactory.createCheckboxField("ViewChildOrgDirectDebit","SecurityProfileDetail.InstrumentsWorkSubsidiaires",SecurityAccess.hasRights(securityProfileRights,
		SecurityAccess.VIEW_CHILD_ORG_DIRECTDEBIT),isReadOnly,false,"labelClass=\"formWidth\"","","")%>
</div>

<%
//DirectDebits
String DirectDebits = resMgr.getText("SecurityProfileDetail.DirectDebits", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.DirectDebits", false, false, false, "");

String DDICreateMod = widgetFactory.createCheckboxField("DDICreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DDI_CREATE_MODIFY),isReadOnly,false,"","","none");

String DDIDelete = widgetFactory.createCheckboxField("DDIDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DDI_DELETE),isReadOnly,false,"","","none");

String DDIRoute = widgetFactory.createCheckboxField("DDIRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DDI_ROUTE),isReadOnly,false,"","","none");

String DDIAuthorise = widgetFactory.createCheckboxField("DDIAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DDI_AUTHORIZE),isReadOnly,false,"","","none");

String DDIUploadFile = widgetFactory.createCheckboxField("DDIUploadFile","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.DDI_UPLOAD_FILE),isReadOnly,false,"","","none");

String DDIProxyAuthorise = widgetFactory.createCheckboxField("DDIProxyAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.PROXY_DDI_AUTHORIZE),isReadOnly,false,"","","none");

%>
<table width="100%" class="formDocumentsTable">
      <thead>
      <tr>
            <th width="38%">&nbsp;</th>
            <th width="10%"><%=inCol1%></th>
            <th width="10%"><%=inCol2%></th>
            <th width="10%"><%=inCol3%></th>
            <th width="10%"><%=inCol4%></th>
            <th width="10%"><%=inCol5%></th>
            <th width="12%"><%=inCol7%></th>
     </tr>
    </thead>
	
      <tr >
            <th colspan="9" style="text-align: left;">
			<%=widgetFactory.createRadioButtonField("SelectAllDDI", "SelectDDIAll", 
            "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; <%=widgetFactory.createRadioButtonField("SelectAllDDI", "SelectDDINone", 
            "SecurityProfileDetail.secectNone","N", false, isReadOnly,"","")%> 
			</th>
      </tr>
      
      <tr>
            <td><%=DirectDebits%></td>
            <td align="center"><%=DDICreateMod%></td>
            <td align="center"><%=DDIDelete%></td>
            <td align="center"><%=DDIRoute%></td>
            <td align="center"><%=DDIAuthorise%></td>
            <td align="center"><%=DDIProxyAuthorise%></td>
            <td align="center"><%=DDIUploadFile%></td>
      </tr>
</table>
</div>

<%-- END  5. Direct Debits  --%>
<%-- START 6.Invoice MAnagement --%>
 <%= widgetFactory.createSectionHeader("6", "SecurityProfileDetail.6InvoiceMgmt")%>
 
 <%= widgetFactory.createCheckboxField("InvoiceMgtView","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
               SecurityAccess.ACCESS_INVOICE_MGT_AREA),isReadOnly,false,"onClick=\"deselectInvoiceMgmtRights(false);\"","","")%>
               
 <%

String invoiceMgmtCol1 = resMgr.getText("SecurityProfileDetail.InvoiceMgtupload", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InvoiceMgtupload", false, false, false, "");
String invoiceMgmtCol2 = resMgr.getText("SecurityProfileDetail.InvoiceMgtview", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InvoiceMgtview", false, false, false, "");
String invoiceMgmtCol3 = resMgr.getText("SecurityProfileDetail.InvoiceMgtRemoveFailed", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.InvoiceMgtRemoveFailed", false, false, false, "");


String invoiceMgmtUpload = widgetFactory.createCheckboxField("UploadInvoice","",SecurityAccess.hasRights(securityProfileRights,
       SecurityAccess.UPLOAD_INVOICES),isReadOnly,false,"onClick=\"pickInvoiceMgmtAccess();\"","","none");

String invoiceMgmtViewUpload = widgetFactory.createCheckboxField("ViewUploadedInvoice","",SecurityAccess.hasRights(securityProfileRights,
             SecurityAccess.VIEW_UPLOADED_INVOICES),isReadOnly,false,"onClick=\"pickInvoiceMgmtAccess();\"","","none");

String invoiceMgmtRemove = widgetFactory.createCheckboxField("RemoveUploadedFiles","",SecurityAccess.hasRights(securityProfileRights,
             SecurityAccess.REMOVE_UPLOADED_FILES),isReadOnly,false,"onClick=\"pickInvoiceMgmtAccess();\"","","none");


%>
               
 <table width="100%" class="formDocumentsTable">
 <thead>
  <tr>
    <th>&nbsp;</th>
      <th align="center"><%=invoiceMgmtCol1%></th>
      <th align="center"><%=invoiceMgmtCol2%></th>
      <th align="center"><%=invoiceMgmtCol3%></th>
  </tr>
  </thead>
  <tr align="left">
            <th colspan="4" style="text-align: left;"> <%-- Removed extra double qoutes - Komal ANZ Issue No. 31 --%> 
            <%=widgetFactory.createRadioButtonField("SelectAllInv", "", 
                        "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllUploadInvRights();\"","")%>
            &nbsp; &nbsp;&nbsp;&nbsp; 
            <%=widgetFactory.createRadioButtonField("SelectAllInv", "", 
                        "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllUploadInvRights();\"","")%>
            </th>
  </tr>
  <tr>
      <td><%=resMgr.getText("SecurityProfileDetail.InvoiceProcessing", TradePortalConstants.TEXT_BUNDLE)%> </td>
      <td align="center"><%=invoiceMgmtUpload%></td>
      <td align="center"><%=invoiceMgmtViewUpload%></td>
      <td align="center"><%=invoiceMgmtRemove%></td>
   </tr>
 </table>                         
 </div>

<%-- END 6.Invoice Management --%>

<%-- 7. Reports starts here --%>

<%= widgetFactory.createSectionHeader( "7","SecurityProfileDetail.7Reports") %>

<%= widgetFactory.createCheckboxField("AccessReports","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
               SecurityAccess.ACCESS_REPORTS_AREA),isReadOnly,false,"onClick=\"deselectReportRights();\"","","")%>
<%

String repCol1 = resMgr.getText("SecurityProfileDetail.CannotView", TradePortalConstants.TEXT_BUNDLE); //widgetFactory.createLabel("", "SecurityProfileDetail.CannotView", false, false, false, "");
String repCol2 = resMgr.getText("SecurityProfileDetail.CanViewOnly", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.CanViewOnly", false, false, false, "");
String repCol3 = resMgr.getText("SecurityProfileDetail.CanMaintain", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.CanMaintain", false, false, false, "");

//CustomReport -//
String CustomReport = resMgr.getText("SecurityProfileDetail.CustomReport", TradePortalConstants.TEXT_BUNDLE);

String MaintainCustomReport1 = widgetFactory.createRadioButtonField("MaintainCustomReport", "", 
            "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.VIEW_OR_MAINTAIN_CUSTOM_REPORT), isReadOnly,"onClick=\"pickReportsAccess();\"","");

String MaintainCustomReport2 = widgetFactory.createRadioButtonField("MaintainCustomReport", "", 
            "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.VIEW_CUSTOM_REPORT), isReadOnly,"onClick=\"pickReportsAccess();\"","");

String MaintainCustomReport3 = widgetFactory.createRadioButtonField("MaintainCustomReport", "", 
            "", MAINTAIN,SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.MAINTAIN_CUSTOM_REPORT), isReadOnly,"onClick=\"pickReportsAccess();\"","");



//StandardReport -
String StandardReport = resMgr.getText("SecurityProfileDetail.StandardReport", TradePortalConstants.TEXT_BUNDLE);



String MaintainStandardReport1 = widgetFactory.createRadioButtonField("MaintainStandardReport", "", 
            "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_STANDARD_REPORT), isReadOnly,"onClick=\"pickReportsAccess();\"","") ;

String MaintainStandardReport2 = widgetFactory.createRadioButtonField("MaintainStandardReport", "", 
            "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_STANDARD_REPORT), isReadOnly,"onClick=\"pickReportsAccess();\"","") ;

String MaintainStandardReport3 = widgetFactory.createRadioButtonField("MaintainStandardReport", "", 
            "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.MAINTAIN_STANDARD_REPORT), isReadOnly,"onClick=\"pickReportsAccess();\"","") ;
%>

<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr>
            <th>&nbsp;</th>
            <th align="center"><%=repCol1%></th>
            <th align="center"><%=repCol2%></th>
            <th align="center"><%=repCol3%></th>
      </tr>
      </thead>
      <tr>
            <td><%=CustomReport%></td>
            <td align="center"><%=MaintainCustomReport1%></td>
            <td align="center"><%=MaintainCustomReport2%></td>
            <td align="center"><%=MaintainCustomReport3%></td>

      </tr>
      <tr>
            <td><%=StandardReport%></td>
            <td align="center"><%=MaintainStandardReport1%></td>
            <td align="center"><%=MaintainStandardReport2%></td>
            <td align="center"><%=MaintainStandardReport3%></td>

      </tr>
</table>
</div>
<%-- 7. Reports ends here --%>

<%-- 8. Ref data starts here --%>
<%= widgetFactory.createSectionHeader( "8","SecurityProfileDetail.8RefData") %>
<%=widgetFactory.createCheckboxField("AccessRefData","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
               SecurityAccess.ACCESS_REFDATA_AREA),isReadOnly,false,"onClick=\"deselectRefDataRights();\"","","")%>
<%
//REF DATA COLUMNS HEADINGS
String refCol1 = resMgr.getText("SecurityProfileDetail.CannotView", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.CannotView", false, false, false, "");
String refCol2 = resMgr.getText("SecurityProfileDetail.CanViewOnly", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.CanViewOnly", false, false, false, "");
String refCol3 = resMgr.getText("SecurityProfileDetail.CanMaintain", TradePortalConstants.TEXT_BUNDLE);//widgetFactory.createLabel("", "SecurityProfileDetail.CanMaintain", false, false, false, "");


//M Eerupula 04/16/2013 Rel8.3 CR776
// CorpOrgProfile
String myOrgProfile = resMgr.getText("SecurityProfileDetail.OrgProfile", TradePortalConstants.TEXT_BUNDLE);


String MaintainOrgProfile1 = widgetFactory.createRadioButtonField("MaintainOrgProfile", "", 
        "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.VIEW_OR_MAINTAIN_ORG_PROFILE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

String MaintainOrgProfile2 = widgetFactory.createRadioButtonField("MaintainOrgProfile", "", 
        "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.VIEW_ORG_PROFILE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

String MaintainOrgProfile3 = widgetFactory.createRadioButtonField("MaintainOrgProfile", "", 
        "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
            SecurityAccess.MAINTAIN_ORG_PROFILE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
//FXRate -
String FXRate = resMgr.getText("SecurityProfileDetail.FXRate", TradePortalConstants.TEXT_BUNDLE);

String MaintainFXRate1 = widgetFactory.createRadioButtonField("MaintainFXRate", "", 
            "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.VIEW_OR_MAINTAIN_FX_RATE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

String MaintainFXRate2 = widgetFactory.createRadioButtonField("MaintainFXRate", "", 
            "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.VIEW_FX_RATE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

String MaintainFXRate3 = widgetFactory.createRadioButtonField("MaintainFXRate", "", 
            "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.MAINTAIN_FX_RATE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");


//InstTemplate -
String InstTemplate = resMgr.getText("SecurityProfileDetail.InstTemplate", TradePortalConstants.TEXT_BUNDLE);



String MaintainTemplate1 = widgetFactory.createRadioButtonField("MaintainTemplate", "", 
            "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_TEMPLATE), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

String MaintainTemplate2 = widgetFactory.createRadioButtonField("MaintainTemplate", "", 
            "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_TEMPLATE), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

String MaintainTemplate3 = widgetFactory.createRadioButtonField("MaintainTemplate", "", 
            "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.MAINTAIN_TEMPLATE), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;


String CreateFixedPaymentTemplate = widgetFactory.createCheckboxField("CreateFixedPaymentTemplate","SecurityProfileDetail.CreateFixedTemplate",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.CREATE_FIXED_PAYMENT_TEMPLATE),isReadOnly,false,"onClick=\"pickRefDataAccess();\"","","");
///=InputField.createCheckboxField("", "Y", "",        SecurityAccess.hasRights(securityProfileRights,                       SecurityAccess.),       "ListText", "", isReadOnly,        "onClick=\"();\"")

//String CreateFixedTemplate = widgetFactory.createLabel("", "SecurityProfileDetail.CreateFixedTemplate", false, false, false, "");

//NotificationRule -
String NotificationRule = resMgr.getText("SecurityProfileDetail.NotificationRule", TradePortalConstants.TEXT_BUNDLE);

String MaintainNotificationRule1 = widgetFactory.createRadioButtonField("MaintainNotificationRule", "", 
            "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.VIEW_OR_MAINTAIN_NOTIFICATION_RULE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

String MaintainNotificationRule2 = widgetFactory.createRadioButtonField("MaintainNotificationRule", "", 
            "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.VIEW_NOTIFICATION_RULE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

String MaintainNotificationRule3 = widgetFactory.createRadioButtonField("MaintainNotificationRule", "", 
            "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                SecurityAccess.MAINTAIN_NOTIFICATION_RULE), isReadOnly,"onClick=\"pickRefDataAccess();\"","");


%>
<%//Party -
      String Party = resMgr.getText("SecurityProfileDetail.Party", TradePortalConstants.TEXT_BUNDLE);
      
      String MaintainParty1 = widgetFactory.createRadioButtonField("MaintainParty", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_OR_MAINTAIN_PARTIES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainParty2 = widgetFactory.createRadioButtonField("MaintainParty", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_PARTIES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainParty3 = widgetFactory.createRadioButtonField("MaintainParty", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.MAINTAIN_PARTIES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String CreateNewTransactionParty = widgetFactory.createCheckboxField("CreateNewTransactionParty","SecurityProfileDetail.CreateNewParties",SecurityAccess.hasRights(securityProfileRights,
              SecurityAccess.CREATE_NEW_TRANSACTION_PARTY),isReadOnly,false,"onClick=\"pickRefDataAccess();\"","","");
      ///=InputField.createCheckboxField("", "Y", "",        SecurityAccess.hasRights(securityProfileRights,                       SecurityAccess.),       "ListText", "", isReadOnly,        "onClick=\"();\"")

      //String CreateNewTransactionPartyLab = widgetFactory.createLabel("", "SecurityProfileDetail.CreateNewParties", false, false, false, "");
      
      
      //Phrase -
      String Phrase = resMgr.getText("SecurityProfileDetail.Phrase", TradePortalConstants.TEXT_BUNDLE);
      


      String MaintainPhrase1 = widgetFactory.createRadioButtonField("MaintainPhrase", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_PHRASE), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainPhrase2 = widgetFactory.createRadioButtonField("MaintainPhrase", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_PHRASE), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainPhrase3 = widgetFactory.createRadioButtonField("MaintainPhrase", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.MAINTAIN_PHRASE), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;
      %>
<%//SecurityProfile -
      String SecurityProfile = resMgr.getText("SecurityProfileDetail.SecurityProfile", TradePortalConstants.TEXT_BUNDLE);
      
      String MaintainSecProf1 = widgetFactory.createRadioButtonField("MaintainSecProf", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_OR_MAINTAIN_SEC_PROF), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainSecProf2 = widgetFactory.createRadioButtonField("MaintainSecProf", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_SEC_PROF), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainSecProf3 = widgetFactory.createRadioButtonField("MaintainSecProf", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.MAINTAIN_SEC_PROF), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
      //ThreshGroup     
      String ThreshGroup = resMgr.getText("SecurityProfileDetail.ThreshGroup", TradePortalConstants.TEXT_BUNDLE);
      

      String MaintainThreshGrp1 = widgetFactory.createRadioButtonField("MaintainThreshGrp", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_THRESH_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainThreshGrp2 = widgetFactory.createRadioButtonField("MaintainThreshGrp", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_THRESH_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainThreshGrp3 = widgetFactory.createRadioButtonField("MaintainThreshGrp", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.MAINTAIN_THRESH_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;
      %>
<%//User -
      String User = resMgr.getText("SecurityProfileDetail.User", TradePortalConstants.TEXT_BUNDLE);
      
      String MaintainUser1 = widgetFactory.createRadioButtonField("MaintainUser", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_OR_MAINTAIN_USERS), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainUser2 = widgetFactory.createRadioButtonField("MaintainUser", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_USERS), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainUser3 = widgetFactory.createRadioButtonField("MaintainUser", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                                SecurityAccess.MAINTAIN_USERS), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      //WorkGroups      
      String WorkGroups = resMgr.getText("SecurityProfileDetail.WorkGroups", TradePortalConstants.TEXT_BUNDLE);
      

      String MaintainWorkGroups1 = widgetFactory.createRadioButtonField("MaintainWorkGroups", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_WORK_GROUPS), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainWorkGroups2 = widgetFactory.createRadioButtonField("MaintainWorkGroups", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_WORK_GROUPS), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainWorkGroups3 = widgetFactory.createRadioButtonField("MaintainWorkGroups", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.MAINTAIN_WORK_GROUPS), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;
      %>
<%//PanelAuthorizationGroups -
      String PanelAuthorizationGroups = resMgr.getText("SecurityProfileDetail.PanelAuthorizationGroups", TradePortalConstants.TEXT_BUNDLE);
      
      String MaintainPanelAuthGroup1 = widgetFactory.createRadioButtonField("MaintainPanelAuthGroup", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_OR_MAINTAIN_PANEL_AUTH_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainPanelAuthGroup2 = widgetFactory.createRadioButtonField("MaintainPanelAuthGroup", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_PANEL_AUTH_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainPanelAuthGroup3 = widgetFactory.createRadioButtonField("MaintainPanelAuthGroup", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                              SecurityAccess.MAINTAIN_PANEL_AUTH_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
	  
	  // Payment File Definition section
	  //Srinivasu_D CR-599 Rel8.4 11/21/2013 - Start 
	  String PaymentFileDefGroups = resMgr.getText("SecurityProfileDetail.RefDataPaymentDef", TradePortalConstants.TEXT_BUNDLE);
      
      String MaintainPaymentFileDef1 = widgetFactory.createRadioButtonField("MaintainPaymentFileDef", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_FILE_DEF), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainPaymentFileDef2 = widgetFactory.createRadioButtonField("MaintainPaymentFileDef", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_PAYMENT_FILE_DEF), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainPaymentFileDef3 = widgetFactory.createRadioButtonField("MaintainPaymentFileDef", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                              SecurityAccess.MAINTAIN_PAYMENT_FILE_DEF), isReadOnly,"onClick=\"pickRefDataAccess();\"","");   
       
	  // Srinivasu_D CR-599 Rel8.4 11/21/2013 - End
      //TemplateGroups  
      String TemplateGroups = resMgr.getText("SecurityProfileDetail.TemplateGroups", TradePortalConstants.TEXT_BUNDLE);
      

      String MaintainPaymentTemplateGroup1 = widgetFactory.createRadioButtonField("MaintainPaymentTemplateGroup", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_PAYMENT_TEMPLATE_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainPaymentTemplateGroup2 = widgetFactory.createRadioButtonField("MaintainPaymentTemplateGroup", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_PAYMENT_TEMPLATE_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainPaymentTemplateGroup3 = widgetFactory.createRadioButtonField("MaintainPaymentTemplateGroup", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.MAINTAIN_PAYMENT_TEMPLATE_GRP), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;
      %>




<table width="100%" class="formDocumentsTable">
      <%// ----------------- Column headings  ------------------------ %>
      <thead>
      <tr  width="55%">
            <th>&nbsp;</th>
            <th width="15%" align="center"><%=refCol1%></th>
            <th width="15%" align="center"><%=refCol2%></th>
            <th width="15%" align="center"><%=refCol3%></th>
      </tr>
      </thead>
      <tr>
            <td width="40%"><%=myOrgProfile%></td>
            <td width="20%" align="center"><%=MaintainOrgProfile1%></td>
            <td width="20%" align="center"><%=MaintainOrgProfile2%></td>
            <td width="20%" align="center"><%=MaintainOrgProfile3%></td>

      </tr>
      <tr>
            <td width="40%"><%=FXRate%></td>
            <td width="20%" align="center"><%=MaintainFXRate1%></td>
            <td width="20%" align="center"><%=MaintainFXRate2%></td>
            <td width="20%" align="center"><%=MaintainFXRate3%></td>

      </tr>
      <tr>
            <td><%=InstTemplate%><br>
            <%=CreateFixedPaymentTemplate%></td>
            <td align="center"><%=MaintainTemplate1%></td>
            <td align="center"><%=MaintainTemplate2%></td>
            <td align="center"><%=MaintainTemplate3%></td>

      </tr>
      
      <tr>
            <td><%=NotificationRule%></td>
            <td align="center"><%=MaintainNotificationRule1%></td>
            <td align="center"><%=MaintainNotificationRule2%></td>
            <td align="center"><%=MaintainNotificationRule3%></td>

      </tr>

      <tr>
            <td><%=Party%><br>
            <%=CreateNewTransactionParty%></td>
            <td align="center"><%=MaintainParty1%></td>
            <td align="center"><%=MaintainParty2%></td>
            <td align="center"><%=MaintainParty3%></td>
      </tr>

      <tr>
            <td><%=PanelAuthorizationGroups%></td>
            <td align="center"><%=MaintainPanelAuthGroup1%></td>
            <td align="center"><%=MaintainPanelAuthGroup2%></td>
            <td align="center"><%=MaintainPanelAuthGroup3%></td>

      </tr>
	   <tr>
            <td><%=PaymentFileDefGroups%></td>
            <td align="center"><%=MaintainPaymentFileDef1%></td>
            <td align="center"><%=MaintainPaymentFileDef2%></td>
            <td align="center"><%=MaintainPaymentFileDef3%></td>

      </tr>
      <tr>
            <td><%=Phrase%></td>
            <td align="center"><%=MaintainPhrase1%></td>
            <td align="center"><%=MaintainPhrase2%></td>
            <td align="center"><%=MaintainPhrase3%></td>
      </tr>



      <tr>
            <td><%=SecurityProfile%></td>
            <td align="center"><%=MaintainSecProf1%></td>
            <td align="center"><%=MaintainSecProf2%></td>
            <td align="center"><%=MaintainSecProf3%></td>
      </tr>

      <tr>
            <td><%=TemplateGroups%></td>
            <td align="center"><%=MaintainPaymentTemplateGroup1%></td>
            <td align="center"><%=MaintainPaymentTemplateGroup2%></td>
            <td align="center"><%=MaintainPaymentTemplateGroup3%></td>
      </tr>

      <tr>
            <td><%=ThreshGroup      %></td>
            <td align="center"><%=MaintainThreshGrp1%></td>
            <td align="center"><%=MaintainThreshGrp2%></td>
            <td align="center"><%=MaintainThreshGrp3%></td>

      </tr>
      <tr>
            <td><%=User%></td>
            <td align="center"><%=MaintainUser1%></td>
            <td align="center"><%=MaintainUser2%></td>
            <td align="center"><%=MaintainUser3%></td>

      </tr>
      <tr>
            <td><%=WorkGroups %></td>
            <td align="center"><%=MaintainWorkGroups1%></td>
            <td align="center"><%=MaintainWorkGroups2%></td>
            <td align="center"><%=MaintainWorkGroups3%></td>

      </tr>


      <%//PORelatedReferenceData -
      //Jyoti 07-09-2012 IR3198
      //ATP CreationRules -
      String ATPCreationRules = resMgr.getText("SecurityProfileDetail.ATPCreationRules", TradePortalConstants.TEXT_BUNDLE);
      
      String MaintainATPCreateRule1 = widgetFactory.createRadioButtonField("MaintainATPCreateRule", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_OR_MAINTAIN_ATP_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainATPCreateRule2 = widgetFactory.createRadioButtonField("MaintainATPCreateRule", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                      SecurityAccess.VIEW_ATP_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String MaintainATPCreateRule3 = widgetFactory.createRadioButtonField("MaintainATPCreateRule", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                              SecurityAccess.MAINTAIN_ATP_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
      
      //LC CreationRules -
            String LCCreationRules = resMgr.getText("SecurityProfileDetail.LCCreationRules", TradePortalConstants.TEXT_BUNDLE);
            
            String MaintainLCCreateRule1 = widgetFactory.createRadioButtonField("MaintainLCCreateRule", "", 
                        "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,
                            SecurityAccess.VIEW_OR_MAINTAIN_LC_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

            String MaintainLCCreateRule2 = widgetFactory.createRadioButtonField("MaintainLCCreateRule", "", 
                        "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,
                            SecurityAccess.VIEW_LC_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

            String MaintainLCCreateRule3 = widgetFactory.createRadioButtonField("MaintainLCCreateRule", "", 
                        "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,
                                    SecurityAccess.MAINTAIN_LC_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
      // IR3198 ends
      
      //POUploadDefinitions   
      String POUploadDefinitions = resMgr.getText("SecurityProfileDetail.POUploadDefinitions", TradePortalConstants.TEXT_BUNDLE);
      

      String MaintainPOUploadDefn1 = widgetFactory.createRadioButtonField("MaintainPOUploadDefn", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_PO_UPLOAD_DEFN), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainPOUploadDefn2 = widgetFactory.createRadioButtonField("MaintainPOUploadDefn", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_PO_UPLOAD_DEFN), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;

      String MaintainPOUploadDefn3 = widgetFactory.createRadioButtonField("MaintainPOUploadDefn", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.MAINTAIN_PO_UPLOAD_DEFN), isReadOnly,"onClick=\"pickRefDataAccess();\"","") ;
      //InputField.createRadioButtonField("MaintainATPCreateRule", NO_ACCESS, "",!SecurityAccess.hasRights(securityProfileRights,       SecurityAccess.VIEW_OR_MAINTAIN_ATP_CREATE_RULES),                      "ListText", "", isReadOnly,
    //InputField.createRadioButtonField("MaintainATPCreateRule", VIEW_ONLY, "", SecurityAccess.hasRights(securityProfileRights,
                                     // SecurityAccess.VIEW_ATP_CREATE_RULES), "ListText", "", isReadOnly,"onClick=\"pickRefDataAccess();\"")
    // InputField.createRadioButtonField("MaintainATPCreateRule", MAINTAIN, "",                         SecurityAccess.hasRights(securityProfileRights,                                         SecurityAccess.MAINTAIN_ATP_CREATE_RULES),                           "ListText", "", isReadOnly,                           "onClick=\"pickRefDataAccess();\"")
      %>
      <tr class="tableSubHeader">
            <td colspan="4" align="left"><%=resMgr.getText("SecurityProfileDetail.PORelatedReferenceData", TradePortalConstants.TEXT_BUNDLE)%></td>
      </tr>
      
      <tr>
            <td><%=LCCreationRules  %></td>
            <td align="center"><%=MaintainLCCreateRule1%></td>
            <td align="center"><%=MaintainLCCreateRule2%></td>
            <td align="center"><%=MaintainLCCreateRule3%></td>
      </tr>
      <tr>
            <td><%=ATPCreationRules %></td>
            <td align="center"><%=MaintainATPCreateRule1%></td>
            <td align="center"><%=MaintainATPCreateRule2%></td>
            <td align="center"><%=MaintainATPCreateRule3%></td>
      </tr>
      <tr>
            <td><%=POUploadDefinitions%></td>
            <td align="center"><%=MaintainPOUploadDefn1%></td>
            <td align="center"><%=MaintainPOUploadDefn2%></td>
            <td align="center"><%=MaintainPOUploadDefn3%></td>
      </tr>

      
      <%// Invoice related refdata
      
      //Invoice Definiiton
      String invoiceDefinitons = resMgr.getText("RefDataHome.InvoiceDefinitionsLong", TradePortalConstants.TEXT_BUNDLE);
      
      String invoiceDefiniton1 = widgetFactory.createRadioButtonField("MaintainInvoiceDefinitions", "", 
                  "",NO_ACCESS,  !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_INVOICE_DEFINITION), !isAdmin||isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String invoiceDefiniton2 = widgetFactory.createRadioButtonField("MaintainInvoiceDefinitions", "", 
                  "",VIEW_ONLY,  SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_INVOICE_DEFINITION), !isAdmin||isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String invoiceDefiniton3 = widgetFactory.createRadioButtonField("MaintainInvoiceDefinitions", "", 
                  "",MAINTAIN,  SecurityAccess.hasRights(securityProfileRights,SecurityAccess.MAINTAIN_INVOICE_DEFINITION), !isAdmin||isReadOnly,"onClick=\"pickRefDataAccess();\"","");
      
      //Trading Partner Rule
      
      String tradingPartnerRules = resMgr.getText("RefDataHome.InvoiceMgmtTradingRule", TradePortalConstants.TEXT_BUNDLE);
      
      String tradingPartnerRule1 = widgetFactory.createRadioButtonField("MaintainReceivablesMatchRule", "", 
                  "",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_RECEIVABLES_MATCH_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String tradingPartnerRule2 = widgetFactory.createRadioButtonField("MaintainReceivablesMatchRule", "", 
                  "",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_RECEIVABLES_MATCH_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

      String tradingPartnerRule3 = widgetFactory.createRadioButtonField("MaintainReceivablesMatchRule", "", 
                  "",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,   SecurityAccess.MAINTAIN_RECEIVABLES_MATCH_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
	// DK CR709 Rel8.2 10/31/2012 BEGINS
	//Loan Request Creation Rules
	
	String lrCreationRules = resMgr.getText("RefDataHome.LRCreationRule", TradePortalConstants.TEXT_BUNDLE);
	String lrCreationRule1 = widgetFactory.createRadioButtonField("MaintainLRCreateRule", "", 
			"",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_LR_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

	String lrCreationRule2 = widgetFactory.createRadioButtonField("MaintainLRCreateRule", "", 
			"",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_LR_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

	String lrCreationRule3 = widgetFactory.createRadioButtonField("MaintainLRCreateRule", "", 
			"",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,   SecurityAccess.MAINTAIN_LR_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

	//ATP Creation Rules
	
	String atpInvCreationRules = resMgr.getText("RefDataHome.ATPCreationRules", TradePortalConstants.TEXT_BUNDLE);
	String atpInvCreationRule1 = widgetFactory.createRadioButtonField("ATPInvoiceCreationRule", "", 
			"",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_ATP_INV_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

	String atpInvCreationRule2 = widgetFactory.createRadioButtonField("ATPInvoiceCreationRule", "", 
			"",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_ATP_INV_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

	String atpInvCreationRule3 = widgetFactory.createRadioButtonField("ATPInvoiceCreationRule", "", 
			"",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,   SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

	// DK CR709 Rel8.2 10/31/2012 ENDS	

  	//AAlubala - Rel 8.2 CR741 - ERP GL Codes - BEGIN
  	String erpGlCodes = resMgr.getText("RefDataHome.ErpGlCodes", TradePortalConstants.TEXT_BUNDLE);
  	String erpGlCodes1 = widgetFactory.createRadioButtonField("MaintainErpGlCodes", "", 
  			"",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_ERP_GL_CODES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

  	String erpGlCodes2 = widgetFactory.createRadioButtonField("MaintainErpGlCodes", "", 
  			"",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_ERP_GL_CODES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

  	String erpGlCodes3 = widgetFactory.createRadioButtonField("MaintainErpGlCodes", "", 
  			"",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,   SecurityAccess.MAINTAIN_ERP_GL_CODES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
  	//Discount Codes
  	String discountCodes = resMgr.getText("RefDataHome.DiscountCodes", TradePortalConstants.TEXT_BUNDLE);
  	String discountCodes1 = widgetFactory.createRadioButtonField("MaintainDiscountCodes", "", 
  			"",NO_ACCESS, !SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_OR_MAINTAIN_DISCOUNT_CODES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

  	String discountCodes2 = widgetFactory.createRadioButtonField("MaintainDiscountCodes", "", 
  			"",VIEW_ONLY, SecurityAccess.hasRights(securityProfileRights,  SecurityAccess.VIEW_DISCOUNT_CODES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");

  	String discountCodes3 = widgetFactory.createRadioButtonField("MaintainDiscountCodes", "", 
  			"",MAINTAIN, SecurityAccess.hasRights(securityProfileRights,   SecurityAccess.MAINTAIN_DISCOUNT_CODES), isReadOnly,"onClick=\"pickRefDataAccess();\"","");
  		
  	//CR741 - END      
      %>
      <tr class="tableSubHeader">
        <td colspan="4"><%=resMgr.getText("SecurityProfileDetail.InvoiceRelatedReferenceData", TradePortalConstants.TEXT_BUNDLE)%></td>
      </tr>
      <tr>
          <td><%=tradingPartnerRules%></td>
          <td align="center"><%=tradingPartnerRule1%></td>
            <td align="center"><%=tradingPartnerRule2%></td>
            <td align="center"><%=tradingPartnerRule3%></td>
      </tr>
      <tr>
          <td><%=invoiceDefinitons%></td>
          <td align="center"><%=invoiceDefiniton1%></td>
            <td align="center"><%=invoiceDefiniton2%></td>
            <td align="center"><%=invoiceDefiniton3%></td>
      </tr>
	<%-- DK CR709 Rel8.2 10/31/2012 BEGINS --%>
    <tr>
	    <td><%=atpInvCreationRules%></td>
	    <td align="center"><%=atpInvCreationRule1%></td>
	    <td align="center"><%=atpInvCreationRule2%></td>
	    <td align="center"><%=atpInvCreationRule3%></td>
	</tr>  
	<tr>
	    <td><%=lrCreationRules%></td>
	    <td align="center"><%=lrCreationRule1%></td>
	    <td align="center"><%=lrCreationRule2%></td>
            <td align="center"><%=lrCreationRule3%></td>
	</tr>	
	<%-- DK CR709 Rel8.2 10/31/2012 ENDS --%>
      <%--AAlubala Rel8.2 CR741 ERP Codes - BEGIN  --%>
      <tr>
          <td><%=erpGlCodes%></td>
          <td align="center"><%=erpGlCodes1%></td>
            <td align="center"><%=erpGlCodes2%></td>
            <td align="center"><%=erpGlCodes3%></td>
      </tr>
      <tr>
          <td><%=discountCodes%></td>
          <td align="center"><%=discountCodes1%></td>
            <td align="center"><%=discountCodes2%></td>
            <td align="center"><%=discountCodes3%></td>
      </tr>            
      <%-- CR741 END --%>
</table>
</div>

</div>
<%-- Srinivasu_D CR#269 Rel8.4 09/02/2013 - start --%>
<%-- START  9. Conversion Center  --%>
<%= widgetFactory.createSectionHeader("9", "SecurityProfileDetail.9CC")%>
<% //isReadOnly = true; %>
<div>
<%=widgetFactory.createCheckboxField("AccessConverCenter","SecurityProfileDetail.noAccess",!SecurityAccess.hasRights(securityProfileRights,
		SecurityAccess.ACCESS_CONVERSION_CENTER_AREA),isReadOnly,false,"onClick=\"deselectConvCenterRights(false);\"","","")%>
</div>

<%
//Outgoing Gurantee
String ConversionCenter = resMgr.getText("SecurityProfileDetail.OutgoingGuar", TradePortalConstants.TEXT_BUNDLE);

String CCCreateMod = widgetFactory.createCheckboxField("CCCreateMod","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.CC_GUA_CREATE_MODIFY),isReadOnly,false,"onClick=\"pickConvCenterAccess();\"","","none");

String CCDelete = widgetFactory.createCheckboxField("CCDelete","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.CC_GUA_DELETE),isReadOnly,false,"onClick=\"pickConvCenterAccess();\"","","none");

String CCRoute = widgetFactory.createCheckboxField("CCRoute","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.CC_GUA_ROUTE),isReadOnly,false,"onClick=\"pickConvCenterAccess();\"","","none");

String CCAuthorise = widgetFactory.createCheckboxField("CCAuthorise","",SecurityAccess.hasRights(securityProfileRights,
        SecurityAccess.CC_GUA_CONVERT),isReadOnly,false,"onClick=\"pickConvCenterAccess();\"","","none");

String InstHeader = resMgr.getText("InstrumentHistory.Instruments", TradePortalConstants.TEXT_BUNDLE);

%>
<table width="70%">
<tr><td>
<table class="formDocumentsTable">
 <thead>
      <tr>
            <th width="42%">&nbsp;</th>
            <th width="14%"><%=inCol1%></th>
            <th ><%=inCol2%></th>
            <th ><%=inCol3%></th>
            <th ><%=inCol11%></th>

     </tr>
    </thead>
	
      <tr >
            <th colspan="9" style="text-align: left;">
			<%=widgetFactory.createRadioButtonField("SelectAllCC", "SelectCCIAll", 
            "SecurityProfileDetail.selectAll","Y", false, isReadOnly,"onClick=\"selectAllConversionCenterRights();\"","")%>
		    &nbsp; &nbsp;&nbsp;&nbsp; <%=widgetFactory.createRadioButtonField("SelectAllCC", "SelectCCINone", 
            "SecurityProfileDetail.secectNone","N", false, isReadOnly,"onClick=\"selectAllConversionCenterRights();\"","")%> 			 
			</th>
      </tr>
	  </table>
	  <table class="formDocumentsCCTable">  
      <tr>
	  <td align='left'>&nbsp;<%=InstHeader%></td>   
	  </tr>
	  </table>

	   <table class="formDocumentsTable">
	  <tr>
            <td width="42%"><%=ConversionCenter%></td>
            <td width="14%" align='center'><%=CCCreateMod%></td>
            <td align='center'><%=CCDelete%></td>
            <td align='center'><%=CCRoute%></td>
            <td align='center'><%=CCAuthorise%></td>

      </tr>
</table>
</td>

<tr>
</table>
     
</div>

<%-- END  9. Conversion Center  --%>
<%-- Srinivasu_D CR#269 Rel8.4 09/02/2013 - end --%>
<%-- formContentt Area closes here --%>
</div>
<%--formArea--%>

<%-- side bar --%>
<%    String links = ""; %>
<div class="formSidebar" data-dojo-type="widget.FormSidebar"
      data-dojo-props="form: 'SecurityProfileDetail'">
<jsp:include page="/common/RefDataSidebar.jsp">
      <jsp:param value="<%=links %>" name="links" />  
      <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
      <jsp:param name="error" value="<%=error%>" />
      <jsp:param name="saveCloseOnClick" value="none" />
      <jsp:param name="saveOnClick" value="none" />
      <jsp:param name="showSaveButton" value="<%=showSave%>" />
      <jsp:param name="showSaveCloseButton" value="<%=saveClose%>" />
      <jsp:param name="showSaveAsButton" value="<%=showSaveAs%>" />
      <jsp:param name="showDeleteButton" value="<%=showDelete%>" />
      <jsp:param name="showCloseButton" value="true" />
      <jsp:param name="showHelpButton" value="false" />
      <jsp:param name="cancelAction" value="goToRefDataHome" />
      <jsp:param name="saveAsDialogPageName" value="securityProfileSaveAs.jsp" />
      <jsp:param name="saveAsDialogId" value="saveAsDialogId" />
      <jsp:param name="showLinks" value="true" />
</jsp:include></div>
<%--closes sidebar area--%>
</form>

</div>
<%--closes pageContent area--%>
</div>
<%--closes pageMain area--%>


<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<div id="saveAsDialogId" style="display: none;"></div>











<%
  String focusField = "Name";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
  
  
  require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
		ready(function(){
			
			
			
			
			console.log ('securityprofiledetails.jsp onload working');
			var AccessDirectDebit = registry.byId("AccessDirectDebit");
			var ViewChildOrgDirectDebit = registry.byId("ViewChildOrgDirectDebit");
			
			var SelectDDIAll = registry.byId("SelectDDIAll");
			var SelectDDINone = registry.byId("SelectDDINone");
			
			
			var DDICreateMod = registry.byId("DDICreateMod");
			var DDIDelete = registry.byId("DDIDelete");
			var DDIRoute = registry.byId("DDIRoute");
			var DDIAuthorise = registry.byId("DDIAuthorise");
			var DDIProxyAuthorise = registry.byId("DDIProxyAuthorise");
			var DDIUploadFile = registry.byId("DDIUploadFile");
			
			var allChecked=function(){
				AccessDirectDebit.set('checked',false);
				DDICreateMod.set('checked',true);
				DDIDelete.set('checked',true);
				DDIRoute.set('checked',true);
				DDIAuthorise.set('checked',true);
				DDIProxyAuthorise.set('checked',true);
				DDIUploadFile.set('checked',true);
			};
			var allUnChecked=function(){
				DDICreateMod.set('checked',false);
				DDIDelete.set('checked',false);
				DDIRoute.set('checked',false);
				DDIAuthorise.set('checked',false);
				DDIProxyAuthorise.set('checked',false);
				DDIUploadFile.set('checked',false);
			};
			
			on(SelectDDIAll, "change", function(isChecked){
				if(isChecked){
					allChecked();
				}
			},true);
			
			on(SelectDDINone, "change", function(isChecked){
				if(isChecked){
					allUnChecked();
				}
			},true);
			
			on(AccessDirectDebit, "change", function(isChecked){
				if(isChecked){
					SelectDDINone.set('checked',true);
					ViewChildOrgDirectDebit.set('checked',false);
				}
			},true);
			
			on(ViewChildOrgDirectDebit, "change", function(isChecked){
				if(isChecked){
					AccessDirectDebit.set('checked',false);
					SelectDDINone.set('checked',false);
				}
			},true);
			
			
			
			
			on(DDICreateMod, "change", function(isChecked){
				if(isChecked){
					AccessDirectDebit.set('checked',false);
					SelectDDINone.set('checked',false);
				}
			},true);
			on(DDIDelete, "change", function(isChecked){
				if(isChecked){
					AccessDirectDebit.set('checked',false);
					SelectDDINone.set('checked',false);
				}
			},true);
			on(DDIRoute, "change", function(isChecked){
				if(isChecked){
					AccessDirectDebit.set('checked',false);
					SelectDDINone.set('checked',false);
				}
			},true);
			on(DDIAuthorise, "change", function(isChecked){
				if(isChecked){
					AccessDirectDebit.set('checked',false);
					SelectDDINone.set('checked',false);
				}
			},true);
			on(DDIProxyAuthorise, "change", function(isChecked){
				if(isChecked){
					AccessDirectDebit.set('checked',false);
					SelectDDINone.set('checked',false);
				}
			},true);
			on(DDIUploadFile, "change", function(isChecked){
				if(isChecked){
					AccessDirectDebit.set('checked',false);
					SelectDDINone.set('checked',false);
				}
			},true);
			
			
			
			
			
			
		});
	});
  
  
  function submitSaveAs() 
  {
          if(window.document.saveAsForm.newProfileName.value == ""){
            return false;
          }
            document.SecurityProfileDetail.Name.value=window.document.saveAsForm.newProfileName.value
            <%--  Sets a hidden field with the 'button' that was pressed.  This
            value is passed along to the mediator to determine what
            action should be done.  --%>
            document.SecurityProfileDetail.saveAsFlag.value = "Y";            
            require(["dijit/registry","t360/common"],function(registry,common) {
                  common.setButtonPressed('<%=TradePortalConstants.BUTTON_SAVEANDCLOSE%>','0');
                  document.forms[0].setAttribute("method","post"); <%-- Jyoti 28-09-2012 IR3042 added              --%>
                  document.forms[0].submit();
            });               
            hideDialog("saveAsDialogId");             
   } 
</script>
<%
  }
%>

</body>

<%
  // Finally, reset the cached document to eliminate carryover of
  // information to the next visit of this page.
  formMgr.storeInDocCache("default.doc", new DocumentHandler());

  // All done with the bean, so get rid of it.
  beanMgr.unregisterBean("SecurityProfile");
%>
</html>
