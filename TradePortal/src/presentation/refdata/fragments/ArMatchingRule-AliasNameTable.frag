<%--
**********************************************************************************
                              ArMatchingRule Detail

  Description:
     This page is used to maintain individual corporate customer organizations. It
     supports insert, update, and delete.  Errors found during a save are
     redisplayed on the same page. Successful updates return the user to the
     Organizations Home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2012
 *	   @ Developer Komal     
 *     All rights reserved
--%>
<%
    
 String options="";
 String countryCode = editArMatchingRule.getAttribute("address_country");
%>

      <div class="columnLeft" >
  
    <span class="asterisk">*</span>
        <%=widgetFactory.createTextField("buyer_name","ArMatchingRuleDetail.BuyerName",
						editArMatchingRule.getAttribute("buyer_name"), "35", isReadOnly, true, false, "", "", "none")%>
		<br><br><span class="asterisk">*</span>
        <%=widgetFactory.createTextField("address1","TradingPartnerDetails.TradingAddress1",
        		editArMatchingRule.getAttribute("address_line_1"), "35", isReadOnly, true, false, "", "", "none")%>
						
		<br></br>
        <%=widgetFactory.createTextField("address2","TradingPartnerDetails.TradingAddress2",
        		editArMatchingRule.getAttribute("address_line_2"), "35", isReadOnly, false, false, "", "", "none")%>							
     </div>
     
     
     <div class="columnRight">
    <span class="asterisk">*</span>
        <%=widgetFactory.createTextField("city","TradingPartnerDetails.TradingCity",
        		editArMatchingRule.getAttribute("address_city"), "23", isReadOnly, true, false, "", "", "none")%>
        		<br>
	<div style="clear:both"></div>
	
	<div class="formItem inline"  style="float:left;margin-left:0px;">
	<br> <%-- Srinivasu_D Fix of IR#T36000014546 Rel8.2 03/05/2013 start--%>
	<%--
	<label for="state"><%=resMgr.getText("TradingPartnerDetails.TradingProvince", TradePortalConstants.TEXT_BUNDLE)%></label><br>
	<input data-dojo-type="dijit.form.TextBox" name="state" id="state" class=char10   maxLength="35" class="char35">
--%>
	<span class="asterisk"></span>
        <%=widgetFactory.createTextField("state","TradingPartnerDetails.TradingProvince",
        		editArMatchingRule.getAttribute("address_state"), "8", isReadOnly, false, false, "", "", "none")%>
</div>
<div class="formItem inline" style="float:left;" >
<br>
<%--
<label for="postalcode"><%=resMgr.getText("TradingPartnerDetails.TradingPostalCode", TradePortalConstants.TEXT_BUNDLE)%></label><br>
<input data-dojo-type="dijit.form.TextBox" name="postalcode" id="postalcode" class=char10  maxLength="8" class="char8">
--%>
<span class="asterisk"></span>
        <%=widgetFactory.createTextField("postalcode","TradingPartnerDetails.TradingPostalCode",
        		editArMatchingRule.getAttribute("postalcode"), "8", isReadOnly, false, false, "", "", "none")%>
</div>
<%-- Srinivasu_D Fix of IR#T36000014546 Rel8.2 03/05/2013 end --%>

	
   
        <div style="clear:both"></div>	                  
        <%
			 options = Dropdown.createSortedRefDataOptions( TradePortalConstants.COUNTRY,countryCode, resMgr.getResourceLocale() );					   
		 %>
        <span class="asterisk">*</span>
        <%=widgetFactory.createSelectField( "country", "TradingPartnerDetails.TradingCountry"," ", options, isReadOnly, true, false, "", "", "none")  %> 
</div>
<div style="clear: both;"></div>
<br/>
<br/>
<div>
<table id="aliasTable" class="formDocumentsTable">
	  <thead>
		<tr>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.AliasNumber")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.AliasNumber", TradePortalConstants.TEXT_BUNDLE)%></b></th>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.AliasNumber")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.AliasNumber", TradePortalConstants.TEXT_BUNDLE)%></b></th>
		</tr>
	   </thead>

	<tbody>
		<%
			/* if (TradePortalConstants.INDICATOR_YES.equals(canViewPaymentTemplateGroups)) { */
		%>
		<input type=hidden value="<%=aliasNameCount%>" name="aliasNameCount" id="aliasNameCount">
			<%
			iLoop = 0;
			while(iLoop < numItemsName){%>
				<%@ include file="ArMatchingRule-AliasNameTermRows.frag"%>
			<%iLoop = iLoop+2; 		
				}
			/* } */
			%>
	</tbody>
</table>
<br/>
	<% if (!(isReadOnly)) {		%>
			
	<button data-dojo-type="dijit.form.Button" type="submit" id="add4MoreDefined" name="add4MoreDefined">
	<%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	add4MoreDefined();
 
	</script>
	
	</button>
	<%=widgetFactory.createHoverHelp("add4MoreDefined","POStructure.Add4More") %>
		<% } else {		%>
					&nbsp;
		<% }		%>

<%-- <input type=hidden value='<%=insertMode%>' name="insertMode" id="insertModeID">  --%>
	</div>			
<script language="JavaScript">  
function add4MoreDefined() {
	
	require([ "dojo/_base/array", "dojo/dom", "dojo/dom-construct",
				"dojo/dom-class", "dojo/dom-style", "dojo/query",
				"dijit/registry", "dojo/on", "t360/common", "t360/dialog",
				"dojo/ready", "dojo/NodeList-traverse","dojo/domReady!" ], function(arrayUtil,
				dom, domConstruct, domClass, domStyle, query, registry, on,
				common, dialog, ready) {
	var table = dom.byId('aliasTable');
	var insertRowIdx = table.rows.length;
	var lastElement = parseInt(insertRowIdx)-1;
	var iLoop = eval((lastElement * 2)); 
	<%--  var iLoop = document.getElementById("aliasNameCount").value;
	iLoop= eval(parseInt(iLoop)+1);  --%>
	for (i=0;i<2;i++) {
		<%-- var row = table.insertRow(insertRowIdx); --%>
		<%-- row.id = 'AliasName'+insertRowIdx ; --%>
		
		common.appendAjaxInvTableRows(table,"aliasNameIndex"+iLoop,"/portal/refdata/fragments/ArMatchingRule-AliasNameTermRows.jsp?iLoop="+iLoop);  
		
		insertRowIdx = insertRowIdx+1;
		 iLoop = (parseInt(iLoop))+2; 
		
	}	 
	});
}
</script>
