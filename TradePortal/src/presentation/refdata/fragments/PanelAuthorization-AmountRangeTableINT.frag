<div id = "INTRow<%=intLoop+1%>">
			<div id = "INTTable<%=intLoop+1%>" style="border: 1px solid black;">
					<div class="spanHeaderTabel">
							<%=widgetFactory.createLabel("Serial",(intLoop+1) +".", false, false, false, "inline")%>
							<%=widgetFactory.createLabel("PmntAmtRange","PanelAuthorizationGroupDetail.PaymentRange", false, false, false, "inline")%>
							<%=widgetFactory.createAmountField("panelAuthRangeMinAmtINT"+intLoop,"",panelRangeMinAmount, "", isReadOnly, false, false,"style=\"width: 120px;\"", "", "inline")%>
							<%=widgetFactory.createLabel("PmntAmtRangeTo","to", false, false, false, "inline")%>
							<%=widgetFactory.createAmountField("panelAuthRangeMaxAmtINT"+intLoop,"",panelRangeMaxAmount, "", isReadOnly, false, false,"style=\"width: 120px;\"", "", "inline")%>
							<%	if (!isReadOnly){ %>
							<span style="float: right;">
			              		<a href="javascript:removeRangeINT(<%=intLoop %>);" >Remove Amount Range</a>
			              	</span>
			              <% } %>
						<div style="clear: both;"></div>
					</div>

						<br>
						<div class="spanTable">
							<div class="spanRow spanHeader">
								<span class="spanCol char0">&nbsp;</span>
								<span class="spanCol char6"><%=widgetFactory.createLabel("","PanelAuthorizationGroupDetail.LevelA", true, false, false, "inline")%></span>
								<span class="spanCol char6"><%=widgetFactory.createLabel("","PanelAuthorizationGroupDetail.LevelB", true, false, false, "inline")%></span>
								<span class="spanCol char6"><%=widgetFactory.createLabel("","PanelAuthorizationGroupDetail.LevelC", true, false, false, "inline")%></span>
								<span class="spanCol char11"><%=widgetFactory.createLabel("","PanelAuthorizationGroupDetail.TotalAuthorisers", true, false, false, "inline")%></span>
								<span class="spanCol char6"><%=widgetFactory.createLabel("","PanelAuthorizationGroupDetail.ClearRow", true, false, false, "inline")%></span>
								<div style="clear: both;"></div>
							</div>


							<% for(int iLoop = 0; iLoop < 5; iLoop++) { %>
							<div class="spanRow">
								<span class="spanCol"><%= (iLoop+1) %></span>
								<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelAUsersGroupINT"+(iLoop+1)+intLoop, "", panelAuthorizationRangeINTList[intLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
								<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelBUsersGroupINT"+(iLoop+1)+intLoop, "", panelAuthorizationRangeINTList[intLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
								<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelCUsersGroupINT"+(iLoop+1)+intLoop, "", panelAuthorizationRangeINTList[intLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
								<%
								    int panelA = 0;
								    int panelB = 0;
								    int panelC = 0;
								    int totalNumOfAuthGroup = 0;
								    try {
								   	 	 panelA = Integer.parseInt(panelAuthorizationRangeINTList[intLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)));
								    } catch(NumberFormatException nfe) {
								    	panelA = 0;
								    }
								    try {
								   	     panelB = Integer.parseInt(panelAuthorizationRangeINTList[intLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)));
								    } catch(NumberFormatException nfe) {
								    	panelB = 0;
								    }
								    try {
								   	 	panelC = Integer.parseInt(panelAuthorizationRangeINTList[intLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)));

								    } catch(NumberFormatException nfe) {
								    	panelC = 0;
								    }
								    totalNumOfAuthGroup = panelA + panelB + panelC;
								    %>
									<span class="spanCol"><%= totalNumOfAuthGroup%></span>
									<span class="spanCol">
										<%	if (!isReadOnly){ %>
							              		<a href="javascript:clearTextINT(<%=intLoop %>, <%=iLoop %>);" >Clear</a>
							              <% } %>
									</span>
								</div>
								<% } %>
							</div>
					</div>
				</div>

	<%
		if(!InstrumentServices.isBlank(panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_min_amt"))) {
				panelRangeMinAmount = TPCurrencyUtility.getDisplayAmount(
						panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_min_amt"), baseCurrency, userLocale);

			} else {
				panelRangeMinAmount = panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_min_amt");
			}
			if(!InstrumentServices.isBlank(panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_max_amt"))) {
				panelRangeMaxAmount = TPCurrencyUtility.getDisplayAmount(
						panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_max_amt"), baseCurrency, userLocale);
			} else {
				panelRangeMaxAmount = panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_max_amt");
			}

			if(panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_oid") != null) {
				out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOidINT" + intLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( panelAuthorizationRangeINTList[intLoop].getAttribute("panel_auth_range_oid"), userSession.getSecretKey()) + "'>");
			} else {
				out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOidINT" + intLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey()) + "'>");
			}
			out.print("<INPUT TYPE=HIDDEN NAME='InstrumentTypeCodeINT" + intLoop + "' VALUE='" + InstrumentType.FUNDS_XFER + "' ID='InstrumentTypeCodeINT"+intLoop +"'>");

	%>
