<%--
*******************************************************************************
      BankGroupDetail Page
  
Description:   This jsp simply displays the html for the general portion of 
               the BankGroupDetail.jsp.  This page is called from 
               BankGroupDetail.jsp  and is called by:

	       <%@ include file="BankGroupDetail.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%> 

<tr>
		
		<td>
			
			 <%=widgetFactory.createTextField("code" + bankGroupDetailIndex,"", bankPmtRptCode.getAttribute("code"),"3",isReadOnly,false,false,"onblur=\"this.value=this.value.toUpperCase()\"class='char8'" ,"","none")%>
			
		</td>
		
		<td>
		
		 <%=widgetFactory.createTextField("shortDescription" + bankGroupDetailIndex,"", bankPmtRptCode.getAttribute("short_description"),"15",isReadOnly,false,false,"class='char35'","" ,"none")%>
		</td>
		
		<td>
		
		 <%=widgetFactory.createTextField("description" + bankGroupDetailIndex,"", bankPmtRptCode.getAttribute("description"),"30",isReadOnly,false,false,"class='char35'","","none")%>
		</td>
	</tr>
