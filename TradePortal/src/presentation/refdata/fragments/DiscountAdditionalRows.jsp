<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, com.ams.tradeportal.common.cache.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%	
beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.CustomerDiscountCodeWebBean", "CustomerDiscountCode");
CustomerDiscountCodeWebBean customerDiscountCode = (CustomerDiscountCodeWebBean)beanMgr.getBean("CustomerDiscountCode");

WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
String parentOrgID        = userSession.getOwnerOrgOid();
String oid = "0";
int currCount = 4;
boolean	isReadOnly		= false;

String i = request.getParameter("iLoop");		
%>
		<%-- Rel 9.3 XSS CID 11210, 11315, 11404 --%>
		<tr id="disccode<%=StringFunction.xssCharsToHtml(i)%>">
			<td>      
			  <%=widgetFactory.createTextField("discountCode"+StringFunction.xssCharsToHtml(i),"","" ,"2",false,true,false, "style='width: 75px'", "", "") %>
			</td> 
 			
			<td>      
			  <%=widgetFactory.createTextField("desc"+StringFunction.xssCharsToHtml(i),"",	"" ,"30",false,true,false, "style='width: 175px'", "", "") %>
			</td>
			 
			<td>         
			<%--widgetFactory.createCheckboxField("deactivate"+i, "",false, false, false,  "", "", "")--%> 
				<div class="formItem " >				
				<%--<input data-dojo-type="dijit.form.CheckBox" name="deactivate"<%=i%> id="deactivate"<%=i%>  value="Y" > --%>
				<%out.print("<input type='CheckBox' name='deactivate"+StringFunction.xssCharsToHtml(i)+"' id='deactivate"+StringFunction.xssCharsToHtml(i)+"'  value='Y'>"); %>
				</div>			
			</td>		                                                
		</tr>		
	  <input type="hidden" value="<%=oid %>" name="oid<%=StringFunction.xssCharsToHtml(i)%>">
	  <input type="hidden" value="<%=StringFunction.xssCharsToHtml(parentOrgID) %>" name="powner<%=StringFunction.xssCharsToHtml(i)%>">		