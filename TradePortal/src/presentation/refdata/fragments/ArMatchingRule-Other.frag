<%--
**********************************************************************************
                              ArMatchingRule Detail

  Description:
     This page is used to maintain individual corporate customer organizations. It
     supports insert, update, and delete.  Errors found during a save are
     redisplayed on the same page. Successful updates return the user to the
     Organizations Home page.

**********************************************************************************
--%>

<%--
 *
 *     Copyright   2012
 *	   @ Developer Komal     
 *     All rights reserved
--%>
<div class="columnLeft">
<%= widgetFactory.createSubsectionHeader("ArMatchingRuleDetail.Tolerances") %>

<%=widgetFactory.createCheckboxField("ArMatchingRuleTolPerInd","ArMatchingRuleDetail.Percent", editArMatchingRule.getAttribute("tolerance_percent_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly,false, "", "", "onClick=\"unCheckNoTolInd();\"")%>
	
	 <div class="formItem">
	   <%= widgetFactory.createSubLabel( "ArMatchingRuleDetail.PlusSign") %>
	   <%= widgetFactory.createPercentField( "ArMatchingRuleTolPerPlus", "", editArMatchingRule.getAttribute("tolerance_percent_plus"), isReadOnly) %>
	   <%-- <%= widgetFactory.createHoverHelp("ArMatchingRuleTolPerPlus", "ArMatchingRuleDetail.Plus")%> --%>
	   <%= widgetFactory.createSubLabel( "ArMatchingRuleDetail.PercentSign") %>
	   <%= widgetFactory.createSubLabel( "ArMatchingRuleDetail.MinusSign") %>
	   <%= widgetFactory.createPercentField( "ArMatchingRuleTolPerMinus", "", editArMatchingRule.getAttribute("tolerance_percent_minus"), isReadOnly) %>
	   <%-- <%= widgetFactory.createHoverHelp("ArMatchingRuleTolPerMinus", "ArMatchingRuleDetail.Minus")%> --%>
	   <%= widgetFactory.createSubLabel( "ArMatchingRuleDetail.PercentSign") %>
	   <div style="clear:both;"></div>
	   </div>
	   
<%=widgetFactory.createCheckboxField("ArMatchingRuleTolAmtInd","ArMatchingRuleDetail.Amount", editArMatchingRule.getAttribute("tolerance_amount_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly,false, "", "", "onClick=\"unCheckNoTolInd();\"")%>
 	<div class = "formItem">
	   <%= widgetFactory.createSubLabel( "ArMatchingRuleDetail.PlusSign") %>
	   <%= widgetFactory.createPercentField( "ArMatchingRuleTolAmtPlus", "", editArMatchingRule.getAttribute("tolerance_amount_plus"), isReadOnly) %>
	   <%-- <%= widgetFactory.createHoverHelp("ArMatchingRuleTolAmtPlus", "ArMatchingRuleDetail.Plus")%> --%>
	   
	   <%= widgetFactory.createSubLabel( "ArMatchingRuleDetail.MinusSign") %>
	   <%= widgetFactory.createPercentField( "ArMatchingRuleTolAmtMinus", "", editArMatchingRule.getAttribute("tolerance_amount_minus"), isReadOnly) %>
	   <%-- <%= widgetFactory.createHoverHelp("ArMatchingRuleTolAmtMinus", "ArMatchingRuleDetail.Minus")%> --%>
	   
	   <div style="clear:both;"></div>
	</div>
<%=widgetFactory.createCheckboxField("ArMatchingRuleNoTolInd","ArMatchingRuleDetail.NoTolerance", editArMatchingRule.getAttribute("no_tolerance_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly,false, "", "", "onClick=\"pickCommTolCheckbox();\"")%>
</div>

<div class="columnRight">
	<%= widgetFactory.createSubsectionHeader("ArMatchingRuleDetail.PartialPayments") %>
	<%=widgetFactory.createCheckboxField("ArMatchingRulePayToPortalInd","ArMatchingRuleDetail.PayToPortal", editArMatchingRule.getAttribute("partial_pay_to_portal_indicator").equals(TradePortalConstants.INDICATOR_YES), isReadOnly,false, "", "", "")%>
</div>