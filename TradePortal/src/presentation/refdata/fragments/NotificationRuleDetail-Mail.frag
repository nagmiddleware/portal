<%--
*******************************************************************************
       Notification Rule Detail - Mail Messages (Tab) Page
  
Description:   This jsp simply displays the html for the mail messages portion 
               of the NotificationRuleDetail.jsp.  This page is called from
               NotificationRuleDetail.jsp  and is called by:

	       <%@ include file="NotificationRuleDetail-Mail.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>

<%
  secureParms.put("name", notificationRule.getAttribute("name"));
%>

 <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th colspan="2"><%= resMgr.getText("NotificationRuleDetail.Email", TradePortalConstants.TEXT_BUNDLE)%></th>		
		 </tr>
		 <tr>
			<th>&nbsp;</th>
			<th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
			<th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>		
		 </tr>
	 </thead>
	 <tbody>      
     <tr>       
      <td>         
	  	<%=widgetFactory.createSubLabel( "NotificationRuleDetail.Discrepancies")%>
      </td>
      <td align="center">             
              <%=widgetFactory.createRadioButtonField("emailForDiscrepancy","TradePortalConstants.NOTIF_RULE_ALWAYS"+"row2","",TradePortalConstants.NOTIF_RULE_ALWAYS,emailForDisc.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                         
      </td>      
      <td align="center"> 
        <%=widgetFactory.createRadioButtonField("emailForDiscrepancy","TradePortalConstants.NOTIF_RULE_NONE"+"row2","",TradePortalConstants.NOTIF_RULE_NONE,!emailForDisc.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                          
      </td>
     </tr>
     <tr>       
      <td> 
        <%=widgetFactory.createSubLabel( "NotificationRuleDetail.MailMessage")%>
	  </td>
      <td align="center">             
              <%=widgetFactory.createRadioButtonField("emailForMessage","TradePortalConstants.NOTIF_RULE_ALWAYS"+"row1","",TradePortalConstants.NOTIF_RULE_ALWAYS,emailForMsg.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                         
      </td>      
      <td align="center"> 
        <%=widgetFactory.createRadioButtonField("emailForMessage","TradePortalConstants.NOTIF_RULE_NONE"+"row1","",TradePortalConstants.NOTIF_RULE_NONE,!emailForMsg.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                          
      </td>
     </tr>
     <tr>       
      <td> 
        <%=widgetFactory.createSubLabel( "NotificationRuleDetail.RecPayMatch")%>
	  </td>
      <td align="center">             
          <%=widgetFactory.createRadioButtonField("emailForRecPayMatch","TradePortalConstants.NOTIF_RULE_ALWAYS"+"row3","",TradePortalConstants.NOTIF_RULE_ALWAYS,emailForRecPayMatch.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                         
      </td>      
      <td align="center"> 
        <%=widgetFactory.createRadioButtonField("emailForRecPayMatch","TradePortalConstants.NOTIF_RULE_NONE"+"row3","",TradePortalConstants.NOTIF_RULE_NONE,!emailForRecPayMatch.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                          
      </td>
     </tr>
     <%-- SSikhakolli - Rel-9.4 CR-818 --%>
     <tr>       
     <td> 
       <%=widgetFactory.createSubLabel( "NotificationRuleDetail.SettlementInstructionReq")%>
	  </td>
     <td align="center">             
         <%=widgetFactory.createRadioButtonField("emailForSettlementInstructionReq","TradePortalConstants.NOTIF_RULE_ALWAYS"+"row4","",TradePortalConstants.NOTIF_RULE_ALWAYS,emailForSettlementInstructionReq.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                         
     </td>      
     <td align="center"> 
       <%=widgetFactory.createRadioButtonField("emailForSettlementInstructionReq","TradePortalConstants.NOTIF_RULE_NONE"+"row4","",TradePortalConstants.NOTIF_RULE_NONE,!emailForSettlementInstructionReq.equals(TradePortalConstants.NOTIF_RULE_ALWAYS), isReadOnly)%>                          
     </td>
    </tr>
    </tbody>
 </table>