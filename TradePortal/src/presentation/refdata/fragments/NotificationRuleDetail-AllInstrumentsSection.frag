<%--
*******************************************************************************
       Notification Rule Detail - All Instrumnet setting section
  
Description:    This jsp simply displays the html for the All Instrumnet setting 
		of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:


*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>
<%		
	String userSql = new String();
	QueryListView userEmailqueryListView = null;
	DocumentHandler userEmailCriterionList = new DocumentHandler();
	DocumentHandler doc2 = new DocumentHandler(); 
			

	if (getDataFromDoc){
		doc2 = defaultDoc.getComponent("/In");
	}
	//Below Loop is to generate sections for all Instrument Catagories except MailMessages, Supplier Portal Invoices and Host to Host Invoice/Credit Note Approvals
	for(iLoop = 1; iLoop <= dyanInstrCatArray.length; iLoop++){
	defUserIdArray = new String[24];
	tempUserIdArray = new String[24];
		if(!InstrumentType.MESSAGES.equals(dyanInstrCatArray[iLoop-1]) && 
				!InstrumentType.SUPP_PORT_INST_TYPE.equals(dyanInstrCatArray[iLoop-1]) && 
				!InstrumentType.HTWOH_INV_INST_TYPE.equals(dyanInstrCatArray[iLoop-1])){
		
		sectionCounter++;

		String instrValue = dyanInstrCatArray[iLoop-1];
        		transArray = instrTransArrays.get(instrValue);
        		
        		for(int tLoop = 0; tLoop < transArray.length; tLoop++){
        			//System.out.println(", "+dyanInstrCatArray[iLoop-1]+" - "+transArray[tLoop]);
        			String key;
	   			    key = dyanInstrCatArray[iLoop-1]+"_"+transArray[tLoop];
	
	   			    NotificationRuleCriterionWebBean notifyRuleCriterion = null;
	   			    notifyRuleCriterion = (NotificationRuleCriterionWebBean)criterionRuleTransList.get(key);
	   			    yy++;
	   			 	if (notifyRuleCriterion != null) {
	   		      		//load using OID
	 					out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + notifyRuleCriterion.getAttribute("instrument_type_or_category") + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + notifyRuleCriterion.getAttribute("transaction_type_or_action") + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='" + notifyRuleCriterion.getAttribute("criterion_oid") + "'>");
	   		      		out.print("<input type=hidden name='send_notif_setting"+yy+"' id='send_notif_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_notif_setting") + "'>");
	   		      		out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_email_setting") + "'>");
	   		      		out.print("<input type=hidden name='additional_email_addr"+yy+"' id='additional_email_addr"+yy+"' value='" + notifyRuleCriterion.getAttribute("additional_email_addr") + "'>");
	   		      		
		   		      	userEmailqueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		   		      	userSql = "select notify_rule_user_list_oid, p_user_oid as user_oid from notify_rule_user_list where p_criterion_oid = ? and p_user_oid !=0 ";
	                	userEmailqueryListView.setSQL(userSql,new Object[]{notifyRuleCriterion.getAttribute("criterion_oid")});
	                	userEmailqueryListView.getRecords();
	                	
	                	userEmailCriterionList = userEmailqueryListView.getXmlResultSet();
							
	                	emailVector = userEmailCriterionList.getFragments("/ResultSetRecord");
	                	numItems = emailVector.size();
	                	
	                	NotifyRuleUserListWebBean notifyRuleUserList[] = new NotifyRuleUserListWebBean[numItems];
	                	
	                	userIDArray = new String[24];
	                	dbUserIDArray = new StringBuilder();
	                	userEmailValues = "";
	                	String tmpUserId = null;
	                	for (int eLoop=0;eLoop<numItems;eLoop++) {
	                		notifyRuleUserList[eLoop] = beanMgr.createBean(NotifyRuleUserListWebBean.class, "NotifyRuleUserList");
	                  		DocumentHandler notifyRuleUserListDoc = (DocumentHandler) emailVector.elementAt(eLoop);
							tmpUserId = notifyRuleUserListDoc.getAttribute("/USER_OID");
							if(setupUserList.contains(tmpUserId)){
	                  			userIDArray[eLoop] = notifyRuleUserListDoc.getAttribute("/USER_OID");
	                  		}else{
								userIDArray[eLoop] = "0";
							}
	                  		dbUserIDArray.append(notifyRuleUserListDoc.getAttribute("/USER_OID"));
	                  		if(eLoop<numItems-1){
	                  			dbUserIDArray.append(",");
	                  		}
	                	}//eLoop
	                	
				   		for (int vLoop=0;vLoop<userIDArray.length;vLoop++) {
				   			if(StringFunction.isNotBlank(userIDArray[vLoop])){
				   			//	userIDArray[vLoop] = "0";
							userEmailValues += userIDArray[vLoop];
				   			}
				   			//userEmailValues += userIDArray[vLoop];
				   			
				   			if(vLoop != userIDArray.length-1){
									if(userEmailValues.length()>0){
				   				userEmailValues += ",";
									}
				   			}
				   		}
	                	
	   		      		//Email/User ID starts here
		   			    out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[0]+"'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[1]+"'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[2]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[3]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[4]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[5]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[6]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[7]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[8]+"'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[9]+"'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[10]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[11]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[12]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[13]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[14]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[15]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[16]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[17]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[18]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[19]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[20]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[21]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[22]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[23]+"'>");
		   		    } else {//if it is a new form
		   		    	out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + dyanInstrCatArray[iLoop-1] + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + transArray[tLoop] + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='0'>");		   		     	
		   		      	out.print("<input type=hidden name='send_notif_setting"+yy+"' id='send_notif_setting"+yy+"' value=''>");
		   		      	out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value=''>");
		   		      	out.print("<input type=hidden name='additional_email_addr"+yy+"' id='additional_email_addr"+yy+"' value=''>");
		   		     
		   		      	userIDArray = new String[24];
	                	userEmailValues = "";
	                	
				   		for (int vLoop=0;vLoop<userIDArray.length;vLoop++) {
				   			if(StringFunction.isNotBlank(userIDArray[vLoop])){
				   							userEmailValues += userIDArray[vLoop];
				   			}
				   			if(vLoop != userIDArray.length-1){
								if(userEmailValues.length()>0){
				   				userEmailValues += ",";
								}
				   			}
				   		}
		   		      	//Email/User ID starts here
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		    }
					out.print("<input type=hidden name='notificationUserIds"+(yy)+"' id='notificationUserIds"+(yy)+"' value='"+userEmailValues+"'>");
					out.print("<input type=hidden name='dbNotificationUserIds"+(yy)+"' id='dbNotificationUserIds"+(yy)+"' value='"+dbUserIDArray.toString()+"'>");
					out.print("<input type=hidden name='clear_tran_ind"+(yy)+"' id='clear_tran_ind"+(yy)+"' value='N'>");

	        	}//ent tLoop
				

		if (getDataFromDoc){
			defSendNotifSetting = "";
			defSendEmailSetting = "";
			defAddlEmailAddresses = "";
			defUserIds = "";
			defUserNames = "";
			defApplytoAllTransBtn = "";
			defClearAllTransBtn = "";
			
			//System.out.print("get from doc");
			defListVector = doc2.getFragments("/NotificationRule/DefaultList("+(iLoop)+")");
				
			if(defListVector !=null && defListVector.size()>0){
				DocumentHandler notifyRuleDefInstrDoc = (DocumentHandler) defListVector.elementAt(0);

				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_notif_setting"))){
					defSendNotifSetting = notifyRuleDefInstrDoc.getAttribute("/send_notif_setting");
				}
	
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_email_setting"))){
					defSendEmailSetting = notifyRuleDefInstrDoc.getAttribute("/send_email_setting");
				}
	
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/additional_email_addr"))){
					defAddlEmailAddresses = notifyRuleDefInstrDoc.getAttribute("/additional_email_addr");
				}
	
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/notificationUserIds"))){
					defUserIds = notifyRuleDefInstrDoc.getAttribute("/notificationUserIds");
				}
				
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/instNotificationUserNames"))){
					defUserNames = notifyRuleDefInstrDoc.getAttribute("/instNotificationUserNames");
				}
				
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran"))){
					defApplytoAllTransBtn = notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran");
				}
	
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran"))){
					defClearAllTransBtn = notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran");
				}
				
				if(StringFunction.isBlank(defUserIds)){
					tempUserIdArray = new String[0];
				}else if(!defUserIds.equals("")){
					tempUserIdArray = defUserIds.split(",");
					List<String> list = new ArrayList<String>();
				    for(String s : tempUserIdArray) {
				       if(!("").equals(s) && !("0").equals(s)){ 
				          list.add(s);
				       }
				    }
				
				    tempUserIdArray = list.toArray(new String[list.size()]);
				}
				uidx = 0;
				for(int i=0; i<tempUserIdArray.length; i++){
					if(!("0").equals(tempUserIdArray[i]) && !("").equals(tempUserIdArray[i])){
						defUserIdArray[uidx] = tempUserIdArray[i];
						uidx++;
					}
				}
			}
		}
%> 
		<%=widgetFactory.createSectionHeader(""+(iLoop+1), ""+sectionHeaderLable.get(dyanInstrCatArray[iLoop-1]), null, true) %>       
  		<%=widgetFactory.createWideSubsectionHeader(""+sectionNoteLable.get(dyanInstrCatArray[iLoop-1])) %>	
    	<table>
    		<tr>
    			<td width="20%">
    				<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendPortalNotificatios") %>
    			</td>
    			<td width="18%">
    				<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
    			</td>
    			<td width="35%">
					<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailRecipients") %>
				</td>
				<td width="27%">
					<%=widgetFactory.createSubLabel("NotificationRuleDetail.AddlEmailRecipients", "subLabelNarrowLineHight") %>
				</td>
    		</tr>
    		<tr>
    			<td width="20%" style="vertical-align: top;">
    				<%=widgetFactory.createRadioButtonField("notifySetting"+iLoop,"notifySettingAlways"+iLoop,
    						"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,
    						TradePortalConstants.NOTIF_RULE_ALWAYS.equals(defSendNotifSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("notifySetting"+iLoop,"notifySettingNone"+iLoop,
    						"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,
    						TradePortalConstants.NOTIF_RULE_NONE.equals(defSendNotifSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("notifySetting"+iLoop,"notifySettingChrgsDocsOnly"+iLoop,
    						"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,
    						TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(defSendNotifSetting),isReadOnly)%>
				
    			</td>
 
    			<td width="18%" style="vertical-align: top;">
    				<%=widgetFactory.createRadioButtonField("emailSetting"+iLoop,"emailSettingAlways"+iLoop,
    						"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,
    						TradePortalConstants.NOTIF_RULE_ALWAYS.equals(defSendEmailSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("emailSetting"+iLoop,"emailSettingNone"+iLoop,
    						"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,
    						TradePortalConstants.NOTIF_RULE_NONE.equals(defSendEmailSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("emailSetting"+iLoop,"emailSettingChrgsDocsOnly"+iLoop,
    						"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,
    						TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(defSendEmailSetting),isReadOnly)%>					
    			</td>
 					
 				<td width="35%" style="vertical-align: top;">
					<div id="notifUsersRows<%=iLoop%>" style="height: auto; overflow-y: auto; overflow-x: hidden; max-height: 135px;">
						<table border="1" id="NotifUsersTable<%=iLoop%>" style="width:90%">
							<%tableRowIdPrefix="NotifUsersTable"+iLoop; %>
							<tbody>
							<%
								if (getDataFromDoc){
									if(tempUserIdArray.length == 0){
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
									}else if(tempUserIdArray.length % 2 == 0){
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = tempUserIdArray.length / 2;
									}else{
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = (tempUserIdArray.length / 2) + 1;
									}
								}
								if(DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT < 2) {
									DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
								}
								notifyEmail = "emailId";
								notifEmailIndex = (iLoop-1) * 24;
								rowIndex = 0;
								uidx = 0;
								sectionNum = iLoop;
							%>
									<%@ include file="NotificationRuleDetail-UserEmailRows.frag" %>  
							</tbody>
						</table>
					</div>
	        		<%
  						if (!isReadOnly) {
							if( StringFunction.isNotBlank(notificationRuleOid) && !notificationRuleOid.equals("0")) {
						%>	
						<div>
						<button data-dojo-type="dijit.form.Button" type="button" id="updateUsersOnly<%=iLoop%>">
							<%=resMgr.getText("NotificationRuleDetail.UpdateUsers",TradePortalConstants.TEXT_BUNDLE)%>
  							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.updateUsersOnly(<%=iLoop%>, 
													'<%=criteriaStartCounter.get(dyanInstrCatArray[iLoop-1]).intValue()%>', 
													'<%=instrTransArrayStr.get(dyanInstrCatArray[iLoop-1])%>', 
													'<%=dyanInstrCatArray[iLoop-1]%>', 'ALL');
							</script>
						</button>
						</div>
						<%}%>
						<div>
						<button data-dojo-type="dijit.form.Button" type="button" id="add2MoreUsers<%=iLoop%>">
							<%=resMgr.getText("NotificationRuleDetail.Add2Users",TradePortalConstants.TEXT_BUNDLE)%>
  							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.add2MoreUserEmails (<%=iLoop%>, <%=(iLoop-1) * 24%>);
							</script>
						</button>
						</div>
					<%
  						}
					%>  
				</td>
 					
    			<td width="27%" align="left" style="vertical-align: top;">
	     				<%= widgetFactory.createTextArea("additionalEmailAddr"+iLoop, "", defAddlEmailAddresses, isReadOnly, false, false, "maxlength='300' rows='6'", "", "none") %>
						<%if( StringFunction.isNotBlank(notificationRuleOid) && !notificationRuleOid.equals("0")) { %>
						<button data-dojo-type="dijit.form.Button"  name="UpdateEmailsOnly<%=iLoop%>" id="UpdateEmailsOnly<%=iLoop%>" type="button">
	  						<%=resMgr.getText("NotificationRuleDetail.UpdateEmailOnly",TradePortalConstants.TEXT_BUNDLE)%>
	  						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								local.updateEmailsOnly(<%=iLoop%>, 
														'<%=criteriaStartCounter.get(dyanInstrCatArray[iLoop-1]).intValue()%>', 
														'<%=instrTransArrayStr.get(dyanInstrCatArray[iLoop-1])%>', 
														'<%=dyanInstrCatArray[iLoop-1]%>', 'ALL');
							</script>
	    				</button>
	    				<br>
    				<%}%>
    				<button data-dojo-type="dijit.form.Button"  name="ApplytoAllTransactions<%=iLoop%>" id="ApplytoAllTransactions<%=iLoop%>" type="button">
  						<%=resMgr.getText("NotificationRuleDetail.ApplytoAllTransactions.Button",TradePortalConstants.TEXT_BUNDLE)%>
  						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.applyToAllTransactions(<%=iLoop%>, 
														'<%=criteriaStartCounter.get(dyanInstrCatArray[iLoop-1]).intValue()%>', 
														'<%=instrTransArrayStr.get(dyanInstrCatArray[iLoop-1])%>', 
														'<%=dyanInstrCatArray[iLoop-1]%>', 'ALL');
						</script>
    				</button>
    				
    				<button data-dojo-type="dijit.form.Button"  name="Override<%=iLoop%>" id="Override<%=iLoop%>" type="button">
  						<%=resMgr.getText("NotificationRuleDetail.Override.Button",TradePortalConstants.TEXT_BUNDLE)%>
  						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.ocerrideTransactionsSection(<%=iLoop%>, 
															 '<%=criteriaStartCounter.get(dyanInstrCatArray[iLoop-1]).intValue()%>', 
															 '<%=instrTransArrayStr.get(dyanInstrCatArray[iLoop-1])%>', 
															 '<%=dyanInstrCatArray[iLoop-1]%>', 'ALL', 
															 '<%=sectionDialogTitle.get(dyanInstrCatArray[iLoop-1])%>');
						</script>
    				</button>
    		
    				<button data-dojo-type="dijit.form.Button"  name="ClearAll<%=iLoop%>" id="ClearAll<%=iLoop%>" type="button">
  						<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
  						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.clearInstrCategorySection(<%=iLoop%>, 
															'<%=criteriaStartCounter.get(dyanInstrCatArray[iLoop-1]).intValue()%>', 
															'<%=instrTransArrayStr.get(dyanInstrCatArray[iLoop-1])%>', 
															'<%=dyanInstrCatArray[iLoop-1]%>', 'ALL');
						</script>
    				</button>
				</td>
				<input type="hidden" name="instrumentType<%=iLoop%>" id="instrumentType<%=iLoop%>" value="<%=dyanInstrCatArray[iLoop-1]%>">
				<input type="hidden" name="applyInst<%=iLoop%>" id="applyInst<%=iLoop%>" value="<%=defApplytoAllTransBtn%>">
				<input type="hidden" name="clearAll<%=iLoop%>" id="clearAll<%=iLoop%>" value="<%=defClearAllTransBtn%>">
				<input type="hidden" name="updateRecipientsOnlyInst<%=iLoop%>" id="updateRecipientsOnlyInst<%=iLoop%>" value="">
				<input type="hidden" name="updateEmailsOnlyInst<%=iLoop%>" id="updateEmailsOnlyInst<%=iLoop%>" value="">
				<input type="hidden" name="changeInd<%=iLoop%>" id="changeInd<%=iLoop%>" value="">
				<input type="hidden" name="colType<%=iLoop%>" id="colType<%=iLoop%>" value="H">	
				<input type="hidden" name="instNotificationUserIds<%=iLoop%>" id="instNotificationUserIds<%=iLoop%>" value="<%=defUserIds%>">
				<input type="hidden" name="instNotificationUserNames<%=iLoop%>" id="instNotificationUserNames<%=iLoop%>" value="<%=defUserNames%>">
			<%
				
   			%>
    		</tr>
    	</table> 
	
			<% if(dataList.contains(instrValue)) {%>

					 <span class="availNotifyItemSelect">
					<%=resMgr.getText("NotificationRuleDetail.DataExists",TradePortalConstants.TEXT_BUNDLE)%>
					</span>
					<% } %>
	
    </div>
<% 
	}//end of if display condition
	}//end of for
%>