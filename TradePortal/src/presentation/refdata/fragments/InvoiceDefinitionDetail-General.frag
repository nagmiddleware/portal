<%--
	*******************************************************************************
	       Invoice Definition Detail - General and Data Definition Layout (Tab) Page
	
	    Description:   This jsp simply displays the html for the data description
	portion of the Invvoice Definition.jsp.  This page is called from
	Invoice Definition Detail.jsp  and is called by:
			<%@ include file="InvoiceDefinitionDetail.jsp" %>
	Note: the values stored in secureParms in this page are done deliberately to avoid
	being overwritten as each tab is a submit action.  As a result, if this data does
	not make it into the xml document, then it may be overwritten with a null value
	in this tab or another.
	*******************************************************************************
	--%>
	<%--
	 *
	 *     Copyright   2001
	 *     American Management Systems, Incorporated
	 *     All rights reserved
	 *     Srinivasu IR-KDUL122873659 12/30/2011
	 *     Layout changes included as per IR given above
	--%>
	<%
		final int NUMBER_OF_BUYER_OR_SELLER_FIELD = TradePortalConstants.INV_BUYER_OR_SELLER_USER_DEF_FIELD;
		final int NUMBER_OF_ITEM_DETAILS_PROD_FIELD = TradePortalConstants.INV_LINE_ITEM_TYPE_OR_VALUE_FIELD;
	
		String inv_field_data_type_options = ""; //List of refdata options for Dropdown(s)
		String defaultText = " ";
		String options;
		String del_options = "";
		int counterTmp = 3;
		String currentDefinedLab = "";
		String currentDefined = "";
		String currentDefinedLabReq = "";
		String currentDefinedReq = "";
		String currentDefinedLabUpdt = "";
		int invSummDataOrder;
		int invSummGoodOrder;
		
		boolean invRecType=true ;
		boolean invPayType=false ;
		String  recVisible = "";
		String payVisible = "none";
		String currentDefinedLabDataReq = "";
		String currentDefinedDataReq = "";
		String temp="";
	%>
	<script language='JavaScript'>
		
	
	</script>
	<%-- General Title Panel details start --%>
	
	<%=widgetFactory.createSectionHeader("1",
						"InvUploadDefinitionDetail.General")%>
	
	<div class = "columnLeft">
		<% if (isReadOnly) { %>
		<input type=hidden value="<%=invoiceDef.getAttribute("name")%>" name=name>
		<input type=hidden value="<%=invoiceDef.getAttribute("description")%>" name=description>
		<input type=hidden value="<%=invoiceDef.getAttribute("date_format")%>" name=date_format>
		<input type=hidden value="<%=invoiceDef.getAttribute("delimiter_char")%>" name=delimiter_char>
		<input type=hidden value="<%=invoiceDef.getAttribute("invoice_type_indicator")%>" name=InvoiceTypeIndicator>
		<input type=hidden value="<%=invoiceDef.getAttribute("include_column_headers")%>" name=include_column_headers>
		<%}%>
		<%-- Name and Description --%>
	
		<%=widgetFactory.createSubsectionHeader(
						"INVUploadDefinitionDetail.NameAndDescription", false,
						false, false, "")%>
	
		<%=widgetFactory.createTextField("name",
						"InvoiceDefinition.name", invoiceDef.getAttribute("name"),
						"35", isReadOnly,true,false,"class = 'char25'", "", "") %>
	
		<%=widgetFactory.createTextField("description",
						"InvoiceDefinition.Description",
						invoiceDef.getAttribute("description"), "65", isReadOnly,true,false,"class = 'char25'", "", "") %>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			
	
	</div>
	<div class = "columnRight">
	
		<%-- Date Format --%>
	
		<%=widgetFactory.createSubsectionHeader(
						"InvoiceDefinition.InvDataFormarDesc", false, false, false,
						"")%>
	
		<%
			String poDateFormat = invoiceDef.getAttribute("date_format");
		%>
	
	        <%
	        	del_options = Dropdown.createSortedRefDataOptions(
	        			TradePortalConstants.INV_DATE_FORMAT,
	        			invoiceDef.getAttribute("date_format"),
	        			resMgr.getResourceLocale());
	        	out.print(widgetFactory.createSelectField("date_format",
	        			"POStructureDefinition.DateFormat", " ", del_options, isReadOnly, true, false,
	        			"", "", ""));
	        %>
	
		<%-- Data Format --%>
	
		<%
			del_options = Dropdown.createSortedRefDataOptions(
					TradePortalConstants.INV_DELIMITER_CHAR,
					invoiceDef.getAttribute("delimiter_char"),
					resMgr.getResourceLocale());
		%>
	          <%=widgetFactory.createSelectField("delimiter_char",
						"POUploadDefinitionDetail.DelimiterCharacter", " ",
						del_options, isReadOnly, true, false, "", "", "")%>
	     	
	      <div padding-left: 1px;>
	      	
	      	
			 <%=widgetFactory.createLabel("",
						"InvoiceDefinition.InvoicePayType", false, false, false,
						"","")%>
		      
			  <%
			  
					String  invTypeIndicator = invoiceDef.getAttribute("invoice_type_indicator");
					if(InstrumentServices.isNotBlank(invTypeIndicator) && TradePortalConstants.INVOICE_TYPE_REC_MGMT.equals(invTypeIndicator)){
						invRecType = true;
						invPayType = false;
						recVisible ="";
						payVisible ="none";
					}
					else if(InstrumentServices.isNotBlank(invTypeIndicator) && TradePortalConstants.INVOICE_TYPE_PAY_MGMT.equals(invTypeIndicator)){
						invRecType = false;
						invPayType = true;
						payVisible ="";
						recVisible ="none";
					}
					if(invRecType == false && invPayType == false){
						recVisible ="";
						payVisible ="none";
					}
					
			  %>
		
		   <%out.print(widgetFactory.createRadioButtonField("InvoiceTypeIndicator", 
	                                                       "TradePortalConstants.INVOICE_TYPE_REC_MGMT", "InvoiceDefinition.ReceivableMgmt",TradePortalConstants.INVOICE_TYPE_REC_MGMT, invRecType, 
	                                                       (isReadOnly || invAssigned), 
	                                                       "",""));%>					
	
			
			<%out.print(widgetFactory.createRadioButtonField("InvoiceTypeIndicator", 
	                                                       "TradePortalConstants.INVOICE_TYPE_PAY_MGMT","InvoiceDefinition.PayableMgmt" ,TradePortalConstants.INVOICE_TYPE_PAY_MGMT ,invPayType, 
	                                                         (isReadOnly || invAssigned), 
	                                                         "",""));%>					
	
	</div>	
	      	
	<br>
	<br>
		<%=widgetFactory.createCheckboxField(
						"include_column_headers",
						"InvoiceDefinition.includeColumnHeading",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef
								.getAttribute("include_column_headers")),
						isReadOnly, false, "", "", "none")%>
	
	</div>
	
	</div>
	
	<%-- General Title Panel End --%>
	
	<%-- Data Definition Title Panel details start --%>
	<%=widgetFactory.createSectionHeader("2",
						"InvUploadDefinitionDetail.DataDefinition")%>
	
	<div class = "columnLeft">
		<%=widgetFactory.createLabel("",
						"INVUploadDefinitionDetail.DefSep1", false, false, false,
						"none")%>
	
	
	<table class="formDocumentsTable" id="InvDefTableId" style="width:98%;">
	<thead>
		<tr style="height:35px;">
			<th colspan=4 style="text-align:left;">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.InvSumm")%>
			</th>
		</tr>
	</thead>
	<tbody>
	      <tr >
	      	 <td nowrap width="15" >&nbsp;</td>
	 		 <td nowrap width="130"  >
			  	<b><%=resMgr.getText("InvoiceDefinition.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		  </td>
	 		 <td  width="20" align="center" >
			  	<b><%=resMgr.getText("InvoiceDefinition.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	 		 <td width="30" >
			  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoiceDefinition.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	      </tr>
	      <tr>
	 		 <td >&nbsp;</td>
	 		 <td nowrap>
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.InvoiceID", false, true, false, "none")%>
	 		  </td>
	 		  <td align="right">
	 		  <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	 		  
	 		  <td >&nbsp;</td>
	 	  </tr>
	      <tr>
	 		 <td >&nbsp;</td>
	 		 <td nowrap >
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.IssueDate", false, true, false, "none")%>
	 		  </td>
	 		  
	 		  <td align="right">
	 			<%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td>
	 	  </tr>
	      <tr>
	 		 <td >&nbsp;</td>
	 		 <td nowrap >
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.DueDate", false, true, false, "none")%>
	 		  </td>
	 		  <td align="right">
	 			<%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td>
	 	  </tr>
	 	  <tr id ="buyerId" style="display:<%=recVisible%>">
	 		 <td width="20" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("buyer_id_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="buyer_id_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	          <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.buyer_id", TradePortalConstants.TEXT_BUNDLE)%>" name="buyer_id" id="buyer_id">
	             <%=widgetFactory.createCheckboxField(
						"buyer_id_req",
						"",
						invoiceDef.getAttribute("buyer_id_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	         </td>
	 		 <td nowrap>
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.BuyerID", false, true, false, "none")%>
	 		  </td>
	 		  <td align="right">
	 			<%= widgetFactory.createLabel("", "30", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td>
	 	  </tr>
	       <tr id ="buyerName" style="display:<%=recVisible%>">
	 		 <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("buyer_name_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="buyer_name_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	          <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.buyer_name", TradePortalConstants.TEXT_BUNDLE)%>" name="buyer_name" id="buyer_name">
	             <%=widgetFactory.createCheckboxField(
						"buyer_name_req",
						"",
						invoiceDef.getAttribute("buyer_name_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	         </td>
	 		 <td nowrap >
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.BuyerName", false, true, false, "none")%>
	 		  </td>
	 		  <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td>
	 	  </tr>
	 	  
	 	  <tr id ="sellerId" style="display:<%=payVisible%>">
			<td width="15" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("seller_id_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="seller_id_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
			
	          <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.seller_id", TradePortalConstants.TEXT_BUNDLE)%>" name="seller_id" id="seller_id">
	             <%=widgetFactory.createCheckboxField(
						"seller_id_req",
						"",
						invoiceDef.getAttribute("seller_id_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	         </td>
	 		 <td nowrap>
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.SellerId", false, true, false, "none")%>
	 		  </td>
	 		  <td align="right">
	 			<%= widgetFactory.createLabel("", "30", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td> 
		  </tr>
		  <tr id ="sellerName" style="display:<%=payVisible%>">
		  	<td width="15" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("seller_name_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="seller_name_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
		  	
	          <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.seller_name", TradePortalConstants.TEXT_BUNDLE)%>" name="seller_name" id="seller_name">
	             <%=widgetFactory.createCheckboxField(
						"seller_name_req",
						"",
						invoiceDef.getAttribute("seller_name_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	         </td>
	 		 <td >
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.SellerName", false, true, false, "none")%>
	 		  </td>
	 		 <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td>
		  </tr>
	 	  
	 	  
	 	  <tr>
	 		 <td nowrap width="4">&nbsp</td>
	 		 <td nowrap >
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("",
						"InvoiceDefinition.Cuurency", false, true, false, "none")%>
	 		  </td>
	 		  <td align="right">
	 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td>
	 	  </tr>
	 	  <tr>
	 		 <td nowrap width="4">&nbsp</td>
	 		 <td nowrap >
	 		 <span class="asterisk">*</span>
			  	<%=widgetFactory.createLabel("", "InvoiceDefinition.Amount",
						false, true, false, "none")%>
	 		  </td>
	 		  <td align="right">
	 			<%= widgetFactory.createLabel("", "15,3", false, false, false, "none") %>
	 		  </td>
	 		  <td >&nbsp;</td>
	 	  </tr>
	 	  <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("linked_to_instrument_type_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="linked_to_instrument_type_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.linked_to_instrument_type", TradePortalConstants.TEXT_BUNDLE)%>" name="linked_to_instrument_type" id="linked_to_instrument_type">
	
	              <%=widgetFactory.createCheckboxField(
						"linked_to_instrument_type_req",
						"",
						invoiceDef.getAttribute("linked_to_instrument_type_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td  >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.LinkedInsrtTy", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("linked_to_instrument_type_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="linked_to_instrument_type_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       
	        
	 <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.linked_to_instrument_type", TradePortalConstants.TEXT_BUNDLE)%>" name="linked_to_instrument_type" id="linked_to_instrument_type">
	 
	              <%=widgetFactory.createCheckboxField(
						"linked_to_instrument_type_data_req",
						"",
						invoiceDef.getAttribute("linked_to_instrument_type_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("invoice_type_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="invoice_type_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.invoice_type", TradePortalConstants.TEXT_BUNDLE)%>" name="invoice_type" id="invoice_type">
	
	              <%=widgetFactory.createCheckboxField(
						"invoice_type_req",
						"",
						invoiceDef.getAttribute("invoice_type_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td  >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.InvoiceType", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("invoice_type_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="invoice_type_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       
	 <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.invoice_type", TradePortalConstants.TEXT_BUNDLE)%>" name="invoice_type" id="invoice_type">
	 
	              <%=widgetFactory.createCheckboxField(
						"invoice_type_data_req",
						"",
						invoiceDef.getAttribute("invoice_type_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	<%-- Leelavathi Commented below code for spacing of the Credit Note Indicator field and added below code--%>       
	<%-- Leelavathi IR#T36000016795  Rel8400 17/09/2013 Begin--%>       
	      <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("credit_note_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_note_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.credit_note", TradePortalConstants.TEXT_BUNDLE)%>" name="credit_note" id="credit_note">
	
	              <%=widgetFactory.createCheckboxField(
						"credit_note_req",
						"",
						invoiceDef.getAttribute("credit_note_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.credit_note", false, false, false, "none")%><br>
			   	<%-- Nar IR-T36000026261 Release 8.4.0.0 Re-apply the missing code for Credit note text--%>
		        <%=widgetFactory.createTextField("CreditNoteIndText", "", invoiceDef.getAttribute("credit_note_ind_text") ,
			    		 "11", isReadOnly, false, false, "", "", "none") %>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "11", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("credit_note_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_note_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.credit_note", TradePortalConstants.TEXT_BUNDLE)%>" name="credit_note" id="credit_note">
	
	              <%=widgetFactory.createCheckboxField(
						"credit_note_data_req",
						"",
						invoiceDef.getAttribute("credit_note_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	<%-- Leelavathi IR#T36000016795  Rel8400 17/09/2013 End--%>
	       <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("goods_description_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="goods_description_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.goods_description", TradePortalConstants.TEXT_BUNDLE)%>" name="goods_description" id="goods_description">
	
	              <%=widgetFactory.createCheckboxField(
						"goods_description_req",
						"",
						invoiceDef.getAttribute("goods_description_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.GoodDesc", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "70", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("goods_description_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="goods_description_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.goods_description", TradePortalConstants.TEXT_BUNDLE)%>" name="goods_description" id="goods_description">
	
	              <%=widgetFactory.createCheckboxField(
						"goods_description_data_req",
						"",
						invoiceDef.getAttribute("goods_description_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("incoterm_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="incoterm_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.incoterm", TradePortalConstants.TEXT_BUNDLE)%>" name="incoterm" id="incoterm">
	
	              <%=widgetFactory.createCheckboxField(
						"incoterm_req",
						"",
						invoiceDef.getAttribute("incoterm_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.Incoterm", false, false, false, "none")%>
	 		</td>
	       <td align="right">
	 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("incoterm_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="incoterm_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.incoterm", TradePortalConstants.TEXT_BUNDLE)%>" name="incoterm" id="incoterm">
	
	              <%=widgetFactory.createCheckboxField(
						"incoterm_data_req",
						"",
						invoiceDef.getAttribute("incoterm_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("country_of_loading_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="country_of_loading_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.country_of_loading", TradePortalConstants.TEXT_BUNDLE)%>" name="country_of_loading" id="country_of_loading">
	
	              <%=widgetFactory.createCheckboxField(
						"country_of_loading_req",
						"",
						invoiceDef.getAttribute("country_of_loading_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.CountryOfLoading", false, false, false,
						"none")%>
	 		</td>
	       <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("country_of_loading_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="country_of_loading_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.country_of_loading", TradePortalConstants.TEXT_BUNDLE)%>" name="country_of_loading" id="country_of_loading">
	
	              <%=widgetFactory.createCheckboxField(
						"country_of_loading_data_req",
						"",
						invoiceDef.getAttribute("country_of_loading_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>          
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("country_of_discharge_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="country_of_discharge_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.country_of_discharge", TradePortalConstants.TEXT_BUNDLE)%>" name="country_of_discharge" id="country_of_discharge">
	
	              <%=widgetFactory.createCheckboxField(
						"country_of_discharge_req",
						"",
						invoiceDef.getAttribute("country_of_discharge_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.CountryOfDischarge", false, false,
						false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>          
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("country_of_discharge_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="country_of_discharge_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.country_of_discharge", TradePortalConstants.TEXT_BUNDLE)%>" name="country_of_discharge" id="country_of_discharge">
	
	              <%=widgetFactory.createCheckboxField(
						"country_of_discharge_data_req",
						"",
						invoiceDef.getAttribute("country_of_discharge_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("vessel_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="vessel_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.vessel", TradePortalConstants.TEXT_BUNDLE)%>" name="vessel" id="vessel">
	
	              <%=widgetFactory.createCheckboxField(
						"vessel_req",
						"",
						invoiceDef.getAttribute("vessel_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("", "InvoiceDefinition.Vessel",
						false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("vessel_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="vessel_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.vessel", TradePortalConstants.TEXT_BUNDLE)%>" name="vessel" id="vessel">
	
	              <%=widgetFactory.createCheckboxField(
						"vessel_data_req",
						"",
						invoiceDef.getAttribute("vessel_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("carrier_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="carrier_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.carrier", TradePortalConstants.TEXT_BUNDLE)%>" name="carrier" id="carrier">
	
	              <%=widgetFactory.createCheckboxField(
						"carrier_req",
						"",
						invoiceDef.getAttribute("carrier_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.Carrier", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("carrier_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="carrier_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.carrier", TradePortalConstants.TEXT_BUNDLE)%>" name="carrier" id="carrier">
	
	              <%=widgetFactory.createCheckboxField(
						"carrier_data_req",
						"",
						invoiceDef.getAttribute("carrier_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>              
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("actual_ship_date_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="actual_ship_date_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.actual_ship_date", TradePortalConstants.TEXT_BUNDLE)%>" name="actual_ship_date" id="actual_ship_date">
	
	              <%=widgetFactory.createCheckboxField(
						"actual_ship_date_req",
						"",
						invoiceDef.getAttribute("actual_ship_date_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory
						.createLabel("", "InvoiceDefinition.ActualShipDate", false,
								false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>              
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("actual_ship_date_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="actual_ship_date_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.actual_ship_date", TradePortalConstants.TEXT_BUNDLE)%>" name="actual_ship_date" id="actual_ship_date">
	
	              <%=widgetFactory.createCheckboxField(
						"actual_ship_date_data_req",
						"",
						invoiceDef.getAttribute("actual_ship_date_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td align='center'>             
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("payment_date_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="payment_date_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.payment_date", TradePortalConstants.TEXT_BUNDLE)%>" name="payment_date" id="payment_date">
	
	              <%=widgetFactory.createCheckboxField(
						"payment_date_req",
						"",
						invoiceDef.getAttribute("payment_date_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.PaymentDate", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
	 		  </td>
	 		        <td align='center'>             
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("payment_date_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="payment_date_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.payment_date", TradePortalConstants.TEXT_BUNDLE)%>" name="payment_date" id="payment_date">
	
	              <%=widgetFactory.createCheckboxField(
						"payment_date_data_req",
						"",
						invoiceDef.getAttribute("payment_date_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <%-- Narayan CR-913 30-Jan-2014 Rel9.0 Start --%>
           <tr id ="supplierToSendDate" style="display:<%=payVisible%>">
	       <td align='center'>             
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.send_to_supplier_date", TradePortalConstants.TEXT_BUNDLE)%>" name="send_to_supplier_date" id="send_to_supplier_date">	
	              <%=widgetFactory.createCheckboxField("send_to_supplier_date_req", "", TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("send_to_supplier_date_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       <td nowrap >
	             <%=widgetFactory.createLabel("", "InvoiceDefinition.SupplierToSendDate", false, false, false, "none")%>
	        </td>
	        <td align="right">
	             <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
	        </td>
	       <td align='center'>             
	             <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.send_to_supplier_date", TradePortalConstants.TEXT_BUNDLE)%>" name="send_to_supplier_date" id="send_to_supplier_date">	
	             <%=widgetFactory.createCheckboxField("send_to_supplier_date_data_req", "", TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("send_to_supplier_date_data_req")), isReadOnly, false, "", "", "none")%>
	       </td>
	       </tr>
	 	   <%-- Narayan CR-913 30-Jan-2014 Rel9.0 End --%>
	 	   <tr>
	       <td align='center'>              
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("purchase_ord_id_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="purchase_ord_id_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.purchase_ord_id", TradePortalConstants.TEXT_BUNDLE)%>" name="purchase_ord_id" id="purchase_ord_id">
	
	              <%=widgetFactory.createCheckboxField(
						"purchase_ord_id_req",
						"",
						invoiceDef.getAttribute("purchase_order_id_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceDefinition.PurchageOrdDate", false, false, false,
						"none")%>
	 		</td>
	       <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>              
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("purchase_ord_id_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="purchase_ord_id_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.purchase_ord_id", TradePortalConstants.TEXT_BUNDLE)%>" name="purchase_ord_id" id="purchase_ord_id">
	
	              <%=widgetFactory.createCheckboxField(
						"purchase_ord_id_data_req",
						"",
						invoiceDef.getAttribute("purchase_order_id_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <%-- Narayan CR914 Rel9.2 03-Nov-2014 Add- Begin --%>
           <tr id ="endToEndID" style="display:<%=payVisible%>">
	       <td align='center'>             
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.end_to_end_id", TradePortalConstants.TEXT_BUNDLE)%>" name="end_to_end_id" id="end_to_end_id">	
	              <%=widgetFactory.createCheckboxField("end_to_end_id_req", "", TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("end_to_end_id_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       <td nowrap >
	             <%=widgetFactory.createLabel("", "InvoiceDefinition.end_to_end_ID", false, false, false, "none")%>
	        </td>
	        <td align="right">
	             <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	        </td>
	       <td align='center'>             
	             <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.end_to_end_id", TradePortalConstants.TEXT_BUNDLE)%>" name="end_to_end_id" id="end_to_end_id">	
	             <%=widgetFactory.createCheckboxField("end_to_end_id_data_req", "", TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("end_to_end_id_data_req")), isReadOnly, false, "", "", "none")%>
	       </td>
	       </tr>
	 	   <%-- Narayan CR914 Rel9.2 03-Nov-2014 Add- end --%>
	</tbody>
	</table>
	
	
	<table>
	
	     <tr>
	     <td>&nbsp</td>
	     </tr>
	</table>
	
	<br/>
	
	<div>
	<table class="formDocumentsTable" id="InvBuyerDefinedTableId" style="width:97%;">
	<thead>
		<tr style="height:35px">
			<th colspan="4" style="text-align:left;font-weight: bolder;">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.BuyerUserDef")%>
			</th>
		</tr>
	</thead>
	<tbody>
	      <tr>
	      <td nowrap width="10">&nbsp;</td>
	      <td align=left width="130">
			<b><%=resMgr.getText("InvoiceDefinition.Label", TradePortalConstants.TEXT_BUNDLE)%></b>
	       </td>
	       <td width="20" align='left'>
			<b><%=resMgr.getText("InvoiceDefinition.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
	       </td>
	       <td  width="30" align='left'>
			  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoiceDefinition.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	       </tr>
	
	 	 <tr>
	 	      <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("buyer_users_def1_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="buyer_users_def1_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
						"buyer_users_def1_req",
						"",
						invoiceDef.getAttribute("buyer_users_def1_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	 		<td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								invoiceDef.getAttribute("buyer_users_def1_label"))) { %>
								<%=widgetFactory.createTextField("buyer_users_def1_label",
						"", invoiceDef.getAttribute("buyer_users_def1_label"),
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField("buyer_users_def1_label",
						"", invoiceDef.getAttribute("buyer_users_def1_label"),
						"35", isReadOnly, false, false, "width = '10'", "", "none")%>
	 		</td>
	 		<td  align='right'>
				<%=widgetFactory.createLabel("", "140", false, false, false,
						"none")%>
	 		 </td>
	 		 <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("buyer_users_def1_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="buyer_users_def1_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	 		  <%=widgetFactory.createCheckboxField(
						"buyer_users_def1_data_req",
						"",
						invoiceDef.getAttribute("buyer_users_def1_label_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	 	  </tr>
	
	 	  <tr>
	 	      <td  align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("buyer_users_def2_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="buyer_users_def2_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
						"buyer_users_def2_req",
						"",
						invoiceDef.getAttribute("buyer_users_def2_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	              <td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								invoiceDef.getAttribute("buyer_users_def2_label"))) { %>
								<%=widgetFactory.createTextField("buyer_users_def2_label",
						"", invoiceDef.getAttribute("buyer_users_def2_label"),
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField("buyer_users_def2_label",
						"", invoiceDef.getAttribute("buyer_users_def2_label"),
						"35", isReadOnly, false, false, "width = '10'", "", "none")%>
	 		  </td>
	 		  <td align='right'>
				<%=widgetFactory.createLabel("", "140", false, false, false,
						"none")%>
	 		  </td>
	 		  <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("buyer_users_def2_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="buyer_users_def2_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
						"buyer_users_def2_data_req",
						"",
						invoiceDef.getAttribute("buyer_users_def2_label_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	 	  </tr>
	<%
		counterTmp = 3;
	
		while (counterTmp <= 10) {
			currentDefinedLab = "buyer_users_def"
					+ (new Integer(counterTmp)).toString() + "_label";
			currentDefined = invoiceDef.getAttribute(currentDefinedLab);
			if (InstrumentServices.isBlank(currentDefined)) {
				break;
			}
			currentDefinedLabReq = "buyer_users_def"
					+ (new Integer(counterTmp)).toString() + "_req";
			currentDefinedReq = invoiceDef
					.getAttribute(currentDefinedLabReq);
			currentDefinedLabDataReq = "buyer_users_def"
				+ (new Integer(counterTmp)).toString() + "_data_req";
			temp = "buyer_users_def"
				+ (new Integer(counterTmp)).toString() + "_label_data_req";
			currentDefinedDataReq = invoiceDef
				.getAttribute(temp);
	
	%>
	 	  <tr>
	 	      <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								currentDefinedReq)) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="<%=currentDefinedLabReq%>"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <%=widgetFactory.createCheckboxField(
							currentDefinedLabReq, "",
							TradePortalConstants.INDICATOR_YES
									.equals(currentDefinedReq), isReadOnly, false,
							"", "", "none")%>
	              </td>
	              <td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								currentDefined)) { %>
								<%=widgetFactory.createTextField(currentDefinedLab,
						"", currentDefined,
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
	
			<%=widgetFactory.createTextField(currentDefinedLab,
						"", currentDefined,
						"35", isReadOnly, true, false, "width = '10'", "", "none")%>
	 		  </td>
	 		  <td align='right'>
				<%=widgetFactory.createLabel("", "140", false, false,
							false, "none")%>
	 		  </td>
	 		   <td  align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								currentDefinedDataReq)) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="<%=currentDefinedLabDataReq%>"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <%=widgetFactory.createCheckboxField(
							currentDefinedLabDataReq, "",
							TradePortalConstants.INDICATOR_YES
									.equals(currentDefinedDataReq), isReadOnly, false,
							"", "", "none")%>
	              </td>
	 	  </tr>
	<%
		counterTmp++;
		}
	%>
	
	</tbody>
	</table>
	<%
		if (!(isReadOnly)) {
	%>
		<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreDefined">
		<%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		local.add4MoreDefined('InvBuyerDefinedTableId');
		</script>
		</button>
		<%=widgetFactory.createHoverHelp("add4MoreDefined","POStructure.Add4More") %>
	<%
		} else {
	%>
						&nbsp;
	<%
		}
	%>
	</div>
	<br/>
	<p>
	
	<%-- seller --%>
	<div>
	<table class="formDocumentsTable" id="InvSellerDefinedTableId" style="width:97%;">
	<thead>
		<tr style="height:35px">
			<th colspan=4 style="text-align:left">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.SellerUserDef")%>
			</th>
		</tr>
	</thead>
	<tbody>
	      <tr>
	      <td nowrap width="10">&nbsp;</td>
	      <td align=left width="130">
			<b><%=resMgr.getText("InvoiceDefinition.Label", TradePortalConstants.TEXT_BUNDLE)%></b>
	       </td>
	       <td width="20" align='left'>
			<b><%=resMgr.getText("InvoiceDefinition.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
	       </td>
	       <td  width="30" align='left'>
			  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoiceDefinition.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	       </tr>
	 	 <tr>
	 	      <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("seller_users_def1_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="seller_users_def1_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
						"seller_users_def1_req",
						"",
						invoiceDef.getAttribute("seller_users_def1_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	 		<td >
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								invoiceDef.getAttribute("seller_users_def1_label"))) { %>
								<%=widgetFactory.createTextField("seller_users_def1_label",
						"", invoiceDef.getAttribute("seller_users_def1_label"),
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField("seller_users_def1_label",
						"", invoiceDef.getAttribute("seller_users_def1_label"),
						"35", isReadOnly, false, false, "width = '10'", "", "none")%>
	 		</td>
	 		<td align='right'>
				<%=widgetFactory.createLabel("", "140", false, false, false,
						"none")%>
	 		 </td>
	 		  <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("seller_users_def1_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="seller_users_def1_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	
	              <%=widgetFactory.createCheckboxField(
						"seller_users_def1_data_req",
						"",
						invoiceDef.getAttribute("seller_users_def1_label_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	 	  </tr>
	 	  <tr>
	 	      <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("seller_users_def2_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="seller_users_def2_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
						"seller_users_def2_req",
						"",
						invoiceDef.getAttribute("seller_users_def2_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	              <td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								invoiceDef.getAttribute("seller_users_def2_label"))) { %>
								<%=widgetFactory.createTextField("seller_users_def2_label",
						"", invoiceDef.getAttribute("seller_users_def2_label"),
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField("seller_users_def2_label",
						"", invoiceDef.getAttribute("seller_users_def2_label"),
						"35", isReadOnly, false, false, "width = '10'", "", "none")%>
	 		  </td>
	 		  <td align='right'>
				<%=widgetFactory.createLabel("", "140", false, false, false,
						"none")%>
	 		  </td>
	 		  <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("seller_users_def2_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="seller_users_def2_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
						"seller_users_def2_data_req",
						"",
						invoiceDef.getAttribute("seller_users_def2_label_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	              </td>
	 	  </tr>
	<%
		counterTmp = 3;
		temp="";
		while (counterTmp <= 10) {
			currentDefinedLab = "seller_users_def"
					+ (new Integer(counterTmp)).toString() + "_label";
			currentDefined = invoiceDef.getAttribute(currentDefinedLab);
			if (InstrumentServices.isBlank(currentDefined)) {
				break;
			}
			currentDefinedLabReq = "seller_users_def"
					+ (new Integer(counterTmp)).toString() + "_req";
			currentDefinedReq = invoiceDef
					.getAttribute(currentDefinedLabReq);
			
			currentDefinedLabDataReq = "seller_users_def"
				+ (new Integer(counterTmp)).toString() + "_data_req";
			temp = "seller_users_def"
				+ (new Integer(counterTmp)).toString() + "_label_data_req";
			currentDefinedDataReq = invoiceDef
				.getAttribute(temp);
	%>
	 	  <tr>
	 	      <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								currentDefinedReq)) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="<%=currentDefinedLabReq%>"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
							currentDefinedLabReq, "",
							TradePortalConstants.INDICATOR_YES
									.equals(currentDefinedReq), isReadOnly, false,
							"", "", "none")%>
	              </td>
	              <td>
	 		  <% if (isReadOnly && StringFunction.isNotBlank(
								currentDefined)) { %>
								<%=widgetFactory.createTextField(currentDefinedLab,
						"", currentDefined,
						"35", false, true, false, "width = '10' style='display:none;'", "", "none")%>
	 			 
	 		  <% } %>
			<%=widgetFactory.createTextField(currentDefinedLab, "",
							currentDefined, "35", isReadOnly, true, false, "width = '10'", "", "none")%>
	 		  </td>
	 		  <td align='right'>
				<%=widgetFactory.createLabel("", "140", false, false,
							false, "none")%>
	 		  </td>
	 		   <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								currentDefinedDataReq)) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="<%=currentDefinedLabDataReq%>"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <%=widgetFactory.createCheckboxField(
							currentDefinedLabDataReq, "",
							TradePortalConstants.INDICATOR_YES
									.equals(currentDefinedDataReq), isReadOnly, false,
							"", "", "none")%>
	              </td>
	 	  </tr>
	<%
		counterTmp++;
		}
	%>
	
	</tbody>
	</table>
	
	<%
		if (!(isReadOnly)) {
	%>
		<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreSellDefined">
		<%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		local.add4MoreDefined('InvSellerDefinedTableId');
		</script>
		</button>
		<%=widgetFactory.createHoverHelp("add4MoreSellDefined","POStructure.Add4More") %>
	<%
		} else {
	%>
						&nbsp;
	<%
		}
	%>
	
	</div>
	
	<%-- Rel8.2 IR T36000014892 04/01/2013 --%>
	<%-- AAlubala Rel8.2 CR741 Credit Note Fields - BEGIN --%>
	<br><br>
	<div>
	<table class="formDocumentsTable" id="InvCreditNoteTableId" style="width:98%;display:<%=recVisible%>;">
	<thead>
		<tr style="height:35px">
			<th colspan=4 style="text-align:left;">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.CreditNoteDataFields")%>
			</th>
		</tr>
	</thead>
	<tbody>
	 
	      <tr >
	      	 <td nowrap width="15" >&nbsp;</td>
	 		 <td nowrap width="130"  >
			  	<b><%=resMgr.getText("InvoiceDefinition.Label", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		  </td>
	 		 <td  width="20" align="center" >
			  	<b><%=resMgr.getText("InvoiceDefinition.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	 		 <td width="30" >
			  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoiceDefinition.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	      </tr>  
	<%-- Leelavathi commented below code  because spacing in the credit note data fields and added below code --%>         
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>
	     
	     <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("discount_code_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_discount_code_id_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.discount_code", TradePortalConstants.TEXT_BUNDLE)%>" name="credit_discount_code_id" id="credit_discount_code_id">
	
	              <%=widgetFactory.createCheckboxField(
						"credit_discount_code_id_req",
						"",
						invoiceDef.getAttribute("discount_code_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.discount_code", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("discount_code_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_discount_code_id_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	
	              <%=widgetFactory.createCheckboxField(
						"credit_discount_code_id_data_req",
						"",
						invoiceDef.getAttribute("discount_code_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	    
	    
	    <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("discount_gl_code_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_discount_gl_code_id_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.discount_gl_code", TradePortalConstants.TEXT_BUNDLE)%>" name="credit_discount_gl_code_id" id="credit_discount_gl_code_id">
	
	              <%=widgetFactory.createCheckboxField(
						"credit_discount_gl_code_id_req",
						"",
						invoiceDef.getAttribute("discount_gl_code_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.discount_gl_code", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "17", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("discount_gl_code_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_discount_gl_code_id_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.discount_gl_code", TradePortalConstants.TEXT_BUNDLE)%>" name="credit_discount_gl_code_id" id="credit_discount_gl_code_id">
	
	              <%=widgetFactory.createCheckboxField(
						"credit_discount_gl_code_id_data_req",
						"",
						invoiceDef.getAttribute("discount_gl_code_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	    
	    <tr>
	       <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("discount_comments_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_discount_comments_id_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.discount_comments", TradePortalConstants.TEXT_BUNDLE)%>" name="credit_discount_comments_id" id="credit_discount_comments_id">
	
	              <%=widgetFactory.createCheckboxField(
						"credit_discount_comments_id_req",
						"",
						invoiceDef.getAttribute("discount_comments_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	 		<td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.discount_comments", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "100", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("discount_comments_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="credit_discount_comments_id_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.discount_comments", TradePortalConstants.TEXT_BUNDLE)%>" name="credit_discount_comments_id" id="credit_discount_comments_id">
	
	              <%=widgetFactory.createCheckboxField(
						"credit_discount_comments_id_data_req",
						"",
						invoiceDef.getAttribute("discount_comments_data_req").equals(
								TradePortalConstants.INDICATOR_YES), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>            
	 <%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>      
	</tbody>
	</table>
	</div>
	
	<%-- CR741 - END --%>
	
	
	<%-- DK CR709 Rel8.2 Starts --%>
	<br/><br/>
	<p>
	<div>
	<table class="formDocumentsTable" id="InvPayInstrucTableId">
	<thead>
		<tr style="height:35px">
			<th colspan=4 style="text-align:left;">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.PayInst")%>
			</th>
		</tr>
	</thead>
	<tbody>
	      <tr >
	      	 <td nowrap width="15" >&nbsp;</td>
	 		 <td nowrap width="130"  >
			  	<b><%=resMgr.getText("InvoiceDefinition.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		  </td>
	 		 <td  width="20" align="center" >
			  	<b><%=resMgr.getText("InvoiceDefinition.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	 		 <td width="30" >
			  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("InvoiceDefinition.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
	 		 </td>
	      </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("pay_method_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="pay_method_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.pay_method", TradePortalConstants.TEXT_BUNDLE)%>" name="pay_method" id="pay_method">
	              <%=widgetFactory.createCheckboxField("pay_method_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("pay_method_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	       
	        <td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.pay_method", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "4", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("pay_method_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="pay_method_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.pay_method", TradePortalConstants.TEXT_BUNDLE)%>" name="pay_method" id="pay_method">
	              <%=widgetFactory.createCheckboxField(
						"pay_method_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("pay_method_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_acct_num_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_acct_num_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_acct_num", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_acct_num" id="ben_acct_num">       
	              <%=widgetFactory.createCheckboxField("ben_acct_num_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_acct_num_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	       <%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>
	 		
	       <td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.ben_acct_num", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "34", false, false, false, "none") %>
	 		  </td>
	 		   <%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_acct_num_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_acct_num_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_acct_num", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_acct_num" id="ben_acct_num"> 
	              <%=widgetFactory.createCheckboxField(
						"ben_acct_num_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_acct_num_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_add1_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_add1_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_add1", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_add1" id="ben_add1">
	              <%=widgetFactory.createCheckboxField("ben_add1_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_add1_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	        <%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>
	 		
	        <td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.ben_add1", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	 		   <%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	         <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_add1_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_add1_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	         <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_add1", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_add1" id="ben_add1">
	              <%=widgetFactory.createCheckboxField(
						"ben_add1_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_add1_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_add2_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_add2_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_add2", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_add2" id="ben_add2">       
	              <%=widgetFactory.createCheckboxField("ben_add2_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_add2_req")),
	                            isReadOnly, false,  "", "", "none")%>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>                            
	      
	        <td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.ben_add2", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_add2_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_add2_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_add2", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_add2" id="ben_add2">  
	              <%=widgetFactory.createCheckboxField(
						"ben_add2_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_add2_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_add3_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_add3_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_add3", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_add3" id="ben_add3">       
	              <%=widgetFactory.createCheckboxField("ben_add3_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_add3_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.ben_add3", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
	 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_add3_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_add3_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_add3", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_add3" id="ben_add3">  
	              <%=widgetFactory.createCheckboxField(
						"ben_add3_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_add3_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_email_addr_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_email_addr_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	       <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_email_addr", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_email_addr" id="ben_email_addr">       
	              <%=widgetFactory.createCheckboxField("ben_email_addr_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_email_addr_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>

	        <td nowrap >
				<%=widgetFactory.createLabel("",
						"InvoiceUploadRequest.ben_email_addr", false, false, false, "none")%>
	 		</td>
	        <td align="right">
	 			<%= widgetFactory.createLabel("", "255", false, false, false, "none") %>
	 		  </td>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_email_addr_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_email_addr_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_email_addr", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_email_addr" id="ben_email_addr">  
	              <%=widgetFactory.createCheckboxField(
						"ben_email_addr_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_email_addr_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_country_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_country_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_country", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_country" id="ben_country">
	              <%=widgetFactory.createCheckboxField("ben_country_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_country_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>        
	 		
			<td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_country", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>        
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_country_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_country_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_country", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_country" id="ben_country">
	              <%=widgetFactory.createCheckboxField(
						"ben_country_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_country_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_name_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_name_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_name", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_name" id="ben_bank_name">
	              <%=widgetFactory.createCheckboxField("ben_bank_name_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_bank_name_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_bank_name", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "120", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%> 
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_name_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_name_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_name", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_name" id="ben_bank_name">
	              <%=widgetFactory.createCheckboxField(
						"ben_bank_name_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_bank_name_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	      
	       
	 
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_code_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_code_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_code", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_code" id="ben_branch_code">       
	              <%=widgetFactory.createCheckboxField("ben_branch_code_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_branch_code_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_branch_code", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%> 
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_code_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_code_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_code", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_code" id="ben_branch_code">
	              <%=widgetFactory.createCheckboxField(
						"ben_branch_code_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_branch_code_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_sort_code_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_sort_code_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_sort_code", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_sort_code" id="ben_bank_sort_code">
	              <%=widgetFactory.createCheckboxField("ben_bank_sort_code_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_bank_sort_code_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>

	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_bank_sort_code", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "8", false, false, false, "none") %>
			 		  </td>

	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_sort_code_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_sort_code_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_sort_code", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_sort_code" id="ben_bank_sort_code">
	              <%=widgetFactory.createCheckboxField(
						"ben_bank_sort_code_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_bank_sort_code_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_add1_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_add1_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_add1", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_add1" id="ben_branch_add1">       
	              <%=widgetFactory.createCheckboxField("ben_branch_add1_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_branch_add1_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	 <%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_branch_add1", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%> 
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_add1_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_add1_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	         <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_add1", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_add1" id="ben_branch_add1">      
	              <%=widgetFactory.createCheckboxField(
						"ben_branch_add1_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_branch_add1_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_add2_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_add2_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_add2", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_add2" id="ben_branch_add2">       
	              <%=widgetFactory.createCheckboxField("ben_branch_add2_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_branch_add2_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_branch_add2", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%> 
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_add2_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_add2_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_add2", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_add2" id="ben_branch_add2"> 
	              <%=widgetFactory.createCheckboxField(
						"ben_branch_add2_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_add2_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_city_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_city_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_city", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_city" id="ben_bank_city">       
	              <%=widgetFactory.createCheckboxField("ben_bank_city_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_bank_city_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>        
	 	
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_bank_city", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "31", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%> 
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_city_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_city_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_city", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_city" id="ben_bank_city">
	              <%=widgetFactory.createCheckboxField(
						"ben_bank_city_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_bank_city_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_province_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_province_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_province", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_province" id="ben_bank_province">       
	              <%=widgetFactory.createCheckboxField("ben_bank_province_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_bank_province_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_bank_province", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "8", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%> 
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_bank_province_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_bank_province_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_bank_province", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_bank_province" id="ben_bank_province">   
	              <%=widgetFactory.createCheckboxField(
						"ben_bank_province_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_bank_province_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_country_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_country_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_country", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_country" id="ben_branch_country">       
	              <%=widgetFactory.createCheckboxField("ben_branch_country_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("ben_branch_country_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.ben_branch_country", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%> 
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("ben_branch_country_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="ben_branch_country_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.ben_branch_country", TradePortalConstants.TEXT_BUNDLE)%>" name="ben_branch_country" id="ben_branch_country"> 
	              <%=widgetFactory.createCheckboxField(
						"ben_branch_country_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("ben_branch_country_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("charges_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="charges_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	                     <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.charges", TradePortalConstants.TEXT_BUNDLE)%>" name="charges" id="charges">
	              <%=widgetFactory.createCheckboxField("charges_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("charges_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.charges", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("charges_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="charges_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.charges", TradePortalConstants.TEXT_BUNDLE)%>" name="charges" id="charges">
	              <%=widgetFactory.createCheckboxField(
						"charges_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("charges_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("central_bank_rep1_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="central_bank_rep1_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	                     <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.central_bank_rep1", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep1" id="central_bank_rep1">       
	              <%=widgetFactory.createCheckboxField("central_bank_rep1_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("central_bank_rep1_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.central_bank_rep1", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("central_bank_rep1_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="central_bank_rep1_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	         <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.central_bank_rep1", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep1" id="central_bank_rep1">  
	              <%=widgetFactory.createCheckboxField(
						"central_bank_rep1_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("central_bank_rep1_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>        
	       </tr>
	       <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("central_bank_rep2_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="central_bank_rep2_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.central_bank_rep2", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep2" id="central_bank_rep2">
	              <%=widgetFactory.createCheckboxField("central_bank_rep2_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("central_bank_rep2_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 Begin--%>       
	 		
	        <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.central_bank_rep2", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("central_bank_rep2_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="central_bank_rep2_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.central_bank_rep2", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep2" id="central_bank_rep2">
	              <%=widgetFactory.createCheckboxField(
						"central_bank_rep2_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("central_bank_rep2_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	        <tr>
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("central_bank_rep3_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="central_bank_rep3_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	                     <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.central_bank_rep3", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep3" id="central_bank_rep3">       
	              <%=widgetFactory.createCheckboxField("central_bank_rep3_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("central_bank_rep3_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>       
	 	       <td nowrap >
						<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest.central_bank_rep3", false, false, false, "none")%>
			 		</td>
			        <td align="right">
			 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
			 		  </td>
	<%-- Leelavathi IR#T36000016795  Rel8400 18/09/2013 End--%>
	        <td align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("central_bank_rep3_data_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="central_bank_rep3_data_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	        <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.central_bank_rep3", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep3" id="central_bank_rep3">    
	              <%=widgetFactory.createCheckboxField(
						"central_bank_rep3_data_req",
						"",
						TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("central_bank_rep3_data_req")), isReadOnly,
						false, "", "", "none")%>
	       </td>
	       </tr>
	       <%-- Narayan CR913 Rel9.0 15-Feb-2014 ADD - Begin --%>
	       <tr id ="buyerAcctNum" style="display:<%=payVisible%>">
	       <td align='center'>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.buyer_acct_num", TradePortalConstants.TEXT_BUNDLE)%>" name="buyer_acct_num" id="buyer_acct_num">       
	              <%=widgetFactory.createCheckboxField("buyer_acct_num_req", "", TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("buyer_acct_num_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	        <td>
				  <%=widgetFactory.createLabel("","InvoiceUploadRequest.buyer_acct_num", false, false, false, "none")%>
			</td>
			 <td align='right'>
			 	   <%= widgetFactory.createLabel("", "30", false, false, false, "none") %>
			 </td>
	        <td align='center'>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.buyer_acct_num", TradePortalConstants.TEXT_BUNDLE)%>" 
	              name="buyer_acct_num" id="buyer_acct_num">    
	              <%=widgetFactory.createCheckboxField("buyer_acct_num_data_req","", 
	            		  TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("buyer_acct_num_data_req")), isReadOnly,
                          false, "", "", "none")%>
	       </td>
	       </tr>
	       
	       <tr id ="buyerAcctCurr" style="display:<%=payVisible%>">
	       <td align='center'>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.buyer_acct_currency", TradePortalConstants.TEXT_BUNDLE)%>" 
	              name="buyer_acct_currency" id="buyer_acct_currency">       
	              <%=widgetFactory.createCheckboxField("buyer_acct_currency_req", "", 
	            		  TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("buyer_acct_currency_req")), isReadOnly, false,  "", "", "none")%>
	       </td>
	        <td>
				  <%=widgetFactory.createLabel("","InvoiceUploadRequest.buyer_acct_currency", false, false, false, "none")%>
			</td>
			 <td align='right'>
			 	   <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
			 </td>
	        <td align='center'>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.buyer_acct_currency", TradePortalConstants.TEXT_BUNDLE)%>" 
	              name="buyer_acct_currency" id="buyer_acct_currency">    
	              <%=widgetFactory.createCheckboxField("buyer_acct_currency_data_req","", 
	            		  TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("buyer_acct_currency_data_req")), isReadOnly,
                          false, "", "", "none")%>
	       </td>
	       </tr>
	       <%-- Narayan CR913 Rel9.0 15-Feb-2014 ADD - End --%>
	       
	       <%--Rel9.4 CR 1001 Starts --%>
	       <tr id = "repoatingCode1" style="display:<%=payVisible%>">
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("reporting_code_1_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="reporting_code_1_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	                     <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.reporting_code_1", TradePortalConstants.TEXT_BUNDLE)%>" name="reporting_code_1" id="reporting_code_1">       
	              <%=widgetFactory.createCheckboxField("reporting_code_1_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("reporting_code_1_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	     
	      
	      
	        <td nowrap >
				  <%=widgetFactory.createLabel("","InvoiceUploadRequest.reporting_code_1", false, false, false, "none")%>
			</td>
			 <td align='right'>
			 	   <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
			 </td>
	        <td align='center'>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.reporting_code_1", TradePortalConstants.TEXT_BUNDLE)%>" 
	              name="reporting_code1" id="reporting_code1">    
	              <%=widgetFactory.createCheckboxField("reporting_code_1_data_req","", 
	            		  TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("reporting_code_1_data_req")), isReadOnly,
                          false, "", "", "none")%>
	       </td>
	       </tr>
	       <tr id = "repoatingCode2" style="display:<%=payVisible%>">
	       <td nowrap class=ListText width="30" align='center'>
	 		  <% if (isReadOnly && TradePortalConstants.INDICATOR_YES.equals(
								invoiceDef.getAttribute("reporting_code_2_req"))) { %>
	 			 <input data-dojo-type="dijit.form.CheckBox" name="reporting_code_2_req"  value="Y"  checked style="display:none;" />
	 		  <% } %>
	                     <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.reporting_code_2", TradePortalConstants.TEXT_BUNDLE)%>" name="reporting_code_2" id="reporting_code_2">       
	              <%=widgetFactory.createCheckboxField("reporting_code_2_req", "",
	            		  TradePortalConstants.INDICATOR_YES.
	                                    equals(invoiceDef.getAttribute("reporting_code_2_req")),
	                            isReadOnly, false,  "", "", "none")%>
	       </td>
	     
	        <td>
				  <%=widgetFactory.createLabel("","InvoiceUploadRequest.reporting_code_2", false, false, false, "none")%>
			</td>
			 <td align='right'>
			 	   <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
			 </td>
	        <td align='center'>
	              <input type=hidden value="<%=resMgr.getText("InvoiceUploadRequest.reporting_code_2", TradePortalConstants.TEXT_BUNDLE)%>" 
	              name="reporting_code2" id="reporting_code2">    
	              <%=widgetFactory.createCheckboxField("reporting_code_2_data_req","", 
	            		  TradePortalConstants.INDICATOR_YES.equals(invoiceDef.getAttribute("reporting_code_2_data_req")), isReadOnly,
                          false, "", "", "none")%>
	       </td>
	       </tr>
	       <%--Rel9.4 CR 1001 Ends --%>
	</tbody>
	</table>
	</div>
	<%-- DK CR709 Rel8.2 Ends --%>
	</div>
	
	<div class = "columnRight">
	
	<%=widgetFactory.createLabel("",
						"INVUploadDefinitionDetail.DefSep2", false, false, false,
						"none")%>
	
	<table class="formDocumentsTable" id="InvDefFileOrderTableId" >
	<thead>
		<tr style="height:45px">
			<th colspan=2 style="text-align:left">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.InvOrder")%>
			</th>
		</tr>
	</thead>
	
	<tbody>
	
	      <tr>
	 		 <td width="20" >
			  	<%=widgetFactory.createLabel("",
						"POStructureDefinition.Order", false, false, false, "none")%>
	 		  </td> 		 
	 		  <td width="180">
			  	<%=widgetFactory.createLabel("",
						"POStructureDefinition.FieldName", false, false, false, "none")%>
	 		  </td>
	       </tr>
	
	<%
	//RKAZI Rel 8.2 T36000011727 - START 
	//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
	//NOTE: please do not reformat the code. 
		String[] initialVal = { "invoice_id", "issue_date", "due_date", "currency", "amount" };
		boolean initialFlag = false;
		if (InstrumentServices.isBlank(invoiceDef
				.getAttribute("inv_summary_order1")))
			initialFlag = true;
		String currentValue;
	
		if (initialFlag) {
			for (counterTmp = 1; counterTmp <= initialVal.length; counterTmp++) {
	
				currentValue = initialVal[counterTmp - 1];
	%>
	
	 	  <tr> 		  	
	 		 <td  align="center">
	   			<input type="text" name="" value='<%=counterTmp%>'
	   				data-dojo-type="dijit.form.TextBox" 
	   				id='text<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' /><input type=hidden id="upload_order_<%=counterTmp%>" name="inv_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValue%>" />
	 		  </td>
	          <td id='fileFieldName<%=counterTmp%>' >
				<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest." + currentValue, false,
								false, false, "none")%>
	
	 		  </td>
		  </tr>
	<%
		}
		} else {
			counterTmp = 1;
			// invoice summary data in upload invoice file has maximum 34 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order34)
			while ((counterTmp <= 34)
					&& InstrumentServices
							.isNotBlank(invoiceDef
									.getAttribute("inv_summary_order"
											+ (new Integer(counterTmp))
													.toString()))) {
	
				currentValue = invoiceDef.getAttribute("inv_summary_order"
						+ (new Integer(counterTmp)).toString());
	%>
	
	   	   <tr id= "<%=currentValue%>_req_order">
	 		 <td  align="center">
	   			<input type="text" name="" value='<%=counterTmp%>'
	   				data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
	   				id='text<%=counterTmp%>'  style="width: 25px;" '<%=isReadOnly%>' /><input type=hidden id="upload_order_<%=counterTmp%>" name="inv_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValue%>" />
	 		  </td>
	          <td id='fileFieldName<%=counterTmp%>' align="left">
				<%=widgetFactory.createLabel("",
								"InvoiceUploadRequest." + currentValue, false,
								false, false,"labelClass=\"invOrderLabel\"", "none")%>
				<% if(invSummOptinalVal.contains(currentValue)){ %>				
				<span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='<%=currentValue%>'></span>
	           <%} %>
	 		  </td>
		   </tr>
	
	<%
		counterTmp++;
			}
		}
	%>
	</tbody>
	</table>
	<div>
	<%-- invSummDataOrder variable stores the total invoice summary order present for file upload order --%>
	<%
		invSummDataOrder = counterTmp;
		if (!(isReadOnly)) {
	%>
	<button data-dojo-type="dijit.form.Button" type="button" id="updateSysDefOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			local.updateDefOrder(document.getElementById("InvDefFileOrderTableId").rows.length, 'text');
		</script>
	</button>
	<%
		} else {
	%>
						&nbsp;
	<%
		}
	%>
	
	</div>
	<br/><br/>
	
	<table class="formDocumentsTable" id="poStructSumDataOrderTableId" >
	<thead>
		<tr style="height:35px">
			<th colspan=2 style="text-align:left;font: bold;">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.InvGoodsOrder")%>
			</th>
		</tr>
	</thead>
	<tbody>
	
	      <tr>      
	 		 <td width="20" >
			  	<%=widgetFactory.createLabel("",
						"POStructureDefinition.Order", false, false, false, "none")%>
	 		  </td>
	 		  <td width="180" >
			  	<%=widgetFactory.createLabel("",
						"POStructureDefinition.FieldName", false, false, false, "none")%>
	 		  </td>
	       </tr>
	
	<%
		//Draw theAssigment Ordre Table
		String invSumValue = null;
		String invSumField = null;
		String invSumReq = TradePortalConstants.INDICATOR_YES;
		int reStartIndex = 1;
		String allFields = "";
		String indexLabel = "";
		// invoice summary goods has maximum 28 option in invoice file upload (i.e. inv_goods_order1, inv_goods_order2, ...inv_goods_order28)
		while ((reStartIndex <= 28)
				&& InstrumentServices.isNotBlank(invoiceDef
						.getAttribute("inv_goods_order"
								+ (new Integer(reStartIndex)).toString()))) {
	
			invSumField = invoiceDef.getAttribute("inv_goods_order"
					+ (new Integer(reStartIndex)).toString());
			// all user defined field are saved in database as 'buyer_users_def1_label' or 'seller_users_def1_label' to get the label entered by user
			//while others label gets from property.
			if (invSumField.contains("users_")) {
				invSumValue = invoiceDef.getAttribute(invSumField);
			} else {
				invSumValue = "InvoiceUploadRequest."+ invoiceDef.getAttribute("inv_goods_order"+ (new Integer(reStartIndex)).toString());
			}
	
			int indexOfLabel = invSumField.indexOf("_label"); // user entered fields
			if (indexOfLabel > -1) {
				indexLabel = invSumField.substring(0, indexOfLabel)+ "_req";
				invSumReq = invoiceDef.getAttribute(indexLabel);
			}else{			
				if("country_of_loading".equals(invSumField)){
					invSumField = "country_of_loading";
				}else if("purchase_ord_id".equals(invSumField)){
					invSumField = "purchase_order_id";
				}
				indexLabel = invSumField + "_req";
				invSumReq = invoiceDef.getAttribute(indexLabel);
			}
	
			if ((TradePortalConstants.INDICATOR_YES.equals(invSumReq))
					&& (InstrumentServices.isNotBlank(invSumValue))) {
				allFields = StringFunction.xssCharsToHtml(allFields + invSumField);
	%>
	 	  <tr id= "<%=StringFunction.xssCharsToHtml(indexLabel)%>_order">
	 		 <td  align="center">
	 		     <input type="text" name="" value='<%=counterTmp%>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
	   				id='userDefText<%=reStartIndex%>'  style="width: 25px;" '<%=isReadOnly%>' /><input type=hidden id="upload_userdef_order_<%=reStartIndex%>"
	 		          name="inv_goods_order<%=(reStartIndex)%>"
	 		          value="<%=invoiceDef.getAttribute("inv_goods_order"
								+ (new Integer(reStartIndex)).toString())%>" />
	   			 
	 		  </td>
	 		  <td id='userDefFileFieldName<%=reStartIndex%>' align="left">
			  	<%=widgetFactory.createLabel("", invSumValue, false,
								false, false,"labelClass=\"invOrderLabel\"", "none")%>			
				<span class="deleteSelectedItem" style="margin:0.3em 1em" id='<%=invoiceDef.getAttribute("inv_goods_order"
								+ (new Integer(reStartIndex)).toString())%>'></span>
	 		  </td>
	 	  </tr>
	<%
		counterTmp++;
			}
			reStartIndex++;
		}
		//RKAZI Rel 8.2 T36000011727 - END
	%>
	</tbody>
	</table>
	
	<%
		invSummGoodOrder = counterTmp;
		if (!(isReadOnly)) {
	%>
	<button data-dojo-type="dijit.form.Button" type="button" id="updateUserDefOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			local.updateSumDefOrder(document.getElementById("poStructSumDataOrderTableId").rows.length, 
	                              document.getElementById("InvDefFileOrderTableId").rows.length, 'sumDataPos');
		</script>
	</button>
	<%
		} else {
	%>
						&nbsp;
	<%
		}
	%>
	
	
	<br><br>
	<%-- AAlubala - Rel8.2 CR741 Discount codes START--%>
	
	<table class="formDocumentsTable" id="discountCodeDataOrderTableId" style="display:<%=recVisible%>;">
	<thead>
		<tr style="height:35px">
			<th colspan=2 style="text-align:left;font: bold;">
				<%=widgetFactory
						.createSubLabel("INVUploadDefinitionDetail.CreditNoteDataOrder")%>
			</th>
		</tr>
	</thead>
	<tbody>
	
	      <tr>      
	 		 <td width="20" >
			  	<%=widgetFactory.createLabel("",
						"POStructureDefinition.Order", false, false, false, "none")%>
	 		  </td>
	 		  <td width="180" >
			  	<%=widgetFactory.createLabel("",
						"POStructureDefinition.FieldName", false, false, false, "none")%>
	 		  </td>
	       </tr>
	
	<%
		//The order assignment table for Credit Note
		String invSumValue1 = null;
		String invSumField1 = null;
		String invSumReq1 = TradePortalConstants.INDICATOR_YES;
		int reStartIndex01 = 1;
		String allFields01 = "";
		String indexLabel01 = "";
		String beanValue = "";
		String preValueString = "";
		// invoice credit note has maximum 3 option in invoice file upload (i.e. inv_credit_order1, inv_credit_order2,inv_credit_order3)
		while ((reStartIndex01 <= TradePortalConstants.INV_CREDIT_LINE_ORDER_FIELDS)
				&& InstrumentServices.isNotBlank(invoiceDef
						.getAttribute("inv_credit_order"
								+ (new Integer(reStartIndex01)).toString()))) {
			
			invSumField1 = invoiceDef.getAttribute("inv_credit_order"
					+ (new Integer(reStartIndex01)).toString());
			
				if("credit_discount_code_id".equals(invSumField1)){
					preValueString = "discount_code";
				}else if("credit_discount_gl_code_id".equals(invSumField1)){
					preValueString = "discount_gl_code";
				}else if("credit_discount_comments_id".equals(invSumField1)){
					preValueString = "discount_comments";
				}
	
				indexLabel01 = invSumField1 + "_req";
				beanValue = preValueString + "_req";
				
				invSumReq1 = invoiceDef.getAttribute(beanValue);
	
		invSumValue1 = "InvoiceUploadRequest."+preValueString;
	
			if ((TradePortalConstants.INDICATOR_YES.equals(invSumReq1))
					&& (InstrumentServices.isNotBlank(invSumValue1))) {
				
				%>
			 	  <tr id= "<%=StringFunction.xssCharsToHtml(indexLabel01)%>_order">
			 		 <td  align="center">
			   			 <input type="text" name="" value='<%=counterTmp%>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
			   				id='1userDefText<%=reStartIndex01%>'  style="width: 25px;" '<%=isReadOnly%>'><input type=hidden id="1upload_userdef_order_<%=reStartIndex01%>"
			 		          name="inv_credit_order<%=(reStartIndex01)%>"
			 		          value="<%=invoiceDef.getAttribute("inv_credit_order"
										+ (new Integer(reStartIndex01)).toString())%>">
			 		  </td>
			 		  <td id='1userDefFileFieldName<%=reStartIndex01%>' align="left">
					  	<%=widgetFactory.createLabel("", invSumValue1, false,
										false, false,"labelClass=\"invOrderLabel\"", "none")%>			
						<span class="deleteSelectedItem" style="margin:0.3em 1em" id='<%=invoiceDef.getAttribute("inv_credit_order"
										+ (new Integer(reStartIndex01)).toString())%>'></span>
			 		  </td>
			 	  </tr>
			<%
		counterTmp++;
			}
			reStartIndex01++;
		}
	%>
	</tbody>
	</table>
	
	
	<%-- CR741 END --%>
	
	 <input type=hidden name=poSumFieldMax value=<%=counterTmp%>>
	         <input type=hidden name=allFields     value=<%=allFields%>>
	<%
		invSummGoodOrder = counterTmp;
		if (!(isReadOnly)) {
	%>
	<div id="updateCreditNoteOrderButton">
	<button data-dojo-type="dijit.form.Button" type="button" id="updateCreditNoteOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			local.updateCreditNoteDefOrder(document.getElementById("discountCodeDataOrderTableId").rows.length, 
	                              (document.getElementById("InvDefFileOrderTableId").rows.length - 1) + (document.getElementById("poStructSumDataOrderTableId").rows.length - 1), 'creditNotePos');
		</script>
	</button>
	</div>
	<%
		} else {
	%>
						&nbsp;
	<%
		}
	%>
	
	
	</div>
	</div>