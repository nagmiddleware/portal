<%
	
	secureParams.put("buyer_name", editArMatchingRule.getAttribute("buyer_name"));

	//T36000015898,if we make these fields,secure params, these become virtually readonly,making them non editable
	// from trading parter rule page. These fields needs to editable from rule page.
	// Hence commented out under T36000015898 
	

	// secureParams.put("ArMatchingRulePayToPortalInd", editArMatchingRule.getAttribute("partial_pay_to_portal_indicator"));
 	// secureParams.put("IndividualInvoiceAllowed", editArMatchingRule.getAttribute("individual_invoice_allowed"));
	
	secureParams.put("ArMatchingRuleTolAmtInd", editArMatchingRule.getAttribute("tolerance_amount_indicator"));
	secureParams.put("ArMatchingRuleNoTolInd", editArMatchingRule.getAttribute("no_tolerance_indicator"));
	secureParams.put("ArMatchingRuleTolPerInd", editArMatchingRule.getAttribute("tolerance_percent_indicator"));
	
	String  receivable_inv_check		= null;
	String  receivable_inv_radio		= null;
	String  credit_note_check			= null;
	String  credit_note_radio			= null;
	
	   boolean                        defaultRadioButtonValue1        = false;
	   boolean                        defaultRadioButtonValue2        = false;
	   boolean                        defaultRadioButtonValue3        = false;
	   boolean						  defaultRadioButtonValue4        = false;
	   boolean                        defaultRadioButtonValueA        = false;
	   boolean                        defaultRadioButtonValueB        = false;
	   boolean                        defaultRadioButtonValueC        = false;
	   boolean                        defaultRadioButtonValueD        = false;	
	   boolean                        defaultRadioButtonValueE        = false;	
	   boolean                        defaultCheckBoxValueA           = false;
	   boolean                        defaultCheckBoxValueB           = false;

	receivable_inv_check        = editArMatchingRule.getAttribute("receivable_invoice");
	receivable_inv_radio        = editArMatchingRule.getAttribute("receivable_authorise");
	credit_note_check           = editArMatchingRule.getAttribute("credit_note");
	credit_note_radio           = editArMatchingRule.getAttribute("credit_authorise");
	
%>

<table width="100%" class="formDocumentsTable">
      <thead>
            <tr>
                  <th align="left">&nbsp;</th>
                  <th align="left" width="24%">&nbsp;</th>
                  <th align="center"><%=resMgr.getText("TradingPartnerDetails.AuthorisationsOneUser",TradePortalConstants.TEXT_BUNDLE)%></th>
                  <th text-align="left"><%=resMgr.getText("TradingPartnerDetails.AuthorisationsTwoUser",TradePortalConstants.TEXT_BUNDLE)%></th>
                  <th align="left" width="22%"><%=resMgr.getText("TradingPartnerDetails.AuthorisationsTwoGroupUser",TradePortalConstants.TEXT_BUNDLE)%></th>
                  <th align="center"><%=resMgr.getText("TradingPartnerDetails.AuthorisationsNotReq",TradePortalConstants.TEXT_BUNDLE)%></th> 
                  <th align="center" width="28%"><%=resMgr.getText("TradingPartnerDetails.AuthorisationsCustSetting",TradePortalConstants.TEXT_BUNDLE)%></th>                              
            </tr>
      </thead>    
  <tbody>
     <tr>
            <td align="center" valign="top">  
         <% 
         if((!(InstrumentServices.isBlank(aroid) || ("0").equals(aroid)) || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(receivable_inv_check))){
		   
		   defaultCheckBoxValueA =true;
		
		   if (InstrumentServices.isBlank(receivable_inv_radio))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
			     defaultRadioButtonValue4 = false;
				 
              }
              else if ((TradePortalConstants.DUAL_AUTH_REQ_ONE_USER).equals(receivable_inv_radio))
              {
                  defaultRadioButtonValue1 = true;
                  defaultRadioButtonValue2 = false;
                  defaultRadioButtonValue3 = false;
			      defaultRadioButtonValue4 = false;

              }
              else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(receivable_inv_radio))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
			     defaultRadioButtonValue4 = false;

              }
              else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(receivable_inv_radio))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
			     defaultRadioButtonValue4 = false;

              } else if ((TradePortalConstants.DUAL_AUTH_REQ_CORPORATE).equals(receivable_inv_radio)) 
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
			     defaultRadioButtonValue4 = true;

              }
         }
         else
         {
              defaultCheckBoxValueA    = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = true;
         }
             
			out.print(widgetFactory.createCheckboxField("receivable_invoice", "", 
					defaultCheckBoxValueA, isReadOnly, false,
                    "onClick=\"setILCControls()\"", "", "none"));
                                                    
		 %>
      </td>
      <td> 
      <b><%=resMgr.getText("TradingPartnerDetails.AuthorisationsReceivableInvoices",TradePortalConstants.TEXT_BUNDLE)%></b>
         <%-- <%= widgetFactory.createSubLabel("TradingPartnerDetails.AuthorisationsReceivableInvoices") %>--%>
      </td>
      <td align="center" valign="top"> 
          <%
             out.print(widgetFactory.createRadioButtonField("receivable_authorise", 
                    "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1, 
                     isReadOnly, 
                     "onClick=\"setILCCheckBox()\"", ""));       
          %>
      </td>     
      <td align="center" valign="top"> 
          <%            
             out.print(widgetFactory.createRadioButtonField("receivable_authorise", 
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2, 
                  isReadOnly, 
                  "onClick=\"setILCCheckBox()\"", ""));
          %>
      </td>     
      <td align="center" valign="top">       
          <%            
             out.print(widgetFactory.createRadioButtonField("receivable_authorise", 
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3, 
                  isReadOnly, 
                  "onClick=\"setILCCheckBox()\"", ""));
          %>        
      </td>
      <td align="center" valign="top"> &nbsp;                       
      </td>
      <td align="center" valign="top">       
          <%            
             out.print(widgetFactory.createRadioButtonField("receivable_authorise", 
                  "TradePortalConstants.DUAL_AUTH_REQ_CORPORATE", "", TradePortalConstants.DUAL_AUTH_REQ_CORPORATE, defaultRadioButtonValue4, 
                  isReadOnly, 
                  "onClick=\"setILCCheckBox()\"", ""));
          %>        
      </td>
    </tr>
    
    
    
    
    
    
    
    
    
    <tr>
            <td align="center" valign="top">  
          <% 
           if((!(InstrumentServices.isBlank(aroid) || ("0").equals(aroid)) || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(credit_note_check))){
			defaultCheckBoxValueB =true;
			
		    if (InstrumentServices.isBlank(credit_note_radio))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
			  defaultRadioButtonValueD = false;
			  defaultRadioButtonValueE = false;
              }
              else if ((TradePortalConstants.DUAL_AUTH_NOT_REQ).equals(credit_note_radio))
              {
                  defaultRadioButtonValueA = true;
                  defaultRadioButtonValueB = false;
                  defaultRadioButtonValueC = false;
			   defaultRadioButtonValueD = false;
			   defaultRadioButtonValueE = false;
              }
              else if ((TradePortalConstants.DUAL_AUTH_REQ_ONE_USER).equals(credit_note_radio))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
			  defaultRadioButtonValueD = false;
			  defaultRadioButtonValueE = false;
              }
              else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS).equals(credit_note_radio))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
			  defaultRadioButtonValueD = false;
			  defaultRadioButtonValueE = false;
              } 
              else if ((TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS).equals(credit_note_radio)) 
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
			  defaultRadioButtonValueD = true;
			  defaultRadioButtonValueE = false;
              } 
              else if ((TradePortalConstants.DUAL_AUTH_REQ_CORPORATE).equals(credit_note_radio)) 
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
			  defaultRadioButtonValueD = false;
			  defaultRadioButtonValueE = true;
              }               
         }
         else
         {
                 defaultCheckBoxValueB    = false;
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
			  defaultRadioButtonValueD = false;
			  defaultRadioButtonValueE = true;
         }
             
			out.print(widgetFactory.createCheckboxField("credit_note", "", 
					defaultCheckBoxValueB, isReadOnly, false,
                    "onClick=\"setSLCControls()\"", "", "none"));
                                                    
		 %>
      </td>
      <td> 
        <%---  <%= widgetFactory.createSubLabel("TradingPartnerDetails.AuthorisationsCreditNote") %>--%>
          <b><%=resMgr.getText("TradingPartnerDetails.AuthorisationsCreditNote",TradePortalConstants.TEXT_BUNDLE)%></b>
      </td>
      <td align="center" valign="top"> 
          <%
             out.print(widgetFactory.createRadioButtonField("credit_authorise", 
                    "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER1", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueB, 
                     isReadOnly, 
                     "onClick=\"setSLCCheckBox()\"", ""));       
          %>
      </td>     
      <td align="center" valign="top"> 
          <%            
             out.print(widgetFactory.createRadioButtonField("credit_authorise", 
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS1", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueC, 
                  isReadOnly, 
                  "onClick=\"setSLCCheckBox()\"", ""));
          %>
      </td>     
      <td align="center" valign="top">       
          <%            
             out.print(widgetFactory.createRadioButtonField("credit_authorise", 
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS1", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueD, 
                  isReadOnly, 
                  "onClick=\"setSLCCheckBox()\"", ""));
          %>        
      </td>
      <td align="center" valign="top">
        <%            
             out.print(widgetFactory.createRadioButtonField("credit_authorise", 
                  "TradePortalConstants.DUAL_AUTH_NOT_REQ1", "", TradePortalConstants.DUAL_AUTH_NOT_REQ, defaultRadioButtonValueA, 
                  isReadOnly, 
                  "onClick=\"setSLCCheckBox()\"", ""));
          %>                         
      </td>
      <td align="center" valign="top">       
          <%            
             	out.print(widgetFactory.createRadioButtonField("credit_authorise", 
                  "TradePortalConstants.DUAL_AUTH_REQ_CORPORATE1", "", TradePortalConstants.DUAL_AUTH_REQ_CORPORATE, defaultRadioButtonValueE, 
                  isReadOnly, 
                  "onClick=\"setSLCCheckBox()\"", ""));
          %>        
      </td>
    </tr>
  </tbody>
 </table> 

<script language='JavaScript'>

function setSLCCheckBox()
{
   <%-- document.forms[0].credit_note.checked = true; --%>
   dijit.getEnclosingWidget(document.forms[0].credit_note).set('checked', true);
}

function setILCControls()
 {
   if (document.forms[0].receivable_invoice.checked == false)
   {
	   <%-- document.forms[0].receivable_authorise[0].checked = false; --%>
        <%-- document.forms[0].receivable_authorise[1].checked = false; --%>
	   <%-- document.forms[0].receivable_authorise[2].checked = false; --%>
       <%--  document.forms[0].receivable_authorise[3].checked = false;         --%>
  
        dijit.getEnclosingWidget(document.forms[0].receivable_authorise[0]).set('checked', false);
        dijit.getEnclosingWidget(document.forms[0].receivable_authorise[1]).set('checked', false);
        dijit.getEnclosingWidget(document.forms[0].receivable_authorise[2]).set('checked', false);
        dijit.getEnclosingWidget(document.forms[0].receivable_authorise[3]).set('checked', false);
		<%-- Srini_D IR-KRUM010338190 01/23/2012 Rel8.0 Start --%>
  }
}
function setILCCheckBox()
{
	
   <%-- document.forms[0].receivable_invoice.checked = true; --%>
   
   dijit.getEnclosingWidget(document.forms[0].receivable_invoice).set('checked', true);

  
}

function setSLCControls()
{
   if (document.forms[0].credit_note.checked == false)
   {
	 <%-- document.forms[0].credit_authorise[0].checked = false; --%>
      <%-- document.forms[0].credit_authorise[1].checked = false; --%>
	 <%-- document.forms[0].credit_authorise[2].checked = false; --%>
     <%--  document.forms[0].credit_authorise[3].checked = false; --%>
	 <%-- document.forms[0].credit_authorise[4].checked = false;	 --%>
	 
	 dijit.getEnclosingWidget(document.forms[0].credit_authorise[0]).set('checked', false);
     dijit.getEnclosingWidget(document.forms[0].credit_authorise[1]).set('checked', false);
     dijit.getEnclosingWidget(document.forms[0].credit_authorise[2]).set('checked', false);
     dijit.getEnclosingWidget(document.forms[0].credit_authorise[3]).set('checked', false);
     dijit.getEnclosingWidget(document.forms[0].credit_authorise[4]).set('checked', false);

   }
}
</script>
