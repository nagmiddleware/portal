<%--
*******************************************************************************
       Invoice Definition Detail - General and Data Definition Layout (Tab) Page

    Description:   This jsp simply displays the html for the data description
portion of the Payment Method Definition.jsp.  This page is called from
Payment Method Definition Detail.jsp  and is called by:
		<%@ include file="PaymentDefinitionsDetail.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to avoid
being overwritten as each tab is a submit action.  As a result, if this data does
not make it into the xml document, then it may be overwritten with a null value
in this tab or another.
*******************************************************************************
--%>

<%-- Data Definition Title Panel details start --%>
<%
    Map sizeMap = new HashMap();
    sizeMap.put("fd_record_type_size",1);
    sizeMap.put("fd_bsb_size",7);
    sizeMap.put("fd_account_size",9);
    sizeMap.put("fd_reserved1_size",1);
    sizeMap.put("fd_sequence_number_size",2);
    sizeMap.put("fd_nm_of_usr_fin_inst_size",3);
    sizeMap.put("fd_reserved2_size",7);
    sizeMap.put("fd_nm_of_usr_sup_file_size",26);
    sizeMap.put("fd_user_ident_num_size",6);
    sizeMap.put("fd_desc_of_entr_on_file_size",12);
    sizeMap.put("fd_date_to_be_process_size",6);
    sizeMap.put("fd_time_size",4);
    sizeMap.put("fd_reserved3_size",36);
    sizeMap.put("dr_record_type_size",1);
    sizeMap.put("dr_bsb_size",7);
    sizeMap.put("dr_account_type_size",3);
    sizeMap.put("dr_acct_num_to_be_cred_size",9);
    sizeMap.put("dr_withhold_tax_ind_size",1);
    sizeMap.put("dr_trans_code_size",2);
    sizeMap.put("dr_nw_var_bsb_acct_det_size",1);
    sizeMap.put("dr_title_acct_cred_size",32);
    sizeMap.put("dr_amt_to_be_cred_size",10);
    sizeMap.put("dr_lodge_ref_size",18);
    sizeMap.put("dr_trace_bsb_num_size",7);
    sizeMap.put("dr_trace_acct_num_size",9);
    sizeMap.put("dr_nm_of_remitter_size",16);
    sizeMap.put("dr_withhold_amt_size",8);
    sizeMap.put("dr_lodge_ref_bsb_num_size",7);
    sizeMap.put("dr_lodge_ref_acct_typ_size",3);
    sizeMap.put("dr_lodeg_ref_acct_num_size",12);
    sizeMap.put("dr_reserved_size",12);
    sizeMap.put("br_record_type_size",1);
    sizeMap.put("br_reserved1_size",7);
    sizeMap.put("br_reserved2_size",12);
    sizeMap.put("br_btch_net_tot_amt_size",10);
    sizeMap.put("br_btch_cred_tot_amt_size",10);
    sizeMap.put("br_btch_deb_tot_amt_size",10);
    sizeMap.put("br_reserved3_size",24);
    sizeMap.put("br_btch_tot_itm_cnt_size",6);
    sizeMap.put("br_reserved4_size",40);
%>
<%=widgetFactory.createSectionHeader("2",
					"PaymentDefinitionDetail.ABAFF")%>

<div class = "columnLeft">
	<%=widgetFactory.createLabel("",
					"PaymentDefinitions.ABADefSep1", false, false, false,
					"none")%>


    <table class="formDocumentsTable" id="PmtFFDefFDTableId" style="width:98%;">
    <thead>
    <tr style="height:35px;">
        <th colspan=3 style="text-align:left;">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitions.ABADescriptiveRecordFields")%>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr style="height:35px">
        <td nowrap width="145"  >
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td  width="20" align="center" >
            <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="30" >
            <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_record_type", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_record_type_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_record_type_size"))? paymentDef.getAttribute("fd_record_type_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_record_type_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_bsb", false, true, false, "none")%>
        </td>

        <td align="center">
            <input type="hidden" name="fd_bsb_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_bsb_size"))? paymentDef.getAttribute("fd_bsb_size") : "7" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_bsb_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_bsb", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_bsb" id="fd_bsb">

            <%=widgetFactory.createCheckboxField(
                    "fd_bsb_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.fd_account",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_account_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_account_size"))? paymentDef.getAttribute("fd_account_size") : "9" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_account_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "9", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_account", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_account" id="fd_account">

            <%=widgetFactory.createCheckboxField(
                    "fd_account_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_reserved1", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_reserved1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_reserved1_size"))? paymentDef.getAttribute("fd_reserved1_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_reserved1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_reserved1", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_reserved1" id="fd_reserved1">

            <%=widgetFactory.createCheckboxField(
                    "fd_reserved1_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.fd_sequence_number",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_sequence_number_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_sequence_number_size"))? paymentDef.getAttribute("fd_sequence_number_size") : "2" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_sequence_number_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
        </td>
        <td >&nbsp;</td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_nm_of_usr_fin_inst", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_nm_of_usr_fin_inst_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_nm_of_usr_fin_inst_size"))? paymentDef.getAttribute("fd_nm_of_usr_fin_inst_size") : "3" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_nm_of_usr_fin_inst_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

            <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_reserved2", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_reserved2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_reserved2_size"))? paymentDef.getAttribute("fd_reserved2_size") : "7" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_reserved2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_reserved2", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_reserved2" id="fd_reserved2">

            <%=widgetFactory.createCheckboxField(
                    "fd_reserved2_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>

    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_nm_of_usr_sup_file", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_nm_of_usr_sup_file_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_nm_of_usr_sup_file_size"))? paymentDef.getAttribute("fd_nm_of_usr_sup_file_size") : "26" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_nm_of_usr_sup_file_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "26", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_user_ident_num", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_user_ident_num_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_user_ident_num_size"))? paymentDef.getAttribute("fd_user_ident_num_size") : "6" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_user_ident_num_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "6", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_desc_of_entr_on_file", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_desc_of_entr_on_file_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_desc_of_entr_on_file_size"))? paymentDef.getAttribute("fd_desc_of_entr_on_file_size") : "12" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_desc_of_entr_on_file_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "12", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_date_to_be_process", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_date_to_be_process_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_date_to_be_process_size"))? paymentDef.getAttribute("fd_date_to_be_process_size") : "6" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_date_to_be_process_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "6", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_time", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_time_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_time_size"))? paymentDef.getAttribute("fd_time_size") : "4" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_time_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "4", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_time", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_time" id="fd_time">

            <%=widgetFactory.createCheckboxField(
                    "fd_time_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.fd_reserved3", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="fd_reserved3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("fd_reserved3_size"))? paymentDef.getAttribute("fd_reserved3_size") : "36" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fd_reserved3_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "36", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.fd_reserved3", TradePortalConstants.TEXT_BUNDLE)%>" name="fd_reserved3" id="fd_reserved3">

            <%=widgetFactory.createCheckboxField(
                    "fd_reserved3_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>

        </td>
    </tr>
    </tbody>
    </table>

<table>

     <tr style="height:38px">
     <td>&nbsp</td>
     </tr>
</table>

    <table class="formDocumentsTable" id="PmtFFDefDRTableId" style="width:98%;">
    <thead>
    <tr style="height:35px;">
        <th colspan=3 style="text-align:left;">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitions.ABADRFields")%>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr style="height:35px">
        <td nowrap width="145"  >
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td  width="20" align="center" >
            <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="30" >
            <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_record_type", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_record_type_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_record_type_size"))? paymentDef.getAttribute("dr_record_type_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_record_type_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_bsb", false, true, false, "none")%>
        </td>

        <td align="center">
            <input type="hidden" name="dr_bsb_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_bsb_size"))? paymentDef.getAttribute("dr_bsb_size") : "7" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_bsb_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.dr_acct_num_to_be_cred",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_acct_num_to_be_cred_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_acct_num_to_be_cred_size"))? paymentDef.getAttribute("dr_acct_num_to_be_cred_size") : "9" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_acct_num_to_be_cred_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "9", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_withhold_tax_ind", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_withhold_tax_ind_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_withhold_tax_ind_size"))? paymentDef.getAttribute("dr_withhold_tax_ind_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_withhold_tax_ind_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.dr_withhold_tax_ind", TradePortalConstants.TEXT_BUNDLE)%>" name="dr_withhold_tax_ind" id="dr_withhold_tax_ind">

            <%=widgetFactory.createCheckboxField(
                    "dr_withhold_tax_ind_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.dr_trans_code",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_trans_code_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_trans_code_size"))? paymentDef.getAttribute("dr_trans_code_size") : "2" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_trans_code_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
        </td>
        <td >&nbsp;</td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_amt_to_be_cred_aba", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_amt_to_be_cred_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_amt_to_be_cred_size"))? paymentDef.getAttribute("dr_amt_to_be_cred_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_amt_to_be_cred_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_title_acct_cred_aba", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_title_acct_cred_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_title_acct_cred_size"))? paymentDef.getAttribute("dr_title_acct_cred_size") : "32" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_title_acct_cred_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "32", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>

    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_lodge_ref", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_lodge_ref_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_lodge_ref_size"))? paymentDef.getAttribute("dr_lodge_ref_size") : "18" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_lodge_ref_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "18", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_trace_bsb_num", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_trace_bsb_num_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_trace_bsb_num_size"))? paymentDef.getAttribute("dr_trace_bsb_num_size") : "7" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_trace_bsb_num_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_trace_acct_num", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_trace_acct_num_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_trace_acct_num_size"))? paymentDef.getAttribute("dr_trace_acct_num_size") : "9" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_trace_acct_num_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "9", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_nm_of_remitter", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_nm_of_remitter_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_nm_of_remitter_size"))? paymentDef.getAttribute("dr_nm_of_remitter_size") : "16" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_nm_of_remitter_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "16", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.dr_withhold_amt", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="dr_withhold_amt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("dr_withhold_amt_size"))? paymentDef.getAttribute("dr_withhold_amt_size") : "8" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='dr_withhold_amt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "8", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.dr_withhold_amt", TradePortalConstants.TEXT_BUNDLE)%>" name="dr_withhold_amt" id="dr_withhold_amt">

            <%=widgetFactory.createCheckboxField(
                    "dr_withhold_amt_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    </tbody>
    </table>

<table>

     <tr style="height:38px">
     <td>&nbsp</td>
     </tr>
</table>


    <table class="formDocumentsTable" id="PmtFFDefBRTableId" style="width:98%;">
    <thead>
    <tr style="height:35px;">
        <th colspan=3 style="text-align:left;">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitions.ABABRFields")%>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr style="height:35px">
        <td nowrap width="145"  >
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td  width="20" align="center" >
            <b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="30" >
            <b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_record_type", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_record_type_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_record_type_size"))? paymentDef.getAttribute("br_record_type_size") : "1" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_record_type_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_reserved1", false, true, false, "none")%>
        </td>

        <td align="center">
            <input type="hidden" name="br_reserved1_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved1_size"))? paymentDef.getAttribute("br_reserved1_size") : "7" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved1_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "7", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.br_reserved2",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_reserved2_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved2_size"))? paymentDef.getAttribute("br_reserved2_size") : "12" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved2_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "12", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_reserved2", TradePortalConstants.TEXT_BUNDLE)%>" name="br_reserved2" id="br_reserved2">

            <%=widgetFactory.createCheckboxField(
                    "br_reserved2_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_btch_net_tot_amt", false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_net_tot_amt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_net_tot_amt_size"))? paymentDef.getAttribute("br_btch_net_tot_amt_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_net_tot_amt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.br_btch_cred_tot_amt",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_cred_tot_amt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_cred_tot_amt_size"))? paymentDef.getAttribute("br_btch_cred_tot_amt_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_cred_tot_amt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td align="center">
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_btch_cred_tot_amt", TradePortalConstants.TEXT_BUNDLE)%>" name="br_btch_cred_tot_amt" id="br_btch_cred_tot_amt">

            <%=widgetFactory.createCheckboxField(
                    "br_btch_cred_tot_amt_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            
            <%=widgetFactory.createLabel("", "PaymentDefinitions.br_btch_deb_tot_amt",
                    false, true, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_deb_tot_amt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_deb_tot_amt_size"))? paymentDef.getAttribute("br_btch_deb_tot_amt_size") : "10" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_deb_tot_amt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
        </td>
        <td align="center">
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_btch_deb_tot_amt", TradePortalConstants.TEXT_BUNDLE)%>" name="br_btch_deb_tot_amt" id="br_btch_deb_tot_amt">

            <%=widgetFactory.createCheckboxField(
                    "br_btch_deb_tot_amt_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_reserved3", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_reserved3_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved3_size"))? paymentDef.getAttribute("br_reserved3_size") : "24" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved3'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />

            <%= widgetFactory.createLabel("", "24", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_reserved3", TradePortalConstants.TEXT_BUNDLE)%>" name="br_reserved3" id="br_reserved3">

            <%=widgetFactory.createCheckboxField(
                    "br_reserved3_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    <tr>
        <td  >
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_btch_tot_itm_cnt", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_btch_tot_itm_cnt_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_btch_tot_itm_cnt_size"))? paymentDef.getAttribute("br_btch_tot_itm_cnt_size") : "6" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_btch_tot_itm_cnt_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "6", false, false, false, "none") %>
        </td>
        <td align='center'>&nbsp;</td>
    </tr>

    <tr>
        <td>
            <%=widgetFactory.createLabel("",
                    "PaymentDefinitions.br_reserved4", false, false, false, "none")%>
        </td>
        <td align="center">
            <input type="hidden" name="br_reserved4_size" value='<%=StringFunction.isNotBlank(paymentDef.getAttribute("br_reserved4_size"))? paymentDef.getAttribute("br_reserved4_size") : "40" %>'
                   data-dojo-type="dijit.form.TextBox"
                   id='br_reserved4_size'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' />
            <%= widgetFactory.createLabel("", "40", false, false, false, "none") %>
        </td>
        <td align='center'>
            <input type=hidden value="<%=resMgr.getText("PaymentUploadRequest.br_reserved4", TradePortalConstants.TEXT_BUNDLE)%>" name="br_reserved4" id="br_reserved4">

            <%=widgetFactory.createCheckboxField(
                    "br_reserved4_data_req",
                    "",
                    true, isReadOnly,
                    false, "", "", "none")%>
        </td>
    </tr>
    </tbody>
    </table>



</div>

<div class = "columnRight">

<%=widgetFactory.createLabel("",
					"PaymentDefinitionDetail.ABADefSep2", false, false, false,
					"none")%>
<table>
    <tr style="height:18px;"><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
</table>

    <table class="formDocumentsTable" id="PmtFFFDDefFileOrderTableId" >
        <thead>
        <tr style="height:35px">
            <th colspan=3 style="text-align:left">
                <%=widgetFactory
                        .createSubLabel("PaymentDefinitionDetail.ABADescriptiveRecordFieldsOrd")%>
            </th>
        </tr>
        </thead>

        <tbody>

        <tr style="height:35px">
	        <td width="20" >
	            <b><%=resMgr.getText("PaymentDefinitions.Order", TradePortalConstants.TEXT_BUNDLE)%></b>
	        </td>
	        <td width="160">
	            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
	        </td>
	        <td width="20">
		        <b><%=resMgr.getText("PaymentDefinitions.Position", TradePortalConstants.TEXT_BUNDLE)%></b>
	        </td>
        </tr>

        <%
            //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
            String[] initialValFFHdr = { "fd_record_type", "fd_bsb", "fd_account", "fd_reserved1", "fd_sequence_number", "fd_nm_of_usr_fin_inst", "fd_reserved2", "fd_nm_of_usr_sup_file", "fd_user_ident_num", "fd_desc_of_entr_on_file", "fd_date_to_be_process", "fd_time", "fd_reserved3" };
            boolean initialFlagFFHdr = false;
            if (InstrumentServices.isBlank(paymentDef
                    .getAttribute("fd_summary_order1")))
                initialFlagFFHdr = true;
            String currentValueFFHdr;
            counterTmp = 1;
            int positionHdr = 1;
            if (initialFlagFFHdr) {
                for (counterTmp = 1; counterTmp <= initialValFFHdr.length; counterTmp++) {

                    currentValueFFHdr = initialValFFHdr[counterTmp - 1];
                    if (counterTmp ==1){
                        positionHdr = 1;
                    }else{
                        positionHdr = positionHdr + ((Integer)sizeMap.get(initialValFFHdr[counterTmp - 2]+"_size")).intValue() ;
                    }
        %>

        <tr>
            <td  align="center">
                <input type="text" name="" value='<%=counterTmp%>'
                       data-dojo-type="dijit.form.TextBox"
                       id='htext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="hupload_order_<%=counterTmp%>" name="fd_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
            </td>
            <td id='hfileFieldName<%=counterTmp%>' >
                <%=widgetFactory.createLabel("",
                        "PaymentUploadRequest." + currentValueFFHdr, false,
                        false, false, "none")%>

            </td>
            <td  align="right">
                <input type="text" name="" value='<%=positionHdr%>'
                       data-dojo-type="dijit.form.TextBox"
                       id='hposition<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="hsize<%=counterTmp%>" name="hsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
            </td>
        </tr>
        <%
            }
        } else {
            counterTmp = 1;
            positionHdr = 1;
            // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
            while ((counterTmp <= 13)
                    && InstrumentServices
                    .isNotBlank(paymentDef
                            .getAttribute("fd_summary_order"
                                    + (new Integer(counterTmp))
                                    .toString()))) {

                currentValueFFHdr = paymentDef.getAttribute("fd_summary_order"
                        + (new Integer(counterTmp)).toString());
                if (counterTmp == 1){
                    positionHdr = 1;
                } else{
                    String tmpSizeField = paymentDef.getAttribute("fd_summary_order"
                            + (new Integer(counterTmp - 1)).toString());
                    String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                    if(StringFunction.isBlank(fSize)) {
                        fSize = (String)sizeMap.get(tmpSizeField+"_size");
                    }
                    positionHdr = positionHdr + (new Integer(fSize)).intValue();
                }
        %>

        <tr id= "h<%=currentValueFFHdr%>_req_order">
            <td  align="center">
                <input type="text" name="" value='<%=counterTmp%>'
                       data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                       id='htext<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="hupload_order_<%=counterTmp%>" name="fd_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
            </td>
            <td id='hfileFieldName<%=counterTmp%>'>
                <%=widgetFactory.createLabel("",
                        "PaymentUploadRequest." + currentValueFFHdr, false,
                        false, false,"", "none")%>
                <% if(pmtSummOptionalVal.contains(currentValueFFHdr) && !pmtSummReqVal.contains(currentValueFFHdr)){ %>
                <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='h<%=currentValueFFHdr%>'></span>
                <%} %>
            </td>
            <td  align="right">
                <input type="text" name="" value='<%=positionHdr%>'
                       data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                       id='hposition<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField" /><input type=hidden id="hsize<%=counterTmp%>" name="hsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFHdr%>" />
            </td>
        </tr>

        <%
                    counterTmp++;
                }
            }
        %>
        </tbody>
    </table>
<table>

     <tr style="height:37px">
     <td>&nbsp</td>
     </tr>
</table>

<table class="formDocumentsTable" id="PmtFFDefDRFileOrderTableId" >
    <thead>
    <tr style="height:35px">
        <th colspan=3 style="text-align:left">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitionDetail.ABADRFieldsOrder")%>
        </th>
    </tr>
    </thead>

    <tbody>

    <tr style="height:35px">
        <td width="20" >
            <b><%=resMgr.getText("PaymentDefinitions.Order", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="160">
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="20">
	        <b><%=resMgr.getText("PaymentDefinitions.Position", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>

    <%
        //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
        String[] initialValFF = { "dr_record_type", "dr_bsb", "dr_acct_num_to_be_cred", "dr_withhold_tax_ind", "dr_trans_code", "dr_amt_to_be_cred", "dr_title_acct_cred", "dr_lodge_ref", "dr_trace_bsb_num", "dr_trace_acct_num", "dr_nm_of_remitter", "dr_withhold_amt" };
        boolean initialFlagFF = false;
        if (InstrumentServices.isBlank(paymentDef
                .getAttribute("dr_summary_order1")))
            initialFlagFF = true;
        String currentValueFF;
        counterTmp = 1;
        int positionPay = 1;
        if (initialFlagFF) {
            for (counterTmp = 1; counterTmp <= initialValFF.length; counterTmp++) {

                currentValueFF = initialValFF[counterTmp - 1];
                if (counterTmp ==1){
                    positionPay = 1;
                }else{
                    positionPay = positionPay + ((Integer)sizeMap.get(initialValFF[counterTmp - 2]+"_size")).intValue() ;
                }

    %>

    <tr>
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='ftext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fupload_order_<%=counterTmp%>" name="dr_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
        <td id='ffileFieldName<%=counterTmp%>' >
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequestABA." + currentValueFF, false,
                    false, false, "none")%>

        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionPay%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='fposition<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fsize<%=counterTmp%>" name="fsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
    </tr>
    <%
        }
    } else {
        counterTmp = 1;
        positionPay = 1;
        // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
        while ((counterTmp <= 12)
                && InstrumentServices
                .isNotBlank(paymentDef
                        .getAttribute("dr_summary_order"
                                + (new Integer(counterTmp))
                                .toString()))) {

            currentValueFF = paymentDef.getAttribute("dr_summary_order"
                    + (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionPay = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("dr_summary_order"
                        + (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String)sizeMap.get(tmpSizeField+"_size");
                }

                positionPay = positionPay + (new Integer(fSize)).intValue();
            }

    %>

    <tr id= "f<%=currentValueFF%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='ftext<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fupload_order_<%=counterTmp%>" name="dr_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
        <td id='ffileFieldName<%=counterTmp%>'>
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequestABA." + currentValueFF, false,
                    false, false,"", "none")%>
            <% if(pmtSummOptionalVal.contains(currentValueFF)){ %>
            <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='f<%=currentValueFF%>'></span>
            <%} %>
        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionPay%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='fposition<%=counterTmp%>'  style="width: 25px;border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="fsize<%=counterTmp%>" name="fsize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFF%>" />
        </td>
    </tr>

    <%
                counterTmp++;
            }
        }
    %>
    </tbody>
</table>
<table>

     <tr style="height:40px">
     <td>&nbsp</td>
     </tr>
</table>

<table class="formDocumentsTable" id="PmtFFDefBRFileOrderTableId" >
    <thead>
    <tr style="height:35px">
        <th colspan=3 style="text-align:left">
            <%=widgetFactory
                    .createSubLabel("PaymentDefinitionDetail.ABABRFieldsOrder")%>
        </th>
    </tr>
    </thead>

    <tbody>

    <tr style="height:35px">
        <td width="20" >
            <b><%=resMgr.getText("PaymentDefinitions.Order", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="160">
            <b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
        <td width="20">
	        <b><%=resMgr.getText("PaymentDefinitions.Position", TradePortalConstants.TEXT_BUNDLE)%></b>
        </td>
    </tr>

    <%
        //RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
        String[] initialValFFBR = { "br_record_type", "br_reserved1", "br_reserved2", "br_btch_net_tot_amt", "br_btch_cred_tot_amt", "br_btch_deb_tot_amt", "br_reserved3", "br_btch_tot_itm_cnt", "br_reserved4" };
        boolean initialFlagFFBR = false;
        if (InstrumentServices.isBlank(paymentDef
                .getAttribute("br_summary_order1")))
            initialFlagFFBR = true;
        String currentValueFFBR;
        counterTmp = 1;
        int positionBR = 1;
        if (initialFlagFFBR) {
            for (counterTmp = 1; counterTmp <= initialValFFBR.length; counterTmp++) {

                currentValueFFBR = initialValFFBR[counterTmp - 1];
                if (counterTmp ==1){
                    positionBR = 1;
                }else{
                    positionBR = positionBR + ((Integer)sizeMap.get(initialValFFBR[counterTmp - 2]+"_size")).intValue() ;
                }

    %>

    <tr>
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='itext<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px;border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="iupload_order_<%=counterTmp%>" name="br_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBR%>" />
        </td>
        <td id='ifileFieldName<%=counterTmp%>' >
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFBR, false,
                    false, false, "none")%>

        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionBR%>'
                   data-dojo-type="dijit.form.TextBox"
                   id='iposition<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="isize<%=counterTmp%>" name="isize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBR%>" />
        </td>
    </tr>
    <%
        }
    } else {
        counterTmp = 1;
        positionBR = 1;
        // invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
        while ((counterTmp <= 9)
                && InstrumentServices
                .isNotBlank(paymentDef
                        .getAttribute("br_summary_order"
                                + (new Integer(counterTmp))
                                .toString()))) {

            currentValueFFBR = paymentDef.getAttribute("br_summary_order"
                    + (new Integer(counterTmp)).toString());
            if (counterTmp == 1){
                positionBR = 1;
            } else{
                String tmpSizeField = paymentDef.getAttribute("br_summary_order"
                        + (new Integer(counterTmp - 1)).toString());
                String fSize =   paymentDef.getAttribute(tmpSizeField+"_size");
                if(StringFunction.isBlank(fSize)) {
                    fSize = (String)sizeMap.get(tmpSizeField+"_size");
                }

                positionBR = positionBR + (new Integer(fSize)).intValue();
            }

    %>

    <tr id= "f<%=currentValueFFBR%>_req_order">
        <td  align="center">
            <input type="text" name="" value='<%=counterTmp%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='itext<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField" /><input type=hidden id="iupload_order_<%=counterTmp%>" name="br_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBR%>" />
        </td>
        <td id='ifileFieldName<%=counterTmp%>'>
            <%=widgetFactory.createLabel("",
                    "PaymentUploadRequest." + currentValueFFBR, false,
                    false, false,"", "none")%>
            <% if(pmtSummOptionalVal.contains(currentValueFFBR)){ %>
            <span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='f<%=currentValueFFBR%>'></span>
            <%} %>
        </td>
        <td  align="right">
            <input type="text" name="" value='<%=positionBR%>'
                   data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
                   id='iposition<%=counterTmp%>'  style="width: 25px; border-color: #ffffff; background-color: #ffffff;" readOnly=true class="ordField"/><input type=hidden id="isize<%=counterTmp%>" name="isize<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueFFBR%>" />
        </td>
    </tr>

    <%
                counterTmp++;
            }
        }
    %>
    </tbody>
</table>
<br/>

</div>


</div>
