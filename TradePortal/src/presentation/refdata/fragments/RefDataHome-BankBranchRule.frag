<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Transactions Home Pending Tab

  Description:
    Contains HTML to create the Receivables Match Notic tab for the Receivables Management Home page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="RefDataHome-BankBranchRule.frag" %>
*******************************************************************************
--%> 


<%-- 
**************************************************************************
    Get all the search parameters 
**************************************************************************
--%>


<%@ include file="/refdata/fragments/RefDataHome-BankBranchRule-SearchParms.frag" %>

<%-- 
**************************************************************************
   Display the search section 
**************************************************************************
--%>

<%@ include file="/refdata/fragments/RefDataHome-BankBranchRule-SearchFilter.frag" %>


<%-- 
**************************************************************************
   Display the List view of the Bank Branches 
**************************************************************************
--%>
<%
			//ssikhakolli - Rel 9.1 08/07/2014 - DGrid Migration task - Begin
			gridHtml = dGridFactory.createDataGrid("BankBranchRulesDataGridId","BankBranchRulesDataGrid", null);
			gridLayout = dGridFactory.createGridLayout("BankBranchRulesDataGridId", "BankBranchRulesDataGrid");
			//gridHtml = dgFactory.createDataGrid("BankBranchRulesDataGridId","BankBranchRulesDataGrid", null);
			//gridLayout = dgFactory.createGridLayout("BankBranchRulesDataGridId", "BankBranchRulesDataGrid");
			//ssikhakolli - Rel 9.1 08/07/2014 - DGrid Migration task - End
			
			initSearchParms = "pOid="+parentOrgID;
%>
<div id="bankBranchRulesGrid">
<%=gridHtml%>
</div>
