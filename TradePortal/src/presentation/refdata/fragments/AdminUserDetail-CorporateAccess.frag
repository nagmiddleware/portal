<%
        // Setup the has customer access radio button options; if the 
        // indicator is empty or "N", make the first option the default
        boolean hasCustomerAccess = false;

        if (user.getAttribute("customer_access_ind").equals("Y")) {
           hasCustomerAccess = true;
        } else {
           hasCustomerAccess = false;
        }
%>
<%//=InputField.createRadioButtonField("CustomerAccess", "N", "", !hasCustomerAccess, "ListText", "Y", isReadOnly)%>
<%//= widgetFactory.createRadioButtonField("CustomerAccess",	"","AdminUserDetail.CorpAccessNotAble",  "N",	!hasCustomerAccess,	isReadOnly)%>
<%//= widgetFactory.createRadioButtonField("CustomerAccess",	"","AdminUserDetail.CorpAccessAble",  "Y",	hasCustomerAccess,	isReadOnly)%>

<%=widgetFactory.createLabel("", "AdminUserDetail.CorporateAccessText", false, false, false, "")%>

 &nbsp;&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField("CustomerAccess", "", "AdminUserDetail.No","N", !hasCustomerAccess, isReadOnly,"","")%>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;<%=widgetFactory.createRadioButtonField("CustomerAccess", "", "AdminUserDetail.Yes","Y", hasCustomerAccess, isReadOnly,"","")%>
<br>

<%
          options = ListBox.createOptionList(corpSecProfileDoc, 
                             "SECURITY_PROFILE_OID", "NAME", 
                             user.getAttribute("cust_access_sec_profile_oid"), userSession.getSecretKey());
defaultText = resMgr.getText("AdminUserDetail.selectProfile", TradePortalConstants.TEXT_BUNDLE);
//out.println(InputField.createSelectField("CorpAccessSecurityProfile", "", defaultText, options, "ListText", isReadOnly,  "onChange='document.AdminUserDetail.CustomerAccess[1].click()'"));
%>
&nbsp;&nbsp;&nbsp;&nbsp;<%=widgetFactory.createSelectField( "CorpAccessSecurityProfile", "AdminUserDetail.CustSecurityProfile",defaultText, options, isReadOnly, false, false, 

"onChange='document.AdminUserDetail.CustomerAccess[1].click()'", "", "")  %> 