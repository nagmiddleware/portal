<div id =BWARow<%=bwaLoop+1%>>

			<div id = "BWATable<%=bwaLoop+1%>" style="border: 1px solid black;">
				<div class="spanHeaderTabel">
							<%=widgetFactory.createLabel("Serial",(bwaLoop+1) +".", false, false, false, "inline")%>
							<%=widgetFactory.createLabel("PmntAmtRange","PanelAuthorizationGroupDetail.PaymentRange", false, false, false, "inline")%>
							<%-- <%=widgetFactory.createTextField("panelAuthRangeMinAmtBWA"+bwaLoop, "",panelRangeMinAmount, "30", isReadOnly, false, false,"style=\"width: 150px;\"", "", "inline")%> --%>
							<%=widgetFactory.createAmountField("panelAuthRangeMinAmtBWA"+bwaLoop, "",panelRangeMinAmount, "", isReadOnly, false, false,"style=\"width: 120px;\"", "", "inline")%>
							<%=widgetFactory.createLabel("PmntAmtRangeTo","to", false, false, false, "inline")%>
							<%=widgetFactory.createAmountField("panelAuthRangeMaxAmtBWA"+bwaLoop, "",panelRangeMaxAmount, "", isReadOnly, false, false,"style=\"width: 120px;\"", "", "inline")%>
							<%	if (!isReadOnly){ %>
							<span style="float: right;">
			              		<a href="javascript:removeRangeBWA(<%=bwaLoop%>);" >Remove Amount Range</a>
			              	</span>
			              	<%}%>
						<div style="clear: both;"></div>
						</div>
						<br>

						<div class="spanTable">
						      <div class="spanRow spanHeader">
									<span class="spanCol char0">&nbsp;</span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.LevelA")%></span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.LevelB")%></span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.LevelC")%></span>
									<span class="spanCol char11"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.TotalAuthorisers")%></span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.ClearRow")%></span>
								<div style="clear: both;"></div>
							</div>

							<% for(int iLoop = 0; iLoop < 5; iLoop++) { %>

								<div class="spanRow">
									<span class="spanCol"><%= (iLoop+1) %></span>
									<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelAUsersGroupBWA"+(iLoop+1)+bwaLoop, "", panelAuthorizationRangeBWAList[bwaLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
									<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelBUsersGroupBWA"+(iLoop+1)+bwaLoop, "", panelAuthorizationRangeBWAList[bwaLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
									<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelCUsersGroupBWA"+(iLoop+1)+bwaLoop, "", panelAuthorizationRangeBWAList[bwaLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
									<%
									    int panelA = 0;
									    int panelB = 0;
									    int panelC = 0;
									    int totalNumOfAuthGroup = 0;
									    try {
									   	 	 panelA = Integer.parseInt(panelAuthorizationRangeBWAList[bwaLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)));
									    } catch(NumberFormatException nfe) {
									    	panelA = 0;
									    }
									    try {
									   	     panelB = Integer.parseInt(panelAuthorizationRangeBWAList[bwaLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)));
									    } catch(NumberFormatException nfe) {
									    	panelB = 0;
									    }
									    try {
									   	 	panelC = Integer.parseInt(panelAuthorizationRangeBWAList[bwaLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)));

									    } catch(NumberFormatException nfe) {
									    	panelC = 0;
									    }
									    totalNumOfAuthGroup = panelA + panelB + panelC;
									    %>
									<span class="spanCol"><%= totalNumOfAuthGroup%></span>
									<span class="spanCol">
										<%	if (!isReadOnly){ %>
							              		<a href="javascript:clearTextBWA(<%=bwaLoop %>, <%=iLoop %>);" >Clear</a>
							              <% } %>
									</span>
								</div>
								<% } %>
							</div>
					</div>
				</div>

<%
	if(!InstrumentServices.isBlank(panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_min_amt"))) {
			panelRangeMinAmount = TPCurrencyUtility.getDisplayAmount(
					panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_min_amt"), baseCurrency, userLocale);

		} else {
			panelRangeMinAmount = panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_min_amt");
		}
		if(!InstrumentServices.isBlank(panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_max_amt"))) {
			panelRangeMaxAmount = TPCurrencyUtility.getDisplayAmount(
					panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_max_amt"), baseCurrency, userLocale);
		} else {
			panelRangeMaxAmount = panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_max_amt");
		}

		if(panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_oid") != null) {
			out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOidBWA" + bwaLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( panelAuthorizationRangeBWAList[bwaLoop].getAttribute("panel_auth_range_oid"), userSession.getSecretKey()) + "'>");
		} else {
			out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOidBWA" + bwaLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey()) + "'>");
		}
		out.print("<INPUT TYPE=HIDDEN NAME='InstrumentTypeCodeBWA"  + bwaLoop + "' VALUE='" + InstrumentType.XFER_BET_ACCTS + "' ID='InstrumentTypeCodeBWA"+bwaLoop +"' >");
%>