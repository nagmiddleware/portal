<%--this fragment is included in both the 
    Transaction-IMP_DLC-ISS-TransportDocsShipment fragment for existing
    rows as well as in the Transaction-IMP_DLC-ISS-ShipmentTermsRows.jsp
    for adding new rows via ajax.

  Passed variables:
    shipmentTermsIndex - an integer value, zero based
    isReadOnly
    isFromExpress
--%>
<tr>
	<%if (insertMode) {
			defaultText = resMgr.getText("UserDetail.selectTemplateGroup",TradePortalConstants.TEXT_BUNDLE);
		} else {
			defaultText = " ";
		}	
		%>						
	<% for( int iColumnCount = 0; iColumnCount < 2; iColumnCount++){ %>
		<td><%=(iLoop + iColumnCount +1) + "."%></td>
		<td>
			<%options = ListBox.createOptionList(templateGroupDoc,"PAYMENT_TEMPLATE_GROUP_OID","NAME",
						templateGroup[iLoop+iColumnCount].getAttribute("payment_template_group_oid"),userSession.getSecretKey());%>
			 
			<%=widgetFactory.createSelectField("PaymentTemplateGroupOid" + (iLoop+iColumnCount), "", 
					defaultText,options, isReadOnly, false, false, "", "", "none")%> 
			<% 	out.print("<INPUT TYPE=HIDDEN NAME='UserAuthorizedTemplateGroupOid"+ (iLoop+iColumnCount) 
			+"' VALUE='"+ EncryptDecrypt.encryptStringUsingTripleDes(templateGroup[iLoop+iColumnCount].getAttribute("authorized_template_group_oid"), userSession.getSecretKey())+ "'>"); %>
		</td>
	<% } %>			
</tr>
