<%
    
 String options="";
 String countryCode = editArMatchingRule.getAttribute("address_country");
%>

      <div class="columnLeft">
  
        <%= widgetFactory.createLabel("", "ArMatchingRuleDetail.BuyerName", isBuyerNameIDReadOnly, true, false, "") %>
        <%=widgetFactory.createTextField("buyer_name","",
						editArMatchingRule.getAttribute("buyer_name"), "35", isBuyerNameIDReadOnly, true)%>
						
		<%= widgetFactory.createLabel("", "TradingPartnerDetails.TradingAddress1", isReadOnly, true, false, "") %>
        <%=widgetFactory.createTextField("address1","",
        		editArMatchingRule.getAttribute("address_line_1"), "35", isReadOnly, true)%>
						
		<%= widgetFactory.createLabel("", "TradingPartnerDetails.TradingAddress2", isReadOnly, false, false, "") %>
        <%=widgetFactory.createTextField("address2","",
        		editArMatchingRule.getAttribute("address_line_2"), "35", isReadOnly, false)%>								
     </div>
     
     
     <div class="columnRight">
     <%= widgetFactory.createLabel("", "TradingPartnerDetails.TradingCity", isReadOnly, true, false, "") %>
        <%=widgetFactory.createTextField("city","",
        		editArMatchingRule.getAttribute("address_city"), "23", isReadOnly, true)%>
						
		<%= widgetFactory.createLabel("", "TradingPartnerDetails.TradingProvince", isReadOnly, false, false, "") %>
        <%=widgetFactory.createTextField("state","",
        		editArMatchingRule.getAttribute("address_state"), "35", isReadOnly, false)%>
						
		<%= widgetFactory.createLabel("", "TradingPartnerDetails.TradingPostalCode", isReadOnly, false, false, "") %>
        <%=widgetFactory.createTextField("postalcode","",
        		editArMatchingRule.getAttribute("postalcode"), "8", isReadOnly, false)%>
        	                  
        <%
			 options = Dropdown.createSortedRefDataOptions( TradePortalConstants.COUNTRY,countryCode, resMgr.getResourceLocale() );					   
		 %>
        <%= widgetFactory.createLabel("", "TradingPartnerDetails.TradingCountry", isReadOnly, true, false, "") %>
        <%=widgetFactory.createSelectField( "country", ""," ", options, isReadOnly, false, false, "", "", "")  %> 
     
     </div>

		<table id="BuyerAlias" class="formDocumentsTable" name="BuyerAlias">
	     <thead>
		<tr>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.Alias")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.Alias", TradePortalConstants.TEXT_BUNDLE)%></b></th>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.Alias")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.Alias", TradePortalConstants.TEXT_BUNDLE)%></b></th>
		</tr>
	   </thead>

	   <tbody>
	       <% 
		   for (iLoop = 0; iLoop < aliasNameCount; iLoop = iLoop + 2) {
			%>
			<tr>	
			    <td><%=iLoop+1 + "."%></td>			
				<td >
			       <%=widgetFactory.createTextField("ArMatchingRuleNameAlias" + iLoop,"", arbuyernamealias[iLoop].getAttribute("buyer_name_alias"), "30", isReadOnly, false)%>
				<%
					//attributeOID = 
						out
						.print("<INPUT TYPE=HIDDEN NAME='BuyerNameAliasoid"
								+ iLoop
								+ "' VALUE='"
								+ EncryptDecrypt
								.encryptStringUsingTripleDes(arbuyernamealias[iLoop]
								.getAttribute("ar_buyer_name_alias_oid"))
								+ "'>");
				%>
				</td>
                <td><%=iLoop+2 + "."%></td>
                <td>
			<%	if(iLoop+1<52){
				%>
				<%=widgetFactory.createTextField("ArMatchingRuleNameAlias" + (1 + iLoop),"", arbuyernamealias[1 + iLoop].getAttribute("buyer_name_alias"), "30", isReadOnly, false)%>
				<%
							out
							.print("<INPUT TYPE=HIDDEN NAME='BuyerNameAliasoid"
							+ (1 + iLoop)
							+ "' VALUE='"
							+ EncryptDecrypt
								.encryptStringUsingTripleDes(arbuyernamealias[1 + iLoop]
									.getAttribute("ar_buyer_name_alias_oid"))
							+ "'>");
				} else{
				%>
				&nbsp;
				<%}%>
				</td>											
			</tr>						
			<%
			}			
			%>		
	  </tbody>
    </table>		

   <%    
   if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreTradeAlias">
				Add 4 More Aliases
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					addBuyerName('" + resMgr.getText("ArMatchingRuleDetail.AliasNumber", TradePortalConstants.TEXT_BUNDLE) + " ','BuyerAlias')
				</script>
			</button>
  <% } else {%>
					&nbsp;
  <% }		%>