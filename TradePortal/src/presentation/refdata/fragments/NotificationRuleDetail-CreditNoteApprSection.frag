<%--
*******************************************************************************
       Notification Rule Detail - CreditNoteAppr setting section
  
Description:    This jsp simply displays the html for the CreditNoteAppr setting 
		of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-CreditNoteApprSection.frag" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>
<%--Host to Host Invoice/Credit Note Approvals sections begins here --%>

<%
	sectionCounter++;
	defUserIdArray = new String[24];
	tempUserIdArray = new String[24];
	if (getDataFromDoc){
		defSendEmailSetting = "";
		defAddlEmailAddresses = "";
		defNotifEmailFerquency = "";
		defUserIds = "";
		defUserNames = "";
		defApplytoAllTransBtn = "";
		defClearAllTransBtn = "";
		
		defListVector = doc2.getFragments("/NotificationRule/DefaultList("+(sectionCounter)+")");
		
		if(defListVector !=null && defListVector.size()>0){
			DocumentHandler notifyRuleDefInstrDoc = (DocumentHandler) defListVector.elementAt(0);
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_email_setting"))){
			defSendEmailSetting = notifyRuleDefInstrDoc.getAttribute("/send_email_setting");
				}
					if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/additional_email_addr"))){
			defAddlEmailAddresses = notifyRuleDefInstrDoc.getAttribute("/additional_email_addr");
					}
						if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/notify_email_freq"))){
			defNotifEmailFerquency = notifyRuleDefInstrDoc.getAttribute("/notify_email_freq");
						}
			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/notificationUserIds"))){
			defUserIds = notifyRuleDefInstrDoc.getAttribute("/notificationUserIds");
			}
			
			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/instNotificationUserNames"))){
				defUserNames = notifyRuleDefInstrDoc.getAttribute("/instNotificationUserNames");
			}
			
				if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran"))){
			defApplytoAllTransBtn = notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran");
				}
					if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran"))){
			defClearAllTransBtn = notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran");
					}
			
			if(StringFunction.isBlank(defUserIds)){
					tempUserIdArray = new String[0];
			}else if(!defUserIds.equals("")){
					tempUserIdArray = defUserIds.split(",");
					List<String> list = new ArrayList<String>();
				    for(String s : tempUserIdArray) {
				       if(!("").equals(s) && !("0").equals(s)){ 
				          list.add(s);
				       }
				    }
				
				    tempUserIdArray = list.toArray(new String[list.size()]);
			}
			uidx = 0;
			for(int i=0; i<tempUserIdArray.length; i++){
				if(!("0").equals(tempUserIdArray[i]) && !("").equals(tempUserIdArray[i])){
					defUserIdArray[uidx] = tempUserIdArray[i];
					uidx++;
				}
			}
		}
	}
%>
<%=widgetFactory.createSectionHeader(""+(sectionCounter+1), "NotificationRuleDetail.HosttoHostInvoiceSection", null, true) %>     
	<%=widgetFactory.createNote("NotificationRuleDetail.CreditNoteInvoicesNote","HosttoHostInvoiceNote1")%>   
		<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.HosttoHostInvoiceNote2") %>	
	<table width='100%' border='0'>
		<tr>
			<td width="33%">
				<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
			</td>
			<td width="5%">
				&nbsp;
			</td>
			<td width="35%">
				<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailRecipients") %>
			</td>
			<td width="27%">
				<%=widgetFactory.createSubLabel("NotificationRuleDetail.AddlEmailRecipients", "subLabelNarrowLineHight") %>
			</td>
		</tr>
		<tr>
			<td width="33%" style="vertical-align: top;">
				<%=widgetFactory.createRadioButtonField("emailSetting"+sectionCounter,"emailSettingYes"+sectionCounter,
						"NotificationRuleDetail.Yes",TradePortalConstants.INDICATOR_YES,
						TradePortalConstants.INDICATOR_YES.equals(defSendEmailSetting), isReadOnly)%>                         
				<br>
				<%=widgetFactory.createRadioButtonField("emailSetting"+sectionCounter,"emailSettingNo"+sectionCounter,
						"NotificationRuleDetail.No",TradePortalConstants.INDICATOR_NO,
						TradePortalConstants.INDICATOR_NO.equals(defSendEmailSetting), isReadOnly)%>
				<br><br>
				<%=widgetFactory.createLabel("", "NotificationRuleDetail.EmailInterval", false, false, false, "inline") %>
				<%= widgetFactory.createTextField("notifyEmailInterval"+sectionCounter, "", defNotifEmailFerquency, "5", 
							isReadOnly,false,false,"","regExp:'^[1-9][0-9]*0$',invalidMessage:'"+resMgr.getText("NotificationRuleDetail.InvalidNotificationEmailFreqValue",TradePortalConstants.TEXT_BUNDLE)+"'","none") %>
				
			</td>
			<td width="5%" style="vertical-align: top;">
				&nbsp;
			</td>
			<td width="35%" style="vertical-align: top;">
				<div id="notifUsersRows<%=sectionCounter%>" style="height: auto; overflow-y: auto; overflow-x: hidden; max-height: 135px;">
					<table border="1" id="NotifUsersTable<%=sectionCounter%>" style="width:90%">
						<%tableRowIdPrefix="NotifUsersTable"+sectionCounter; %>
						<tbody>
						<%
							if (getDataFromDoc){
									if(tempUserIdArray.length == 0){
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
									}else if(tempUserIdArray.length % 2 == 0){
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = tempUserIdArray.length / 2;
									}else{
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = (tempUserIdArray.length / 2) + 1;
									}
									if(DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT < 2) {
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
									}
									if(DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT < 2) {
										DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
									}
							}
							notifyEmail = "emailId";
							notifEmailIndex = (sectionCounter-1) * 24;
							rowIndex = 0;
							uidx = 0;
							sectionNum = sectionCounter;
						%>
								<%@ include file="NotificationRuleDetail-UserEmailRows.frag" %>  
						</tbody>
					</table>
				</div>
			<%
				if (!isReadOnly) {
					if( StringFunction.isNotBlank(notificationRuleOid) && !notificationRuleOid.equals("0")) {
			%>		
				<div>
					<button data-dojo-type="dijit.form.Button" type="button" id="updateUsersOnly<%=sectionCounter%>">
						<%=resMgr.getText("NotificationRuleDetail.UpdateUsers",TradePortalConstants.TEXT_BUNDLE)%>
						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.updateUsersOnly(<%=sectionCounter%>, 
													'<%=criteriaStartCounter.get(dyanInstrCatArray[sectionCounter-1]).intValue()%>', 
													'<%=instrTransArrayStr.get(dyanInstrCatArray[sectionCounter-1])%>', 
													'<%=dyanInstrCatArray[sectionCounter-1]%>', 'OTHERS');
						</script>
					</button>
				</div>
			<%}%>	
				<div>
					<button data-dojo-type="dijit.form.Button" type="button" id="add2MoreUsers<%=sectionCounter%>">
						<%=resMgr.getText("NotificationRuleDetail.Add2Users",TradePortalConstants.TEXT_BUNDLE)%>
						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.add2MoreUserEmails (<%=sectionCounter%>, <%=(sectionCounter-1) * 24%>);
						</script>
					</button>
				</div>
			<%
				}
			%> 
			</td>
			<td width="27%" style="vertical-align: top;">
				<%= widgetFactory.createTextArea("additionalEmailAddr"+sectionCounter, "", defAddlEmailAddresses, isReadOnly, false, false, "maxlength='300' rows='6'", "", "none") %>
				<div class="availNotifySpace">
				</div>
				<%if( StringFunction.isNotBlank(notificationRuleOid) && !notificationRuleOid.equals("0")) { %>
					<button data-dojo-type="dijit.form.Button"  name="UpdateEmailsOnly<%=sectionCounter%>" id="UpdateEmailsOnly<%=sectionCounter%>" type="button">
						<%=resMgr.getText("NotificationRuleDetail.UpdateEmailOnly",TradePortalConstants.TEXT_BUNDLE)%>
						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.updateEmailsOnly(<%=sectionCounter%>, 
														'<%=criteriaStartCounter.get(dyanInstrCatArray[sectionCounter-1]).intValue()%>', 
														'<%=instrTransArrayStr.get(dyanInstrCatArray[sectionCounter-1])%>', 
														'<%=dyanInstrCatArray[sectionCounter-1]%>', 'OTHERS');
						</script>
					</button>
					<br>
				<%} %>	
				<button data-dojo-type="dijit.form.Button"  name="ApplytoAllTransactions<%=sectionCounter%>" id="ApplytoAllTransactions<%=sectionCounter%>" type="button">
					<%=resMgr.getText("NotificationRuleDetail.ApplytoAllTransactions.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.applyToAllTransactions(<%=sectionCounter%>, 
													'<%=criteriaStartCounter.get(dyanInstrCatArray[sectionCounter-1]).intValue()%>', 
													'<%=instrTransArrayStr.get(dyanInstrCatArray[sectionCounter-1])%>', 
													'<%=dyanInstrCatArray[sectionCounter-1]%>', 'OTHERS');
					</script>
				</button>
				<button data-dojo-type="dijit.form.Button"  name="Override<%=sectionCounter%>" id="Override<%=sectionCounter%>" type="button">
					<%=resMgr.getText("NotificationRuleDetail.Override.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.ocerrideTransactionsSection(<%=sectionCounter%>, 
														 '<%=criteriaStartCounter.get(dyanInstrCatArray[sectionCounter-1]).intValue()%>', 
														 '<%=instrTransArrayStr.get(dyanInstrCatArray[sectionCounter-1])%>', 
														 '<%=dyanInstrCatArray[sectionCounter-1]%>', 'OTHERS', 
														 '<%=sectionDialogTitle.get(dyanInstrCatArray[sectionCounter-1])%>');
					</script>
				</button>
				<button data-dojo-type="dijit.form.Button"  name="ClearAll<%=sectionCounter%>" id="ClearAll<%=sectionCounter%>" type="button">
					<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.clearInstrCategorySection(<%=sectionCounter%>, 
														'<%=criteriaStartCounter.get(dyanInstrCatArray[sectionCounter-1]).intValue()%>', 
														'<%=instrTransArrayStr.get(dyanInstrCatArray[sectionCounter-1])%>', 
														'<%=dyanInstrCatArray[sectionCounter-1]%>', 'OTHERS');
					</script>
				</button>	
				<input type="hidden" name="instrumentType<%=sectionCounter%>" id="instrumentType<%=sectionCounter%>" value="H2H_INV_CRN">
				<input type="hidden" name="changeInd<%=sectionCounter%>" id="changeInd<%=sectionCounter%>" value="">
				<input type="hidden" name="applyInst<%=sectionCounter%>" id="applyInst<%=sectionCounter%>" value="">
				<input type="hidden" name="clearAll<%=sectionCounter%>" id="clearAll<%=sectionCounter%>" value="">
				<input type="hidden" name="updateRecipientsOnlyInst<%=sectionCounter%>" id="updateRecipientsOnlyInst<%=sectionCounter%>" value="">
				<input type="hidden" name="updateEmailsOnlyInst<%=sectionCounter%>" id="updateEmailsOnlyInst<%=sectionCounter%>" value="">
				<input type="hidden" name="colType<%=sectionCounter%>" id="colType<%=sectionCounter%>" value="H">
				<input type="hidden" name="instNotificationUserIds<%=sectionCounter%>" id="instNotificationUserIds<%=sectionCounter%>" value="">
				<input type="hidden" name="instNotificationUserNames<%=sectionCounter%>" id="instNotificationUserNames<%=sectionCounter%>" value="<%=defUserNames%>">
			<%
	    		transArray = HTOH_TransArray;
				userSql = new String();
				userEmailqueryListView = null;
				userEmailCriterionList = new DocumentHandler();
				
	    		for(int tLoop = 0; tLoop < transArray.length; tLoop++){
	    			String key;
	   			    key = InstrumentType.HTWOH_INV_INST_TYPE+"_"+transArray[tLoop];

	   			    NotificationRuleCriterionWebBean notifyRuleCriterion = null;
	   			    notifyRuleCriterion = (NotificationRuleCriterionWebBean)criterionRuleTransList.get(key);
	   			    yy++;
	   			 	if (notifyRuleCriterion != null) {
	   		      		//load using OID
	 					out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + notifyRuleCriterion.getAttribute("instrument_type_or_category") + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + notifyRuleCriterion.getAttribute("transaction_type_or_action") + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='" + notifyRuleCriterion.getAttribute("criterion_oid") + "'>");
	   		      		out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_email_setting") + "'>");
	   		      		out.print("<input type=hidden name='additional_email_addr"+yy+"' id='additional_email_addr"+yy+"' value='" + notifyRuleCriterion.getAttribute("additional_email_addr") + "'>");
	   		      		out.print("<input type=hidden name='notify_email_freq"+yy+"' id='notify_email_freq"+yy+"' value='" + notifyRuleCriterion.getAttribute("notify_email_freq") + "'>");
	   		      	
		   		      	userEmailqueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		   		      	userSql = "select notify_rule_user_list_oid, p_user_oid as user_oid from notify_rule_user_list where p_criterion_oid = ? and p_user_oid!=0";
	                	userEmailqueryListView.setSQL(userSql,new Object[]{notifyRuleCriterion.getAttribute("criterion_oid")});
	                	userEmailqueryListView.getRecords();
	                	
	                	userEmailCriterionList = userEmailqueryListView.getXmlResultSet();
	                
	                	emailVector = userEmailCriterionList.getFragments("/ResultSetRecord");
	                	numItems = emailVector.size();
	                	
	                	NotifyRuleUserListWebBean notifyRuleUserList[] = new NotifyRuleUserListWebBean[numItems];
	                	
	                	dbUserIDArray = new StringBuilder();
	                	userIDArray = new String[24];
	                	userEmailValues = "";
	                	
	                	for (int eLoop=0;eLoop<numItems;eLoop++) {
	                		notifyRuleUserList[eLoop] = beanMgr.createBean(NotifyRuleUserListWebBean.class, "NotifyRuleUserList");
	                  		DocumentHandler notifyRuleUserListDoc = (DocumentHandler) emailVector.elementAt(eLoop);
	                  		userIDArray[eLoop] = notifyRuleUserListDoc.getAttribute("/USER_OID");
	                  		
	                  		dbUserIDArray.append(notifyRuleUserListDoc.getAttribute("/USER_OID"));
	                  		if(eLoop<numItems-1){
	                  			dbUserIDArray.append(",");
	                  		}
	                	}//eLoop
	                	
				   		for (int vLoop=0;vLoop<userIDArray.length;vLoop++) {
				   			if(StringFunction.isBlank(userIDArray[vLoop])){
				   				userIDArray[vLoop] = "0";
				   			}
				   			userEmailValues += userIDArray[vLoop];
				   			
				   			if(vLoop != userIDArray.length-1){
				   				userEmailValues += ",";
				   			}
				   		}
	                	
	   		      		//Email/User ID starts here
		   			    out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[0]+"'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[1]+"'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[2]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[3]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[4]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[5]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[6]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[7]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[8]+"'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[9]+"'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[10]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[11]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[12]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[13]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[14]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[15]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[16]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[17]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[18]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[19]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[20]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[21]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[22]+"'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='"+userIDArray[23]+"'>");
				   		
		   		    } else {//if it is a new form
		   		    	out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + InstrumentType.HTWOH_INV_INST_TYPE + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + transArray[tLoop] + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='0'>");		   		     	
		   		      	out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value=''>");
		   		      	out.print("<input type=hidden name='additional_email_addr"+yy+"' id='additional_email_addr"+yy+"' value=''>");
		   		      	out.print("<input type=hidden name='notify_email_freq"+yy+"' id='notify_email_freq"+yy+"' value=''>");
		   		      	
		   		      	userIDArray = new String[24];
	                	userEmailValues = "";
	                	
				   		for (int vLoop=0;vLoop<userIDArray.length;vLoop++) {
				   			if(StringFunction.isBlank(userIDArray[vLoop])){
				   				userIDArray[vLoop] = "0";
				   			}
				   			
				   			userEmailValues += userIDArray[vLoop];
				   			
				   			if(vLoop != userIDArray.length-1){
				   				userEmailValues += ",";
				   			}
				   		}
		   		      	//Email/User ID starts here
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		    	out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		  		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
				   		out.print("<input type=hidden  id='email_id"+(xx++)+"' value='0'>");
		   		    }
					out.print("<input type=hidden name='notificationUserIds"+(yy)+"' id='notificationUserIds"+(yy)+"' value='"+userEmailValues+"'>");
					out.print("<input type=hidden name='dbNotificationUserIds"+(yy)+"' id='dbNotificationUserIds"+(yy)+"' value='"+dbUserIDArray.toString()+"'>");
					out.print("<input type=hidden name='clear_tran_ind"+(yy)+"' id='clear_tran_ind"+(yy)+"' value='N'>");
	        	}//ent tLoop
				
				%>
			</td>
		</tr>
		<tr>
		<td width="30%" style="vertical-align: top;">
		<% if(dataList.contains(InstrumentType.HTWOH_INV_INST_TYPE)) {%>
					
					 <span class="availNotifyItemSelect">&nbsp;
					<%=resMgr.getText("NotificationRuleDetail.DataExists",TradePortalConstants.TEXT_BUNDLE)%>
					</span>
		<% } %>
		</td>
		</tr>
	</table> 
</div><%--Host to Host Invoice/Credit Note Approvals sections ends here --%>