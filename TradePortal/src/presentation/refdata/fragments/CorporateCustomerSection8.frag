<%--this fragment is included in CorporateCustomerDetail.jsp --%>

  <%
    if (xmlDoc.getAttribute("/In/CorporateOrganization") == null) {
        if(!InstrumentServices.isBlank(corporateOrg.getAttribute("transfer_daily_limit_amt"))) {
            transferDailyLimitAmt = TPCurrencyUtility.getDisplayAmount(corporateOrg.getAttribute("transfer_daily_limit_amt"),  corporateOrg.getAttribute("base_currency_code"), userLocale);
        } else {
            transferDailyLimitAmt = corporateOrg.getAttribute("transfer_daily_limit_amt");
        }

        if(!InstrumentServices.isBlank(corporateOrg.getAttribute("domestic_pymt_daily_limit_amt"))) {
            domesticPmtDailyLimitAmt = TPCurrencyUtility.getDisplayAmount(corporateOrg.getAttribute("domestic_pymt_daily_limit_amt"), corporateOrg.getAttribute("base_currency_code"), userLocale);
        } else {
            domesticPmtDailyLimitAmt = corporateOrg.getAttribute("domestic_pymt_daily_limit_amt");
        }

		if(!InstrumentServices.isBlank(corporateOrg.getAttribute("intl_pymt_daily_limit_amt"))) {
            intlPmtDailyLimitAmt = TPCurrencyUtility.getDisplayAmount(corporateOrg.getAttribute("intl_pymt_daily_limit_amt"), corporateOrg.getAttribute("base_currency_code"), userLocale);
        } else {
            intlPmtDailyLimitAmt = corporateOrg.getAttribute("intl_pymt_daily_limit_amt");
        }
    } else {
            transferDailyLimitAmt = corporateOrg.getAttribute("transfer_daily_limit_amt");
            domesticPmtDailyLimitAmt = corporateOrg.getAttribute("domestic_pymt_daily_limit_amt");
            intlPmtDailyLimitAmt = corporateOrg.getAttribute("intl_pymt_daily_limit_amt");
    }
   %>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <thead>
      <tr>
        <%--cquinton 11/10/2012 ie7 formats oddly if no content. add blank space--%>
        <th align="left">&nbsp;</th>
        <th align="left">&nbsp;</th>
        <th align="left"><%=widgetFactory.createSubLabel("CorpCust.DailyLimitAmount")%></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td align="center">
<%
  defaultCheckBoxValue = false;
  if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(enforceDailyLimitIntlPymt))) {
    defaultCheckBoxValue = true;
  } 
  out.print(widgetFactory.createCheckboxField("EnforceDailyLimitIntlPymt", "", defaultCheckBoxValue, isReadOnly, false,"onClick=\"local.setDailyLimitTextFields()\"","","none"));
%>
        </td>
        <td><%= widgetFactory.createSubLabel("CorpCust.FundsTransferRequests")%></td>
        <td align="center">
          <%=widgetFactory.createAmountField("IntlPymtDailyLimit", "", intlPmtDailyLimitAmt, "", isReadOnly, false, false, "onkeyup='setDailyLimitCheckBox()' style='width:380px'", "", "none")%>
        </td>
      </tr>

      <tr>
        <td align="center">
<%
  defaultCheckBoxValue = false; 
  if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(enforceDailyLimitDomesticPayment))) {
    defaultCheckBoxValue = true;
  }
  out.print(widgetFactory.createCheckboxField("EnforceDailyLimitDomesticPayment", "", defaultCheckBoxValue, isReadOnly, false,"onClick=\"local.setDailyLimitTextFields()\"","","none"));
%>
        </td>
        <td><%= widgetFactory.createSubLabel("CorpCust.DomesticPayment")%></td>
        <td align="center" nowrap>
          <%=widgetFactory.createAmountField("DomesticPymtDailyLimit", "", domesticPmtDailyLimitAmt, "", isReadOnly, false, false,"onkeyup='setDailyLimitCheckBox()' style='width:380px'", "", "none")%>
        </td>
      </tr>
      <tr>
        <td align="center">
<%
  defaultCheckBoxValue = false;
  if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(enforceDailyLimitTransfer))) {
    defaultCheckBoxValue = true;
  } 
  out.print(widgetFactory.createCheckboxField("EnforceDailyLimitTransfer", "", defaultCheckBoxValue, isReadOnly, false,"onClick=\"local.setDailyLimitTextFields()\"","","none"));
%>
        </td>
        <td><%= widgetFactory.createSubLabel("CorpCust.TransferBtwAccts")%></td>
        <td align="center">
          <%= widgetFactory.createAmountField( "TransferDailyLimit", "", transferDailyLimitAmt, "", isReadOnly, false, false, "onkeyup='setDailyLimitCheckBox()' style='width:380px'", "", "none") %>
        </td>
      </tr>
    </tbody>
  </table>
