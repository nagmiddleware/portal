<%--this fragment is included in CorporateCustomerDetail.jsp --%>

	<div class="formItem">
       <%= widgetFactory.createSubLabel("CorpCust.SelectReptCategsText") %>
    </div>
    
	<div class="columnLeft">
         <%
        	defaultText = " ";
         	dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg1Oid, userSession.getSecretKey());
            out.print(widgetFactory.createSelectField( "ReportingCategory1", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

             defaultText = " ";
             dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg3Oid, userSession.getSecretKey());
             out.print(widgetFactory.createSelectField( "ReportingCategory3", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

             defaultText = " ";
             dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg5Oid, userSession.getSecretKey());
             out.print(widgetFactory.createSelectField( "ReportingCategory5", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

             defaultText = " ";
             dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg7Oid, userSession.getSecretKey());
             out.print(widgetFactory.createSelectField( "ReportingCategory7", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

             defaultText = " ";
             dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg9Oid, userSession.getSecretKey());
             out.print(widgetFactory.createSelectField( "ReportingCategory9", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));
        %>
	</div>
	
    <div class="columnRight">

        <%
           defaultText = " ";
           dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg2Oid, userSession.getSecretKey());
           out.print(widgetFactory.createSelectField( "ReportingCategory2", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

		   defaultText = " ";
           dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg4Oid, userSession.getSecretKey());
           out.print(widgetFactory.createSelectField( "ReportingCategory4", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

		   defaultText = " ";
           dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg6Oid, userSession.getSecretKey());
           out.print(widgetFactory.createSelectField( "ReportingCategory6", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

           defaultText = " ";
           dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg8Oid, userSession.getSecretKey());
           out.print(widgetFactory.createSelectField( "ReportingCategory8", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));

		   defaultText = " ";
           dropdownOptions = ListBox.createOptionList(reportCategsDoc, "CODE", "DESCR", corporateOrgReptCateg10Oid, userSession.getSecretKey());
           out.print(widgetFactory.createSelectField( "ReportingCategory10", "", defaultText, dropdownOptions, isReadOnly, false, false, "style='width:260px'", "", ""));
        %>
    </div>