<%--
*******************************************************************************
                                    BankGrpRestrictRuleDetail - Bank Groups Section

  *******************************************************************************
--%>
<div class="formItem  required">
  <%= widgetFactory.createLabel("BankGrpRestrictRuleDetail.BankGroup","BankGrpRestrictRuleDetail.BankGroup",isReadOnly, false, false,"none")%>
  <table id="bankGroupsTable" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 30%;">
      <col span="1" style="width: 30%;">
      <col span="1" style="width: 30%;">
    </colgroup>
	<tbody>
		
			<%
			int iLoop = 0;
			int value=0;
			while(iLoop < numTotalBankGroupRows){%>
				<tr>
					<%
					
					if (!isReadOnly) {
							defaultText = resMgr.getText("UserDetail.selectTemplateGroup",TradePortalConstants.TEXT_BUNDLE);
						} else {
							defaultText = " ";
						}	
						%>						
					<% for( int iColumnCount = 0; iColumnCount < 3; iColumnCount++){ %>
						<td> <%try{%>
									<%options = ListBox.createOptionList(bankGroupsDoc,"ORGANIZATION_OID","NAME",bankGrpRestictionForRules[value].getAttribute("bank_organization_group_oid"),userSession.getSecretKey());
									%>
									<%=widgetFactory.createSelectField("BankOrganizationGroupOid"+value, "", " ",options, isReadOnly, false, false, "class='char21'", "", "none")%> 
									<% 	out.print("<INPUT TYPE=HIDDEN NAME='BankGrpForRestrictRuleOid"+value+"' VALUE='"+ EncryptDecrypt.encryptStringUsingTripleDes(bankGrpRestictionForRules[value].getAttribute("bank_grp_for_restrict_rls_oid"), userSession.getSecretKey())+ "'>"); %>
						     <%}catch(Exception e){
						    		 options = ListBox.createOptionList(bankGroupsDoc,"ORGANIZATION_OID","NAME","",userSession.getSecretKey());%>
									<%=widgetFactory.createSelectField("BankOrganizationGroupOid"+value, "", " ",options, isReadOnly, false, false, "class='char21'", "", "none")%> 
									<%out.print("<INPUT TYPE=HIDDEN NAME='BankGrpForRestrictRuleOid"+value+"' VALUE=''>"); 
						     }
						     %>
						</td>
					<% value++;} %>			
				</tr>
					<%iLoop++;	
				}
			%>
	</tbody>
</table>
	<% if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add3MoreBankGroups">
				<%=resMgr.getText("BankGrpRestrictRuleDetail.Add3MoreBankGroup",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					add3MoreBankGroups();
				</script>
			</button>
		<% } else {		%>
					&nbsp;
		<% }		%>

<input type=hidden value='<%=bankGroupsDoc%>' name="bankGroupsDoc" id="bankGroupsDocID">
<input type=hidden value='<%=isNew%>' name="isNew" id="isNewID">
	</div>			
<script language="JavaScript">  


function add3MoreBankGroups() {
	var tbl = document.getElementById('bankGroupsTable');<%-- to identify the table in which the row will get insert --%>
	var lastElement = tbl.rows.length;
	var lastCellElement=lastElement*3;
	var defaultText = '<%=defaultText%>'
	
	for(i=lastElement;i<lastElement+1;i++){
		if(lastCellElement<50){
   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
   	 	newRow.id = "bnkGroupRow_"+i;
   	 	}
		for(j=0;j<3;j++){
			var cell=newRow.insertCell(j);
			if(lastCellElement<50){
				cell.innerHTML='<select data-dojo-type=\"dijit.form.FilteringSelect\" class=\"char21\" name=\"BankOrganizationGroupOid'+lastCellElement+'\" id=\"BankOrganizationGroupOid'+lastCellElement+'\"></select><INPUT TYPE=HIDDEN NAME=\"BankGrpForRestrictRuleOid'+lastCellElement+'\" VALUE=\"\">';
			}
			lastCellElement++;
		}
		
        require(["dojo/parser", "dijit/registry"], function(parser, registry) {
         	parser.parse(newRow.id);
         	var bankGroupOid = registry.byId("BankOrganizationGroupOid0");
            if(bankGroupOid!=null){
              var bankGroupStore = bankGroupOid.store;
              for(j=0;j<3;j++){
              		lastCellElement--;
              		if(lastCellElement<50){
						registry.byId('BankOrganizationGroupOid'+lastCellElement).set("store",bankGroupStore);
						registry.byId('BankOrganizationGroupOid'+lastCellElement).attr('displayedValue', "");
					}
					
				}
            }
    	 });
	}
	
}
 
</script>
