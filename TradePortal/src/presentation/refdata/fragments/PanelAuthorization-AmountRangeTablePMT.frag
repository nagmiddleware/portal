	<div id = "PMTRow<%=pmtLoop+1%>">
			<div id = "PMTTable<%=pmtLoop+1%>"  style="border: 1px solid black;">
							<div class="spanHeaderTabel">
								<%=widgetFactory.createLabel("Serial",(pmtLoop+1) +".", false, false, false, "inline")%>
								<%=widgetFactory.createLabel("PmntAmtRange","PanelAuthorizationGroupDetail.PaymentRange", false, false, false, "inline")%>
								<%=widgetFactory.createAmountField("panelAuthRangeMinAmtPMT"+pmtLoop, "",panelRangeMinAmount, "", isReadOnly, false, false,"style=\"width: 120px;\"", "", "inline")%>
								<%=widgetFactory.createLabel("PmntAmtRangeTo","to", false, false, false, "inline")%>
								<%=widgetFactory.createAmountField("panelAuthRangeMaxAmtPMT"+pmtLoop, "",panelRangeMaxAmount, "", isReadOnly, false, false,"style=\"width: 120px;\"", "", "inline")%>
								<span style="float: right;">
								<%	if (!isReadOnly){ %>
				              		<a href="javascript:removeRangePMT(<%=pmtLoop %>);" >Remove Amount Range</a>
				              	<% } %>
								</span>
								<div style="clear: both;"></div>
						</div>
					<br>
						<div class="spanTable">
						<div class="spanRow spanHeader">
									<span class="spanCol char0">&nbsp;</span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.LevelA")%></span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.LevelB")%></span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.LevelC")%></span>
									<span class="spanCol char11"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.TotalAuthorisers")%></span>
									<span class="spanCol char6"><%=widgetFactory.createSubLabel("PanelAuthorizationGroupDetail.ClearRow")%></span>
									<div style="clear: both;"></div>
								</div>

							<% for(int iLoop = 0; iLoop < 5; iLoop++) { %>

								<div class="spanRow">
									<span class="spanCol"><%= (iLoop+1) %></span>
									<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelAUsersGroupPMT"+(iLoop+1)+pmtLoop, "", panelAuthorizationRangePMTList[pmtLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
									<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelBUsersGroupPMT"+(iLoop+1)+pmtLoop, "", panelAuthorizationRangePMTList[pmtLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
									<span class="spanCol"><%=widgetFactory.createTextField("numOfPanelCUsersGroupPMT"+(iLoop+1)+pmtLoop, "", panelAuthorizationRangePMTList[pmtLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)), "5", isReadOnly, false, false,"", "", "inline")%></span>
									<%
									    int panelA = 0;
									    int panelB = 0;
									    int panelC = 0;
									    int totalNumOfAuthGroup = 0;
									    try {
									   	 	 panelA = Integer.parseInt(panelAuthorizationRangePMTList[pmtLoop].getAttribute("num_of_panel_A_users_group"+(iLoop + 1)));
									    } catch(NumberFormatException nfe) {
									    	panelA = 0;
									    }
									    try {
									   	     panelB = Integer.parseInt(panelAuthorizationRangePMTList[pmtLoop].getAttribute("num_of_panel_B_users_group"+(iLoop + 1)));
									    } catch(NumberFormatException nfe) {
									    	panelB = 0;
									    }
									    try {
									   	 	panelC = Integer.parseInt(panelAuthorizationRangePMTList[pmtLoop].getAttribute("num_of_panel_C_users_group"+(iLoop + 1)));

									    } catch(NumberFormatException nfe) {
									    	panelC = 0;
									    }
									    totalNumOfAuthGroup = panelA + panelB + panelC;
									    %>
									<span class="spanCol"><%= totalNumOfAuthGroup%></span>
									<span class="spanCol">
										<%	if (!isReadOnly){ %>
							              		<a href="javascript:clearTextPMT(<%=pmtLoop %>, <%=iLoop %>);" >Clear</a>
							              <% } %>
									</span>
								</div>
								<% } %>

						</div>
					</div>
				</div>
<%
		if(!InstrumentServices.isBlank(panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_min_amt"))) {
				panelRangeMinAmount = TPCurrencyUtility.getDisplayAmount(
						panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_min_amt"), baseCurrency, userLocale);

			} else {
				panelRangeMinAmount = panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_min_amt");
			}
			if(!InstrumentServices.isBlank(panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_max_amt"))) {
				panelRangeMaxAmount = TPCurrencyUtility.getDisplayAmount(
						panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_max_amt"), baseCurrency, userLocale);
			} else {
				panelRangeMaxAmount = panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_max_amt");
			}

			if(panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_oid") != null) {
				out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOidPMT" + pmtLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( panelAuthorizationRangePMTList[pmtLoop].getAttribute("panel_auth_range_oid"), userSession.getSecretKey()) + "'>");
			} else {
				out.print("<INPUT TYPE=HIDDEN NAME='panelAuthOidPMT" + pmtLoop + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey()) + "'>");
			}
			out.print("<INPUT TYPE=HIDDEN NAME='InstrumentTypeCodePMT"  + pmtLoop + "' VALUE='" + InstrumentType.DOMESTIC_PMT + "' ID='InstrumentTypeCodePMT"+pmtLoop +"'>");
%>
