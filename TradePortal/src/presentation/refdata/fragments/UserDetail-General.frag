<%--
*******************************************************************************
                                    User Detail - General Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<div class="columnLeft">
	<%=widgetFactory.createTextField("UserId","UserDetail.UserID",user.getAttribute("user_identifier"),
					"30",isReadOnly,true,false,"class='char20'","", "")%>
	<div>
	<%=widgetFactory.createTextField("FirstName","UserDetail.FirstName",user.getAttribute("first_name"),
					"15",isReadOnly,true,false,"class='char20'","", "inline")%>
	<%=widgetFactory.createTextField("MiddleInitial","UserDetail.MiddleInitial",user.getAttribute("middle_initial"),
					"1",isReadOnly,false,false,"style=\"width: 50px;\"","", "inline")%>
					<div style="clear: both;"></div>
	</div>
	<%=widgetFactory.createTextField("LastName","UserDetail.LastName",user.getAttribute("last_name"),
					"30",isReadOnly,true,false,"class='char20'","", "")%>
	<%=widgetFactory.createTextField("PhoneNumber","UserDetail.PhoneNumber",user.getAttribute("telephone_num"),
					"20",isReadOnly,false,false,"class='char20'","", "")%>
	<%=widgetFactory.createTextField("FaxNumber","UserDetail.FaxNumber",user.getAttribute("fax_num"),
					"20",isReadOnly,false,false,"class='char20'","", "")%>
</div>
<div class="columnRight">					
	<%=widgetFactory.createTextField("EmailAddress","UserDetail.EmailAddress",user.getAttribute("email_addr"),
					"50",isReadOnly,false,false,"","", "")%>
	<%
		options = Dropdown.createSortedRefDataOptions("LOCALE",
        user.getAttribute("locale"), loginLocale);
		if (insertMode) {
			defaultText = resMgr.getText("UserDetail.selectRegion",
		                  TradePortalConstants.TEXT_BUNDLE);
		} else {
			defaultText = "";
		}
	%>
	<%=widgetFactory.createSelectField("RegionSetting",
					"UserDetail.RegionSetting", " ", options, isReadOnly,
					true, false, "class='char25'", "", "")%>
	
<%// jgadela R 8.4 CR-854 T36000024797
	if(TradePortalConstants.INDICATOR_YES.equals(cbReportingLangOptionInd)){%>
	<% // jgadela Rel 8.4 CR-854 [BEGIN]- Adding reporting language option
		// jgadela Rel 8.4- CR-854 IR T36000023762  [BEGIN]- Adding default language
		String repLang = user.getAttribute("reporting_language");
		if(StringFunction.isBlank(repLang)){
			repLang = "en_CA";
		}
		// jgadela Rel 8.4- CR-854 IR T36000023762  [END]- Adding default language
		
		options = Dropdown.createSortedRefDataOptions("REPORTING_LANGUAGE", repLang, loginLocale);
		if (insertMode) {
			defaultText = resMgr.getText("UserDetail.selectReportingLanguage",
		                  TradePortalConstants.TEXT_BUNDLE);
		} else {
			defaultText = "";
		}
	%>
	<%=widgetFactory.createSelectField("ReportingLanguage",
					"UserDetail.ReportingLanguage", " ", options, isReadOnly,
					true, false, "class='char25' onChange='onChangeRepLang()'", "", "")%>
	
	<% // jgadela Rel 8.4 CR-854 [END]- Adding reporting language option%>
<%}%>
	
	
	
	<%
        options = Dropdown.createSortedRefDataOptions("TIMEZONE",
                                user.getAttribute("timezone"),
                                loginLocale);
        if (insertMode) {
          defaultText = resMgr.getText("UserDetail.selectTimeZone",
                                       TradePortalConstants.TEXT_BUNDLE);
        } else {
          defaultText = "";
        }
	%>	
	<%=widgetFactory.createSelectField("TimeZone",
					"UserDetail.TimeZone", " ", options, isReadOnly,
					true, false, "class='char25'", "", "")%>	
	<%
        options = Dropdown.createSortedRefDataOptions("WIP_VIEW",
                            user.getAttribute("default_wip_view"), loginLocale);
        if (insertMode) {
          defaultText = resMgr.getText("UserDetail.selectWIP",
                                       TradePortalConstants.TEXT_BUNDLE);
        } else {
          defaultText = "";
        }
	%>	
	<%=widgetFactory.createSelectField("DefaultWorkView",
					"UserDetail.DefaultWorkView", " ", options, isReadOnly,
					true, false, "class='char25'", "", "")%>
	<%
        options = Dropdown.createSortedRefDataOptions("DATEPATTERN",
                                user.getAttribute("datepattern"),
                                loginLocale);        
	%>
		<% 
	if (!isReadOnly){ 
	%>
	<%=widgetFactory.createSelectField("DatePattern",
					"UserDetail.DatePattern", " ", options, isReadOnly,
					true, false, "class='char25'", "", "")%>
	<%} %>																																		
</div>