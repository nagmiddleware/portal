<%--
*******************************************************************************
       Notification Rule Detail - Email Alerts (Tab) Page
  
Description:   This jsp simply displays the html for the general portion of 
               the NotificationRuleDetail.jsp.  This page is called from 
               NotificationRuleDetail.jsp  and is called by:

	       <%@ include file="NotificationRuleDetail-EmailAlerts.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%> 
<%        
secureParms.put("name", notificationRule.getAttribute("name"));	 
%>
		&nbsp;&nbsp;&nbsp;<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailAlertMessage") %>
		<br>
		
        <%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_TRANSACTION" ,"NotificationRuleDetail.WithReceipt",TradePortalConstants.NOTIF_RULE_TRANSACTION,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_TRANSACTION), isReadOnly, "", "")%>                          
        <br>
        
        <%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_DAILY" ,"NotificationRuleDetail.Daily",TradePortalConstants.NOTIF_RULE_DAILY,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY), isReadOnly, "", "")%>                         
        <br>
        
        <%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_BOTH" ,"NotificationRuleDetail.Both",TradePortalConstants.NOTIF_RULE_BOTH,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_BOTH), isReadOnly, "", "")%>                         
        <br>
              
        <%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_NOEMAIL" ,"NotificationRuleDetail.NoEmail",TradePortalConstants.NOTIF_RULE_NOEMAIL,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL), isReadOnly, "", "")%>                         
                                  
        <%-- 14 Dec. 2012 Pratee ## Commented below code as per Kim Mail on 13th Dec. 2012 --%>
        <%-- <%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_NONOTIFICATION" ,"NotificationRuleDetail.NoNotifications",TradePortalConstants.NOTIF_RULE_NONOTIFICATION ,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NONOTIFICATION ), isReadOnly, "", "")%> --%>

  <input type="hidden" name="emailForMessage" value="<%=emailForMsg%>">
  <input type="hidden" name ="emailForDiscrepancy" value="<%=emailForDisc%>">
  <input type="hidden" name ="emailForRecPayMatch" value="<%=emailForRecPayMatch%>">
  <input type="hidden" name ="emailForSettlementInstructionReq" value="<%=emailForSettlementInstructionReq%>">
