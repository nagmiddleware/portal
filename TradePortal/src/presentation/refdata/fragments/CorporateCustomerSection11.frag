<%--this fragment is included in CorporateCustomerDetail.jsp --%>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable" id="aliasTable">
    <thead>
      <tr>
        <th align="left"><%=widgetFactory.createSubLabel("CorpCust.AliasNum")%></th>
        <th align="left"><%=widgetFactory.createSubLabel("CorpCust.MsgCategory")%></th>
      </tr>
    </thead>
    <tbody>
    
      <%--passes in aliasList, isReadOnly, isFromExpress, loginLocale,
          plus new aliasIndex below--%>
<%
  int aliasIndex = 0;
%>
      <%@ include file="CorporateCustomerAliasRows.frag" %>   	

    </tbody>
  </table>

<%
  if (!isReadOnly) {
%>
  <div>
    <button data-dojo-type="dijit.form.Button" type="button" id="add4MoreAlias">
    <%=resMgr.getText("CorpCust.Add4Aliases",TradePortalConstants.TEXT_BUNDLE)%>
      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        local.add4MoreAlias();
      </script>
    </button>
  </div>
<%
  }
%>
