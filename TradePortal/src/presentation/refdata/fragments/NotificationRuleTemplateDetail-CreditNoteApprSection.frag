<%--
*******************************************************************************
       Notification Rule Detail - CreditNoteAppr setting section
  
Description:    This jsp simply displays the html for the CreditNoteAppr setting 
		of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-CreditNoteApprSection.frag" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>
<%--Host to Host Invoice/Credit Note Approvals sections begins here --%>

<%
			String defCCSendEmailSetting = "";
			String defCCNotifEmailFerquency = "";
			String defApplytoAllTransCNApprovals ="";
			String defClearAllTransCNApprovals = "";
			

if (getDataFromDoc){	
	defCCSendEmailSetting = "";
	defCCNotifEmailFerquency = "";
	defApplytoAllTransCNApprovals = "";
	defClearAllTransCNApprovals = "";
	
	DocumentHandler notifyRuleDefInstrDoc = (DocumentHandler) defListVector.elementAt(33);
		if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_email_setting"))){
	defCCSendEmailSetting = notifyRuleDefInstrDoc.getAttribute("/send_email_setting");
		}
		if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/notify_email_freq"))){
	defCCNotifEmailFerquency = notifyRuleDefInstrDoc.getAttribute("/notify_email_freq");
				}
		if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran"))){
	defApplytoAllTransCNApprovals = notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran");
		}
			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran"))){
	defClearAllTransCNApprovals = notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran");
			}
	
}
%>
<%=widgetFactory.createSectionHeader("35", "NotificationRuleDetail.HosttoHostInvoiceSection", null, true) %>     
	<%=widgetFactory.createNote("NotificationRuleDetail.CreditNoteInvoicesNote","HosttoHostInvoiceNote1")%>   
		<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.HosttoHostInvoiceNote2") %>	
	<table width="100%">
		<tr>
			<td width="30%">
				<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
			</td>
			<td width="8%">
				&nbsp;
			</td>
			<td width="13.5%">

			</td>
			<td width="36%">
			
			</td>
		</tr>
		<tr>
			<td width="30%" style="vertical-align: top;">
				<%=widgetFactory.createRadioButtonField("emailSetting34","emailSettingYes34",
						"NotificationRuleDetail.Yes",TradePortalConstants.INDICATOR_YES,
						TradePortalConstants.INDICATOR_YES.equals(defCCSendEmailSetting), isReadOnly)%>                         
				<br>
				<%=widgetFactory.createRadioButtonField("emailSetting34","emailSettingNo34",
						"NotificationRuleDetail.No",TradePortalConstants.INDICATOR_NO,
						TradePortalConstants.INDICATOR_NO.equals(defCCSendEmailSetting), isReadOnly)%>
				<br><br>
				<%=widgetFactory.createLabel("", "NotificationRuleDetail.EmailInterval", false, false, false, "inline") %>
				<%= widgetFactory.createTextField("notifyEmailInterval34", "", defCCNotifEmailFerquency, "5", 
							isReadOnly,false,false,"","regExp:'^[1-9][0-9]*0$',invalidMessage:'"+resMgr.getText("NotificationRuleDetail.InvalidNotificationEmailFreqValue",TradePortalConstants.TEXT_BUNDLE)+"'","none") %>
					
			</td>
			<td width="8%" style="vertical-align: top;">
				&nbsp;
			</td>
			<td width="13.5%">

			</td>
			<td width="36%" align='left' style="vertical-align: top;">
				<button data-dojo-type="dijit.form.Button"  name="ApplytoAllTransactions34" id="ApplytoAllTransactions34" type="button">
					<%=resMgr.getText("NotificationRuleDetail.ApplytoAllTransactions.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.applyToAllOtherTransactions(34, <%=instrTransCriteriaFieldStartCount[33]%>);
					</script>
				</button>
				<br>
				<button data-dojo-type="dijit.form.Button"  name="Override34" id="Override34" type="button">
					<%=resMgr.getText("NotificationRuleDetail.Override.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.overrideTransactionsSection(34, <%=instrTransCriteriaFieldStartCount[33]%>, '<%=sectionDialogTitleArray[33]%>','OTHERS');
					</script>
				</button>
				<button data-dojo-type="dijit.form.Button"  name="ClearAll34" id="ClearAll34" type="button">
					<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.clearAllOtherSection(34, <%=instrTransCriteriaFieldStartCount[33]%>);
					</script>
				</button>	
				<input type="hidden" name="instrumentType34" id="instrumentType34" value="H2H_INV_CRN">
				<input type="hidden" name="applyInst34" id="applyInst34" value="<%=defApplytoAllTransCNApprovals%>">
				<input type="hidden" name="clearAll34" id="clearAll34" value="<%=defClearAllTransCNApprovals%>">
				
			
			</td>
		</tr>		
	</table> 
	<%
		transArray = instrTransArrays[33];
				
	    		for(int tLoop = 0; tLoop < transArray.length; tLoop++){
	    			String key;
	   			    key = instrumentCatagory[33]+"_"+transArray[tLoop];

	   			    NotificationRuleCriterionWebBean notifyRuleCriterion = null;
	   			    notifyRuleCriterion = (NotificationRuleCriterionWebBean)criterionRuleTransList.get(key);
	   			    yy++;
	   			 	if (notifyRuleCriterion != null) {
	   		      		//load using OID
	 					out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + notifyRuleCriterion.getAttribute("instrument_type_or_category") + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + notifyRuleCriterion.getAttribute("transaction_type_or_action") + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='" + notifyRuleCriterion.getAttribute("criterion_oid") + "'>");
	   		      		out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_email_setting") + "'>");
	   		      		out.print("<input type=hidden name='notify_email_freq"+yy+"' id='notify_email_freq"+yy+"' value='" + notifyRuleCriterion.getAttribute("notify_email_freq") + "'>");
	   		      		
		   		    } else {//if it is a new form
					
		   		    	out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + instrumentCatagory[33] + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + transArray[tLoop] + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='0'>");		   		     	
		   		      	out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value=''>");
		   		      	out.print("<input type=hidden name='notify_email_freq"+yy+"' id='notify_email_freq"+yy+"' value=''>");
		   		      	
		   		      	
		   		    }
				}//ent tLoop
		
	%>
	<table width="100%">
			<% if(dataList.contains(InstrumentType.HTWOH_INV_INST_TYPE)) {%>
				<span class="availNotifyItemSelect">
							<%=resMgr.getText("NotificationRuleDetail.DataExists",TradePortalConstants.TEXT_BUNDLE)%>
						</span>
			<% } %>
	</table>
<%
%>
</div><%--Host to Host Invoice/Credit Note Approvals sections ends here --%>