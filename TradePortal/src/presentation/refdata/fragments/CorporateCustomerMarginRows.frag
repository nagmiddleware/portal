<%--this fragment is included in both the 
    CorporateCustomerSection12 fragment for existing rows as well as the  CorporateCustomerAddMarginRows.jsp servlet for adding new rows via ajax.
  Passed variables: marginRuleList - a list of margin rules, marginIndex - an integer value, zero based of the 1st,isReadOnly,isFromExpress,loginLocale--%>

<%	
  for (int idx=0; idx<marginRuleList.size(); idx++) { 
    CustomerMarginRuleWebBean marginRule = (CustomerMarginRuleWebBean)marginRuleList.get(idx);
%>

  <tr id="marginIndex<%=marginIndex+idx%>">
    <td align="center"><%=marginIndex+idx+1%></td>
    <td align="center">
<% 			 			 
    String options1 = Dropdown.createSortedCurrencyCodeOptions(marginRule.getAttribute("currency"), loginLocale); 
    out.println(widgetFactory.createSelectField("marginCurr" + (marginIndex+idx), "", " ", options1, 
                  isReadOnly, false, false, "class='char4' onChange=\"local.enableMarginThresholdAmt("+(marginIndex+idx)+")\"", "", "none")); 	  		  	
    out.print("<INPUT TYPE=HIDDEN NAME='customerMarginRuleOid" + (marginIndex+idx) + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes(marginRule.getAttribute("customer_margin_rule_oid"), userSession.getSecretKey())+ "'>");
%>
    </td>
    <td align="center">
<%
    options1 = Dropdown.createSortedRefDataOptions("INSTRUMENT_TYPE", marginRule.getAttribute("instrument_type"), loginLocale);
    out.print(widgetFactory.createSelectField("instrumentType" + (marginIndex+idx), "", " ", options1, isReadOnly, false, false, "", "", "none"));
%>
    </td>
    <td align="center">
<% 
    String thresholdAmt1="";
    if (InstrumentServices.isNotBlank(marginRule.getAttribute("threshold_amt")) && InstrumentServices.isNotBlank(marginRule.getAttribute("currency"))) {
      thresholdAmt1 = TPCurrencyUtility.getDisplayAmount(
        marginRule.getAttribute("threshold_amt"), 
        marginRule.getAttribute("currency"), loginLocale);			
    } 
    else {
      thresholdAmt1 = marginRule.getAttribute("threshold_amt");
    }
    String thresholdAmtHtmlProps = "";
    if (InstrumentServices.isBlank(marginRule.getAttribute("currency"))) {
      thresholdAmtHtmlProps = " disabled=\"disabled\"";
    }
%>	 
      <%= widgetFactory.createTextField( "thresholdAmount" + (marginIndex+idx), "", thresholdAmt1, "20", 
            isReadOnly, false, false, "class='char14'"+thresholdAmtHtmlProps, "", "none") %>
    </td>
    <td align="center">
<%
    options1 = Dropdown.createSortedRefDataOptions("REFINANCE_RATE_TYPE", marginRule.getAttribute("rate_type"), loginLocale);
           		out.print(widgetFactory.createSelectField("rateType" + (marginIndex+idx), "", " ", options1, isReadOnly, false, false, "class='char8'", "", "none"));
%>
    </td>
    <td align="center">
      <%= widgetFactory.createTextField( "margin" + (marginIndex+idx), "", marginRule.getAttribute("margin"), "35", isReadOnly, false, false, "class='char10'", "", "none") %>
    </td>      	 
  </tr>
<%
  } 
%>
