<%--
*******************************************************************************
             PO Upload Definition Detail - File Definition (Tab) Page

  Description:   This jsp simply displays the html for the file tab portion of 
the PO Upload Definition.jsp.  This page is called from 
PO UploadDefinition Detail.jsp  and is called by:
		<%@ include file="POUploadDefinitionDetail-File.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to 
avoid being overwritten as each tab is a submit action.  As a result, if this 
data does not make it into the xml document, then it may be overwritten with a 
null value in this tab or another.
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<script type="text/javascript">

function setChg(ind){
	if(document.getElementById("chg_order_"+ind)){
		
		var temp_val= document.getElementById("chg_order_"+ind).value;
		var new_val = document.getElementById("text"+ind).value;
		if(parseInt(new_val)>parseInt(ind)){
			document.getElementById("chg_order_"+ind).value="3";
		}
		else if(parseInt(new_val)<parseInt(ind)){
			document.getElementById("chg_order_"+ind).value="1";
		}
	}	
}





function test1()
{
	 
	var data = new Array();
	var tab = document.getElementById( 'poFileOrderTable' );
	var rows1 = tab.rows;
	var loop=0;
	var data1 = new Array();
	var dataLoop=0;
	var arrTemp=new Array();
	var cnt=0;
	for ( var x = 1; x < rows1.length; x++ ) 
	{
			var td = rows1[x].getElementsByTagName( 'td' );
			data1 = new Array();
			loop = 0;
			num=x-1;
			for ( var y = 0; y < td.length; y++ ) 
			{
				var input = td[y].getElementsByTagName( 'input' );
				for ( var z = 0; z < input.length; z++ ) 
				{
					
					 if(input[z].id.indexOf("text")!=-1 || input[z].id.indexOf("upload_order_")!=-1){
							data1[loop]=input[z].value;
							loop= loop+1;
							data1[loop]=input[z].id;
							loop= loop+1;
						  }
				}
				var c = td[y].innerText;
				c = c.replace(/^\s+|\s+$/g,'');

				if(c.length>0){
					var temp=td[y].id;
					if(temp.indexOf("fileFieldName")!=-1){
						temp = temp.replace("fileFieldName","");
						num=temp;
						arrTemp[cnt]=parseInt(num);
						cnt=cnt+1;
					}else if(temp.indexOf("fileFieldName")==-1){
						continue;	
					}
					
					data1[loop] = document.getElementById("fileFieldName"+num).id;
					loop = loop+1;
					data1[loop] = document.getElementById("fileFieldName"+num).innerText;
					loop = loop+1;
					
				}
			}
			
			

			data1[loop]= document.getElementById("upload_order_"+num).value;
			loop=loop+1;
			data1[loop]=document.getElementById("upload_order_"+num).id;
			loop=loop+1;
			data1[loop] = document.getElementById("chg_order_"+num).value;
			loop = loop+1;
			data[dataLoop]=data1;
			dataLoop=dataLoop+1;
			
	}
	
	data.sort(mySorting1);
	
	
	function mySorting1(a, b) {

  var o1 = parseInt(a[0]);
  var o2 = parseInt(b[0]);

  var p1 = parseInt(a[6]);
  var p2 = parseInt(b[6]);

  if (o1 != o2) {
    if (o1 < o2) return -1;
    if (o1 > o2) return 1;
    return 0;
  }
  if (p1 < p2) return -1;
  if (p1 > p2) return 1;
  return 0;
}

	
	
	    var innerHtml = "";
		var i=0;
		for(var ind=arrTemp.length-1; ind>=1;ind--)
		{
			
			i=arrTemp[ind];
			if(dijit.byId("upload_order_"+i))
				dijit.byId("upload_order_"+i).destroy(true);
			if(document.getElementById("upload_order_"+i))
				document.getElementById("upload_order_"+i).removeAttribute("id");
			
			if(dijit.byId("fileFieldName"+i))
				dijit.byId("fileFieldName"+i).destroy(true);
			if(document.getElementById("fileFieldName"+i))
				document.getElementById("fileFieldName"+i).removeAttribute("id");
			
			if(dijit.byId("text"+i))
				dijit.byId("text"+i).destroy(true);
			if(document.getElementById("text"+i))
				document.getElementById("text"+i).removeAttribute("id");

			if(dijit.byId("chg_order_"+i))
				dijit.byId("chg_order_"+i).destroy(true);
			if(document.getElementById("chg_order_"+i))
				document.getElementById("chg_order_"+i).removeAttribute("id");

			tab.deleteRow(ind+1);
		}
		i=1;
			document.getElementById("upload_order_"+i).id="upload_order_0";
			document.getElementById("fileFieldName"+i).id="fileFieldName0";
			document.getElementById("text"+i).id=="text0"	
			document.getElementById("chg_order_"+i).id="chg_order_0";
		i=1;
		if(dijit.byId("upload_order_"+i))
			dijit.byId("upload_order_"+i).destroy(true);
		if(document.getElementById("upload_order_"+i))
			document.getElementById("upload_order_"+i).removeAttribute("id");
		
		if(dijit.byId("fileFieldName"+i))
			dijit.byId("fileFieldName"+i).destroy(true);
		if(document.getElementById("fileFieldName"+i))
			document.getElementById("fileFieldName"+i).removeAttribute("id");
		
		if(dijit.byId("text"+i))
			dijit.byId("text"+i).destroy(true);
		if(document.getElementById("text"+i))
			document.getElementById("text"+i).removeAttribute("id");

		if(dijit.byId("chg_order_"+i))
			dijit.byId("chg_order_"+i).destroy(true);
		if(document.getElementById("chg_order_"+i))
			document.getElementById("chg_order_"+i).removeAttribute("id");
		
		
		
		for(var i=0;i<data.length;i++)
		{
				var rowCount = tab.rows.length;	
				var data_temp1 = data[i];
				var newRow = tab.insertRow(rowCount);
				var cell0 = newRow.insertCell(0);
				var cell1 = newRow.insertCell(1);
				var num = i+1;
				cell0.width="10px";
				
				newRow.id = rowCount;	
				var gTxtBox = new dijit.form.TextBox({
			        name: "text"+num,
			        id: "text"+num,
			        value: num,
			        trim:true
			 	});
				require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
					ready(function(){
				 dojo.connect(registry.byId('text'+num),'onChange',function(){
					 var temp = this.id;
					 temp = temp.replace("text","");
					 setChg(temp);
					  
			    });})});
				
				
				dojo.addClass(gTxtBox.domNode, "selectedItemOrderTextBox");
				cell0.setAttribute("style","width:20%");
			    cell0.setAttribute("align","left");
			    cell0.appendChild(gTxtBox.domNode);
			   
				cell1.innerText=data_temp1[3];
			    cell1.setAttribute("id","fileFieldName"+num);
			    cell1.setAttribute("align","left"); 

				require(["dojo/parser"], function(parser) {
			     	parser.parse(newRow.id);     	
				 });

			    
			    var input1 = document.createElement("input");

			    input1.setAttribute("type", "hidden");
				input1.setAttribute("name", "upload_order_"+num);
				input1.setAttribute("id","upload_order_"+num);
				input1.setAttribute("value", data_temp1[4]);
				
				 var input2 = document.createElement("input");
			    input2.setAttribute("type", "hidden");
				input2.setAttribute("name", "chg_order_"+num);
				input2.setAttribute("id","chg_order_"+num);
				input2.setAttribute("value", "2");
				newRow.appendChild(input1);
				newRow.appendChild(input2);
			
		}
		 i=0;
			  if(dijit.byId("upload_order_"+i))
					dijit.byId("upload_order_"+i).destroy(true);
				if(document.getElementById("upload_order_"+i))
					document.getElementById("upload_order_"+i).removeAttribute("id");
				
				if(dijit.byId("fileFieldName"+i))
					dijit.byId("fileFieldName"+i).destroy(true);
				if(document.getElementById("fileFieldName"+i))
					document.getElementById("fileFieldName"+i).removeAttribute("id");
				
				if(dijit.byId("text"+i))
					dijit.byId("text"+i).destroy(true);
				if(document.getElementById("text"+i))
					document.getElementById("text"+i).removeAttribute("id");
				if(dijit.byId("chg_order_"+i))
					dijit.byId("chg_order_"+i).destroy(true);
				if(document.getElementById("chg_order_"+i))
					document.getElementById("chg_order_"+i).removeAttribute("id");
				tab.deleteRow(1);
		  
	    
}


function poUploadFileUpdateOrder(gIndex){
	
	test1();
	
	
}

</script>

<%
Debug.debug("******************** IN FILE DEFINITION TAB JSP *************************");	

final int NUMBER_OF_FILE_DEF_ORDER_FIELDS = 31;

String fieldNameOptionList = poUploadDef.buildFieldOptionList();
Debug.debug("(File)fieldNameOptionList :  "+fieldNameOptionList);

//Parse fieldNameOptionList, get map as key:value
Map<Integer, List> hashMap = poUploadDef.parseFieldOptionsList(fieldNameOptionList, "POUploadDefinitionDetail-File");
List<String> list = null;

int x=1;

String defaultText = " ";
String options;
fileFormatType = poUploadDef.getAttribute("file_format");
poDefTypeManualOnly = poUploadDef.getAttribute("definition_type").equals(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL);

//This will ensure that the 'update message' will have the correct reference to this 
//PO Upload Defn.  Otherwise 'null' is substituted in for the defnintion name inthe message.

secureParms.put("name", poUploadDef.getAttribute("name"));

//Because these are check boxes, and the tabs are submit actions - we need to ensure that
//These values make it to the mapfile otherwise the premediator will reset the values to 'N'

//secureParms.put("default_flag", poUploadDef.getAttribute("default_flag"));
//secureParms.put("include_column_header", poUploadDef.getAttribute("include_column_header"));
//secureParms.put("include_footer", poUploadDef.getAttribute("include_footer"));
//cquinton 10/21/2013 Rel 8.3 ir#21748 include_po_text is another that should not be explicitly set on
// page entry. comment it out
//secureParms.put("include_po_text", poUploadDef.getAttribute("include_po_text"));
%>


  <% if (!poDefTypeManualOnly) { //Display the file_format only if the po definition may be used for file upload
  %>

<% } // End if (poDefTypeManualOnly)
%>

<table id="poFileOrderTable" class="formDocumentsTable" border=1 style="width:30%" cellspacing="0" >
	<thead>
	<tr style="height:35px">
		<th style="text-align:left;"><%= resMgr.getText("POUploadDefinitionDetail.Order", TradePortalConstants.TEXT_BUNDLE) %></th>
		<th style="text-align:left;"><%= resMgr.getText("POUploadDefinitionDetail.FieldName", TradePortalConstants.TEXT_BUNDLE) %></th>
	</tr>
	</thead>
	<tbody>
<%
   for( x=1; x <= hashMap.size(); x++ ) {
	   
	   list = hashMap.get(x);
		   
%>	

	<tr>
		<td align="center" style="width:20%">
			
   			<input type="text" name="" value='<%=x %>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true" 
   				id='text<%=x %>'  <%=(isReadOnly?"readonly":"")%> class="selectedItemOrderTextBox" onchange="setChg(<%=x%>);">
		</td>
		<td id='fileFieldName<%=x %>' align="left"><%= list.get(1) %></td>

			<input type=hidden name='upload_order_<%=x %>'  
						id='upload_order_<%=x %>'
				value='<%=list.get(0) %>'>
			<input type=hidden name='chg_order_<%=x %>'  
						id='chg_order_<%=x %>'
				value='2'>
				
				
	</tr>
<%	
  }
%>
</tbody>
</table>
  
<br>
<button data-dojo-type="dijit.form.Button" id="updateOrder" <%=(isReadOnly?"disabled":"")%>
		onClick="poUploadFileUpdateOrder(<%=x %>);" type="button">
		<%=resMgr.getText("POUploadDefnSelector.UpdateOrder",TradePortalConstants.TEXT_BUNDLE)%>
</button>
