<%--
*******************************************************************************
                       User Detail - SubsidiaryAccess Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<%
  // Setup the has subsidiary access radio button options; if the
  // indicator is empty or "N", make the first option the default
  boolean hasSubsidiaryAccess = false;
  if (user.getAttribute("customer_access_ind").equals("Y")) {
     hasSubsidiaryAccess = true;
  } else {
     hasSubsidiaryAccess = false;
  }
%>
  <div class="formItem">
    <%--Added defect #4161 by dillip --%>
    <%=widgetFactory.createRadioButtonField(
         "SubsidiaryAccess", "NotAbleSubsidiary",
         "UserDetail.NotAbleSubsidiary", "N", !hasSubsidiaryAccess,
         isReadOnly, "labelClass=\"formWidth\"", "")%>
    <%--Ended Here defect #4161 by dillip --%>
    <div style="clear:both;"></div>

    <%=widgetFactory.createRadioButtonField(
         "SubsidiaryAccess", "AbleSubsidiary",
         "UserDetail.AbleSubsidiary", "Y", hasSubsidiaryAccess,
         isReadOnly, "labelClass=\"formWidth\"", "")%>
    <div style="clear:both;"></div>

    <%=widgetFactory.createCheckboxField("SubAccessViewParentOrgData","UserDetail.SubAccessRefdata",
		user.getAttribute("use_data_while_in_sub_access").equals(TradePortalConstants.INDICATOR_YES),isReadOnly,false,
		"onClick='setSubAccessRadioButton();' labelClass=\"formWidth\"", "", "")%>
  </div>

  <div class='formItem<%=isReadOnly?" readOnly":""%>'>
    <%=widgetFactory.createInlineLabel("SubsidiaryAccessSecurityProfile","UserDetail.SubsidiarySecurityProfile")%>
    <div style="clear:both;"></div>
    <%=widgetFactory.createNote("UserDetail.SubsidiarySecurityRights")%>
    <div style="clear:both;"></div>

<%
  options = ListBox.createOptionList(secProfileDoc,
    "SECURITY_PROFILE_OID", "NAME",
    user.getAttribute("cust_access_sec_profile_oid"), userSession.getSecretKey());
  defaultText = resMgr.getText("UserDetail.selectProfile",
    TradePortalConstants.TEXT_BUNDLE);
%>	
    <%--SPenke T36000006206 10-10-2012 Begin --%>	
    <%=widgetFactory.createSelectField("SubsidiaryAccessSecurityProfile",
         "", " ", options, isReadOnly,
         false, false, "onChange='document.UserDetail.SubsidiaryAccess[1].click()'", "", "none")%>	
    <%--SPenke T36000006206 10-10-2012 End --%>				
  </div>
  <br/>
<div class="formItem">
  <%=widgetFactory.createCheckboxField("SubsidConfidentialPaymentInd","UserDetail.AccessToSubsidConfidentialPayment",
       TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("subsid_confidential_indicator")),isReadOnly,false,
       "labelClass=\"formWidth\"","","")%>

  
   <div class="formItemWithIndent2">
      <%=widgetFactory.createNote("UserDetail.AccessToSubsidiaryConfidentialNote")%>
      </div>	
</div>

<%
  options = ListBox.createOptionList(thresholdGroupDoc,
    "THRESHOLD_GROUP_OID",
    "THRESHOLD_GROUP_NAME",
    user.getAttribute("subsidiary_threshold_group_oid"),
    // Encrypt the OID
    userSession.getSecretKey());
  defaultText = resMgr.getText("UserDetail.selectThreshold",
    TradePortalConstants.TEXT_BUNDLE);
%>
				
  <%=widgetFactory.createSelectField("SubsidiaryThresholdGroup",
       "UserDetail.SubsidiaryThresholdGroup", " ", options, isReadOnly,
       false, false, "", "", "")%>	
				
<%
  options = ListBox.createOptionList(workGroupDoc,
    "WORK_GROUP_OID",
    "WORK_GROUP_NAME",
    user.getAttribute("subsidiary_work_group_oid"),
    // Encrypt the OID
    userSession.getSecretKey());
    defaultText = resMgr.getText("UserDetail.selectWorkGroup",
      TradePortalConstants.TEXT_BUNDLE);
%>
  <%=widgetFactory.createSelectField("SubsidiaryWorkGroup",
       "UserDetail.SubsidiaryWorkGroup", " ", options, isWorkGroupReadOnly,
       false, false, "", "", "")%>					

  <div class="formItem">
    <%=widgetFactory.createInlineLabel("","UserDetail.UseProfileInSubsidiaryMode")%>	
    <div style="clear:both;"></div>
<%
  //Use Parent or Subsidiary Customer profile in Subsidiary mode
  boolean useSubsidiaryProfile = false;
  if (user.getAttribute("use_subsidiary_profile").equals("Y")) {
    useSubsidiaryProfile = true;
  } else {
    useSubsidiaryProfile = false;
  }
%>
    <%=widgetFactory.createRadioButtonField(
         "useSubsidiaryProfile","UseParentProfile","UserDetail.UseParentProfile", "N",
         !useSubsidiaryProfile, isReadOnly, "labelClass=\"formWidth\" onClick=\"enablePanelSelection();\"", "")%>
    <div style="clear:both;"></div>

    <%=widgetFactory.createRadioButtonField(
         "useSubsidiaryProfile","UseSubsidiaryProfile","UserDetail.UseSubsidiaryProfile", "Y",
         useSubsidiaryProfile, isReadOnly, "labelClass=\"formWidth\" onClick=\"enablePanelSelection();\"", "")%>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>

<%--CR 821 - Rel 8.3 START--%>
  <div class='formItem<%=isReadOnly?" readOnly":""%>'>
    <%=widgetFactory.createInlineLabel("PanelAuthority","UserDetail.PanelAutority")%>
    <div style="clear:both;"></div>
    <%=widgetFactory.createNote("UserDetail.PanelGroupNote")%>
    <div style="clear:both;"></div>
<%
//Commented for CR 821 Rel 8.3
//  options = Dropdown.createSortedRefDataOptions("PANEL_AUTH_TYPE",user.getAttribute("sub_panel_authority_code"),loginLocale);
		
/* IR#T36000020800 -- Prateep# We have to set sub_panel_authority_code here in this section as same options we are using in section4 */
		panelAliasesOptions = widgetFactory.createSortedOptions("PANEL_AUTH_TYPE", panelAliasesMap, user.getAttribute("sub_panel_authority_code"));
/* IR#T36000020800 -- Prateep End */ 


  if (isReadOnly) {
    defaultText = "";
  } 
  else {
    defaultText = resMgr.getText("UserDetail.selectPanelAutority",
      TradePortalConstants.TEXT_BUNDLE);
  }
%>
    <%=widgetFactory.createSelectField("SubsidiaryPanelAuthority",
					"", " ", panelAliasesOptions, isReadOnly,
					false, false, "", "", "none")%>
  </div>
 
 
  <%=widgetFactory.createCheckboxField("SubsidiaryAuthorizeOwnInputInd","UserDetail.AuthorizeOwnOutput",
       TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("sub_authorize_own_input_ind")),isReadOnly)%>
       
    <div class="formItemWithIndent2">
      <%=widgetFactory.createNote("UserDetail.AuthorizeOwnOutputNote")%>
    </div>
    
<%--CR 821 - Rel 8.3 END--%>    