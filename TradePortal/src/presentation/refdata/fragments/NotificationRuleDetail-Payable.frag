<%--
*******************************************************************************
       Notification Rule Detail - Payables Page
  
Description:    This jsp simply displays the html for notification payables, which is included in NotificationRuleDetail.jsp.  


*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>

<%

String[] payTransactionTypes	= {TradePortalConstants.PAYABLES_CREATE,
    					TradePortalConstants.PAYABLES_CHANGE,    
    					TradePortalConstants.PAYABLES_PAYMENT};

String[] payCriterionTypes		= {TradePortalConstants.NOTIF_RULE_NOTIFICATION, TradePortalConstants.NOTIF_RULE_EMAIL};
    					
secureParms.put("name", notificationRule.getAttribute("name"));

%>  
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
	<thead>
		<tr>
			<th class="columnWidth200"><%= resMgr.getText("NotificationRuleDetail.Payables", TradePortalConstants.TEXT_BUNDLE)%></th>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Notification", TradePortalConstants.TEXT_BUNDLE)%></th>
			
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Email", TradePortalConstants.TEXT_BUNDLE)%></th>	
					
		 </tr>
		 <tr>
		    <th>&nbsp;</th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		 </tr>
	 </thead>
	 <tbody>
	 <% 
	 for (int transCount=0; transCount<payTransactionTypes.length; transCount++) 
	 { 
	   String[] criterionRuleOid		= new String[2];
	   String[] criterionRuleSetting 	= {//TradePortalConstants.NOTIF_RULE_ALWAYS,
	   						TradePortalConstants.NOTIF_RULE_NONE,
	   						TradePortalConstants.NOTIF_RULE_NONE};
	   String transactionTypeLabel           = payTransactionTypes[transCount];
  	%>
    <tr>
    	<td align="left">
    	<%=widgetFactory.createSubLabel("NotificationRuleDetail.TransactionType." + transactionTypeLabel)%> 	
    	</td>
<%

  // If the notification rule specifies that only daily emails are sent then do not display
  // email criterion settings. Daily emails are driven by messages/notifications already 
  // present in the portal, not the notification rule's email settings
  int criterionTypeCount = 2;
   
  for (int criterionCount=0; criterionCount<criterionTypeCount; criterionCount++)
  {
    String key;
    key = TradePortalConstants.PAYABLE_MANAGEMENT;
    key = key.concat(payTransactionTypes[transCount]);
    key = key.concat(payCriterionTypes[criterionCount]);

    NotifyRuleCriterionWebBean notifyRuleCriterion = null;
    notifyRuleCriterion = (NotifyRuleCriterionWebBean)criterionRuleList.get(key);
     
    if (notifyRuleCriterion != null)
    {
      criterionRuleOid[criterionCount] = notifyRuleCriterion.getAttribute("criterion_oid");
      criterionRuleSetting[criterionCount] = notifyRuleCriterion.getAttribute("setting");
    }
    
    String criterionRuleType = payCriterionTypes[criterionCount];
    criterionRuleCount++;
    
%>

    <jsp:include page="/refdata/NotificationRuleDetailRow.jsp">
      <jsp:param name="transactionTypes" 	value='<%=payTransactionTypes[transCount]%>' />    
      <jsp:param name="instrCategory" 		value='<%=TradePortalConstants.PAYABLE_MANAGEMENT%>' />
      <jsp:param name="criterionRuleOid"	value='<%=criterionRuleOid[criterionCount]%>' />
      <jsp:param name="criterionRuleSetting" 	value='<%=criterionRuleSetting[criterionCount]%>' />
      <jsp:param name="criterionRuleType" 	value='<%=criterionRuleType%>' />
      <jsp:param name="criterionRuleCount" 	value='<%=criterionRuleCount%>' />	
      <jsp:param name="isReadOnly" 		value='<%=isReadOnly%>' />
    </jsp:include>
        
<%
  }
}
%>
</tbody>
</table><br></br>