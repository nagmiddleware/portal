<%--this fragment is included in both the 
    CorporateCustomerSection13 fragment for existing rows as well as the
    CorporateCustomerAddSPMarginRows.jsp servlet for adding new rows via ajax.

  Passed variables:
    marginRuleList - a list of margin rules
    marginIndex - an integer value, zero based of the 1st 
    isReadOnly
    isFromExpress
    loginLocale
--%>
<%-- Ravindra - CR-708B - Initial version --%>
<%	
  for (int idx=0; idx<spMarginRuleList.size(); idx++) { 
    SPMarginRuleWebBean spMarginRule = (SPMarginRuleWebBean)spMarginRuleList.get(idx);
%>

  <tr id="spMarginIndex<%=spMarginIndex+idx%>">
    <td align="center"><%=spMarginIndex+idx+1%></td>
    <td align="center">
	<% 			 			 
	    String options1 = Dropdown.createSortedCurrencyCodeOptions(spMarginRule.getAttribute("currency"), loginLocale); 
	    out.println(widgetFactory.createSelectField("SpMarginCurr" + (spMarginIndex+idx), "", " ", options1, isReadOnly, false, false, "class='char4' onChange=\"local.enableSPMarginThresholdAmt("+(spMarginIndex+idx)+")\"", "", "none")); 	  		  	
	    out.print("<INPUT TYPE=HIDDEN NAME='SpCustomerMarginRuleOid" + (spMarginIndex+idx) + "' VALUE='" + EncryptDecrypt.encryptStringUsingTripleDes(spMarginRule.getAttribute("sp_margin_rule_oid"), userSession.getSecretKey())+ "'>");
	%>
    </td>
    <td align="center">
		<% 
		    String thresholdAmt1="";
		    if (InstrumentServices.isNotBlank(spMarginRule.getAttribute("threshold_amt")) && InstrumentServices.isNotBlank(spMarginRule.getAttribute("currency"))) {
		      thresholdAmt1 = TPCurrencyUtility.getDisplayAmount(
		      							spMarginRule.getAttribute("threshold_amt"), 
		        						spMarginRule.getAttribute("currency"), 
		        						loginLocale
		        						);			
		    } 
		    else {
		      thresholdAmt1 = spMarginRule.getAttribute("threshold_amt");
		    }
		    String thresholdAmtHtmlProps = "";
		    if (InstrumentServices.isBlank(spMarginRule.getAttribute("currency"))) {
		      thresholdAmtHtmlProps = " readonly ";
		    }
		%>	 
      <%= widgetFactory.createTextField( "SpThresholdAmount" + (spMarginIndex+idx), "", thresholdAmt1, "20", isReadOnly, false, false, "class='char14'"+thresholdAmtHtmlProps, "", "none") %>
    </td>
    <td align="center">
<%
    options1 = Dropdown.createSortedRefDataOptions("REFINANCE_RATE_TYPE", spMarginRule.getAttribute("rate_type"), loginLocale);
    out.print(widgetFactory.createSelectField("SpRateType" + (spMarginIndex+idx), "", " ", options1, isReadOnly, false, false, "class='char8' onChange=\"local.enableDisableSPMargin("+(spMarginIndex+idx)+")\"", "", "none"));
%>
    </td>
    <td align="center">
      <%= widgetFactory.createTextField( "SpMargin" + (spMarginIndex+idx), "", spMarginRule.getAttribute("margin"), "35", isReadOnly, false, false, "class='char10'", "", "none") %>
    </td>      	 
  </tr>
<%
  } 
%>
