<%
	
	defaultAdminAuthMethod = user.getAttribute("authentication_method");
	if ( insertMode && StringFunction.isBlank(defaultAdminAuthMethod) ) {	
	   StringBuilder authSql = new StringBuilder(" SELECT authentication_method FROM ");
	   String adminUserOwnerLevel = user.getAttribute("ownership_level");
	   String adminUserOwnerOrg = user.getAttribute("owner_org_oid");
	   if ( TradePortalConstants.OWNER_GLOBAL.equals(adminUserOwnerLevel) ) {
	       authSql.append(" global_organization WHERE organization_oid = ? ");
	   } else if (  TradePortalConstants.OWNER_BOG.equals(adminUserOwnerLevel) ) {
	       authSql.append(" bank_organization_group, client_bank WHERE bank_organization_group.p_client_bank_oid = client_bank.organization_oid  ");
	       authSql.append(" bank_organization_group.organization_oid = ? ");
	   } else {
	     authSql.append(" client_bank where organization_oid = ? ");
	   }
       DocumentHandler	authListDoc = DatabaseQueryBean.getXmlResultSet(authSql.toString(), false, adminUserOwnerOrg);
       if (authListDoc != null){
			defaultAdminAuthMethod = authListDoc.getAttribute("/ResultSetRecord(0)/AUTHENTICATION_METHOD");
		}
	}

	if(defaultAdminAuthMethod.equals(TradePortalConstants.AUTH_PASSWORD)){
		defaultDisplayDiv = "PasswordFields";
	}else if(defaultAdminAuthMethod.equals(TradePortalConstants.AUTH_CERTIFICATE)){
		defaultDisplayDiv = "CertificateFields";
	}else if(defaultAdminAuthMethod.equals(TradePortalConstants.AUTH_SSO)){
		defaultDisplayDiv = "SSOFields";
	}else if(defaultAdminAuthMethod.equals(TradePortalConstants.AUTH_2FA)){
		defaultDisplayDiv = "2FAFields";
	}

        Vector codesToExclude = new Vector();
        codesToExclude.addElement(TradePortalConstants.AUTH_PERUSER);
        codesToExclude.addElement(TradePortalConstants.AUTH_REGISTERED_SSO);
        codesToExclude.addElement(TradePortalConstants.AUTH_AES256);

        options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AUTHENTICATION_METHOD, 
                                                                 defaultAdminAuthMethod, 
                                                                 resMgr.getResourceLocale(), codesToExclude);
        String authDefaultText = "";	
  	    if(defaultAdminAuthMethod != ""){
  		   authDefaultText = resMgr.getText("AdminUserDetail.SelectAuthMethod", TradePortalConstants.TEXT_BUNDLE);
  	    }
%>

<%=widgetFactory.createSelectField("AuthenticationMethod", "Authentication Method", authDefaultText, options, isReadOnly, true, false, "onChange=\"toggleAuthMethodFields()\"", "", "")%>
 
<div id="CertificateFieldsDiv">
  <div class = "columnLeft">
    <%@ include file="UserCertificateFields.frag" %>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
   </div>
</div>

<div id="2FAFieldsDiv">
   <div class = "columnLeft">
      <%@ include file="User2FAFields.frag" %>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
      <div>&nbsp;</div>
   </div>
</div>

<div id="PasswordFieldsDiv">
<div class = "columnRight" id = "PWDcolumnRight">
<%@ include file="UserLoginPasswordFields.frag" %> 
</div>
</div>

<div id="SSOFieldsDiv">
<%@ include file="UserSSOFields.frag" %>
</div>
