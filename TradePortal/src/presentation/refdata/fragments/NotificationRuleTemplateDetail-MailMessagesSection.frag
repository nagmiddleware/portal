<%--
*******************************************************************************
       Notification Rule Detail - MailMessages setting section
  
Description:    This jsp simply displays the html for the Mail Messages setting 
		of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-MailMessagesSection.frag" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>
<%//Mail Messages Section begins below %>
<%
	String defApplytoAllTransMMessages = "";
	String defClearAllTransMMessages = "";
	String defSendEmailSettingMMessages = "";
if (getDataFromDoc){
	defSendEmailSettingMMessages = "";
	defApplytoAllTransMMessages = "";
	defClearAllTransMMessages = "";
	
	DocumentHandler notifyRuleDefInstrDoc = (DocumentHandler) defListVector.elementAt(31);
			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_email_setting"))){
	defSendEmailSettingMMessages = notifyRuleDefInstrDoc.getAttribute("/send_email_setting");
			}
			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran"))){
	defApplytoAllTransMMessages = notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran");
				}
						if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran"))){
	defClearAllTransMMessages = notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran");
						}
}
%>
	<%=widgetFactory.createSectionHeader("33", "NotificationRuleDetail.MailMessagesSection", null, true) %>       
		<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.MailMessagesNote") %>	
	<table width="100%">
		<tr>
			<td width="30%">
				<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
			</td>
			<td width="8%">
				&nbsp;
			</td>
			<td width="13.5%">

			</td>
			<td width="36%">
				
			</td>
		</tr>
		<tr>
			<td width="30%" style="vertical-align: top;">
				<%=widgetFactory.createRadioButtonField("emailSetting32","emailSettingAlways32",
						"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,
						TradePortalConstants.NOTIF_RULE_ALWAYS.equals(defSendEmailSettingMMessages), isReadOnly)%>                         
				<br>
				<%=widgetFactory.createRadioButtonField("emailSetting32","emailSettingNone32",
						"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,
						TradePortalConstants.NOTIF_RULE_NONE.equals(defSendEmailSettingMMessages), isReadOnly)%>
				
			</td>
			<td width="8%" style="vertical-align: top;">
				&nbsp;
			</td>
			<td width="13.5%" style="vertical-align: top;">
			
			</td>
			<td width="35%" style="vertical-align: top;">
				<button data-dojo-type="dijit.form.Button"  name="ApplytoAllTransactions32" id="ApplytoAllTransactions32" type="button">
					<%=resMgr.getText("NotificationRuleDetail.ApplytoAllTransactions.Button",TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.applyToAllMessageTransactions(32, <%=instrTransCriteriaFieldStartCount[31]%>);
					</script>
				</button>
				<br>
				<button data-dojo-type="dijit.form.Button"  name="Override32" id="Override32" type="button">
						<%=resMgr.getText("NotificationRuleDetail.Override.Button",TradePortalConstants.TEXT_BUNDLE)%>
						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.overrideTransactionsSection(32, <%=instrTransCriteriaFieldStartCount[31]%>, '<%=sectionDialogTitleArray[31]%>','MESSAGES');
						</script>
				</button>
				<button data-dojo-type="dijit.form.Button"  name="ClearAll32" id="ClearAll32" type="button">
						<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.clearAllMessagesSection(32, <%=instrTransCriteriaFieldStartCount[31]%>);

						</script>
				</button>
				<input type="hidden" name="instrumentType32" id="instrumentType32" value="MESSAGES">
				<input type="hidden" name="applyInst32" id="applyInst32" value="<%=defApplytoAllTransMMessages%>">
				<input type="hidden" name="clearAll32" id="clearAll32" value="<%=defClearAllTransMMessages%>">
				
			<%
        		transArray = instrTransArrays[31];
				for(int tLoop = 0; tLoop < transArray.length; tLoop++){
        			String key;
	   			    key = instrumentCatagory[31]+"_"+transArray[tLoop];
	
	   			    NotificationRuleCriterionWebBean notifyRuleCriterion = null;
	   			    notifyRuleCriterion = (NotificationRuleCriterionWebBean)criterionRuleTransList.get(key);
	   			    yy++;
	   			 	if (notifyRuleCriterion != null) {
	   		      		//load using OID
	 					out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + notifyRuleCriterion.getAttribute("instrument_type_or_category") + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + notifyRuleCriterion.getAttribute("transaction_type_or_action") + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='" + notifyRuleCriterion.getAttribute("criterion_oid") + "'>");
	   		      		out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_email_setting") + "'>");
	   		      	} else {//if it is a new form
		   		    	out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + instrumentCatagory[31] + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + transArray[tLoop] + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='0'>");		   		     	
		   		      	out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value=''>");
		   		    }
				}//ent tLoop
				
   			%>
			</td>
		</tr>
	</table> 
	<table width="100%">
			<% if(dataList.contains(InstrumentType.MESSAGES)) {%>
						<span class="availNotifyItemSelect">
							<%=resMgr.getText("NotificationRuleDetail.DataExists",TradePortalConstants.TEXT_BUNDLE)%>
						</span>					
			<% } %>
	</table>
</div>