
<%--for the ajax include--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*, java.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
	//parameters -
boolean isReadOnly = false;
String parmIndexINT = "";
String panelRangeMinAmount = null;
String panelRangeMaxAmount = null;
String userLocale = null;
String userSecurityRights = null;
String userOrgOid = null;
String clientBankOid = null;
String userType = null;
String corporateOrgOid = null;
String sValue = null;
String baseCurrency = "";
CorporateOrganizationWebBean corporateOrg = null;

//Retrieve the user's locale, security rights, and organization oid
userSecurityRights = userSession.getSecurityRights();
userLocale         = userSession.getUserLocale();
userOrgOid         = userSession.getOwnerOrgOid();
clientBankOid      = userSession.getClientBankOid();		
// Retrieve the type of user so that we can get the appropriate read only rights; we can only be on this 
// page if we're a client bank user or a BOG user
userType = userSession.getOwnershipLevel();

beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.PanelAuthorizationRangeWebBean", "PanelAuthorizationRangeList");
beanMgr.registerBean("com.ams.tradeportal.busobj.webbean.CorporateOrganizationWebBean", "CorporateOrganization");
corporateOrg = (CorporateOrganizationWebBean) beanMgr.getBean("CorporateOrganization");

corporateOrgOid = userOrgOid;    
corporateOrg.getById(corporateOrgOid );
baseCurrency = corporateOrg.getAttribute("base_currency_code");

isReadOnly = !SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_PANEL_AUTH_GRP);

	parmIndexINT = request.getParameter("indexINT");
	
	int intLoop = 0;
	if (parmIndexINT != null) {
		try {
			intLoop = (new Integer(parmIndexINT)).intValue();
		} catch (Exception ex) {
		}
	}
	//other necessaries
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	PanelAuthorizationRangeWebBean panelAuthorizationRangeINTList[] = new PanelAuthorizationRangeWebBean[6];
	//when included per ajax, the business objects will be blank
	panelAuthorizationRangeINTList[intLoop] = beanMgr.createBean(PanelAuthorizationRangeWebBean.class, "PanelAuthorizationRange");	

%>
<%@ include file="PanelAuthorization-AmountRangeTableINT.frag"%>