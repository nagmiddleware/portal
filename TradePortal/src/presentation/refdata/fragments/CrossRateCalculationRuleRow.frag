<%--this fragment is included in both the 
    Transaction-IMP_DLC-ISS-TransportDocsShipment fragment for existing
    rows as well as in the Transaction-IMP_DLC-ISS-ShipmentTermsRows.jsp
    for adding new rows via ajax.

  Passed variables:
    shipmentTermsIndex - an integer value, zero based
    isReadOnly
    isFromExpress
--%>

<%--TODO: this should iterate through the # of existing shipment terms, or create the correct number of new rows depending on the situation--%>

  <tr>
	     	<td>
				<%=(crossRateCalcIndex+1)%>
			</td>
			<td>
				<%
				frmCcyOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, crossRateRule.getAttribute("from_ccy"), loginLocale);
				%>
				<%=widgetFactory.createSelectField("from_ccy"+(crossRateCalcIndex),
							"", frmCcyDefaultText, frmCcyOptions, isReadOnly,
							false, false, "style=\"width: 75px;\"", "", "none")%>
			</td>
			<td>
				<%
				toCcyOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, crossRateRule.getAttribute("to_ccy"), loginLocale);
				%>
				<%=widgetFactory.createSelectField("to_ccy"+(crossRateCalcIndex),
							"", toCcyDefaultText, toCcyOptions, isReadOnly,
							false, false, "style=\"width: 75px;\"", "", "none")%>
			</td>
			 <td align="left">              
                <% multiDivValue = crossRateRule.getAttribute("calc_method"); %>
				<%
				out.print(widgetFactory.createRadioButtonField("calc_method"+(crossRateCalcIndex),
						"TradePortalConstants.DIVIDE"+(crossRateCalcIndex),"CrossRateCalculationRuleDetail.Divide",
						TradePortalConstants.DIVIDE,multiDivValue.equals(TradePortalConstants.DIVIDE), isReadOnly));
                %>
				<%
				out.print(widgetFactory.createRadioButtonField("calc_method"+(crossRateCalcIndex),
						"TradePortalConstants.MULTIPLY"+(crossRateCalcIndex),"CrossRateCalculationRuleDetail.Multiply",
						TradePortalConstants.MULTIPLY,multiDivValue.equals(TradePortalConstants.MULTIPLY), isReadOnly));
                %>            
            </td>
			<td>
				<%=widgetFactory.createNumberField("lower_variance"+(crossRateCalcIndex), "",
						crossRateRule.getAttribute("lower_variance"), "14", isReadOnly, false, false,
						 "style=\"width: 100px;\"","", "none")%>
			</td>				
			<td>
				<%=widgetFactory.createNumberField("upper_variance"+(crossRateCalcIndex), "",
						crossRateRule.getAttribute("upper_variance"), "14", isReadOnly, false, false,
						 "style=\"width: 100px;\"","", "none")%>
			</td>	
			<td>
				<%	if (!isReadOnly){ %>  										      						
	              	<div id="clear"><a href="javascript:clearText(<%=crossRateCalcIndex%>, '<%=frmCcyDefaultText%>', '<%=toCcyDefaultText%>');" >Clear</a></div>
	              	<%=widgetFactory.createHoverHelp("clear","RefData.clear")%>	
	              <% } %>
			</td>	
			<% 
			String criterionOid = "";
			if (InstrumentServices.isNotBlank(crossRateRule.getAttribute("criterion_oid")))
				criterionOid = EncryptDecrypt.encryptStringUsingTripleDes(crossRateRule.getAttribute("criterion_oid"), userSession.getSecretKey());
			out.print("<INPUT TYPE=HIDDEN NAME='criterion_oid" + crossRateCalcIndex + "' VALUE='" +criterionOid+ "'>"); 
			%>
	     </tr>
	     
<script language="JavaScript">
function clearText(crossRateCalcIndex, frmCcyDefaultText, toCcyDefaultText) {
			document.getElementById("lower_variance"+crossRateCalcIndex).value = "";
			document.getElementById("upper_variance"+crossRateCalcIndex).value = "";
			var to_ccy=dijit.byId("to_ccy"+crossRateCalcIndex);
			to_ccy.setDisplayedValue(toCcyDefaultText);
			var from_ccy=dijit.byId("from_ccy"+crossRateCalcIndex);
			from_ccy.setDisplayedValue(frmCcyDefaultText);
			dijit.byId("TradePortalConstants.MULTIPLY"+crossRateCalcIndex).set('checked',false);
			dijit.byId("TradePortalConstants.DIVIDE"+crossRateCalcIndex).set('checked',false);
		}
</script>