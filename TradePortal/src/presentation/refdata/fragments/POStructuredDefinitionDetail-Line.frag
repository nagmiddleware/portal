<%--
*******************************************************************************
             PO Upload Definition Detail - File Definition (Tab) Page

  Description:   This jsp simply displays the html for the file tab portion of 
the PO Upload Definition.jsp.  This page is called from 
PO UploadDefinition Detail.jsp  and is called by:
		<%@ include file="POUploadDefinitionDetail-File.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to 
avoid being overwritten as each tab is a submit action.  As a result, if this 
data does not make it into the xml document, then it may be overwritten with a 
null value in this tab or another.
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@page import="com.ams.tradeportal.busobj.util.InstrumentServices"%>
<%
Debug.debug("******************** IN FILE DEFINITION TAB JSP *************************");	

final int NUMBER_OF_FILE_DEF_ORDER_FIELDS = 31;


int x=1;

String defaultText = " ";
String options;
String lineDetail=poDefinitionRef.getAttribute("line_item_detail_provided");
%>

<div  class = "columnLeft">
	<%=widgetFactory.createCheckboxField("line_item_detail_provided", "POStructureDefinition.LineItemDetaidNotReq", 
		poDefinitionRef.getAttribute("line_item_detail_provided").
                                   equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", 
                                   "none")%>
</div>
<div class = "columnRight">
</div>
<div style="clear:both;"></div>

<div class = "columnLeft">
<%=widgetFactory.createSubLabel("POStructureDefinition.Step1Text")%> 			
</div>
<div class = "columnRight">
<%=widgetFactory.createLabel("","POStructureDefinition.Step2Text", false, false, false, "none")%> 			
</div>
<div style="clear:both;"></div>
<div class = "columnLeft">

<%-- PO  Line Item Detail started--%>


<table class="formDocumentsTable" id="poSysDefinitionLineTableId" style="width: 96%">
<thead>
	<tr style="height:35px">
		<th colspan=3 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.SystemDefinedFields") %>
		</th>
	</tr>
</thead>
<tbody>
		<tr>
			<td nowrap width="4">&nbsp</td>
		<td nowrap width="190">
			<%=widgetFactory.createLabel("","POStructureDefinition.FieldName", false, false, false, "none")%>
		</td> 
		<td nowrap width="20" >
			<%=widgetFactory.createLabel("","POStructureDefinition.Size", false, false, false, "none")%>
		</td> 
		</tr>
    	<tr>
 		 <td nowrap width="4" >
			<input type="hidden" name="line_item_num_req" value="<%=TradePortalConstants.INDICATOR_YES%>">
			&nbsp;
      	 </td> 
     	 <td nowrap width="190">
     	 	<span class="asterisk">*</span>
			<%= widgetFactory.createLabel("", "POStructureDefinition.LineItemNumber", false, true, false, "none") %>  
         </td> 
         <td width="20" align="center">
               <%= widgetFactory.createLabel("", "70", false, false, false, "none") %>  
         </td>
        </tr>
        
    	<tr>
 		<td nowrap width="4">
			<input type="hidden" name="unit_price_req" value="<%=TradePortalConstants.INDICATOR_YES%>">
			&nbsp;
      	</td> 
     	<td  nowrap width="190">
     		<span class="asterisk">*</span>
			<%= widgetFactory.createLabel("", "POStructureDefinition.LineItemUnitPrice", false, true, false, "none") %>  
        	</td> 
        <td width="20" align="center">
      			<%= widgetFactory.createLabel("", "11,5", false, false, false, "none") %>  
        	</td>
        </tr>        
        
        <tr>
		<td nowrap width="4" >
		    <input type=hidden value="<%=resMgr.getText("POStructureDefinition.LineItemUnitMeasure", TradePortalConstants.TEXT_BUNDLE)%>" name="unit_of_measure" id="unit_of_measure">
			<%=widgetFactory.createCheckboxField("unit_of_measure_req", "",
	                            poDefinitionRef.getAttribute("unit_of_measure_req").
				             equals(TradePortalConstants.INDICATOR_YES),
	                            isReadOnly, false,  "", "", "none")%>
	 	</td> 
	     	<td nowrap width="190">
			<%= widgetFactory.createLabel("", "POStructureDefinition.LineItemUnitMeasure", false, false, false, "none") %>  
	        </td> 
	        <td width="20" align="center">
	      		<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>  
	        </td>
        </tr>
          
        <tr>
	 	<td nowrap width="4">
			<input type="hidden" name="quantity_req" value="<%=TradePortalConstants.INDICATOR_YES%>">
			&nbsp;
	      	</td> 
	     	<td nowrap width="190">
	     	<span class="asterisk">*</span>
			<%= widgetFactory.createLabel("", "POStructureDefinition.LineItemDetailQuantity", false, true, false, "none") %>  
	        </td> 
	        <td width="20" align="center">
	      		<%= widgetFactory.createLabel("", "11,5", false, false, false, "none") %>  
	        </td>
	</tr>
	<%-- /*KMehta - 18 Aug 2014 - Rel9.1 IR-T36000013783 - Change  - Begin*/ --%>        
       <%--  <tr>
	 	<td nowrap width="4" >
	 	          <input type=hidden value="<%=resMgr.getText("POStructureDefinition.LineItemDetailQntPlus", TradePortalConstants.TEXT_BUNDLE)%>" name="quantity_var_plus" id="quantity_var_plus">
	              <%=widgetFactory.createCheckboxField("quantity_var_plus_req", "",
	                            poDefinitionRef.getAttribute("quantity_var_plus_req").
				             equals(TradePortalConstants.INDICATOR_YES),
	                            isReadOnly, false,  "", "", "none")%>
	      	</td> 
	     	<td nowrap width="190">
			<%= widgetFactory.createLabel("", "POStructureDefinition.LineItemDetailQntPlus", false, false, false, "none") %>  
	        </td> 
	        <td width="20" align="center">
	      		<%= widgetFactory.createLabel("", "6,3", false, false, false, "none") %>  
	        </td>
        </tr>  --%> 
	<%-- /*KMehta - 18 Aug 2014 - Rel9.1 IR-T36000013783 - Change  - End*/ --%>      
</tbody>
</table>
<table>
     <tr>
     <td>&nbsp;</td>
     </tr>
</table>

<table class="formDocumentsTable" id="poUserDefinitionLineTableId" style="width: 96%">
<thead>
	<tr style="height:35px">
		<th colspan=3 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.ProductUserDefinedFields") %>
		</th>
	</tr>
</thead>
<tbody>
      <tr>
      <td></td>
      <td width="190">
		<%= widgetFactory.createLabel("", "POStructureDefinition.Label", false, false, false, "none") %>
 	   
       </td>
       <td width="20">
		<%= widgetFactory.createLabel("", "POStructureDefinition.Size", false, false, false, "none") %>
       </td>
       </tr>
        <tr>
 	      <td nowrap width="30" >
              
              <%=widgetFactory.createCheckboxField("prod_chars_ud1_req", "",
            		  poDefinitionRef.getAttribute("prod_chars_ud1_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
              </td>
 		<td width="190">
 		<%--IR T36000018158 - make prod_chars_ud1_label default to Description if its new definition--%>
 		<%-- NAR IR-T36000028622 add hidden field only when it is new PO def --%>
 		<% if(isNew){ %>
 		<input type="hidden" id="prod_chars_ud1_label" name="prod_chars_ud1_label" value="<%=prodCharUD1Label%>">
 		<%}%>
		<%=widgetFactory.createTextField("prod_chars_ud1_label","",
				prodCharUD1Label ,"35",
				isReadOnly || isNew,true,false,"class = 'char25'", "", "none") %>
 		</td>
 		<td width="20" align="center">
 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
		 </td>
 	    </tr>

 	    <tr>
 	      <td nowrap width="30">
             
              <%=widgetFactory.createCheckboxField("prod_chars_ud2_req", "",
            		  poDefinitionRef.getAttribute("prod_chars_ud2_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
              </td>
 		<td width="190">
		<%=widgetFactory.createTextField("prod_chars_ud2_label","",
				poDefinitionRef.getAttribute("prod_chars_ud2_label") ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		</td>
 		<td width="20" align="center">
			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		 </td>
 	    </tr>

 	    <tr>
 	      <td nowrap width="30" >
             
              <%=widgetFactory.createCheckboxField("prod_chars_ud3_req", "",
            		  poDefinitionRef.getAttribute("prod_chars_ud3_req").
                                    equals(TradePortalConstants.INDICATOR_YES),
                            isReadOnly, false,  "", "", "none")%>
              </td>
 		<td width="190">
		<%=widgetFactory.createTextField("prod_chars_ud3_label","",
				poDefinitionRef.getAttribute("prod_chars_ud3_label") ,"35",
			isReadOnly,true,false,"class = 'char25'", "", "none") %>
 		</td>
 		<td width="20" align="center">
			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		 </td>
 	    </tr>

<%
int totalLineUDDefinedField = 0;
for(int i=4; i<=7; i++){
	currentDefinedLab = "prod_chars_ud" + (new Integer(i)).toString() + "_label";
	currentDefined = poDefinitionRef.getAttribute(currentDefinedLab);
	
	currentDefinedLabReq = "prod_chars_ud" + (new Integer(i)).toString() + "_req";
	currentDefinedReq = poDefinitionRef.getAttribute(currentDefinedLabReq);
	
	if(InstrumentServices.isNotBlank(currentDefined)){
		totalLineUDDefinedField++;
	}else if("Y".equals(currentDefinedReq)){
		totalLineUDDefinedField++;
	}
}



int counterLineTmp = 4;
while (counterLineTmp <= NUMBER_OF_ITEM_DETAILS_PROD_FIELD){

	 currentDefinedLab = "prod_chars_ud" + (new Integer(counterLineTmp)).toString() + "_label";
	 currentDefined = poDefinitionRef.getAttribute(currentDefinedLab);
	 if (InstrumentServices.isBlank(currentDefined)) {
			if(totalLineUDDefinedField == 0){
				break;
			}
		}else{
			totalLineUDDefinedField = totalLineUDDefinedField -1;
		}
	 currentDefinedLabReq = "prod_chars_ud" + (new Integer(counterLineTmp)).toString() + "_req";
	 currentDefinedReq = poDefinitionRef.getAttribute(currentDefinedLabReq);

%>
    <tr>
      <td width="30"><%=widgetFactory.createCheckboxField("prod_chars_ud" + counterLineTmp + "_req", "",
	                            poDefinitionRef.getAttribute("prod_chars_ud" + counterLineTmp + "_req").
				             equals(TradePortalConstants.INDICATOR_YES),
	                            isReadOnly, false,  "", "", "none")%>
      </td>
      <td width="190">                             
		<%=widgetFactory.createTextField("prod_chars_ud" + counterLineTmp + "_label","",
			poDefinitionRef.getAttribute("prod_chars_ud" + counterLineTmp + "_label") ,"35", isReadOnly,true,false,"class = 'char25'", "", "none") %>
			
      </td>   
 		<td width="20" align="center">
			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		 </td>
    </tr>
<%
counterLineTmp++;
}
%>
</tbody>
</table>
<br>
<%
	if (!(isReadOnly)) {
%>
	<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreLineDefined" name="add4MoreLineDefined">
	 <%=resMgr.getText("common.Add4MoreText",TradePortalConstants.TEXT_BUNDLE)%>
	 <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	   add4MoreDefined('poUserDefinitionLineTableId');
	</script>
	</button>  
	<%=widgetFactory.createHoverHelp("add4MoreLineDefined","POStructure.Add4More") %>  
<%
	} else {
%>
					&nbsp;
<%
	}
%>
</div>

<div class = "columnRight">

<table id="poStructLineItemOrderTableId" class="formDocumentsTable">
	<thead>
	  <tr style="height:35px">
		<th colspan=2 style="text-align:left">
			<%= widgetFactory.createSubLabel("POStructureDefinition.LineItemDetailsDataOrder") %>
		</th>
	  </tr>
	</thead>
	<tbody>
      <tr>
 		 <td width="50" >
		  	<%=widgetFactory.createLabel("","POStructureDefinition.Order", false, false, false, "none")%>
 		  </td>
 		 <td >
		  	<%=widgetFactory.createLabel("","POStructureDefinition.FieldName", false, false, false, "none")%>
 		 </td> 
       </tr>
<%

String [] initialLineItemVal = {"line_item_num", "unit_price", "quantity"}; 
boolean initialLineItemFlag = false;
int totalLineItem = 3;
if (InstrumentServices.isBlank(poDefinitionRef.getAttribute("po_line_item_field1"))){
	initialLineItemFlag = true;
}else{
	totalLineItem = 12;
}

poSumValue = null;
poSumField = null;
poSumReq = TradePortalConstants.INDICATOR_YES;
reStartIndex = 1;


while ((initialLineItemFlag ||InstrumentServices.isNotBlank
	(poDefinitionRef.getAttribute("po_line_item_field" + (new Integer(reStartIndex )).toString())))
      &&(reStartIndex  <= totalLineItem))
{

if(initialLineItemFlag){
	poSumField = initialLineItemVal[reStartIndex-1];
}else{
	poSumField = poDefinitionRef.getAttribute("po_line_item_field" + (new Integer(reStartIndex )).toString());
}
poSumValue = poDefinitionRef.getAttribute(poSumField);
if ((InstrumentServices.isBlank(poSumValue))&&(InstrumentServices.isNotBlank(poSumField))){
	poSumValue = "PurchaseOrderUploadRequest." + poSumField;
    indexLabel = poSumField;
}else{
	indexLabel = poSumField.substring(0,poSumField.indexOf("_label"));
}
/*KMehta - 18 Aug 2014 - Rel9.1 IR-T36000013783 - Change  - Begin */ 
/* 
if ("quantity_variance_plus".equals(indexLabel)){
	indexLabel = "quantity_var_plus";
}
 */
 /*KMehta - 18 Aug 2014 - Rel9.1 IR-T36000013783 - Change  - End */ 
poSumReq = poDefinitionRef.getAttribute(indexLabel + "_req");

if ((poLineItemOptinalVal.contains(indexLabel)||(TradePortalConstants.INDICATOR_YES.equals(poSumReq)))&& InstrumentServices.isNotBlank(poSumValue))
{
	currentValue = poSumField;
	if("N".equals(lineDetail) || InstrumentServices.isBlank(lineDetail)){
%>
 	  <tr id= "<%=StringFunction.xssCharsToHtml(indexLabel)%>_req_order">
 		 <td align="center">
 		    <input type=hidden id="upload_userDefLine_order_<%=reStartIndex%>" name="po_line_item_field<%=reStartIndex%>" 
 		       value="<%=StringFunction.xssCharsToHtml(currentValue)%>">

   			 <input type="text" name="" value='<%=counterTmp %>' data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true" 
   				id='userDefLineText<%=reStartIndex %>'  style="width: 25px;">
 		  </td> 
 		  <td id='userDefLineFileFieldName<%=reStartIndex%>' align="left">
		  	<%=widgetFactory.createLabel("", poSumValue, false, false, false, "none")%>
		  	<% if(!isReadOnly){ 
		  	 if(poLineItemOptinalVal.contains(poSumField)){ %>				
			  <span class="" id='<%=StringFunction.xssCharsToHtml(indexLabel)%>'></span>
           <%} else{%>
                <span class="deleteSelectedPOStructureDefinitipnItem" id='<%=StringFunction.xssCharsToHtml(indexLabel)%>'></span>
           <%}}%>
           
 		  </td>
 	  </tr>
<%
	counterTmp++;
  }
}	
	reStartIndex++;
}
%>
</tbody>
</table>
<br>
<div>
<%
	if (!(isReadOnly)) {
%> 
<button data-dojo-type="dijit.form.Button" type="button" id="updateUserLineOrder">
  <%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		        updateSumLineItemOrder(document.getElementById("poStructLineItemOrderTableId").rows.length , (document.getElementById("poStructSumDataOrderTableId").rows.length) +
                              (document.getElementById("poStructDataFileOrderTableId").rows.length),  'sumDataPos'); 
	</script>
</button>
<%
	} else {
%>
					&nbsp;
<%
	}
%>

</div>
<br/><br/>	

</div>
