<%--
*******************************************************************************
       PO Upload Definition Detail - Goods Description Layout (Tab) Page

    Description:   This jsp simply displays the html for the goods description
portion of the PO Upload Definition.jsp.  This page is called from
PO UploadDefinition Detail.jsp  and is called by:
		<%@ include file="POUploadDefinitionDetail-General.jsp" %>
Note: the values stored in secureParms in this page are done deliberately to avoid
being overwritten as each tab is a submit action.  As a result, if this data does
not make it into the xml document, then it may be overwritten with a null value
in this tab or another.
*******************************************************************************
--%>
<%-- 
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<style>
.asterisk {
        font-size: 16px;
        }</style>
<script type="text/javascript">

function add4MoreRows() {
	<%-- so that user can only add 2 rows --%>
	var tbl = document.getElementById('poDataDefinitionTableId');<%-- to identify the table in which the row will get insert --%>
	var lastElement = tbl.rows.length;
	var NewRow="";
	var j = 0;
	
	<%-- if(lastElement > 10){
		lastElement = lastElement - 9;
	} --%>
		 
	for(i=lastElement;i<lastElement+4;i++){
		
		if(i==34){
			return;
		}
		
   	 	var newRow = tbl.insertRow(i);<%-- creation of new row --%>
   	 	var cell0 = newRow.insertCell(0);<%-- first  cell in the row --%>
		var cell1 = newRow.insertCell(1);<%-- second  cell in the row --%>
		var cell2 = newRow.insertCell(2);<%-- third  cell in the row --%>
		var cell3 = newRow.insertCell(3);<%-- fourth  cell in the row --%>
		var cell4 = newRow.insertCell(4);<%-- fifth  cell in the row --%>
		
		newRow.id = "poUploadTableIndex"+i;
		j = i-9;
		<%        // Display the datatype but exclude Large Text from drop down since it is only used for PO Text.
        Vector exclusion1 = new Vector();
        exclusion1.add(TradePortalConstants.PO_FIELD_DATA_TYPE_LARGETEXT);
        String po_field_data_type_options1 = Dropdown.createSortedRefDataOptions( TradePortalConstants.PO_FIELD_DATA_TYPE,
								" ", resMgr.getResourceLocale(),
                              exclusion1 );
        po_field_data_type_options1 = "<option value=\"\"> </option>"+po_field_data_type_options1;
        
        
%>
		cell0.innerHTML ='<%=resMgr.getText("POUploadDefinitionDetail.DataItem",TradePortalConstants.TEXT_BUNDLE)%> '+j;
        
		cell1.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"other'+j+'_field_name\" id=\"other'+j+'_field_name\" class=\'char15\' onChange=\'createDynamicRow(this);\'  maxLength=\"15\" >';
        cell1.setAttribute("align","right");
        
        cell2.innerHTML ='<input data-dojo-type=\"dijit.form.TextBox\" name=\"other'+j+'_size\" id=\"other'+j+'_size\" class=\'char5\' onChange=\'createSize(this);\' maxLength=\"2\">';
        cell2.setAttribute("align","right");
        
        cell3.innerHTML ='<select data-dojo-type=\"dijit.form.FilteringSelect\" name=\"other'+j+'_datatype\" id=\"other'+j+'_datatype\" class=\'char7\'>'+'<%=po_field_data_type_options1%>'+'</select>';
        cell3.setAttribute("align","right");
        
        cell4.innerHTML ='35';
        cell4.setAttribute("align","right");
        
        require(["dojo/parser"], function(parser) {
         	parser.parse(newRow.id);
    	 });
      
	}
	<%-- document.getElementById("noOfRows1").value = eval(lastElement+3);    --%>
}

<%-- function to reNumber the rows during delete and Add. --%>
function reNumber(tableName){
	var tbl = document.getElementById(tableName);<%-- to identify the table in which the row will get insert --%>
	var poTableIndex = tbl.rows.length;
	var rows = tbl.rows;
	var startRow=0;
	var txtVar = "";
	if("poGoodsOrderTable"==tableName)
	{
		rowNum=1;
		startRow=2;
		txtVar= "gtext";
	}	
	else{
		rowNum=1;
		startRow=1;
		txtVar= "text";
	}
		for ( var x = startRow; x <poTableIndex; x++ ) 
		{
				
				var td = rows[x].getElementsByTagName('td');
				
				for ( var y = 0; y < td.length; y++ ) 
				{
					var input = td[y].getElementsByTagName( 'input' );
					for ( var z = 0; z < input.length; z++ ) 
					{
						
						if(input[z].id.indexOf(txtVar)!=-1)
						{
							var nam = input[z].id;
							document.getElementById(nam).value=rowNum;
							rowNum=rowNum+1;
							
						}								
						 
					}
				}
		}	
	
	
}<%-- end reNumber function. --%>

function createSize(thisObj){
	
	var temp = "";
	var tempSize = "";
	
	if(null != thisObj.name){
		if(thisObj.name=="other1_size"){
			document.getElementById("other1_name").value=thisObj.value;
			
		}
		temp = thisObj.name.replace('_size','');		
	}
	
	if( document.getElementById("goods_"+temp) ){
		
		tempSize = document.getElementById("goods_"+temp).value;
		tempSize = tempSize.replace('goodsFieldName','');
		document.getElementById('goodsSize'+tempSize).innerText = thisObj.value;
		
	}
}


function createDynamicRow(thisObj)
{
	
	var poDatDefTab = document.getElementById('poDataDefinitionTableId');
	var poDataDefRowLen = poDatDefTab.rows.length;
	var poDatDefRows = poDatDefTab.rows;
	
	
	var arr= new Array();
	var arrVal=new Array();
	var len =0;
	for ( var x = 0; x < poDatDefRows.length; x++ )
	{
	
        var td = poDatDefRows[x].getElementsByTagName( 'td' );
        for ( var y = 0; y < td.length; y++ )
		{
            var input = td[y].getElementsByTagName( 'input' );
            for ( var z = 0; z < input.length; z++ ) 
			{
            	var inp= input[z];
            	var val="";
            	if(inp!=undefined)
            		val=inp.value;
            	
            	if(input[z].id.indexOf("_field_name")!=-1 && val.length>0)
				{
            		
            		arr[len]=input[z].id.replace('_field_name','');
            		
            		arrVal[len]=input[z].value;
            		len++;
            	}
                
            }
        }
    }
	
	
	
	var tab = document.getElementById('poFileOrderTable');<%-- to identify the table in which the row will get insert --%>
	var rowCount = tab.rows.length;
	
	var poFieldDefRows = tab.rows;
	
	
	var arrNoFound = new Array();
	var arrNoFoundVal= new Array();
	var l =0;
	for(var k=0;k<arr.length;k++)
	{
		var arrValu=arr[k];
		for(var i=1;i<=rowCount;i++)
		{
			var found = false;
		
		var str = 'upload_order_'+i;
		var temp = document.getElementById(str);
		if(temp==undefined)
			temp="";
		else 
			temp=temp.value;
		
		
		if(temp==arrValu){
			found=true;	
			break;
			}	
		}
		
		if(found==false){
			arrNoFound[l]=arr[k];
			arrNoFoundVal[l]=arrVal[k];
			l++;
		}
	}<%-- end of for --%>
	
	for(var i=1;i<rowCount;i++)
	{
		
		if(document.getElementById('poFileOrderTable').rows[i].style.display==''){
			var tempDoc= document.getElementById("text"+i);
			if(tempDoc!=undefined)
			{
				var str = 'upload_order_'+i;
				var tempDoc1 = document.getElementById(str);
				
				if (tempDoc1!=undefined)
				{

					var found=false;
					var chkVal= tempDoc1.value;
					for(var k=0;k<arr.length;k++){
						var arrValChk = arr[k];
	
						if(chkVal ==arrValChk){
							document.getElementById("fileFieldName"+i).innerHTML=arrVal[k];
							found = true;
							break;
						}<%-- end of tempDc if --%>
						
					}<%-- end end of for k --%>
					if(found==false){
						document.getElementById("fileFieldName"+i).innerHTML="";
						document.getElementById(str).value="";
						document.getElementById("text"+i).value="";
						document.getElementById('poFileOrderTable').rows[i].style.display = 'none';
					}
				}<%-- end of tempDoc1 if --%>
			}<%-- end of tempDocif --%>
		}<%-- endof if checking style --%>
	}<%-- end of for i --%>
	rowCount = tab.rows.length;
	
	for(var i=0;i<arrNoFound.length;i++)
	{
		var num = rowCount;
		var newRow = tab.insertRow(num);
		var cell0 =newRow.insertCell(0);
		var cell1 =newRow.insertCell(1);
		var fileTextBox = new dijit.form.TextBox({
	        name: "",
	        id: "text"+num,
	        value: num        
	 	});
		require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
			ready(function(){
		 dojo.connect(registry.byId('text'+num),'onChange',function(){
			 var temp = this.id;
			 temp = temp.replace("text","");
			 setChg(temp);
			  
	    });})});
		dojo.addClass(fileTextBox.domNode, "selectedItemOrderTextBox");
		
		cell0.appendChild(fileTextBox.domNode);
	    cell0.setAttribute("align","center");
		
		cell1.innerText=arrNoFoundVal[i];
	    cell1.setAttribute("id","fileFieldName"+num);
	    cell1.setAttribute("align","left"); 
	    newRow.id = "poTableIndex"+i;
	    require(["dojo/parser"], function(parser) {
         	parser.parse(newRow.id);
    	 });
	    var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "upload_order_"+num);
		input.setAttribute("id","upload_order_"+num);
		input.setAttribute("value", arrNoFound[i]);
		 var input2 = document.createElement("input");
	    input2.setAttribute("type", "hidden");
		input2.setAttribute("name", "chg_order_"+num);
		input2.setAttribute("id","chg_order_"+num);
		input2.setAttribute("value", "2");
		newRow.appendChild(input);
		newRow.appendChild(input2);
		rowCount+1;
	}
	reNumber('poFileOrderTable');
	<%-- Goods Description --%>
	
	
	var tabGD = document.getElementById('poGoodsOrderTable');<%-- to identify the table in which the row will get insert --%>
	var rowCount = tabGD.rows.length;
	
	var poGDRows = tabGD.rows;
	var arrRemove  = new Array();
	var delRow = 0;
				var inr=0;
				var rows = tabGD.rows;
				
				var arrTempp= new Array();
				for ( var x = 2; x <rows.length; x++ ) 
				{
						
						var td = rows[x].getElementsByTagName('td');
						
						for ( var y = 0; y < td.length; y++ ) 
						{
							var input = td[y].getElementsByTagName( 'input' );
							for ( var z = 0; z < input.length; z++ ) 
							{
								
								if(input[z].id.indexOf("gtext")!=-1)
								{
									var nam = input[z].id;
									
									nam = nam.replace("gtext","");
									
									arrTempp[inr]=nam;
									inr=inr+1;
								}								
								 
							}
						}
				}
	
	
	for(var ind=0;ind<=arrTempp.length;ind++)
	{
		
		var i =parseInt(arrTempp[ind]);
		if (tabGD.rows[ind].style.display=='' ){
			var tempDoc= document.getElementById("gtext"+i);
			if(tempDoc!=undefined)
			{
				var str = 'goods_descr_orderKey_'+i;
				var tempDoc1 = document.getElementById(str);
				
				if (tempDoc1!=undefined)
				{
					var found=false;
					var chkVal= tempDoc1.value;
					for(var k=0;k<arr.length;k++){
						var arrValChk = arr[k];
						
						
						if(chkVal ==arrValChk){
							document.getElementById("goodsFieldName"+i).innerHTML=arrVal[k];
							found = true;
							break;
						}<%-- end of tempDc if --%>
						
					}<%-- end end of for k --%>
					if(found==false){
						
						arrRemove[delRow]=i;
						delRow =delRow+1 ;
						arrRemove[delRow]=(ind+2);
						break;
						
					}
				}<%-- end of tempDoc1 if --%>
			}<%-- end of tempDocif --%>
		}
	}<%-- end of for i --%>
	
	rowCount = tabGD.rows.length;
	 arrTempp = new Array();
	
	
	for (var ind=2;ind<=rowCount;ind++){

		var it = ind-1;
		if(it==arrRemove[1]){
			var i=arrRemove[0];
			
			if(dijit.byId("goods_descr_orderKey_"+i))
				dijit.byId("goods_descr_orderKey_"+i).destroy(true);
			if(document.getElementById("goods_descr_orderKey_"+i))
				document.getElementById("goods_descr_orderKey_"+i).removeAttribute("id");
			
			if(dijit.byId("goods_descr_order_"+i))
				dijit.byId("goods_descr_order_"+i).destroy(true);
			if(document.getElementById("goods_descr_order_"+i))
				document.getElementById("goods_descr_order_"+i).removeAttribute("id");
			
			if(dijit.byId("goodsFieldName"+i))
				dijit.byId("goodsFieldName"+i).destroy(true);
			if(document.getElementById("goodsFieldName"+i))
				document.getElementById("goodsFieldName"+i).removeAttribute("id");
			
			if(dijit.byId("gtext"+i))
				dijit.byId("gtext"+i).destroy(true);
			if(document.getElementById("gtext"+i))
				document.getElementById("gtext"+i).removeAttribute("id");
			

			if(dijit.byId("goodsSize"+i))
				dijit.byId("goodsSize"+i).destroy(true);
			if(document.getElementById("goodsSize"+i))
				document.getElementById("goodsSize"+i).removeAttribute("id");
			
			if(dijit.byId("goodsCheckbox"+i))
				dijit.byId("goodsCheckbox"+i).destroy(true);
			if(document.getElementById("goodsCheckbox"+i))
				document.getElementById("goodsCheckbox"+i).removeAttribute("id");
			tabGD.deleteRow(it);
			
			break;
		}
	}
	
				
				var inr=0;
				var rows = tabGD.rows;
				
				for ( var x = 2; x <rows.length; x++ ) 
				{
						
						var td = rows[x].getElementsByTagName('td');
						
						for ( var y = 0; y < td.length; y++ ) 
						{
							var input = td[y].getElementsByTagName( 'input' );
							for ( var z = 0; z < input.length; z++ ) 
							{
								
								if(input[z].id.indexOf("goodsCheckbox")!=-1)
								{
									var nam = input[z].id;
									nam = nam.replace("goodsCheckbox","");
									arrTempp[inr]=nam;
									inr=inr+1;
								}								
								 
							}
						}
				}

 
	arrTempp.sort(function(a,b){return a-b});
	
	var numm=0;
	if(arrTempp.length>0)
		numm=arrTempp[arrTempp.length-1];
	
	numm = parseInt(numm)+1;
		rowCount = tabGD.rows.length;
	
	for(var i=0;i<arrNoFound.length;i++)
	{
		if("po_text"==arrNoFound[i]){
			continue;
		}
		var temp_num = rowCount-1;
		
		var row = tabGD.insertRow(temp_num+1);
		var num=numm;
		cell0 = row.insertCell(0);<%-- first  cell in the row --%>
		cell1 = row.insertCell(1);<%-- second  cell in the row	 --%>
		var cell2 = row.insertCell(2);<%-- third  cell in the row --%>
		var cell3 = row.insertCell(3);<%-- fourth  cell in the row --%>
		
		row.id = rowCount;	
		rowCount = rowCount-1;<%-- As goods description table already contains a header and an blank row, reduce the count by 1 --%>
		
		var gCheckBox = new dijit.form.CheckBox({
	        id: "goodsCheckbox"+num,
	        checked:false
	    });
		
		
		var goodsTextBox = new dijit.form.TextBox({
	        name: "",
	        id: "gtext"+num,
	        value: temp_num,
	        trim:true
	 	});
		
		
		require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
			ready(function(){
		 dojo.connect(registry.byId('gtext'+num),'onChange',function(){
			 var temp = this.id;
			 temp = temp.replace("gtext","");
			 setChgGoods(temp);
			  
	    });})});
		dojo.addClass(goodsTextBox.domNode, "selectedItemOrderTextBox");
		
		cell0.appendChild(gCheckBox.domNode);
	    cell0.setAttribute("align","center");
		
		cell1.appendChild(goodsTextBox.domNode);
	    cell1.setAttribute("align","center");
		
		cell2.innerText=arrNoFoundVal[i];
	    cell2.setAttribute("id","goodsFieldName"+num);
	    cell2.setAttribute("align","left"); 
	    
	    if("currency"==arrNoFound[i]){
	    	cell3.innerText="3";
	    }else if("last_ship_dt"==arrNoFound[i]){
	    	cell3.innerText="10";
	    }else{
	    	cell3.innerText=" ";
	    }
	    
	    
	    cell3.setAttribute("id","goodsSize"+num);
	    cell3.setAttribute("align","center"); 
		
	    require(["dojo/parser"], function(parser) {
	     	parser.parse(row.id);     	
		 });
	    
	    var input1 = document.createElement("input");
	    input1.setAttribute("type", "hidden");
		input1.setAttribute("name", "goods_descr_order_"+num);
		input1.setAttribute("id","goods_descr_order_"+num);
		input1.setAttribute("value", "");
		
		var input2 = document.createElement("input");
	    input2.setAttribute("type", "hidden");
		input2.setAttribute("name", "goods_descr_orderKey_"+num);
		input2.setAttribute("id","goods_descr_orderKey_"+num);
		input2.setAttribute("value", arrNoFound[i]);
		
		var input3 = document.createElement("input");
	    input3.setAttribute("type", "hidden");
		input3.setAttribute("name", "goods_"+arrNoFound[i]);
		input3.setAttribute("id","goods_"+arrNoFound[i]);
		input3.setAttribute("value", num);
		
		var input4 = document.createElement("input");
	    input4.setAttribute("type", "hidden");
		input4.setAttribute("name", "gchg_order_"+num);
		input4.setAttribute("id","gchg_order_"+num);
		input4.setAttribute("value","2");
		
		require(["dojo/ready","dijit/registry", "dojo/on"], function(ready,registry, on){
			ready(function(){
		on(gCheckBox, "change", function(isChecked){
			if(isChecked){
				calcFunc();
			}
			
			else{
				calcFunc();
			}
		},true);
			})});
	
		row.appendChild(input1);
		row.appendChild(input2);
		row.appendChild(input3);
		row.appendChild(input4);
		rowCount+1;
	}
	reNumber('poGoodsOrderTable');
	
	
	
}


<%--  Vishal Sarkary REL 9.1 IR-#32010 start --%>
function delimiterCheck(){
	  require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom) { 
		 if(registry.byId("TradePortalConstants.PO_FIXED_LENGTH").checked){
		 registry.byId("delimiter_char").reset();
		 }
	    });
	}	
	
	function delimiterSelectField(){
	
	require(["dijit/registry","dojo/dom","dojo/domReady!"], function(registry,dom) { 
		if(!registry.byId("TradePortalConstants.PO_FIXED_LENGTH").checked){
			  registry.byId("TradePortalConstants.PO_DELIMITED").set('checked', true);
	 		  registry.byId("delimiter_char").value;
		}
		else if(registry.byId("TradePortalConstants.PO_FIXED_LENGTH").checked && registry.byId("delimiter_char").value){
			  registry.byId("TradePortalConstants.PO_DELIMITED").set('checked', true);
	  		  registry.byId("delimiter_char").value;
		}
		
	    });
	}
<%--  Vishal Sarkary REL 9.1 IR-#32010 end	 --%>

</script>
<%
int rowCount = 0;

if(request.getParameter("numberOfMultipleObjects")!=null) {
	rowCount = Integer.parseInt(request.getParameter("numberOfMultipleObjects"));
  }
  else {
	rowCount = 4;
  }

String fileFormatType = poUploadDef.getAttribute("file_format");
boolean poDefTypeManualOnly = poUploadDef.getAttribute("definition_type").equals(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL);
String del_options = "";



String po_field_data_type_options = "";		//List of refdata options for Dropdown(s)

//Because these are check boxes, and the tabs are submit actions - we need to ensure that
//These values make it to the mapfile otherwise the pre-mediator will reset the values to 'N'

//secureParms.put("include_column_header", poUploadDef.getAttribute("include_column_header"));
//secureParms.put("include_footer", poUploadDef.getAttribute("include_footer"));
//cquinton 10/21/2013 Rel 8.3 ir#21748 this is a field on the ui. do not set it explicitly
//secureParms.put("include_po_text", poUploadDef.getAttribute("include_po_text"));
%>

<%-- General Title Panel details start --%>

<%= widgetFactory.createSectionHeader("1", "POUploadDefinitionDetail.General") %>
		
<div class = "columnLeft">

	<%-- Name and Description --%>

	<%= widgetFactory.createSubsectionHeader("POUploadDefinitionDetail.NameAndDescription",false, false, false,"" ) %>
	
	<%=widgetFactory.createTextField("name","POUploadDefinitionDetail.DefinitionName",
			poUploadDef.getAttribute("name") ,"35",isReadOnly,true,false,"class='char35';", "", "") %>
			
	<%=widgetFactory.createTextField("description","POUploadDefinitionDetail.Description",
			poUploadDef.getAttribute("description") ,"65",isReadOnly,true,false,"class='char35';", "", "") %>
			
	<%--  Definition Type (Upload/Manual/Both)	--%>
	<% 
		String orgPOUploadIndicator = userSession.getOrgAutoLCCreateIndicator();
		String orgPOManualIndicator = userSession.getOrgManualPOIndicator();
		//Krishna IR-FDUH110549529 05/11/2007 Begin
		String orgATPPOUploadIndicator = userSession.getOrgAutoATPCreateIndicator();
		String orgATPPOManualIndicator = userSession.getOrgATPManualPOIndicator();
		
		
		boolean isImportLCUpload= orgPOUploadIndicator!=null && orgPOUploadIndicator.equals(TradePortalConstants.INDICATOR_YES);
		
		boolean isATPUpload= orgATPPOUploadIndicator!=null && orgATPPOUploadIndicator.equals(TradePortalConstants.INDICATOR_YES);
		
		boolean isImportLCManual=orgPOManualIndicator!=null && orgPOManualIndicator.equals(TradePortalConstants.INDICATOR_YES);
		
		boolean isATPManual=orgATPPOManualIndicator!=null && orgATPPOManualIndicator.equals(TradePortalConstants.INDICATOR_YES);

	// If the Corporate customer can enter POs both through FileUpload and Manually eigther it is Import LC or ATP,
	// let user select the PO Definition type                
		if((isImportLCUpload || isATPUpload)&& (isImportLCManual || isATPManual))
		//Krishna IR-FDUH110549529 05/11/2007 End   
		{ 
			poDefinitionType = poUploadDef.getAttribute("definition_type");
	%>
	
		<%= widgetFactory.createLabel("", "POUploadDefinitionDetail.ThePODefinitionWillBeUsed", false, false, false, "") %>
		<div class="formItemWithIndent1">
		<%=widgetFactory.createRadioButtonField("definition_type","TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD",
				"POUploadDefinitionDetail.UploadPOFileOnly", TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD,
				poDefinitionType.equals(TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD),isReadOnly, "onClick='showRequiredData();'", "") %>
		<br>		
		<%=widgetFactory.createRadioButtonField("definition_type","TradePortalConstants.PO_DEFINITION_TYPE_MANUAL",
				"POUploadDefinitionDetail.ManualEntry", TradePortalConstants.PO_DEFINITION_TYPE_MANUAL,
				poDefinitionType.equals(TradePortalConstants.PO_DEFINITION_TYPE_MANUAL),isReadOnly, "onClick='showRequiredData();'", "") %>
		<br>		
		<%=widgetFactory.createRadioButtonField("definition_type","TradePortalConstants.PO_DEFINITION_TYPE_BOTH",
				"POUploadDefinitionDetail.BothUploadAndManual", TradePortalConstants.PO_DEFINITION_TYPE_BOTH,
				poDefinitionType.equals(TradePortalConstants.PO_DEFINITION_TYPE_BOTH),isReadOnly, "onClick='showRequiredData();'", "") %>
		</div>		
  
	<%  } // end  if(isImportLCUpload || isATPUpload)&&...  Krishna IR-FDUH110549529  Begin
	//If the corporate customer can only enter POs by uploading eigther it is Import LC or ATP,
	//the PO Definition is for uploading only                          
	else if((isImportLCUpload || isATPUpload) && !(isImportLCManual || isATPManual))
	{
	secureParms.put("definition_type", TradePortalConstants.PO_DEFINITION_TYPE_UPLOAD);
	}
	// If the corporate customer can only enter POs by manually eigther it is Import LC or ATP,
	// the PO Definition is for manual entry only 
	else if(isImportLCManual || isATPManual && !(isImportLCUpload  || isATPUpload))
	{
	secureParms.put("definition_type", TradePortalConstants.PO_DEFINITION_TYPE_MANUAL);
	}  
	// Krishna IR-FDUH110549529  End
	  %>
	<%-- Default Checkbox --%>  
	
	<%=widgetFactory.createCheckboxField("default_flag", "POUploadDefinitionDetail.default", 
		poUploadDef.getAttribute("default_flag").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "")%>
			
	
 			
</div>
<div class = "columnRight">
	
	<%-- Date Format --%>
	
	<%= widgetFactory.createSubsectionHeader("POUploadDefinitionBeanAlias.date_format",false, true, false,"" ) %>
	
	<% String poDateFormat = poUploadDef.getAttribute("date_format"); %>
	
	<%=widgetFactory.createRadioButtonField("date_format","TradePortalConstants.PO_DATE_FORMAT_US",
				"POUploadDefinitionDetail.MonthDayYear", TradePortalConstants.PO_DATE_FORMAT_US,
				poDateFormat.equals(TradePortalConstants.PO_DATE_FORMAT_US),isReadOnly, "style='margin-left: 25px;'", "") %>
	<br>			
	<%=widgetFactory.createRadioButtonField("date_format","TradePortalConstants.PO_DATE_FORMAT_EURO",
			"POUploadDefinitionDetail.DayMonthYear", TradePortalConstants.PO_DATE_FORMAT_EURO,
			poDateFormat.equals(TradePortalConstants.PO_DATE_FORMAT_EURO),isReadOnly, "style='margin-left: 25px;'", "") %>
			
	<%-- PO Data Format --%>
	<div id="podataformatdivid">
	<%= widgetFactory.createSubsectionHeader("POUploadDefinitionDetail.PODataFormat",false, true, false,"" ) %>
	
	<%// if (!poDefTypeManualOnly) { //Display the file_format only if the po definition may be used for file upload
  	%>
  	
  		<%=widgetFactory.createRadioButtonField("file_format","TradePortalConstants.PO_DELIMITED",
			"POUploadDefinitionDetail.DelimitedData", TradePortalConstants.PO_DELIMITED,
			fileFormatType.equals(TradePortalConstants.PO_DELIMITED),isReadOnly, "style='margin-left: 25px;'", "") %>
		<%	
		 del_options = Dropdown.createSortedRefDataOptions( TradePortalConstants.DELIMITER_CHAR, 
							 poUploadDef.getAttribute("delimiter_char"), resMgr.getResourceLocale() );
         %>
         
         <div class="formItemWithIndent1">
          <%= widgetFactory.createSelectField("delimiter_char", "POUploadDefinitionDetail.DelimiterCharacter", 
      			" ", del_options,  isReadOnly, false,false, "onChange='delimiterSelectField();'", "", "none") %>
      	</div>	
      		
     <%-- Vishal Sarkary REL 9.1 IR-#32010  Added delimiter function call	Start --%>	
      	<%=widgetFactory.createRadioButtonField("file_format","TradePortalConstants.PO_FIXED_LENGTH",
			"POUploadDefinitionDetail.FixedLengthData", TradePortalConstants.PO_FIXED_LENGTH,
			fileFormatType.equals(TradePortalConstants.PO_FIXED_LENGTH),isReadOnly, "style='margin-left: 25px;' ; onChange='delimiterCheck();'", "") %>
		
	<%--  Vishal Sarkary REL 9.1 IR-#32010 end --%>	
			
 
  	<%//} %>
	 </div>

	
</div>

</div>

<%-- General Title Panel End --%>




<%-- Data Definition Title Panel details start --%>
<%= widgetFactory.createSectionHeader("2", "POUploadDefinitionDetail.DataDefinition") %>

 
	
	<input type=hidden value="<%=rowCount%>" name="numberOfMultipleObjects"
         id="noOfRows"> 


	<table id="poDataDefinitionTableId" class="formDocumentsTable" style="width:85%" border="1" cellspacing="0" >
	<tbody>
		<tr >
			<th style="width:40%">&nbsp;</th>
			<th style="text-align:left;"><%= resMgr.getText("POUploadDefinitionDetail.FieldName", TradePortalConstants.TEXT_BUNDLE) %></th>
			<th  style="text-align:left;"><%= resMgr.getText("POUploadDefinitionDetail.Size", TradePortalConstants.TEXT_BUNDLE) %></th>
			<th width="15%" style="text-align:left;"><%= resMgr.getText("POUploadDefinitionDetail.DataType", TradePortalConstants.TEXT_BUNDLE) %></th>
			<th style="text-align:left;width:30%;"><%= resMgr.getText("POUploadDefinitionDetail.MaxLength", TradePortalConstants.TEXT_BUNDLE) %></th>
		</tr>
	
		<tr>
			<td>
				<div id='ben_ponum_req' class="requiredAstrickWithOutFormItem">
					<label for="benPONumberLabel"><%=resMgr.getText("POUploadDefinitionDetail.PONumber", TradePortalConstants.TEXT_BUNDLE)%></label>
				</div>
				
			</td>
			
			
			<td>
					<%=widgetFactory.createTextField("po_num_field_name","",
    		  			poUploadDef.getAttribute("po_num_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);' ", "", "none") %>
			</td>
			<td>
					<%=widgetFactory.createTextField("po_num_size","",
    		  			poUploadDef.getAttribute("po_num_size") ,"2",isReadOnly,false,false, "class='char5'; onChange='createSize(this)'; ", "", "none") %>
			</td>
			<td> <%= resMgr.getText("POUploadDefinitionDetail.Text", TradePortalConstants.TEXT_BUNDLE) %></td>
			<td style="text-align:right;"><%= resMgr.getText("14", TradePortalConstants.TEXT_BUNDLE) %></td>
		</tr>
		
		<tr>
			<td><%=resMgr.getText("POUploadDefinitionDetail.ItemNumber",TradePortalConstants.TEXT_BUNDLE)  %></td>
			
			
			<td>
					<%=widgetFactory.createTextField("item_num_field_name","",
    		  			poUploadDef.getAttribute("item_num_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);'", "", "none") %>
			</td>
			<td>
					<%=widgetFactory.createTextField("item_num_size","",
    		  			poUploadDef.getAttribute("item_num_size") ,"2",isReadOnly,false,false, "class='char5'; onChange='createSize(this)';", "", "none") %>
			
			</td>
			<td><%= resMgr.getText("POUploadDefinitionDetail.Text", TradePortalConstants.TEXT_BUNDLE) %> </td>
			<td style="text-align:right;"><%= resMgr.getText("14", TradePortalConstants.TEXT_BUNDLE) %></td>
		</tr>
		
		<tr>
			<td><%=resMgr.getText("POUploadDefinitionDetail.POText",TradePortalConstants.TEXT_BUNDLE)  %></td>
			
			<td>
					<%=widgetFactory.createTextField("po_text_field_name","",
    		  			poUploadDef.getAttribute("po_text_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);'", "", "none") %>
			</td>
			<td>
					<%=widgetFactory.createTextField("po_text_size","",
    		  			poUploadDef.getAttribute("po_text_size") ,"3",isReadOnly,false,false, "class='char5'; onChange='createSize(this)';", "", "none") %>
			</td>
			<td> <%= widgetFactory.createSubLabel("POUploadDefinitionDetail.LargeText") %> </td>
			<td style="text-align:right;"><%= resMgr.getText("250", TradePortalConstants.TEXT_BUNDLE) %></td>
		</tr>
		
		<tr>
			<td><%=resMgr.getText("POUploadDefinitionDetail.LatestShipmentDate",TradePortalConstants.TEXT_BUNDLE)  %></td>
			
			<td>
					<%=widgetFactory.createTextField("last_ship_dt_field_name","",
    		  			poUploadDef.getAttribute("last_ship_dt_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);'", "", "none") %>
			</td>
				<td style="text-align:right;"><%= resMgr.getText("10", TradePortalConstants.TEXT_BUNDLE) %></td>
			<td> <%= widgetFactory.createSubLabel("POUploadDefinitionDetail.Date") %> </td>
			<td style="text-align:right;"><%= resMgr.getText("10", TradePortalConstants.TEXT_BUNDLE) %></td>
		</tr>
	
		
		
		<tr>
			<td colspan="5" class="tableSubHeaderWithOutBold"><%=resMgr.getText("POUploadDefinitionDetail.IfPODefinitionIsForUpload",TradePortalConstants.TEXT_BUNDLE)  %></td>
		</tr>
	
		
		<tr>
			<td>
				
				<div id='ben_name_req' >
					<label for="benNameLabel"><%=resMgr.getText("POUploadDefinitionDetail.BeneficiaryName",TradePortalConstants.TEXT_BUNDLE)  %></label>
				</div>
			</td>
			<td>
					<%=widgetFactory.createTextField("ben_name_field_name","",
    		  			poUploadDef.getAttribute("ben_name_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);'", "", "none") %>
			</td>
			<td>
					<%=widgetFactory.createTextField("ben_name_size","",
    		  			poUploadDef.getAttribute("ben_name_size") ,"2",isReadOnly,false,false, "class='char5' ; onChange='createSize(this)'; ", "", "none") %>
				
			</td>
			<td> <%= widgetFactory.createSubLabel("POUploadDefinitionDetail.Text") %> </td>
			<td style="text-align:right;"><%= resMgr.getText("35", TradePortalConstants.TEXT_BUNDLE) %></td>
		</tr>
		
		<tr>
			<td>
				
				<div id='ben_cur_req' >
					<label for="benCurrLabel"><%=resMgr.getText("POUploadDefinitionDetail.Currency",TradePortalConstants.TEXT_BUNDLE)  %></label>
				</div>
			</td>
			<td>
					<%=widgetFactory.createTextField("currency_field_name","",
    		  			poUploadDef.getAttribute("currency_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);'", "", "none") %>
			</td>
		
				<td style="text-align:right;"><%= resMgr.getText("3", TradePortalConstants.TEXT_BUNDLE) %></td>
						
			<td><%= resMgr.getText("POUploadDefinitionDetail.Text", TradePortalConstants.TEXT_BUNDLE) %> </td>
			<td style="text-align:right;"><%= resMgr.getText("3", TradePortalConstants.TEXT_BUNDLE) %></td>
		</tr>
		
		<tr>
			<td>
				
				<div id='ben_amt_req' >
					<label for="benAmtLabel"><%=resMgr.getText("POUploadDefinitionDetail.POAmount",TradePortalConstants.TEXT_BUNDLE)  %></label>
				</div>
			</td>
			<td>
					<%=widgetFactory.createTextField("amount_field_name","",
    		  			poUploadDef.getAttribute("amount_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);'", "", "none") %>
			</td>
			<td>
					<%=widgetFactory.createTextField("amount_size","",
    		  			poUploadDef.getAttribute("amount_size") ,"2",isReadOnly,false,false, "class='char5'; onChange='createSize(this)';", "", "none") %>
			</td>
			<td><%= resMgr.getText("POUploadDefinitionDetail.Number", TradePortalConstants.TEXT_BUNDLE) %> </td>
			<td style="text-align:right;"><%= resMgr.getText("22", TradePortalConstants.TEXT_BUNDLE) %></td>
		</tr>
	
		
		<tr>
			<td colspan="5" class="tableSubHeader"><b><%=resMgr.getText("POUploadDefinitionDetail.OtherDataItem",TradePortalConstants.TEXT_BUNDLE)  %></b></td>
		</tr>
	</tbody>
	<tbody>
	<%
	for(int x=1; x<=NUMBER_OF_OTHER_FIELDS; x++) {
		String def= poUploadDef.getAttribute("other" + x + "_size");
		if(poUploadDef.getAttribute("other" + x + "_field_name")!=null && poUploadDef.getAttribute("other" + x + "_field_name").trim().length()==0){
			def="";
		}
		String name ="";
		if(x==1)
			name= "other"+x+"_name";
	%>
	
		
		<tr>

	<td><%=resMgr.getText("POUploadDefinitionDetail.DataItem",TradePortalConstants.TEXT_BUNDLE)+" "+x %></td>



	<td><%=widgetFactory.createTextField("other" + x + "_field_name","",
				poUploadDef.getAttribute("other" + x + "_field_name") ,"15", isReadOnly,false,false, "class='char15'; onChange='createDynamicRow(this);'", "", "none") %>
	<% if(x==1){%>
	<input type="hidden" name ="<%=name%>" id="<%=name %>" value="<%=def%>"/>
	<%} %>
	</td>

	<td>
		<%=widgetFactory.createTextField("other" + x + "_size","",
				def ,"2",isReadOnly,false,false, "class='char5'; onChange='createSize(this)';", "", "none") %>
	</td>
	
	<td nowrap class=ListText>
<%        // Display the datatype but exclude Large Text from drop down since it is only used for PO Text.
          Vector exclusion = new Vector();
          exclusion.add(TradePortalConstants.PO_FIELD_DATA_TYPE_LARGETEXT);
          po_field_data_type_options = Dropdown.createSortedRefDataOptions( TradePortalConstants.PO_FIELD_DATA_TYPE,
								poUploadDef.getAttribute("other" + x + "_datatype"), resMgr.getResourceLocale(),
                                exclusion );
          
%>

		
		
		<%= widgetFactory.createSelectField("other" + x + "_datatype", "", 
					" ", po_field_data_type_options, (isReadOnly), false, false, 
					"class='char7';", "", "none") %>
					
					
      </td>

<td style="text-align:right;"><%= resMgr.getText("35", TradePortalConstants.TEXT_BUNDLE) %></td>
</tr>
		
	<%
	}
	%>
	
	</tbody>	
		
	</table>
	<br/>
	<button data-dojo-type="dijit.form.Button" type="button" name="add4MoreRow" id="add4MoreRow" <%=(isReadOnly?"disabled":"")%> >
	<%=resMgr.getText("POUploadDefinitionDetail.Add4More",TradePortalConstants.TEXT_BUNDLE)  %>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	add4MoreRows();
	</script>
	</button>
	 <%=widgetFactory.createHoverHelp("add4MoreRow","PODefinition.Add4More") %>
	<input type=hidden value='<%=poUploadDef%>' name="poUploadDef" id="poUploadDefID">
	
	

</div>
<%-- Data Definition Title Panel End --%>


  
  

<input type=hidden name ="po_num_datatype" value="<%=TradePortalConstants.PO_FIELD_DATA_TYPE_TEXT%>">
<input type=hidden name ="item_num_datatype" value="<%=TradePortalConstants.PO_FIELD_DATA_TYPE_TEXT%>">
<input type=hidden name ="po_text_datatype" value="<%=TradePortalConstants.PO_FIELD_DATA_TYPE_LARGETEXT%>">
<input type=hidden name ="last_ship_dt_size" value="10">
<input type=hidden name ="last_ship_dt_datatype" value="<%=TradePortalConstants.PO_FIELD_DATA_TYPE_DATE%>">
<input type=hidden name ="ben_name_datatype" value="<%=TradePortalConstants.PO_FIELD_DATA_TYPE_TEXT%>">
<input type=hidden name="currency_size" value="3">
<input type=hidden name ="currency_datatype" value="<%=TradePortalConstants.PO_FIELD_DATA_TYPE_TEXT%>">
<input type=hidden name ="amount_datatype" value="<%=TradePortalConstants.PO_FIELD_DATA_TYPE_NUMBER%>">
