<%--
*******************************************************************************
                                    User Detail - Assigned TO Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
  <%--cquinton 12/10/2012 correct spacing and readonlyness--%>
  <div class="columnLeft">
<%
        options = ListBox.createOptionList(secProfileDoc,
                                   "SECURITY_PROFILE_OID", "NAME",
                                   user.getAttribute("security_profile_oid"),
					     // Encrypt the OID
					     userSession.getSecretKey());
        if (insertMode) {
          defaultText = resMgr.getText("UserDetail.selectProfile",
                                       TradePortalConstants.TEXT_BUNDLE);
        } else {
          defaultText = "";
        }
%>
    <%=widgetFactory.createSelectField("SecurityProfile",
					"UserDetail.SecurityProfile", " ", options, isReadOnly,
					true, false, "", "", "")%>

    <div class='formItem<%=isReadOnly?" readOnly":""%>'>
      <%=widgetFactory.createInlineLabel("ThresholdGroup","UserDetail.ThresholdGroup")%>
      <div style="clear:both;"></div>
      <%=widgetFactory.createNote("UserDetail.ThresholdGroupNote", "columnWidth")%>
      <div style="clear:both;"></div>
<%
        options = ListBox.createOptionList(thresholdGroupDoc,
                                      "THRESHOLD_GROUP_OID",
                                      "THRESHOLD_GROUP_NAME",
                                      user.getAttribute("threshold_group_oid"),
						  // Encrypt the OID
						  userSession.getSecretKey());
        defaultText = resMgr.getText("UserDetail.selectThreshold",
                                     TradePortalConstants.TEXT_BUNDLE);
%>
      <%=widgetFactory.createSelectField("ThresholdGroup",
					"", " ", options, isReadOnly,
					false, false, "", "", "none")%>
    </div>
  </div>

  <div class="columnRight">
    <div class='formItem<%=isReadOnly?" readOnly":""%>'>
      <%=widgetFactory.createInlineLabel("WorkGroup","UserDetail.WorkGroup")%>
      <div style="clear:both;"></div>
      <%=widgetFactory.createNote("UserDetail.WorkGroupNote")%>
      <div style="clear:both;"></div>
<%
        options = ListBox.createOptionList(workGroupDoc,
                                      "WORK_GROUP_OID",
                                      "WORK_GROUP_NAME",
                                      user.getAttribute("work_group_oid"),
						  // Encrypt the OID
						  userSession.getSecretKey());
        defaultText = resMgr.getText("UserDetail.selectWorkGroup",
                                     TradePortalConstants.TEXT_BUNDLE);
%>
      <%=widgetFactory.createSelectField("WorkGroup",
					"", " ", options, isWorkGroupReadOnly,
					false, false, "", "", "none")%>	
    </div>
  </div>
