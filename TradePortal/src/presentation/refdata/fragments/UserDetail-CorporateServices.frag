<%--
*******************************************************************************
                                    User Detail - Assigned TO Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<%--cquinton 12/12/2012 the condition for showing the section is included on the main
    page.  but necessary variables are set here--%>
<%
  // Setup the has doc prep radio button options; if the
  // indicator is empty or "N", make the first option the default
  boolean hasDocPrepAccess = false;

  if (user.getAttribute("doc_prep_ind").equals(TradePortalConstants.INDICATOR_YES)) {
    hasDocPrepAccess = true;
  } else {
    hasDocPrepAccess = false;
  }
%>
  <%--cquinton 12/10/2012 fixing formatting issue - use noDivider style--%>
  <div class="columnLeft noDivider">
    <div class="formItem">
      <%= widgetFactory.createSubLabel("UserDetail.AccessCorporateServicesText")%>
      <br/>
 			<%=widgetFactory.createRadioButtonField(
           "DocPrepInd",
           "AbleDocumentPreparation",
           "UserDetail.AbleDocumentPreparation", "Y", hasDocPrepAccess,
           isReadOnly, "", "")%>
     
      <br/>
 			<%=widgetFactory.createRadioButtonField(
           "DocPrepInd",
           "NotAbleDocumentPreparation",
           "UserDetail.NotAbleDocumentPreparation", "N", !hasDocPrepAccess,
           isReadOnly, "", "")%>
     
      <br/>
    </div>
  </div>
  
  <div class="columnRight">                    
    <%=widgetFactory.createTextField("DocPrepUsername","UserDetail.DocPrepUsername",user.getAttribute("doc_prep_username"),
         "30",isReadOnly,false,false,"onChange='document.UserDetail.DocPrepInd[1].click()'","", "")%>
  </div>	

  <%----------------End of Doc Prep ----------------------%>
