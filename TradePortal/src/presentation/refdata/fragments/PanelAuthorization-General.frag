<div>
<div class="columnLeft">
	<%=widgetFactory
					.createTextField(
							"PanelAuthorizationGroupId",
							"PanelAuthorizationGroupDetail.PanelAuthorizationGroupID",
							paGroup.getAttribute("panel_auth_group_id"), "4",
							isReadOnly, true, false, "", "", "")%>
</div>
<div class="columnRight">
	<%=widgetFactory
					.createTextField(
							"Name",
							"PanelAuthorizationGroupDetail.PanelAuthorizationGroupName",
							paGroup.getAttribute("name"), "35", isReadOnly,
							true, false, "", "", "")%>
	<input type=hidden name="owner_org_oid"
		value="<%=userSession.getOwnerOrgOid()%>">
</div>
</div>

<%=widgetFactory
					.createNote("PanelAuthorizationGroupDetail.PanelAuthorizationGroupIdComment")%>