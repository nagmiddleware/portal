<%--this fragment is included in CorporateCustomerDetail.jsp --%>

  <%= widgetFactory.createLabel("","CorpCust.ForNotificationEmail",isReadOnly, false, false, "labelClass=\"singleSpaced\"", "") %>

	<div class="columnLeft">
            <%
              defaultText = " ";
              if (!hasNotificationRules)  {
				defaultText = resMgr.getText("CorpCust.SelectNotifRule", TradePortalConstants.TEXT_BUNDLE);
              }
              dropdownOptions = ListBox.createOptionList(notificationRulesDoc, "NOTIFICATION_RULE_OID", "NAME", corporateOrgNotifRuleOid, userSession.getSecretKey());
              out.print(widgetFactory.createSelectField( "NotificationRuleOid", "CorpCust.ForNotificationRule", defaultText, dropdownOptions, isReadOnly));

      		sqlQuery = new StringBuilder();
        	queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
        	sqlQuery.append("select report_categories_ind from client_bank");
        	sqlQuery.append(" where organization_oid = ?");
      		Debug.debug(sqlQuery.toString());
      		queryListView.setSQL(sqlQuery.toString(),new Object[]{clientBankOid});
      		queryListView.getRecords();
      		String reportCategInd = queryListView.getRecordValue("report_categories_ind");
			
      		if (!("Y".equals(reportCategInd))) {
     	 %>

				<%=resMgr.getText("UserDetail.ReportType", TradePortalConstants.TEXT_BUNDLE)%>

				<%
				// Setup the        type radio button options;
				// if the indicator is set
				// to the empty string or "N", make the first option (Cash Management) the default
				String reportingType = corporateOrg.getAttribute("reporting_type");

				out.println(widgetFactory.createRadioButtonField("ReportingType", "TradePortalConstants.REPORTING_TYPE_CASH_MGMT",
                        "UserDetail.CashManagement", TradePortalConstants.REPORTING_TYPE_CASH_MGMT, reportingType.equals(TradePortalConstants.REPORTING_TYPE_CASH_MGMT), isReadOnly, "", ""));

				out.println(widgetFactory.createRadioButtonField("ReportingType", "TradePortalConstants.REPORTING_TYPE_TRADE_SRV",
                              "UserDetail.TradePortal", TradePortalConstants.REPORTING_TYPE_TRADE_SRV, reportingType.equals(TradePortalConstants.REPORTING_TYPE_TRADE_SRV), isReadOnly, "", ""));
			} else {
			%>
                  <input type=hidden  name=ReportingType value="T" >
            <% 
			} 
             
			dropdownOptions = Dropdown.createSortedRefDataOptions(TradePortalConstants.INSTRUMENT_LANGUAGE, corporateOrgEmailLanguage, userLocale);
            out.print(widgetFactory.createSelectField( "EmailLanguage", "CorpCust.SendEmailIn", "", dropdownOptions, isReadOnly));
            %>
    </div>
    
	<div class="columnRight">
				<%= widgetFactory.createTextArea( "EmailReceiver", "CorpCust.ForEmailReceipt", corporateOrg.getAttribute("email_receiver"), isReadOnly, false,false,"maxlength=\"150\"", "", "") %>
    </div>
