<%--
*******************************************************************************
       Notification Rule Detail - Import/Outgoing (Tab) Page
  
Description:    This jsp simply displays the html for the import/outgoing 
		portion of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-Import.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>

<%
//<Papia Dastidar IR#BKUF080335338 & BIUF080336121 25thAug 2005>
//Modified the constant name in TradePortalConstants.java from ACCOUNTS_REC to 
//ACCOUNTS_RECEIVABLE_UPDATE  as per Weian's suggestion
String[] transaction3Types	= {TradePortalConstants.NEW_RECEIVABLES,
    					// TradePortalConstants.RECEIVABLES_UPDATE,//IR PNUJ012862412 
    					TransactionType.CHANGE_RECEIVABLES, //IR PNUJ012862412 
    					TradePortalConstants.APPLY_RECEIVABLES_PAYMENT,
    					TransactionType.ISSUE,    //IR VIUI121940171
    					TransactionType.LIQUIDATE};//IR VIUI121940171

String[] criterion3Types		= {TradePortalConstants.NOTIF_RULE_NOTIFICATION, TradePortalConstants.NOTIF_RULE_EMAIL};

secureParms.put("name", notificationRule.getAttribute("name"));

%>  
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
	<thead>
		<tr>
			<th class="columnWidth200"><%= resMgr.getText("NotificationRuleDetail.Receivables", TradePortalConstants.TEXT_BUNDLE)%></th>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Notification", TradePortalConstants.TEXT_BUNDLE)%></th>
			<%
      		/* if (!emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY) && !emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
      		{ */
			%>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Email", TradePortalConstants.TEXT_BUNDLE)%></th>	
			<%
      		/* }  */    
			%>		
		 </tr>
		 <tr>
		    <th>&nbsp;</th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		 </tr>
	 </thead>
	 <tbody>
	 <% 
	 for (int transCount=0; transCount<transaction3Types.length; transCount++) 
	 { 
	   String[] criterionRuleOid		= new String[2];
	   String[] criterionRuleSetting 	= {//TradePortalConstants.NOTIF_RULE_ALWAYS,
	   						TradePortalConstants.NOTIF_RULE_NONE,
	   						TradePortalConstants.NOTIF_RULE_NONE};
	   String transactionTypeLabel           = transaction3Types[transCount];
	   transactionTypeLabel = TradePortalConstants.REC+transactionTypeLabel;
  	%>
    <tr>
    	<td align="left">
    	<%=widgetFactory.createSubLabel("NotificationRuleDetail.TransactionType" + transactionTypeLabel)%> 	
    	</td>
<%

  // If the notification rule specifies that only daily emails are sent then do not display
  // email criterion settings. Daily emails are driven by messages/notifications already 
  // present in the portal, not the notification rule's email settings
  int criterionTypeCount = 2;
  /* if (emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY) || emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
  {
    criterionTypeCount = 1;
  } */
  
  for (int criterionCount=0; criterionCount<criterionTypeCount; criterionCount++)
  {
    String key;
    key = InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT;
    key = key.concat(transaction3Types[transCount]);
    key = key.concat(criterion3Types[criterionCount]);

    NotifyRuleCriterionWebBean notifyRuleCriterion = null;
    notifyRuleCriterion = (NotifyRuleCriterionWebBean)criterionRuleList.get(key);
     
    if (notifyRuleCriterion != null)
    {
      criterionRuleOid[criterionCount] = notifyRuleCriterion.getAttribute("criterion_oid");
      criterionRuleSetting[criterionCount] = notifyRuleCriterion.getAttribute("setting");
    }
    
    String criterionRuleType = criterion3Types[criterionCount];
    criterionRuleCount++;
    
%>

    <jsp:include page="/refdata/NotificationRuleDetailRow.jsp">
      <jsp:param name="transactionTypes" 	value='<%=transaction3Types[transCount]%>' />    
      <jsp:param name="instrCategory" 		value='<%=InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT%>' />
      <jsp:param name="criterionRuleOid"	value='<%=criterionRuleOid[criterionCount]%>' />
      <jsp:param name="criterionRuleSetting" 	value='<%=criterionRuleSetting[criterionCount]%>' />
      <jsp:param name="criterionRuleType" 	value='<%=criterionRuleType%>' />
      <jsp:param name="criterionRuleCount" 	value='<%=criterionRuleCount%>' />	
      <jsp:param name="isReadOnly" 		value='<%=isReadOnly%>' />
    </jsp:include>
        
<%
  }
}
%>
</tbody>
</table><br></br>