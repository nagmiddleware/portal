<%
HashSet pmtDLMSummReqVal = new HashSet();
    
    pmtDLMSummReqVal.add("bene_account_number");
%>
<%-- Data Definition Title Panel details start --%>
<%=widgetFactory.createSectionHeader("3",
					"PaymentDefinitionDetail.DFF")%>

<div class = "columnLeft">
	<%=widgetFactory.createLabel("",
            "PaymentDefinitions.DefSep1", false, false, false,
            "none")%>


<table class="formDocumentsTable" id="PmtDLMDefTableId" style="width:98%;">
<thead>
	<tr style="height:35px;">
		<th colspan=4 style="text-align:left;">
			<%=widgetFactory
					.createSubLabel("PaymentDefinitions.DelimitedFF")%>
		</th>
	</tr>
</thead>
<tbody>
      <tr style="height:35px">
      	 <td width="15" align='center' >
             <b><%=resMgr.getText("PaymentDefinitions.ReqField", TradePortalConstants.TEXT_BUNDLE)%></b>
         </td>
 		 <td nowrap width="130"  >
		  	<b><%=resMgr.getText("PaymentDefinitions.FieldName", TradePortalConstants.TEXT_BUNDLE)%></b>
 		  </td>
 		 <td  width="20" align="center" >
		  	<b><%=resMgr.getText("PaymentDefinitions.Size", TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
 		 <td width="30" >
		  	<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=resMgr.getText("PaymentDefinitions.DataReq", TradePortalConstants.TEXT_BUNDLE)%></b>
 		 </td>
      </tr>
      <tr>
 		 <td >&nbsp;</td>
 		 <td>
 		 <span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("",
                    "PaymentDefinitions.PaymentMethod", false, true, false, "none")%>
 		  </td>
 		  <td align="right">
 		  <%= widgetFactory.createLabel("", "4", false, false, false, "none") %>
 		  </td>
 		  
 		  <td >&nbsp;</td>
 	  </tr>
      <tr>
 		 <td >&nbsp;</td>
 		 <td>
 		 <span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("",
                    "PaymentDefinitions.DebitAcctNum", false, true, false, "none")%>
 		  </td>
 		  
 		  <td align="right">
 			<%= widgetFactory.createLabel("", "30", false, false, false, "none") %>
 		  </td>
 		  <td >&nbsp;</td>
 	  </tr>
      <tr>
 		 <td >&nbsp;</td>
 		 <td  >
 		 <span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("",
                    "PaymentDefinitions.BeneficiaryName", false, true, false, "none")%>
 		  </td>
 		  <td align="right">
 			<%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
 		  </td>
 		  <td >&nbsp;</td>
 	  </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_account_number", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_account_number" id="bene_account_number">
              <%=widgetFactory.createCheckboxField(
                      "bene_account_number_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("bene_account_number_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
          <td  id="td_bene_account_number">
              <span class="asterisk">*</span>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.BeneficiaryAcctNum", false, true, false, "none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "34", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_account_number", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_account_number" id="bene_account_number">

              <%=widgetFactory.createCheckboxField(
                      "bene_account_number_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("bene_account_number_data_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_branch_code", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bank_branch_code" id="bene_bank_branch_code">
              <%=widgetFactory.createCheckboxField(
                      "bene_bank_branch_code_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("bene_bank_branch_code_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
          <td  id="td_bene_bank_branch_code">
              <span class="asterisk">*</span>
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.BeneficiaryBankBrnchCode", false, true, false, "none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_branch_code", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bank_branch_code" id="bene_bank_branch_code">

              <%=widgetFactory.createCheckboxField(
                      "bene_bank_branch_code_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("bene_bank_branch_code_data_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
      </tr>

 	  <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payment_currency", TradePortalConstants.TEXT_BUNDLE)%>" name="payment_currency" id="payment_currency">
              <%=widgetFactory.createCheckboxField(
                      "payment_currency_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("payment_currency_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
 		 <td  >
 		 <span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("",
					"PaymentDefinitions.Cuurency", false, true, false, "none")%>
 		  </td>
 		  <td align="right">
 			<%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
 		  </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payment_currency", TradePortalConstants.TEXT_BUNDLE)%>" name="payment_currency" id="payment_currency">

              <%=widgetFactory.createCheckboxField(
                      "payment_currency_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("payment_currency_data_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
 	  </tr>
 	  <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payment_amount", TradePortalConstants.TEXT_BUNDLE)%>" name="payment_amount" id="payment_amount">
              <%=widgetFactory.createCheckboxField(
                      "payment_amount_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("payment_amount_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
 		 <td  >
 		 <span class="asterisk">*</span>
		  	<%=widgetFactory.createLabel("", "PaymentDefinitions.Amount",
					false, true, false, "none")%>
 		  </td>
 		  <td align="right">
 			<%= widgetFactory.createLabel("", "15,3", false, false, false, "none") %>
 		  </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payment_amount", TradePortalConstants.TEXT_BUNDLE)%>" name="payment_amount" id="payment_amount">

              <%=widgetFactory.createCheckboxField(
                      "payment_amount_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("payment_amount_data_req")), isReadOnly,
                      false, "style='display: none;'", "", "none")%>
          </td>
 	  </tr>
 	  <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.customer_reference", TradePortalConstants.TEXT_BUNDLE)%>" name="customer_reference" id="customer_reference">

              <%=widgetFactory.createCheckboxField(
					"customer_reference_req",
					"",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("customer_reference_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td  >
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.CustomerReference", false, false, false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "20", false, false, false, "none") %>
 		  </td>
        <td align='center'>
 <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.customer_reference", TradePortalConstants.TEXT_BUNDLE)%>" name="customer_reference" id="customer_reference">
 
              <%=widgetFactory.createCheckboxField(
					"customer_reference_data_req",
					"",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("customer_reference_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.beneficiary_country", TradePortalConstants.TEXT_BUNDLE)%>" name="beneficiary_country" id="beneficiary_country">

              <%=widgetFactory.createCheckboxField(
					"beneficiary_country_req",
					"",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("beneficiary_country_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td  id="td_beneficiary_country">
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneficiaryCountry", false, false, false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
 		  </td>
        <td align='center'>
 <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.beneficiary_country", TradePortalConstants.TEXT_BUNDLE)%>" name="beneficiary_country" id="beneficiary_country">
 
              <%=widgetFactory.createCheckboxField(
					"beneficiary_country_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("beneficiary_country_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.execution_date", TradePortalConstants.TEXT_BUNDLE)%>" name="execution_date" id="execution_date">

              <%=widgetFactory.createCheckboxField(
					"execution_date_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("execution_date_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.ExecutionDate", false, false, false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.execution_date", TradePortalConstants.TEXT_BUNDLE)%>" name="execution_date" id="execution_date">

              <%=widgetFactory.createCheckboxField(
					"execution_date_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("execution_date_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address1", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address1" id="bene_address1">

              <%=widgetFactory.createCheckboxField(
					"bene_address1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address1_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td id="td_bene_address1">
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneAddress1", false, false, false, "none")%>
 		</td>
       <td align="right">
 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address1", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address1" id="bene_address1">

              <%=widgetFactory.createCheckboxField(
					"bene_address1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address1_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address2", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address2" id="bene_address2">

              <%=widgetFactory.createCheckboxField(
					"bene_address2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address2_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneAddress2", false, false, false,
					"none")%>
 		</td>
       <td align="right">
 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address2", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address2" id="bene_address2">

              <%=widgetFactory.createCheckboxField(
					"bene_address2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address2_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>          
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address3", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address3" id="bene_address3">

              <%=widgetFactory.createCheckboxField(
					"bene_address3_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address3_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneAddress3", false, false,
					false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		  </td>
        <td align='center'>          
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address3", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address3" id="bene_address3">

              <%=widgetFactory.createCheckboxField(
					"bene_address3_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address3_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address4", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address4" id="bene_address4">

              <%=widgetFactory.createCheckboxField(
					"bene_address4_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address4_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("", "PaymentDefinitions.BeneAddress4",
					false, false, false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_address4", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_address4" id="bene_address4">

              <%=widgetFactory.createCheckboxField(
					"bene_address4_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_address4_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_fax_no", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_fax_no" id="bene_fax_no">

              <%=widgetFactory.createCheckboxField(
					"bene_fax_no_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_fax_no_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneFaxNo", false, false, false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "15", false, false, false, "none") %>
 		  </td>
        <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_fax_no", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_fax_no" id="bene_fax_no">

              <%=widgetFactory.createCheckboxField(
					"bene_fax_no_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_fax_no_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
       <tr>
       <td align='center'>              
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_email_id", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_email_id" id="bene_email_id">

              <%=widgetFactory.createCheckboxField(
					"bene_email_id_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_email_id_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory
					.createLabel("", "PaymentDefinitions.BeneEmailId", false,
							false, false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "255", false, false, false, "none") %>
 		  </td>
        <td align='center'>              
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_email_id", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_email_id" id="bene_email_id">

              <%=widgetFactory.createCheckboxField(
					"bene_email_id_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_email_id_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
              <tr>
       <td align='center'>             
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.charges", TradePortalConstants.TEXT_BUNDLE)%>" name="charges" id="charges">

              <%=widgetFactory.createCheckboxField(
					"charges_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("charges_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.Charges", false, false, false, "none")%>
 		</td>
        <td align="right">
 			<%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
 		  </td>
 		        <td align='center'>             
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.charges", TradePortalConstants.TEXT_BUNDLE)%>" name="charges" id="charges">

              <%=widgetFactory.createCheckboxField(
					"charges_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("charges_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
 	    <tr>
       <td align='center'>              
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_name", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bank_name" id="bene_bank_name">

              <%=widgetFactory.createCheckboxField(
					"bene_bank_name_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bank_name_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
 		<td>
			<%=widgetFactory.createLabel("",
					"PaymentDefinitions.BeneBankName", false, false, false,
					"none")%>
 		</td>
       <td align="right">
 			<%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
 		  </td>
        <td align='center'>              
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bank_name", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bank_name" id="bene_bank_name">

              <%=widgetFactory.createCheckboxField(
					"bene_bank_name_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bank_name_data_req")), isReadOnly,
					false, "", "", "none")%>
       </td>
       </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_name", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_name" id="bene_bnk_brnch_name">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_name_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_name_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_name", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_name", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_name" id="bene_bnk_brnch_name">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_name_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_name_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr1", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_addr1" id="bene_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_addr1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_addr1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr1", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_addr1" id="bene_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_addr1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr2", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_addr2" id="bene_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_addr2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_addr2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_addr2", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_addr2" id="bene_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_addr2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_addr2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_city", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_city" id="bene_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_city_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_city_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_city", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "31", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_city", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_city" id="bene_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_city_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_city_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_prvnc" id="bene_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_prvnc_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_prvnc_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_prvnc", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "8", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_prvnc" id="bene_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_prvnc_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_prvnc_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_cntry" id="bene_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_cntry_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_cntry_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.bene_bnk_brnch_cntry", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.bene_bnk_brnch_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="bene_bnk_brnch_cntry" id="bene_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"bene_bnk_brnch_cntry_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("bene_bnk_brnch_cntry_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payable_location", TradePortalConstants.TEXT_BUNDLE)%>" name="payable_location" id="payable_location">

              <%=widgetFactory.createCheckboxField(
					"payable_location_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("payable_location_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_payable_location">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.payable_location", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "20", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.payable_location", TradePortalConstants.TEXT_BUNDLE)%>" name="payable_location" id="payable_location">

              <%=widgetFactory.createCheckboxField(
					"payable_location_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("payable_location_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.print_location", TradePortalConstants.TEXT_BUNDLE)%>" name="print_location" id="print_location">

              <%=widgetFactory.createCheckboxField(
					"print_location_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("print_location_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_print_location">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.print_location", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "20", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.print_location", TradePortalConstants.TEXT_BUNDLE)%>" name="print_location" id="print_location">

              <%=widgetFactory.createCheckboxField(
					"print_location_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("print_location_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.delvry_mth_n_delvrto", TradePortalConstants.TEXT_BUNDLE)%>" name="delvry_mth_n_delvrto" id="delvry_mth_n_delvrto">

              <%=widgetFactory.createCheckboxField(
					"delvry_mth_n_delvrto_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("delvry_mth_n_delvrto_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_delvry_mth_n_delvrto">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.delvry_mth_n_delvrto", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.delvry_mth_n_delvrto", TradePortalConstants.TEXT_BUNDLE)%>" name="delvry_mth_n_delvrto" id="delvry_mth_n_delvrto">

              <%=widgetFactory.createCheckboxField(
					"delvry_mth_n_delvrto_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("delvry_mth_n_delvrto_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address1", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address1" id="mailing_address1">

              <%=widgetFactory.createCheckboxField(
					"mailing_address1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address1", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address1" id="mailing_address1">

              <%=widgetFactory.createCheckboxField(
					"mailing_address1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address2", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address2" id="mailing_address2">

              <%=widgetFactory.createCheckboxField(
					"mailing_address2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address2", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address2" id="mailing_address2">

              <%=widgetFactory.createCheckboxField(
					"mailing_address2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address3", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address3" id="mailing_address3">

              <%=widgetFactory.createCheckboxField(
					"mailing_address3_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address3_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address3", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address3", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address3" id="mailing_address3">

              <%=widgetFactory.createCheckboxField(
					"mailing_address3_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address3_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address4", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address4" id="mailing_address4">

              <%=widgetFactory.createCheckboxField(
					"mailing_address4_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address4_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.mailing_address4", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.mailing_address4", TradePortalConstants.TEXT_BUNDLE)%>" name="mailing_address4" id="mailing_address4">

              <%=widgetFactory.createCheckboxField(
					"mailing_address4_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("mailing_address4_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.details_of_payment", TradePortalConstants.TEXT_BUNDLE)%>" name="details_of_payment" id="details_of_payment">

              <%=widgetFactory.createCheckboxField(
					"details_of_payment_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("details_of_payment_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.details_of_payment", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "140", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.details_of_payment", TradePortalConstants.TEXT_BUNDLE)%>" name="details_of_payment" id="details_of_payment">

              <%=widgetFactory.createCheckboxField(
					"details_of_payment_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("details_of_payment_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.instruction_number", TradePortalConstants.TEXT_BUNDLE)%>" name="instruction_number" id="instruction_number">

              <%=widgetFactory.createCheckboxField(
					"instruction_number_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("instruction_number_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.instruction_number", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "10", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.instruction_number", TradePortalConstants.TEXT_BUNDLE)%>" name="instruction_number" id="instruction_number">

              <%=widgetFactory.createCheckboxField(
					"instruction_number_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("instruction_number_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_cd", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_cd" id="f_int_bnk_brnch_cd">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_cd_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cd_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_brnch_cd", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_cd", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_cd" id="f_int_bnk_brnch_cd">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_cd_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cd_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_name", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_name" id="f_int_bnk_name">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_name_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_name_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_name", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_name", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_name" id="f_int_bnk_name">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_name_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_name_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_nm", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_nm" id="f_int_bnk_brnch_nm">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_nm_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_nm_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_brnch_nm", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_nm", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_nm" id="f_int_bnk_brnch_nm">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_nm_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_nm_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_addr1", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_addr1" id="f_int_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_addr1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_brnch_addr1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_addr1", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_addr1" id="f_int_bnk_brnch_addr1">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_addr1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_addr2", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_addr2" id="f_int_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_addr2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_brnch_addr2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_addr2", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_addr2" id="f_int_bnk_brnch_addr2">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_addr2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_addr2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_city", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_city" id="f_int_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_city_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_city_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_brnch_city", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "31", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_city", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_city" id="f_int_bnk_brnch_city">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_city_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_city_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_prvnc" id="f_int_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_prvnc_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_prvnc_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_brnch_prvnc", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "8", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_prvnc", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_prvnc" id="f_int_bnk_brnch_prvnc">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_prvnc_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_prvnc_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_cntry" id="f_int_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_cntry_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cntry_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.f_int_bnk_brnch_cntry", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "2", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.f_int_bnk_brnch_cntry", TradePortalConstants.TEXT_BUNDLE)%>" name="f_int_bnk_brnch_cntry" id="f_int_bnk_brnch_cntry">

              <%=widgetFactory.createCheckboxField(
					"f_int_bnk_brnch_cntry_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("f_int_bnk_brnch_cntry_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep1", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep1" id="central_bank_rep1">

              <%=widgetFactory.createCheckboxField(
					"central_bank_rep1_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep1_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td id="td_central_bank_rep1">
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.central_bank_rep1", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep1", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep1" id="central_bank_rep1">

              <%=widgetFactory.createCheckboxField(
					"central_bank_rep1_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep1_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep2", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep2" id="central_bank_rep2">

              <%=widgetFactory.createCheckboxField(
					"central_bank_rep2_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep2_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.central_bank_rep2", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep2", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep2" id="central_bank_rep2">

              <%=widgetFactory.createCheckboxField(
					"central_bank_rep2_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep2_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep3", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep3" id="central_bank_rep3">

              <%=widgetFactory.createCheckboxField(
					"central_bank_rep3_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep3_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.central_bank_rep3", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "35", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.central_bank_rep3", TradePortalConstants.TEXT_BUNDLE)%>" name="central_bank_rep3" id="central_bank_rep3">

              <%=widgetFactory.createCheckboxField(
					"central_bank_rep3_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("central_bank_rep3_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>
      
      <%-- Rel9.4 IR-45461 Starts --%>
      
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code1", TradePortalConstants.TEXT_BUNDLE)%>" name="reporting_code1" id="reporting_code1">

              <%=widgetFactory.createCheckboxField(
                      "reporting_code1_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code1_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td id="td_reporting_code1">
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.reporting_code1", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code1", TradePortalConstants.TEXT_BUNDLE)%>" name="reporting_code1" id="reporting_code1">

              <%=widgetFactory.createCheckboxField(
                      "reporting_code1_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code1_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code2", TradePortalConstants.TEXT_BUNDLE)%>" name="reporting_code2" id="reporting_code2">

              <%=widgetFactory.createCheckboxField(
                      "reporting_code2_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code2_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
          <td id="td_reporting_code2">
              <%=widgetFactory.createLabel("",
                      "PaymentDefinitions.reporting_code2", false, false, false,
                      "none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "3", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.reporting_code2", TradePortalConstants.TEXT_BUNDLE)%>" name="reporting_code2" id="reporting_code2">

              <%=widgetFactory.createCheckboxField(
                      "reporting_code2_data_req",
                      "",
                      TradePortalConstants.INDICATOR_YES.equals(
                              paymentDef.getAttribute("reporting_code2_data_req")), isReadOnly,
                      false, "", "", "none")%>
          </td>
      </tr>
      <%-- Rel9.4 IR-45461 Ends --%>
      
      
      <tr>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.confidential_ind", TradePortalConstants.TEXT_BUNDLE)%>" name="confidential_ind" id="confidential_ind">

              <%=widgetFactory.createCheckboxField(
					"confidential_ind_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("confidential_ind_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
          <td>
              <%=widgetFactory.createLabel("",
					"PaymentDefinitions.confidential_ind", false, false, false,
					"none")%>
          </td>
          <td align="right">
              <%= widgetFactory.createLabel("", "1", false, false, false, "none") %>
          </td>
          <td align='center'>
              <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.confidential_ind", TradePortalConstants.TEXT_BUNDLE)%>" name="confidential_ind" id="confidential_ind">

              <%=widgetFactory.createCheckboxField(
					"confidential_ind_data_req",
					"",
					TradePortalConstants.INDICATOR_YES.equals(
							paymentDef.getAttribute("confidential_ind_data_req")), isReadOnly,
					false, "", "", "none")%>
          </td>
      </tr>

      <tr>
      <td align='center'>
          <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_number", TradePortalConstants.TEXT_BUNDLE)%>" name="fx_contract_number" id="fx_contract_number">

          <%=widgetFactory.createCheckboxField(
                  "fx_contract_number_req",
                  "",
                  TradePortalConstants.INDICATOR_YES.equals(
                          paymentDef.getAttribute("fx_contract_number_req")), isReadOnly,
                  false, "", "", "none")%>
      </td>
      <td  >
          <%=widgetFactory.createLabel("",
                  "PaymentDefinitions.fx_contract_number", false, false, false, "none")%>
      </td>
      <td align="right">
      <%= widgetFactory.createLabel("", "14", false, false, false, "none") %>
        
      </td>
      <td align='center'>
          <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_number", TradePortalConstants.TEXT_BUNDLE)%>" name="fx_contract_number" id="fx_contract_number">

          <%=widgetFactory.createCheckboxField(
                  "fx_contract_number_data_req",
                  "",
                  TradePortalConstants.INDICATOR_YES.equals(
                          paymentDef.getAttribute("fx_contract_number_data_req")), isReadOnly,
                  false, "", "", "none")%>
      </td>
      </tr>
      
      <tr>
      <td align='center'>
          <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_rate", TradePortalConstants.TEXT_BUNDLE)%>" name="fx_contract_rate" id="fx_contract_rate">
    
          <%=widgetFactory.createCheckboxField(
                  "fx_contract_rate_req",
                  "",
                  TradePortalConstants.INDICATOR_YES.equals(
                          paymentDef.getAttribute("fx_contract_rate_req")), isReadOnly,
                  false, "", "", "none")%>
      </td>
      <td>
          <%=widgetFactory.createLabel("",
                  "PaymentDefinitions.fx_contract_rate", false, false, false, "none")%>
      </td>
      <td align="right">
      
      <%= widgetFactory.createLabel("", "5,8", false, false, false, "none") %>
        
      </td>
      <td align='center'>
      <input type="hidden" value="<%=resMgr.getText("PaymentUploadRequest.fx_contract_rate", TradePortalConstants.TEXT_BUNDLE)%>" name="fx_contract_rate" id="fx_contract_rate">

      <%=widgetFactory.createCheckboxField(
              "fx_contract_rate_data_req",
              "",
              TradePortalConstants.INDICATOR_YES.equals(
                      paymentDef.getAttribute("fx_contract_rate_data_req")), isReadOnly,
              false, "", "", "none")%>
      </td>    
      </tr>

</tbody>
</table>


<table>

     <tr>
     <td>&nbsp</td>
     </tr>
</table>

<br/>
<br/>
<p>


<br/><br/>
<p>
</div>

<div class = "columnRight">

<%=widgetFactory.createLabel("",
					"PaymentDefinitionDetail.DLMDefSep2", false, false, false,
					"none")%>
<table>
    <tr style="height:16px;"><td>&nbsp;</td></tr>
    
</table>
<table class="formDocumentsTable" id="PmtDLMDefFileOrderTableId" >
<thead>
	<tr style="height:35px">
		<th colspan=2 style="text-align:left">
			<%=widgetFactory
					.createSubLabel("PaymentDefinitions.DelimitedFileDataOrder")%>
		</th>
	</tr>
</thead>

<tbody>

      <tr style="height:35px">
 		 <td width="20" >
		  	<%=widgetFactory.createLabel("",
					"POStructureDefinition.Order", false, false, false, "none")%>
 		  </td>
 		  <td width="180">
		  	<%=widgetFactory.createLabel("",
					"POStructureDefinition.FieldName", false, false, false, "none")%>
 		  </td>
       </tr>

<%
//RKAZI Rel 8.2 T36000011727 - START
//Reformated code to avoid creation of #text node under the summary order tables which causes issues undr IE7's javascript.
//NOTE: please do not reformat the code.
	String[] initialValDLM = { "payment_method", "debit_account_number", "beneficiary_name","bene_account_number","bene_bank_branch_code", "payment_currency", "payment_amount" };
	boolean initialFlagDLM = false;
	if (InstrumentServices.isBlank(paymentDef
			.getAttribute("pmt_mthd_summary_order1")))
		initialFlagDLM = true;
	String currentValueDLM;
	if (initialFlagDLM) {
		for (counterTmp = 1; counterTmp <= initialValDLM.length; counterTmp++) {

			currentValueDLM = initialValDLM[counterTmp - 1];
%>

 	  <tr id= "<%=currentValueDLM%>_req_order">
 		 <td  align="center">
   			<input type="text" name="" value='<%=counterTmp%>'
   				data-dojo-type="dijit.form.TextBox"
                    id='text<%=counterTmp%>'  style="width: 25px;margin: 1px 1px 1px 1px;" '<%=isReadOnly%>' /><input type="hidden" id="upload_order_<%=counterTmp%>" name="pmt_mthd_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueDLM%>" />
 		  </td>
          <td id='fileFieldName<%=counterTmp%>' >
			<%=widgetFactory.createLabel("",
							"PaymentUploadRequest." + currentValueDLM, false,
							false, false, "none")%>

 		  </td>
	  </tr>
<%
	}
	} else {
		counterTmp = 1;
		// invoice summary data in upload invoice file has maximum 28 option( i.e. inv_summary_order1, inv_summary_order2,..inv_summary_order9)
		while ((counterTmp <= 65)
				&& InstrumentServices
						.isNotBlank(paymentDef
								.getAttribute("pmt_mthd_summary_order"
										+ (new Integer(counterTmp))
												.toString()))) {

			currentValueDLM = paymentDef.getAttribute("pmt_mthd_summary_order"
					+ (new Integer(counterTmp)).toString());
%>

   	   <tr id= "<%=currentValueDLM%>_req_order">
 		 <td  align="center">
   			<input type="text" name="" value='<%=counterTmp%>'
   				data-dojo-type="dijit.form.TextBox" data-dojo-props="trim:true"
   				id='text<%=counterTmp%>'  style="width: 25px;" '<%=isReadOnly%>' /><input type="hidden" id="upload_order_<%=counterTmp%>" name="pmt_mthd_summary_order<%=(new Integer(counterTmp)).toString()%>" value="<%=currentValueDLM%>" />
 		  </td>
          <td id='fileFieldName<%=counterTmp%>' align="left">
			<%=widgetFactory.createLabel("",
							"PaymentUploadRequest." + currentValueDLM, false,
							false, false,"labelClass=\"invOrderLabel\"", "none")%>
              <% if(pmtSummOptionalVal.contains(currentValueDLM) && !pmtDLMSummReqVal.contains(currentValueDLM)){ %>
			<span class="deleteSelectedItem"  style="margin:0.3em 1em;" id='<%=currentValueDLM%>'></span>
           <%} %>
 		  </td>
	   </tr>

<%
	counterTmp++;
		}
	}
%>
</tbody>
</table>
<br/>
<div>

<%
	invSummDataOrder = counterTmp;
	if (!(isReadOnly)) {
%>
<button data-dojo-type="dijit.form.Button" type="button" id="updateSysDefOrder"><%=resMgr.getText("common.updateButton",TradePortalConstants.TEXT_BUNDLE)%>
	<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
		updateDefOrder(document.getElementById("PmtDLMDefFileOrderTableId").rows.length, 'text');
	</script>
</button>
<%
	} else {
%>
					&nbsp;
<%
	}
%>

</div>
<br/><br/>


</div>


</div>
