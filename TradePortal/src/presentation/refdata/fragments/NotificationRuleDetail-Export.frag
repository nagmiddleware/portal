<%--
*******************************************************************************
       Notification Rule Detail - Export/Incoming (Tab) Page
  
Description:    This jsp simply displays the html for the export/incoming 
		portion of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:

		<%@ include file="NotificationRuleDetail-Export.jsp" %>

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>

<%
//<Papia Dastidar IR#BKUF080335338 & BIUF080336121 25thAug 2005>
//Modified the constant name in TradePortalConstants.java from ACCOUNTS_REC to 
//ACCOUNTS_RECEIVABLE_UPDATE  as per Weian's suggestion
//Suresh 07/20/2011 IR-SPUL062980539 added TransactionType.PCS
String[] transaction2Types	= {TradePortalConstants.ACCOUNTS_RECEIVABLE_UPDATE,
				   TradePortalConstants.ADJUSTMENT,
				   TransactionType.ADVISE, 
				   TransactionType.AMEND, 
				   TransactionType.ASSIGNMENT,
                                   TransactionType.BUY,
                                   TransactionType.CHANGE,  
                                   TransactionType.PCS,                
   				   TransactionType.PCE,
   				   TransactionType.CREATE_LOI, 
				   TransactionType.CREATE_USANCE,
				   TransactionType.DEACTIVATE,
  				   TransactionType.DOC_EXAM,
				   TransactionType.NAC,
				   TransactionType.EXPIRE, 
				   TransactionType.EXTEND, 
				   TransactionType.ISSUE, 
				   TransactionType.TRANSFER,
			           TransactionType.LIQUIDATE, 
			           TransactionType.PAYMENT,
				   TransactionType.PRE_ADVISE,     
				   TransactionType.REACTIVATE, 
				   TransactionType.REDUCE,
				   TransactionType.REVOLVE, 
				   TransactionType.TRANSFER_PAY};

String[] criterion2Types		= {TradePortalConstants.NOTIF_RULE_NOTIFICATION, TradePortalConstants.NOTIF_RULE_EMAIL};

secureParms.put("name", notificationRule.getAttribute("name"));

%>  
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Notification", TradePortalConstants.TEXT_BUNDLE)%></th>
			<%
      		/* if (!emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY) && !emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
      		{ */
			%>
			<th colspan="3"><%= resMgr.getText("NotificationRuleDetail.Email", TradePortalConstants.TEXT_BUNDLE)%></th>	
			<%
      		/* }  */    
			%>		
		 </tr>
		 <tr>
		    <th>&nbsp;</th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.Always")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.ChargesDocuments")%></th>
		    <th><%=widgetFactory.createSubLabel( "NotificationRuleDetail.None")%></th>
		 </tr>
	 </thead>
	 <tbody>
	 <% 
	 for (int transCount=0; transCount<transaction2Types.length; transCount++) 
	 { 
	   String[] criterionRuleOid		= new String[2];
	   String[] criterionRuleSetting 	= {TradePortalConstants.NOTIF_RULE_ALWAYS, TradePortalConstants.NOTIF_RULE_NONE};
	   String transactionTypeLabel           = transaction2Types[transCount];

	   if (transaction2Types[transCount].equals(TransactionType.AMEND))
	     transactionTypeLabel = TransactionType.AMEND + TransactionType.AMEND_TRANSFER;
  	%>
    <tr>
    	<td align="left">
    	<%=widgetFactory.createSubLabel("NotificationRuleDetail.TransactionType" + transactionTypeLabel)%> 	
    	</td>
<%

  // If the notification rule specifies that only daily emails are sent then do not display
  // email criterion settings. Daily emails are driven by messages/notifications already 
  // present in the portal, not the notification rule's email settings
  int criterionTypeCount = 2;
  /* if (emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY) || emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL))
  {
    criterionTypeCount = 1;
  } */
	
  for (int criterionCount=0; criterionCount<criterionTypeCount; criterionCount++)
  {
    String key;
    key = TradePortalConstants.EXPORT;
    key = key.concat(transaction2Types[transCount]);
    key = key.concat(criterion2Types[criterionCount]);

    NotifyRuleCriterionWebBean notifyRuleCriterion = null;
    notifyRuleCriterion = (NotifyRuleCriterionWebBean)criterionRuleList.get(key);
     
    if (notifyRuleCriterion != null)
    {
      criterionRuleOid[criterionCount] = notifyRuleCriterion.getAttribute("criterion_oid");
      criterionRuleSetting[criterionCount] = notifyRuleCriterion.getAttribute("setting");
    }
    String criterionRuleType = criterion2Types[criterionCount];
    criterionRuleCount++;

%>

    <jsp:include page="/refdata/NotificationRuleDetailRow.jsp">
      <jsp:param name="transactionTypes" 	value='<%=transaction2Types[transCount]%>' />    
      <jsp:param name="instrCategory" 		value='<%=TradePortalConstants.EXPORT%>' />
      <jsp:param name="criterionRuleOid"	value='<%=criterionRuleOid[criterionCount]%>' />
      <jsp:param name="criterionRuleSetting" 	value='<%=criterionRuleSetting[criterionCount]%>' />
      <jsp:param name="criterionRuleType" 	value='<%=criterionRuleType%>' />
      <jsp:param name="criterionRuleCount" 	value='<%=criterionRuleCount%>' />	
      <jsp:param name="isReadOnly" 		value='<%=isReadOnly%>' />
    </jsp:include>
        
<%
  }
}
%>
</tbody>
</table>