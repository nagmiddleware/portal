<%

     Debug.debug("******************** TradePartnerRUles-Invoice.frag *************************");  
	
      DocumentHandler queryDoc = null; //this doc will be use to get data from a query   
     ownerLevel      = userSession.getOwnershipLevel();
	 String introptions;
	 String recInstrumentType = editArMatchingRule.getAttribute("rec_instrument_type");
	 String payInstrumentType = editArMatchingRule.getAttribute("pay_instrument_type");
	 // String instrType       = defaultDoc.getAttribute("/In/ArMatchingRule/instrument_type");
	// DK CR709 Rel8.2 Starts	 
	String recLoanType = editArMatchingRule.getAttribute("rec_loan_type");
	 String payLoanType = editArMatchingRule.getAttribute("pay_loan_type");
	 String ltOptions;
	// DK CR709 Rel8.2 Ends
%>

<div class="columnLeft">
  <%= widgetFactory.createSubsectionHeader("TradingPartnerDetails.InvoicesRecInstrument",false, false, false,"" ) %>
 <%---- 
  <%= widgetFactory.createLabel("", "", isReadOnly, false, false, "") %><br>---%>
  
  <%
	  //Get the Organization Oid from the userSession WebBean first
	  Long organization_oid = new Long( userSession.getOwnerOrgOid() );

	  //Get the instrument type
	  //Build the option tags for the dropdown box.
	  //Display the dropdown box
	 		 
	   
	   Vector recInstrumentVector = new Vector();
	   recInstrumentVector.addElement(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT);
	   recInstrumentVector.addElement(InstrumentType.LOAN_RQST);	   
	   introptions = Dropdown.createSortedRefDataIncludeOptions( TradePortalConstants.INSTRUMENT_TYPE, recInstrumentType, resMgr,resMgr.getResourceLocale(), recInstrumentVector,false );
	   Vector recLoanTypeVector = new Vector();
	   recLoanTypeVector.addElement(TradePortalConstants.TRADE_LOAN);
	   recLoanTypeVector.addElement(TradePortalConstants.EXPORT_FIN);
	   recLoanTypeVector.addElement(TradePortalConstants.INV_FIN_REC);
	   ltOptions = Dropdown.createSortedRefDataIncludeOptions(TradePortalConstants.LOAN_TYPE, recLoanType,resMgr,resMgr.getResourceLocale(),recLoanTypeVector,false);	   
	%>
      <%= widgetFactory.createSelectField( "rec_instrument_type", "ArMatchingRuleDetail.UploagingGroupby"," ", introptions, isReadOnly, false, false, "onChange='displayRecLoanType()'", "", "") %>
      <div id="RecLoanTypeSection">
      <%= widgetFactory.createSelectField( "rec_loan_type", "TradingPartnerDetails.LoanType"," ",ltOptions, isReadOnly, false, false, "", "", "") %>
      </div>
</div>

<div class="columnRight">
 <%= widgetFactory.createSubsectionHeader("TradingPartnerDetails.InvoicesPayInstrument",false, false, false,"" ) %>
 
 <%
 	Vector payInstrumentVector = new Vector();
 	payInstrumentVector.addElement(InstrumentType.APPROVAL_TO_PAY);
	payInstrumentVector.addElement(InstrumentType.LOAN_RQST);	   
	String payoptions = Dropdown.createSortedRefDataIncludeOptions( TradePortalConstants.INSTRUMENT_TYPE, payInstrumentType, resMgr,resMgr.getResourceLocale(),payInstrumentVector,false );
	   Vector payLoanTypeVector = new Vector();
	   payLoanTypeVector.addElement(TradePortalConstants.TRADE_LOAN);
	   payLoanTypeVector.addElement(TradePortalConstants.INV_FIN_BR);
	   payLoanTypeVector.addElement(TradePortalConstants.INV_FIN_BB);
	   ltOptions = Dropdown.createSortedRefDataIncludeOptions(TradePortalConstants.LOAN_TYPE, payLoanType,resMgr,resMgr.getResourceLocale(),payLoanTypeVector,false);	   
 %>
 
  <%= widgetFactory.createSelectField( "pay_instrument_type", "ArMatchingRuleDetail.UploagingGroupby"," ", payoptions, isReadOnly, false, false, "onChange='displayPayLoanType()'", "", "") %>	
  <div id="PayLoanTypeSection">
  <%= widgetFactory.createSelectField( "pay_loan_type", "TradingPartnerDetails.LoanType"," ",ltOptions, isReadOnly, false, false, "", "", "") %>
  </div>
</div>
 
<script LANGUAGE="JavaScript">
 
  function unCheckInvoiceAllow(){
    <%-- RKAZI IR MMUM020953219 Rel 8.0 02/29/2012 changed field check - Start --%>
	if(document.forms[0].IndividualInvoiceAllowed.checked){
		<%-- document.forms[0].IndividualInvoiceAllowed.value='Y'; --%>
		dijit.getEnclosingWidget(document.forms[0].IndividualInvoiceAllowed).set('checked', true);
	}
	else
	  {
		<%-- document.forms[0].IndividualInvoiceAllowed.value='N'; --%>
		dijit.getEnclosingWidget(document.forms[0].IndividualInvoiceAllowed).set('checked', false);
	  }
    <%-- RKAZI IR MMUM020953219 Rel 8.0 02/29/2012 changed field check - End --%>
  }
  </script>