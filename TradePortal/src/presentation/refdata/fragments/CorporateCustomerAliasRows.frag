<%--this fragment is included in both the 
    CorporateCustomerSection11 fragment for existing rows as well as the
    CorporateCustomerAddAliasRows.jsp servlet for adding new rows via ajax.

  Passed variables:
    aliasList - a list of margin rules
    aliasIndex - an integer value, zero based of the 1st 
    isReadOnly
    isFromExpress
    loginLocale
--%>

<%
  for (int idx = 0; idx<aliasList.size(); idx++) {
    CustomerAliasWebBean customerAlias = (CustomerAliasWebBean) aliasList.get(idx);
%>

  <tr id="aliasIndex<%=aliasIndex+idx%>">
    <td align="center">
      <%= widgetFactory.createTextField( "alias"+(aliasIndex+idx), "", customerAlias.getAttribute("alias"), "70", isReadOnly, false, false, "style='width:300px'", "", "none") %>
    </td>
    <td align="center">
<%
    String currMsgCat0 = customerAlias.getAttribute("gen_message_category_oid");
    if (currMsgCat0 == null)  currMsgCat0 = "";
    String pagOptions1 = ListBox.createOptionList(msgCategoryList, "GEN_MESSAGE_CATEGORY_OID", "DESCR", currMsgCat0);
    out.println(widgetFactory.createSelectField( "genMessageCategory"+(aliasIndex+idx), "", " ", pagOptions1, isReadOnly, false, false, "style='width:300px'", "", "none"));

    String customerAliasOid1 = "";
    if (InstrumentServices.isNotBlank(customerAlias.getAttribute("customer_alias_oid"))) {
      customerAliasOid1 = EncryptDecrypt.encryptStringUsingTripleDes(customerAlias.getAttribute("customer_alias_oid"), userSession.getSecretKey());
    }
    out.print("<INPUT TYPE=HIDDEN NAME='customerAliasOid" + (aliasIndex+idx) + "' VALUE='" +customerAliasOid1+ "'>");
%>
    </td>
  </tr>
<%
  }
%>
