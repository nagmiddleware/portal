<%

	String defaultText = " ";
	String options;
	String del_options = "";
	int counterTmp = 3;
	String currentDefinedLab = "";
	String currentDefined = "";
	String currentDefinedLabReq = "";
	String currentDefinedReq = "";
	String currentDefinedLabUpdt = "";
	int invSummDataOrder;
	
	boolean invRecType=false ;
	boolean invPayType=false ;
	String  recVisible = "";
	String payVisible = "none";
	String currentDefinedLabDataReq = "";
	String currentDefinedDataReq = "";
	String temp="";
%>
<script language='JavaScript'>
	

</script>
<%-- General Title Panel details start --%>

<%=widgetFactory.createSectionHeader("1",
					"InvUploadDefinitionDetail.General")%>

<div class = "columnLeft">

	<%-- Name and Description --%>

	<%=widgetFactory.createSubsectionHeader(
					"INVUploadDefinitionDetail.NameAndDescription", false,
					false, false, "")%>
	<%=widgetFactory.createTextField("name",
					"PaymentDefinitions.name", paymentDef.getAttribute("name"),
					"35", isReadOnly,true,false,"class = 'char25'", "", "") %>

	<%=widgetFactory.createTextField("description",
					"PaymentDefinitions.Description",
					paymentDef.getAttribute("description"), "65", isReadOnly,true,false,"class = 'char25'", "", "") %>
        <%
            del_options = Dropdown.createSortedRefDataOptions(
                    TradePortalConstants.PMT_METHOD,
                    paymentDef.getAttribute("payment_method"),
                    resMgr.getResourceLocale());
        %>
         <%-- KMehta IR T36000026012 @Rel8400 12 Mar 2014 Start --%> 
        <%=widgetFactory.createSelectField("payment_method",
                "Payment Method", " ",
                del_options, isReadOnly, true, false, "onChange='setBenAcctMandate(false);'", "", "")%>
	<%-- KMehta IR T36000026012 @Rel8400 12 Mar 2014  End --%>
<%
if (isReadOnly){
%>
<input type=hidden name="name" value='<%=paymentDef.getAttribute("name")%>'></input>
<input type=hidden name="description" value='<%=paymentDef.getAttribute("description")%>'></input>
<input type=hidden name="date_format" value='<%=paymentDef.getAttribute("date_format")%>'></input>
<input type=hidden name="date_format" value='<%=paymentDef.getAttribute("date_format")%>'></input>

	        <%=widgetFactory.createSelectField("payment_method",
                "", " ",
                del_options, false, true, false, "onChange='setBenAcctMandate(false);' style='display: none;' ", "", "")%>
<%}%>
	
		<div>&nbsp;</div>
		<div>&nbsp;</div>
		<div>&nbsp;</div>
		<div>&nbsp;</div>

		

</div>
<div class = "columnRight">

	<%-- Date Format --%>

	<%=widgetFactory.createSubsectionHeader(
					"PaymentDefinitions.PmtDataFormatDesc", false, false, false,
					"")%>

	<%
		String poDateFormat = paymentDef.getAttribute("date_format");
		Vector excludeDateFormat = new Vector();
		excludeDateFormat.add("ddMMyy");
	%>

        <%
        	del_options = Dropdown.createSortedRefDataOptions(
        			TradePortalConstants.PAYMENT_DATE_FORMAT,
        			paymentDef.getAttribute("date_format"),
        			resMgr.getResourceLocale(), excludeDateFormat);
        			
        	del_options = "<option value=\"ddMMyy\"" + ("ddMMyy".equals(poDateFormat)? "selected=\"selected\"" : "") + ">DDMMYY</option>".concat(del_options) ;	
        	out.print(widgetFactory.createSelectField("date_format",
        			"POStructureDefinition.DateFormat", " ", del_options, isReadOnly, true, false,
        			"", "", ""));
        %>

	<%-- Data Format --%>
    <%=widgetFactory.createSubsectionHeader(
            "PaymentDefinitions.FileFormatType", false, false, false,
            "")%>



<span class="asterisk">&nbsp;&nbsp;</span>
        <span class="asterisk">*</span>
		  <%
		  

				if(InstrumentServices.isNotBlank(fileFormatType) && TradePortalConstants.PAYMENT_FILE_TYPE_FIX.equals(fileFormatType)){
					invRecType = true;
					invPayType = false;
					recVisible ="";
					payVisible ="none";
				}
				else if(InstrumentServices.isNotBlank(fileFormatType) && TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT.equals(fileFormatType)){
					invRecType = false;
					invPayType = true;
					payVisible ="";
					recVisible ="none";
				} else if(InstrumentServices.isNotBlank(fileFormatType) && TradePortalConstants.PAYMENT_FILE_TYPE_ABA.equals(fileFormatType)){

                      invRecType = true;
                      invPayType = false;
                      payVisible ="";
                      recVisible ="none";
                } else if(InstrumentServices.isNotBlank(fileFormatType) && TradePortalConstants.PAYMENT_FILE_TYPE_PNG.equals(fileFormatType)){

                    invRecType = true;
                    invPayType = false;
                    payVisible ="";
                    recVisible ="none";
                }
				if(invRecType == false && invPayType == false){
					recVisible ="";
					payVisible ="none";
				}
				
		  %>
	
	   <%out.print(widgetFactory.createRadioButtonField("file_format_type",
                                                       "TradePortalConstants.PAYMENT_FILE_TYPE_FIX", "PaymentDefinitions.FixedFF",isABADef ? fileFormatType : TradePortalConstants.PAYMENT_FILE_TYPE_FIX, invRecType,
                                                       (isReadOnly || invAssigned), 
                                                       "onClick=\"setBenAcctMandate(true);\"",""));%>					
	


<br/>
<br/>
<span class="asterisk">&nbsp;&nbsp;&nbsp;&nbsp;</span>
		<%out.print(widgetFactory.createRadioButtonField("file_format_type",
                                                       "TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT","PaymentDefinitions.DelimitedFileFormat" ,TradePortalConstants.PAYMENT_FILE_TYPE_DELIMIT ,invPayType,
                                                         (isReadOnly || invAssigned), 
                                                         "",""));%>

	
    <%
        del_options = Dropdown.createSortedRefDataOptions(
                TradePortalConstants.PAY_DEF_DLM,
                paymentDef.getAttribute("delimiter_char"),
                resMgr.getResourceLocale());
    %>  &nbsp;&nbsp;
    
  
    <%=widgetFactory.createSelectField("delimiter_char",
            "", " ",
            del_options, isReadOnly, true, false, "onChange='setDelimitedFileFormat();'", "", "none")%>

   <br/>
   <br/>
  <br/>
  <br/>
    <%=widgetFactory.createCheckboxField(
            "include_column_headers",
            "PaymentDefinitions.includeColumnHeading",
            TradePortalConstants.INDICATOR_YES.equals(paymentDef
                    .getAttribute("include_column_headers")),
            isReadOnly, false, "", "", "")%>

</div>
</div>

 <%--General Title Panel End--%>
<%
    if (TradePortalConstants.PAYMENT_FILE_TYPE_ABA.equals(fileFormatType)){
%>
<div id="ABAFFDiv">
    <%@ include file="PaymentDefinitionsDetail-GeneralABA.frag" %>
</div>
<%
    } else if ("PNG".equals(fileFormatType)){
%>
<div id="PNGFFDiv">
    <%@ include file="PaymentDefinitionsDetail-GeneralPNG.frag" %>
</div>
<%
    }else {
%>
    <jsp:include page="/refdata/fragments/PaymentDefinitionsDetail-GeneralFetchDLMAndFF.jsp">
       <jsp:param name="oid" value="<%=paymentDefOid%>" />
       <jsp:param name="retrieveFromDB"  value="<%=retrieveFromDB%>" />
       <jsp:param name="getDataFromDoc"  value="<%=getDataFromDoc%>" />
       <jsp:param name="isReadOnly"  value="<%=isReadOnly%>" />
    </jsp:include>
<%
    }
%>