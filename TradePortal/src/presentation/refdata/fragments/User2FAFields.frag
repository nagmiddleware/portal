<%--
*******************************************************************************
                         User Login Password Fields

  Description:
     This fragment is used to construct the login and password fields for both 
  the Admin and User Detail pages.
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%
  if (!corpAuthMethod.equals(TradePortalConstants.AUTH_2FA)
      && !corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER)) {

  } else if ( corpAuthMethod.equals(TradePortalConstants.AUTH_PERUSER) && !isAdminUser ) {
%>
     <%
           boolean unableTOView = false;
         
      if (!(TradePortalConstants.INDICATOR_YES.equals(canViewIDFields) || userSecurityType.equals(TradePortalConstants.ADMIN)))
         {
           unableTOView = true;
         }
      %>

       
  <%= widgetFactory.createRadioButtonField("AuthenticationMethod", "TradePortalConstants.AUTH_2FA","UserDetail.Use2FA",
        TradePortalConstants.AUTH_2FA, TradePortalConstants.AUTH_2FA.equals(userAuthMethod), isReadOnly)%>
<%
  }
%>

  
  
 <%
//Disable radio choice when a user cannot view id fields
boolean disableRadio = true;
if (TradePortalConstants.INDICATOR_YES.equals(canViewIDFields) ||
		TradePortalConstants.ADMIN.equals(userSecurityType)) {
	if(isReadOnly){
		disableRadio = isReadOnly;
	}else{
	disableRadio=false;
	}
}

boolean rsaToken = false;
boolean vascoToken = false;

if (TradePortalConstants.RSA_TOKEN.equals(user.getAttribute("token_type"))) {
	rsaToken = true;
}
else if (TradePortalConstants.VASCO_TOKEN.equals(user.getAttribute("token_type"))) {
	vascoToken = true;
} 

//AAlubala - IR#LHUM053058274 Toggle visibility for VASCO 2FA options
    StringBuilder twoFAOptions = new StringBuilder();
      twoFAOptions.append("<option value=\"\"");
     if (!rsaToken && !vascoToken) twoFAOptions.append("\" selected ");
      twoFAOptions.append(">").append("</option>");

      twoFAOptions.append("<option value=\"").append(TradePortalConstants.RSA_TOKEN).append("\"");
     if (rsaToken) twoFAOptions.append(" selected ");
      twoFAOptions.append(">RSA").append("</option>");

      twoFAOptions.append("<option value=\"").append(TradePortalConstants.VASCO_TOKEN).append("\"");
     if (vascoToken) twoFAOptions.append(" selected ");
      twoFAOptions.append(">VASCO").append("</option>");

%>
  <%--cquinton 12/6/2012 wrap it all in some indenting--%>
  <div id="user2FALogin"class='<%=isAdminUser?"":"indentHalf"%>' >
        <%= widgetFactory.createSelectField("TokenType", "UserDetail.SecurityDeviceType", "", twoFAOptions.toString(),  disableRadio) %>
        
      <%--cquinton 1/28/2013 Note: DO NOT TRANSLATE RSA or VASCO as they are company/technology names--%>
    <%--SPenke T36000006179 10/11/2012 End --%>
    <%---Rpasupulati IRT36000035977 Start--%>
  <%if((TradePortalConstants.INDICATOR_YES.equals(canViewIDFields) ||
		TradePortalConstants.ADMIN.equals(userSecurityType))){%>
    
    <%= widgetFactory.createTextField( "TokenID2fa", "UserDetail.TokenID", user.getAttribute("token_id_2fa"), "32", isReadOnly, false, false,  "class='char20'", "","" ) %>
<%}%> 
<%---Rpasupulati IRT36000035977 End--%>
  </div>
