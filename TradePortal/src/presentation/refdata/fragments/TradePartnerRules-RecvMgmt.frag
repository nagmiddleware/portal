<%
	//AAlubala - Rel8.2 CR741 - ERP Codes - 01/08/2013 - BEGIN
	DocumentHandler    erpCodesDoc    = null;
    DocumentHandler    erpPayCodesDoc    = null;
    DocumentHandler    erpOverPayCodesDoc    = null;
	DocumentHandler    erpDiscCodesDoc    = null;
	DocumentHandler    cusDiscDoc                   = null;
	StringBuffer       dropdownOptions          = new StringBuffer();
	StringBuffer       whereClauseStr              = new StringBuffer();
	StringBuffer       sqlStmntStr                 = new StringBuffer();
	StringBuffer thePaySQL = new StringBuffer();
	StringBuffer theOverPaySQL = new StringBuffer();
	StringBuffer theDiscSQL = new StringBuffer();
	StringBuffer thePayDropDown = new StringBuffer();
	StringBuffer theOverPayDropDown = new StringBuffer();
	StringBuffer theDiscDropDown = new StringBuffer();
	String defaultPayCode = editArMatchingRule.getAttribute("payment_gl_code");
	String defaultOverPayCode = editArMatchingRule.getAttribute("over_payment_gl_code");
	String defaultDiscountCode = editArMatchingRule.getAttribute("discount_gl_code");
	StringBuffer       payDefaultSql                 = new StringBuffer();
	StringBuffer       overPayDefaultSql                 = new StringBuffer();
	StringBuffer       discountDefaultSql                 = new StringBuffer();
    DocumentHandler    defaultPayCodesDoc    = null;
    DocumentHandler    defaultOverPayCodesDoc    = null;
	DocumentHandler    defaultDiscCodesDoc    = null;	
	//CR741 - END
	secureParams.put("buyer_name", editArMatchingRule.getAttribute("buyer_name"));	  	
	boolean tole_percen = false;
	boolean tole_amount = false;
	boolean partial_pay=false;
	boolean no_tole=false;
	String tol_percent_plus = "";
	String tol_percent_minus = "";
	String tol_amount_minus = "";
	String tol_amount_plus = "";
	String no_tol_value="";
	String partial_pay_indicator = "";
	partial_pay_indicator = editArMatchingRule.getAttribute("partial_pay_to_portal_indicator");
	no_tol_value = editArMatchingRule.getAttribute("no_tolerance_indicator");
	
	if((no_tol_value.trim().length()<1) || (no_tol_value.equals(TradePortalConstants.INDICATOR_NO)))
		no_tole=false;
    else
		no_tole=true;

	if((partial_pay_indicator.trim().length()<1) || (partial_pay_indicator.equals(TradePortalConstants.INDICATOR_NO)))
		partial_pay=false;
    else
		partial_pay=true;
	
	   
	tol_percent_plus=editArMatchingRule.getAttribute("tolerance_percent_plus");
	tol_percent_minus=editArMatchingRule.getAttribute("tolerance_percent_minus");
	if(tol_percent_plus.trim().length()<1 && tol_percent_minus.trim().length()<1)
		tole_percen=false;
	else
		tole_percen=true;

	tol_amount_plus=editArMatchingRule.getAttribute("tolerance_amount_plus");
	tol_amount_minus=editArMatchingRule.getAttribute("tolerance_amount_minus");
	if(tol_amount_plus.trim().length()<1 && tol_amount_minus.trim().length()<1)
		tole_amount=false;
	else
		tole_amount=true;
	
	Debug.debug("tole_amount:"+tole_amount+"\t:"+(tol_amount_plus.trim().length()+"\t:"+ tol_amount_minus.trim().length()));
	if(editArMatchingRule.getAttribute("no_tolerance_indicator").equals("Y")){
	}
	//AiA - IR T36000018264 - Flip the divs (right and left) so vertical line goes all way the section
%>
<div class="columnRight">
  <%= widgetFactory.createSubsectionHeader("ArMatchingRuleDetail.Tolerances",false, false, false,"" ) %>
  <div class="formItem">
  <%= widgetFactory.createCheckboxField("ArMatchingRuleTolPerInd", "ArMatchingRuleDetail.Percent", 
		  tole_percen, isReadOnly, false, "onClick=\"unCheckNoTolInd()\"", "", "none")%><br>
  <div style="clear: both;"></div>
 
 
  <span class="formItem"><%= widgetFactory.createLabel("ArMatchingRuleDetail.PlusSign","ArMatchingRuleDetail.PlusSign",isReadOnly, false, false,"labelClass='alignVerticalMiddle'", "none") %> 
  <%= widgetFactory.createAmountField( "ArMatchingRuleTolPerPlus", "",editArMatchingRule.getAttribute("tolerance_percent_plus"), "", 
		                     isReadOnly, true, false,  "class='char3' onChange=\"textBoxCriteria()\"", "","none" ) %></span>
  <%= widgetFactory.createLabel("ArMatchingRuleDetail.PercentSign","ArMatchingRuleDetail.PercentSign",isReadOnly, false, false,"", "none") %> 
  
  <span class="formItem"><%= widgetFactory.createLabel("ArMatchingRuleDetail.MinusSign","ArMatchingRuleDetail.MinusSign",isReadOnly, false, false, "none") %> </span>
  <span class="formItem"><%= widgetFactory.createAmountField( "ArMatchingRuleTolPerMinus", "",editArMatchingRule.getAttribute("tolerance_percent_minus"), "", 
		                     isReadOnly, true, false,  "class='char3' onChange=\"textBoxCriteria()\"", "","none" ) %></span>
  <%= widgetFactory.createLabel("ArMatchingRuleDetail.PercentSign","ArMatchingRuleDetail.PercentSign",isReadOnly, false, false, "none") %> <br>
  <div style="clear: both;"></div>
 
   </div>
  
  
  <div class="formItem">
  <%= widgetFactory.createCheckboxField("ArMatchingRuleTolAmtInd", "ArMatchingRuleDetail.Amount", 
		  tole_amount, isReadOnly, false, "onClick=\"unCheckNoTolInd()\"", "", "none")%><br>
  <div style="clear: both;"></div>
  
  <span class="formItem"><%= widgetFactory.createLabel("ArMatchingRuleDetail.PlusSign","ArMatchingRuleDetail.PlusSign",isReadOnly, false, false, "none") %> 
  <%= widgetFactory.createAmountField( "ArMatchingRuleTolAmtPlus", "",editArMatchingRule.getAttribute("tolerance_amount_plus"), "", 
		                     isReadOnly, true, false,  "class='char3' onChange=\"textBoxCriteria()\"", "","none" ) %></span>
  
  
  <span class="formItem"><%= widgetFactory.createLabel("ArMatchingRuleDetail.MinusSign","ArMatchingRuleDetail.MinusSign",isReadOnly, false, false, "none") %></span> 
  <span class="formItem"><%= widgetFactory.createAmountField( "ArMatchingRuleTolAmtMinus", "",editArMatchingRule.getAttribute("tolerance_amount_minus"), "", 
		                     isReadOnly, true, false,  "class='char3' onChange=\"textBoxCriteria()\"", "","none" ) %></span>
 <br>
  
  <div style="clear: both;"></div>
  </div>
  
  <span class="formItem"><%= widgetFactory.createCheckboxField("ArMatchingRuleNoTolInd", "ArMatchingRuleDetail.NoTolerance", 
		  no_tole, isReadOnly, false, "onClick=\"pickCommTolCheckbox()\"", "", "none")%></span>
  
</div>
<div class="columnLeft">
  <%= widgetFactory.createSubsectionHeader("ArMatchingRuleDetail.PartialPayments",false, false, false,"" ) %>
  <div class="formItem">
  <%= widgetFactory.createCheckboxField("ArMatchingRulePayToPortalInd", "ArMatchingRuleDetail.PayToPortal", 
		  partial_pay, isReadOnly, false, "onClick=\"unCheckPartial()\"", "", "none")%>
  </div>
</div>
 
 <%--AAlubala Rel8.2 CR741 12/18/2012  Default ERP GL Codes BEGIN--%>
  <%
  
 	//Customer ERP GL Code Web Bean
	beanMgr.registerBean(
			"com.ams.tradeportal.busobj.webbean.CustomerErpGlCodeWebBean",
			"CustomerErpGlCode");
  CustomerErpGlCodeWebBean customerErpGlCode = (CustomerErpGlCodeWebBean) beanMgr
			.getBean("CustomerErpGlCode");
 
  //This query will be used to populate the discount codes dropdown list
   //jgadela R90 IR T36000026319 - SQL INJECTION FIX
   sqlStmntStr.append("select erp_gl_code, erp_gl_description, erp_gl_category, customer_erp_gl_code_oid, p_owner_oid, deactivate_ind, default_gl_code_ind");
   sqlStmntStr.append(" from customer_erp_gl_code"); 
   sqlStmntStr.append(" where deactivate_ind = '"+TradePortalConstants.INDICATOR_NO+"'");
   sqlStmntStr.append(" AND p_owner_oid in (?)"); //Rel8.2 IR T36000015507 - use only customer owned codes
   
  // sqlStmntStr.append(") and erp_gl_category in ('PAYMENT') ");
  // sqlStmntStr.append("  ");
  
   sqlStmntStr.append(" AND erp_gl_category = '");

   
   thePaySQL.append(sqlStmntStr);
   thePaySQL.append(TradePortalConstants.ERP_PAYMENT);
   thePaySQL.append("' ");
   //default code
   payDefaultSql.append(thePaySQL);
   payDefaultSql.append(" AND default_gl_code_ind = '");
   payDefaultSql.append(TradePortalConstants.INDICATOR_YES);
   payDefaultSql.append("' ");
   
   theOverPaySQL.append(sqlStmntStr);
   theOverPaySQL.append(TradePortalConstants.ERP_OVERPAYMENT);
   theOverPaySQL.append("' ");
   //default code
   overPayDefaultSql.append(theOverPaySQL);
   overPayDefaultSql.append(" AND default_gl_code_ind = '");
   overPayDefaultSql.append(TradePortalConstants.INDICATOR_YES);
   overPayDefaultSql.append("' ");   
   
   theDiscSQL.append(sqlStmntStr);
   theDiscSQL.append(TradePortalConstants.ERP_DISCOUNT);
   theDiscSQL.append("' ");
   //default code
   discountDefaultSql.append(theDiscSQL);
   discountDefaultSql.append(" AND default_gl_code_ind = '");
   discountDefaultSql.append(TradePortalConstants.INDICATOR_YES);
   discountDefaultSql.append("' ");   

	//jgadela R90 IR T36000026319 - SQL INJECTION FIX
   erpPayCodesDoc = DatabaseQueryBean.getXmlResultSet(thePaySQL.toString(), false, new Object[]{userSession.getOwnerOrgOid()});
   erpOverPayCodesDoc = DatabaseQueryBean.getXmlResultSet(theOverPaySQL.toString(), false, new Object[]{userSession.getOwnerOrgOid()});
   erpDiscCodesDoc = DatabaseQueryBean.getXmlResultSet(theDiscSQL.toString(), false, new Object[]{userSession.getOwnerOrgOid()});


   if(erpPayCodesDoc == null)
	   erpPayCodesDoc = new DocumentHandler();
   if(erpOverPayCodesDoc == null)
	   erpOverPayCodesDoc = new DocumentHandler();
   if(erpDiscCodesDoc == null)
	   erpDiscCodesDoc = new DocumentHandler(); 
   
   //Rel 8.2 IR T36000015507 - 04/02/2013
   //Now obtain the default codes
	//jgadela R90 IR T36000026319 - SQL INJECTION FIX
    defaultPayCodesDoc = DatabaseQueryBean.getXmlResultSet(payDefaultSql.toString(), false, new Object[]{userSession.getOwnerOrgOid()});
    defaultOverPayCodesDoc = DatabaseQueryBean.getXmlResultSet(overPayDefaultSql.toString(), false, new Object[]{userSession.getOwnerOrgOid()});
    defaultDiscCodesDoc = DatabaseQueryBean.getXmlResultSet(discountDefaultSql.toString(), false, new Object[]{userSession.getOwnerOrgOid()});

    if(defaultPayCodesDoc == null)
    	defaultPayCodesDoc = new DocumentHandler();
    if(defaultOverPayCodesDoc == null)
    	defaultOverPayCodesDoc = new DocumentHandler();
    if(defaultDiscCodesDoc == null)
    	defaultDiscCodesDoc = new DocumentHandler();    
    
   //if no code is defined, then use the default one
   //Rel8.2 IR T36000015788 - Default to blank
   if(StringFunction.isBlank(defaultPayCode))
	   //defaultPayCode = defaultPayCodesDoc.getAttribute("/ResultSetRecord/ERP_GL_CODE");
   			defaultPayCode = " ";
   if(StringFunction.isBlank(defaultOverPayCode))
	   //defaultOverPayCode = defaultOverPayCodesDoc.getAttribute("/ResultSetRecord/ERP_GL_CODE");
	   defaultOverPayCode = " ";
   if(StringFunction.isBlank(defaultDiscountCode))
	   //defaultDiscountCode = defaultDiscCodesDoc.getAttribute("/ResultSetRecord/ERP_GL_CODE"); 
	   defaultDiscountCode = " ";
   //Rel8.2 IR T36000015507 - END
    
   thePayDropDown.append(Dropdown.createMultiTextOptions(erpPayCodesDoc, "ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION", defaultPayCode, resMgr.getResourceLocale()));  
   
   theOverPayDropDown.append(Dropdown.createMultiTextOptions(erpOverPayCodesDoc, "ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION", defaultOverPayCode, resMgr.getResourceLocale()));
   
   theDiscDropDown.append(Dropdown.createMultiTextOptions(erpDiscCodesDoc, "ERP_GL_CODE","ERP_GL_CODE,ERP_GL_DESCRIPTION", defaultDiscountCode, resMgr.getResourceLocale()));  
       
%>
<div class="columnLeft">

  <%= widgetFactory.createSubsectionHeader("ArMatchingRuleDetail.DefaultERPGLCodes",false, false, false,"" ) %>
  <div class="formItem">
<%--IR T36000021779- use default values while creating Select field. defaultPayCode/defaultOverPayCode/defaultDiscountCode --%>
      <%= widgetFactory.createSelectField( "paymentGlCode", "ArMatchingRuleDetail.DefaultPayGLCodes",defaultPayCode, thePayDropDown.toString(), isReadOnly, false, false, "", "", "") %>

      <%= widgetFactory.createSelectField( "overpayGlCode", "ArMatchingRuleDetail.DefaultOverPayGLCodes",defaultOverPayCode, theOverPayDropDown.toString(), isReadOnly, false, false, "", "", "") %>   

      <%= widgetFactory.createSelectField( "discountGlCode", "ArMatchingRuleDetail.DefaultDiscountGLCodes",defaultDiscountCode, theDiscDropDown.toString(), isReadOnly, false, false, "", "", "") %>          
  </div>
</div> 
  <%--CR741 - END --%>
  <script>
	
  function textBoxCriteria(){
		 require(["dijit/registry"],
				  	function(registry) {
			 
			 
			 
	   
	   	var perPlus = registry.byId("ArMatchingRuleTolPerPlus").value.trim();
	   	var perMinus = registry.byId("ArMatchingRuleTolPerMinus").value.trim();
	   	var amtPlus = registry.byId("ArMatchingRuleTolAmtPlus").value.trim();
	   	var amtMinus = registry.byId("ArMatchingRuleTolAmtMinus").value.trim();
	   	

				if (!(isNaN(perPlus) && isNaN(perMinus)) )
				{
 					registry.byId('ArMatchingRuleTolPerInd').set("checked",true);
					registry.byId('ArMatchingRuleNoTolInd').set("checked",false);
		 	  	}
				if (!(isNaN(amtPlus) && isNaN(amtMinus)) )
		 	  	{
 					registry.byId('ArMatchingRuleTolAmtInd').set('checked',true);
					registry.byId('ArMatchingRuleNoTolInd').set('checked',false);
		 		}
		 }); 
	}
  
	function pickCommTolCheckbox() {
	    require(["dijit/registry"],
	      function(registry) {
	  	  
 			
	    	if(registry.byId("ArMatchingRuleNoTolInd").checked == true)
	 	   {
    	  		
 	    		
	    		<%-- document.forms[0].ArMatchingRuleTolPerInd.checked = false; --%>
	      		<%-- document.forms[0].ArMatchingRuleTolAmtInd.checked = false; --%>
	      		registry.byId('ArMatchingRuleTolPerInd').set("checked",false);
	      		registry.byId('ArMatchingRuleTolAmtInd').set("checked",false);
	      		registry.byId("ArMatchingRuleTolPerPlus").set("value",'');
	    	   registry.byId("ArMatchingRuleTolPerMinus").set("value",'');
	    	   	registry.byId("ArMatchingRuleTolAmtPlus").set("value",'');
	    	   	registry.byId("ArMatchingRuleTolAmtMinus").set("value",'');
	    	   	registry.byId("ArMatchingRuleTolPerPlus").set("required",false);
		    	registry.byId("ArMatchingRuleTolPerMinus").set("required",false);
		    	registry.byId("ArMatchingRuleTolAmtPlus").set("required",false);
		    	registry.byId("ArMatchingRuleTolAmtMinus").set("required",false);
		  }
	  });
	}
	
	function unCheckNoTolInd() {
	    require(["dijit/registry"],
	  	    function(registry) {
	    	
 	    	
		 	  if (document.forms[0].ArMatchingRuleTolPerInd.checked ||
		 	  		document.forms[0].ArMatchingRuleTolAmtInd.checked)
		 	  	{	
		 	  	<%-- document.forms[0].ArMatchingRuleNoTolInd.checked =  false; --%>
		 	  	registry.byId('ArMatchingRuleNoTolInd').set("checked",false);
		 	  	}
		
		 	  if(!document.forms[0].ArMatchingRuleTolPerInd.checked)
		 	  {	
		 	  	document.forms[0].ArMatchingRuleTolPerPlus.value='';
		 	  	document.forms[0].ArMatchingRuleTolPerMinus.value='';
		 	  }
		
		 	  if(!document.forms[0].ArMatchingRuleTolAmtInd.checked)
				{
				document.forms[0].ArMatchingRuleTolAmtMinus.value= '';
		 	  	document.forms[0].ArMatchingRuleTolAmtPlus.value='';
		
		 		}
		});
	}

	
	
	function unCheckPartial(){
 	    require(["dijit/registry"],
 	    		function(registry) {
					if(document.forms[0].ArMatchingRulePayToPortalInd.checked){
						registry.byId('ArMatchingRulePayToPortalInd').set("checked",true);
					}
					else {
						registry.byId('ArMatchingRulePayToPortalInd').set("checked",false);
					}
		}); 
	}	
	


</script>