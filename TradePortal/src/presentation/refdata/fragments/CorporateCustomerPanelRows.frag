<%--this fragment is included in  
    CorporateCustomerSection10 fragment for adding multiple Panel Groups to an Account
Added for CR 821 - Rel 8.3
  Passed variables:
    panelIndex - an integer value, zero based of the 1st 
    rowIndex	
    isReadOnly
    corporateOrgOid
    panelAuthGroupList
--%>
<%

  for (int idx = 0; idx<DEFAULT_PANELGROUP_COUNT; idx++) {
%>

  <tr id="panelIndex<%=rowIndex+idx%>">
    <td align="left" width="15%">
    <%
    out.println(widgetFactory.createSelectField("pagIdSelectionEdit"+(panelIndex+idx+1), "","", pagOptionsPayments, isReadOnlyAcc, false, false, " required=\"false\" class=\"char9\"" , "", "none"));
	 %>
    </td>
    <td align="left" width="15%">
<%
	out.println(widgetFactory.createSelectField("pagIdSelectionEdit"+(panelIndex+idx+2), "","", pagOptionsPayments, isReadOnlyAcc, false, false, " required=\"false\" class=\"char9\"" , "", "none"));
	
	 out.print("<INPUT TYPE=HIDDEN NAME='pagIdSelectionEdit" + (panelIndex+idx+1) + "' VALUE='" +""+ "'>");
	 out.print("<INPUT TYPE=HIDDEN NAME='pagIdSelectionEdit" + (panelIndex+idx+2) + "' VALUE='" +""+ "'>");
%>
    </td>
  </tr>
<%
panelIndex++;
  }
%>
