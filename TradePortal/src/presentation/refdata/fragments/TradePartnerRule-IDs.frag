<%= widgetFactory.createLabel("", "ArMatchingRuleDetail.BuyerId", isBuyerNameIDReadOnly, true, false, "") %>
        <%=widgetFactory.createTextField("ArMatchingRuleBuyerId","",
						editArMatchingRule.getAttribute("buyer_id"), "30", isBuyerNameIDReadOnly, true)%>
     
		<table id="BuyerIdAlias" class="formDocumentsTable" name="BuyerIdAlias">
	     <thead>
		<tr>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.Trade_ID")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.Trade_ID", TradePortalConstants.TEXT_BUNDLE)%></b></th>
			<th>&nbsp;</th>
			<%---<th><%=widgetFactory.createSubLabel("ArMatchingRuleDetail.Trade_ID")%></th>---%>
			<th class="genericCol"><b><%=resMgr.getText("ArMatchingRuleDetail.Trade_ID", TradePortalConstants.TEXT_BUNDLE)%></b></th>
		</tr>
	   </thead>

	   <tbody>
	       <% 
		   for (iLoop = 0; iLoop < aliasIdCount; iLoop = iLoop + 2) {
			%>
			<tr>	
			    <td><%=iLoop+1 + "."%></td>			
				<td >
			       <%=widgetFactory.createTextField("ArMatchingRuleIdAlias" + iLoop,"", arbuyeridalias[iLoop].getAttribute("buyer_id_alias"), "35", isReadOnly, false)%>
				<%
					//attributeOID = 
						out
						.print("<INPUT TYPE=HIDDEN NAME='BuyerIdAliasoid"
								+ iLoop
								+ "' VALUE='"
								+ EncryptDecrypt
								.encryptStringUsingTripleDes(arbuyernamealias[iLoop]
								.getAttribute("ar_buyer_id_alias_oid"))
								+ "'>");
				%>
				</td>
                <td><%=iLoop+2 + "."%></td>
                <td>
			<%	if(iLoop+1<52){
				%>
				<%=widgetFactory.createTextField("ArMatchingRuleIdAlias" + (1 + iLoop),"", arbuyeridalias[1 + iLoop].getAttribute("buyer_id_alias"), "35", isReadOnly, false)%>
				<%
							out
							.print("<INPUT TYPE=HIDDEN NAME='BuyerIdAliasoid"
							+ (1 + iLoop)
							+ "' VALUE='"
							+ EncryptDecrypt
								.encryptStringUsingTripleDes(arbuyernamealias[1 + iLoop]
									.getAttribute("ar_buyer_id_alias_oid"))
							+ "'>");
				} else{
				%>
				&nbsp;
				<%}%>
				</td>											
			</tr>						
			<%
			}			
			%>		
	  </tbody>
    </table>		

   <%    
   if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreTradeID">
				Add 4 More
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					addBuyerName('" + resMgr.getText("ArMatchingRuleDetail.AliasId", TradePortalConstants.TEXT_BUNDLE) + " ','BuyerIdAlias')
				</script>
			</button>
  <% } else {%>
					&nbsp;
  <% }		%>