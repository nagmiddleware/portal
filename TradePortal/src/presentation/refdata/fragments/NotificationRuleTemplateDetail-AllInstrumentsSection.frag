<%--
*******************************************************************************
       Notification Rule Detail - All Instrumnet setting section
  
Description:    This jsp simply displays the html for the All Instrumnet setting 
		of the NotificationRuleDetail.jsp.  This page is called
		from NotificationRuleDetail.jsp  and is called by:


*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 ******************************************************************************
--%>
<%		
	String userEmailValues = "";
	String tempValue;
	String defSendNotifSetting = "";
	String defSendEmailSetting = "";
	String defNotifEmailFerquency = "";
	String defApplytoAllTransBtn = "";
	String defClearAllTransBtn = "";
	
	if (getDataFromDoc){
		DocumentHandler doc2 = defaultDoc.getComponent("/In");
		defListVector = doc2.getFragments("/NotificationRuleTemplate/DefaultList");
		System.out.print("defListVector size: "+defListVector.size());
	}
	//Below Loop is to generate sections for all Instrument Catagories except MailMessages, Supplier Portal Invoices and Host to Host Invoice/Credit Note Approvals
	for(iLoop = 1; iLoop <= 31; iLoop++){

		
				

		if (getDataFromDoc){
			defSendNotifSetting = "";
			defSendEmailSetting = "";
			defApplytoAllTransBtn = "";
			defClearAllTransBtn = "";		

			Vector defListAllVector =  defaultDoc.getComponent("/In").getFragments("/NotificationRuleTemplate/DefaultList("+iLoop+")");			

			if(defListAllVector !=null && defListAllVector.size()>0){
			DocumentHandler notifyRuleDefInstrDoc = (DocumentHandler) defListAllVector.elementAt(0);
			//System.out.println("notifyRuleDefInstrDoc:"+notifyRuleDefInstrDoc);
			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_notif_setting"))){
				defSendNotifSetting = notifyRuleDefInstrDoc.getAttribute("/send_notif_setting");
			}

			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/send_email_setting"))){
				defSendEmailSetting = notifyRuleDefInstrDoc.getAttribute("/send_email_setting");
			}

			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran"))){
				defApplytoAllTransBtn = notifyRuleDefInstrDoc.getAttribute("/apply_to_all_tran");
			}

			if(StringFunction.isNotBlank(notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran"))){
				defClearAllTransBtn = notifyRuleDefInstrDoc.getAttribute("/clear_to_all_tran");
			}
			}
		}
%> 
		<%=widgetFactory.createSectionHeader(""+(iLoop+1), "NotificationRuleDetail."+sectionHeaderLableArray[iLoop-1], null, true) %>       
  		<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail."+sectionNoteLableArray[iLoop-1]) %>	
    	<table width="100%">
    		<tr>
    			<td width="20%">
    				<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendPortalNotificatios") %>
    			</td>
    			<td width="18%">
    				<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
    			</td>
    			<td width="15%">
				
				</td>
				<td width="37%">
				
				</td>
    		</tr>
    		<tr>
    			<td width="20%" style="vertical-align: top;">
    				<%=widgetFactory.createRadioButtonField("notifySetting"+iLoop,"notifySettingAlways"+iLoop,
    						"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,
    						TradePortalConstants.NOTIF_RULE_ALWAYS.equals(defSendNotifSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("notifySetting"+iLoop,"notifySettingNone"+iLoop,
    						"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,
    						TradePortalConstants.NOTIF_RULE_NONE.equals(defSendNotifSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("notifySetting"+iLoop,"notifySettingChrgsDocsOnly"+iLoop,
    						"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,
    						TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(defSendNotifSetting),isReadOnly)%>						
    			</td>
 
    			<td width="18%" style="vertical-align: top;">
    				<%=widgetFactory.createRadioButtonField("emailSetting"+iLoop,"emailSettingAlways"+iLoop,
    						"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,
    						TradePortalConstants.NOTIF_RULE_ALWAYS.equals(defSendEmailSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("emailSetting"+iLoop,"emailSettingNone"+iLoop,
    						"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,
    						TradePortalConstants.NOTIF_RULE_NONE.equals(defSendEmailSetting), isReadOnly)%>
    				<br>
    				<%=widgetFactory.createRadioButtonField("emailSetting"+iLoop,"emailSettingChrgsDocsOnly"+iLoop,
    						"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,
    						TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY.equals(defSendEmailSetting),isReadOnly)%>						
					
    			</td>
 					
 				<td width="15%" style="vertical-align: top;">
		    		
				</td>
 					
    			<td width="37%" align="left" style="vertical-align: top;">
					<button data-dojo-type="dijit.form.Button"  name="ApplytoAllTransactions<%=iLoop%>" id="ApplytoAllTransactions<%=iLoop%>" type="button">
  						<%=resMgr.getText("NotificationRuleDetail.ApplytoAllTransactions.Button",TradePortalConstants.TEXT_BUNDLE)%>
  						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.applyToAllTransactions(<%=iLoop%>, <%=instrTransCriteriaFieldStartCount[iLoop-1]%>);
						</script>
    				</button>
    				<br>
    				<button data-dojo-type="dijit.form.Button"  name="Override<%=iLoop%>" id="Override<%=iLoop%>" type="button">
  						<%=resMgr.getText("NotificationRuleDetail.Override.Button",TradePortalConstants.TEXT_BUNDLE)%>
  						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						local.overrideTransactionsSection(<%=iLoop%>,<%=instrTransCriteriaFieldStartCount[iLoop-1]%>,'<%=sectionDialogTitleArray[iLoop-1]%>','ALL');
						</script>
    				</button>
    				<button data-dojo-type="dijit.form.Button"  name="ClearAll<%=iLoop%>" id="ClearAll<%=iLoop%>" type="button">
  						<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
  						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
							local.clearInstrCategorySection(<%=iLoop%>, <%=instrTransCriteriaFieldStartCount[iLoop-1]%>);
						</script>
    				</button>
				</td>
				<input type="hidden" name="instrumentType<%=iLoop%>" id="instrumentType<%=iLoop%>" value="<%=instrumentCatagory[iLoop-1]%>">
				<input type="hidden" name="applyInst<%=iLoop%>" id="applyInst<%=iLoop%>" value="<%=defApplytoAllTransBtn%>">
				<input type="hidden" name="clearAll<%=iLoop%>" id="clearAll<%=iLoop%>" value="<%=defClearAllTransBtn%>">

			<%
        		
   			%>
    		</tr>			
    	</table> 
		<%
			transArray = instrTransArrays[iLoop-1];
		String instrValue = "";	
        		for(int tLoop = 0; tLoop < transArray.length; tLoop++){
        			String key;
	   			    key = instrumentCatagory[iLoop-1]+"_"+transArray[tLoop];
	
	   			    NotificationRuleCriterionWebBean notifyRuleCriterion = null;
	   			    notifyRuleCriterion = (NotificationRuleCriterionWebBean)criterionRuleTransList.get(key);
	   			    yy++;
	   			 	if (notifyRuleCriterion != null) {
						instrValue=notifyRuleCriterion.getAttribute("instrument_type_or_category");
	   		      		//load using OID
	 					out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + notifyRuleCriterion.getAttribute("instrument_type_or_category") + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + notifyRuleCriterion.getAttribute("transaction_type_or_action") + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='" + notifyRuleCriterion.getAttribute("criterion_oid") + "'>");
	   		      		out.print("<input type=hidden name='send_notif_setting"+yy+"' id='send_notif_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_notif_setting") + "'>");
	   		      		out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value='" + notifyRuleCriterion.getAttribute("send_email_setting") + "'>");
	   		      	} else {//if it is a new form
		   		    	out.print("<input type=hidden name='instrument_type_or_category"+yy+"' id='instrument_type_or_category"+yy+"' value='" + instrumentCatagory[iLoop-1] + "'>");
						out.print("<input type=hidden name='transaction_type_or_action"+yy+"' id='transaction_type_or_action"+yy+"' value='" + transArray[tLoop] + "'>");
		   		      	out.print("<input type=hidden name='criterion_oid"+yy+"' id='criterion_oid"+yy+"' value='0'>");		   		     	
		   		      	out.print("<input type=hidden name='send_notif_setting"+yy+"' id='send_notif_setting"+yy+"' value=''>");
		   		      	out.print("<input type=hidden name='send_email_setting"+yy+"' id='send_email_setting"+yy+"' value=''>");
		   		      	 	
		   		    }
				}//ent tLoop

				%>
		<table width="100%">
					<% if(dataList.contains(instrValue)) {%>
						<span class="availNotifyItemSelect">
							<%=resMgr.getText("NotificationRuleDetail.DataExists",TradePortalConstants.TEXT_BUNDLE)%>
						</span>
					<% } %>
    	</table>
    </div>
<%          
	}//end of for
%>