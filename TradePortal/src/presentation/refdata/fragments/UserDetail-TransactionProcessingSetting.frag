<%--
*******************************************************************************
            User Detail - Transaction Processing Setting Section

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>
<%=widgetFactory.createCheckboxField("ConfidentialPaymentInd","UserDetail.AccessToConfidentialPayment",
		TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("confidential_indicator")),isReadOnly)%>
<div class="formItemWithIndent2">
	<%=widgetFactory.createNote("UserDetail.AccessToConfidentialPaymentNote")%>
	
</div>
<br>
<%=widgetFactory.createCheckboxField("LiveMarketRateInd","UserDetail.LiveMarketRateInd",
		TradePortalConstants.INDICATOR_YES.equals(user.getAttribute("live_market_rate_ind")),isReadOnly)%>
<div class="formItemWithIndent2">
	<%=widgetFactory.createNote("UserDetail.LiveMarketRateNote")%>
</div>			