<%
         // IR MMUJ021661361 Change Begin
    	 String newSearch = request.getParameter("NewSearch");
         String filterBuyerName = null;
         String filterBuyerId = null;
       	 //123456 Add Begin
         String searchListViewName = "ARMMatchingRuleListView.xml"; //123456
         String DELIMITER = "@"; 
        StringBuffer newSearchCriteria = new StringBuffer();
       	 //123456 Add end
         // Retrieve the search criteria from the request if we are coming from a form submission.
         // Otherwise get the previous search criteria from the session object (e.g. coming back from party details).
         if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
         {
            filterBuyerName = request.getParameter("BuyerName");
   	 		filterBuyerId = request.getParameter("BuyerId");
   	 	  }
        
     
        	  if (filterBuyerName != null)  filterBuyerName = filterBuyerName.toUpperCase();
              else filterBuyerName = "";

              if (filterBuyerId != null)  filterBuyerId = filterBuyerId.toUpperCase();
              else filterBuyerId = "";
        	  
        	  newSearchCriteria.append("BuyerName=" + EncryptDecrypt.stringToBase64String(filterBuyerName) + DELIMITER);
        	  newSearchCriteria.append("BuyerId=" + EncryptDecrypt.stringToBase64String(filterBuyerId) );	
          
         /*************************************************************
          * BuyerFilterForm
          * This form contains the filter text field and submit button to filter parties
          *************************************************************/
         %>

<%-- JavaScript for Instrument History tab to enable form submission by enter.
           		 event.which works for NetScape and event.keyCode works for IE  --%>
	        <script LANGUAGE="JavaScript">
	         function filterTradingPartnerRulesOnEnter(fieldID){
	    		require(["dojo/on","dijit/registry"],function(on, registry) {
	    		    on(registry.byId(fieldID), "keypress", function(event) {
	    		        if (event && event.keyCode == 13) {
	    		        	dojo.stopEvent(event);
	    		        	searchARMRules();
	    		        }
	    		    });
	    		});	<%-- end require --%>
	    	}<%-- end of filterTradingPartnerRulesOnEnter() --%>
      		
        	    function enterSubmit(event, myform) {
            		if (event && event.which == 13) {
               			setButtonPressed('null', 0);
               			myform.submit();
            		}
		            else if (event && event.keyCode == 13) {
        		       setButtonPressed('null', 0);
               		   myform.submit();
            		}
            		else {
               			return true;
            		}
         		}
<%--  IR MMUJ021661361 Add End   --%>
      
      </script>
          
          <input type="hidden" name="NewSearch" value="Y">  

  <div class="searchDivider"></div>
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
       <%=widgetFactory.createSearchTextField("BuyerName", "RefDataReceivablesMatchingRules.BuyerName", "20","onKeydown='Javascript: filterTradingPartnerRulesOnEnter(\"BuyerName\");'") %>
      &nbsp;&nbsp;<%=widgetFactory.createSearchTextField("BuyerId", "RefDataReceivablesMatchingRules.BuyerId", "20","onKeydown='Javascript: filterTradingPartnerRulesOnEnter(\"BuyerId\");'") %>
    </span>
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="searchParty">
      <%= resMgr.getText("common.search", TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchARMRules();</script>
      </button>
       <%=widgetFactory.createHoverHelp("searchParty", "SearchHoverText") %>
    </span>
    <div style="clear:both;"></div>
  </div>

	<%
		//ssikhakolli - Rel 9.1 07/09/2014 - DGrid Migration task - Begin
		gridHtml = dGridFactory.createDataGrid("ARMMatchingRuleDataGridId","ARMMatchingRuleDataGrid", null);
	  	gridLayout = dGridFactory.createGridLayout("ARMMatchingRuleDataGrid");
	  	//ssikhakolli - Rel 9.1 07/09/2014 - DGrid Migration task - End
	  	
	  	initSearchParms = "";	      
	%>
	
	<%=gridHtml%>
