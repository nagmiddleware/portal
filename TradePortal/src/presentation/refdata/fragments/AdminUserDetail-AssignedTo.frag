<div class="columnLeft"> 
	<%
          options = ListBox.createOptionList(adminSecProfileDoc, 
                                   "SECURITY_PROFILE_OID", "NAME", 
                                   user.getAttribute("security_profile_oid"),
					     userSession.getSecretKey());
          if (insertMode) {
            defaultText = resMgr.getText("AdminUserDetail.selectProfile",TradePortalConstants.TEXT_BUNDLE);
          } else {
            defaultText = "";
          }
       //   out.println(InputField.createSelectField("SecurityProfile", "",  defaultText, options, "ListText", isReadOnly));
	%>
	<%=widgetFactory.createSelectField( "SecurityProfile", "AdminUserDetail.AdminSecurityProfile",defaultText, options, isReadOnly, true, false, "", "", "")  %> 
	<%
      out.println("<input type=hidden name=OwnerOrgAndLevel value=" + EncryptDecrypt.encryptStringUsingTripleDes(orgAndLevel, userSession.getSecretKey()) + ">");
	%> 
        <%--  <%= widgetFactory.createLabel("", "AdminUserDetail.Organization", false, true, false, "") %>
        <%//=resMgr.getText("AdminUserDetail.Organization", TradePortalConstants.TEXT_BUNDLE)%> --%>
        
     
     <%=widgetFactory.createTextField("","AdminUserDetail.Organization",StringFunction.xssCharsToHtml(orgName),"35",true,false,false,"","","")%>   
       


</div>
<div class="columnRight"> 

<%
          options = Dropdown.createSortedRefDataOptions("TIMEZONE", user.getAttribute("timezone"), loginLocale);
          if (insertMode) {
            defaultText = resMgr.getText("AdminUserDetail.selectTimeZone",
                                       TradePortalConstants.TEXT_BUNDLE);
          } else {
            defaultText = "";
          }
         // out.println(InputField.createSelectField("TimeZone", "", defaultText,  options, "ListText",isReadOnly));
%>
<%=widgetFactory.createSelectField( "TimeZone", "AdminUserDetail.TimeZone",defaultText, options, isReadOnly, true, false, "class='char21'", "", "")  %> 
<%-- pgedupudi - Rel-9.3 CR-976A 03/26/2015 Start--%>
<%-- IR#T36000039962 pgedupudi - Rel-9.3 CR-976A 06/04/2015 --%>
<%
          if(ownershipLevel.equals(TradePortalConstants.OWNER_BANK) ){
          options = ListBox.createOptionList(bnkGrpRestrictRuleDoc,"BANK_GRP_RESTRICT_RULES_OID","NAME",user.getAttribute("bank_grp_restrict_rule_oid"),userSession.getSecretKey());
          if (insertMode) {
            defaultText = resMgr.getText("AdminUserDetail.selectBankGrpRestrictRule",
                                       TradePortalConstants.TEXT_BUNDLE);
          } else {
            defaultText = resMgr.getText("AdminUserDetail.selectBankGrpRestrictRule",
                                       TradePortalConstants.TEXT_BUNDLE);
          }
%>
<%=widgetFactory.createSelectField( "BankGrpRestrictRule", "AdminUserDetail.BankGrpRestrictRule",defaultText, options, isReadOnly, false, false, "class='char21'", "", "")  %>
<%}%>
<%-- pgedupudi - Rel-9.3 CR-976A 03/26/2015 End--%>
</div>