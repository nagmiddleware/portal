<%
		
	secureParams.put("buyer_name", editArMatchingRule.getAttribute("buyer_name"));
	
	//String instrumentType            = "";
	String currencyOptions="";
	//instrumentType = editArMatchingRule.getAttribute("instrument_type");
	String  bank_def_radio=null;
	 boolean  defaultRadioButtonBankDef1	  =	false;
     boolean  defaultRadioButtonBankDef2	  =	false;
	 String currencyType ="";
	 String insType="";

	String defaultText1="";
	String instrumentOptions="";
	String rateOptions="";
	String defaultText2="";
	String rateType ="";
	String daysHtmlProps="";
%>

 <%=widgetFactory.createWideSubsectionHeader("TradingPartnerDetails.InvoicesUploadRules") %>
 <div class ="columnLeft">
  <%= widgetFactory.createLabel("TradingPartnerDetails.CalInterAndDisct","TradingPartnerDetails.CalInterAndDisct",isReadOnly, false, false, "") %>
  <div class="formItem">
   <%
			bank_def_radio=editArMatchingRule.getAttribute("payment_day_allow");
			
			if(bank_def_radio.equals(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_YES)){
				defaultRadioButtonBankDef1=true;
				defaultRadioButtonBankDef2=false;
				}
			else if(bank_def_radio.equals(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO)){
				defaultRadioButtonBankDef1=false;
				defaultRadioButtonBankDef2=true;
			} else {
				defaultRadioButtonBankDef2=true;
			}
                                                      
           out.print(widgetFactory.createRadioButtonField("payment_day_allow", 
                                     "TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_YES", "TradingPartnerDetails.AllowPayDate", TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_YES, defaultRadioButtonBankDef1, 
                                      isReadOnly, 
                                      "onClick=\"disableDaysBeforeAfter(false)\"", "")); 
           %> 
          
           <div style="clear: both;"></div>
           <% 
           out.print(widgetFactory.createRadioButtonField("payment_day_allow", 
                   "TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO", "TradingPartnerDetails.AllowDueDate", TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO, defaultRadioButtonBankDef2, 
                    isReadOnly, 
                    "onClick=\"disableDaysBeforeAfter(true)\"", ""));  
           %>
            </div>
  <div style="clear: both;"></div>
  <%--
       <%= widgetFactory.createLabel("","TradingPartnerDetails.RecInvFinOffered",isReadOnly, false, false, "") %>--%>
       <div class="formItem">
       <%= widgetFactory.createNumberField( "days_finance_of_payment", "TradingPartnerDetails.RecInvFinOffered", editArMatchingRule.getAttribute("days_finance_of_payment"), "3", 
		                     isReadOnly, false, false,"style='width:4em'","","none") %>
		                      <%=resMgr.getText("TradingPartnerDetails.DayBeforePayAndDue", TradePortalConstants.TEXT_BUNDLE)%>
	 </div>
 </div>
 
 <div class="columnRight">
  <div class="formItem">
   <%
			bank_def_radio=editArMatchingRule.getAttribute("payment_day_allow");
			if(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_YES.equals(bank_def_radio)){
			}else{
				daysHtmlProps = "disabled";
			}
			%>
   <%=resMgr.getText("TradingPartnerDetails.PayDateCanbe", TradePortalConstants.TEXT_BUNDLE)%>
   <br/>
   <br/>
   
   
     <%= widgetFactory.createNumberField( "days_before", "", editArMatchingRule.getAttribute("days_before"),"3", 
    		 isReadOnly, false, false, daysHtmlProps, "","none" ) %> 
		                     
		    <%=resMgr.getText("TradingPartnerDetails.DayBeforeOr", TradePortalConstants.TEXT_BUNDLE)%>
	
	<div style="clear: both;"></div>
	 <br/>               	                     
    
		 <%= widgetFactory.createNumberField( "days_after","", editArMatchingRule.getAttribute("days_after"), "3", 
		                     isReadOnly,false,false, daysHtmlProps, "","none") %>   
		                     
		<%=resMgr.getText("TradingPartnerDetails.DayAfterDueDate", TradePortalConstants.TEXT_BUNDLE)%>
		<br/>
    
 <br/>
    
     <%= widgetFactory.createPercentField( "invoice_value_finance", "TradingPartnerDetails.InvToFin", editArMatchingRule.getAttribute("invoice_value_finance"), 
		                     isReadOnly,  false,false,"", "","none" ) %> 
		                     
		                     
		<%=resMgr.getText("TradingPartnerDetails.InvValToFin", TradePortalConstants.TEXT_BUNDLE)%>  
  
    </div>
 </div>
 
 <div style="clear: both;"></div>

 <%-- Narayan CR 913 Rel9.0.0.0 23-Jan-2014 End --%>
 <%=widgetFactory.createWideSubsectionHeader("PayableInvPayInstr.PayablesInvPayInstr") %>
 <%= widgetFactory.createLabel("PayableInvPayInstr.PayablesInvPayInstrDescr","PayableInvPayInstr.PayablesInvPayInstrDescr",isReadOnly, false, false, "") %>
 <div class="formItem">
 <table class="formDocumentsTable" style="width:50%" id="payableInvPayInstTable">
      <thead>
            <tr>
                <th colspan="3" align="left" class="genericCol"><%= resMgr.getText("PayableInvPayInstr.InvPayInstr", TradePortalConstants.TEXT_BUNDLE) %></th>
            </tr>
            <tr>
            	  <th>&nbsp;</th>
                  <th><%= resMgr.getText("PayableInvPayInstr.Currency", TradePortalConstants.TEXT_BUNDLE) %></th>
                  <th><%= resMgr.getText("PayableInvPayInstr.InvPayInstrExist", TradePortalConstants.TEXT_BUNDLE) %></th>
           </tr> 
           <input type=hidden value="<%=payInvRowCount%>" name="numberOfMultipleObjects3" id="payInvNumOfRows">          
      </thead>
      <tbody>
		<%	
		    String payableInvPayInstOid = null;
		    for (mLoop=0; mLoop < payableInvPayInstList.size(); mLoop++) { 
		    	payableInvPayInst = (PayableInvPayInstWebBean)payableInvPayInstList.get(mLoop);
		%>
		<tr id="payableInvPayInstIndex<%=mLoop%>">
				<td align="center"><%=mLoop+1%></td>
				<td align="center">
				 <% currencyOptions = Dropdown.createSortedCurrencyCodeOptions(payableInvPayInst.getAttribute("currency_code"), resMgr.getResourceLocale()); 
	  		  	 out.println(widgetFactory.createSelectField("PayCurrencyCode" + mLoop, "", " ", currencyOptions, isReadOnly, false, false, "class='char5'", "", "none"));
	  		  	payableInvPayInstOid = payableInvPayInst.getAttribute("payable_inv_pay_inst_oid");
	  		  	 if(StringFunction.isNotBlank(payableInvPayInstOid)){
	  		  		payableInvPayInstOid =  EncryptDecrypt.encryptStringUsingTripleDes(payableInvPayInstOid, userSession.getSecretKey());
	  		  	 }
			     out.print("<INPUT TYPE=HIDDEN NAME='PayableInvPayInstOid" + mLoop + "' VALUE='" + StringFunction.xssCharsToHtml(payableInvPayInstOid)+ "'>");
			  	%>
			  	</td>
			  	<td align="center">
			  	<%= widgetFactory.createCheckboxField("InvPayInstructionInd" + mLoop, "",
			  			TradePortalConstants.INDICATOR_YES.equals(payableInvPayInst.getAttribute("inv_pay_instruction_ind")), isReadOnly, false, "", "", "none")%>
				</td>
				
        	  <%} %>
         </tr>
        </tbody>
       </table>
       <br>
       <% if (!isReadOnly) {%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MorePayableInstr">
			<%= resMgr.getText("common.Add4MoreText", TradePortalConstants.TEXT_BUNDLE) %>
				 <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                      add4MorePaybaleInvPayInstr();  
                 </script>
			</button>
			<%=widgetFactory.createHoverHelp("add4MorePayableInstr","PayableInvPayInstr.Add4More") %>
		<% } else {	%>
			&nbsp;
		<% } %>
        </div>
  <div style="clear: both;"></div>
  <%-- Narayan CR 913 Rel9.0.0.0 23-Jan-2014 End --%>
 <%=widgetFactory.createWideSubsectionHeader("TradingPartnerDetails.TPSCustomerIDHeader") %>
 <div class="formItem">
 <%-- Leelavathi IR#T36000017451 05/24/2013 Begin --%>
 	
 	<%= widgetFactory.createTextField( "tps_customer_id", "TradingPartnerDetails.TPSCustomerID", editArMatchingRule.getAttribute("tps_customer_id"), "20", isReadOnly,false,false, "","","none" ) %>
 	<%-- Leelavathi IR#T36000017451 05/24/2013 End --%>
<br/>
</div>
<div style="clear: both;"></div>
<div class="formItem">
 <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable" id="tradeMarginTable">
      <thead>
            <tr>
                <th colspan="6" align="left" class="genericCol"><%= resMgr.getText("TradingPartnerDetails.BankDefinedRulesMarginRules", TradePortalConstants.TEXT_BUNDLE) %></th>
            </tr>
            <tr>
            	  <th>&nbsp;</th>
                  <th class="genericCol"><%= resMgr.getText("TradingPartnerDetails.BankDefinedRulesCurrency", TradePortalConstants.TEXT_BUNDLE) %></th>
                  <th class="genericCol"><%= resMgr.getText("TradingPartnerDetails.BankDefinedRulesInstrumentType", TradePortalConstants.TEXT_BUNDLE) %></th>
                  <th class="genericCol"><%= resMgr.getText("TradingPartnerDetails.BankDefinedRulesThreshold", TradePortalConstants.TEXT_BUNDLE) %></th>
                  <th class="genericCol"><%= resMgr.getText("TradingPartnerDetails.BankDefinedRulesRateType", TradePortalConstants.TEXT_BUNDLE) %></th>
                  <th class="genericCol"><%= resMgr.getText("TradingPartnerDetails.BankDefinedRulesMargin", TradePortalConstants.TEXT_BUNDLE) %></th>
           </tr>
           <input type=hidden value="<%=row2Count%>" name="numberOfMultipleObjects2" id="numberOf2Rows">
      </thead>
      <tbody>
		<%	
		    String tradePartnerMarginOid;
		    for (mLoop=0; mLoop < tradePartnerMarginList.size(); mLoop++) { 
				tradePartnerMargin = (TradePartnerMarginRulesWebBean)tradePartnerMarginList.get(mLoop);
		%>
		<tr id="marginIndex<%=mLoop%>">
				<td><%=mLoop+1%></td>
				<td>
				 <% currencyOptions = Dropdown.createSortedCurrencyCodeOptions(tradePartnerMargin.getAttribute("currency_code"), resMgr.getResourceLocale()); 
	  		  	 out.println(widgetFactory.createSelectField("CurrencyCode" + mLoop, "", " ", currencyOptions, isReadOnly, false, false, "class='char5' onChange=\"enableMarginThresholdAmt("+(mLoop)+")\"", "", "none"));
	  		  	tradePartnerMarginOid = tradePartnerMargin.getAttribute("trade_margin_rule_oid");
	  		  	 if(InstrumentServices.isNotBlank(tradePartnerMarginOid)){
	  		  		tradePartnerMarginOid =  EncryptDecrypt.encryptStringUsingTripleDes(tradePartnerMarginOid, userSession.getSecretKey());
	  		  	 }
			     out.print("<INPUT TYPE=HIDDEN NAME='TraderMarginAliasoid" + mLoop + "' VALUE='" + tradePartnerMarginOid + "'>");
			  	%>
			  	</td>
			  	<td>
			  	 <%
			  	 Vector recInstrumentVector1 = new Vector();
				   recInstrumentVector1.addElement(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT);
				  
			  	  instrumentOptions = Dropdown.createSortedRefDataIncludeOptions( TradePortalConstants.INSTRUMENT_TYPE, tradePartnerMargin.getAttribute("margin_instrument_type"),resMgr, resMgr.getResourceLocale(), recInstrumentVector1,true );
           		  out.print(widgetFactory.createSelectField("MarginInstrumentType" + mLoop, "", " ", instrumentOptions, isReadOnly, false, false, "class='char25'", "", "none"));
        		%>
				</td>
				<td>
				<% String thresholdAmt="";
	    			 if(InstrumentServices.isNotBlank(tradePartnerMargin.getAttribute("threshold_amount")) && InstrumentServices.isNotBlank(tradePartnerMargin.getAttribute("currency_code"))) {
							thresholdAmt = TPCurrencyUtility.getDisplayAmount(
									tradePartnerMargin.getAttribute("threshold_amount"), 
									tradePartnerMargin.getAttribute("currency_code"), resMgr.getResourceLocale());			
			
		 			} else {
							thresholdAmt = tradePartnerMargin.getAttribute("threshold_amount");
					}

	    			    String thresholdAmtHtmlProps = "";
	    			    if (InstrumentServices.isBlank(tradePartnerMargin.getAttribute("currency_code"))) {
	    			      thresholdAmtHtmlProps = " disabled=\"disabled\"";
	    			    }
	    			 
	  		   %>	 
	  		  <%= widgetFactory.createAmountField("ThresholdAmount" + (mLoop),"", thresholdAmt,"", isReadOnly,false, false, "class='char20'"+thresholdAmtHtmlProps, "","none") %>
	  		  </td>
	  		  <td>
	  		  <%
	  		    rateOptions = Dropdown.createSortedRefDataOptions("REFINANCE_RATE_TYPE", tradePartnerMargin.getAttribute("rate_type"), resMgr.getResourceLocale());
           		out.print(widgetFactory.createSelectField("RateType" + mLoop, "", " ", rateOptions, isReadOnly, false, false, "class='char6'", "", "none"));
        	  %>
        	  </td>
        	  <td>
        	  <%= widgetFactory.createNumberField("Margin" + mLoop, "", tradePartnerMargin.getAttribute("margin"), "", isReadOnly, false, false, "class='char2'", "constraints:{min:0,max:999,pattern:'###.######'}", "none") %>
        	  
        	  </td>
        	  </tr>
        	  <%} %>
        </tbody>
       </table>
  	   <br>
		<% if (!(isReadOnly)) {		%>
			<button data-dojo-type="dijit.form.Button" type="button" id="add4MoreMargin">
			<%-- Jyothikumari.G 07/07/2013 Rel8.2 IR-T36000016914 Start --%>
			<%-- I added the below line for the button Add4MoreRules as per IR-T36000016914 --%>
			<%= resMgr.getText("common.Add4MoreRules", TradePortalConstants.TEXT_BUNDLE) %>
			<%-- Jyothikumari.G 07/07/2013 Rel8.2 IR-T36000016914 End --%>
				 <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                        add4MoreTradeRules();
                 </script>
			</button>
			<%=widgetFactory.createHoverHelp("add4MoreMargin","POStructure.Add4More") %>
		<% } else {		%>
					&nbsp;
		<% }		%>
        </div>
				
<script>


function add4MoreTradeRules() {
	
	require([ "dojo/_base/array", "dojo/dom", "dojo/dom-construct",
				"dojo/dom-class", "dojo/dom-style", "dojo/query",
				"dijit/registry", "dojo/on", "t360/common", "t360/dialog",
				"dojo/ready", "dojo/NodeList-traverse" ], function(arrayUtil,
				dom, domConstruct, domClass, domStyle, query, registry, on,
				common, dialog, ready) {
	var table = dom.byId('tradeMarginTable');
	var insertRowIdx = table.rows.length;
	var lastElement = parseInt(insertRowIdx)-1;
	 common.appendAjaxTableRows(table,"marginIndex","/portal/refdata/TradeMarginRows.jsp?marginIndex="+(lastElement));
	});
}

function enableMarginThresholdAmt(marginIdx){
    require(["dijit/registry"],
		  	function(registry) {
    	 var thrCurWidget = registry.byId("CurrencyCode"+marginIdx);
    	    var thrAmtWidget = registry.byId("ThresholdAmount"+marginIdx);
    	    if ( thrCurWidget && thrAmtWidget ) {
    	      var thrCur = thrCurWidget.get('value');
    	      if ( thrCur && thrCur.length>0 ) {
    	        thrAmtWidget.set('disabled',false);
    	      }
    	      else {
    	        thrAmtWidget.set('disabled',true);
    	      }
    	    }
	}); 
}


function disableDaysBeforeAfter(disable) {
	   require(["dijit/registry"],
			  	function(registry) {
		   var dBefore = registry.byId('days_before');
		    var dAfter = registry.byId('days_after');
		   
	   if (disable) {
		   if(dBefore){
     
       dBefore.set("disabled",true);
       dBefore.set("value","");
		   }
		   if(dAfter){
       dAfter.set("disabled",true);
       dAfter.set("value","");
		   }
    }
   else {
	 
	   if(dBefore){
       dBefore.set("disabled",false);
	   }
	   if(dAfter){
       dAfter.set("disabled",false);
	   }
     
   }
	   }); 
}

</script>