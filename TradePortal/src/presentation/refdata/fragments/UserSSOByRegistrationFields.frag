<%--
*******************************************************************************
                   User SSO By Registration Fields

  Description:
     This fragment is used to construct the SSO By Registration fields for
  the User Detail page.  These fields are only displayed for corporate users.
*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<% if (!isAdminUser) { %>
  <% if (TradePortalConstants.AUTH_PERUSER.equals(corpAuthMethod)) { %>     		
  <%= widgetFactory.createRadioButtonField("AuthenticationMethod","TradePortalConstants.AUTH_REGISTERED_SSO","UserDetail.UseSSOByRegistration",	TradePortalConstants.AUTH_REGISTERED_SSO,	TradePortalConstants.AUTH_REGISTERED_SSO.equals(userAuthMethod),	isReadOnly)%><br/>
  <% } else if (!TradePortalConstants.AUTH_REGISTERED_SSO.equals(corpAuthMethod)) {%>    
  <% }

   boolean isRegistered = InstrumentServices.isNotBlank(user.getAttribute("registered_sso_id")); 
  String userRegistered = user.getAttribute("registered_sso_id");
  //AiA - initialize to equivalent of false
  //IR#T36000015711 - 07/17/2013 - START 
  String registered="X";
  if (isRegistered) {
    registered = "A";
  }
  else {
    registered = "X";
  } 
//IR#T36000015711 - END 
  String newRegistrationPassword = doc.getAttribute("/In/User/newRegistrationPassword");
  if (newRegistrationPassword == null ) {
    newRegistrationPassword = "";
  } 
  %>
  <%--cquinton 12/6/2012 restore correct styling for readonly fields. wrap all in some indenting instead--%>
  <div class="indentHalf">
    <%--SPenke T36000006206 10-10-2012 Begin --%>
    <%= widgetFactory.createTextField( "RegistrationLoginID", "UserDetail.RegistrationLoginId", user.getAttribute("registration_login_id"), "32", 
          isReadOnly, false, false,  "class='char20'", "","" ) %>

<%-- Do not show the password section if in read only mode --%>
<% if (!isReadOnly) { %>  
    <%= widgetFactory.createTextField( "NewRegistrationPassword", "UserDetail.RegistrationPassword", newRegistrationPassword, "", isReadOnly, false, false,  "type=\"password\" AutoComplete=\"off\"class='char20'", "","" ) %>
<% } %>
    <%--SPenke T36000006206 10-10-2012 End --%>  
   	    
    <div class='formItem<%=isReadOnly?" readOnly":""%>'>    
      <%= widgetFactory.createLabel("", "UserDetail.Registered", isReadOnly, false, false, "none") %>
      <br/>
      <%--AiA 
      IR#T36000015711 - 07/17/2013 - Registered check box value 
     
      <%= widgetFactory.createRadioButtonField("RegisteredSSOIdType","RegisteredSSOId-Yes","common.Yes",TradePortalConstants.INDICATOR_A,	TradePortalConstants.INDICATOR_A.equals(userRegistered),	isReadOnly)%><br/>      
      <%= widgetFactory.createRadioButtonField("RegisteredSSOIdType","RegisteredSSOId-No","common.No",	TradePortalConstants.INDICATOR_X,	TradePortalConstants.INDICATOR_X.equals(userRegistered),	isReadOnly)%>
       --%>
      <%= widgetFactory.createRadioButtonField("RegisteredSSOIdType","RegisteredSSOId-Yes","common.Yes",TradePortalConstants.INDICATOR_A,	TradePortalConstants.INDICATOR_A.equals(registered),	isReadOnly)%><br/>      
      <%= widgetFactory.createRadioButtonField("RegisteredSSOIdType","RegisteredSSOId-No","common.No",	TradePortalConstants.INDICATOR_X,	TradePortalConstants.INDICATOR_X.equals(registered),	isReadOnly)%>
 <br>
        <%-- 
        Unregister button to display if user is registered
        --%>		
<% 
   if (isRegistered) {
%>
  
    <button data-dojo-type="dijit.form.Button"  name="UnregisterAccount" id="UnregisterAccount" type="button" data-dojo-props="iconClass:'delete'" >
      <%=resMgr.getText("common.UnregisterAccountText",TradePortalConstants.TEXT_BUNDLE)%>

      <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
        setButtonPressed('<%=TradePortalConstants.BUTTON_UNREGISTER_ACCOUNT%>', '0');
        document.forms['UserDetailForm-UnregisterUserAccount'].submit();
      </script>

    </button> 
   
  <%=widgetFactory.createHoverHelp("SaveCloseButton","SaveCloseHoverText") %> 
<%
  }
%>
 
      <%--
      IR#T36000015711 end  
        --%>        
    </div>
          
  </div> 

<% }// if (TradePortalConstants.OWNER_CORPORATE.equals(user.getAttribute("ownership_level"))) %>
