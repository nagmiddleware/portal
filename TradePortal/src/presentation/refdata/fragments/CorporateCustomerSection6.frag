<%--this fragment is included in CorporateCustomerDetail.jsp --%>


  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 30%;">
      <col span="1" style="width: 16%;">
      <col span="1" style="width: 16%;">
      <col span="1" style="width: 21%;">
      <col span="1" style="width: 16%;">
    </colgroup>
    <thead>
      <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
      <th>&nbsp;</th>
      <th>&nbsp;</th>
      <th align="center"><%=resMgr.getText("CorpCust.AuthorizeOneUser",TradePortalConstants.TEXT_BUNDLE)%></th>
      <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoUsers",TradePortalConstants.TEXT_BUNDLE)%></th>
      <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoWorkGroups",TradePortalConstants.TEXT_BUNDLE)%></th>
      <th align="center"><%=resMgr.getText("CorpCust.PanelAuth",TradePortalConstants.TEXT_BUNDLE)%></th>
    </thead>
    <thead>
      <th class="tableSubHeader withCheckBoxColumn" colspan="6"><%=resMgr.getText("CorpCust.Trade",TradePortalConstants.TEXT_BUNDLE)%></th>
    </thead>
    <tbody>
      <tr>
        <td valign="top">
<%
  if ((!insertModeFlag || errorFlag) && (corporateOrgAWBInd.equals(TradePortalConstants.INDICATOR_YES))) {
    defaultCheckBoxValue = true;

    if (corporateOrgAWBDualInd.equals("")) {
      defaultRadioButtonValueA = false;
      defaultRadioButtonValueB = false;
      defaultRadioButtonValueC = false;
      defaultRadioButtonValueD = false;
    }
    else if (corporateOrgAWBDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER)) {
      defaultRadioButtonValueA = true;
      defaultRadioButtonValueB = false;
      defaultRadioButtonValueC = false;
      defaultRadioButtonValueD = false;
    }
    else if (corporateOrgAWBDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS)) {
      defaultRadioButtonValueA = false;
      defaultRadioButtonValueB = true;
      defaultRadioButtonValueC = false;
      defaultRadioButtonValueD = false;
    }
    else if (corporateOrgAWBDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS)) {
        defaultRadioButtonValueA = false;
        defaultRadioButtonValueB = false;
        defaultRadioButtonValueC = true;
        defaultRadioButtonValueD = false;
    }
    else {
      defaultRadioButtonValueA = false;
      defaultRadioButtonValueB = false;
      defaultRadioButtonValueC = false;
      defaultRadioButtonValueD = true;
    }
  }
  else {
    defaultCheckBoxValue     = false;
    defaultRadioButtonValueA = false;
    defaultRadioButtonValueB = false;
    defaultRadioButtonValueC = false;
    defaultRadioButtonValueD = false;
  }

  out.print(widgetFactory.createCheckboxField("AirWaybillsIndicator", "",
                                              defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setAWBRadioButtons()\"", "", "none"));
%>
        </td>
        <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.AirWaybills") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgAWBCheckerAuthIndicator.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("AirWaybillsCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setAWBCheckBox()\"", "", "none") %>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("AirWaybillsDualAuthorizationIndicator",
                                                 "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                                                 isReadOnly,
                                                 "onClick=\"local.setAWBCheckBox()\"", ""));
%>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("AirWaybillsDualAuthorizationIndicator",
            "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
            isReadOnly,
            "onClick=\"local.setAWBCheckBox()\"", ""));
%>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("AirWaybillsDualAuthorizationIndicator",
            "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
            isReadOnly,
            "onClick=\"local.setAWBCheckBox()\"", ""));
%>
        </td>
        <td align="center" valign="top">
<%
	out.print(widgetFactory.createRadioButtonField("AirWaybillsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "AWB", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                  isReadOnly,
                  "onClick=\"local.setAWBCheckBox('Y')\"", "")+"<br>");
	String panelOptions = "<option  value=\"\"></option>";
	//MEer Rel 8.3 IR -19792-- change attribute awb_panel_group_oid to air_panel_group_oid
	panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","AIR_IND", corporateOrg.getAttribute("air_panel_group_oid"));
	out.print(widgetFactory.createSelectField("AirWaybillsPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setAWBPanelRadioButton()\"" , "", "none"));
%>        
        </td>
      </tr>
      <tr>
        <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgATPInd)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgATPDualInd.equals(""))
              {
                   defaultRadioButtonValueA  = false;
                   defaultRadioButtonValueB  = false;
                   defaultRadioButtonValueC  = false;
                   defaultRadioButtonValueD = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgATPDualInd))
              {
                 defaultRadioButtonValueA = true;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgATPDualInd))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgATPDualInd))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
                 defaultRadioButtonValueD = false;
              }
              else
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }

           out.print(widgetFactory.createCheckboxField("ApprovalToPayIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setATPRadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.ApprovaltoPay") %>
           <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgATPCheckerAuthIndicator.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("ApprovalToPayCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setATPCheckBox()\"", "", "none") %>
       
          <div class="tableTextShortestIndent">
            <%= widgetFactory.createSubLabel("CorpCust.ATPProcessPurchaseText")%>
            <br>
          </div>
          <%
             if ((!insertModeFlag || errorFlag) &&
                 (autoATPCreate.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }

			 out.print(widgetFactory.createCheckboxField("ApprovalToPayProcessPOUploadIndicator", "CorpCust.ATPProcessPurchaseOrders",
	                   defaultCheckBoxValue, isReadOnly, false,
	                   "onClick=\"local.setATPCheckBox()\"", "", "none"));
               %>
               <br>
               <%

             if ((!insertModeFlag || errorFlag) &&
                 (allowATPManualPOEntry.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }

			 out.print(widgetFactory.createCheckboxField("ApprovalToPayProcessPOManualIndicator", "CorpCust.ATPProcessPurchaseOrdersManual",
	                   defaultCheckBoxValue, isReadOnly, false,
	                   "onClick=\"local.setATPCheckBox()\"", "", "none"));
              %>
          <br>
	 	   <%-- Naveen 27-July-2012 IR-T36000003307 --%>
	  	   <%-- Commented the below label as it was not getting aligned with the corresponding checkbox and passed the same as parameter to above checkbox to resolve the issue	 --%>
	       <%-- <%= widgetFactory.createSubLabel("CorpCust.ATPProcessPurchaseOrdersManual") %>	 --%>

          <%--cquinton 11/14/2012 add atp invoice upload capability start--%>
          <div class="tableTextShortestIndent">
            <%= widgetFactory.createSubLabel("CorpCust.ATPProcessInvoiceText")%>
          <br>
            
          </div>
<%
  if ((!insertModeFlag || errorFlag) &&
      (TradePortalConstants.INDICATOR_YES.equals(atpProcessInvoice))) {
    defaultCheckBoxValue = true;
  }
  else {
    defaultCheckBoxValue = false;
  }

  out.print(widgetFactory.createCheckboxField("ATPProcessInvoiceIndicator", "CorpCust.ATPProcessInvoiceUpload",
            defaultCheckBoxValue, isReadOnly, false,
            "onClick=\"local.setATPCheckBox()\"", "", "none"));
%>
<br>
          <%--cquinton 11/14/2012 add atp invoice upload capability end--%>

        </td>
      <td align="center" valign="top">
          <%
          out.print(widgetFactory.createRadioButtonField("ApprovalToPayDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "ATP", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                  isReadOnly,
                  "onClick=\"local.setATPCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
          <%
           out.print(widgetFactory.createRadioButtonField("ApprovalToPayDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "ATP", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                  isReadOnly,
                  "onClick=\"local.setATPCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
          <%
           out.print(widgetFactory.createRadioButtonField("ApprovalToPayDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "ATP", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                  isReadOnly,
                  "onClick=\"local.setATPCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
      <%
      	out.print(widgetFactory.createRadioButtonField("ApprovalToPayDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "ATP", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                  isReadOnly,
                  "onClick=\"local.setATPCheckBox()\"", "")+"<br>");
      	panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","ATP_IND",corporateOrg.getAttribute("atp_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("ApprovalToPayPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setATPPanelRadioButton()\"" , "", "none"));
       %>
      </td>
      </tr>
      <tr>
            <td valign="top">
            <%
           if ((!insertModeFlag || errorFlag)&& (corporateOrgECOLInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgECOLDualInd.equals(""))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgECOLDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValueA = true;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgECOLDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgECOLDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
                 defaultRadioButtonValueD = false;
              }
              else
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }

           out.print(widgetFactory.createCheckboxField("ExportCollectionsIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setECOLRadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.ExportCollections") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgECOLCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("ExportCollectionsCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setECOLCheckBox()\"", "", "none") %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("ExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "EC", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                  isReadOnly,
                  "onClick=\"local.setECOLCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
         <%
             out.print(widgetFactory.createRadioButtonField("ExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "EC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                  isReadOnly,
                  "onClick=\"local.setECOLCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("ExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "EC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                  isReadOnly,
                  "onClick=\"local.setECOLCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("ExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "EC", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                  isReadOnly,
                  "onClick=\"local.setECOLCheckBox()\"", "")+"<br>");
	        panelOptions = "<option  value=\"\"></option>";
    		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","EXP_COL_IND",corporateOrg.getAttribute("exp_col_panel_group_oid"));
    		out.print(widgetFactory.createSelectField("ExportCollectionsPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setECOLPanelRadioButton()\"" , "", "none"));
          %>
      </td>
      </tr>
      <tr>
            <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (corporateOrgELCInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgELCDualInd.equals(""))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgELCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgELCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgELCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

           out.print(widgetFactory.createCheckboxField("ExportLettersOfCreditIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setELCRadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.ExportLC") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgExpDLCCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("ExportDLCCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setELCCheckBox()\"", "", "none") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ExportLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "ELC", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setELCCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ExportLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "ELC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setELCCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ExportLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "ELC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setELCCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ExportLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "ELC", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setELCCheckBox()\"", "")+"<br>");
        	panelOptions = "<option  value=\"\"></option>";
  			panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","EXP_DLC_IND", corporateOrg.getAttribute("exp_dlc_panel_group_oid"));
  			out.print(widgetFactory.createSelectField("ExportLCPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setELCPanelRadioButton()\"" , "", "none"));
        %>
      </td>
      </tr>
      <tr>
        <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag)&& (corporateOrgNECOLInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgNECOLDualInd.equals(""))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgNECOLDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValueA = true;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgNECOLDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgNECOLDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
                 defaultRadioButtonValueD = false;
              }
              else
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }

           out.print(widgetFactory.createCheckboxField("NewExportCollectionsIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setNECOLRadioButtons()\"", "", "none"));
        %>
      </td>
       <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.NewExportCollections") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgNewExpCollCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("NewExportCollectionsCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setNECOLCheckBox()\"", "", "none") %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("NewExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "NEC", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                  isReadOnly,
                  "onClick=\"local.setNECOLCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("NewExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "NEC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                  isReadOnly,
                  "onClick=\"local.setNECOLCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("NewExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "NEC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                  isReadOnly,
                  "onClick=\"local.setNECOLCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("NewExportCollectionsDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "NEC", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                  isReadOnly,
                  "onClick=\"local.setNECOLCheckBox()\"", "")+"<br>");
          	panelOptions = "<option  value=\"\"></option>";
    		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","EXP_OCO_IND",corporateOrg.getAttribute("exp_oco_panel_group_oid"));
    		out.print(widgetFactory.createSelectField("NewExportCollectionsPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setNECOLPanelRadioButton()\"" , "", "none"));
          %>
      </td>
      </tr>
      <%-- SSikhakolli - Rel-9.4 CR-818 - Begin --%>
      <tr>
      <td valign="top">
      <%
         if ((!insertModeFlag || errorFlag)&& (corporateOrgImportCOLInd.equals(TradePortalConstants.INDICATOR_YES)))
         {
            defaultCheckBoxValue = true;

            if (corporateOrgImportColDualInd.equals(""))
            {
               defaultRadioButtonValueA = false;
               defaultRadioButtonValueB = false;
               defaultRadioButtonValueC = false;
               defaultRadioButtonValueD = false;
            }
            else if (corporateOrgImportColDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
            {
               defaultRadioButtonValueA = true;
               defaultRadioButtonValueB = false;
               defaultRadioButtonValueC = false;
               defaultRadioButtonValueD = false;
            }
            else if (corporateOrgImportColDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
            {
               defaultRadioButtonValueA = false;
               defaultRadioButtonValueB = true;
               defaultRadioButtonValueC = false;
               defaultRadioButtonValueD = false;
            }
            else if (corporateOrgImportColDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
            {
               defaultRadioButtonValueA = false;
               defaultRadioButtonValueB = false;
               defaultRadioButtonValueC = true;
               defaultRadioButtonValueD = false;
            }
            else
            {
               defaultRadioButtonValueA = false;
               defaultRadioButtonValueB = false;
               defaultRadioButtonValueC = false;
               defaultRadioButtonValueD = true;
            }
         }
         else
         {
            defaultCheckBoxValue     = false;
            defaultRadioButtonValueA = false;
            defaultRadioButtonValueB = false;
            defaultRadioButtonValueC = false;
            defaultRadioButtonValueD = false;
         }

         out.print(widgetFactory.createCheckboxField("ImportCollectionsIndicator", "",
                 defaultCheckBoxValue, isReadOnly, false,
                 "onClick=\"local.setImportCOLRadioButtons()\"", "", "none"));
      %>
    </td>
     <td valign="top">
        <%= widgetFactory.createSubLabel("CorpCust.ImportCollections") %>
        <br>
        <%
        if ((!insertModeFlag || errorFlag) &&
               (corporateOrgImportCollCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;
           }
           else
           {
              defaultCheckBoxValue = false;
           }
         %>
        <%= widgetFactory.createCheckboxField("ImportCollectionsCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
      		  							defaultCheckBoxValue, isReadOnly, false,
                                            "onClick=\"local.setImportCOLCheckBox()\"", "", "none") %>
    </td>
    <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportCollectionsDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "IC", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                isReadOnly,
                "onClick=\"local.setImportCOLCheckBox()\"", ""));
        %>
    </td>
    <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportCollectionsDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "IC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                isReadOnly,
                "onClick=\"local.setImportCOLCheckBox()\"", ""));
        %>
    </td>
    <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportCollectionsDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "IC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                isReadOnly,
                "onClick=\"local.setImportCOLCheckBox()\"", ""));
        %>
    </td>
    <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportCollectionsDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "IC", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                isReadOnly,
                "onClick=\"local.setImportCOLCheckBox()\"", "")+"<br>");
        	panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","IMPORT_COL_IND",corporateOrg.getAttribute("imp_col_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("ImportCollectionsPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setImportCOLPanelRadioButton()\"" , "", "none"));
        %>
    </td>
    </tr>
    <%-- SSikhakolli - Rel-9.4 CR-818 - End --%>
      <tr>
      <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (corporateOrgILCInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgILCDualInd.equals(""))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgILCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgILCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgILCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

           out.print(widgetFactory.createCheckboxField("ImportLettersOfCreditIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setILCRadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.ImportDLC") %>
          <br>
            <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgImportDLCCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("ImportDLCCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setILCCheckBox()\"", "", "none") %>	
          <br>
          <div class="tableTextShortestIndent">
            <%= widgetFactory.createSubLabel("CorpCust.ATPProcessPurchaseText")%>
            <br>
          </div>
           <%
             if ((!insertModeFlag || errorFlag) &&
                 (autoLCCreate.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	  %>
            <%=widgetFactory.createCheckboxField("ImportDLCProcessPOUploadIndicator", "CorpCust.ProcessPurchaseOrders",
	                   defaultCheckBoxValue, isReadOnly, false,
	                   "onClick=\"local.setILCCheckBox()\"", "", "none")%>
	  <%-- Naveen 27-July-2012 IR-T36000003307 --%>
	  	   <%-- Commented the below label as it was not getting aligned with the corresponding checkbox and passed the same as parameter to above checkbox to resolve the issue	 --%>
	  <%-- <%= widgetFactory.createSubLabel("CorpCust.ProcessPurchaseOrders") %>	 --%>
        <br>
        <%
            if ((!insertModeFlag || errorFlag) &&
                 (allowManualPOEntry.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
	    %>
	    <%=widgetFactory.createCheckboxField("ImportDLCProcessPOManualIndicator", "CorpCust.ProcessPurchaseOrdersManual",
	                   defaultCheckBoxValue, isReadOnly, false,
	                   "onClick=\"local.setILCCheckBox()\"", "", "none")%>
	                   
		<br>
                           
	    <%-- Naveen 27-July-2012 IR-T36000003307 --%>
	  	   <%-- Commented the below label as it was not getting aligned with the corresponding checkbox and passed the same as parameter to above checkbox to resolve the issue	 --%>
        <%-- <%= widgetFactory.createSubLabel("CorpCust.ProcessPurchaseOrdersManual") %>	 --%>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportDLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "IDLC", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setILCCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportDLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "IDLC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setILCCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportDLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "IDLC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setILCCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ImportDLCDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "IDLC", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setILCCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","IMP_DLC_IND",corporateOrg.getAttribute("imp_dlc_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("ImportDLCPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setILCPanelRadioButton()\"" , "", "none"));
        %>
      </td>
      </tr>
      <tr>
            <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (corporateOrgLRInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgLRDualInd.equals(""))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgLRDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgLRDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgLRDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

           out.print(widgetFactory.createCheckboxField("LoanRequestIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setLRRadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.LoanRequests") %>
          <br>
           <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgLoanRequestCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("LoanRequestCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setLRCheckBox()\"", "", "none") %>
<%-- DK CR709 Rel8.2 10/31/2012 Start --%>     
<%--  //KMehta 2 Sep 2014 - Rel9.1 IR-T36000031738 - Change  - Begin --%>
        <br>
        <div class="tableTextShortestIndent">
	        <%= widgetFactory.createSubLabel("CorpCust.ProcessInvoicesVia") %>
	        <br>
        </div>
        <%
                    if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgProcessInvoicesFileUploadInd)))
                      defaultCheckBoxValue = true;
                   else
                      defaultCheckBoxValue  = false;                  
           out.print(widgetFactory.createCheckboxField("ProcessInvoicesFileUploadIndicator", "CorpCust.ProcessInvoicesFileUpload",
                   defaultCheckBoxValue, isReadOnly, false,
                   "", "", "none"));
        %>
        <br>
        <div class="tableTextShortestIndent">
	        <%= widgetFactory.createSubLabel("CorpCust.LoanTypes") %>  
	        <br>
        </div>
        <%
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgTradeLoanInd)))
                      defaultCheckBoxValue = true;
                   else
                      defaultCheckBoxValue  = false;                  
           out.print(widgetFactory.createCheckboxField("TradeLoanIndicator", "CorpCust.TradeLoan",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setLRCheckBox()\"", "", "none"));
           %>
        <br>
        <%
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgImportLoanInd)))
                      defaultCheckBoxValue = true;
                   else
                      defaultCheckBoxValue  = false;                  
           out.print(widgetFactory.createCheckboxField("ImportLoanIndicator", "CorpCust.ImportLoan",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setLRCheckBox()\"", "", "none"));
           %>
        <br>
        <%
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgExportLoanInd)))
                      defaultCheckBoxValue = true;
                   else
                      defaultCheckBoxValue  = false;                  
           out.print(widgetFactory.createCheckboxField("ExportLoanIndicator", "CorpCust.ExportLoan",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setLRCheckBox()\"", "", "none"));
           %>
        <br>
        <%
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvoiceFinancingInd)))
                      defaultCheckBoxValue = true;
                   else
                      defaultCheckBoxValue  = false;                  
           out.print(widgetFactory.createCheckboxField("InvoiceFinancingIndicator", "CorpCust.InvoiceFinance",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setLRCheckBox()\"", "", "none"));
           %>
        <br>
<%--  //KMehta 2 Sep 2014 - Rel9.1 IR-T36000031738 - Change  - End --%>         	    
        <%-- DK CR709 Rel8.2 10/31/2012 End --%>
      </td>
       <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("LoanRequestDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "LR", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setLRCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("LoanRequestDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "LR", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setLRCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("LoanRequestDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "LR", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setLRCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("LoanRequestDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "LR", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setLRCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","LRQ_IND",corporateOrg.getAttribute("lrq_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("LoanRequestPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setLRPanelRadioButton()\"" , "", "none"));
        %>
      </td>
      </tr>
      <tr>
            <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag)&& (corporateOrgGTEEInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgGTEEDualInd.equals(""))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgGTEEDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgGTEEDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgGTEEDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

           out.print(widgetFactory.createCheckboxField("GuaranteesIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setGTEERadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.Guarantees") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgGuaranteesCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("GuaranteesCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setGTEECheckBox()\"", "", "none") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("GuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "G", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setGTEECheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("GuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "G", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setGTEECheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("GuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "G", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setGTEECheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("GuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "G", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setGTEECheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
        //MEer Rel 8.3 IR -19792-- change attribute guar_panel_group_oid to gua_panel_group_oid
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","GUA_IND", corporateOrg.getAttribute("gua_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("GuaranteesPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setGTEEPanelRadioButton()\"" , "", "none"));
        %>
      </td>
      </tr>
      <tr>
      <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (corporateOrgSLCInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgSLCDualInd.equals(""))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgSLCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValueA = true;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgSLCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgSLCDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
                 defaultRadioButtonValueD = false;
              }
              else
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }

           out.print(widgetFactory.createCheckboxField("StandbyLettersOfCreditIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setSLCControls()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.StandbyDLC") %>
          <br>
           <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgSLCCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("StandbyDLCCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setSLCCheckBox()\"", "", "none") %>
          <br>
          <div class="tableTextShortestIndent">
            <%= widgetFactory.createSubLabel("CorpCust.SelectMessage") %>
            <br>
          </div><%--end of tableTextIndent--%>
<%
             if ((!insertModeFlag || errorFlag) &&
                 (!corporateOrgSLCUseOnlyGuarantee.equals(TradePortalConstants.INDICATOR_YES) && !corporateOrgSLCUseBoth.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
%>
            <%=widgetFactory.createRadioButtonField("SimpleDetailBoth",
                  "Simple", "CorpCust.AllowOnlySimpleSLC", "Simple", defaultCheckBoxValue,
                  isReadOnly,
                  "onClick=\"local.setSLCRadio('S')\"", "")%>


            <br>
          <%
             if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgSLCUseOnlyGuarantee.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
            // out.print(widgetFactory.createCheckboxField("StandbyDLCUseOnlyGuaranteeIndicator", "CorpCust.AllowOnlyDetailedSLC",
            //         defaultCheckBoxValue, isReadOnly, false,
           //          "onClick=\"local.setSLCCheckBox()\"", "", "none"));
          %>
            <%=widgetFactory.createRadioButtonField("SimpleDetailBoth",
                     "Detail", "CorpCust.AllowOnlyDetailedSLC", "Detail", defaultCheckBoxValue,
                     isReadOnly,
                     "onClick=\"local.setSLCRadio('D')\"", "")%>

            <input type="hidden" name="StandbyDLCUseOnlyGuaranteeIndicator" value="<%=corporateOrgSLCUseOnlyGuarantee%>">
         <%-- Naveen 27-July-2012 IR-T36000003307 --%>
	  	 <%-- Commented the below label as it was not getting aligned with the corresponding checkbox and passed the same as parameter to above checkbox to resolve the issue	 --%>
         <%-- <%= widgetFactory.createSubLabel("CorpCust.AllowOnlyDetailedSLC") %>	 --%>
            <br>
           <%
             if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgSLCUseBoth.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           // out.print(widgetFactory.createCheckboxField("StandbyLCUseBothFormsIndicator", "CorpCust.AllowSimpleAndDetailedSLC",
            //         defaultCheckBoxValue, isReadOnly, false,
            //         "onClick=\"local.setSLCCheckBox()\"", "", "none"));
	  %>
	    <%=widgetFactory.createRadioButtonField("SimpleDetailBoth",
                     "Both", "CorpCust.AllowSimpleAndDetailedSLC", "Both", defaultCheckBoxValue,
                     isReadOnly,
                     "onClick=\"local.setSLCRadio('B')\"", "")%>
	         <input type="hidden" name="StandbyLCUseBothFormsIndicator" value="<%=corporateOrgSLCUseBoth%>">
	         <br>
            <%-- Naveen 27-July-2012 IR-T36000003307 --%>
 	   	    <%-- Commented the below label as it was not getting aligned with the corresponding checkbox and passed the same as parameter to above checkbox to resolve the issue	 --%>
        	<%-- <%= widgetFactory.createSubLabel("CorpCust.AllowSimpleAndDetailedSLC") %>	 --%>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("StandbyDLCDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "SDLC", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                  isReadOnly,
                  "onClick=\"local.setSLCCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("StandbyDLCDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "SDLC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                  isReadOnly,
                  "onClick=\"local.setSLCCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("StandbyDLCDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "SDLC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                  isReadOnly,
                  "onClick=\"local.setSLCCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("StandbyDLCDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "SDLC", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                  isReadOnly,
                  "onClick=\"local.setSLCCheckBox()\"", "")+"<br>");
          	panelOptions = "<option  value=\"\"></option>";
    		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","SLC_IND",corporateOrg.getAttribute("slc_panel_group_oid"));
    		out.print(widgetFactory.createSelectField("StandbyDLCPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setSLCPanelRadioButton()\"" , "", "none"));
          %>
      </td>
    </tr>
    <tr>
      <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (corporateOrgSGTEEInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgSGTEEDualInd.equals(""))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgSGTEEDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgSGTEEDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (corporateOrgSGTEEDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

           out.print(widgetFactory.createCheckboxField("ShippingGuaranteesIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setSGTEERadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.ShippingGuarantees") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgSHPCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("ShippingGuaranteeCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setSGTEECheckBox()\"", "", "none") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ShippingGuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "SG", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setSGTEECheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ShippingGuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "SG", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setSGTEECheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ShippingGuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "SG", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setSGTEECheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ShippingGuaranteesDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "SG", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setSGTEECheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","SHP_IND",corporateOrg.getAttribute("shp_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("ShippingGuaranteesPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setSGTEEPanelRadioButton()\"" , "", "none"));
        %>
      </td>
    </tr>
    <tr>
      <td valign="top">
            <%
           if ((!insertModeFlag || errorFlag) && (corporateOrgRAInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgRADualInd.equals(""))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgRADualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValueA = true;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgRADualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgRADualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
                 defaultRadioButtonValueD = false;
              }
              else
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }

           out.print(widgetFactory.createCheckboxField("RequestAdviseIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setRARadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.RequestAdvise") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgRQACheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("RequestAdviseCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setRACheckBox()\"", "", "none") %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("RequestAdviseDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "RA", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                  isReadOnly,
                  "onClick=\"local.setRACheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("RequestAdviseDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "RA", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                  isReadOnly,
                  "onClick=\"local.setRACheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
         <%
             out.print(widgetFactory.createRadioButtonField("RequestAdviseDualAuthorizationIndicator",
                 "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "RA", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                 isReadOnly,
                 "onClick=\"local.setRACheckBox()\"", ""));
          %>
        </td>
        <td align="center" valign="top">
         <%
             out.print(widgetFactory.createRadioButtonField("RequestAdviseDualAuthorizationIndicator",
                 "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "RA", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                 isReadOnly,
                 "onClick=\"local.setRACheckBox()\"", "")+"<br>");
    	    panelOptions = "<option  value=\"\"></option>";
	   		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","RQA_IND",corporateOrg.getAttribute("rqa_panel_group_oid"));
   			out.print(widgetFactory.createSelectField("RequestAdvisePanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setRAPanelRadioButton()\"" , "", "none"));
          %>
        </td>
      </tr>
      <%--Ravindra - CR-708B - Start--%>
      <tr>
      <td valign="top">
            <%
           if ((!insertModeFlag || errorFlag) && (corporateOrgSPInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;

              if (corporateOrgSPDualInd.equals(""))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgSPDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
              {
                 defaultRadioButtonValueA = true;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgSPDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (corporateOrgSPDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
                 defaultRadioButtonValueD = false;
              }
              else
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }

           out.print(widgetFactory.createCheckboxField("SupplierPortalIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setSPRadioButtons()\"", "", "none"));
        %>
      </td>
      <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.SupplierPortal") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
         
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("SupplierPortalDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "SP", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                  isReadOnly,
                  "onClick=\"local.setSPCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("SupplierPortalDualAuthorizationIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "SP", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                  isReadOnly,
                  "onClick=\"local.setSPCheckBox()\"", ""));
          %>
      </td>
      <td align="center" valign="top">
         <%
             out.print(widgetFactory.createRadioButtonField("SupplierPortalDualAuthorizationIndicator",
                 "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "SP", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                 isReadOnly,
                 "onClick=\"local.setSPCheckBox()\"", ""));
          %>
        </td>
        <td align="center" valign="top">
         <%
             out.print(widgetFactory.createRadioButtonField("SupplierPortalDualAuthorizationIndicator",
                 "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "SP", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                 isReadOnly,
                 "onClick=\"local.setSPCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
      //MEer Rel 8.3 IR -19792-- change attribute sp_panel_group_oid to supplier_panel_group_oid
   		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","SP_IND",corporateOrg.getAttribute("supplier_panel_group_oid"));
   		out.print(widgetFactory.createSelectField("SupplierPortalPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setSPPanelRadioButton()\"" , "", "none"));
          %>
        </td>
      </tr>
      <%--Ravindra - CR-708B - End--%>
     
      <tr>
        <td valign="top">
<%
  if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgNonPortalOrigInstrInd)))
    defaultCheckBoxValue = true;
  else
    defaultCheckBoxValue  = false;

  out.print(widgetFactory.createCheckboxField("NonPortalOriginatedInstrIndicator", "",
            defaultCheckBoxValue, isReadOnly, false,
            "onClick=\"local.setNPOIIndicator(document.forms[0].NonPortalOriginatedInstrIndicator.checked)\"", "", "none"));
%>
        </td>
        <td>
          <%= widgetFactory.createSubLabel("CorpCust.NonPortalOriginatedInstruments") %>
          <br>
<%
  //Ravindra - Rel7100 -IR VRUL062239953 - 12th Aug 2011 - Start
  if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgNonPortalOrigInstrInd)))
    defaultCheckBoxValue = true;
  else
    defaultCheckBoxValue  = false;
  //Ravindra - Rel7100 -IR VRUL062239953 - 12th Aug 2011 - End
  out.print(widgetFactory.createCheckboxField("NonPortalOriginatedInstrAllowedIndicator", "CorpCust.NonPortalOriginatedInstrumentsAllowed",
            defaultCheckBoxValue, isReadOnly, false,
            "onClick=\"local.setNPOIIndicator(document.forms[0].NonPortalOriginatedInstrAllowedIndicator.checked)\"", "", "none"));
%>
          <%-- Naveen 27-July-2012 IR-T36000003307 --%>
          <%-- Commented the below label as it was not getting aligned with the corresponding checkbox and passed the same as parameter to above checkbox to resolve the issue	 --%>
          <%-- <%= widgetFactory.createSubLabel("CorpCust.NonPortalOriginatedInstrumentsAllowed") %>	 --%>
          <br>
        </td>
        <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      
   <%--SURREWSH -Rel-9.4 CR-932 Start  --%>
      <tr>
        <td valign="top">
<%
  if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgBillingInstrumentsIndicator)))
    defaultCheckBoxValue = true;
  else
    defaultCheckBoxValue  = false;

  out.print(widgetFactory.createCheckboxField("BillingInstrumentsIndicator", "",
            defaultCheckBoxValue, isReadOnly, false,
            "onClick=\"local.setBIndicator(document.forms[0].BillingInstrumentsIndicator.checked)\"", "", "none"));
%>
        </td>
        <td>
          <%= widgetFactory.createSubLabel("CorpCust.BillingInstruments") %>
          <br>
<%

  if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgBillingInstrumentsIndicator)))
    defaultCheckBoxValue = true;
  else
	  
    defaultCheckBoxValue  = false;
 
  out.print(widgetFactory.createCheckboxField("BillingInstrumentsAllowedIndicator", "CorpCust.BillingInstrumentsAllowed",
            defaultCheckBoxValue, isReadOnly, false,
            "onClick=\"local.setBIndicator(document.forms[0].BillingInstrumentsAllowedIndicator.checked)\"", "", "none"));
%>
          <br>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
   </tr> 
   <%--SURREWSH -Rel-9.4 CR-932 End --%>
 </tbody>
  </table>

  <br>
  <br>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 31%;">
      <col span="1" style="width: 12%;">
      <col span="1" style="width: 12%;">
      <col span="1" style="width: 18%;">
      <col span="1" style="width: 10%;">
      <col span="1" style="width: 12%;">
    </colgroup>
    <thead>
      <tr>
        <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeOneUser",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoUsers",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoWorkGroups",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.PanelAuth",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.DRAuthorizeInstrDefaults",TradePortalConstants.TEXT_BUNDLE)%></th>
      </tr>
    </thead>
    <thead>
      <th class="tableSubHeader withCheckBoxColumn" colspan="7">
        <%=resMgr.getText("CorpCust.OtherAuthorisationSettings",TradePortalConstants.TEXT_BUNDLE)%>
      </th>
    </thead>
    <tbody>
      <tr>
        <td align="center" valign="top">
<%
  if ((!insertModeFlag || errorFlag)) {

     if (corporateOrgDRDualInd.equals("")) {
       defaultRadioButtonValue1 = true;
       defaultRadioButtonValue2 = false;
       defaultRadioButtonValue3 = false;
       defaultRadioButtonValue4 = false;
       defaultRadioButtonValue5 = false;
     }
     else if (TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corporateOrgDRDualInd)) {
       defaultRadioButtonValue1 = true;
       defaultRadioButtonValue2 = false;
       defaultRadioButtonValue3 = false;
       defaultRadioButtonValue4 = false;
       defaultRadioButtonValue5 = false;
     }
     else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDRDualInd)) {
       defaultRadioButtonValue1 = false;
       defaultRadioButtonValue2 = true;
       defaultRadioButtonValue3 = false;
       defaultRadioButtonValue4 = false;
       defaultRadioButtonValue5 = false;
     }
     else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDRDualInd)) {
       defaultRadioButtonValue1 = false;
       defaultRadioButtonValue2 = false;
       defaultRadioButtonValue3 = true;
       defaultRadioButtonValue4 = false;
       defaultRadioButtonValue5 = false;
     }
     else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDRDualInd)) {
         defaultRadioButtonValue1 = false;
         defaultRadioButtonValue2 = false;
         defaultRadioButtonValue3 = false;
         defaultRadioButtonValue4 = true;
         defaultRadioButtonValue5 = false;
     }
     else {
       defaultRadioButtonValue1 = false;
       defaultRadioButtonValue2 = false;
       defaultRadioButtonValue3 = false;
       defaultRadioButtonValue4 = false;
       defaultRadioButtonValue5 = true;
     }
   }
   else {
     defaultRadioButtonValue1 = false;
     defaultRadioButtonValue2 = false;
     defaultRadioButtonValue3 = false;
     defaultRadioButtonValue4 = false;
     defaultRadioButtonValue5 = false;
   }

  //to ensure this table is same size as others, create a hidden checkbox
  out.print(widgetFactory.createCheckboxField("", "",
            false, false, false,
            "style=\"visibility: hidden;\"", "", "none"));
%>
        </td>
        <td valign="top">
          <%= widgetFactory.createSubLabel("CorpCust.DiscrepancyResponse") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgDRCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("DiscrepancyResponseCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "", "", "none") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgIncludeSettleInstrInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("DiscrepancyResponseIncludeSettleInstrInd", "CorpCust.IncludeSettlementInstruction",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "", "", "none") %>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("DiscrepancyResponseDualAuthorizationIndicator",
    "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER"+"DR", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, 
    defaultRadioButtonValue2, isReadOnly, "onClick=\"local.setDCRPanelDropDown()\"", ""));
%>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("DiscrepancyResponseDualAuthorizationIndicator",
    "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS"+"DR", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, 
    defaultRadioButtonValue3, isReadOnly, "onClick=\"local.setDCRPanelDropDown()\"", ""));
%>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("DiscrepancyResponseDualAuthorizationIndicator",
    "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS"+"DR", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, 
    defaultRadioButtonValue4, isReadOnly, "onClick=\"local.setDCRPanelDropDown()\"", ""));
%>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("DiscrepancyResponseDualAuthorizationIndicator",
    "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH"+"DR", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, 
    defaultRadioButtonValue5, isReadOnly, "onClick=\"local.setDCRPanelDropDown()\"", "")+"<br>");
panelOptions = "<option  value=\"\"></option>";
panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","DCR_IND",corporateOrg.getAttribute("dcr_panel_group_oid"));
out.print(widgetFactory.createSelectField("DiscrepancyResponsePanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setDCRPanelRadioButton()\"" , "", "none"));
%>
        </td>
        <td align="center" valign="top">
<%
  out.print(widgetFactory.createRadioButtonField("DiscrepancyResponseDualAuthorizationIndicator",
    "TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS"+"DR", "", TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS, 
    defaultRadioButtonValue1, isReadOnly, "onClick=\"local.setDCRPanelDropDown()\"", ""));
%>
        </td>
      </tr>
      <%-- SSikhakolli - Rel-9.4 CR-818 - Begin --%>
      <tr>
      <td align="center" valign="top">
<%
if ((!insertModeFlag || errorFlag)) {

   if (corporateOrgSIDualInd.equals("")) {
     defaultRadioButtonValue1 = true;
     defaultRadioButtonValue2 = false;
     defaultRadioButtonValue3 = false;
     defaultRadioButtonValue4 = false;
     defaultRadioButtonValue5 = false;
   }
   else if (TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corporateOrgSIDualInd)) {
     defaultRadioButtonValue1 = true;
     defaultRadioButtonValue2 = false;
     defaultRadioButtonValue3 = false;
     defaultRadioButtonValue4 = false;
     defaultRadioButtonValue5 = false;
   }
   else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgSIDualInd)) {
     defaultRadioButtonValue1 = false;
     defaultRadioButtonValue2 = true;
     defaultRadioButtonValue3 = false;
     defaultRadioButtonValue4 = false;
     defaultRadioButtonValue5 = false;
   }
   else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgSIDualInd)) {
     defaultRadioButtonValue1 = false;
     defaultRadioButtonValue2 = false;
     defaultRadioButtonValue3 = true;
     defaultRadioButtonValue4 = false;
     defaultRadioButtonValue5 = false;
   }
   else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgSIDualInd)) {
       defaultRadioButtonValue1 = false;
       defaultRadioButtonValue2 = false;
       defaultRadioButtonValue3 = false;
       defaultRadioButtonValue4 = true;
       defaultRadioButtonValue5 = false;
   }
   else {
     defaultRadioButtonValue1 = false;
     defaultRadioButtonValue2 = false;
     defaultRadioButtonValue3 = false;
     defaultRadioButtonValue4 = false;
     defaultRadioButtonValue5 = true;
   }
 }
 else {
   defaultRadioButtonValue1 = false;
   defaultRadioButtonValue2 = false;
   defaultRadioButtonValue3 = false;
   defaultRadioButtonValue4 = false;
   defaultRadioButtonValue5 = false;
 }

//to ensure this table is same size as others, create a hidden checkbox
out.print(widgetFactory.createCheckboxField("", "",
          false, false, false,
          "style=\"visibility: hidden;\"", "", "none"));
%>
      </td>
      <td valign="top">
        <%= widgetFactory.createSubLabel("CorpCust.SettlementInstructionMsgResponse") %>
        <br>
        <%
        if ((!insertModeFlag || errorFlag) &&
               (corporateOrgSICheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
           {
              defaultCheckBoxValue = true;
           }
           else
           {
              defaultCheckBoxValue = false;
           }
         %>
        <%= widgetFactory.createCheckboxField("SettlementInstructionCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
      		  							defaultCheckBoxValue, isReadOnly, false,
                                            "", "", "none") %>
      <td align="center" valign="top">
<%
out.print(widgetFactory.createRadioButtonField("SettlementInstructionDualAuthorizationIndicator",
  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER"+"SI", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, 
  defaultRadioButtonValue2, isReadOnly, "onClick=\"local.setSIPanelDropDown()\"", ""));
%>
      </td>
      <td align="center" valign="top">
<%
out.print(widgetFactory.createRadioButtonField("SettlementInstructionDualAuthorizationIndicator",
  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS"+"SI", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, 
  defaultRadioButtonValue3, isReadOnly, "onClick=\"local.setSIPanelDropDown()\"", ""));
%>
      </td>
      <td align="center" valign="top">
<%
out.print(widgetFactory.createRadioButtonField("SettlementInstructionDualAuthorizationIndicator",
  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS"+"SI", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, 
  defaultRadioButtonValue4, isReadOnly, "onClick=\"local.setSIPanelDropDown()\"", ""));
%>
      </td>
      <td align="center" valign="top">
<%
out.print(widgetFactory.createRadioButtonField("SettlementInstructionDualAuthorizationIndicator",
  "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH"+"SI", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, 
  defaultRadioButtonValue5, isReadOnly, "onClick=\"local.setSIPanelDropDown()\"", "")+"<br>");
panelOptions = "<option  value=\"\"></option>";
panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","SETTLEMENT_INSTR_IND",corporateOrg.getAttribute("sit_panel_group_oid"));
out.print(widgetFactory.createSelectField("SettlementInstructionPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setSIPanelRadioButton()\"" , "", "none"));
%>
      </td>
      <td align="center" valign="top">
<%
out.print(widgetFactory.createRadioButtonField("SettlementInstructionDualAuthorizationIndicator",
  "TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS"+"SI", "", TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS, 
  defaultRadioButtonValue1, isReadOnly, "onClick=\"local.setSIPanelDropDown()\"", ""));
%>
      </td>
    </tr>
    <%-- SSikhakolli - Rel-9.4 CR-818 - End --%>
    </tbody>
  </table>

  <br>
  <br>

  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 30%;">
      <col span="1" style="width: 16%;">
      <col span="1" style="width: 16%;">
      <col span="1" style="width: 16%;">
      <col span="1" style="width: 16%;">
    </colgroup>
    <thead>
      <tr>
        <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeOneUser",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoUsers",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoWorkGroups",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.PanelAuth",TradePortalConstants.TEXT_BUNDLE)%></th>
      </tr>
    </thead>
    <thead>
      <th class="tableSubHeader withCheckBoxColumn" colspan="6"><%=resMgr.getText("CorpCust.DomesticPayment",TradePortalConstants.TEXT_BUNDLE)%></th>
      <input type=hidden name=PaymtInstrSettingsAuthIndicator value=<%=allowPymtInstrumentAuth%>>
    </thead>
    <tbody>
            <tr>
                  <td valign="top">
        <%
        if ((!insertModeFlag || errorFlag) && (corporateOrgFTRInd.equals(TradePortalConstants.INDICATOR_YES)))
        {
           defaultCheckBoxValue = true;

           if (corporateOrgFTRDualInd.equals(""))
           {
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }
           else if (corporateOrgFTRDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_ONE_USER))
           {
              defaultRadioButtonValueA = true;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }
           else if (corporateOrgFTRDualInd.equals(TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS))
           {
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = true;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgFTRDualInd))
           {
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = true;
              defaultRadioButtonValueD = false;
           }
           else
           {
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = true;
           }
        }
        else
        {
           defaultCheckBoxValue     = false;
           defaultRadioButtonValueA = false;
           defaultRadioButtonValueB = false;
           defaultRadioButtonValueC = false;
           defaultRadioButtonValueD = false;
        }

           out.print(widgetFactory.createCheckboxField("FundsTransferIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setFTRRadioButtons()\"", "", "none"));
        %>
      </td>
      <td>
          <%= widgetFactory.createSubLabel("CorpCust.FundsTransferRequests") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgFTRQCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("FundsTransferCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setFTRCheckBox('N')\"", "", "none") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("FundsTransferDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "FTR", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                isReadOnly,
                "onClick=\"local.setFTRCheckBox('N')\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("FundsTransferDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "FTR", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                isReadOnly,
                "onClick=\"local.setFTRCheckBox('N')\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("FundsTransferDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "FTR", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                isReadOnly,
                "onClick=\"local.setFTRCheckBox('N')\"", ""));
        %>
     </td>
     <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("FundsTransferDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "FTR", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                isReadOnly,
                "onClick=\"local.setFTRCheckBox('Y')\"", ""));
        %>
      </td>
      </tr>

	<tr>
        <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag)&& (TradePortalConstants.INDICATOR_YES.equals(corporateOrgDPInd)))
           {
              defaultCheckBoxValue = true;

              if ("".equals(corporateOrgDualDPInd))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDualDPInd))
              {
                 defaultRadioButtonValueA = true;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDualDPInd))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = true;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDualDPInd))
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = true;
                 defaultRadioButtonValueD = false;
              }
              else
              {
                 defaultRadioButtonValueA = false;
                 defaultRadioButtonValueB = false;
                 defaultRadioButtonValueC = false;
                 defaultRadioButtonValueD = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValueA = false;
              defaultRadioButtonValueB = false;
              defaultRadioButtonValueC = false;
              defaultRadioButtonValueD = false;
           }

           out.print(widgetFactory.createCheckboxField("DomesticPaymentIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setDPRadioButtons()\"", "", "none"));
        %>
      </td>
      <td>
          <%= widgetFactory.createSubLabel("CorpCust.DomesticPayment") %>
          <br>
          <%
           //MDB IR# PMUL011037699 1/12/11 - Begin
             if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(processPaymentUploadInd)))
                defaultCheckBoxValue = true;
             else
                defaultCheckBoxValue  = false;

             out.print(widgetFactory.createCheckboxField("ProcessPaymentFileUploadIndicator", "CorpCust.PaymentProcessFileUpload",
                     defaultCheckBoxValue, isReadOnly, false,
                     "onClick=\"local.setPaymentsCheckBox()\" labelClass=\"shortWidth1\"", "", "none"));
           //MDB IR# PMUL011037699 1/12/11 - End
          %>
              <br>
		<%-- Naveen 27-July-2012 IR-T36000003307 --%>
		<%-- Added the new property- CorpCust.AutoAuthoriseIncomingPayment --%>
		<%
          	 straightThroughAuthorizeInd = corporateOrg.getAttribute("straight_through_authorize");
             out.print(widgetFactory.createCheckboxField("straightThroughAuthorizeIndicator", "CorpCust.AutoAuthoriseIncomingPayment",
            		 straightThroughAuthorizeInd.equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,
                     "onClick=\"local.setPaymentsCheckBox()\" labelClass=\"shortWidth1\"", "", "none"));
          %>
          <br>
          <%-- Rel9.2 CR 988 - Adding new checkbox for allowing auto authorize of H2H partial payments --%>
		<%
          	 out.print(widgetFactory.createCheckboxField("allowPartialPayAuthorizeIndicator", "CorpCust.PartialAuthoriseIncomingPayment",
            		 partialPayAuthorizeInd.equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,
                     "onClick=\"local.setPaymentsCheckBox()\" labelClass=\"shortWidth1\"", "", "none"));
          %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgFTDPCheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
           
          <%-- Sandeep - Rel 8.3 String Test IR# T36000020220 08/27/2013 - Begin --%>
          <%-- Removed onclick event 'onClick=\"local.setDPCheckBox('N')\"' --%>
          <%= widgetFactory.createCheckboxField("DomesticPaymentCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "", "", "none") %>
          <%-- Sandeep - Rel 8.3 String Test IR# T36000020220 08/27/2013 - End --%>
          
          <br>
          <% //CR 857 
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgPaymentBenAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          
          <%= widgetFactory.createCheckboxField("PaymentBenePanelAuthIndicator", "CorpCust.PaymentBenAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setPaymentBenAuthInDCheckBox()\" labelClass=\"shortWidth1\"", "", "none") %>
                                              
          <br>
       </td>
       <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("DomesticPaymentDualAuthIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "DP", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValueA,
                  isReadOnly,
                  "onClick=\"local.setDPCheckBox('N')\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("DomesticPaymentDualAuthIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "DP", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValueB,
                  isReadOnly,
                  "onClick=\"local.setDPCheckBox('N')\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("DomesticPaymentDualAuthIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "DP", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValueC,
                  isReadOnly,
                  "onClick=\"local.setDPCheckBox('N')\"", ""));
          %>
      </td>
      <td align="center" valign="top">
          <%
             out.print(widgetFactory.createRadioButtonField("DomesticPaymentDualAuthIndicator",
                  "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "DP", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValueD,
                  isReadOnly,
                  "onClick=\"local.setDPCheckBox('Y')\"", ""));
          %>
      </td>
      </tr>
      <tr>
        <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgTBAInd)))
           {
              defaultCheckBoxValue = true;

              if ("".equals(corporateOrgDualTBAInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDualTBAInd))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDualTBAInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDualTBAInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

           out.print(widgetFactory.createCheckboxField("TransferBtwAcctsIndicator", "",
                   defaultCheckBoxValue, isReadOnly, false,
                   "onClick=\"local.setTBARadioButtons()\"", "", "none"));
        %>
      </td>
      <td>
          <%= widgetFactory.createSubLabel("CorpCust.TransferBtwAccts") %>
          <br>
          <%
          if ((!insertModeFlag || errorFlag) &&
                 (corporateOrgFTBACheckerAuthInd.equals(TradePortalConstants.INDICATOR_YES)))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          <%= widgetFactory.createCheckboxField("TransferBtxAcctsCheckerAuthIndicator", "CorpCust.CheckerAuthorization",
        		  							defaultCheckBoxValue, isReadOnly, false,
                                              "onClick=\"local.setTBACheckBox('N')\"", "", "none") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("TransferBtwAcctsDualAuthIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "TBA", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setTBACheckBox('N')\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("TransferBtwAcctsDualAuthIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "TBA", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setTBACheckBox('N')\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("TransferBtwAcctsDualAuthIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "TBA", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setTBACheckBox('N')\"", ""));
        %>
      </td>
      <td align="center" valign="top">
      <%--Defect 5493 by dillip --%>
        <%
           out.print(widgetFactory.createRadioButtonField("TransferBtwAcctsDualAuthIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "TBA", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setTBACheckBox('Y')\"", ""));
        %>
        <%--Ended Here by dillip --%>
      </td>
      </tr>
      </tbody>
      </table>
     <br><br>
  <%-- Pratiksha CR-434 09/24/2008 ADD BEGIN--%>
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 26%;">
      <col span="1" style="width: 13%;">
      <col span="1" style="width: 13%;">
      <col span="1" style="width: 18%;">
      <col span="1" style="width: 13%;">
      <col span="1" style="width: 12%;">
    </colgroup>
    <thead>
      <tr>
        <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeOneUser",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoUsers",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoWorkGroups",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.PanelAuth",TradePortalConstants.TEXT_BUNDLE)%></th>
        <%--SPenke T36000006033 10/11/2012 Begin --%>
        <th align="left"><%=resMgr.getText("CorpCust.AuthorizationNotReq",TradePortalConstants.TEXT_BUNDLE)%></th>
      </tr>
    </thead>
    <thead>
      <th class="tableSubHeader withCheckBoxColumn" colspan="7">
        <%--SPenke T36000006033 10/11/2012 End --%>
        <%--cquinton 11/14/2012 update heading text --%>
        <%=resMgr.getText("CorpCust.ReceivablesManagement",TradePortalConstants.TEXT_BUNDLE)%>
      </th>
    </thead>
    <tbody>
            <tr>
                  <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgARMInd)))
           {
              defaultCheckBoxValue = true;

              if ("".equals(corporateOrgARMDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
             else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgARMDualInd))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgARMDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgARMDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
                    defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

            out.print(widgetFactory.createCheckboxField("ReceivablesManagementIndicator", "",
                defaultCheckBoxValue, isReadOnly, false,
                "onClick=\"local.setRMRadioButtons()\"", "", "none"));
       %>
      </td>
      <td valign="top">
      <%= widgetFactory.createSubLabel("CorpCust.MatchingApproval") %>
      <br>
          <%
          if ((!insertModeFlag || errorFlag))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
     </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ReceivablesManagementDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "ARM", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setARMCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
          out.print(widgetFactory.createRadioButtonField("ReceivablesManagementDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "ARM", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setARMCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ReceivablesManagementDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "ARM", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setARMCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ReceivablesManagementDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "ARM", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setARMCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","MATCHIN_APPROVAL_IND",corporateOrg.getAttribute("mtch_apprv_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("MatchingApprovalPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setRMPanelRadioButton()\"" , "", "none"));
        %>
      </td>
      <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center" valign="top">
        <%
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgARMInvoiceInd)))
        {
           defaultCheckBoxValue = true;

           if ("".equals(corporateOrgARMInvoiceDualInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }
          else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgARMInvoiceDualInd))
           {
              defaultRadioButtonValue1 = true;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgARMInvoiceDualInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = true;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgARMInvoiceDualInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = true;
              defaultRadioButtonValue4 = false;
           }
           else
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = true;
           }
        }
        else
        {
           defaultCheckBoxValue     = false;
		  	  defaultRadioButtonValue1 = false;
           defaultRadioButtonValue2 = false;
           defaultRadioButtonValue3 = false;
           defaultRadioButtonValue4 = false;
        }

            out.print(widgetFactory.createCheckboxField("ReceivablesManagementInvoiceIndicator", "",
                defaultCheckBoxValue, isReadOnly, false,
                "onClick=\"local.setRMInvoiceRadioButtons()\"", "", "none"));
       %>
      </td>
      <td valign="top">
      <%= widgetFactory.createSubLabel("CorpCust.InvoiceAuthorisation") %>
      <br>
          <%
          if ((!insertModeFlag || errorFlag))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ReceivablesManagementInvoiceDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "INV", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setARMInvoiceCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
          out.print(widgetFactory.createRadioButtonField("ReceivablesManagementInvoiceDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "INV", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setARMInvoiceCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ReceivablesManagementInvoiceDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "INV", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setARMInvoiceCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("ReceivablesManagementInvoiceDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "INV", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setARMInvoiceCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","INV_AUTH_IND",corporateOrg.getAttribute("inv_auth_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("InvoiceAuthorizationPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setRMInvoicePanelRadioButton()\"" , "", "none"));
        %>
      </td>
      <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
      <td>&nbsp;</td>
    </tr>
	<%-- //#RKAZI CR1006 04-24-2015  - BEGIN --%>
    <tr>
        <td align="center" valign="top">
            <%
                if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgDecRECInvoiceInd)))
                {
                    defaultCheckBoxValue = true;

                    if ("".equals(corporateOrgDecRECInvoiceDualInd))
                    {
                        defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = false;
                        defaultRadioButtonValue3 = false;
                        defaultRadioButtonValue4 = false;
                    }
                    else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDecRECInvoiceDualInd))
                    {
                        defaultRadioButtonValue1 = true;
                        defaultRadioButtonValue2 = false;
                        defaultRadioButtonValue3 = false;
                        defaultRadioButtonValue4 = false;
                    }
                    else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDecRECInvoiceDualInd))
                    {
                        defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = true;
                        defaultRadioButtonValue3 = false;
                        defaultRadioButtonValue4 = false;
                    }
                    else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDecRECInvoiceDualInd))
                    {
                        defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = false;
                        defaultRadioButtonValue3 = true;
                        defaultRadioButtonValue4 = false;
                    }
                    else
                    {
                        defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = false;
                        defaultRadioButtonValue3 = false;
                        defaultRadioButtonValue4 = true;
                    }
                }
                else
                {
                    defaultCheckBoxValue     = false;
                    defaultRadioButtonValue1 = false;
                    defaultRadioButtonValue2 = false;
                    defaultRadioButtonValue3 = false;
                    defaultRadioButtonValue4 = false;
                }

                out.print(widgetFactory.createCheckboxField("ReceivablesManagementDeclineInvoiceIndicator", "",
                        defaultCheckBoxValue, isReadOnly, false,
                        "onClick=\"local.setRMDeclineInvoiceRadioButtons()\"", "", "none"));
            %>
        </td>
        <td valign="top">
            <%= widgetFactory.createSubLabel("CorpCust.RecDeclineInvoice") %>
            <br>
            <%
                if ((!insertModeFlag || errorFlag))
                {
                    defaultCheckBoxValue = true;
                }
                else
                {
                    defaultCheckBoxValue = false;
                }
            %>

        </td>
        <td align="center" valign="top">
            <%
                out.print(widgetFactory.createRadioButtonField("ReceivablesManagementDeclineInvoiceDualIndicator",
                        "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "DECINV", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                        isReadOnly,
                        "onClick=\"local.setDeclineRMInvoiceCheckBox()\"", ""));
            %>
        </td>
        <td align="center" valign="top">
            <%
                out.print(widgetFactory.createRadioButtonField("ReceivablesManagementDeclineInvoiceDualIndicator",
                        "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "DECINV", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                        isReadOnly,
                        "onClick=\"local.setDeclineRMInvoiceCheckBox()\"", ""));

            %>
        </td>
        <td align="center" valign="top">
            <%
                out.print(widgetFactory.createRadioButtonField("ReceivablesManagementDeclineInvoiceDualIndicator",
                        "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "DECINV", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                        isReadOnly,
                        "onClick=\"local.setDeclineRMInvoiceCheckBox()\"", ""));
            %>
        </td>
        <td align="center" valign="top">
            &nbsp;
        </td>
        <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
        <td>&nbsp;</td>
    </tr>
	<%-- //#RKAZI CR1006 04-24-2015  - END --%>
    <tr>
      <td align="center" valign="top">
        <%
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgCreditNoteInd)))
        {
           defaultCheckBoxValue = true;

           if ("".equals(corporateOrgCreditNoteDualInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_NOT_REQ.equals(corporateOrgCreditNoteDualInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = true;
           }
          else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgCreditNoteDualInd))
           {
              defaultRadioButtonValue1 = true;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgCreditNoteDualInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = true;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgCreditNoteDualInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = true;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = true;
              defaultRadioButtonValue5 = false;
           }
        }
        else
        {
           defaultCheckBoxValue     = false;
		   defaultRadioButtonValue1 = false;
           defaultRadioButtonValue2 = false;
           defaultRadioButtonValue3 = false;
           defaultRadioButtonValue4 = false;
           defaultRadioButtonValue5 = false;
        }

            out.print(widgetFactory.createCheckboxField("CreditNoteIndicator", "",
                defaultCheckBoxValue, isReadOnly, false,
                "onClick=\"local.setRMCreditNoteRadioButtons()\"", "", "none"));
       %>
      </td>
      <td valign="top">
      <%= widgetFactory.createSubLabel("CorpCust.CreditNoteAuthorisation") %>
      <br>
          <%
          if ((!insertModeFlag || errorFlag))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("CreditNoteDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "CRED", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setARMCreditNoteCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
          out.print(widgetFactory.createRadioButtonField("CreditNoteDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "CRED", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setARMCreditNoteCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("CreditNoteDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "CRED", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setARMCreditNoteCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("CreditNoteDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "CRED", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setARMCreditNoteCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","CREDIT_AUTH_IND",corporateOrg.getAttribute("credit_auth_panel_group_oid"));
  		out.print(widgetFactory.createSelectField("CreditAuthorizationPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setRMCreditNotePanelRadioButton()\"" , "", "none"));
        %>
      </td>
      <td align="center" valign="top">
      <%
      out.print(widgetFactory.createRadioButtonField("CreditNoteDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_NOT_REQ", "", TradePortalConstants.DUAL_AUTH_NOT_REQ, defaultRadioButtonValue5,
                isReadOnly,
                "onClick=\"local.setARMCreditNoteCheckBox()\"", ""));
      %>
      </td>
    </tr>
  </tbody>
  </table>

  <br><br>
  <%--Rel 9.0 CR913 Begin --%>
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 26%;">
      <col span="1" style="width: 13%;">
      <col span="1" style="width: 13%;">
      <col span="1" style="width: 18%;">
      <col span="1" style="width: 13%;">
      <col span="1" style="width: 12%;">
    </colgroup>
    <thead>
      <tr>
        <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeOneUser",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoUsers",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.AuthorizeTwoWorkGroups",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="center"><%=resMgr.getText("CorpCust.PanelAuth",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="left"><%=resMgr.getText("CorpCust.AuthorizationNotReq",TradePortalConstants.TEXT_BUNDLE)%></th>
      </tr>
    </thead>
    <thead>
      <th class="tableSubHeader withCheckBoxColumn" colspan="7">
        <%=resMgr.getText("CorpCust.PayablesManagement",TradePortalConstants.TEXT_BUNDLE)%>
      </th>
    </thead>
    <tbody>
            <tr>
                  <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAuthInd)))
           {
              defaultCheckBoxValue = true;

              if ("".equals(corporateOrgDualPayInvAuthUploadedInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
             else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDualPayInvAuthUploadedInd))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDualPayInvAuthUploadedInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDualPayInvAuthUploadedInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

            out.print(widgetFactory.createCheckboxField("PayablesInvoiceUploadedAuthIndicator", "",
                defaultCheckBoxValue, isReadOnly, false,
                "onClick=\"local.setPayAuthRadioButtons()\"", "", "none"));
       %>
      </td>
      <td valign="top">
      <%= widgetFactory.createSubLabel("CorpCust.PayablesInvoiceAuthorisation") %>
      <br>
          <%
          if ((!insertModeFlag || errorFlag))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
     </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("PayablesUploadInvDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "PAYAUTH", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setPayAuthCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
          out.print(widgetFactory.createRadioButtonField("PayablesUploadInvDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "PAYAUTH", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setPayAuthCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("PayablesUploadInvDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "PAYAUTH", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setPayAuthCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("PayablesUploadInvDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "PAYAUTH", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setPayAuthCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
        panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","PAY_UPL_INV_AUTH_IND",corporateOrg.getAttribute("upl_pay_inv_panel_group_oid"));
        out.print(widgetFactory.createSelectField("UploadedPayablesAuthPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setRMPanelRadioButton()\"" , "", "none"));
        %>
      </td>
      <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
      <td>&nbsp;</td>
    </tr>
	<%-- //#RKAZI CR1006 04-24-2015  - BEGIN --%>
    <tr>
       <td valign="top">
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgDecPAYInvoiceInd)))
           {
              defaultCheckBoxValue = true;

              if ("".equals(corporateOrgDecPAYInvoiceDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
             else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDecPAYInvoiceDualInd))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDecPAYInvoiceDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDecPAYInvoiceDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
                 defaultRadioButtonValue4 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
                 defaultRadioButtonValue4 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
           }

            out.print(widgetFactory.createCheckboxField("PayablesInvoiceDeclineIndicator", "",
                defaultCheckBoxValue, isReadOnly, false,
                "onClick=\"local.setPayMgmtDeclineRadioButtons()\"", "", "none"));
       %>
      </td>
      <td valign="top">
      <%= widgetFactory.createSubLabel("CorpCust.PayablesMgmtInvoiceDecline") %>
      <br>
          <%
          if ((!insertModeFlag || errorFlag))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
     </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("PayablesInvDeclineDualIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "PAYMGMTDEC", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setPayMgmtDeclineCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
          out.print(widgetFactory.createRadioButtonField("PayablesInvDeclineDualIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "PAYMGMTDEC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setPayMgmtDeclineCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("PayablesInvDeclineDualIndicator",
                   "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "PAYMGMTDEC", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                   isReadOnly,
                   "onClick=\"local.setPayMgmtDeclineCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">&nbsp;</td>
      <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
      <td>&nbsp;</td>
    </tr>
	<%-- //#RKAZI CR1006 04-24-2015  - END --%>
        <tr>
                <td valign="top">
                    <%
                        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvMgmtInd)))
                        {
                            defaultCheckBoxValue = true;

                            if ("".equals(corporateOrgDualPayInvAuthInd))
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = false;
                            }
                            else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDualPayInvAuthInd))
                            {
                                defaultRadioButtonValue1 = true;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = false;
                            }
                            else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDualPayInvAuthInd))
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = true;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = false;
                            }
                            else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDualPayInvAuthInd))
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = true;
                                defaultRadioButtonValue4 = false;
                            }
                            else
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = true;
                            }
                        }
                        else
                        {
                            defaultCheckBoxValue     = false;
                            defaultRadioButtonValue1 = false;
                            defaultRadioButtonValue2 = false;
                            defaultRadioButtonValue3 = false;
                            defaultRadioButtonValue4 = false;
                        }

                        out.print(widgetFactory.createCheckboxField("PayablesInvoiceAuthIndicator", "",
                                defaultCheckBoxValue, isReadOnly, false,
                                "onClick=\"local.setPayMgmtRadioButtons()\"", "", "none"));
                    %>
                </td>
                <td valign="top">
                    <%= widgetFactory.createSubLabel("CorpCust.PayablesMgmtInvoiceUpdates") %>
                    <br>
                    <%
                        if ((!insertModeFlag || errorFlag))
                        {
                            defaultCheckBoxValue = true;
                        }
                        else
                        {
                            defaultCheckBoxValue = false;
                        }
                    %>
                </td>
                <td align="center" valign="top">
                    <%
                        out.print(widgetFactory.createRadioButtonField("PayablesInvAuthDualAuthorizationIndicator",
                                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "PAYMGMT", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                                isReadOnly,
                                "onClick=\"local.setPayMgmtCheckBox()\"", ""));
                    %>
                </td>
                <td align="center" valign="top">
                    <%
                        out.print(widgetFactory.createRadioButtonField("PayablesInvAuthDualAuthorizationIndicator",
                                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "PAYMGMT", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                                isReadOnly,
                                "onClick=\"local.setPayMgmtCheckBox()\"", ""));

                    %>
                </td>
                <td align="center" valign="top">
                    <%
                        out.print(widgetFactory.createRadioButtonField("PayablesInvAuthDualAuthorizationIndicator",
                                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "PAYMGMT", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                                isReadOnly,
                                "onClick=\"local.setPayMgmtCheckBox()\"", ""));
                    %>
                </td>
                <td align="center" valign="top">
                    <%
                        out.print(widgetFactory.createRadioButtonField("PayablesInvAuthDualAuthorizationIndicator",
                                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "PAYMGMT", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                                isReadOnly,
                                "onClick=\"local.setPayMgmtCheckBox()\"", "")+"<br>");
                        panelOptions = "<option  value=\"\"></option>";
                        panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","PAY_MGM_INV_AUTH_IND",corporateOrg.getAttribute("pay_inv_panel_group_oid"));
                        out.print(widgetFactory.createSelectField("PayablesAuthPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setRMPanelRadioButton()\"" , "", "none"));
                    %>
                </td>
                <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
                <td>&nbsp;</td>
            </tr>
    <%--Rel 9.2 CR 914A START--%>
    <tr>
      <td align="center" valign="top">
        <%
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrNoteMgmtInd)))
        {
           defaultCheckBoxValue = true;

           if ("".equals(corporateOrgDualPayCrNoteAuthInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_NOT_REQ.equals(corporateOrgDualPayCrNoteAuthInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = true;
           }
          else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDualPayCrNoteAuthInd))
           {
              defaultRadioButtonValue1 = true;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDualPayCrNoteAuthInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = true;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgDualPayCrNoteAuthInd))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = true;
              defaultRadioButtonValue4 = false;
              defaultRadioButtonValue5 = false;
           }
           else 
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
              defaultRadioButtonValue4 = true;
              defaultRadioButtonValue5 = false;
           }
        }
        else
        {
           defaultCheckBoxValue     = false;
		   defaultRadioButtonValue1 = false;
           defaultRadioButtonValue2 = false;
           defaultRadioButtonValue3 = false;
           defaultRadioButtonValue4 = false;
           defaultRadioButtonValue5 = false;
        }

            out.print(widgetFactory.createCheckboxField("AllowPayCrNote", "",
                defaultCheckBoxValue, isReadOnly, false,
                "onClick=\"local.setPMCreditNoteRadioButtons()\"", "", "none"));
       %>
      </td>
      <td valign="top">
      <%= widgetFactory.createSubLabel("CorpCust.CreditNoteAuthorisation") %>
      <br>
          <%
          if ((!insertModeFlag || errorFlag))
             {
                defaultCheckBoxValue = true;
             }
             else
             {
                defaultCheckBoxValue = false;
             }
           %>
          
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("DualAuthPayCrNote",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "CREDPAY", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setPMCreditNoteCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
          out.print(widgetFactory.createRadioButtonField("DualAuthPayCrNote",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "CREDPAY", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setPMCreditNoteCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("DualAuthPayCrNote",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "CREDPAY", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setPMCreditNoteCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("DualAuthPayCrNote",
                "TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH" + "CREDPAY", "", TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH, defaultRadioButtonValue4,
                isReadOnly,
                "onClick=\"local.setPMCreditNoteCheckBox()\"", "")+"<br>");
        panelOptions = "<option  value=\"\"></option>";
  		panelOptions += ListBox.createPanelGroupOptionList(panelAuthgroupList, "PANEL_AUTH_GROUP_OID", "PAGID","PAY_CREDIT_NOTE_AUTH_IND",corporateOrg.getAttribute("pay_cr_note_auth_panel_grp_oid"));
  		out.print(widgetFactory.createSelectField("PayCrNoteAuthPanelGroupOid", "CorpCust.PanelGroupId","", panelOptions, isReadOnly, false, false, " required=\"false\" class=\"char10\" onChange=\"local.setPMCreditNotePanelRadioButton()\"" , "", "none"));
        %>
      </td>
      <td align="center" valign="top">
      <%
      out.print(widgetFactory.createRadioButtonField("DualAuthPayCrNote",
                "TradePortalConstants.DUAL_AUTH_NOT_REQ"+"CREDPAY", "", TradePortalConstants.DUAL_AUTH_NOT_REQ, defaultRadioButtonValue5,
                isReadOnly,
                "onClick=\"local.setPMCreditNoteCheckBox()\"", ""));
      %>
      </td>
    </tr>
    <%--Rel9.2 CR 914A END--%>
	<%-- //#RKAZI CR1006 04-24-2015  - BEGIN --%>
        <tr>
                <td align="center" valign="top">
                    <% 
                        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayDecCRNInd)))
                        {
                            defaultCheckBoxValue = true;

                            if ("".equals(corporateOrgPayDecCRNDualInd))
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = false;
                                defaultRadioButtonValue5 = false;
                            }
                            else if (TradePortalConstants.DUAL_AUTH_NOT_REQ.equals(corporateOrgPayDecCRNDualInd))
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = false;
                                defaultRadioButtonValue5 = true;
                            }
                            else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgPayDecCRNDualInd))
                            {
                                defaultRadioButtonValue1 = true;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = false;
                                defaultRadioButtonValue5 = false;
                            }
                            else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgPayDecCRNDualInd))
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = true;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = false;
                                defaultRadioButtonValue5 = false;
                            }
                            else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrgPayDecCRNDualInd))
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = true;
                                defaultRadioButtonValue4 = false;
                                defaultRadioButtonValue5 = false;
                            }
                            else
                            {
                                defaultRadioButtonValue1 = false;
                                defaultRadioButtonValue2 = false;
                                defaultRadioButtonValue3 = false;
                                defaultRadioButtonValue4 = true;
                                defaultRadioButtonValue5 = false;
                            }
                        }
                        else
                        {
                            defaultCheckBoxValue     = false;
                            defaultRadioButtonValue1 = false;
                            defaultRadioButtonValue2 = false;
                            defaultRadioButtonValue3 = false;
                            defaultRadioButtonValue4 = false;
                            defaultRadioButtonValue5 = false;
                        }
                        out.print(widgetFactory.createCheckboxField("DeclinePayCrNote", "",
                                defaultCheckBoxValue, isReadOnly, false,
                                "onClick=\"local.setPMDeclineCreditNoteRadioButtons()\"", "", "none"));
                    %>
                </td>
                <td valign="top">
                    <%= widgetFactory.createSubLabel("CorpCust.CreditNoteDecline") %>
                    <br>
                    <%
                        if ((!insertModeFlag || errorFlag))
                        {
                            defaultCheckBoxValue = true;
                        }
                        else
                        {
                            defaultCheckBoxValue = false;
                        }
                    %>

                </td>
                <td align="center" valign="top">
                    <%
                        out.print(widgetFactory.createRadioButtonField("DualDeclinePayCrNote",
                                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "DECCREDPAY", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                                isReadOnly,
                                "onClick=\"local.setPMDeclineCreditNoteCheckBox()\"", ""));
                    %>
                </td>
                <td align="center" valign="top">
                    <%
                        out.print(widgetFactory.createRadioButtonField("DualDeclinePayCrNote",
                                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "DECCREDPAY", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                                isReadOnly,
                                "onClick=\"local.setPMDeclineCreditNoteCheckBox()\"", ""));

                    %>
                </td>
                <td align="center" valign="top">
                    <%
                        out.print(widgetFactory.createRadioButtonField("DualDeclinePayCrNote",
                                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "DECCREDPAY", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                                isReadOnly,
                                "onClick=\"local.setPMDeclineCreditNoteCheckBox()\"", ""));
                    %>
                </td>
                <td align="center" valign="top">&nbsp;</td>
                <td align="center" valign="top">&nbsp;</td>
            </tr>
			<%-- //#RKAZI CR1006 04-24-2015  - END --%>
  </tbody>
  </table>
  <br><br>
  <%--Rel 9.0 CR913 End --%>
  <%--AAlubala - 12/13/2012
  Rel8.2 CR741 Begin  
  Set the ERP Reporting Radio Buttons and checkbox
  --%>
  <%=widgetFactory.createWideSubsectionHeader("CorpCust.ERPTransactionReportingHeader") %>  
       		<%
       		//corporateErpReporting
        if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateErpReporting)))
        {
           defaultCheckBoxValue = true;

           if (StringFunction.isBlank(corporateDiscountCodeType))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
           }
           else if (TradePortalConstants.BANK_AND_CUSTOMER_ERP_CODES.equals(corporateDiscountCodeType))
           {
              defaultRadioButtonValue1 = true;
              defaultRadioButtonValue2 = false;
           }           
           else if (TradePortalConstants.CUSTOMER_ONLY_ERP_CODES.equals(corporateDiscountCodeType))
           {
              defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = true;
           }
        }
        
        //Rel 8.2 IR T36000016083 - Start
        //Prevent checkbox remaiing checked when it is unchecked value in db
        if(TradePortalConstants.INDICATOR_NO.equals(corporateErpReporting)){
        	defaultCheckBoxValue = false;
            defaultRadioButtonValue1 = false;
            defaultRadioButtonValue2 = false;        	
        }
      //Rel 8.2 IR T36000016083 - End	
       		out.print(widgetFactory.createCheckboxField("ERPTransactionIndicator", "CorpCust.ERPTransactionReporting",
       				defaultCheckBoxValue, isReadOnly, false,
       	                	"onClick=\"setERPRadioButtons()\"", "", ""));       		
				%>
				
				<%
             	out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createRadioButtonField("ERPTransactionSelector",
                  	"BankAndCustomer", "CorpCust.BankAndCustomer", TradePortalConstants.BANK_AND_CUSTOMER_ERP_CODES, defaultRadioButtonValue1,
                  	isReadOnly,
                	"onClick=\"setERPTransactionCheckBox\"", ""));
             	%>
				<br>
				<%
             	out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createRadioButtonField("ERPTransactionSelector",
             		"CustomerOnly", "CorpCust.CustomerOnly", TradePortalConstants.CUSTOMER_ONLY_ERP_CODES, defaultRadioButtonValue2,
                  	isReadOnly,
                	"onClick=\"setERPTransactionCheckBox\"", ""));				
  %>
       	<br> 
 
 <%--CR741 - END --%>
  <br><br>

  <%-- NARAYAN Beign--%>
  <table width="100%" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 5%;">
      <col span="1" style="width: 35%;">
      <col span="1" style="width: 20%;">
      <col span="1" style="width: 20%;">
      <col span="1" style="width: 20%;">
    </colgroup>
    <thead>
      <tr>
        <%--cquinton 11/10/2012 no content causes table to render odd in ie7. add space as content--%>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <%-- SPenke T36000006033 10/11/2012 Begin --%>
        <th align="left"><%=resMgr.getText("CorpCust.AuthorizeOneUser",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="left"><%=resMgr.getText("CorpCust.AuthorizeTwoUsers",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="left"><%=resMgr.getText("CorpCust.AuthorizeTwoWorkGroups",TradePortalConstants.TEXT_BUNDLE)%></th>
      </tr>
    </thead>
    <thead>
      <th class="tableSubHeader withCheckBoxColumn" colspan="6">
        <%=resMgr.getText("CorpCust.DirectDebits",TradePortalConstants.TEXT_BUNDLE)%>            
      </th>
      <%-- SPenke T36000006033 10/11/2012 End --%>
  </thead>
  <tbody>
    <tr>
      <td>
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgDDInd)))
           {
              defaultCheckBoxValue = true;

              if ("".equals(corporateOrgDDDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
              }
             else if (TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrgDDDualInd))
              {
                 defaultRadioButtonValue1 = true;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = false;
              }
              else if (TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrgDDDualInd))
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = true;
                 defaultRadioButtonValue3 = false;
              }
              else
              {
                 defaultRadioButtonValue1 = false;
                 defaultRadioButtonValue2 = false;
                 defaultRadioButtonValue3 = true;
              }
           }
           else
           {
              defaultCheckBoxValue     = false;
                    defaultRadioButtonValue1 = false;
              defaultRadioButtonValue2 = false;
              defaultRadioButtonValue3 = false;
           }

            out.print(widgetFactory.createCheckboxField("DirectDebitsIndicator", "",
                defaultCheckBoxValue, isReadOnly, false,
                "onClick=\"local.setDDRadioButtons()\"", "", "none"));
       %>
      </td>
      <td>
            <%= widgetFactory.createSubLabel("CorpCust.DirectDebits") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("DirectDebitsDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_ONE_USER" + "DD", "", TradePortalConstants.DUAL_AUTH_REQ_ONE_USER, defaultRadioButtonValue1,
                isReadOnly,
                "onClick=\"local.setDDCheckBox()\"", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("DirectDebitsDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS" + "DD", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS, defaultRadioButtonValue2,
                isReadOnly,
                "onClick=\"local.setDDCheckBox()\"", ""));

        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("DirectDebitsDualAuthorizationIndicator",
                "TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS" + "DD", "", TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS, defaultRadioButtonValue3,
                isReadOnly,
                "onClick=\"local.setDDCheckBox()\"", ""));
        %>
      </td>
  </tbody>
  </table>
  <br><br>
  <%-- NARAYAN END--%>

  <%-- CR-710 NARAYAN BEGIN--%>
  <%=widgetFactory.createWideSubsectionHeader("CorpCust.InvoiceManagement") %>
  <div class="columnLeft">
  <%=widgetFactory.createSubsectionHeader("CorpCust.InvoicesGeneral") %>  
    <div class="formItem">
      <%= widgetFactory.createSubLabel("CorpCust.Invoices")%>
<%
  if ((!insertModeFlag || errorFlag) && corporateOrgARMFileuploadInd != null ) {
    if (TradePortalConstants.INVOICES_UPLOAD_FILE.equals(corporateOrgARMFileuploadInd)) {
      InvoiceManagementIndicatorValue1 = true;
      InvoiceManagementIndicatorValue2 = false;
    }
    else if (TradePortalConstants.INVOICES_NOT_UPLOAD_FILE.equals(corporateOrgARMFileuploadInd)) {
      InvoiceManagementIndicatorValue1 = false;
      InvoiceManagementIndicatorValue2 = true;
    }
    else {
      InvoiceManagementIndicatorValue1 = false;
      InvoiceManagementIndicatorValue2 = false;
    }
  }else{
    	InvoiceManagementIndicatorValue1 = false;
        InvoiceManagementIndicatorValue2 = false;
  }
  
  
%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("InvoiceManagementIndicator", "InvoicesNotUploadedFromFiles", "CorpCust.InvoicesNotUploadedFromFiles", TradePortalConstants.INVOICES_NOT_UPLOAD_FILE, InvoiceManagementIndicatorValue2,
      	isReadOnly,
    	"onClick=\"local.clearInvoiceFields()\"", "")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("InvoiceManagementIndicator", "InvoicesFromUploadedFiles", "CorpCust.InvoicesFromUploadedFiles", TradePortalConstants.INVOICES_UPLOAD_FILE, InvoiceManagementIndicatorValue1,
      	isReadOnly,
    	"onClick=\"local.clearInvoiceFields()\"", "")%> 
    	<br>
    	<%=widgetFactory.createCheckboxField("PayablesInvoicesIndicator", "CorpCust.PayablesInvoices",
        		TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadPayInd), isReadOnly, false,
                "onClick=\"local.setInvoiceUploadIndicator()\"", "", "inline") %>
        <%=widgetFactory.createCheckboxField("ReceivablesInvoicesIndicator", "CorpCust.ReceivablesInvoices",
        		TradePortalConstants.INDICATOR_YES.equals(corporateOrgInvFileUploadRecInd), isReadOnly, false,
                "onClick=\"local.setInvoiceUploadIndicator()\"", "", "inline") %>       
      <div style="clear: both;"></div>
      

      <%= widgetFactory.createSubLabel("CorpCust.FailedInvText")%>
      <div style="clear: both;"></div>
	<%
	//DK IR - T36000011610 Rel8.2 02/14/2013 Starts
	if ((!insertModeFlag || errorFlag) && failedInvoiceInd != null )
	{
	     if (TradePortalConstants.INDICATOR_NO.equals(failedInvoiceInd))
	    {
	         defaultRadioButtonValue1 = true;
	         defaultRadioButtonValue2 = false;
	    }
	     else if (TradePortalConstants.INDICATOR_YES.equals(failedInvoiceInd))
	   {
	         defaultRadioButtonValue1 = false;
	         defaultRadioButtonValue2 = true;
	   }else
	   {
	       defaultRadioButtonValue1 = false;
	       defaultRadioButtonValue2 = false;
	         }
	}
	else
	{
	    defaultRadioButtonValue1 = false;
	    defaultRadioButtonValue2 = false;
	     }
	if(InvoiceManagementIndicatorValue2==true){
	    defaultRadioButtonValue1 = false;
	    defaultRadioButtonValue2 = false;
	}
	//DK IR - T36000011610 Rel8.2 02/14/2013 Ends
	%>
      <%= widgetFactory.createRadioButtonField("FailedInvoiceIndicator", "FailedInvoiceIndicator.UploadAndGroupSuccessful",
            "CorpCust.FailedInv.UploadAndGroupSuccessful",
            TradePortalConstants.INDICATOR_NO,
            defaultRadioButtonValue1, isReadOnly,
            "", "")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("FailedInvoiceIndicator", "FailedInvoiceIndicator.FailAll",
            "CorpCust.FailedInv.FailAll",
            TradePortalConstants.INDICATOR_YES,
            defaultRadioButtonValue2, isReadOnly,
            "", "")%>
    <%-- DK - CR708D Rel8.2 10/31/2012 Begin --%> 
    <br>
    
  <div style="clear: both;"></div>
 <br>
 <%= widgetFactory.createSubLabel("CorpCust.RestrictedInvoiceUploadDirectory")%>
 <%
                 if ((!insertModeFlag || errorFlag) && corporateOrgRestrictedInvoicesUploadInd != null )
                 {
                 	 if (TradePortalConstants.INVOICE_UPLOAD_NOT_RESTRICTED_DIRECTORY .equals(corporateOrgRestrictedInvoicesUploadInd))
                     {
                        defaultRadioButtonValue1 = true;
                        defaultRadioButtonValue2 = false;
                     }
                	 else if (TradePortalConstants.INVOICE_UPLOAD_RESTRICTED_DIRECTORY .equals(corporateOrgRestrictedInvoicesUploadInd))
                    {
                       defaultRadioButtonValue1 = false;
                       defaultRadioButtonValue2 = true;
                    }else
                    {
                   	   defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = false;
           			 }
                 }
                 else
                 {
              	   defaultRadioButtonValue1 = false;
                   defaultRadioButtonValue2 = false;
      			 }
   %>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("RestrictedInvoiceUploadIndicator", "InvoiceUploadsNotRestrictedDir", "CorpCust.InvoiceUploadsNotRestrictedDir", TradePortalConstants.INVOICE_UPLOAD_NOT_RESTRICTED_DIRECTORY, defaultRadioButtonValue1,
      	isReadOnly,
    	"onClick=\"local.clearInvoiceUploadDir()\"", "")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("RestrictedInvoiceUploadIndicator", "InvoiceUploadsRestrictedDir", "CorpCust.InvoiceUploadsRestrictedDir", TradePortalConstants.INVOICE_UPLOAD_RESTRICTED_DIRECTORY, defaultRadioButtonValue2,
      	isReadOnly,
    	"onClick=\"local.clearInvoiceUploadDir()\"", "")%>
      <div style="clear: both;"></div>
      <div class="indentHalf">
        <%= widgetFactory.createTextField( "SpecifyRestrictedInvoiceUploadDir", "",corporateOrgRestrictedInvoicesUploadDirectory, "200", isReadOnly, false, false,  "onkeyup=\"local.setRestrictedInvoiceUploadIndicator()\" width=\"30\"", "","" ) %>
      </div>
      
      <%= widgetFactory.createSubLabel("CorpCust.ACInvoiceUpload")%>
    <br>
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgAutoLoanRequestInvUpldInd)))
             defaultCheckBoxValue = true;
          else
             defaultCheckBoxValue  = false;                  
           out.print(widgetFactory.createCheckboxField("LoanRequestsFromInvoiceUploadIndicator", "CorpCust.AutoCreationOfLoanReqFromInvUpload",
             defaultCheckBoxValue, isReadOnly, false,
             "", "", "none"));
        %>
        <br>
        <%
           if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgAutoATPInvUpldInd)))
             defaultCheckBoxValue = true;
          else
             defaultCheckBoxValue  = false;                  
           out.print(widgetFactory.createCheckboxField("ATPINVOnlyFromInvoiceUploadInd", "CorpCust.AutoCreationOfATPInvOnlyFromInvUpload",
             defaultCheckBoxValue, isReadOnly, false,
             "", "", "none"));
        %>
        <div style="clear: both;"></div>
    </div>
  <div style="clear:both;"></div>
  </div>
  <div class="columnRight">
  
  <%=widgetFactory.createSubsectionHeader("CorpCust.PayablesInvoices") %> 
  <%=widgetFactory.createCheckboxField("PayablesInvoicesGrouped", "CorpCust.PayablesInvoicesGrouped",
		  TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvGroupingInd), isReadOnly, false,
                "", "", "") %>
  <%=widgetFactory.createCheckboxField("PayablesInvoicesIntegrated", "CorpCust.PayablesInvoicesIntegrated",
		  TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvUtilisedInd), isReadOnly, false,
                "", "", "") %>  
				<%-- //#RKAZI CR1006 04-24-2015  - BEGIN --%>
  <%=widgetFactory.createCheckboxField("PayablesAllowH2HPortalApproval", "CorpCust.PayablesAllowH2HPortalApproval",
          TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayInvAllowH2hApprovalInd), isReadOnly, false,
                "", "", "") %>        
					<%-- //#RKAZI CR1006 04-24-2015  - END --%>
  <%=widgetFactory.createSubsectionHeader("CorpCust.ReceivablesInvoices") %>  
  <%=widgetFactory.createCheckboxField("ReceivablesProgramUtilised", "CorpCust.ReceivablesProgramUtilised",
		  TradePortalConstants.INDICATOR_YES.equals(corporateOrgRecInvUtilisedInd), isReadOnly, false,
                "", "", "") %> 
	<%-- //#RKAZI CR1006 04-24-2015  - BEGIN --%>
  <%=widgetFactory.createCheckboxField("ReceivablesAllowH2HPortalApproval", "CorpCust.ReceivablesAllowH2HPortalApproval",
          TradePortalConstants.INDICATOR_YES.equals(corporateOrgRecInvAllowH2hApprovalInd), isReadOnly, false,
                "", "", "") %>        
	<%-- //#RKAZI CR1006 04-24-2015  - END --%>
    <div class="formItem">
      <%= widgetFactory.createSubLabel("CorpCust.RecInvoices")%>
<%
                if ((!insertModeFlag || errorFlag) && corporateOrgInterestDiscountInd != null )
                 {
                 	 if (TradePortalConstants.INTEREST_COLLECTED_FINANCE_AMOUNT .equals(corporateOrgInterestDiscountInd))
                     {
                        InterestFinanceAmountIndicator1 = true;
                        InterestFinanceAmountIndicator2 = false;
                     }
                	 else if (TradePortalConstants.DISCOUNT_NETTED_FINANCE_AMOUNT .equals(corporateOrgInterestDiscountInd))
                    {
                       InterestFinanceAmountIndicator1 = false;
                       InterestFinanceAmountIndicator2 = true;
                    }else
                    {
                   	   InterestFinanceAmountIndicator1 = false;
                        InterestFinanceAmountIndicator2 = false;
           			 }
                 }
                 else
                 {
              	   InterestFinanceAmountIndicator1 = false;
                   InterestFinanceAmountIndicator2 = false;
      			 }
				 if(InvoiceManagementIndicatorValue2==true){
					 InterestFinanceAmountIndicator1 = false;
					 InterestFinanceAmountIndicator2 = false;
				 }
%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("InterestFinanceAmountIndicator", "InterestCollectedAddedToFA", "CorpCust.InterestCollectedAddedToFA", TradePortalConstants.INTEREST_COLLECTED_FINANCE_AMOUNT, InterestFinanceAmountIndicator1,
      	isReadOnly,
    	"onClick=\"onClick=local.clearInvoiceDiscountFields()\"", "")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("InterestFinanceAmountIndicator", "DiscountNettedFromFA", "CorpCust.DiscountNettedFromFA", TradePortalConstants.DISCOUNT_NETTED_FINANCE_AMOUNT, InterestFinanceAmountIndicator2,
      	isReadOnly,
    	"onClick=\"local.clearInvoiceDiscountFields()\"", "")%>
      <div style="clear: both;"></div>
      <br>

      <%= widgetFactory.createSubLabel("CorpCust.CalculateDiscountUsing")%>
<%
                 if ((!insertModeFlag || errorFlag) && corporateOrgCalculateDiscountInd != null )
                 {

                 	 if (TradePortalConstants.STRAIGHT_DISCOUNT .equals(corporateOrgCalculateDiscountInd))
                     {
                        defaultRadioButtonValue1 = true;
                        defaultRadioButtonValue2 = false;
                     }
                	 else if (TradePortalConstants.DISCOUNT_YIELD .equals(corporateOrgCalculateDiscountInd))
                    {
                       defaultRadioButtonValue1 = false;
                       defaultRadioButtonValue2 = true;
                    }else
                    {
                   	   defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = false;
           			 }
		//Srinivasu IR-KIUL121454215 12/28/2011 Start
		 if(corporateOrgInterestDiscountInd!=null && TradePortalConstants.INTEREST_COLLECTED_FINANCE_AMOUNT.equals(corporateOrgInterestDiscountInd)){
			if(TradePortalConstants.STRAIGHT_DISCOUNT.equals(corporateOrgCalculateDiscountInd) || TradePortalConstants.DISCOUNT_YIELD.equals(corporateOrgCalculateDiscountInd)){

				defaultRadioButtonValue1=false;
				defaultRadioButtonValue2=false;

				}
			}
		//Srinivasu IR-KIUL121454215 12/28/2011 End
                 }
                 else
                 {
              	   defaultRadioButtonValue1 = false;
                   defaultRadioButtonValue2 = false;
      			 }
				 if(InvoiceManagementIndicatorValue2==true){
					 defaultRadioButtonValue1 = false;
					 defaultRadioButtonValue2 = false;
				 }
%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("CalculateDiscountIndicator", "StraightDiscount", "CorpCust.StraightDiscount", TradePortalConstants.STRAIGHT_DISCOUNT, defaultRadioButtonValue1,
      	isReadOnly,
    	"onClick=\"local.setNettedDiscountFiled()\"", "")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("CalculateDiscountIndicator", "DiscountToYield", "CorpCust.DiscountToYield", TradePortalConstants.DISCOUNT_YIELD, defaultRadioButtonValue2,
      	isReadOnly,
    	"onClick=\"local.setNettedDiscountFiled()\"", "")%>
      <div style="clear: both;"></div>
    </div>

    <%--cquinton 11/14/2012 cr708 start--%>
    <div class="formItem">
    <div style="clear: both;"></div>
    
      <%= widgetFactory.createSubLabel("CorpCust.WhileUploadingInvoices")%>
<%
           if ((!insertModeFlag || errorFlag) && corporateOrgGRInvoicesUploadInd != null )
                 {
                    if (TradePortalConstants.INVOICES_NOT_GROUPED_TPR.equals(corporateOrgGRInvoicesUploadInd))
                     {
                        //RKazi IR MUM020844303 Rel 8.0 02/17/2012 reversed the flags from true to false and vice-versa
                        defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = true;
                     }
                     else if (TradePortalConstants.INVOICES_GROUPED_TPR.equals(corporateOrgGRInvoicesUploadInd))
                    {
                        //RKazi IR MUM020844303 Rel 8.0 02/17/2012 reversed the flags from true to false and vice-versa
                        defaultRadioButtonValue1 = true;
                        defaultRadioButtonValue2 = false;
                    }else
                    {
                       defaultRadioButtonValue1 = false;
                        defaultRadioButtonValue2 = false;
                     }
            }
            else
            {
                   defaultRadioButtonValue1 = false;
                   defaultRadioButtonValue2 = false;
            }
           if(InvoiceManagementIndicatorValue2==true){
                       defaultRadioButtonValue1 = false;
                       defaultRadioButtonValue2 = false;
           }
    %>
    <div style="clear: both;"></div>
    <%= widgetFactory.createRadioButtonField("GRInvoiceIndicator", "InvoicesGroupedFromTPR", "CorpCust.InvoicesGroupedFromTPR", TradePortalConstants.INVOICES_GROUPED_TPR, defaultRadioButtonValue1,
        isReadOnly,
        "", "")%>
  <div style="clear: both;"></div>
   <%= widgetFactory.createRadioButtonField("GRInvoiceIndicator", "InvoicesNotGroupedFromTPR", "CorpCust.InvoicesNotGroupedFromTPR", TradePortalConstants.INVOICES_NOT_GROUPED_TPR, defaultRadioButtonValue2,
        isReadOnly,
        "", "")%>  
    <br>
    <%-- DK - CR708D Rel8.1 10/31/2012 End --%>
    </div>
    <div style="clear: both;"></div>
    <%--cquinton 11/14/2012 cr708 end--%>

  </div>
<%-- DK - CR709 Rel8.1 10/31/2012 Begin --%>
 <div style="clear: both;"></div>
 <%=widgetFactory.createWideSubsectionHeader("CorpCust.TradeLoanInvoiceRules") %>
 <%

                 if ((!insertModeFlag || errorFlag) && corporateOrgUsePaymentDateAllowedInd != null )
                 {
                 	if (TradePortalConstants.INDICATOR_YES.equals(corporateOrgUsePaymentDateAllowedInd))
                     {
                 		UsePaymentDateAllowedIndicatorValue1 = true;
                 		UsePaymentDateAllowedIndicatorValue2 = false;
                     }
                	 else if (TradePortalConstants.INDICATOR_NO.equals(corporateOrgUsePaymentDateAllowedInd))
                    {
                		 UsePaymentDateAllowedIndicatorValue1 = false;
                		 UsePaymentDateAllowedIndicatorValue2 = true;
                    }else
                    {
                    	UsePaymentDateAllowedIndicatorValue1 = false;
                    	UsePaymentDateAllowedIndicatorValue2 = false;
           			 }
                 }
                 else
                 {
                	 UsePaymentDateAllowedIndicatorValue1 = false;
                	 UsePaymentDateAllowedIndicatorValue2 = false;
      			 }

   %>
 <div class="columnLeft">
 <div class="formItem">
   	<%=
    	widgetFactory.createSubLabel("CorpCust.UsePaymentDateAllow")
	%>
	<br>
	<div style="clear: both;"></div>
  <%= widgetFactory.createRadioButtonField("UsePaymentDateAllowedInd", "YesPaymentDateUsed", "CorpCust.YesPaymentDateUsed", TradePortalConstants.INDICATOR_YES, UsePaymentDateAllowedIndicatorValue1,
      	isReadOnly,"", "")%>
      	<div style="clear: both;"></div>
    <%= widgetFactory.createRadioButtonField("UsePaymentDateAllowedInd", "NoInvoiceDueDateUsed", "CorpCust.NoInvoiceDueDateUsed", TradePortalConstants.INDICATOR_NO, UsePaymentDateAllowedIndicatorValue2,
      	isReadOnly,"", "")%>
	<br>
	<br>
	<div style="clear: both;"></div>
	<%=
     	widgetFactory.createLabel("CorpCust.PaymentDate","CorpCust.PaymentDate",isReadOnly, false, false, "")
	%>
	<%--Used Table to resolve IR #T36000016895 RPasupulati Starts----%>
	<table><tr><td>	
	<%= widgetFactory.createTextField( "DaysBeforePaymentDate", "", corporateOrgAllowedDaysBeforePaymentDate, "3", isReadOnly, false, false, "class='char0'", "", "inline" ) %>
      </td><td valign=top><%= widgetFactory.createInlineLabel("", "CorpCust.PaymentDaysBefore")%></td></tr>
      <div style="clear: both;"></div>
      <tr><td>
      <%= widgetFactory.createTextField( "DaysAfterPaymentDate", "", corporateOrgAllowedDaysAfterPaymentDate, "3", isReadOnly, false, false, "class='char0'", "", "inline" ) %>
      </td><td valign=top>
      <%= widgetFactory.createInlineLabel("", "CorpCust.PaymentDaysAfter")%>
      </td></tr></table>
      <%--Used Table to resolve IR #T36000016895 RPasupulati Ends----%>
         <div style="clear: both;"></div>			
 </div>
 </div>
 <div class="columnRight">
 <div style="clear: both;"></div>
 <span class='readOnly'>
	<lable>
	<%=resMgr.getText("CorpCust.DaysBeforeLoanMaturityDate1",TradePortalConstants.TEXT_BUNDLE)%>
	<%= widgetFactory.createTextField( "FinancingDaysBeforeLoanMaturity", "", corporateOrgFinancingDaysBeforeLoanMaturity, "3", isReadOnly, false, false, "class='char0'", "", "none" ) %>
     <%=resMgr.getText("CorpCust.DaysBeforeLoanMaturityDate2",TradePortalConstants.TEXT_BUNDLE)%>
     </lable></span>
	<br>
	<br>
	<div style="clear: both;"></div>		
	<%-- DK IR T36000014742  Rel8.2 03/01/2013 BEGINS  --%>
	<span class='readOnly'>
	<label>
		<%=resMgr.getText("CorpCust.InvoiceForReceivablesTradeLoan1",TradePortalConstants.TEXT_BUNDLE)%>
		<%= widgetFactory.createPercentField( "InvoicePercentToFinanceTradeLoan", "", corporateOrgInvoicePercentToFinanceTradeLoan, isReadOnly, false, false, "", "", "none" ) %>
     <%=resMgr.getText("CorpCust.InvoiceForReceivablesTradeLoan2",TradePortalConstants.TEXT_BUNDLE)%>
    </label> </span>
      <%-- DK IR T36000014742  Rel8.2 03/01/2013 ENDS  --%>
    <div style="clear: both;"></div>
 </div>
 
 <%-- DK - CR709 Rel8.1 10/31/2012 End --%>	

  <div style="clear: both;"></div>
  <%-- NARAYAN END--%>

  <%--cquinton 11/14/2012 cr708 start--%>
  <%=widgetFactory.createWideSubsectionHeader("CorpCust.ATPInvoiceHeader") %>

  <span class="columnLeft">
    <div class="formItem">
      <%= widgetFactory.createSubLabel("CorpCust.ATPInvoiceProcessing")%>
      <div style="clear: both;"></div>
<%
  if(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_YES.equals(allowPaymentDate)){
    defaultRadioButtonValue1=true;
    defaultRadioButtonValue2=false;
  }
  else if(TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO.equals(allowPaymentDate)){
    defaultRadioButtonValue1=false;
    defaultRadioButtonValue2=true;
  } 
  else {
    defaultRadioButtonValue1=true;
  }

%>
      <%= widgetFactory.createRadioButtonField("PaymentDayAllow", "AllowPaymentDate",
            "CorpCust.ATPInvoiceProcessing.AllowPaymentDate",
            TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_YES,
            defaultRadioButtonValue1, isReadOnly,
            "onClick=\"local.disableDaysBeforeAfter(false);return true;\"", "")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createRadioButtonField("PaymentDayAllow", "UseInvoiceDueDate",
            "CorpCust.ATPInvoiceProcessing.UseInvoiceDueDate",
            TradePortalConstants.BANK_DEF_PAYMENT_ALLOW_NO,
            defaultRadioButtonValue2, isReadOnly,
            "onClick=\"local.disableDaysBeforeAfter(true);return true;\"", "")%>
    </div>
    <div style="clear: both;"></div>
  </span>

  <span class="columnRight">
    <div class="formItem">
      <%= widgetFactory.createSubLabel("CorpCust.ATPInvoicePaymentDate")%>
      <div style="clear: both;"></div>

      <%= widgetFactory.createTextField( "DaysBefore", "", daysBefore, "4", isReadOnly, false, false, "", "", "inline" ) %>
      &nbsp;<%= widgetFactory.createInlineLabel("", "CorpCust.ATPInvoiceDaysBefore")%>
      <div style="clear: both;"></div>
      <%= widgetFactory.createTextField( "DaysAfter", "", daysAfter, "6", isReadOnly, false, false, "width=\"4\"", "", "inline" ) %>
      &nbsp;<%= widgetFactory.createInlineLabel("", "CorpCust.ATPInvoiceDaysAfterInvoiceDueDate")%>
      <div style="clear: both;"></div>
    </div>
  </span>
  <div style="clear: both;"></div>
  <%--cquinton 11/14/2012 cr708 end--%>


<%-- R9.2 CR914 start --%>

  <div style="clear: both;"></div>
  
  <%=widgetFactory.createWideSubsectionHeader("CorpCust.PayCrNote.header") %>
 
  <span class="columnLeft">
 
	 
 	 <%=widgetFactory.createSubsectionHeader("CorpCust.PayCrNote.subHeader") %>  
 
 
  <div class="formItem">
  	
 	<%= widgetFactory.createRadioButtonField("AllowPayCrNoteUpload", "cantUpload",
            "CorpCust.PayCrNote.cantupload",
            TradePortalConstants.INDICATOR_NO,
            isPayCrNoteUploadNotAllowed, isReadOnly,
            "onClick=\"local.clearPayCrNoteFields()\"", "")%><br>	
 
   
   <%= widgetFactory.createRadioButtonField("AllowPayCrNoteUpload", "canUpload",
            "CorpCust.PayCrNote.canupload",
            TradePortalConstants.INDICATOR_YES,
            isPayCrNoteUploadAllowed, isReadOnly,
            "onClick=\"local.clearPayCrNoteFields()\"", "")%>    
   
   </div> 
  
 	
 	<div style="clear: both;"></div>
	      
    <div class="formItem">
    <span class='readOnly'>
	
	<label>
		<%=resMgr.getText("CorpCust.PayCrNote.unutilized.message1",TradePortalConstants.TEXT_BUNDLE)%>
		<%= widgetFactory.createTextField( "PayCrNoteAvailableDays", "", payCreditNoteAvailableDays, "3", isReadOnly, false, false, "class='char0'", "", "none" ) %>
	     <%=resMgr.getText("CorpCust.PayCrNote.unutilized.message2",TradePortalConstants.TEXT_BUNDLE)%>
     </label>
	
	</span>
	
	</div>
  	
  	 <div style="clear: both;"></div>
	 
	 <span class='readOnly'>
 	  <%= widgetFactory.createCheckboxField("AllowEndToEndProcessing", "CorpCust.PayCrNote.endtoendId",
				isEndToEndIdProcessAllowed, isReadOnly, false,
	                	"onClick=\"local.disableApplyCrNotesButtons()\"", "", "")%>
	   <%= widgetFactory.createCheckboxField("AllowManualCrNoteApplication", "CorpCust.PayCrNote.allowManual",
				isManualCreditNoteApplicationAllowed, isReadOnly, false,
	                	"onClick=\"local.enableApplyCrNotesButtons()\"", "", "")%>
  	</span>
     
     <div style="clear: both;"></div>
    
      
      <div class="formItem">   	
        <%= widgetFactory.createSubLabel("CorpCust.PayCrNote.apply")%><br>
	                	
	                	
       <%= widgetFactory.createRadioButtonField("ApplicationMethod", "Propotionately",
            "CorpCust.PayCrNote.apply.propotionately",
            TradePortalConstants.CREDIT_NOTE_APPLICATION_PROPOTIONATE,
            isPropotionateApplication, isReadOnly,
            "onClick=\"local.clearSequentialButtons()\"", "")%>
            <br>	
       <%= widgetFactory.createRadioButtonField("ApplicationMethod", "Sequentially",
            "CorpCust.PayCrNote.apply.sequentially",
            TradePortalConstants.CREDIT_NOTE_APPLICATION_SEQUENTIAL,
            isSequentialApplication, isReadOnly,
            "", "")%>
        <br>	   
		
		<div class="indentHalf">
       <%= widgetFactory.createRadioButtonField("SequentialMethod", "BasedOnInvoice",
            "CorpCust.PayCrNote.apply.sequentially.basedOnInvoice",
            TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_INVOICE_ID,
            isBasedOnInvoice, isReadOnly,
            "onClick=\"local.setSequentialButton()\"", "")%>
            <br>	
       <%= widgetFactory.createRadioButtonField("SequentialMethod", "PaymentDueDate",
            "CorpCust.PayCrNote.apply.sequentially.paymentDueDate",
            TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_DATES,
            isPaymentDueDate, isReadOnly,
            "onClick=\"local.setSequentialButton()\"", "")%>
        <br>	   

       <%= widgetFactory.createRadioButtonField("SequentialMethod", "HighToLowAmount",
            "CorpCust.PayCrNote.apply.sequentially.highToLowAmount",
            TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_HIGHTOLOW,
            isHighToLowAmount, isReadOnly,
            "onClick=\"local.setSequentialButton()\"", "")%>
            <br>	
       <%= widgetFactory.createRadioButtonField("SequentialMethod", "LowToHighAmount",
            "CorpCust.PayCrNote.apply.sequentially.LowToHighAmount",
            TradePortalConstants.CREDIT_NOTE_SEQUENTIAL_TYPE_AMOUNT_LOWTOHIGH,
            isLowToHighAmount, isReadOnly,
            "onClick=\"local.setSequentialButton()\"", "")%>
        <br>	   
        </div>      
        </div> 
            	             	
   <div style="clear: both;"></div>
    </span> 
	<%-- //#RKAZI CR1006 04-24-2015  - BEGIN --%>
	<span class="columnRight">
    <div class="formItem">
     
      <div style="clear: both;"></div>

       <%=widgetFactory.createCheckboxField("PayablesCreditNoteAllowH2HPortalApproval", "CorpCust.PayablesCreditNoteAllowH2HPortalApproval",
          TradePortalConstants.INDICATOR_YES.equals(corporateOrgPayCrnAllowH2hApprovalInd), isReadOnly, false,
                "", "", "") %>    
      <div style="clear: both;"></div>
    </div>
  </span>
  <%-- //#RKAZI CR1006 04-24-2015  - END --%> 
  <div style="clear: both;"></div> 
  <div style="clear: both;"></div>
    
  


<%-- R9.2 CR914 End --%>

<%-- IAZ CR-707 Refresh 06/02/2012 Begin --%>
  <%=widgetFactory.createWideSubsectionHeader("CorpCust.PurchaseOrderManagement") %>

<%

  //out.println("po type: " + corporateOrgPODualInd);

		if ((!insertModeFlag || errorFlag) && (TradePortalConstants.INDICATOR_YES.equals(corporateOrgPOInd)))
             	{
             	   defaultCheckBoxValue = true;

                	if (TradePortalConstants.PURCHASE_ORDER_STRUCTURED_FORMAT.equals(corporateOrgPODualInd))
                	{
                	   defaultRadioButtonValue1 = true;
                	   defaultRadioButtonValue2 = false;

                	}
                	else if (TradePortalConstants.PURCHASE_ORDER_DESCRIPTION_TEXT.equals(corporateOrgPODualInd))
                	{
                	   defaultRadioButtonValue1 = false;
                	   defaultRadioButtonValue2 = true;

                	}
                	else{
                 	   defaultRadioButtonValue1 = false;
                 	   defaultRadioButtonValue2 = false;                 		
                	}
                	
             	}
             	else
             	{            		
  					defaultRadioButtonValue1 = false;
        	        defaultRadioButtonValue2 = false;
        	        defaultCheckBoxValue     = false;
             	}

       		out.print(widgetFactory.createCheckboxField("PurchaseOrderManagementIndicator", "CorpCust.PurchaseOrderIndicator",
			defaultCheckBoxValue, isReadOnly, false,
                	"onClick=\"local.setPORadioButtons()\"", "", ""));
%>
  <div class="formItem">
<%
       		out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createSubLabel("CorpCust.PurchaseOrderUpload"));
%>
    <br>
				
<%
            	out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createRadioButtonField("POUploadDualIndicator",
                  	"StructuredPO", "CorpCust.StructuredPO", TradePortalConstants.PURCHASE_ORDER_STRUCTURED_FORMAT, defaultRadioButtonValue1,
                  	isReadOnly,
                	"onClick=\"local.setPOUploadCheckBox\"", ""));
%>
    <br>
<%
             	out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createRadioButtonField("POUploadDualIndicator",
             		"FreeTextPO", "CorpCust.FreeTextPO", TradePortalConstants.PURCHASE_ORDER_DESCRIPTION_TEXT, defaultRadioButtonValue2,
                  	isReadOnly,
                	"onClick=\"local.setPOUploadCheckBox\"", ""));				
%>
  </div>

  <div class="formItem">
    <%out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createSubLabel("CorpCust.RestrictedPurchaseOrderUploadDirectory")); %>	
    <br>		
<%
                    if ((!insertModeFlag || errorFlag) && corporateOrgRestrictedPurchaseOrderUploadInd != null )
                    {
                    	 if (TradePortalConstants.PURCHASE_ORDER_NOT_RESTRICTED_DIRECTORY.equals(corporateOrgRestrictedPurchaseOrderUploadInd))
                        {
                           defaultRadioButtonValue1 = true;
                           defaultRadioButtonValue2 = false;
                        }
                   	 else if (TradePortalConstants.PURCHASE_ORDER_RESTRICTED_DIRECTORY.equals(corporateOrgRestrictedPurchaseOrderUploadInd))
                       {
                          defaultRadioButtonValue1 = false;
                          defaultRadioButtonValue2 = true;
                       }else
                       {
                      	   defaultRadioButtonValue1 = false;
                          defaultRadioButtonValue2 = false;
              			 }
                    }
                    else
                    {
                 	   defaultRadioButtonValue1 = false;
                      defaultRadioButtonValue2 = false;
         			 }
%>


   <%
            out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + widgetFactory.createRadioButtonField("RestrictedPurchaseOrderUploadIndicator",
                 "PoDirNotRestricted", "CorpCust.PurchaseOrderUploadsNotRestrictedDir", TradePortalConstants.PURCHASE_ORDER_NOT_RESTRICTED_DIRECTORY, defaultRadioButtonValue1,
                 isReadOnly));

   %>
    		 <br>
   <%

            out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + widgetFactory.createRadioButtonField("RestrictedPurchaseOrderUploadIndicator",
                 "PoDirRestricted", "CorpCust.PurchaseOrderUploadsRestrictedDir", TradePortalConstants.PURCHASE_ORDER_RESTRICTED_DIRECTORY, defaultRadioButtonValue2,
                 isReadOnly));
%>
    <div class="formItem">
      <div class="indentHalf">
        <%= widgetFactory.createTextField( "SpecifyRestrictedPurchaseOrderUploadDir", "", corporateOrgRestrictedPOUploadDirectory, "200", isReadOnly, false, false, "width=\"30\"", "", "")%>
      </div>
    </div>
  </div>

  <br>
<%-- IAZ CR-707 Refresh 06/02/2012 End --%>






<%-- Srinivasu_D CR-599 Rel8.4 11/20/2013 start --%>


  <%=widgetFactory.createWideSubsectionHeader("CorpCust.PaymentMgmtHeader1") %>

<%              	 

			 if ((!insertModeFlag || errorFlag) && corporateOrgPayMgmtFileInd != null )
                    {
                    	 if (TradePortalConstants.PAYMENT_MGMT_FILE_DEF.equals(corporateOrgPayMgmtFileInd))
                        {
                           defaultRadioButtonValue1 = true;  
                           defaultRadioButtonValue2 = false;
                        }
                   	 else if (TradePortalConstants.PAYMENT_MGMT_PILE_FIXED_FORMAT.equals(corporateOrgPayMgmtFileInd))
                       {
                          defaultRadioButtonValue1 = false;
                          defaultRadioButtonValue2 = true;
                       }else
                       {
                      	   defaultRadioButtonValue1 = false;
                           defaultRadioButtonValue2 = false;
              			 }
                    }
                    else
                    {
                 	   defaultRadioButtonValue1 = false;
                       defaultRadioButtonValue2 = false;
         			 }
             	
             
       		
%>
  <div class="formItem">
	<%
				out.print("&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createSubLabel("CorpCust.PaymentMgmtHeader2"));
	%>
		<br>
					
	<%
					out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createRadioButtonField("PayMgmtFileDelimiterIndicator",
						"PayMgmtDelimiter", "CorpCust.PaymentMgmtRadio1", TradePortalConstants.PAYMENT_MGMT_FILE_DEF, defaultRadioButtonValue1,
						isReadOnly,"", ""));
	%>
		<br>
	<%
					out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createRadioButtonField("PayMgmtFileDelimiterIndicator",
						"PayMgmtPipe", "CorpCust.PaymentMgmtRadio2", TradePortalConstants.PAYMENT_MGMT_PILE_FIXED_FORMAT, defaultRadioButtonValue2,
						isReadOnly,"", ""));				
	%>
	  </div>

  <div class="formItem">
  	
    <%out.print("&nbsp&nbsp&nbsp&nbsp" +widgetFactory.createSubLabel("CorpCust.PaymentMgmtHeader3")); %>	
    <br>		
<%
                    if ((!insertModeFlag || errorFlag) && corporateOrgPayMgmtDirInd != null )
                    {
                    	 if (TradePortalConstants.PAYMENT_MGMT_NOT_RESTRICTED_DIRECTORY.equals(corporateOrgPayMgmtDirInd))
                        {
                           defaultRadioButtonValue1 = true;
                           defaultRadioButtonValue2 = false;
                        }
                   	 else if (TradePortalConstants.PAYMENT_MGMT_RESTRICTED_DIRECTORY.equals(corporateOrgPayMgmtDirInd))
                       {
                          defaultRadioButtonValue1 = false;
                          defaultRadioButtonValue2 = true;
                       }else
                       {
                      	   defaultRadioButtonValue1 = false;
                          defaultRadioButtonValue2 = false;
              			 }
                    }
                    else
                    {
                 	   defaultRadioButtonValue1 = false;
                       defaultRadioButtonValue2 = false;
         			 }
%>


   <%
            out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + widgetFactory.createRadioButtonField("PayMgmtDirectoryIndicator",
                 "PayMgmtDirNotRestricted", "CorpCust.PaymentMgmtRadio3", TradePortalConstants.PAYMENT_MGMT_NOT_RESTRICTED_DIRECTORY, defaultRadioButtonValue1,
                 isReadOnly,"onClick=\"local.clearPayMgmtUploadDir()\"",""));

   %>
    		 <br>
   <%

            out.print("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + widgetFactory.createRadioButtonField("PayMgmtDirectoryIndicator",
                 "PayMgmtDirRestricted", "CorpCust.PaymentMgmtRadio4", TradePortalConstants.PAYMENT_MGMT_RESTRICTED_DIRECTORY, defaultRadioButtonValue2,
                 isReadOnly,"onClick=\"local.clearPayMgmtUploadDir()\"",""));
%>
    <div class="formItem">
      <div class="indentHalf">
        <%= widgetFactory.createTextField( "SpecifyRestrictedPayMgmtUploadDir", "", corporateOrgPayMgmtDirLoc, "200", isReadOnly, false, false, "onkeyup=\"local.setRestrictedPayMgmtUploadIndicator()\" width=\"30\"", "", "")%>
      </div>
    </div>
  </div>

  


<%-- Srinivasu_D CR-599 Rel8.4 11/20/2013 end  --%>


  <br>
 

  <%-- CR ANZ 501 Rel 8.3 03-24-2013 jgadela  Beign--%>
  <%
                    if ((!insertModeFlag || errorFlag) && corpUserDualCtrlReqdInd != null )
                    {
	                    	if (TradePortalConstants.INDICATOR_YES.equals(corpUserDualCtrlReqdInd)){
	                           defaultRadioButtonValue1 = true;
	                           defaultRadioButtonValue2 = false;
	                        }
	                   	 	else if (TradePortalConstants.INDICATOR_NO.equals(corpUserDualCtrlReqdInd)){
	                           defaultRadioButtonValue1 = false;
	                           defaultRadioButtonValue2 = true;
	                       }else
	                       {
	                      	    defaultRadioButtonValue1 = false;
	                          	defaultRadioButtonValue2 = true;
	              		   }
                    }
                    else
                    {
                 	   defaultRadioButtonValue1 = false;
                       defaultRadioButtonValue2 = true;
         			}
%>
  
  <%=widgetFactory.createWideSubsectionHeader("CorpCust.CustomerDualControlManagement") %>	
  <table width="800px;" border="1" cellspacing="0" cellpadding="0" class="formDocumentsTable">
    <colgroup>
      <col span="1" style="width: 25%;">
      <col span="1" style="width: 25%;">
      <col span="1" style="width: 25%;">
    </colgroup>
    <thead>
        <th align="left"></th>
        <th align="left"><%=resMgr.getText("common.Yes",TradePortalConstants.TEXT_BUNDLE)%></th>
        <th align="left"><%=resMgr.getText("common.No",TradePortalConstants.TEXT_BUNDLE)%></th>
      </tr>
    </thead>
  <tbody>
    <tr>
      <td>
            <%= widgetFactory.createSubLabel("RefDataHome.Users") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("corpUserDualCtrlReqdInd",
                "corpUserDualCtrlReqdInd" + "Yes", "", TradePortalConstants.INDICATOR_YES, defaultRadioButtonValue1,isReadOnly, "", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("corpUserDualCtrlReqdInd",
                "corpUserDualCtrlReqdInd" + "No", "", TradePortalConstants.INDICATOR_NO, defaultRadioButtonValue2, isReadOnly,"", ""));
        %>
      </td>
      </tr>
      
      <%
                    if ((!insertModeFlag || errorFlag) && panelAuthorisationDualCtrlReqdInd != null )
                    {
	                    	if (TradePortalConstants.INDICATOR_YES.equals(panelAuthorisationDualCtrlReqdInd)){
	                           defaultRadioButtonValue1 = true;
	                           defaultRadioButtonValue2 = false;
	                        }
	                   	 	else if (TradePortalConstants.INDICATOR_NO.equals(panelAuthorisationDualCtrlReqdInd)){
	                           defaultRadioButtonValue1 = false;
	                           defaultRadioButtonValue2 = true;
	                       }else
	                       {
	                      	    defaultRadioButtonValue1 = false;
	                          	defaultRadioButtonValue2 = true;
	              		   }
                    }
                    else
                    {
                 	   defaultRadioButtonValue1 = false;
                       defaultRadioButtonValue2 = true;
         			}
%>
      
      <tr>
      <td>
            <%= widgetFactory.createSubLabel("CorpCust.PanelAuth") %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("panelAuthorisationDualCtrlReqdInd",
                "panelAuthorisationDualCtrlReqdInd" + "Yes", "", TradePortalConstants.INDICATOR_YES, defaultRadioButtonValue1,isReadOnly, "", ""));
        %>
      </td>
      <td align="center" valign="top">
        <%
           out.print(widgetFactory.createRadioButtonField("panelAuthorisationDualCtrlReqdInd",
                "panelAuthorisationDualCtrlReqdInd" + "No", "", TradePortalConstants.INDICATOR_NO, defaultRadioButtonValue2, isReadOnly,"", ""));
        %>
      </td>
      </tr>
  </tbody>
  </table>
  <br><br>
  
 <%-- CRANZ 501 Rel 8.3 03-24-2013 jgadela  Beign--%>