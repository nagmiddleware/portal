<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, com.ams.tradeportal.common.cache.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%	
beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.CustomerErpGlCodeWebBean", "CustomerErpGlCode");
CustomerErpGlCodeWebBean customerErpGlCode = (CustomerErpGlCodeWebBean)beanMgr.getBean("CustomerErpGlCode");

WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String parentOrgID        = userSession.getOwnerOrgOid();
String oid = "0";
int currCount = 4;
boolean	isReadOnly		= false;

String i = request.getParameter("iLoop");		

//if(i != null){
//	  i = StringFunction.xssCharsToHtml(i);
//}
StringBuffer options = Dropdown.createGLCategoryOptions(resMgr, customerErpGlCode.getAttribute("erp_gl_category"));

%>

<%-- Rel 9.3 XSS CID 11237, 11247, 11523 --%>
<%-- Rel 9.2 Xss CID 11334 --%>
<tr id="erpglcode<%= StringFunction.xssCharsToHtml(i)%>">
	<td>      
	  <%=widgetFactory.createTextField("erpGlCode"+ StringFunction.xssCharsToHtml(i),"",customerErpGlCode.getAttribute("erp_gl_code") ,"17",isReadOnly,true,false, "style='width: 75px'", "", "") %>
	</td> 
	<td>
	 <%= widgetFactory.createSelectField("erpGlCat"+ StringFunction.xssCharsToHtml(i), ""," ", options.toString(), isReadOnly, true, false,"style='width: 150px'", "", "") %>      
	</td> 			
	<td>      
	  <%=widgetFactory.createTextField("desc"+ StringFunction.xssCharsToHtml(i),"",customerErpGlCode.getAttribute("erp_gl_description") ,"30",isReadOnly,true,false, "style='width: 175px'", "", "") %>
	</td>
	<%--<td>
	<%=widgetFactory.createCheckboxField("defaultGlInd"+ StringFunction.xssCharsToHtml(i), "", 
	customerErpGlCode.getAttribute("default_gl_code_ind").equals(TradePortalConstants.INDICATOR_YES), isReadOnly, false,  "", "", "")%>
	</td>	--%>		 
			<td>         
				<div class="formItem " >
				<%out.print("<input type='CheckBox' name='defaultGlInd"+ StringFunction.xssCharsToHtml(i)+"' id='defaultGlInd"+ StringFunction.xssCharsToHtml(i)+"'  value='Y'>"); %>
				</div>			
			</td>
			<td>         
				<div class="formItem " >				
				<%--<input data-dojo-type="dijit.form.CheckBox" name="deactivate"<%=i%> id="deactivate"<%=i%>  value="Y" > --%>
				<%out.print("<input type='CheckBox' name='deactivate"+ StringFunction.xssCharsToHtml(i)+"' id='deactivate"+ StringFunction.xssCharsToHtml(i)+"'  value='Y'>"); %>
				</div>			
			</td>	
			                                                
</tr>

<input type=hidden value="<%=oid %>" name="oid<%=StringFunction.xssCharsToHtml(i)%>">
<input type=hidden value="<%=parentOrgID %>" name="powner<%=StringFunction.xssCharsToHtml(i)%>">