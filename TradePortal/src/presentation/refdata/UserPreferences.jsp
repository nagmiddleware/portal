<%--
*******************************************************************************
  User Preferences

  Description:
    Displays user details directly to user.  Allows user to change their
    password, plus other select user preferences.

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.common.cache.*,com.ams.tradeportal.html.*,java.util.*,
                 com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*,com.amsinc.ecsg.html.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"/>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"/>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"/>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"/>

<%
  boolean isAdminUser = false;//BSL 09/06/11 CR663 Rel 7.1 Add
  // [START] CR-482
  // Check if this TradePortal instance is configured for HSM encryption / decryption
 boolean instanceHSMEnabled = HSMEncryptDecrypt.isInstanceHSMEnabled();
	// [END] CR-482
%>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
CorporateOrganizationBean corpOrgBean = new CorporateOrganizationBean(); //CR 821
String panelAliasesOptions = ""; //CR 821
Hashtable panelAliasesMap = new Hashtable(); //CR 821
String pageTitle;
String instructions;
String formName;
boolean forcedToChangePassword = false;
boolean isNewUser = false;

String forcedToChangePasswordParm;
String reasonParam;
String helpFileName;
String stricterPasswordSetting = null;
String minimumPasswordLength = null;

Hashtable secureParms = new Hashtable();
DocumentHandler doc = formMgr.getFromDocCache();
//CR 821 Rel 8.3 START
String emailAddressFromDoc = "";
DocumentHandler docComp = doc.getComponent("/In");
//emailAddressFromDoc = docComp.getAttribute("/emailAddressUserPref");

//CR 821 Rel 8.3 END
  //jgadela - 29th Aug 2013 - Rel8.3 IR-T36000020216  - Corrected the constructed parameters by passing userSession since we were getting null pointer exception in createSortedOptions method
  // of the class
  WidgetFactory widgetFactory = new WidgetFactory(resMgr, userSession);
  boolean isReadOnly = true;
  //CR 501 Rel 8.3 IR#T36000021112
  boolean isFirstSectionReadOnly = false;
  String pendingRecordExists = TradePortalConstants.INDICATOR_NO;
  boolean isViewInReadOnly = true;
  boolean isWorkGroupReadOnly = false;

  String options;
  String defaultText;
  String link;
  String links = "";
  String certAuthURL = "";
  String oid = "";
//Suresh CR-603 03/11/2011 Begin
  UserWebBean					  user							  = null;
  StringBuffer                    sqlQuery                        = null;
  String						  userDetailReptCateg1Oid		  = null;
  String						  userDetailReptCateg2Oid		  = null;
  String						  userDetailReptCateg3Oid		  = null;
  String						  userDetailReptCateg4Oid		  = null;
  String						  userDetailReptCateg5Oid		  = null;
  String						  userDetailReptCateg6Oid		  = null;
  String						  userDetailReptCateg7Oid		  = null;
  String						  userDetailReptCateg8Oid		  = null;
  String						  userDetailReptCateg9Oid		  = null;
  String						  userDetailReptCateg10Oid	      = null;
  String 						  pSql							  = null;
  String                          dropdownOptions                 = null;
  String 						  allowPayByAnotherAccnt          = "";
  String 						  cbReportingLangOptionInd        = ""; // DK IR T36000025243 Rel8.4 02/18/2014
  //Suresh CR-603 03/11/2011 End
  // CR-451 NShrestha 10/16/2008 Begin --
  int numTotalAccounts = 0;
  int totAccounts = 0;
  int DEFAULT_ACCOUNT_COUNT = 4;
  String buttonPressed = "";
  Vector error = null;
  UserAuthorizedAccountWebBean account[]=null;
  String onLoad = "";
  // CR-451 NShrestha 10/16/2008 End --

  //CR-586 Vshah [BEGIN]
    int numTotalTemplateGroups = 0;
    int totTemplateGroups = 0;
    final int DEFAULT_ROWS = 4;
    int DEFAULT_TEMPLATE_COUNT = 4;
    UserAuthorizedTemplateGroupWebBean templateGroup[]=null;
  //CR-586 Vshah [END]

  // These booleans help determine the mode of the JSP.
  boolean getDataFromDoc = false;
  boolean retrieveFromDB = false;
  boolean insertMode = true;
//jgadela rel8.3 CR501 [START]
  boolean showApprove = false;
  boolean showReject = false;
//jgadela rel8.3 CR501 [END]

  boolean showSave = true;
  boolean showDelete = true;

  boolean showCloseButton=true;
  boolean showDeleteButton=false;
  boolean showSaveButton=false;

  String password1 = "";
  String password2 = "";
//MEer Rel 8.3 T36000021992 
  boolean ignoreDefaultPanelValue = false;



  // Must initialize these 2 doc, otherwise get a compile error.
  DocumentHandler thresholdGroupDoc = new DocumentHandler();
  DocumentHandler secProfileDoc = new DocumentHandler();
  DocumentHandler workGroupDoc = new DocumentHandler();
  DocumentHandler accountDoc = new DocumentHandler();
  DocumentHandler templateGroupDoc = new DocumentHandler(); //CR-586
  String cbMobileBankingInd = null;//MEerupula Rel8.4 CR-934A


  String multipleTransactionPartsInd;
  String displayExtendedListviewInd;

  boolean defaultRadioButtonValue1 = true;
  boolean defaultRadioButtonValue2 = false;

  if(request.getParameter("numberOfMultipleObjects")!=null)
     numTotalAccounts = Integer.parseInt(request.getParameter("numberOfMultipleObjects"));
  else
     numTotalAccounts = DEFAULT_ROWS;

  //CR-586 Vshah [BEGIN]
  if(request.getParameter("numberOfMultipleObjects1")!=null)
     numTotalTemplateGroups = Integer.parseInt(request.getParameter("numberOfMultipleObjects1"));
  else
     numTotalTemplateGroups = DEFAULT_ROWS;
  //CR-586 Vshah [END]


  user = beanMgr.createBean(UserWebBean.class, "User");

  CorporateOrganizationWebBean corporateOrg = null;
//jgadela R90 IR T36000026319 - SQL INJECTION FIX
  Object sqlParams[] = new Object[10];
  sqlParams[0] = userSession.getOwnerOrgOid();
  sqlParams[1] = userSession.getOwnerOrgOid();
  sqlParams[2] = userSession.getOwnerOrgOid();
  sqlParams[3] = userSession.getOwnerOrgOid();
  sqlParams[4] = userSession.getOwnerOrgOid();
  sqlParams[5] = userSession.getOwnerOrgOid();
  sqlParams[6] = userSession.getOwnerOrgOid();
  sqlParams[7] = userSession.getOwnerOrgOid();
  sqlParams[8] = userSession.getOwnerOrgOid();
  sqlParams[9] = userSession.getOwnerOrgOid();

  //Suresh CR-603 03/14/2011 Begin
  DocumentHandler				  reportCategsDoc				  = null;
  pSql = "select CODE, DESCR from refdata where table_type = 'REPORTING_CATEGORY' and CODE IN ( " +
		  "select first_rept_categ from CORPORATE_ORG where organization_oid=? " +
		  "UNION ALL select second_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select third_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select fourth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select fifth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select sixth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select seventh_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select eighth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select nineth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  "UNION ALL select tenth_rept_categ from CORPORATE_ORG where organization_oid=? "+
		  ")";
  reportCategsDoc = DatabaseQueryBean.getXmlResultSet(pSql, false, sqlParams);
  //Suresh CR-603 03/14/2011 End

  String loginLocale;
  String loginRights;

  //Vasavi CR 580 06/01/10 Begin
  String         userSecurityType        = null;

  // Retrieve the user's security type (i.e., ADMIN or NON_ADMIN)
  if (userSession.getSavedUserSession() == null) {
  		userSecurityType = userSession.getSecurityType();
   }else {
	   userSecurityType = userSession.getSavedUserSessionSecurityType();
   }
  Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
  DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
  String canViewIDFields;
  String canViewPaymentTemplateGroups; //IR-VUK090959969

  if (InstrumentServices.isBlank(userSession.getClientBankOid())){
  	    canViewIDFields = "";
  	    canViewPaymentTemplateGroups = ""; //IR-VUK090959969
  }else{
  	   canViewIDFields = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_ID_IND");
  	   canViewPaymentTemplateGroups = CBCResult.getAttribute("/ResultSetRecord(0)/TEMPLATE_GROUPS_IND"); //IR-VUK090959969
  	   allowPayByAnotherAccnt = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT"); // RKazi IR HRUL062032745
  	   cbReportingLangOptionInd = CBCResult.getAttribute("/ResultSetRecord(0)/REPORTING_LANG_OPTION_IND"); // DK IR T36000025243 Rel8.4 02/18/2014
  }
  //Vasavi CR 580 06/01/10 End
  	// RKazi IR HRUL062032745 Start
  	if (InstrumentServices.isBlank(allowPayByAnotherAccnt)){
  		String clientBankOid = userSession.getClientBankOid();
  		QueryListView queryListView   = null;
  		sqlQuery = new StringBuffer();

  		sqlQuery.append("select allow_pay_by_another_accnt");
  		sqlQuery.append(" from client_bank");
  		sqlQuery.append(" where organization_oid =? ");

  		queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
  		//out.println("QUERY: "+sqlQuery.toString());
  		queryListView.setSQL(sqlQuery.toString(),new Object[]{clientBankOid});
  		queryListView.getRecords();

  		DocumentHandler result = queryListView.getXmlResultSet();
  		allowPayByAnotherAccnt = result.getAttribute("/ResultSetRecord(0)/ALLOW_PAY_BY_ANOTHER_ACCNT");
  	}
	// RKazi IR HRUL062032745 Start
// Either get the page parameters from the input cache or from the JSP parameters
if ( (doc.getAttribute("/Error/maxerrorseverity") == null) ||
     (doc.getAttribute("/Error/maxerrorseverity").compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) ) {
  Debug.debug("getting values from params...");
  forcedToChangePasswordParm = request.getParameter("force");
  reasonParam = request.getParameter("reason");
}
else {
  Debug.debug("getting values from input doc...");
  forcedToChangePasswordParm = doc.getAttribute("/In/force");
  reasonParam = doc.getAttribute("/In/reason");
}


if ( (forcedToChangePasswordParm != null) &&
    (forcedToChangePasswordParm.equals("true") ) ) {
  forcedToChangePassword = true;
}

pageTitle = "SecondaryNavigation.UserPreferences";
helpFileName = "admin/user_preferences.htm";
formName = "ChangePassword";
/*if( (reasonParam != null) && (reasonParam.equals("new")) ) {
  isNewUser = true;
  pageTitle = "ChangePassword.NewUserTitle";
  instructions = "ChangePassword.NewUserInstructions";
  formName = "ChangePasswordNewUser";
  helpFileName = "customer/change_passwd_first.htm";
}
else if( (reasonParam != null) && (reasonParam.equals("expired")) ) {
  pageTitle = "ChangePassword.Title";
  instructions = "ChangePassword.ExpiredInstructions";
  formName = "ChangePasswordExpire";
  helpFileName = "customer/change_passwd_expired.htm";
}
else {
  pageTitle = "ChangePassword.Title";
  instructions = "ChangePassword.RegularInstructions";
  formName = "ChangePassword";
  helpFileName = "customer/change_passwd_voluntary.htm";
}*/

// Determine text resource key suffix
// Different instructions are displayed if stricter password rules
// are in effect

String suffix = "";

if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_GLOBAL)) {
  stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
  // [START] CR-482
  minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
  // [END] CR-482
}
else {
  ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");
  clientBank.getById(userSession.getClientBankOid());
   cbMobileBankingInd = clientBank.getAttribute("mobile_banking_access_ind");//MEerupula Rel 8.4 CR-934A

  // [START] CR-482 Updated indenting and added minimumPasswordLength
  if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE)) {
    stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
    minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
  }
  else {
    stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
    minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
  }
  // [END] CR-482
}

if (stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) {
  suffix = "StricterRules";
}


// CHANGE THIS BASED ON MODE

if(!forcedToChangePassword) {
  onLoad = "document.ChangePassword.currentPassword.focus();";
}
else {
  onLoad = "document.ChangePassword.newPassword.focus();";
}
%>



<%-- Body tag included as part of common header --%>

<%
String minimalHeader = TradePortalConstants.INDICATOR_NO;
if (forcedToChangePassword) {
  minimalHeader = TradePortalConstants.INDICATOR_YES;
}
else {
  minimalHeader = TradePortalConstants.INDICATOR_NO;
}
%>

<%
  // Get the webbeans for the login in user and his security profile.  Set the
  // readonly access appropriately.
  loginLocale = userSession.getUserLocale();
  loginRights = userSession.getSecurityRights();

     corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
     corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
     corporateOrg.getDataFromAppServer();
     String corpCustMobileBankingInd = corporateOrg.getAttribute("mobile_banking_access_ind");
     
  if (SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_USERS)) {
     isReadOnly = false;
     // If the user's corporate organization requires certificate authentication for users
     // that can maintain user data and the user's authentication method is password,
     // display the page in read-only mode and display the information message.

      // NAR-SAUJ060247671 24-SEP-2010
      //if user is admin, force certificate condition should not be check for admin user.if admin has user maintinance access reghts,
      // he will be able to edit customer user data and can create user without checking whether he has 2FA and Certificate rights.
     if(!userSecurityType.equals(TradePortalConstants.ADMIN)){
     if (corporateOrg.getAttribute("force_certs_for_user_maint").equals(TradePortalConstants.INDICATOR_YES)) { // Corporate Org requires cert for users that maintain users.
        UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
        thisUser.setAttribute("user_oid", userSession.getUserOid());
        thisUser.getDataFromAppServer();

     // T36000013154 Rel 8.2 07/11/2013 - Start
        String authMethod = StringFunction.isNotBlank(thisUser.getAttribute("authentication_method")) ?
       		 thisUser.getAttribute("authentication_method") : corporateOrg.getAttribute("authentication_method");

        if (!(TradePortalConstants.AUTH_CERTIFICATE.equals(authMethod)
          || TradePortalConstants.AUTH_2FA.equals(authMethod))) { // This user is not using certificate
        	// T36000013154 Rel 8.2 07/11/2013 - End
           // Use a MediatorService to add these errors to the default.doc XML so that
           // it can be accessed by the error page
           MediatorServices mediatorServices = new MediatorServices();
           mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
           mediatorServices.getErrorManager().issueError("UserDetail.jsp",
                                   TradePortalConstants.SECURITY_NEED_CERT_MAINT_USER);
           mediatorServices.addErrorInfo();
           DocumentHandler defaultDoc = new DocumentHandler();
           defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
	       defaultDoc.setAttribute("/In/HasErrors",  TradePortalConstants.INDICATOR_YES);
           formMgr.storeInDocCache("default.doc", defaultDoc);

		   isReadOnly = true;
        }
       }
     }
  } else if (SecurityAccess.hasRights(loginRights,
                                         SecurityAccess.VIEW_USERS) || isViewInReadOnly) {
     isReadOnly = true;
  } else {
     // Don't have maintain or view access, send the user back.
        formMgr.setCurrPage("RefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  }

  // We should only be in this page for corporate users.  If the logged in
  // user's level is not corporate, send back to ref data page.  This
  // condition should never occur but is caught just in case.
  String ownerLevel = userSession.getOwnershipLevel();

  if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
        formMgr.setCurrPage("RefDataHome");
        String physicalPage = NavigationManager.getNavMan().getPhysicalPage("RefDataHome", request);
%>
        <jsp:forward page='<%= physicalPage %>' />
<%
        return;
  }

  if( isViewInReadOnly)
  {
 	isReadOnly = true;
  }

  Debug.debug("login_rights is " + loginRights);

  // Get the document from the cache.  We'll use it to repopulate the screen
  // if the user was entering data with errors.
  doc = formMgr.getFromDocCache();
  Debug.debug("doc from cache is " + doc.toString());
String pendingReferenceDataOid = "";

//CR-451 NShrestha 10/16/2008 Begin --
  if (doc.getDocumentNode("/In/Update/ButtonPressed") != null)
  {
      buttonPressed  = doc.getAttribute("/In/Update/ButtonPressed");
      error = doc.getFragments("/Error/errorlist/error");
  }
//CR-451 NShrestha 10/16/2008 End --

  if (doc.getDocumentNode("/In/User") == null ) {
     // The document does not exist in the cache.  We are entering the page from
     // the listview page (meaning we're opening an existing item or creating
     // a new one.)

     oid = EncryptDecrypt.decryptStringUsingTripleDes(EncryptDecrypt.encryptStringUsingTripleDes(userSession.getUserOid(), userSession.getSecretKey()),userSession.getSecretKey());


     if (oid == null) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
       oid = "0";
     } else if (oid.equals("0")) {
       getDataFromDoc = false;
       retrieveFromDB = false;
       insertMode = true;
     } else {
       // We have a good oid so we can retrieve.
       getDataFromDoc = false;
       retrieveFromDB = true;
       insertMode = false;
     }

  } else {
     // The doc does exist so check for errors.  If highest severity < WARNING,
     // its a good update.

     String maxError = doc.getAttribute("/Error/maxerrorseverity");
     if ((maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0
    		 && !(TradePortalConstants.BUTTON_ADD_4_MORE_ACCOUNTS.equals(buttonPressed)))) {
        // We've returned from a save/update/delete that was successful
        // We shouldn't be here.  Forward to the RefDataHome page.
        formMgr.setCurrPage("RefDataHome");

%>
        <jsp:forward page='/refdata/RefDataHome.jsp' />
<%
        return;
     } else {
        // We've returned from a save/update/delete but have errors.
        // We will display data from the cache.  Determine if we're
        // in insertMode by looking for a '0' oid in the input section
        // of the doc.

        retrieveFromDB = false;
        getDataFromDoc = true;

        String newOid = doc.getAttribute("/In/User/user_oid");

        if (newOid.equals("0")) {
           insertMode = true;
           oid = "0";
        } else {
           // Not in insert mode, use oid from input doc.
           insertMode = false;
           oid = doc.getAttribute("/In/User/user_oid");
        }
     }
  }
  String onlineHelpFileName="";
  onlineHelpFileName = "customer/user_detail.htm";
  // Make sure that users cannot delete themselves
  String thisUserOid;
  thisUserOid = userSession.getUserOid();
  //IAZ CR-475 06/21/09 and 06/28/09 Begin
  //if users of this corp (as driven by Client Bank) cannot modify thier own profiles,
  //make sure the mode isReadOnly if user access his/her own profile
  //if ( thisUserOid.equals(oid) ) showDelete = false;
  try {
  if ( thisUserOid.equals(oid) )
  {
  	showDelete = false;
  	if (!isReadOnly)
  	{
  	   	if (InstrumentServices.isNotBlank(userSession.getClientBankOid()))
   		{
   			String canModifyProfile = CBCResult.getAttribute("/ResultSetRecord(0)/CAN_EDIT_OWN_PROFILE_IND");
  			if (TradePortalConstants.INDICATOR_NO.equals(canModifyProfile)){
  				isReadOnly = true;
  			}

  	 	}
  	}
  }
  }
  catch (Exception any_e)
  {
		System.out.println("General exception " + any_e.toString() + "verifying if user can edit own profile.  Will use default mode.");
  }
  //IAZ CR-475 06/21/09 and 06/28/09 End

  if (retrieveFromDB) {
     // Attempt to retrieve the item.  It should always work.  Still, we'll
     // check for a blank oid -- this indicates record not found.  Set to
     // insert mode and display error.

     getDataFromDoc = false;

     user.setAttribute("user_oid", oid);
    // user.getDataFromAppServer();

   //jgadela rel8.3 CR501 [START]
     if("APPROVE".equalsIgnoreCase(request.getParameter("fromAction"))){
    	 showApprove = true;
    	 showReject = true;
     }

     ReferenceDataPendingWebBean pendingObj = beanMgr.createBean(ReferenceDataPendingWebBean.class,"ReferenceDataPending");
	 user = (UserWebBean) pendingObj.getReferenceData(user, userSession.getUserOid(),showApprove);

	 
	 pendingReferenceDataOid = pendingObj.getAttribute("pending_data_oid");
	 if (pendingObj.isReadOnly()) {
	     isReadOnly = true;
	       //CR 501 Rel 8.3 IR#T36000021112
	     isFirstSectionReadOnly = true;
	     pendingRecordExists = TradePortalConstants.INDICATOR_YES;
	     if(showApprove == false){
		     DocumentHandler errorDoc = new DocumentHandler();
		     String errorMessage = resMgr.getText(TradePortalConstants.PENDING_USER_PREFERENCES,"ErrorMessages");
		     errorDoc.setAttribute("/severity", "5");
		     errorDoc.setAttribute("/message", errorMessage);
		     doc.addComponent("/Error/errorlist/error(0)", errorDoc);
	     }
	 }


	//jgadela rel8.3 CR501 [END]

     if (user.getAttribute("user_oid").equals("")) {
       insertMode = true;
       oid = "0";
       getDataFromDoc = false;
     } else {
       insertMode = false;
     }
  }


  if (getDataFromDoc) {
     // Populate the user bean with the input doc.
     try {
        user.populateFromXmlDoc(doc.getComponent("/In"));
     } catch (Exception e) {
        out.println("Contact Administrator: Unable to get User attributes "
             + " for oid: " + oid + " " + e.toString());
     }

// CR-451 NShrestha 10/16/2008 Begin --
      // Populate account[] bean with account info from document handler
      DocumentHandler doc2 = doc.getComponent("/In");
	  Vector vVector = doc2.getFragments("/User/UserAuthorizedAccountList");
	  totAccounts = vVector.size();
	  String  sValue;
	  Debug.debug("UserAuthorizedAccountList Size = "+numTotalAccounts);
	  numTotalAccounts=(totAccounts > DEFAULT_ROWS)?totAccounts:DEFAULT_ROWS;

	  account = new UserAuthorizedAccountWebBean[numTotalAccounts];
	   for (int iLoop=0; iLoop<totAccounts; iLoop++)
	   {
 	     account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
	     DocumentHandler acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		 try { sValue = acctDoc.getAttribute("/authorized_account_oid"); }
		 catch (Exception e) { sValue = ""; }
		 account[iLoop].setAttribute("authorized_account_oid", sValue);

		 acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		 try { sValue = acctDoc.getAttribute("/account_oid"); }
		 catch (Exception e) { sValue = ""; }
		 account[iLoop].setAttribute("account_oid", sValue);

		 acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		 try { sValue = acctDoc.getAttribute("/account_description"); }
		 catch (Exception e) { sValue = ""; }
		 account[iLoop].setAttribute("account_description", sValue);


	   }

  //CR-586 Begin
  	  DocumentHandler templategroupDoc = doc.getComponent("/In");
	  Vector tgVector = doc2.getFragments("/User/UserAuthorizedTemplateGroupList");
	  totTemplateGroups = tgVector.size();
	  String  tgValue;
	  Debug.debug("UserAuthorizedTemplateGroupList Size = " + numTotalTemplateGroups);
	  numTotalTemplateGroups=(totTemplateGroups > DEFAULT_ROWS)?totTemplateGroups:DEFAULT_ROWS;

	  templateGroup = new UserAuthorizedTemplateGroupWebBean[numTotalTemplateGroups];
	   for (int iLoop=0; iLoop<totTemplateGroups; iLoop++)
	   {
 	     templateGroup[iLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
	     DocumentHandler templateGrpDoc = (DocumentHandler) tgVector.elementAt(iLoop);
		 try { tgValue = templateGrpDoc.getAttribute("/authorized_template_group_oid"); }
		 catch (Exception e) { tgValue = ""; }
		 templateGroup[iLoop].setAttribute("authorized_template_group_oid", tgValue);

		 templateGrpDoc = (DocumentHandler) tgVector.elementAt(iLoop);
		 try { tgValue = templateGrpDoc.getAttribute("/payment_template_group_oid"); }
		 catch (Exception e) { tgValue = ""; }
		 templateGroup[iLoop].setAttribute("payment_template_group_oid", tgValue);


	   }
  //CR-586 End


  }else {
	   // Populate account[] bean with account info from document handler
	   QueryListView queryListView=null;
	   try
		{
		 String  sValue;
		 Vector vVector =null;
		 List<Object> sqlParamsUserPre = new ArrayList();
		 if (retrieveFromDB) {
		 StringBuilder sql = new StringBuilder();
		  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		  //sql.append("select authorized_account_oid, account_description, a_account_oid from USER_AUTHORIZED_ACCT ");			//IAZ 08/07/09 CHF
		  sql.append("select authorized_account_oid, account_description, a_account_oid,  othercorp_customer_indicator from USER_AUTHORIZED_ACCT, ACCOUNT ");	//IAZ 08/07/09 CHT
		  sql.append(" where p_user_oid = ?");
		  sql.append(" and a_account_oid = account_oid and deactivate_indicator != ?" );
		  sqlParamsUserPre.add(user.getAttribute("user_oid"));
		  sqlParamsUserPre.add(TradePortalConstants.INDICATOR_YES);
		  //IAZ 08/07/09 Add
		  //RKAZI 06/24/2011 HRUL062032745 START
		  if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccnt)){
    		sql.append(" and othercorp_customer_indicator != ?");
	    	  sqlParamsUserPre.add(TradePortalConstants.INDICATOR_YES);
		  }
		  //RKAZI 06/24/2011 HRUL062032745 END

		  sql.append(" order by ");
		  sql.append(resMgr.localizeOrderBy("authorized_account_oid"));
		  Debug.debug(sql.toString());

		  queryListView.setSQL(sql.toString(),sqlParamsUserPre);
		  queryListView.getRecords();

		  DocumentHandler acctList = new DocumentHandler();
		  acctList = queryListView.getXmlResultSet();
		  vVector = acctList.getFragments("/ResultSetRecord");
		  totAccounts = vVector.size();
		 }
		 else {
			 totAccounts=0;
		 }
		 numTotalAccounts=(totAccounts > DEFAULT_ROWS)?totAccounts:DEFAULT_ROWS;
	      Debug.debug("UserAuthorizedAccountList Size = "+numTotalAccounts);
	    //if add button is pressed increment Accounts by 4
/*	      if (buttonPressed.equals(TradePortalConstants.BUTTON_ADD_4_MORE_ACCOUNTS)) {
	    	  numTotalAccounts += 4;
	          onLoad += "location.hash='#BottomOfAccounts" + "';";
	      }		 */
	      account = new UserAuthorizedAccountWebBean[numTotalAccounts];

		  for (int iLoop=0;iLoop<totAccounts;iLoop++)
	      {
		    DocumentHandler acctDoc = (DocumentHandler) vVector.elementAt(iLoop);
		    account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
		    try { sValue = acctDoc.getAttribute("/AUTHORIZED_ACCOUNT_OID"); }
		    catch (Exception e) { sValue = ""; }
		    account[iLoop].setAttribute("authorized_account_oid", sValue);

		    try { sValue = acctDoc.getAttribute("/A_ACCOUNT_OID"); }
		    catch (Exception e) { sValue = ""; }
		    account[iLoop].setAttribute("account_oid", sValue);

		    try { sValue = acctDoc.getAttribute("/ACCOUNT_DESCRIPTION"); }
		    catch (Exception e) { sValue = ""; }
		    account[iLoop].setAttribute("account_description", sValue);
		  }

		  if(totAccounts < DEFAULT_ACCOUNT_COUNT) {
			  for(int iLoop = totAccounts; iLoop < DEFAULT_ACCOUNT_COUNT; iLoop++) {
			  	account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
			  }
		  }

		  //CR-586 Vshah [BEGIN]
		  String  tgValue;
		  Vector tgVector =null;
		  if (retrieveFromDB) {
		  	  StringBuffer sql = new StringBuffer();
		  	  queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
		  	  sql.append("select authorized_template_group_oid, a_template_group_oid from USER_AUTHORIZED_TEMPLATE_GROUP, PAYMENT_TEMPLATE_GROUP ");
		  	  sql.append(" where p_user_oid =? ");
		  	  sql.append(" and a_template_group_oid = payment_template_group_oid " );
		  	  sql.append(" order by ");
		  	  sql.append(resMgr.localizeOrderBy("authorized_template_group_oid"));
		  	  Debug.debug(sql.toString());
		  	  queryListView.setSQL(sql.toString(),new Object[]{ user.getAttribute("user_oid")});
		  	  queryListView.getRecords();

		  	  DocumentHandler templateGrpList = new DocumentHandler();
		  	  templateGrpList = queryListView.getXmlResultSet();
		  	  tgVector = templateGrpList.getFragments("/ResultSetRecord");
		  	  totTemplateGroups = tgVector.size();
		  }
		  else {
		          totTemplateGroups=0;
		  }

		  numTotalTemplateGroups=(totTemplateGroups > DEFAULT_ROWS)?totTemplateGroups:DEFAULT_ROWS;
		  Debug.debug("UserAuthorizedTemplateGroupList Size = " + numTotalTemplateGroups);
		  templateGroup = new UserAuthorizedTemplateGroupWebBean[numTotalTemplateGroups];

		  for (int iLoop=0;iLoop<totTemplateGroups;iLoop++)
		  {
		      DocumentHandler templateGrpDoc = (DocumentHandler) tgVector.elementAt(iLoop);
		      templateGroup[iLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
		      try { tgValue = templateGrpDoc.getAttribute("/AUTHORIZED_TEMPLATE_GROUP_OID"); }
		      catch (Exception e) { tgValue = ""; }
		      templateGroup[iLoop].setAttribute("authorized_template_group_oid", tgValue);

		      try { tgValue = templateGrpDoc.getAttribute("/A_TEMPLATE_GROUP_OID"); }
		      catch (Exception e) { tgValue = ""; }
		      templateGroup[iLoop].setAttribute("payment_template_group_oid", tgValue);


		  }

		  if(totTemplateGroups < DEFAULT_TEMPLATE_COUNT) {
		    for(int iLoop = totTemplateGroups; iLoop < DEFAULT_TEMPLATE_COUNT; iLoop++) {
		    	templateGroup[iLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
		    }
		  }
		  //CR-586 Vshah [END]

		}
		catch (Exception e) { e.printStackTrace(); }
		finally {
		try {
		  if (queryListView != null) {
		    queryListView.remove();
		  }
		} catch (Exception e) { System.out.println("Error removing queryListView in UserDetail.jsp"); }
		}



  }

  //UserAuthorizedAccountWebBean account[] = new UserAuthorizedAccountWebBean[numTotalAccounts];
  for (int iLoop=totAccounts; iLoop<numTotalAccounts; iLoop++)
  {
    account[iLoop] = beanMgr.createBean(UserAuthorizedAccountWebBean.class, "UserAuthorizedAccount");
  }

  //CR-586 Vshah [BEGIN]
  for (int jLoop=totTemplateGroups; jLoop<numTotalTemplateGroups; jLoop++)
  {
     templateGroup[jLoop] = beanMgr.createBean(UserAuthorizedTemplateGroupWebBean.class, "UserAuthorizedTemplateGroup");
  }
  //CR-586 Vshah [END]

//CR-451 NShrestha 10/16/2008 Begin --

  // This println is useful for debugging the state of the jsp.
  Debug.debug("retrieveFromDB: " + retrieveFromDB + " getDataFromDoc: "
              + getDataFromDoc + " insertMode: " + insertMode + " oid: "
              + oid);

  if (isReadOnly || insertMode) showDelete = false;
  if (isReadOnly) showSave = false;
  //Suresh CR-603 03/18/2011 Begin
  if (!insertMode)
  {
	  userDetailReptCateg1Oid		  = user.getAttribute("first_rept_categ");
	  userDetailReptCateg2Oid		  = user.getAttribute("second_rept_categ");
	  userDetailReptCateg3Oid		  = user.getAttribute("third_rept_categ");
	  userDetailReptCateg4Oid		  = user.getAttribute("fourth_rept_categ");
	  userDetailReptCateg5Oid		  = user.getAttribute("fifth_rept_categ");
	  userDetailReptCateg6Oid		  = user.getAttribute("sixth_rept_categ");
	  userDetailReptCateg7Oid		  = user.getAttribute("seventh_rept_categ");
	  userDetailReptCateg8Oid		  = user.getAttribute("eighth_rept_categ");
	  userDetailReptCateg9Oid		  = user.getAttribute("nineth_rept_categ");
	  userDetailReptCateg10Oid	      = user.getAttribute("tenth_rept_categ");
  }
  //Suresh CR-603 03/18/2011 End
  isWorkGroupReadOnly = (isReadOnly || userSession.isUsingSubsidiaryAccess()); //WorkGroup not available for subsidiary access
%>

<%
  // Retrieve the data used to populate some of the dropdowns.

  StringBuilder sql = new StringBuilder();

  QueryListView queryListView = null;

  try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView");

      // Get all the thrshold groups that the user may be assigned to
      sql.append("select threshold_group_oid, threshold_group_name ");
      sql.append(" from threshold_group");
      sql.append(" where p_corp_org_oid = ?");
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("threshold_group_name"));
      Debug.debug(sql.toString());

      queryListView.setSQL(sql.toString(),new Object[]{  userSession.getOwnerOrgOid()});

      queryListView.getRecords();

      thresholdGroupDoc = queryListView.getXmlResultSet();
      Debug.debug(thresholdGroupDoc.toString());

      // Get all the security profiles that the user may be assigned to
      sql = new StringBuilder();
      List<Object> sqlParamsSecProf = new ArrayList();
      sql.append("select security_profile_oid, name ");
      sql.append(" from security_profile");
      sql.append(" where security_profile_type = 'NON_ADMIN'");
      sql.append(" and (p_owner_org_oid = ?" );
      sqlParamsSecProf.add(userSession.getOwnerOrgOid());
      // Also include templates from the user's actual organization if using subsidiary access
      if(userSession.showOrgDataUnderSubAccess())
       {
          sql.append(" or p_owner_org_oid = ?");// LOGANATHAN - 05/09/2005 - IR CDUE080455428
          sqlParamsSecProf.add(userSession.getSavedUserSession().getOwnerOrgOid());
       }

      sql.append(" or p_owner_org_oid = ? or p_owner_org_oid = ? or p_owner_org_oid = ? ) order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      sqlParamsSecProf.add(userSession.getGlobalOrgOid());
      sqlParamsSecProf.add(userSession.getBogOid());
      sqlParamsSecProf.add(userSession.getClientBankOid());
	  Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString() ,sqlParamsSecProf);

      queryListView.getRecords();

      secProfileDoc = queryListView.getXmlResultSet();
      Debug.debug(secProfileDoc.toString());

      // Get all the Work Groups that the user may be assigned to
      sql = new StringBuilder();
      
      sql.append("select work_group_oid, work_group_name ");
      sql.append(" from work_group");
      sql.append(" where p_corp_org_oid = ?");
	  sql.append(" and activation_status =?");
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("work_group_name"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),new Object[]{  userSession.getOwnerOrgOid(),TradePortalConstants.ACTIVE});

      queryListView.getRecords();

      workGroupDoc = queryListView.getXmlResultSet();
      Debug.debug(workGroupDoc.toString());
   // CR-451 NShrestha 10/16/2008 Begin --

   // Get all the Account defined in corporate customer that the user may be assigned to
      sql = new StringBuilder();
      List<Object> sqlParamsAcc = new ArrayList();
      sql.append("select account_oid, account_number || ' ' || account_name as account_number");
     // sql.append(", othercorp_customer_indicator"); //Rkazi IR HRUL062032745 Added this field as it is needed for filtering account dropdown
      sql.append(" from account");
      sql.append(" where p_owner_oid = ?");
      sql.append(" and deactivate_indicator != ?");
      sqlParamsAcc.add(userSession.getOwnerOrgOid());
      sqlParamsAcc.add(TradePortalConstants.INDICATOR_YES);
      
      //IAZ 08/07/09 Add
	  //RKAZI 06/24/2011 HRUL062032745 START
	  if (TradePortalConstants.INDICATOR_NO.equals(allowPayByAnotherAccnt)){
    	  sql.append(" and othercorp_customer_indicator != ?");
    	  sqlParamsAcc.add(TradePortalConstants.INDICATOR_YES);
      }
  	  //RKAZI 06/24/2011 HRUL062032745 END
	  sql.append(" order by ");
	  sql.append(resMgr.localizeOrderBy("account_number"));
      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),sqlParamsAcc);

      queryListView.getRecords();

      accountDoc = queryListView.getXmlResultSet();
      Debug.debug(accountDoc.toString());

   // CR-451 NShrestha 10/16/2008 End --

      //CR-586 Vshah [BEGIN]
      sql = new StringBuilder();
      List<Object> sqlParamsTemp = new ArrayList();
      sql.append("select payment_template_group_oid, name");
      sql.append(" from payment_template_group");
      sql.append(" where P_OWNER_ORG_OID in ( ? ,?,?,?)");
      	
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("name"));
      Debug.debug(sql.toString());
      sqlParamsTemp.add(userSession.getOwnerOrgOid());
      sqlParamsTemp.add(userSession.getClientBankOid());
      sqlParamsTemp.add(userSession.getGlobalOrgOid());
      sqlParamsTemp.add(userSession.getBogOid());
      queryListView.setSQL(sql.toString(),sqlParamsTemp);

      queryListView.getRecords();

      templateGroupDoc = queryListView.getXmlResultSet();
      Debug.debug(templateGroupDoc.toString());
      //CR-586 Vshah [END]


    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in UserDetail.jsp");
      }
  }  // try/catch/finally block


  // Determine if the bank requires passwords for authentication.  This
  // boolean gets passed to a fragment when creating the security section.
  //jgadela R90 IR T36000026319 - SQL INJECTION FIX
  String passwordSql = "select authentication_method "
                     + "from corporate_org where organization_oid = ?";

  DocumentHandler result = DatabaseQueryBean.getXmlResultSet(passwordSql, false, new Object[]{userSession.getOwnerOrgOid()});

  String corpAuthMethod = result.getAttribute("/ResultSetRecord(0)/AUTHENTICATION_METHOD");
  String userAuthMethod = user.getAttribute("authentication_method");
  String additionalAuthType = user.getAttribute("additional_auth_type");
  String additionalAuth2FAType = user.getAttribute("auth2fa_subtype");
  String userPrefMobileBankingInd = user.getAttribute("mobile_banking_access_ind");//MEerupula CR-934A
  emailAddressFromDoc = user.getAttribute("email_addr");
	// [START] CR-482


	if(instanceHSMEnabled)
	{
		String ownershipLevel = user.getAttribute("ownership_level");
		if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		{
			Debug.debug("[UserDetail.jsp] User's ownership level is OWNER_GLOBAL");
			stricterPasswordSetting = SecurityRules.getProponixRequireUpperLowerDigit();
			Debug.debug("[UserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
			minimumPasswordLength = String.valueOf(SecurityRules.getProponixPasswordMinimumLengthLimit());
			Debug.debug("[UserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
		} // if(ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL))
		else
		{
			ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class, "ClientBank");

			// [START] PAUK020539189
			String clientBankOid = null;
			if(InstrumentServices.isNotBlank(userSession.getClientBankOid()))
			{
				clientBankOid = userSession.getClientBankOid();
				Debug.debug("[UserDetail.jsp] Got clientBankOid from userSession.getClientBankOid");
			} // if(InstrumentServices.isNotBlank(userSession.getClientBankOid()))
			else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
			{
				clientBankOid = doc.getAttribute("/In/User/user_oid");
				Debug.debug("[UserDetail.jsp] Got clientBankOid from doc.getAttribute(\"/In/User/user_oid\")");
			} // else if(InstrumentServices.isNotBlank(doc.getAttribute("/In/User/user_oid")))
			else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))
			{
				clientBankOid = user.getAttribute("client_bank_oid");
				Debug.debug("[UserDetail.jsp] Got clientBankOid from user.getAttribute(\"client_bank_oid\")");
			} // else if(InstrumentServices.isNotBlank(user.getAttribute("client_bank_oid")))

			if(clientBankOid != null)
			{
				Debug.debug("[UserDetail.jsp] Getting client bank with oid = "+clientBankOid);
				clientBank.getById(clientBankOid);

				if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
				{
					Debug.debug("[UserDetail.jsp] User's ownership level is OWNER_CORPORATE");
					stricterPasswordSetting = clientBank.getAttribute("corp_password_addl_criteria");
					Debug.debug("[UserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
					minimumPasswordLength = clientBank.getAttribute("corp_min_password_length");
					Debug.debug("[UserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
				} // if(ownershipLevel.equals(TradePortalConstants.OWNER_CORPORATE))
				else
				{
					stricterPasswordSetting = clientBank.getAttribute("bank_password_addl_criteria");
					Debug.debug("[UserDetail.jsp] stricterPasswordSetting = " + stricterPasswordSetting);
					minimumPasswordLength = clientBank.getAttribute("bank_min_password_len");
					Debug.debug("[UserDetail.jsp] minimumPasswordLength = " + minimumPasswordLength);
				} // else
			} // if(clientBankOid != null)
		} // else

		// Default values for stricterPasswordSetting and minimumPasswordLength
		if((stricterPasswordSetting == null) || (stricterPasswordSetting.trim().length() == 0))
		{
			stricterPasswordSetting = TradePortalConstants.INDICATOR_YES;
		}
		if((minimumPasswordLength == null) || (minimumPasswordLength.trim().length() == 0))
		{
			minimumPasswordLength = "8";
		}
		// [END] PAUK020539189
	} // if(instanceHSMEnabled)
	// [END] CR-482


%>



<jsp:include page="/common/Header.jsp">
   <jsp:param name="minimalHeaderFlag" value="<%= minimalHeader %>" />
   <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<div class="pageMainNoSidebar">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="<%= pageTitle %>" />
      <jsp:param name="helpUrl"  value="<%= helpFileName %>" />
    </jsp:include>

    <%-- cquinton 10/29/2012 ir#7015 remove return link--%>
    <jsp:include page="/common/PageSubHeader.jsp">
       <jsp:param name="title" value="<%= userSession.getUserFullName() %>" />
    </jsp:include>
        <%
        boolean showtips = userSession.getShowToolTips() == null ? true : userSession.getShowToolTips().booleanValue();
        String datepattern = userSession.getDatePattern();
        %>

    <form id="ChangePassword" name="ChangePassword" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

      <input type=hidden name="buttonName"     value="">
      <input type=hidden value="<%=additionalAuthType%>"
             name="additionalAuthType" id="additionalAuthType">

      <div class="formArea">
        <jsp:include page="/common/ErrorSection.jsp" />
        <div class="formContent"> <%-- Form Content Area starts here --%>
<%
  // [START] CR-482
  if (instanceHSMEnabled)
  {
%>
          <input type=hidden name="encryptedCurrentPassword" value="">
          <%-- [START] CR-543 --%>
         
          <input type=hidden name="protectedCurrentPassword" value="">
          <input type=hidden name="protectedNewPassword" value="">
          <input type=hidden name="encryptedRetypedNewPassword" value="">
          
          <%-- [END] CR-543 --%>
          <input type=hidden name="passwordValidationError" value="">
<%
  } // if (instanceHSMEnabled)
  // [END] CR-482
%>

<%
  Hashtable addlParms = new Hashtable();

  // The system must keep track of how many times the user has entered an invalid current password
  // This is done to prevent someone from attempting to test for different passwords
  // when a user is away from their computer.

  // If the maximum number has been reached, the user is forwarded
  // to the "Too Many Logons" page, which also logs them out
  int numberOfWrongPasswords;

  try {
    numberOfWrongPasswords = Integer.parseInt(doc.getAttribute("/Out/numberOfWrongPasswords"));
  }
  catch(Exception e) {
    numberOfWrongPasswords = 0;
  }


  if ( numberOfWrongPasswords == SecurityRules.getLoginAttemptsBeforeBeingRedirected() ) {
    String tooManyLogonsPage = NavigationManager.getNavMan().getPhysicalPage("TooManyInvalidLogonAttempts", request);

    StringBuffer parms = new StringBuffer();
    parms.append("?branding=");
    parms.append(userSession.getBrandingDirectory());
    parms.append("&organization=");
    parms.append(userSession.getClientBankCode());
    parms.append("&locale=");
    parms.append(userSession.getUserLocale());

%>
          <jsp:forward page='<%= tooManyLogonsPage + parms %>' />
<%
  }

  // Add the numberOfWrongPasswords to formManager
  addlParms.put("numberOfWrongPasswords", String.valueOf(numberOfWrongPasswords));


  addlParms.put("userOID",userSession.getUserOid());

  // The ChangePasswordMediator (where the data is sent from this page)
  // is also used to unlock users.  These flags tell the mediator that we're not unlocking a user here
  addlParms.put("changingPasswordFlag", "true");
  addlParms.put("resettingUserFlag", "false");

  if(forcedToChangePassword) {
    addlParms.put("force",forcedToChangePasswordParm);
    addlParms.put("checkCurrentPasswordFlag", "false");
  }
  else {
    addlParms.put("checkCurrentPasswordFlag", "true");
  }

  if(reasonParam != null) {
    addlParms.put("reason", reasonParam);
  }

  addlParms.put("fromUserPref", "true");

%>

          <%= formMgr.getFormInstanceAsInputField(formName, addlParms) %>

          <%= widgetFactory.createSectionHeader("1", "ChangePassword.PasswordSection") %>
            <div class = "columnLeft" style="border-right:0px">
<%
  if (!forcedToChangePassword) {
%>
              <%= widgetFactory.createTextField( "currentPassword", "ChangePassword.CurrentPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>
<%
  }
%>
              <%= widgetFactory.createTextField( "newPassword", "ChangePassword.NewPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>

              <%= widgetFactory.createTextField( "retypePassword", "ChangePassword.RetypeNewPassword",
                                       "", "32", false, false, false, "type=\"password\" AutoComplete=\"off\"", "", "" ) %>


<%
    options = Dropdown.createSortedRefDataOptions("DATEPATTERN",
                datepattern,
                loginLocale);
%>

              <%=widgetFactory.createSelectField("datepattern",
					"UserDetail.DatePattern", " ", options, isFirstSectionReadOnly,
					true, false, "", "", "")%>

               <%--CR 821 Rel 8.3 START --%>
               <%=widgetFactory.createTextField("emailAddressUserPref","UserDetail.EmailAddress",emailAddressFromDoc,
					"50",isFirstSectionReadOnly,false,false," class='char32'","", "")%>
				<%-- CR 821 Rel 8.3 END --%>

			<div class = "formItem">
                <button id="saveButton" data-dojo-type="dijit.form.Button" type="button" data-dojo-props="iconClass:'save'">
	                  <%=resMgr.getText("common.SaveText",TradePortalConstants.TEXT_BUNDLE)%>
	            </button><%-- save formatting button --%>
                    <%=widgetFactory.createHoverHelp("saveButton","SaveHoverText") %>
               </div><%-- end of formItem --%>
            </div><%-- End of columnLeft --%>
            <div class="columnRight" >
              <%= widgetFactory.createNote("ChangePassword.PasswordRequirements")%>
              <br>
              <br>
              <%= widgetFactory.createNote("ChangePassword.Instructions1") %>
              <br>
              <br>
              <%= widgetFactory.createNote("ChangePassword.Instructions2") %>
              <br>
              <br>
              <%= widgetFactory.createNote("ChangePassword.Instructions3") %>
              <br>
              <br>
              <br>
              <br>
			<%=widgetFactory.createSubLabel("UserDetail.ShowHideTips")%>
			 <div class="formItem">

	       		  <div class="indentHalf">
				  <%=widgetFactory.createRadioButtonField(
					  "showtips",
					  "ShowTips",
					  "UserDetail.ShowTips", "Y",
                       showtips,
                       isFirstSectionReadOnly, "", "")%>

				    <br>
				  <%=widgetFactory.createRadioButtonField(
                       "showtips",
					   "HideTips",
					   "UserDetail.HideTips", "N",
                       ! showtips,
                       isFirstSectionReadOnly, "", "")%>
			</div><%-- indentHalf end --%>
	        	</div> <%-- formItem end --%>
	       <%-- MEerupula Rel8.4 CR-934A --%>
	        <%if(TradePortalConstants.INDICATOR_YES.equals(cbMobileBankingInd) && TradePortalConstants.INDICATOR_YES.equals(corpCustMobileBankingInd)){ %>
           <div id="userPrefMBInd">
            <br>
                <%=widgetFactory.createSubLabel("UserPreferences.EnableMobileBanking")%>
	        	<div class="formItem">
                  <div class="indentHalf">
                  <%=widgetFactory.createRadioButtonField(
                      "mobileBankingAccessInd",
                      "userPrefMBYes",
                      "common.Yes", "Y", 
                      TradePortalConstants.INDICATOR_YES.equals(userPrefMobileBankingInd),
                      false, "", "")%>

                    <br>
                  <%=widgetFactory.createRadioButtonField(
                       "mobileBankingAccessInd",
                       "UserPrefMBNo",
                       "common.No", "N", 
                       TradePortalConstants.INDICATOR_NO.equals(userPrefMobileBankingInd),
                       false, "", "")%>
                   </div><%-- indentHalf end --%>  
               </div> <%-- formItem end --%> 
               </div>
             <%} %>
            </div><%-- End of columnRight --%>
            

          </div><%-- End of createSectionHeader --%>

          <%= widgetFactory.createSectionHeader("2", "ChangePassword.General", "noToggle") %>
            <%@ include file="fragments/UserDetail-General.frag"%>
          </div> <%-- General Tab Ends Here--%>

          <%= widgetFactory.createSectionHeader("3", "ChangePassword.Security", "noToggle") %>
            <%@ include file="fragments/UserSecuritySection.frag"%>
          </div> <%-- Security Tab Ends Here --%>

          <%= widgetFactory.createSectionHeader("4", "ChangePassword.AssignedTo", "noToggle") %>
            <%@ include  file="fragments/UserDetail-AssignedTo.frag"%>
          </div> <%-- AssignedTo Tab Ends Here --%>

          <%= widgetFactory.createSectionHeader("5", "ChangePassword.PanelAuthoritySection", "noToggle") %>
            <%@ include  file="fragments/UserDetail-PanelAuthority.frag"%>
          </div> <%-- Panel Authority Tab Ends Here --%>

          <%= widgetFactory.createSectionHeader("6", "ChangePassword.TransactionProcessingSetting", "noToggle") %>
            <%@ include  file="fragments/UserDetail-TransactionProcessingSetting.frag"%>
          </div> <%-- Transaction Processing Setting Tab Ends Here --%>

          <%= widgetFactory.createSectionHeader("7", "ChangePassword.TemplateGroups", "noToggle") %>
            <%@ include  file="fragments/UserDetail-TemplateGroups.frag"%>
          </div> <%-- Template Groups Tab Ends Here --%>

          <%= widgetFactory.createSectionHeader("8", "ChangePassword.AccountAvailableforMakingPayments", "noToggle") %>
            <%@ include  file="fragments/UserDetail-AccountAccessforFundsTransfer.frag"%>
          </div> <%-- Account Access for FundsTransfer Tab Ends Here --%>

          <%= widgetFactory.createSectionHeader("9", "ChangePassword.ReportCategs", "noToggle") %>
            <%@ include  file="fragments/ChangePassword-ReportCategories.frag"%>
          </div> <%-- Report Categs Tab Ends Here --%>

          <%= widgetFactory.createSectionHeader("10", "ChangePassword.SubsidiaryAccess", "noToggle") %>
            <%@ include  file="fragments/UserDetail-SubsidiaryAccess.frag"%>
          </div> <%-- Report Categs Tab Ends Here --%>
<%
  // ONLY SHOW THIS SECTION IF THE USER'S CORP ORG HAS DOC PREP ACCESS
  if (corporateOrg.getAttribute("doc_prep_ind").equals(TradePortalConstants.INDICATOR_YES)) {
%>
          <%= widgetFactory.createSectionHeader("11", "ChangePassword.DocumentPreparation", "noToggle") %>
            <%@ include  file="fragments/UserDetail-CorporateServices.frag"%>
          </div>
<%
  }
%>

        </div> <%-- Form Content End Here --%>
      </div><%--formArea--%>
		<%-----rpasupulati 11/05/2012 adding cancelaction parmeter in refdata side bar so that up on closeing form navigates to Home----- --%>
      <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'ChangePassword'">
        <jsp:include page="/common/RefDataSidebar.jsp">
          <jsp:param name="showTop" value="true" />
          <jsp:param name="showLinks" value="true" />
          <jsp:param name="cancelAction" value="goToTradePortalHome" />
        </jsp:include>
      </div> <%--closes sidebar area--%>

    <%--cquinton 10/31/2012 move closing form tag so it is before footer, which has other forms--%>
    </form>
  </div>
</div>

<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

<%
  // [START] CR-482
  // Include javascript functions for encryption
  if (instanceHSMEnabled)
  {
%>
<script src="js/crypto/pidcrypt.js"></script>
<script src="js/crypto/pidcrypt_util.js"></script>
<script src="js/crypto/asn1.js"></script><%-- needed for parsing decrypted rsa certificate --%>
<script src="js/crypto/jsbn.js"></script><%-- needed for rsa math operations --%>
<script src="js/crypto/rng.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/prng4.js"></script><%-- needed for rsa key generation --%>
<script src="js/crypto/rsa.js"></script><%-- needed for rsa en-/decryption --%>
<script src="js/crypto/md5.js"></script><%-- needed for key and iv generation --%>
<script src="js/crypto/aes_core.js"></script><%-- needed block en-/decryption --%>
<script src="js/crypto/aes_cbc.js"></script><%-- needed for cbc mode --%>
<script src="js/crypto/sha256.js"></script>
<script src="js/crypto/tpprotect.js"></script><%-- needed for cbc mode --%>
<%
  } // if (instanceHSMEnabled)
%>

<script>

  <%--change password event handler--%>
  require(["dojo/query","dojo/dom", "dojo/on", "dijit/registry", "dojo/ready", "dojo/domReady!"],
      function(query, dom, on, registry, ready) {


	  ready(function() {
    //Save Password button
    var saveButton = dom.byId("saveButton");
    on(saveButton, "click", function(evt){


<%
  if (instanceHSMEnabled) {
    UserWebBean user1 = beanMgr.createBean(UserWebBean.class,    "User");
    user1.getById(userSession.getUserOid());
%>

		var public_key = '-----BEGIN PUBLIC KEY-----\n<%= HSMEncryptDecrypt.getPublicKey() %>\n-----END PUBLIC KEY-----';
		var iv = '<%= HSMEncryptDecrypt.getNewIV() %>';
		var protector = new TPProtect();

      <%-- Ensure that the new password and retyped password match --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.MatchingEntries({errorCode: "<%= TradePortalConstants.ENTRIES_DONT_MATCH %>", newPassword: ChangePassword.newPassword.value, retypePassword: ChangePassword.retypePassword.value});
      }

      <%-- Ensure that the new password is not the same as the user_identifier --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("user_identifier") %>"});
      }

      <%-- Ensure that the new password is not the same as the first_name --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("first_name") %>"});
      }

      <%-- Ensure that the new password is not the same as the last_name --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("last_name") %>"});
      }

      <%-- Ensure that the new password is not the same as the login_id --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsSameAs({errorCode: "<%= TradePortalConstants.PASSWORD_SAME_AS %>", newPassword: ChangePassword.newPassword.value, sameAsString: "<%= user.getAttribute("login_id") %>"});
      }

      <%-- Ensure that the new password does not start or end with a space --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.StartEndWithSpace({errorCode: "<%= TradePortalConstants.PASSWORD_SPACE %>", newPassword: ChangePassword.newPassword.value});
      }

      <%-- Ensure that the new password is the required length --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.IsRequiredLength({errorCode: "<%= TradePortalConstants.PASSWORD_TOO_SHORT %>", newPassword: ChangePassword.newPassword.value, reqLength: <%= minimumPasswordLength %>});
      }

      <%-- Ensure that the new password meets the consecutive characters requirements --%>
      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.CheckConsecutiveChars({errorCode: "<%= TradePortalConstants.PASSWORD_CONSECUTIVE %>", newPassword: ChangePassword.newPassword.value});
      }

<%
    if(stricterPasswordSetting.equals(TradePortalConstants.INDICATOR_YES)) { %>

      if (ChangePassword.passwordValidationError.value == "") {
        ChangePassword.passwordValidationError.value = protector.CheckPasswordComplexity({errorCode: "<%= TradePortalConstants.PASSWORD_UPPER_LOWER_DIGIT %>", newPassword: ChangePassword.newPassword.value});
      }
<%
    }
%>

      <%-- Encrypt two versions of the current password, clear-text and SHA-256 hashed --%>
      <%-- PMitnala Rel 8.3 IR T36000022128 - Additional check to see if the currentPassword value is not blank. 
      This will prevent the encryptedPassword from being sent to mediator, if there is no change done by the user. In a case where only emailaddress is changed--%>
      if (ChangePassword.currentPassword && ChangePassword.currentPassword.value != "") {
        ChangePassword.encryptedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: ChangePassword.currentPassword.value});
        <%-- [START] CR-543 --%>
        var currentPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: ChangePassword.currentPassword.value, challenge: iv});
        ChangePassword.protectedCurrentPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: currentPasswordMac});
      }

      <%-- Encrypt the MAC of the new password --%>
      <%-- PMitnala Rel 8.3 IR T36000022128 - Additional check to see if the currentPassword value is not blank. 
      This will prevent the encryptedPassword from being sent to mediator, if there is no change done by the user. In a case where only emailaddress is changed--%>
      
      <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- START --%>
      if (ChangePassword.newPassword && ChangePassword.newPassword.value != "") {
        var newPasswordMac = protector.SignMAC({publicKey: public_key, usePem: true, iv: iv, password: ChangePassword.newPassword.value, challenge: iv});
        var combinedNewPassword = newPasswordMac + '|' + document.ChangePassword.newPassword.value;
        ChangePassword.protectedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: combinedNewPassword});
      }
      
      
      if (document.ChangePassword.retypePassword) {
          document.ChangePassword.encryptedRetypedNewPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: document.ChangePassword.retypePassword.value});  
       }    
      <%-- T36000030120-smanohar--	check password complexity at server also for HSMEnabled instances-- END --%>
      
      <%-- [END] CR-543 --%>

      <%-- Blank out the password fields --%>
      if (ChangePassword.currentPassword) {
        ChangePassword.currentPassword.value = "";
      }
      if (ChangePassword.newPassword) {
        ChangePassword.newPassword.value = "";
      }
      if(ChangePassword.retypePassword) {
        ChangePassword.retypePassword.value = "";
      }
<%
  } // if (instanceHSMEnabled)
%>

      setButtonPressed('<%= TradePortalConstants.BUTTON_SAVE %>','0');
      document.forms[0].submit();
    });
	  });

    ready(function() {

      <%--cquinton 2/1/2013 ir#10680 move titlepane non toggle behavior to widgetfactory--%>

      <%-- SaveFormatting Button --%>
      <%-- buttonpressed to be called for saveFormating button --%>
      var saveFormattingButton = dom.byId("<%=TradePortalConstants.BUTTON_SAVE_FORMATTING%>");
      if ( saveFormattingButton ) {
        on(saveFormattingButton, "click", function(evt){
          setButtonPressed('<%= TradePortalConstants.BUTTON_SAVE_FORMATTING %>','0');
          document.forms[0].submit();
        });
      }
    });

  });
  


</script>

<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>

</body>
</html>
