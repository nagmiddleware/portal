<%--
*******************************************************************************
                                  ThresholdGroupRow

  Description:
     Create html tags that displays a row in the Threshold Group 
	 Detail page consisting of the instrument type, transaction type,
	 currency, threshold amount and daily limit amount.


  Parameters:
  
 	instrument Type 
	transaction Type
 	currency 			- currency code to display
	locale 				- user's locale
	thresholdName		- threshold amount name, 
	thresholdValue		- threshold amount value,
	limitName			- daily limit name,
	limitValue			- daily limit value	
	readOnly			- TradePortalConstants.INDICATOR_YES or INDICATOR_NO 
						  indicates if the field is readOnly
	greyColor			- TradePortalConstants.INDICATOR_YES or INDICATOR_NO
						  indicates if row is to be in grey color 
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.html.*, com.ams.tradeportal.common.*, com.amsinc.ecsg.util.StringFunction" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<%
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   
   	String instrumentType 	= request.getParameter("instrumentType");
   	String transactionType 	= request.getParameter("transactionType"); 
   	String currency 		= request.getParameter("currency"); 	   
	String locale			= request.getParameter("locale");
	String thresholdName	= request.getParameter("thresholdName"); 
 	String thresholdValue	= request.getParameter("thresholdValue");
 	String limitName		= request.getParameter("limitName"); 	   
	String limitValue		= request.getParameter("limitValue"); 
	String rowSpan			= request.getParameter("rowSpan");
	boolean isReadOnly		= request.getParameter("isReadOnly").equals("true")? true:false;
	boolean greyColor 		= request.getParameter("greyColor").equals("true")? true:false;
	boolean displayErrors	= request.getParameter("displayErrors").equals("true")? true:false;
	boolean span			= request.getParameter("span").equals("true")? true:false;
	
	//Sandeep - Rel 8.3 System Test IR# T36000020808 09/17/2013 - Begin
	//As Part of the Vera Code's 'XSS cross browser check', symbols like '<', '>', '&', '"', ''', '(', and ')' are encoding to some special charecters, 
	//here 'instrumentType' peramater is not a code/lookup value; it is comming as a formated HTML content, so all the above mentioned symbols are encoding to different values.
	//Due to that in UI the comming parameter is considered as a string instead of a HTML content while calling widgetFactory.createSubLabel().
	//Hence commenting the below code.
	//Validation to check Cross Site Scripting	  
	/*
	if(instrumentType != null){
		instrumentType = StringFunction.xssCharsToHtml(instrumentType);
	}
	*/
	//Sandeep - Rel 8.3 System Test IR# T36000020808 09/17/2013 - End

   // Print out a row of text, including input amounts, into html
	%>
	<tr>
		<% 
			String instrType = instrumentType.equals("")?"":instrumentType;
			if(span){
		%>
		<td rowSpan="<%=StringFunction.xssCharsToHtml(rowSpan)%>" valign="top" width="30%">		 
			<%= widgetFactory.createSubLabel(instrType) %>	 
		</td>
		<%} %>
      	<td width="30%">
			<%= widgetFactory.createSubLabel(transactionType) %>	
		</td>
		<td width="4%">
			<%= widgetFactory.createSubLabel(StringFunction.xssCharsToHtml(currency)) %>	
		</td>
		<%-- MEer Rel 9.3 XSS CID-11405, 11359, 11476, 11497 --%>
		<td width="23%">		
			<%= widgetFactory.createTextField(StringFunction.xssCharsToHtml(thresholdName), "", displayErrors? StringFunction.xssCharsToHtml(thresholdValue):TPCurrencyUtility.getDisplayAmount(StringFunction.xssCharsToHtml(thresholdValue), currency, locale), "22", isReadOnly, false, false, "class='char15'", "", "none") %> 				
		</td>
      	<td width="23%">     		
			<%= widgetFactory.createTextField(StringFunction.xssCharsToHtml(limitName), "", displayErrors? StringFunction.xssCharsToHtml(limitValue):TPCurrencyUtility.getDisplayAmount(StringFunction.xssCharsToHtml(limitValue), currency, locale), "22", isReadOnly, false, false, "class='char15'", "", "none") %>				
		</td>
	</tr> 
