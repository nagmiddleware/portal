<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@page import="javax.servlet.jsp.tagext.TryCatchFinally"%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,java.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*, com.ams.tradeportal.common.cache.*, javax.crypto.SecretKey" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session"></jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session"></jsp:useBean>

<%
	Debug.debug("******************");
	/****************************************************************
	 * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A BANKGRPRESTRICTRULE
	 * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
	 * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
	 * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
	 * TO THE FORM MANAGER.
	 ****************************************************************/

	/**********
	 * GLOBALS *
	 **********/
	boolean isNew = false; //tells if this is a new BankGrpRestrictRule
	boolean isReadOnly = false; //tell if the page is read only
	boolean getDataFromDoc = false;
	boolean showSave = true;
	boolean showSaveClose = true;
	boolean showDelete = true;
	boolean showSaveAs = true;

	String oid = "";
	String loginRights;
	StringBuffer sqlQuery = null;
	QueryListView queryListView = null;
	DocumentHandler bankGroupsDoc = null;
	String userOrgOid = null;
	String defaultText="";
	String options;
	BankGrpForRestrictRuleWebBean bankGrpRestictionForRules[]=null;
	int numTotalBankGroups = 0;
	int numTotalBankGroupRows = 4;
	int totBankGroups =0;
    
    

	beanMgr.registerBean(
			"com.ams.tradeportal.busobj.webbean.BankGrpRestrictRuleWebBean",
			"BankGrpRestrictRule");
	BankGrpRestrictRuleWebBean editBankGrpRestrictRule = (BankGrpRestrictRuleWebBean) beanMgr
			.getBean("BankGrpRestrictRule");

	Hashtable secureParams = new Hashtable();

	DocumentHandler doc = null;

	//Get WidgetFactory instance..
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	String refDataHome = "goToRefDataHome"; //used for the close button and Breadcrumb link
	String buttonPressed = "";
	//Get security rights
	loginRights = userSession.getSecurityRights();
	userOrgOid = userSession.getOwnerOrgOid();
	isReadOnly = !SecurityAccess.hasRights(loginRights,
			SecurityAccess.MAINTAIN_BANK_GROUP_RESTRICT_RULES);
	Debug.debug("MAINTAIN_BANK_GROUP_RESTRICT_RULES, isReadOnly == "
			+ SecurityAccess.hasRights(loginRights,
					SecurityAccess.MAINTAIN_BANK_GROUP_RESTRICT_RULES)
			+ ", " + isReadOnly);

	// Get the document from the cache.  We'll use it to repopulate the screen if the
	// user was entering data with errors.
	doc = formMgr.getFromDocCache();
	buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
	Vector error = null;
	error = doc.getFragments("/Error/errorlist/error");
	Debug.debug("doc from cache is " + doc.toString());

	if (doc.getDocumentNode("/In/BankGrpRestrictRule") != null) {
		// The doc does exist so check for errors.

		String maxError = doc.getAttribute("/Error/maxerrorseverity");
		getDataFromDoc = true;
		oid = doc
				.getAttribute("/Out/BankGrpRestrictRule/bank_grp_restrict_rules_oid");
		//if this was an update not an insert, we need to get from in
		if (oid == null) {
			oid = doc
					.getAttribute("/In/BankGrpRestrictRule/bank_grp_restrict_rules_oid");
		}

		if (maxError != null
				&& maxError.compareTo(String
						.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
			//no errors, get data from db
			getDataFromDoc = false;

		} else {
			// We've returned from a save/update/delete but have errors.
			// We will display data from the cache.  Determine if we're
			// in insertMode by looking for a '0' oid in the input section
			// of the doc.
			getDataFromDoc = true;
			if (doc.getAttribute(
					"/In/BankGrpRestrictRule/bank_grp_restrict_rules_oid")
					.equals("0")) {
				Debug.debug("Create BankGrpRestrictRule Error");
				isNew = true;//else it stays false
			}

			if (!isNew)
			// Not in insert mode, use oid from input doc.
			{
				Debug.debug("Update BankGrpRestrictRule Error");
				oid = doc
						.getAttribute("/In/BankGrpRestrictRule/bank_grp_restrict_rules_oid");
			}
		}
	}

	if (getDataFromDoc) {

		DocumentHandler doc2 = doc.getComponent("/In");
		Debug.debug("/In  " + doc2);
		editBankGrpRestrictRule.populateFromXmlDoc(doc2);
		
		Vector bgVector = doc2.getFragments("/BankGrpRestrictRule/BankGrpForRestrictRuleList");
        totBankGroups = bgVector.size();
        String  bgValue;
        bankGrpRestictionForRules = new BankGrpForRestrictRuleWebBean[totBankGroups];
         int totalRows=(totBankGroups%3>0)?(totBankGroups/3)+1:totBankGroups/3;
         numTotalBankGroupRows = (numTotalBankGroupRows>totalRows)?numTotalBankGroupRows:totalRows;
        
	    for (int iLoop=0; iLoop<totBankGroups; iLoop++)
	    {
	 	     bankGrpRestictionForRules[iLoop] = beanMgr.createBean(BankGrpForRestrictRuleWebBean.class, "BankGrpForRestrictRule");
		     DocumentHandler bankGrpforrestricDoc = (DocumentHandler) bgVector.elementAt(iLoop);
			 try { bgValue = bankGrpforrestricDoc.getAttribute("/bank_grp_for_restrict_rls_oid"); }
			 catch (Exception e) { bgValue = ""; }
			 bankGrpRestictionForRules[iLoop].setAttribute("bank_grp_for_restrict_rls_oid", bgValue);
	
			 try { bgValue = bankGrpforrestricDoc.getAttribute("/bank_organization_group_oid"); }
			 catch (Exception e) { bgValue = ""; }
			 bankGrpRestictionForRules[iLoop].setAttribute("bank_organization_group_oid", bgValue);
	
	
		}
		
		
		if (!isNew) {
			editBankGrpRestrictRule.setAttribute(
					"bank_grp_restrict_rules_oid", oid);
			oid = editBankGrpRestrictRule
					.getAttribute("bank_grp_restrict_rules_oid");
		}
	} else if (request.getParameter("oid") != null
			|| (oid != null && !"".equals(oid.trim()) && !"0"
					.equals(oid))) //EDIT BankGrpRestrictRule
	{
		isNew = false;
		if (oid != null && !"".equals(oid.trim()) && !"0".equals(oid)) {
			//We are back in this page from a successful save.
		} else if (request.getParameter("oid") != null) {
			Debug.debug("Oid in request if"
					+ request.getParameter("oid"));
			oid = EncryptDecrypt.decryptStringUsingTripleDes(
					request.getParameter("oid"),
					userSession.getSecretKey());
		}
		editBankGrpRestrictRule.getById(oid);
		
		sqlQuery = new StringBuffer();
        queryListView = null;
            
            try
            {
              queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
              sqlQuery.append("select BANK_GRP_FOR_RESTRICT_RLS_OID,BANK_ORGANIZATION_GROUP_OID ");
              sqlQuery.append(" from BANK_GRP_FOR_RESTRICT_RULES");
              sqlQuery.append(" where P_BANK_GRP_RESTRICT_RULES_OID = ? ");
              sqlQuery.append(" order by BANK_GRP_FOR_RESTRICT_RLS_OID");
              List params=new ArrayList();
	    	  params.add(oid);
	          DocumentHandler bankGrpList = new DocumentHandler();
              bankGrpList = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(),false,params);
              Vector bgVector = bankGrpList.getFragments("/ResultSetRecord");
              String  bgValue;
              totBankGroups = bgVector.size();
              bankGrpRestictionForRules = new BankGrpForRestrictRuleWebBean[totBankGroups];
        	  int totalRows =(totBankGroups%3>0)?(totBankGroups/3)+1:totBankGroups/3;
        	  numTotalBankGroupRows = (numTotalBankGroupRows>totalRows)?numTotalBankGroupRows:totalRows;
        
		      for (int iLoop=0; iLoop<totBankGroups; iLoop++)
		      {
		 	     bankGrpRestictionForRules[iLoop] = beanMgr.createBean(BankGrpForRestrictRuleWebBean.class, "BankGrpForRestrictRule");
			     DocumentHandler bankGrpforrestricDoc = (DocumentHandler) bgVector.elementAt(iLoop);
				 try { bgValue = bankGrpforrestricDoc.getAttribute("/BANK_GRP_FOR_RESTRICT_RLS_OID"); }
				 catch (Exception e) { bgValue = ""; }
				 bankGrpRestictionForRules[iLoop].setAttribute("bank_grp_for_restrict_rls_oid", bgValue);
		
				 try { bgValue = bankGrpforrestricDoc.getAttribute("/BANK_ORGANIZATION_GROUP_OID"); }
				 catch (Exception e) { bgValue = ""; }
				 bankGrpRestictionForRules[iLoop].setAttribute("bank_organization_group_oid", bgValue);
		
		
			  }
            
            }
            catch (Exception e) { e.printStackTrace(); }
		
	}

	else //NEW BankGrpRestrictRule
	{
		isNew = true;
		oid = null;
		showSaveAs = true;
	}

	// Auto save the form when time-out if not readonly.  
	// (Header.jsp will check for auto-save setting of the corporate org).
	String onLoad = "";

	String autoSaveFlag = isReadOnly
			? TradePortalConstants.INDICATOR_NO
			: TradePortalConstants.INDICATOR_YES;

	if (isReadOnly) {
		showSave = false;
		showDelete = false;
		showSaveClose = false;
		showSaveAs = false;
	} else if (isNew) {
		showDelete = false;
		showSaveAs = false;
	}
		
		try{
			sqlQuery = new StringBuffer();
        	queryListView = null;
			sqlQuery.append("select organization_oid, name");
			sqlQuery.append(" from bank_organization_group");
			sqlQuery.append(" where p_client_bank_oid = ? and activation_status = ? order by ");
			sqlQuery.append(resMgr.localizeOrderBy("name"));
			queryListView=(QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
			queryListView.setSQL(sqlQuery.toString(), new Object[]{userOrgOid, TradePortalConstants.ACTIVE});
			queryListView.getRecords();
			bankGroupsDoc = queryListView.getXmlResultSet();
		}catch(Exception e) { e.printStackTrace(); 
		}finally {
            try {
              if (queryListView != null) {
                queryListView.remove();
              }
            } catch (Exception e) { System.out.println("Error removing queryListView in BankGrpRestrnRuleDetail.jsp"); }
        }
	
	
	
	 
%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">
	<%-- Page Main Start --%>
	<div class="pageContent">
		<%--  Page Content Start --%>


		<jsp:include page="/common/PageHeader.jsp">
			<jsp:param name="titleKey" value='<%=resMgr.getText("BankGrpRestrictRuleDetail.BankGrpRestrictRuleTitleKey", TradePortalConstants.TEXT_BUNDLE) %>' />
			<jsp:param name="helpUrl" value="admin/bank_group_restriction_rules.htm" />
		</jsp:include>


		<%
			String subHeaderTitle = "";
			if (isNew) {
				oid = "0";
				editBankGrpRestrictRule.setAttribute("BankGrpRestrictRule_oid",
						oid);
				subHeaderTitle = resMgr.getText(
						"BankGrpRestrictRuleDetail.NewBankGrpRestrictRule",
						TradePortalConstants.TEXT_BUNDLE);
			} else {
				subHeaderTitle = editBankGrpRestrictRule.getAttribute("name");
			}
		%>
		<jsp:include page="/common/PageSubHeader.jsp">
			<jsp:param name="title" value="<%=subHeaderTitle%>" />
		</jsp:include>

		<form name="BankGrpRestrictRuleDetailForm"
			id="BankGrpRestrictRuleDetailForm" data-dojo-type="dijit.form.Form"
			method="post" action="<%=formMgr.getSubmitAction(response)%>">


			<input type="hidden" value="" name="buttonName"> <input
				type="hidden" value="" name="saveAsFlag"> <input
				type="hidden" value="<%=StringFunction.xssCharsToHtml(oid)%>"
				name="oid">


			<%
				/*******************
				 * START SECUREPARAMS
				 ********************/
				secureParams.put("oid", oid);
				secureParams.put("owner_oid", userSession.getOwnerOrgOid());
				secureParams
						.put("ownership_level", userSession.getOwnershipLevel());

				// Used for the "Added By" column
				if (userSession.getSecurityType()
						.equals(TradePortalConstants.ADMIN))
					secureParams.put("ownership_type",
							TradePortalConstants.OWNER_TYPE_ADMIN);
				else
					secureParams.put("ownership_type",
							TradePortalConstants.OWNER_TYPE_NON_ADMIN);

				secureParams.put("login_oid", userSession.getUserOid());
				secureParams.put("login_rights", loginRights);
				secureParams.put("opt_lock",
						editBankGrpRestrictRule.getAttribute("opt_lock"));
				secureParams.put("client_bank_oid",userSession.getClientBankOid());
				/*****************
				 * END SECUREPARAMS
				 ******************/
			%>



			<div class="formArea">
				<jsp:include page="/common/ErrorSection.jsp" />

				<div class="formContent">
					<%--  Form Content Start --%>

					<%=widgetFactory.createSectionHeader("1",
					"BankGrpRestrictRuleDetail.General")%>
					
						<%=widgetFactory.createTextField("nameField",
					"BankGrpRestrictRuleDetail.BankGrpRestrictRuleName",
					editBankGrpRestrictRule.getAttribute("name"), "35",
					isReadOnly, true, false, "style='width: 275px'", "", "")%>
						<%=widgetFactory.createTextField("BankGrpRestrictRuleDesc",
					"BankGrpRestrictRuleDetail.BankGrpRestrictRuleDesc",
					editBankGrpRestrictRule
							.getAttribute("description"), "25",
					isReadOnly, true, false, "style='width: 275px'", "", "")%>
					
				</div>
				<%-- General Tab Ends Here--%>


				<%=widgetFactory.createSectionHeader("2",
					"BankGrpRestrictRuleDetail.RestrictBankGrpRls")%>
					<%@ include  file="fragments/BankGrpRestrictRuleDetail-BankGroups.frag"%> 
			</div>
			<%-- Restriction Bank Groups Tab Ends Here--%>
	</div>
	<%--  Form Content End --%>
</div>
<%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar"
	data-dojo-props="form: 'BankGrpRestrictRuleDetailForm'">
	<jsp:include page="/common/RefDataSidebar.jsp">
		<jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
		<jsp:param name="showSaveButton" value="<%=showSave%>" />
		<jsp:param name="saveOnClick" value="none" />
		<jsp:param name="showSaveAsButton" value="<%=showSaveAs%>" />
		<jsp:param name="showDeleteButton" value="<%=showDelete%>" />
		<jsp:param name="showSaveCloseButton" value="<%=showSaveClose%>" />
		<jsp:param name="saveCloseOnClick" value="none" />
		<jsp:param name="showHelpButton" value="true" />
		<jsp:param name="cancelAction" value="BankGrpRestrictRuleCancel" />
		<jsp:param name="helpLink" value="customer/phrase_detail.htm" />
		<jsp:param name="saveAsDialogPageName"
			value="BankGrpRestrictRuleDetailSaveAs.jsp" />
		<jsp:param name="saveAsDialogId" value="saveAsDialogId" />
		<jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
		<jsp:param name="error" value="<%=error%>" />
		<jsp:param name="showLinks" value="true" />
	</jsp:include>
</div>
<%-- Side Bar End--%>

<%=formMgr.getFormInstanceAsInputField(
					"BankGrpRestrictRuleDetailForm", secureParams)%>
</form>

<div></div>
<%--  Page Content End --%>
<div></div>
<%--  Page Main End --%>


<jsp:include page="/common/Footer.jsp">
	<jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
	<jsp:param name="displayFooter"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
	<jsp:param name="includeNavigationBarFlag"
		value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%
	//save behavior defaults to without hsm
	
	//(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
	
	//String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
	//String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>
 


<div id="saveAsDialogId" style="display: none;"></div>

<%
	String focusField = "nameField";
	if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script type="text/javascript">
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%=focusField%>';
			if (focusFieldId) {
				var focusField = registry.byId(focusFieldId);
				if (focusField) {
					focusField.focus();
				}
			}
		});
	});

	function submitSaveAs() {
		if (window.document.saveAsForm.newProfileName.value == "") {
			return false;
		}
		document.BankGrpRestrictRuleDetailForm.nameField.value = window.document.saveAsForm.newProfileName.value;
		document.BankGrpRestrictRuleDetailForm.oid.value = "0";
		document.BankGrpRestrictRuleDetailForm.saveAsFlag.value = "Y";
		document.BankGrpRestrictRuleDetailForm.buttonName.value = "SaveAndClose";
		for(i=0;i<50;i++){
			if(document.getElementsByName('BankGrpForRestrictRuleOid'+i)[0])
			{
				document.getElementsByName('BankGrpForRestrictRuleOid'+i)[0].value='<%=EncryptDecrypt.encryptStringUsingTripleDes( "", userSession.getSecretKey())%>';
			}
		}
		document.BankGrpRestrictRuleDetailForm.submit();
		hideDialog("saveAsDialogId");
	}
	
	function preSaveAndClose(){
  	 	 
 	  document.forms[0].buttonName.value = "SaveAndClose";
 	  return true;

	}
	
	
</script>
<%
	}
%>

<body></body>

<html></html>

<%
	/**********
	 * CLEAN UP
	 **********/

	beanMgr.unregisterBean("BankGrpRestrictRule");

	// Finally, reset the cached document to eliminate carryover of
	// information to the next visit of this page.
	formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
