
<%--for the ajax include--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 
  String parmValue = parmValue = request.getParameter("aliasIndex");
  int aliasIndex = 0;
  if ( parmValue != null ) {
    try {
    	aliasIndex = (new Integer(parmValue)).intValue();
    } 
    catch (Exception ex ) {
      //todo
    }
  }

  String loginLocale = userSession.getUserLocale();
  boolean isReadOnly = false; //request.getParameter("isReadOnly");
  boolean isFromExpress = false; //request.getParameter("isFromExpress");

  //other necessaries
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  String msgCatSql = "select gen_message_category_oid, gmc.description as DESCR from generic_msg_category gmc order by description";
  DocumentHandler msgCategoryList = DatabaseQueryBean.getXmlResultSet(msgCatSql.toString(), false, new ArrayList<Object>());
  
  //when included per ajax, the business objects will be blank
  // we want 4 of them
  List<CustomerAliasWebBean> aliasList = new ArrayList<CustomerAliasWebBean>();
  aliasList.add(beanMgr.createBean(CustomerAliasWebBean.class,"CustomerAlias"));
  aliasList.add(beanMgr.createBean(CustomerAliasWebBean.class,"CustomerAlias"));
  aliasList.add(beanMgr.createBean(CustomerAliasWebBean.class,"CustomerAlias"));
  aliasList.add(beanMgr.createBean(CustomerAliasWebBean.class,"CustomerAlias"));
%>

<%@ include file="fragments/CorporateCustomerAliasRows.frag" %>
