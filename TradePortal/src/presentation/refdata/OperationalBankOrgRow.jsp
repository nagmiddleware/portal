<%--
*******************************************************************************
                                  OperationalBankOrgRow

  Description:
     Create html tags that displays a row in the Operational Bank Org
	 Detail page consisting of the all the prefix and suffix field(s).
	 This jsp only displays 1 row, but it gets called repetitively from 
	 Operational Bank Org Detail.jsp.


  Parameters:
  
 	descriptionValue 		- The text that describes what instrument type being edited
	prefixName			- The Name for the input field of the prefix for the Instrument.
 	prefixValue			- The value of the prefix to be displayed in the Textfield (if any)
	suffixName 			- The Name for the input field of the suffix for the Instrument.
	suffixValue			- The value of the prefix to be displayed in the Textfield (if any) 
	readOnly			- TradePortalConstants.INDICATOR_YES or INDICATOR_NO 
						  indicates if the field is readOnly
*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*,com.amsinc.ecsg.html.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<%

   
   	String descriptionValue 	= StringFunction.xssCharsToHtml(request.getParameter("descriptionValue"));
   	String prefixName 		= StringFunction.xssCharsToHtml(request.getParameter("prefixName"));
   	String prefixValue 		= StringFunction.xssCharsToHtml(request.getParameter("prefixValue")); 	   
	String suffixName		= StringFunction.xssCharsToHtml(request.getParameter("suffixName"));
	String suffixValue		= StringFunction.xssCharsToHtml(request.getParameter("suffixValue")); 
	boolean readOnly		= request.getParameter("readOnly").equals(
								"true")? true:false;
	 //Validation to check Cross Site Scripting
	  if(descriptionValue != null){
		  descriptionValue = StringFunction.xssCharsToHtml(descriptionValue);
	  }
	  if(prefixName != null){
		  prefixName = StringFunction.xssCharsToHtml(prefixName);
	  }
	  if(prefixValue != null){
		  prefixValue = StringFunction.xssCharsToHtml(prefixValue);
	  }

   // Print out a row for the OperationalBankOrgDetail: Description text, Prefix Textbox, Suffix Textbox
%>


      <td>&nbsp;</td>
      <td nowrap> 
        <p class="ListText">
		<%= resMgr.getText( descriptionValue, TradePortalConstants.TEXT_BUNDLE) %>
	</p>
      </td>
      <td width="15" nowrap>&nbsp;</td>
      <td> 
                <%= InputField.createTextField( prefixName, prefixValue, "6", "6","ListText", readOnly)%>
      </td>
      <td>&nbsp;</td>
      <td>
                <%= InputField.createTextField( suffixName, suffixValue, "6", "6", "ListText", readOnly)%>
      </td>
      <td>&nbsp;</td>
