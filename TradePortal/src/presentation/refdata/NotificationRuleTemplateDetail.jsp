
<%--
*******************************************************************************
                       Notification Rule Template Detail Page
  
  Description: This jsp sets up the data being used for the Notification Rule Template
  The idea is that this jsp will call the jsps that create the 4 tabs associated
  with the Notification Rules.  This jsp will set up the data that is used in 
  the called jsp(s).  The only way a user can get to this page is via Refdata 
  home. 
*******************************************************************************
--%>

<%--
*
*     Copyright   2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.common.InstrumentType,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  Debug.debug("*********************** START: Notification Rule Template Detail Page **************************"); 

  String          notificationRuleOid     = request.getParameter("oid");
  String notificationTemplateId		  = request.getParameter("notificationTemplateId");
  String notifTemplateOid = "";
  System.out.println("notificationTemplateId:"+notificationTemplateId);
  String                ownershipLevel          = ""; 
  String                emailFrequency          = "";
  String                emailForMsg             = "";
  String                emailForDisc            = "";
  String                templateInd            = "";
  String          emailForRecPayMatch     = "";
  String          emailForSettlementInstructionReq     = ""; 
  String          loginLocale             = userSession.getUserLocale();
  String          loginRights             = userSession.getSecurityRights();
  String                userOwnerLevel          = userSession.getOwnershipLevel(); 
  boolean         hasEditRights           = SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_NOTIF_RULE_TEMPLATES );
  DocumentHandler defaultDoc        = formMgr.getFromDocCache();
  boolean         isReadOnly        = true;
  String 		  isTemplate		= TradePortalConstants.INDICATOR_YES;
  boolean               getDataFromDoc          = false;
  boolean showSave = true;
  boolean showDelete = true;
  boolean showSaveClose = true;
  boolean showSaveAs = true;
  Hashtable secureParms = new Hashtable();
  Hashtable criterionRuleList = new Hashtable();
  Hashtable criterionRuleTransList = new Hashtable();
  Hashtable userIdList = new Hashtable();
  String defApplyToAllGrp ="";
  String defClearToAllGrp ="";
  ArrayList  dataList = new ArrayList();
  final String[] sectionHeaderLableArray = {"AirwaybillSection","ApprovaltoPaySection","BillingSection",
		"CleanAdvanceSection","DirectSendCollectionSection","ExportBankersAcceptanceSection",
		"ExportCollectionSection","ExportDeferredPaymentSection","ExportDocumentaryLCSection",
		"ExportIndemnitySection","ExportTradeAcceptanceSection","ImportBankersAcceptanceSection",
		"ImportCollectionSection","ImportDeferredPaymentSection","ImportDocumentaryLCSection",
		"ImportTradeAcceptanceSection","IncomingGuaranteeSection","IncomingStandbyLCSection",
		"InternationalPaymentSection","LoanRequestSection","OutgoingGuaranteeSection",
		"OutgoingStandbyLCSection","PayablesManagementSection","PaymentSection",
		"ReceivablesFinanceSection","ReceivablesManagementSection","RefinanceSection",
		"RequesttoAdviseSection","ShippingGuaranteeSection","SupplierPortalSection",
		"TransferBetweenAccountsSection","MailMessagesSection","SupplierInvoicesSection",
		"HosttoHostInvoiceSection"};
		
  final String[] sectionDialogTitleArray = {
	resMgr.getTextEscapedJS("NotificationRuleDetail.AirwaybillSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
  	resMgr.getTextEscapedJS("NotificationRuleDetail.ApprovaltoPaySectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.BillingSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.CleanAdvanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.DirectSendCollectionSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ExportBankersAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ExportCollectionSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ExportDeferredPaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ExportDocumentaryLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ExportIndemnitySectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ExportTradeAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ImportBankersAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ImportCollectionSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ImportDeferredPaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ImportDocumentaryLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ImportTradeAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.IncomingGuaranteeSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.IncomingStandbyLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.InternationalPaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.LoanRequestSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.OutgoingGuaranteeSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.OutgoingStandbyLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.PayablesManagementSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.PaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ReceivablesFinanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ReceivablesManagementSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.RefinanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.RequesttoAdviseSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.ShippingGuaranteeSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.SupplierPortalDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.TransferBetweenAccountsSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.MailMessagesDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.SupplierPortalInvoicesDialogTitle",TradePortalConstants.TEXT_BUNDLE),
	resMgr.getTextEscapedJS("NotificationRuleDetail.HosttoHostInvoiceDialogTitle",TradePortalConstants.TEXT_BUNDLE)};
	
  final String[] sectionNoteLableArray = {
		"AirwaybillSectionNote","ApprovaltoPaySectionNote","BillingSectionNote",
		"CleanAdvanceSectionNote","DirectSendCollectionSectionNote","ExportBankersAcceptanceSectionNote",
		"ExportCollectionSectionNote","ExportDeferredPaymentSectionNote","ExportDocumentaryLCSectionNote",
		"ExportIndemnitySectionNote","ExportTradeAcceptanceSectionNote","ImportBankersAcceptanceSectionNote",
		"ImportCollectionSectionNote","ImportDeferredPaymentSectionNote","ImportDocumentaryLCSectionNote",
		"ImportTradeAcceptanceSectionNote","IncomingGuaranteeSectionNote","IncomingStandbyLCSectionNote",
		"InternationalPaymentSectionNote","LoanRequestSectionNote","OutgoingGuaranteeSectionNote",
		"OutgoingStandbyLCSectionNote","PayablesManagementSectionNote","PaymentSectionNote",
		"ReceivablesFinanceSectionNote","ReceivablesManagementSectionNote","RefinanceSectionNote",
		"RequesttoAdviseSectionNote","ShippingGuaranteeSectionNote","SupplierPortalNote",
		"TransferBetweenAccountsSectionNote","MailMessagesNote","SupplierPortalInvoicesNote1",
		"HosttoHostInvoiceNote1"};
  

  final String[] instrumentCatagory = {
		InstrumentType.AIR_WAYBILL, InstrumentType.APPROVAL_TO_PAY, InstrumentType.BILLING,
		InstrumentType.CLEAN_BA, InstrumentType.NEW_EXPORT_COL, InstrumentType.EXPORT_BANKER_ACCEPTANCE, 
		InstrumentType.EXPORT_COL, InstrumentType.EXPORT_DEFERRED_PAYMENT, InstrumentType.EXPORT_DLC, 
		InstrumentType.INDEMNITY, InstrumentType.EXPORT_TRADE_ACCEPTANCE, InstrumentType.IMPORT_BANKER_ACCEPTANCE,
		InstrumentType.IMPORT_COL, InstrumentType.IMPORT_DEFERRED_PAYMENT, InstrumentType.IMPORT_DLC, 
		InstrumentType.IMPORT_TRADE_ACCEPTANCE, InstrumentType.INCOMING_GUA, InstrumentType.INCOMING_SLC,
		InstrumentType.FUNDS_XFER, InstrumentType.LOAN_RQST, InstrumentType.GUARANTEE, 
		InstrumentType.STANDBY_LC, InstrumentType.PAYABLES_MGMT, InstrumentType.DOMESTIC_PMT, 
		InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT, InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, InstrumentType.REFINANCE_BA, 
		InstrumentType.REQUEST_ADVISE, InstrumentType.SHIP_GUAR, InstrumentType.SUPPLIER_PORTAL,
		InstrumentType.XFER_BET_ACCTS, InstrumentType.MESSAGES, InstrumentType.SUPP_PORT_INST_TYPE, 
		InstrumentType.HTWOH_INV_INST_TYPE};
  
  final String [] AIR_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "PAY", "REA", "REL"};
  final String [] ATP_TransArray = {"ARU", "ADJ", "AMD", "CHG", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA"};//1 EXT
  final String [] BIL_TransArray = {"ARU", "ADJ", "BCH", "BCA", "PCS", "PCE", "DEA", "BNW","REA", "RED", "RVV"};
  final String [] CBA_TransArray = {"ARU", "ADJ", "BUY", "CHG", "PCS", "PCE", "CUS", "LIQ"};//3 BUY PCS PCE
  
  final String [] EXP_OCO_TransArray = {"ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"};
  final String [] EXP_DBA_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
  final String [] EXP_COL_TransArray = {"ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"};
  final String [] EXP_DFP_TransArray = { "ARU", "ADJ", "BUY",  "CHG", "CUS", "LIQ"};
  
  final String [] EXP_DLC_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
  final String [] LOI_TransArray = {"ARU", "ADJ", "AMD", "CHG", "CRE", "DEA", "EXP", "PAY", "REA"};//2 DEA EXT
  final String [] EXP_TAC_TransArray = { "ARU", "ADJ",  "BUY",  "CHG", "CUS", "LIQ"};
  final String [] IMP_DBA_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"}; 
  
  final String [] IMC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "COL", "NAC", "PAY", "REA"};//DEA
  final String [] IMP_DFP_TransArray = {"ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
  final String [] IMP_DLC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
  final String [] IMP_TAC_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
  
  final String [] INC_GUA_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
  final String [] INC_SLC_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
  final String [] FTRQ_TransArray = {"ARU", "ADJ", "ISS"};
  final String [] LRQ_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"};
  
  final String [] GUA_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
  final String [] SLC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
  final String [] PYB_TransArray = {"CHP", "APM", "PPY"};
  final String [] FTDP_TransArray = {"ARU", "ADJ", "ISS"};
  
  final String [] RFN_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"};
  final String [] REC_TransArray = {"PAR", "CHM", "ARM"};
  final String [] RBA_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "CUS", "LIQ"};
  final String [] RQA_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV", "TRN", "PYT"};
  
  final String [] SHP_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "ISS", "PAY", "REA"};
  final String [] SUPPLIER_PORTAL_TransArray = {"ISS", "LIQ"};
  final String [] FTB_ATransArray = {"ARU", "ADJ", "ISS"};
  final String [] MESSAGES_TransArray = {"DISCR", "MAIL", "MATCH", "SETTLEMENT"};
  
  final String [] LRQ_INV_TransArray = {"LRQ_INV"};
  final String [] HTOH_TransArray = {"PIN", "PCN", "RIN"};

  final String [][] instrTransArrays = {
		  	 AIR_TransArray, ATP_TransArray, BIL_TransArray, 
		  	 CBA_TransArray, EXP_OCO_TransArray, EXP_DBA_TransArray, 
		  	 EXP_COL_TransArray, EXP_DFP_TransArray, EXP_DLC_TransArray, 
		  	 LOI_TransArray, EXP_TAC_TransArray, IMP_DBA_TransArray, 
             IMC_TransArray, IMP_DFP_TransArray, IMP_DLC_TransArray, 
             IMP_TAC_TransArray, INC_GUA_TransArray, INC_SLC_TransArray, 
             FTRQ_TransArray, LRQ_TransArray, GUA_TransArray, 
             SLC_TransArray, PYB_TransArray, FTDP_TransArray, 
             RFN_TransArray, REC_TransArray, RBA_TransArray, 
             RQA_TransArray, SHP_TransArray, SUPPLIER_PORTAL_TransArray, 
             FTB_ATransArray, MESSAGES_TransArray, LRQ_INV_TransArray, 
             HTOH_TransArray};
  
	int counter = 0;
	int [] instrTransCriteriaFieldStartCount = new int [instrumentCatagory.length];
	StringBuilder instrTransCriteriaFieldStartCountStr = new StringBuilder();
	String [] tempTransArray;
	
	for(int i=0;i<instrumentCatagory.length;i++){
		tempTransArray = instrTransArrays[i];
		instrTransCriteriaFieldStartCount[i] = counter;
		
		instrTransCriteriaFieldStartCountStr.append("").append(counter);
		
		for(int t=0;t<tempTransArray.length;t++){
			counter++;
		}
		
		if(i<instrumentCatagory.length-1){
			instrTransCriteriaFieldStartCountStr.append(",");
		}
	}
	
  //final int [] instrTransCriteriaFieldStartCount = {0, 11, 21, 27, 38, 43, 49, 55, 63, 71, 90, 99, 107, 122, 141, 160, 163, 170, 185, 200, 203, 206, 213, 216, 223, 242, 253, 255, 258, 262, 263};
  
  int DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
  int yy=-1;
	int xx=0;
  String          refDataHome       = "goToRefDataHome";
  String          oid;
  String          uOid;
  String          userListOid;
  String          setting;
  String          transactionType;
  String          instrumentType;
  String          sendNotifSetting;
  String          sendEmailSetting; 
  String          addEmailAddress;
  String 		  notifEmailFerquency;
  String 		  notifyEmailRow = "emailDefault";
  String 		  notifyEmail = "defaultEmail";
  String[] userIDArray = new String[20];
  String[] transArray = {};
  int notifEmailIndex = 0;
	int rowIndex = 0; 
  int numItems          = 0; 
  int transItems = 0;
  int criterionRuleCount = 0;
  int iLoop;
  Vector emailVector;
  Vector transVector;
  Vector instrVector;
  Vector defListVector = new Vector();;
  String links ="";
  String buttonPressed ="";
  buttonPressed = defaultDoc.getAttribute("/In/Update/ButtonPressed");
  
  System.out.println("buttonPressed:"+defaultDoc.getFragments("In/Notifcations/One"));
      Vector error = null;
      error = defaultDoc.getFragments("/Error/errorlist/error");
      
		NotificationRuleTemplateWebBean notificationRule = beanMgr.createBean(NotificationRuleTemplateWebBean.class, "NotificationRuleTemplate");
      	
		if(hasEditRights){                 
      		isReadOnly = false;
      	}
      	
      	boolean rawOidValue = false; 
      	
      	if (StringFunction.isBlank(notificationRuleOid)) {
        	notificationRuleOid = defaultDoc.getAttribute("/Out/NotificationRuleTemplate/notification_rule_oid");
            
        	if(notificationRuleOid==null){
              	notificationRuleOid = defaultDoc.getAttribute("/In/NotificationRuleTemplate/notification_rule_oid");
        	}
            if (!StringFunction.isBlank(notificationRuleOid)&& !"0".equals(notificationRuleOid)){
                rawOidValue = true;
            }
        }
	    if(StringFunction.isNotBlank(notificationTemplateId)){
			notifTemplateOid = EncryptDecrypt.decryptStringUsingTripleDes(notificationTemplateId, userSession.getSecretKey());
		}
		if(StringFunction.isNotBlank(notifTemplateOid)){
			   notificationRuleOid = notificationTemplateId;
		 }
		System.out.println("notifTemplateOid :"+notifTemplateOid+"\t notificationRuleOid:"+notificationRuleOid);
      	if (StringFunction.isNotBlank(notificationRuleOid)) {
          	if (!rawOidValue && !"0".equals(notificationRuleOid)){   
            	notificationRuleOid = EncryptDecrypt.decryptStringUsingTripleDes(notificationRuleOid, userSession.getSecretKey());
          	}
              System.out.println("notificationRuleOid():"+notificationRuleOid);
          	notificationRule.setAttribute("notification_rule_oid", notificationRuleOid);
            notificationRule.getDataFromAppServer();
          
            // Make notification rule Template read only if this user is not the same ownership level
          	// as the notification rule
            ownershipLevel = notificationRule.getAttribute("ownership_level");
            
            if (!StringFunction.isBlank(ownershipLevel) && !userOwnerLevel.equals(ownershipLevel)){
              	isReadOnly = true;   
            }
        } else {
         	notificationRuleOid = "0";
        }
      	
      	if (isReadOnly){
            showSave = false;
            showDelete = false;
            showSaveClose=false;
            showSaveAs = false;
        }
      	
        if(notificationRuleOid.equals("0")){
            showDelete = false;
            showSaveAs = false;
        }
        System.out.println("defaultDoc:"+defaultDoc);
      	//Now check to see if we came back with errors - other wise we need to display the list.
        String maxError = defaultDoc.getAttribute("/Error/maxerrorseverity");

        if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0) {
          	getDataFromDoc = true;
          
          	//Disable Delet and SaveAs Buttons by Prateep
          	showDelete = false;
          	showSaveAs = false;
          	notificationRule.populateFromXmlDoc(defaultDoc.getComponent("/In"));
          	notificationRuleOid = notificationRule.getAttribute("notification_rule_oid");

          	if (StringFunction.isBlank(notificationRuleOid)) {
            	notificationRuleOid = "0"; 
          	} 
        }
        
     	// Get notification rule attributes used by other tab pages
        emailFrequency =  StringFunction.xssCharsToHtml(notificationRule.getAttribute("email_frequency"));
        if (StringFunction.isBlank(emailFrequency)){
            emailFrequency = TradePortalConstants.NOTIF_RULE_TRANSACTION;
        }
        
     	emailForMsg    =  StringFunction.xssCharsToHtml( notificationRule.getAttribute("email_for_message"));
        if (StringFunction.isBlank(emailForMsg)){
            emailForMsg = TradePortalConstants.NOTIF_RULE_NONE;
        }

        templateInd   =  StringFunction.xssCharsToHtml(notificationRule.getAttribute("template_ind"));
        if (StringFunction.isBlank(templateInd)){
        	isTemplate = TradePortalConstants.INDICATOR_YES;
        }
        
        if (getDataFromDoc){
            // Populate notifyRuleCriterion[] bean with info from document handler
            DocumentHandler doc2 = defaultDoc.getComponent("/In");
            instrVector = doc2.getFragments("/NotificationRuleTemplate/NotificationRuleCriterionList");
			if(instrVector !=null && instrVector.size()>0){
	            numItems = instrVector.size();
			}
            int transCriteriaStartCount = 0;
            defApplyToAllGrp = doc2.getAttribute("/NotificationRuleTemplate/default_apply_to_all_grp");
			 defClearToAllGrp = doc2.getAttribute("/NotificationRuleTemplate/default_clear_to_all_grp");
            NotificationRuleCriterionWebBean notifyRuleInstrTransCriterion[] = new NotificationRuleCriterionWebBean[numItems];
            if(numItems > 0){
 	           // for (iLoop=0; iLoop<dyanInstrCatArray.length; iLoop++){
 	            	//transCriteriaStartCount = instrTransCriteriaFieldStartCount[iLoop];
 	            	//transArray = instrTransArrays.get(dyanInstrCatArray[iLoop]);
 	            	for(int tLoop = 0; tLoop < numItems; tLoop++){
 						
 	            		notifyRuleInstrTransCriterion[tLoop] = beanMgr.createBean(NotificationRuleCriterionWebBean.class, "NotificationRuleCriterion");
 	
 	                  	DocumentHandler notifyRuleInstrTransCriterionDoc = (DocumentHandler) instrVector.elementAt(transCriteriaStartCount);
 	                  
 	                  	try { instrumentType = notifyRuleInstrTransCriterionDoc.getAttribute("/instrument_type_or_category"); }
 	    	            catch (Exception e) { instrumentType = ""; }
 	            		notifyRuleInstrTransCriterion[tLoop].setAttribute("instrument_type_or_category", instrumentType);
 	            		
 	            		try { oid = notifyRuleInstrTransCriterionDoc.getAttribute("/criterion_oid"); }
 	          			catch (Exception e) { oid = ""; }
 	            		notifyRuleInstrTransCriterion[tLoop].setAttribute("criterion_oid", oid);
 	          		
 	                  	try { transactionType = notifyRuleInstrTransCriterionDoc.getAttribute("/transaction_type_or_action"); }
 	    	            catch (Exception e) { transactionType = ""; }
 	                  	notifyRuleInstrTransCriterion[tLoop].setAttribute("transaction_type_or_action", transactionType);
 	
 	    	            try { sendNotifSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/send_notif_setting"); }
 	    	            catch (Exception e) { sendNotifSetting = ""; }
 	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_notif_setting", sendNotifSetting);
 	    	            
 	    	            try { sendEmailSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/send_email_setting"); }
 	    	            catch (Exception e) { sendEmailSetting = ""; }
 	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_email_setting", sendEmailSetting);
 	    	            
 	    	            try { addEmailAddress = notifyRuleInstrTransCriterionDoc.getAttribute("/additional_email_addr"); }
 	    	            catch (Exception e) { addEmailAddress = ""; }
 	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("additional_email_addr", addEmailAddress);
 	    	            
 	    	            try { notifEmailFerquency = notifyRuleInstrTransCriterionDoc.getAttribute("/notify_email_freq"); }
 	    	            catch (Exception e) { notifEmailFerquency = ""; }
 	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("notify_email_freq", notifEmailFerquency);
 	    	            
 	                    // The hashtable key consists of the instrumentCategory (IMP,EXP,etc) plus the 
 	                    // transactionType (ISS,AMD,etc) and criterionType (A,C,etc)
 	                    String htKey = "";
 	                    htKey = "";
 	                    htKey = instrumentType+"_"+transactionType;
 	                    //htKey = instrumentType+"_"+transactionType;
 	
 	                    criterionRuleTransList.put(htKey, notifyRuleInstrTransCriterion[tLoop]);
 	            		transCriteriaStartCount++;
 						}
 	            	
 	            //}
         	}//end of if(numItems > 0)
        } else
        if (notificationRuleOid != null){  
        	// Populate from the database
            String sql = new String();
        	QueryListView userEmailqueryListView = null;
        	QueryListView transQueryListView = null;
        	QueryListView mailQueryListView = null;
        	QueryListView supplierQueryListView = null;
        	QueryListView htohQueryListView = null;
        	QueryListView queryListView = null;
        	
        	String htKey = "";
			
			Vector vVector = null;
        	DocumentHandler transCriterionList = new DocumentHandler();
        	DocumentHandler mailCriterionList = new DocumentHandler();
        	DocumentHandler supplierCriterionList = new DocumentHandler();
        	DocumentHandler htohCriterionList = new DocumentHandler();
                  
        	queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
        	sql = "select criterion_oid, instrument_type_or_category from notification_rule_criterion where p_notify_rule_oid = ?";
        	Debug.debug(sql);
              
        	queryListView.setSQL(sql,new Object[]{notificationRuleOid});
        	queryListView.getRecords();
              
        	DocumentHandler criterionList = new DocumentHandler();
        	criterionList = queryListView.getXmlResultSet();
        	Debug.debug(criterionList.toString());
        
        	instrVector = criterionList.getFragments("/ResultSetRecord");
        	numItems = instrVector.size();

        	NotificationRuleCriterionWebBean NotificationRuleCriterion[] = new NotificationRuleCriterionWebBean[numItems]; 
          	try {
            	for (iLoop=0;iLoop<instrumentCatagory.length;iLoop++) {
                  	transQueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
                	sql = "select instrument_type_or_category, criterion_oid, transaction_type_or_action, send_notif_setting, "
                			+ " send_email_setting, p_notify_rule_oid, additional_email_addr, notify_email_freq"
                			+ " from notification_rule_criterion where p_notify_rule_oid = ? and instrument_type_or_category = ?";
                      
                	transQueryListView.setSQL(sql,new Object[]{notificationRuleOid, instrumentCatagory[iLoop]});
                	transQueryListView.getRecords();
                      
                	transCriterionList = transQueryListView.getXmlResultSet();
                	Debug.debug(transCriterionList.toString());
                
                	transVector = transCriterionList.getFragments("/ResultSetRecord");
                	transItems = transVector.size();

                	NotificationRuleCriterionWebBean notifyRuleInstrTransCriterion[] = new NotificationRuleCriterionWebBean[transItems];

                	for (int tLoop=0;tLoop<transItems;tLoop++) {
                  		notifyRuleInstrTransCriterion[tLoop] = beanMgr.createBean(NotificationRuleCriterionWebBean.class, "NotificationRuleCriterion");
                  		DocumentHandler notifyRuleInstrTransCriterionDoc = (DocumentHandler) transVector.elementAt(tLoop);
                      
                  		try { instrumentType = notifyRuleInstrTransCriterionDoc.getAttribute("/INSTRUMENT_TYPE_OR_CATEGORY"); }
    		            catch (Exception e) { instrumentType = ""; }
                  		notifyRuleInstrTransCriterion[tLoop].setAttribute("instrument_type_or_category", instrumentType);
                  		
                  		try { oid = notifyRuleInstrTransCriterionDoc.getAttribute("/CRITERION_OID"); }
                		catch (Exception e) { oid = ""; }
						if(TradePortalConstants.INDICATOR_NO.equals(isTemplate)){
                  		notifyRuleInstrTransCriterion[tLoop].setAttribute("criterion_oid", "");
						}else{
							notifyRuleInstrTransCriterion[tLoop].setAttribute("criterion_oid", oid);
						}
                		if(!"0".equals(oid) && !dataList.contains(instrumentType)){
							dataList.add(instrumentType);
						}
                        try { transactionType = notifyRuleInstrTransCriterionDoc.getAttribute("/TRANSACTION_TYPE_OR_ACTION"); }
    		            catch (Exception e) { transactionType = ""; }
                        notifyRuleInstrTransCriterion[tLoop].setAttribute("transaction_type_or_action", transactionType);

    		            try { sendNotifSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/SEND_NOTIF_SETTING"); }
    		            catch (Exception e) { sendNotifSetting = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_notif_setting", sendNotifSetting);
    		            
    		            try { sendEmailSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/SEND_EMAIL_SETTING"); }
    		            catch (Exception e) { sendEmailSetting = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_email_setting", sendEmailSetting);
    		            
    		            try { addEmailAddress = notifyRuleInstrTransCriterionDoc.getAttribute("/ADDITIONAL_EMAIL_ADDR"); }
    		            catch (Exception e) { addEmailAddress = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("additional_email_addr", addEmailAddress);
    		            
    		            try { notifEmailFerquency = notifyRuleInstrTransCriterionDoc.getAttribute("/NOTIFY_EMAIL_FREQ"); }
    		            catch (Exception e) { notifEmailFerquency = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("notify_email_freq", notifEmailFerquency);
                  
                  		// The hashtable key consists of the instrumentCategory (IMP,EXP,etc) plus the 
                  		// transactionType (ISS,AMD,etc) and criterionType (A,C,etc)
                      	htKey = "";
                      	htKey = instrumentCatagory[iLoop]+"_"+transactionType;

                      	criterionRuleTransList.put(htKey, notifyRuleInstrTransCriterion[tLoop]);
            		}//tLoop
            	}
          	} catch (Exception e) { 
        	  	e.printStackTrace(); 
        	} finally {
          		try {
            		if (transQueryListView != null) {
            			transQueryListView.remove();
            		} 
            	} catch (Exception e) { 
            		System.out.println("Error removing queryListView in NotificationRuleDetail.jsp"); 
            	}
          	}       
        }
        
        String notifyRuleUsersSql = "select USER_OID, EMAIL_ADDR, LOGIN_ID from users where activation_status=? and p_owner_org_oid = ? and email_addr is not null";
        DocumentHandler notifyRuleUsersList = DatabaseQueryBean.getXmlResultSet(notifyRuleUsersSql, false, new Object[]{TradePortalConstants.ACTIVE, userSession.getOwnerOrgOid()});
     	System.out.println("notifyRuleUsersList;"+notifyRuleUsersList);
     	String userEmailsOptions = "<option  value=\"\"></option>";
     	userEmailsOptions += ListBox.createOptionList(notifyRuleUsersList, "USER_OID", "LOGIN_ID", "", null);
	    if(StringFunction.isNotBlank(notifTemplateOid)){     	
		secureParms.put("oid","0");
		}else{
			secureParms.put("oid",notificationRuleOid);
		}
	
		secureParms.put("owner_oid",userSession.getOwnerOrgOid());
		secureParms.put("ownership_level",userSession.getOwnershipLevel());
		secureParms.put("login_oid", userSession.getUserOid());
		secureParms.put("login_rights", userSession.getSecurityRights());

		// Used for the "Added By" column
		if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
		  	secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
		else
		  	secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
		
		secureParms.put("login_rights",loginRights);
		secureParms.put("opt_lock", notificationRule.getAttribute("opt_lock"));
%>


<%-- ============================== HTML Starts HERE =================================== --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="RefDataHome.NotificationRuleTemplate"/>
      <jsp:param name="helpUrl" value="admin/notification_rule_template.htm"/>
    </jsp:include>

<%
  String subHeaderTitle = "";
  if( StringFunction.isBlank(notificationRuleOid) || notificationRuleOid.equals("0")) {
    subHeaderTitle = resMgr.getText("NotificationRuleDetail.NewNotificationRuleTemplate",TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = notificationRule.getAttribute("name");
  } 
  
  isReadOnly = false;
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      


<%-- ============================== Display tab specific info ================================= --%>
<form name="NotificationRuleTemplateForm" id="NotificationRuleTemplateForm" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
<input type=hidden value="" name=buttonName id=buttonName>
<input type=hidden value="<%=userSession.getOwnerOrgOid() %>" name=owner_oid>
<input type=hidden name=template_ind id=template_ind value="<%=isTemplate%>">
<input type=hidden name=source_template_notif_oid id=source_template_notif_oid value="<%=notifTemplateOid%>">
	<div class="formArea">
		<jsp:include page="/common/ErrorSection.jsp" />
		<div class="formContent">
			<%= widgetFactory.createSectionHeader("1", "NotificationRuleDetail.EmailAlertsDefaults", null, true) %>
			<div class="columnLeftNoBorder">
			<%= widgetFactory.createTextField( "name", "NotificationRuleDetail.TemplateName", 
				notificationRule.getAttribute("name"), "35", isReadOnly, true, false, "", "", "" ) %>
			</div>
			<div class="columnRight">		
			<%= widgetFactory.createTextField( "description", "NotificationRuleDetail.TemplateDescription", 
				notificationRule.getAttribute("description"), "25", isReadOnly, true, false, "class='char35'", "", "" ) %>
			</div>
			<div class="clear:both"></div>
	
			<%        
			
			secureParms.put("name", notificationRule.getAttribute("name"));	 
			%>
			&nbsp;&nbsp;&nbsp;<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailAlertMessage") %>
			<br>
			
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_TRANSACTION" ,"NotificationRuleDetail.WithReceipt",
					TradePortalConstants.NOTIF_RULE_TRANSACTION,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_TRANSACTION), isReadOnly, "", "")%>                          
			<br>
			
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_DAILY" ,"NotificationRuleDetail.Daily",
				TradePortalConstants.NOTIF_RULE_DAILY,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY), isReadOnly, "", "")%>                         
			<br>
			
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_BOTH" ,"NotificationRuleDetail.Both",
				TradePortalConstants.NOTIF_RULE_BOTH,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_BOTH), isReadOnly, "", "")%>                         
			<br>
			  
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_NOEMAIL" ,"NotificationRuleDetail.NoEmail",
				TradePortalConstants.NOTIF_RULE_NOEMAIL,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL), isReadOnly, "", "")%>
			<br>
			
			<input type="hidden" name="notificationRuleOid" id="notificationRuleOid" value="<%=StringFunction.xssCharsToHtml(notificationRuleOid)%>">
			<input type="hidden" value="<%=isReadOnly%>" name="isReadOnly" id="isReadOnly">
			<input type="hidden" name="defaultApplyToAllGroup" id="defaultApplyToAllGroup" value="">
            <input type="hidden" name="defaultClearToAllGroup" id="defaultClearToAllGroup" value="">
			
			<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.DefaultSectionNote") %>	
			<table>
				<tr>
					<td width="20%">
						<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendPortalNotificatios") %>
					</td>
					<td width="18%">
						<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
					</td>
					<td width="15%">
					
					</td>
					<td width="37%">
					
					</td>
				</tr>
				<tr>
					<td width="20%" style="vertical-align: top;">
						<%=widgetFactory.createRadioButtonField("defaultNotifySetting","defaultNotifySettingAlways",
								"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultNotifySetting","defaultNotifySettingNone",
								"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultNotifySetting","defaultNotifySettingChrgsDocsOnly",
								"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
					</td>
					
					<td width="18%" style="vertical-align: top;">
						<%=widgetFactory.createRadioButtonField("defaultEmailSetting","defaultEmailSettingAlways",
								"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultEmailSetting","defaultEmailSettingNone",
								"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultEmailSetting","defaultEmailSettingChrgsDocsOnly",
								"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
					</td>
					<td width="15%" style="vertical-align: top;">
					</td>
					<td width="37%" align="left" style="vertical-align: top;">
						<button data-dojo-type="dijit.form.Button"  name="ApplytoAllGroupsDefault" id="ApplytoAllGroupsDefault" type="button">
							<%=resMgr.getText("NotificationRuleDetail.ApplytoAllGroups.Button",TradePortalConstants.TEXT_BUNDLE)%>
							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								local.applyToAllGroups();
							</script>
						</button>
						<br>
						<button data-dojo-type="dijit.form.Button"  name="ClearAllDefault" id="ClearAllDefault" type="button">
							<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								local.clearAllDefaultSection();
							</script>
						</button>
					</td>
				</tr>
			</table>
			</div><%-- End div for Default Section Header --%>
      		<%@ include file="fragments/NotificationRuleTemplateDetail-AllInstrumentsSection.frag" %>
      		<%@ include file="fragments/NotificationRuleTemplateDetail-MailMessagesSection.frag" %>
      		<%@ include file="fragments/NotificationRuleTemplateDetail-SupplierPortalInvoicesSection.frag" %>
      		<%@ include file="fragments/NotificationRuleTemplateDetail-CreditNoteApprSection.frag" %>
      		<div id='notificationRuleDetailDialogId'></div>
		</div><%--formContent--%>
	</div><%--formArea--%>  

<%= formMgr.getFormInstanceAsInputField("NotificationRuleTemplateForm", secureParms) %>

<%
  defaultDoc.removeAllChildren("/Error");
  formMgr.storeInDocCache("default.doc", defaultDoc);
%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'NotificationRuleTemplateForm'">
        <jsp:include page="/common/RefDataSidebar.jsp">
           <jsp:param name="showSaveButton" value="<%= showSave %>" />
           <jsp:param name="saveCloseOnClick" value="none" />
           <jsp:param name="saveOnClick" value="none" />
             <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
             <jsp:param name="showDeleteButton" value="<%=showDelete %>" />   
           <jsp:param name="showHelpButton"   value="true" />
           <jsp:param name="showLinks"         value="true"/>
             <jsp:param name="links"         value="<%=links%>"/>
           <jsp:param name="cancelAction"     value="goToRefDataHome" />
           <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
           <jsp:param name="error" value="<%= error%>" /> 
        </jsp:include>
</div>

</form>
</div>
</div>
<div id="saveAsDialogId" style="display: none;"></div>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
 <jsp:include page="/common/RefDataSidebarFooter.jsp" /> 
<script type="text/javascript">
	var local = {};
	function preSaveAndClose(){
	 	  document.forms[0].buttonName.value = "SaveAndClose";
	 	  return true;
	}
</script>
	<script type="text/javascript" src="/portal/js/page/notifyRuleTemplateDetail.js"></script>
</body>
</html>