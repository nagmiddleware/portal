<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
   
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("******************");
   //ctq portal refresh comment out
   //userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");

   /****************************************************************
    * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A payment template group
    * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
    * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
    * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
    * TO THE FORM MANAGER.
    ****************************************************************/

   /**********
   * GLOBALS *
   **********/
   boolean isNew = false; //tells if this is a new payment template group
   boolean isReadOnly = false; //tell if the page is read only
   boolean getDataFromDoc = false;
   boolean showSave = true;
   boolean showDelete = true;
   boolean showSaveButton=true;
   boolean showSaveClose=true;
   boolean showCloseButton=true;
   boolean showDeleteButton=true;
   String buttonPressed ="";
   String oid = null;
   String loginRights;

   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.PaymentTemplateGroupWebBean", "PaymentTemplateGroup");
   PaymentTemplateGroupWebBean editPaymentTemplateGroup = (PaymentTemplateGroupWebBean)beanMgr.getBean("PaymentTemplateGroup");

   Hashtable secureParams   = new Hashtable();

   DocumentHandler doc      = null;

   //Get security rights
   loginRights = userSession.getSecurityRights();
   isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PAYMENT_TEMPLATE_GRP);
   Debug.debug("MAINTAIN_PAYMENT_TEMPLATE_GRP, isReadOnly == " + SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PAYMENT_TEMPLATE_GRP) +", " + isReadOnly);

   /*
    * THE FOLLOWING CODE WAS TAKEN FROM MARK WEITZ'S USER DETAIL PAGE
    */

   // Get the document from the cache.  We'll use it to repopulate the screen if the
   // user was entering data with errors.
   doc = formMgr.getFromDocCache();
   buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
  	Vector error = null;
  	error = doc.getFragments("/Error/errorlist/error");

   Debug.debug("doc from cache is " + doc.toString());
   
   // Logic has been changed to fix opt lock issue and duplicate error while update. TP Refresh - 12-04-2012
   if (doc.getDocumentNode("/In/PaymentTemplateGroup") != null ) {
   // The doc does exist so check for errors.

       String maxError = doc.getAttribute("/Error/maxerrorseverity");
       getDataFromDoc = true;
       // get oid form the doc
       oid = doc.getAttribute("/Out/PaymentTemplateGroup/payment_template_group_oid");
	   if (oid == null ) {
	    	oid = doc.getAttribute("/In/PaymentTemplateGroup/payment_template_group_oid");
	   }

       if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
       // We've returned from a save/update/delete that was successful
       } else {
       // We've returned from a save/update/delete but have errors.
       // We will display data from the cache.  Determine if we're
       // in insertMode by looking for a '0' oid in the input section
       // of the doc.
	           if(doc.getAttribute("/In/PaymentTemplateGroup/payment_template_group_oid").equals("0"))
	           {
	               Debug.debug("Create Payment Template Group Error");
	               isNew = true;//else it stays false
	           }
       }
   }

   /*
    * END OF MARK'S CODE
    */

    Debug.debug("Done Plagiarizing");

    if (getDataFromDoc)
    {

        // We create a new docHandler to work around an aparent bug in AMSEntityWebbean
        // This hurts performance and should be removed as soon as populateFromXmlDoc(doc,path) works
        DocumentHandler doc2 = doc.getComponent("/In");
        Debug.debug("/In  " + doc2);
        editPaymentTemplateGroup.populateFromXmlDoc(doc2);
        if (!isNew)
        {
            editPaymentTemplateGroup.setAttribute("payment_template_group_oid",oid);
            oid = editPaymentTemplateGroup.getAttribute("payment_template_group_oid");
        }
    }
    else if (request.getParameter("oid") != null) //EDIT payment template group
    {
        isNew = false;
        oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
        Debug.debug(oid);
        editPaymentTemplateGroup.getById(oid);
        Debug.debug("Editing Payment Template Group with oid: " + editPaymentTemplateGroup.getAttribute("payment_template_group_oid")); //DEBUG
        Debug.debug("isReadOnly == " + isReadOnly);
        //Make sure that a user can only modify payment template groups from his org
        String paymentTemplateGrpOwnerOid = editPaymentTemplateGroup.getAttribute("owner_org_oid");
        if (!paymentTemplateGrpOwnerOid.equals(userSession.getOwnerOrgOid()))
            isReadOnly = true;
        Debug.debug("isReadOnly == " + isReadOnly);
    }
    else //NEW payment template group
     {
        isNew = true;
        oid = null; 
     }

    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).
    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

    if (isReadOnly)
    {
        showSave = false;
        showSaveButton=false;
        showDeleteButton=false;
        showSaveClose=false;
    }
    else if (isNew)
        
    	showDeleteButton=false;
%>

<%-- Body tag included as part of common header --%>
<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="PaymentTemplateGroupDetail.Groups"/>
      <jsp:param name="helpUrl" value="customer/Template_Group_Form.htm"/>
    </jsp:include>

<%--cquinton 10/29/2012 ir#7015 remove return link--%>
<%
  String subHeaderTitle = "";
  if (isNew) {
    oid = "0";
    editPaymentTemplateGroup.setAttribute("payment_template_group_oid",oid);
                
    subHeaderTitle = resMgr.getText("PaymentTemplateGroupDetail.NewGroup",TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = editPaymentTemplateGroup.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include> 	

<form name="PaymentTemplateGroupDetail" id="PaymentTemplateGroupDetail" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>
	<div class="formMainHeader">
	


<%
            /*******************
            * START SECUREPARAMS
            ********************/
            secureParams.put("oid",oid);
            secureParams.put("owner_oid",userSession.getOwnerOrgOid());
            secureParams.put("ownership_level",userSession.getOwnershipLevel());

		// Used for the "Added By" column
		if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
			secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
		else
			secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);

            secureParams.put("login_oid",userSession.getUserOid());
            secureParams.put("login_rights",loginRights);
            secureParams.put("opt_lock", editPaymentTemplateGroup.getAttribute("opt_lock"));
            /*****************
            * END SECUREPARAMS
            ******************/
        %>
       
  </div>

  <div class="formArea">
  <jsp:include page="/common/ErrorSection.jsp" />

  <div class="formContent">
	<div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
	<div class="dijitTitlePaneContentInner">		     
        <%
        /********************************
         * START payment template group NAME INPUT FIELD
         ********************************/
        Debug.debug("Building payment template group Name field");
        /** IR VIUK090944011 09092010 updating text length from 30 to 35 **/
        
        out.print(widgetFactory.createTextField( "nameField", "PaymentTemplateGroupDetail.TemplName",editPaymentTemplateGroup.getAttribute("name"), "35", isReadOnly, true, false,"class='char30'","",""));
        /********************************
         * END payment template group NAME INPUT FIELD
         ********************************/
         %>
      
        <%
        /********************************
         * START payment template group DESCRIPTION INPUT FIELD
         ********************************/
        Debug.debug("Building payment template group Name field");
        
        out.print(widgetFactory.createTextField( "description", "PaymentTemplateGroupDetail.TemplDesc",editPaymentTemplateGroup.getAttribute("description"), "75", isReadOnly, true, false,"class='char45'","",""));
        /********************************
         * END payment template group DESCRIPTION INPUT FIELD
         ********************************/
         %>
	<br>
								<br>
								<br>
								<br>
								<br>
								
  </div>
</div>  
  </div><%--formContent--%>
  </div><%--formArea--%>

         <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PaymentTemplateGroupDetail'">
	
	                   <jsp:include page="/common/RefDataSidebar.jsp">
		                             <jsp:param name="showSaveButton" value="<%= showSaveButton %>" />
		                             <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
		                             <jsp:param name="showDeleteButton" value="<%= showDeleteButton %>" />
		                             <jsp:param name="cancelAction" value="goToRefDataHome" />
		                             <jsp:param name="showHelpButton" value="false" />
		                             <jsp:param name="helpLink" value="customer/payment_template_group_detail.htm" />
		                              <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
           							<jsp:param name="error" value="<%= error%>" />
           							<jsp:param name="showTop" value="false" />
           							<jsp:param name="showTips" value="true" />	
	
	                   </jsp:include>        
	
	          </div> <%--closes sidebar area--%>
            
      
 
  <%= formMgr.getFormInstanceAsInputField("PaymentTemplateGroupDetailForm", secureParams) %>
</form>
</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />

<%
  String focusField = "nameField";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("PaymentTemplateGroup");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
