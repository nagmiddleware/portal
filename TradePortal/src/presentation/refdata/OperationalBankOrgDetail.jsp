<%--
******************************************************************************
                          Operational Bank Org Detail

  Description:
     This page is used to maintain Operational Bank Orgs.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the organizations home page.
  Business Rules validations occur in the OperationalBankOrganizationBean.java
  file.  The structure of this jsp was modeled after the Client Bank Detail
  page.

******************************************************************************
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.frame.*,com.amsinc.ecsg.html.*,com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("***START******************Operational*Bank*Organization*Detail******************START***");
   userSession.setCurrentPrimaryNavigation("NavigationBar.Organizations");

    /*********\
    * GLOBALS *
    \*********/
    boolean isNew          = false; //is this a new Operational bank org
    boolean readOnly       = false; //is the page read only
    boolean getDataFromDoc = false; //do we need to refresh from the doc cache
    boolean showSave       = true;  //do we show the save button
    boolean showDelete     = true;  //do we show the delete button
    boolean  showSaveClose        = true;

    String oid          = null; //the oid of the operational bank org being edited
    String loginRights  = null; //login rights of the user viewing the page
    String options      = null; //used to create the Bank Group ListBox
    String defaultText  = null; //used to create the Bank Group ListBox

    Hashtable secureParams = new Hashtable();//stores the secure parameters

    DocumentHandler doc = null;//the doc we will use to get data from cache
    DocumentHandler bankOrgGroupsDoc = null;
	//IR# T36000038583 pgedupudi - Rel-9.3 CR-976A 06/03/2015
    List bnkgrps= new ArrayList();
    
    
  //Initialize WidgetFactory.
    WidgetFactory widgetFactory = new WidgetFactory(resMgr); 
    String buttonPressed ="";

    //Operational Bank Organization Web Bean
    beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.OperationalBankOrganizationWebBean", "OperationalBankOrganization");
    OperationalBankOrganizationWebBean editOpBankOrg = (OperationalBankOrganizationWebBean)beanMgr.getBean("OperationalBankOrganization");

    Debug.debug("*** OID passed into form manager from the listview ==" + request.getParameter("oid") + "****");
    
    //Get security rights
    loginRights = userSession.getSecurityRights();
    readOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_OPBANK_BY_CB);
    Debug.debug("MAINTAIN_OPBANK_BY_CB == "+SecurityAccess.hasRights(loginRights,
                                                           SecurityAccess.MAINTAIN_OPBANK_BY_CB));

    // Get the document from the cache.  We'll use it to repopulate the screen if the
    // user was entering data with errors.
    doc = formMgr.getFromDocCache();
    buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
      Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");
    Debug.debug("doc from cache is " + doc.toString());

    if (doc.getDocumentNode("/In/OperationalBankOrganization") != null )
    {
       getDataFromDoc = true;
       String maxError = doc.getAttribute("/Error/maxerrorseverity");
         oid = doc.getAttribute("/Out/OperationalBankOrganization/organization_oid");
         
         if(oid == null || "0".equals(oid) || "".equals(oid)){
             oid =  doc.getAttribute("/In/OperationalBankOrganization/organization_oid");
         }

         if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0) {
         // We've returned from a save/update/delete that was successful
         // We shouldn't be here.  Forward to the RefDataHome page.
         } else {
         // We've returned from a save/update/delete but have errors.
         // We will display data from the cache.  Determine if we're
         // in insertMode by looking for a '0' oid in the input section
         // of the doc.

             if (!isNew)
             // Not in insert mode, use oid from input doc.
             {
                 Debug.debug("Update BankBranchRule Error");
             }
             
             oid = doc.getAttribute("/In/OperationalBankOrganization/organization_oid");
             if(oid.equals("0"))
             {
                 Debug.debug("Error occered while CREATING a Operational Bank Org");
                 isNew = true;//else it stays false
             }
             else {// Not in insert mode, use oid from output doc.
                 Debug.debug("Error occured while UPDATING an Operational Bank Org");
                 oid = doc.getAttribute("/In/OperationalBankOrganization/organization_oid");
             }
         }
    }

 
    if (getDataFromDoc)
    {
        // We create a new docHandler to work around an aparent bug in AMSEntityWebbean
        // This hurts performance and should be removed as soon as populateFromXmlDoc(doc,path) works
        DocumentHandler doc2 = doc.getComponent("/In");
        Debug.debug("Populating editOpBankOrg with:\n" + doc2);
        editOpBankOrg.populateFromXmlDoc(doc2);
        if (!isNew)
            editOpBankOrg.setAttribute("organization_oid",oid);
    }
    else if(request.getParameter("oid")!= null)  //EDIT Operational Bank Org     
    {                                           
        isNew = false;
       
        Debug.debug("Oid in request if"+request.getParameter("oid"));
        oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());  
        Debug.debug("OID -> " + oid);
        editOpBankOrg.getById(oid);
        Debug.debug("Editing party with oid: " + editOpBankOrg.getAttribute("organization_oid")); //DEBUG

    }
    else
     {
        isNew = true;
        oid = null;
     }


    // onLoad is set to default the cursor to the Name field but only if the
    // page is not in readonly mode.
    String onLoad = "";
    if (!readOnly) {
    onLoad = "document.OperationalBankOrgDetail.name.focus();";           //FYI: doc.FormName.ControlName.focus()
    }

    if (readOnly)                //Set flags for button display.
    {
        showSave = false;
        showDelete = false;
        showSaveClose = false;
    }
    else if (isNew)
    {
        oid = "0";
        editOpBankOrg.setAttribute("organization_oid",oid);
        showDelete = false;
    }

    //START SECUREPARAMS
    secureParams.put("oid",oid);
    secureParams.put("login_oid",userSession.getUserOid());
    secureParams.put("login_rights",loginRights);
    secureParams.put("activation_status", TradePortalConstants.ACTIVE);
    secureParams.put("owner_bank_oid", userSession.getOwnerOrgOid());
    secureParams.put("opt_lock", editOpBankOrg.getAttribute("opt_lock"));
    //END SECUREPARAMS


    // Retrieve the data used to populate some of the dropdowns.

     StringBuilder sql = new StringBuilder();

    QueryListView queryListView = null;

    try {
      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
                        formMgr.getServerLocation(), "QueryListView");

      /*******************************************************************************
       *   Creating doc specific to the Bank Groups to store returned results of Query
       *   This data will be used to populate the Bank Group dropdown list.
       *******************************************************************************/
      /*IR# T36000038583 pgedupudi - Rel-9.3 CR-976A 06/03/2015 Start*/
      if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
      	String query="select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ";
 		DocumentHandler bankGrpList = new DocumentHandler();
 		List params=new ArrayList();
 		params.add(userSession.getBankGrpRestrictRuleOid());
         bankGrpList = DatabaseQueryBean.getXmlResultSet(query,false,params);
         Vector bgVector = bankGrpList.getFragments("/ResultSetRecord");
         for (int iLoop=0; iLoop<bgVector.size(); iLoop++)
		    {
		 	     DocumentHandler bankGrpforrestricDoc = (DocumentHandler) bgVector.elementAt(iLoop);
		 	     bnkgrps.add(bankGrpforrestricDoc.getAttribute("/BANK_ORGANIZATION_GROUP_OID"));
			}
      	
      }
      /* IR# T36000038583 pgedupudi - Rel-9.3 CR-976A 06/03/2015 End*/
      
      // Get a list of Bank Org Groups
      sql = new StringBuilder();
      sql.append("select ORGANIZATION_OID, NAME");
      sql.append(" from BANK_ORGANIZATION_GROUP");
      sql.append(" where P_CLIENT_BANK_OID = ?");
      sql.append(" and ACTIVATION_STATUS = ?");
      /* IR# T36000038583 pgedupudi - Rel-9.3 CR-976A 06/03/2015*/
      if(StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid())){
    	  sql.append(" and organization_oid not in "+ StringFunction.toSQLString(bnkgrps));
		}
      sql.append(" order by ");
      sql.append(resMgr.localizeOrderBy("NAME"));
      Debug.debug("Select Statement == " + sql.toString() );

      queryListView.setSQL(sql.toString(),new Object[]{userSession.getOwnerOrgOid(),TradePortalConstants.ACTIVE});
      queryListView.getRecords();

      // bankOrgGroupsDoc can be used to help create a listbox later...

      bankOrgGroupsDoc = queryListView.getXmlResultSet();
      Debug.debug("Returned resultset == " + bankOrgGroupsDoc.toString() );

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in OperationalBankOrgDetail.jsp");
      }
    }  // try/catch/finally block


%>
<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="additionalOnLoad" value="<%=onLoad%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp"></jsp:include>

<div class="pageMain">        <%-- Page Main Start --%>
  <div class="pageContent">   <%--  Page Content Start --%>

    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="OpBankOrg.OperationalBankOrgs"/>
      <jsp:param name="helpUrl" value="admin/op_bank_org_detail.htm"/>
    </jsp:include>

<%
  //cquinton 10/29/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  if (!isNew) {
    subHeaderTitle = editOpBankOrg.getAttribute("name");
  }
  else {
    subHeaderTitle = resMgr.getText("OpBankOrg.NewOpBankOrg", TradePortalConstants.TEXT_BUNDLE);
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>

<form id="OperationalBankOrgDetailForm" name="OperationalBankOrgDetailForm" data-dojo-type="dijit.form.Form" method="post" 
            action="<%=formMgr.getSubmitAction(response)%>">
  <input type=hidden value="" name=buttonName>

<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />
  
<%-- ********************************** Table Definition goes here *********************************** --%>

<div class="formContent">           <%--  Form Content Start --%>

<%-- General Title Panel details start --%>

<%= widgetFactory.createSectionHeader("1", "OpBankOrg.General") %>

<div class = "columnLeft">          <%--  columnLeft Start --%>

      <%=widgetFactory.createTextField("name","OpBankOrg.OpBankOrgName",
            editOpBankOrg.getAttribute("name") ,"35",readOnly,true,false,"", "", "") %>
            
      <%=widgetFactory.createTextField("address1","OpBankOrg.AddressLine1",
            editOpBankOrg.getAttribute("address_line_1") ,"35",readOnly,true,false,"", "", "") %>
            
    <%=widgetFactory.createTextField("address2","OpBankOrg.AddressLine2",
            editOpBankOrg.getAttribute("address_line_2") ,"35",readOnly,false,false,"", "", "") %>
            
    <%=widgetFactory.createTextField("city","OpBankOrg.City",
            editOpBankOrg.getAttribute("address_city") ,"23",readOnly,true,false,"class='char35'", "", "") %>            
            
    <%= widgetFactory.createTextField( "state", "OpBankOrg.ProvinceState", 
            editOpBankOrg.getAttribute("address_state_province"), "8", readOnly, false, false, "style='width:100px'","","inline")%>
            
    <%= widgetFactory.createTextField( "postalCode", "OpBankOrg.PostalCode", 
            editOpBankOrg.getAttribute("address_postal_code"), "8", readOnly, false, false,"style='width:100px'","","inline")%>  
    <div style="clear: both"></div>
            
      <%
      String options2 = Dropdown.createSortedRefDataOptions("COUNTRY", editOpBankOrg.getAttribute("address_country"),
                                                                   resMgr.getResourceLocale());
      %>
            
    <%= widgetFactory.createSelectField( "country", "OpBankOrg.Country"," ", options2, readOnly,true, false,"class='char35'", "", "")%>                             
        
        
      
</div>      <%-- columnLeft End --%>
<div class = "columnRight">         <%-- columnRight Start --%>

      <%=widgetFactory.createTextField("phone_number","OpBankOrg.GeneralPhoneNumber",
            editOpBankOrg.getAttribute("phone_number") ,"20",readOnly,false,false,"class='char14'", "", "") %>  
      <%-- <div class="formItem">
            <%=widgetFactory.createLabel("SwiftAddress","OpBankOrg.SwiftAddress",readOnly,false,false,"none") %>   </div>  --%>
      <%= widgetFactory.createTextField( "swift_address_part1", "OpBankOrg.SwiftAddress", 
                  editOpBankOrg.getAttribute("swift_address_part1"), "8", readOnly, false, false, "style='width:70px'","","inline")%>
            
      <%= widgetFactory.createTextField( "swift_address_part2", "&nbsp;", 
            editOpBankOrg.getAttribute("swift_address_part2"), "3", readOnly, false, false,"style='width:30px'","","none")%>  
      <div style="clear: both"></div>     
   
      <%= widgetFactory.createTextField( "fax_1", "OpBankOrg.FaxNumber1", 
                  editOpBankOrg.getAttribute("fax_1"), "20", readOnly, false, false, "class='char14'","","inline")%>
                  
      <%= widgetFactory.createTextField( "fax_2", "OpBankOrg.FaxNumber2", 
            editOpBankOrg.getAttribute("fax_2"), "20", readOnly, false, false,"class='char14'","","inline")%>  
      <div style="clear: both"></div>        
                                            
    <%= widgetFactory.createTextField( "proponixId", "OpBankOrg.ProponixOpBankOrgID", 
            editOpBankOrg.getAttribute("Proponix_id"), "35", readOnly, true, false,"class='char14'","","")%> 
            
      <%
      if(!bankOrgGroupsDoc.equals(""))
      {        options = ListBox.createOptionList(bankOrgGroupsDoc, 
                                            "ORGANIZATION_OID",
                                                    "NAME", 
                                                     editOpBankOrg.getAttribute("bank_org_group_oid"));
              Debug.debug("Options value before building the combo box ==> " + options );
              defaultText = resMgr.getText("OpBankOrg.selectBankGroup", TradePortalConstants.TEXT_BUNDLE);

       out.println(widgetFactory.createSelectField( "bankGroup", "OpBankOrg.BankGroup",defaultText, options, readOnly,true, false,"class='char30'", "", ""));
                
                
                
      }
         
%>
  
</div>            <%-- columnRight End --%>

</div>            <%-- General Panel End --%>

<%-- Prefix/Suffix Title Panel details start --%>

<%= widgetFactory.createSectionHeader("2", "OpBankOrg.PrefixandSuffixes") %>


<table class="formDocumentsTable" style="width:66%;float: left;" border="0" cellspacing="0" cellpadding="0">
      <thead>
            <tr style="height:35px">
                  <th width="60%">&nbsp;</th>
                  <th width="20%"><%-- <%= widgetFactory.createSubLabel("OpBankOrg.InstPrefix") %> --%><b><%=resMgr.getText("OpBankOrg.InstPrefix", TradePortalConstants.TEXT_BUNDLE)%></b></th>
                  <th width="20%"><%-- <%= widgetFactory.createSubLabel("OpBankOrg.InstSuffix") %> --%><b><%=resMgr.getText("OpBankOrg.InstSuffix", TradePortalConstants.TEXT_BUNDLE)%></b></th>
            </tr>
      </thead>
      <thead class="spanHeaderTabel">           
            <tr>
                  <td colspan="3" class="tableSubHeader"><b><%=resMgr.getText("OpBankOrg.Trade", TradePortalConstants.TEXT_BUNDLE)%></b></td>
                  <%-- <td colspan="3"><b><%= widgetFactory.createSubLabel("OpBankOrg.Trade") %></b></td> --%>
            </tr>       
      </thead>
      
      
      
      <tbody>
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.ApprovaltoPayPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("approvalToPayPrefx","",
                        editOpBankOrg.getAttribute("approval_to_pay_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("approvalToPaySuffx","",
                        editOpBankOrg.getAttribute("approval_to_pay_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.AWBPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("airWaybillPrefx","",
                        editOpBankOrg.getAttribute("airway_bill_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("airWaybillSuffx","",
                        editOpBankOrg.getAttribute("airway_bill_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.ECollPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("exportCollPrefx","",
                        editOpBankOrg.getAttribute("dir_snd_coll_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("exportCollSuffx","",
                        editOpBankOrg.getAttribute("dir_snd_coll_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.NExpCollPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("newExportCollPrefx","",
                        editOpBankOrg.getAttribute("new_exp_coll_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("newExportCollSuffx","",
                        editOpBankOrg.getAttribute("new_exp_coll_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.ELCPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("exportLCPrefix","",
                        editOpBankOrg.getAttribute("export_LC_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("exportLCSuffix","",
                        editOpBankOrg.getAttribute("export_LC_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.ILCPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("importLCprefix","",
                        editOpBankOrg.getAttribute("imp_DLC_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("importLCsuffix","",
                        editOpBankOrg.getAttribute("imp_DLC_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.LoanRequestPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("loanRequestPrefx","",
                        editOpBankOrg.getAttribute("loan_req_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("loanRequestSuffx","",
                        editOpBankOrg.getAttribute("loan_req_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.GteePrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("guaranteePrefix","",
                        editOpBankOrg.getAttribute("guar_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("guaranteeSuffix","",
                        editOpBankOrg.getAttribute("guar_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
            
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.SLCPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("standbyLCprefix","",
                        editOpBankOrg.getAttribute("incoming_SLC_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("standbyLCsuffix","",
                        editOpBankOrg.getAttribute("incoming_SLC_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>

      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.SGteePrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("shipGuarPrefx","",
                        editOpBankOrg.getAttribute("steamship_guar_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("shipGuarSuffx","",
                        editOpBankOrg.getAttribute("steamship_guar_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>

      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.RequestAdvisePrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("requestAdvisePrefx","",
                        editOpBankOrg.getAttribute("request_advise_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("requestAdviseSuffx","",
                        editOpBankOrg.getAttribute("request_advise_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      </tbody>
      <thead class="spanHeaderTabel">           
            <tr>
            <td colspan="3" class="tableSubHeader"><b><%=resMgr.getText("OpBankOrg.Payment", TradePortalConstants.TEXT_BUNDLE)%></b></td>
                  <%-- <td colspan="3"><b><%= widgetFactory.createSubLabel("OpBankOrg.Payment") %></b></td> --%>
            </tr>       
      </thead>
      <tbody>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.FundsTransferRequestPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("fundsTransferPrefx","",
                        editOpBankOrg.getAttribute("funds_transfer_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("fundsTransferSuffx","",
                        editOpBankOrg.getAttribute("funds_transfer_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.DomesticPaymentPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("domesticPaymentPrefx","",
                        editOpBankOrg.getAttribute("domestic_payment_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("domesticPaymentSuffx","",
                        editOpBankOrg.getAttribute("domestic_payment_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.TransferBtwAccountsPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("transferBtwAcctPrefx","",
                        editOpBankOrg.getAttribute("transfer_btw_acct_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("transferBtwAcctSuffx","",
                        editOpBankOrg.getAttribute("transfer_btw_acct_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      </tbody>
      
      <thead class="spanHeaderTabel">           
            <tr>
            <td colspan="3" class="tableSubHeader"><b><%=resMgr.getText("OpBankOrg.DirectDebit", TradePortalConstants.TEXT_BUNDLE)%></b></td>
                  <%-- <td colspan="3"><b><%= widgetFactory.createSubLabel("OpBankOrg.DirectDebit") %></b> </td>--%>
            </tr>       
      </thead>
      
      <tr>
                  <td> 
                        <%= widgetFactory.createSubLabel("OpBankOrg.DirectDebitInstructionPrefix") %>
                  </td>
                  <td> 
                              <%=widgetFactory.createTextField("directDebitPrefx","",
                        editOpBankOrg.getAttribute("direct_debit_prefix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
                  <td>
                              <%=widgetFactory.createTextField("directDebitSuffx","",
                        editOpBankOrg.getAttribute("direct_debit_suffix") ,"6",readOnly,false,false,"", "", "none") %>
                  </td>
      </tr>
      <tbody>
      
      </tbody>
      
</table>
<table><tr>
<td  style="width:100%;float:right;" valign=top>

&nbsp&nbsp<%= widgetFactory.createNote("OpBankOrg.PrefxSufxNoCharacters") %> 
&nbsp&nbsp<%= widgetFactory.createNote("OpBankOrg.PrefxSufxcombination") %>

</td>
</tr>
</table>
</div>            <%-- Prefix/Suffix Title Panel End --%>

</div> <%-- Form Content End --%>
</div><%--formArea--%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'OperationalBankOrgDetailForm'">
      
      <jsp:include page="/common/RefDataSidebar.jsp"> 
            <jsp:param name="showSaveButton" value="<%= showSave %>" />
             <jsp:param name="saveOnClick" value="none" />
            <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
            <jsp:param name="saveCloseOnClick" value="none" />
            <jsp:param name="showDeleteButton" value="<%= showDelete %>" />
            <jsp:param name="showHelpButton" value="true" />
            <jsp:param name="cancelAction" value="OperationalBankOrgCancel" />
            <jsp:param name="helpLink" value="admin/op_bank_org_detail.htm" />
            <jsp:param name="showLinks" value="true" />     
            <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
        <jsp:param name="error" value="<%= error%>" />      
      </jsp:include>     
               
</div> <%--closes sidebar area--%>

<%-- ********************************** End of Table Definition *********************************** --%>
  <%= formMgr.getFormInstanceAsInputField("OperationalBankOrgDetailForm", secureParams) %>
  </form>
  

</div>            <%--  Page Content End --%> 
</div>            <%--  Page Main End --%>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%
  //save behavior defaults to without hsm
  
  
  // (R9.3) XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";

 
%>
  

<jsp:include page="/common/RefDataSidebarFooter.jsp" />
  

</body>
</html>
<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("OperationalBankOrganization");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
   Debug.debug("***END*******************Operational*Bank*Organization*Detail*******************END***");
%>
