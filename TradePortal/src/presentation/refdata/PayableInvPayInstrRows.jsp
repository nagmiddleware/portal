<%--this Jsp for the ajax call to add 4 row in payable invoice payment instructions table--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*,
                 java.util.*" %>



<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session">
</jsp:useBean>

<% 


 boolean isReadOnly = false;
 final int DEFAULT_ROWS = 4; //default row to add in table
 int payInvInstrIndex = 0;
 String parmValue = request.getParameter("payableInvPayInstIndex");
 WidgetFactory widgetFactory = new WidgetFactory(resMgr);	

 if ( StringFunction.isNotBlank(parmValue) ) {
    try {
    	payInvInstrIndex = (new Integer(parmValue)).intValue();
    } 
    catch (Exception ex ) {
    	// row number is not correct. take initial value as 0.
    }
  }

  for (int count=0; count<DEFAULT_ROWS; count++) { 
%>
  <tr id="payableInvPayInstIndex<%=payInvInstrIndex+count%>">
    <td align="center"><%=payInvInstrIndex+count%></td>
	<td align="center">
	  <% String currencyOptions = Dropdown.createSortedCurrencyCodeOptions("", resMgr.getResourceLocale()); %>
	   <%=widgetFactory.createSelectField("PayCurrencyCode" + (payInvInstrIndex+count-1), "", " ", currencyOptions, isReadOnly, false, false, "class='char5'", "", "none")%>	
    </td>
	<td align="center">			  			
	  <%= widgetFactory.createCheckboxField("InvPayInstructionInd" + (payInvInstrIndex+count-1), "", false, isReadOnly, false, "", "", "none")%>
	</td>      	 
 </tr>
 <%
  } 
%>
