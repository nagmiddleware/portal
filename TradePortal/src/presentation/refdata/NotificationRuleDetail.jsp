
<%--
*******************************************************************************
                       Notification Rule Detail Page
  
  Description: This jsp sets up the data being used for the Notification Rule
  The idea is that this jsp will call the jsps that create the 4 tabs associated
  with the Notification Rules.  This jsp will set up the data that is used in 
  the called jsp(s).  The only way a user can get to this page is via Refdata 
  home. 
*******************************************************************************
--%>

<%--
*
*     Copyright   2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.common.InstrumentType,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.tradeportal.common.cache.*,java.util.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval/page setup begins here ****************  --%>

<%
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  Debug.debug("*********************** START: Notification Rule Detail Page **************************"); 

  String          notificationRuleOid     = request.getParameter("oid");
  String notificationTemplateId		  = request.getParameter("notificationTemplateId");
  String notifTemplateOid = "";
 // System.out.println("notificationTemplateId:"+notificationTemplateId);
  String                ownershipLevel          = ""; 
  String                emailFrequency          = "";
  String                emailForMsg             = "";
  String                emailForDisc            = "";
  String                templateInd            = "";
  String          emailForRecPayMatch     = "";
  String          emailForSettlementInstructionReq     = ""; 
  String          loginLocale             = userSession.getUserLocale();
  String          loginRights             = userSession.getSecurityRights();
  String                userOwnerLevel          = userSession.getOwnershipLevel(); 
  boolean         hasEditRights           = SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_NOTIFICATION_RULE);
  DocumentHandler defaultDoc        = formMgr.getFromDocCache();
  boolean         isReadOnly        = true;
  String 		  isTemplate		= TradePortalConstants.INDICATOR_NO;
  boolean               getDataFromDoc          = false;
  boolean showSave = true;
  boolean showDelete = false;
  boolean showSaveClose = true;
  boolean showSaveAs = true;
  Hashtable secureParms = new Hashtable();
  Hashtable criterionRuleList = new Hashtable();
  Hashtable criterionRuleTransList = new Hashtable();
  Hashtable userIdList = new Hashtable();
  ArrayList  dataList = new ArrayList();
  Set setupUserList = new HashSet();
  boolean filterInstruments = true;
  	CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
	corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
	corporateOrg.getDataFromAppServer();

	if (StringFunction.isNotBlank(userSession.getClientBankOid())) 
	  {
	    try
	    {
	  		Cache CBCache = (Cache)TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
	  		DocumentHandler CBCResult = (DocumentHandler)CBCache.get(userSession.getClientBankOid());
	  		String canfilterInstruments = CBCResult.getAttribute("/ResultSetRecord(0)/ALLOW_FILTERING_OF_INSTRUMENTS");
	  		if (TradePortalConstants.INDICATOR_NO.equals(canfilterInstruments))
	  		{
	  			filterInstruments = false;
	  		}
	  	}
	  	catch (Exception e)
	  	{
			System.out.println("General exception in verifying if filtering of instruments is enabled or not.  Will use default mode" + e.toString());
	  	}  	
	  }
  	final String[] portalInstrumentCatagory = {
			InstrumentType.AIR_WAYBILL, InstrumentType.APPROVAL_TO_PAY, InstrumentType.EXPORT_COL, 
			InstrumentType.NEW_EXPORT_COL, InstrumentType.EXPORT_DLC, InstrumentType.IMPORT_COL, 
			InstrumentType.IMPORT_DLC, InstrumentType.FUNDS_XFER, InstrumentType.LOAN_RQST,
			InstrumentType.GUARANTEE, InstrumentType.STANDBY_LC, InstrumentType.PAYABLES_MGMT, 
			InstrumentType.DOMESTIC_PMT, InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, InstrumentType.REQUEST_ADVISE, 
			InstrumentType.SHIP_GUAR,InstrumentType.SUPPLIER_PORTAL, InstrumentType.XFER_BET_ACCTS,
			InstrumentType.MESSAGES, InstrumentType.SUPP_PORT_INST_TYPE, InstrumentType.HTWOH_INV_INST_TYPE};
  	
  	final String[] tpsInstrumentCatagory = {
  			InstrumentType.BILLING, InstrumentType.CLEAN_BA, InstrumentType.INDEMNITY, 
  			InstrumentType.INCOMING_GUA, InstrumentType.INCOMING_SLC, InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT, 
  			InstrumentType.EXPORT_BANKER_ACCEPTANCE, InstrumentType.IMPORT_BANKER_ACCEPTANCE, InstrumentType.EXPORT_DEFERRED_PAYMENT, 
  			InstrumentType.IMPORT_DEFERRED_PAYMENT, InstrumentType.EXPORT_TRADE_ACCEPTANCE, InstrumentType.IMPORT_TRADE_ACCEPTANCE,
  			InstrumentType.REFINANCE_BA};
  
  	HashMap<String, String> dyanInstrType = new HashMap<String, String>();
  
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_airway_bill")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.AIR_WAYBILL)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.AIR_WAYBILL, InstrumentType.AIR_WAYBILL);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_approval_to_pay")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.APPROVAL_TO_PAY)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.APPROVAL_TO_PAY, InstrumentType.APPROVAL_TO_PAY);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_new_export_collection")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.NEW_EXPORT_COL)) || !filterInstruments) {
	 	dyanInstrType.put(InstrumentType.NEW_EXPORT_COL, InstrumentType.NEW_EXPORT_COL);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_export_collection")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.EXPORT_COL)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.EXPORT_COL, InstrumentType.EXPORT_COL);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_export_LC")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.EXPORT_DLC)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.EXPORT_DLC, InstrumentType.EXPORT_DLC);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_imp_col")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.IMPORT_COL)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.IMPORT_COL, InstrumentType.IMPORT_COL);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_import_DLC")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.IMPORT_DLC)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.IMPORT_DLC, InstrumentType.IMPORT_DLC);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_funds_transfer")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.FUNDS_XFER)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.FUNDS_XFER, InstrumentType.FUNDS_XFER);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_loan_request")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.LOAN_RQST)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.LOAN_RQST, InstrumentType.LOAN_RQST);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_guarantee")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.GUARANTEE)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.GUARANTEE, InstrumentType.GUARANTEE);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_SLC")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.STANDBY_LC)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.STANDBY_LC, InstrumentType.STANDBY_LC);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("pay_invoice_utilised_ind"))) || !filterInstruments){
		dyanInstrType.put(InstrumentType.PAYABLES_MGMT, InstrumentType.PAYABLES_MGMT);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_domestic_payments")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.DOMESTIC_PMT)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.DOMESTIC_PMT, InstrumentType.DOMESTIC_PMT);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("rec_invoice_utilised_ind"))) || !filterInstruments){
	  	dyanInstrType.put(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_request_advise")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.REQUEST_ADVISE))  || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.REQUEST_ADVISE, InstrumentType.REQUEST_ADVISE);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_shipping_guar")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.SHIP_GUAR)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.SHIP_GUAR, InstrumentType.SHIP_GUAR);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_supplier_portal"))) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.SUPPLIER_PORTAL, InstrumentType.SUPPLIER_PORTAL);
	}
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_xfer_btwn_accts")) &&
	  		InstrumentServices.canCreateInstrumentType(InstrumentType.XFER_BET_ACCTS)) || !filterInstruments){
	 	dyanInstrType.put(InstrumentType.XFER_BET_ACCTS, InstrumentType.XFER_BET_ACCTS);
	}

	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_non_prtl_org_instr_ind"))) || !filterInstruments){
	 	for(int i = 0; i < tpsInstrumentCatagory.length; i++){
	  		dyanInstrType.put(tpsInstrumentCatagory[i], tpsInstrumentCatagory[i]);
	 	}
	}

  	if(
	  (TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corporateOrg.getAttribute("dual_auth_disc_response")) ||
	  TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrg.getAttribute("dual_auth_disc_response"))||
	  TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrg.getAttribute("dual_auth_disc_response")) ||
	  TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrg.getAttribute("dual_auth_disc_response")) ||
	  TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(corporateOrg.getAttribute("dual_auth_disc_response"))) 
	  ||
	  (TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corporateOrg.getAttribute("dual_settle_instruction")) ||
	  TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrg.getAttribute("dual_settle_instruction"))||
	  TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrg.getAttribute("dual_settle_instruction")) ||
	  TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrg.getAttribute("dual_settle_instruction")) ||
	  TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(corporateOrg.getAttribute("dual_settle_instruction")))
	  ||
	  TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("dual_auth_arm"))
	  || !filterInstruments
	){
		dyanInstrType.put(InstrumentType.MESSAGES, InstrumentType.MESSAGES);
	}
	
	if((TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_supplier_portal")) || !filterInstruments)/*  &&
			InstrumentServices.canCreateInstrumentType(InstrumentType.SUPPLIER_PORTAL) */){
		dyanInstrType.put(InstrumentType.SUPP_PORT_INST_TYPE, InstrumentType.SUPP_PORT_INST_TYPE);
	}
	if(TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_h2h_rec_inv_approval")) ||
			TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_h2h_pay_inv_approval")) ||
			TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_h2h_pay_crn_approval"))
			|| !filterInstruments){
		dyanInstrType.put(InstrumentType.HTWOH_INV_INST_TYPE, InstrumentType.HTWOH_INV_INST_TYPE);
	}	
	
	final String[] instrumentCatagory = {
			InstrumentType.AIR_WAYBILL, InstrumentType.APPROVAL_TO_PAY, InstrumentType.BILLING,
			InstrumentType.CLEAN_BA, InstrumentType.EXPORT_COL, InstrumentType.EXPORT_BANKER_ACCEPTANCE, 
			InstrumentType.NEW_EXPORT_COL, InstrumentType.EXPORT_DEFERRED_PAYMENT, InstrumentType.EXPORT_DLC, 
			InstrumentType.INDEMNITY, InstrumentType.EXPORT_TRADE_ACCEPTANCE, InstrumentType.IMPORT_BANKER_ACCEPTANCE,
			InstrumentType.IMPORT_COL, InstrumentType.IMPORT_DEFERRED_PAYMENT, InstrumentType.IMPORT_DLC, 
			InstrumentType.IMPORT_TRADE_ACCEPTANCE, InstrumentType.INCOMING_GUA, InstrumentType.INCOMING_SLC,
			InstrumentType.FUNDS_XFER, InstrumentType.LOAN_RQST, InstrumentType.GUARANTEE, 
			InstrumentType.STANDBY_LC, InstrumentType.PAYABLES_MGMT, InstrumentType.DOMESTIC_PMT, 
			InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT, InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, InstrumentType.REFINANCE_BA, 
			InstrumentType.REQUEST_ADVISE, InstrumentType.SHIP_GUAR, InstrumentType.SUPPLIER_PORTAL,
			InstrumentType.XFER_BET_ACCTS, InstrumentType.MESSAGES, InstrumentType.SUPP_PORT_INST_TYPE, 
			InstrumentType.HTWOH_INV_INST_TYPE};
	  
	  final String [] AIR_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "PAY", "REA", "REL"};
	  final String [] ATP_TransArray = {"ARU", "ADJ", "AMD", "CHG", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA"};//1 EXT
	  final String [] BIL_TransArray = {"ARU", "ADJ", "BCH", "BCA", "PCS", "PCE", "DEA", "BNW","REA", "RED", "RVV"};
	  final String [] CBA_TransArray = {"ARU", "ADJ", "BUY", "CHG", "PCS", "PCE", "CUS", "LIQ"};//3 BUY PCS PCE
	  
	  final String [] EXP_OCO_TransArray = {"ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"};
	  final String [] EXP_DBA_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
	  final String [] EXP_COL_TransArray = {"ARU", "ADJ", "AMD", "CHG", "NAC", "ISS", "PAY", "REA"};
	  final String [] EXP_DFP_TransArray = { "ARU", "ADJ", "BUY",  "CHG", "CUS", "LIQ"};
	  
	  final String [] EXP_DLC_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
	  final String [] LOI_TransArray = {"ARU", "ADJ", "AMD", "CHG", "CRE", "DEA", "EXP", "PAY", "REA"};//2 DEA EXT
	  final String [] EXP_TAC_TransArray = { "ARU", "ADJ",  "BUY",  "CHG", "CUS", "LIQ"};
	  final String [] IMP_DBA_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"}; 
	  
	  final String [] IMC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "COL", "NAC", "PAY", "REA"};//DEA
	  final String [] IMP_DFP_TransArray = {"ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
	  final String [] IMP_DLC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
	  final String [] IMP_TAC_TransArray = { "ARU", "ADJ", "BUY", "CHG", "CUS", "LIQ"};
	  
	  final String [] INC_GUA_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
	  final String [] INC_SLC_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "PAY", "PRE", "REA", "RED", "RVV", "TRN", "PYT"};
	  final String [] FTRQ_TransArray = {"ARU", "ADJ", "ISS"};
	  final String [] LRQ_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"};
	  
	  final String [] GUA_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
	  final String [] SLC_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV"};
	  final String [] PYB_TransArray = {"CHP", "APM", "PPY"};
	  final String [] FTDP_TransArray = {"ARU", "ADJ", "ISS"};
	  
	  final String [] RFN_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "ISS", "LIQ"};
	  final String [] REC_TransArray = {"PAR", "CHM", "ARM"};
	  final String [] RBA_TransArray = {"ARU", "ADJ", "CHG", "PCS", "PCE", "CUS", "LIQ"};
	  final String [] RQA_TransArray = {"ARU", "ADJ", "ADV", "AMD", "ASN", "CHG", "PCS", "PCE", "DEA", "EXM", "EXP", "EXT", "ISS", "PAY", "REA", "RED", "RVV", "TRN", "PYT"};
	  
	  final String [] SHP_TransArray = {"ARU", "ADJ", "AMD", "CHG", "PCS", "PCE", "DEA", "EXP", "ISS", "PAY", "REA"};
	  final String [] SUPPLIER_PORTAL_TransArray = {"ISS", "LIQ"};
	  final String [] FTB_ATransArray = {"ARU", "ADJ", "ISS"};
	  final String [] MESSAGES_TransArray = {"DISCR", "MAIL", "MATCH", "SETTLEMENT"};
	  
	  final String [] LRQ_INV_TransArray = {"LRQ_INV"};
	  final String [] HTOH_TransArray = {"PIN", "PCN", "RIN"};

	HashMap<String, String[]> instrTransArrays = new HashMap<String, String[]>();
	instrTransArrays.put(InstrumentType.AIR_WAYBILL, AIR_TransArray);
	instrTransArrays.put(InstrumentType.APPROVAL_TO_PAY, ATP_TransArray);
	instrTransArrays.put(InstrumentType.BILLING, BIL_TransArray);
	instrTransArrays.put(InstrumentType.CLEAN_BA, CBA_TransArray);
	instrTransArrays.put(InstrumentType.EXPORT_COL, EXP_OCO_TransArray);
	
	instrTransArrays.put(InstrumentType.EXPORT_BANKER_ACCEPTANCE, EXP_DBA_TransArray);
	instrTransArrays.put(InstrumentType.NEW_EXPORT_COL, EXP_COL_TransArray);
	instrTransArrays.put(InstrumentType.EXPORT_DEFERRED_PAYMENT, EXP_DFP_TransArray);
	instrTransArrays.put(InstrumentType.EXPORT_DLC, EXP_DLC_TransArray);
	instrTransArrays.put(InstrumentType.INDEMNITY, LOI_TransArray);
	
	instrTransArrays.put(InstrumentType.EXPORT_TRADE_ACCEPTANCE, EXP_TAC_TransArray);
	instrTransArrays.put(InstrumentType.IMPORT_BANKER_ACCEPTANCE, IMP_DBA_TransArray);
	instrTransArrays.put(InstrumentType.IMPORT_COL, IMC_TransArray);
	instrTransArrays.put(InstrumentType.IMPORT_DEFERRED_PAYMENT, IMP_DFP_TransArray);
	instrTransArrays.put(InstrumentType.IMPORT_DLC, IMP_DLC_TransArray);
	
	instrTransArrays.put(InstrumentType.IMPORT_TRADE_ACCEPTANCE, IMP_TAC_TransArray);
	instrTransArrays.put(InstrumentType.INCOMING_GUA, INC_GUA_TransArray);
	instrTransArrays.put(InstrumentType.INCOMING_SLC, INC_SLC_TransArray);
	instrTransArrays.put(InstrumentType.FUNDS_XFER, FTRQ_TransArray);
	instrTransArrays.put(InstrumentType.LOAN_RQST, LRQ_TransArray);
	
	instrTransArrays.put(InstrumentType.GUARANTEE, GUA_TransArray);
	instrTransArrays.put(InstrumentType.STANDBY_LC, SLC_TransArray);
	instrTransArrays.put(InstrumentType.PAYABLES_MGMT, PYB_TransArray);
	instrTransArrays.put(InstrumentType.DOMESTIC_PMT, FTDP_TransArray);
	instrTransArrays.put(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT, RFN_TransArray);
	
	instrTransArrays.put(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, REC_TransArray);
	instrTransArrays.put(InstrumentType.REFINANCE_BA, RBA_TransArray);
	instrTransArrays.put(InstrumentType.REQUEST_ADVISE, RQA_TransArray);
	instrTransArrays.put(InstrumentType.SHIP_GUAR, SHP_TransArray);
	instrTransArrays.put(InstrumentType.SUPPLIER_PORTAL, SUPPLIER_PORTAL_TransArray);
	
	instrTransArrays.put(InstrumentType.XFER_BET_ACCTS, FTB_ATransArray);
	instrTransArrays.put(InstrumentType.MESSAGES, MESSAGES_TransArray);
	instrTransArrays.put(InstrumentType.SUPP_PORT_INST_TYPE, LRQ_INV_TransArray);
	instrTransArrays.put(InstrumentType.HTWOH_INV_INST_TYPE, HTOH_TransArray);
	
	String []dyanInstrCatArray = new String[dyanInstrType.size()];
	String instrTypeCode;
	int finalCounter = 0;
	
	System.out.println("*** dyanInstrType.size(): "+dyanInstrType.size());
	System.out.println("*** dyanInstrCatArray.length: "+dyanInstrCatArray.length);
	System.out.println("*** instrumentCatagory.length: "+instrumentCatagory.length);
	
	for (int i = 0; i < instrumentCatagory.length; i++){
		instrTypeCode = instrumentCatagory[i];
		if (null != dyanInstrType.get(instrTypeCode)){
			dyanInstrCatArray[finalCounter] = dyanInstrType.get(instrTypeCode);
			finalCounter++;
		}
	}
	
	//System.out.println("*** dyanInstrCatArray: "+Arrays.toString(dyanInstrCatArray));
	
	StringBuilder dynaInstrTypesStr = new StringBuilder();
	for(int i=0;i<dyanInstrCatArray.length;i++){
		dynaInstrTypesStr.append(dyanInstrCatArray[i]);
		
		if(i != dyanInstrCatArray.length-1){
			dynaInstrTypesStr.append(",");
		}
	}
	
	StringBuilder dynaInstrTransTypesStr = new StringBuilder();
	StringBuilder tempInstrTransTypesStr;
	String [] tempTransArray;
	for(int i=0;i<dyanInstrCatArray.length;i++){
		tempTransArray = instrTransArrays.get(dyanInstrCatArray[i]);
		tempInstrTransTypesStr = new StringBuilder();
		
		//System.out.println("*** dyanInstrCatArray"+i+": "+dyanInstrCatArray[i]);
		//System.out.println("*** tempTransArray: "+Arrays.toString(tempTransArray));
		for(int j=0;j<tempTransArray.length;j++){
			tempInstrTransTypesStr.append(tempTransArray[j]);
			
			if(j != tempTransArray.length-1){
				tempInstrTransTypesStr.append(",");
			}
		}
		
		dynaInstrTransTypesStr.append(tempInstrTransTypesStr);
		
		if(i != dyanInstrCatArray.length-1){
			dynaInstrTransTypesStr.append("||");
		}
	}
	
	HashMap<String, String> instrTransArrayStr = new HashMap<String, String>();
	HashMap<String, Integer> criteriaStartCounter = new HashMap<String, Integer>();
	StringBuilder tempTransStr;
	int counter = 0;
	
	for(int i=0;i<dyanInstrCatArray.length;i++){
		tempTransArray = instrTransArrays.get(dyanInstrCatArray[i]);
		tempTransStr = new StringBuilder();
		
		criteriaStartCounter.put(dyanInstrCatArray[i], new Integer(counter));
		for(int t=0;t<tempTransArray.length;t++){
			counter++;
			
			tempTransStr.append(tempTransArray[t]);
			if(t != tempTransArray.length-1){
				tempTransStr.append(",");
			}
		}
		instrTransArrayStr.put(dyanInstrCatArray[i], tempTransStr.toString());
	}
	
  	HashMap<String, String> sectionHeaderLable = new HashMap<String, String>();
	sectionHeaderLable.put(InstrumentType.AIR_WAYBILL,"NotificationRuleDetail.AirwaybillSection");
	sectionHeaderLable.put(InstrumentType.APPROVAL_TO_PAY,"NotificationRuleDetail.ApprovaltoPaySection");
	sectionHeaderLable.put(InstrumentType.BILLING,"NotificationRuleDetail.BillingSection");
	sectionHeaderLable.put(InstrumentType.CLEAN_BA,"NotificationRuleDetail.CleanAdvanceSection");
	sectionHeaderLable.put(InstrumentType.EXPORT_COL,"NotificationRuleDetail.DirectSendCollectionSection");
	
	sectionHeaderLable.put(InstrumentType.EXPORT_BANKER_ACCEPTANCE,"NotificationRuleDetail.ExportBankersAcceptanceSection");
	sectionHeaderLable.put(InstrumentType.NEW_EXPORT_COL,"NotificationRuleDetail.ExportCollectionSection");
	sectionHeaderLable.put(InstrumentType.EXPORT_DEFERRED_PAYMENT,"NotificationRuleDetail.ExportDeferredPaymentSection");
	sectionHeaderLable.put(InstrumentType.EXPORT_DLC,"NotificationRuleDetail.ExportDocumentaryLCSection");
	sectionHeaderLable.put(InstrumentType.INDEMNITY,"NotificationRuleDetail.ExportIndemnitySection");
	
	sectionHeaderLable.put(InstrumentType.EXPORT_TRADE_ACCEPTANCE,"NotificationRuleDetail.ExportTradeAcceptanceSection");
	sectionHeaderLable.put(InstrumentType.IMPORT_BANKER_ACCEPTANCE,"NotificationRuleDetail.ImportBankersAcceptanceSection");
	sectionHeaderLable.put(InstrumentType.IMPORT_COL,"NotificationRuleDetail.ImportCollectionSection");
	sectionHeaderLable.put(InstrumentType.IMPORT_DEFERRED_PAYMENT,"NotificationRuleDetail.ImportDeferredPaymentSection");
	sectionHeaderLable.put(InstrumentType.IMPORT_DLC,"NotificationRuleDetail.ImportDocumentaryLCSection");
	
	sectionHeaderLable.put(InstrumentType.IMPORT_TRADE_ACCEPTANCE,"NotificationRuleDetail.ImportTradeAcceptanceSection");
	sectionHeaderLable.put(InstrumentType.INCOMING_GUA,"NotificationRuleDetail.IncomingGuaranteeSection");
	sectionHeaderLable.put(InstrumentType.INCOMING_SLC,"NotificationRuleDetail.IncomingStandbyLCSection");
	sectionHeaderLable.put(InstrumentType.FUNDS_XFER,"NotificationRuleDetail.InternationalPaymentSection");
	sectionHeaderLable.put(InstrumentType.LOAN_RQST,"NotificationRuleDetail.LoanRequestSection");
	
	sectionHeaderLable.put(InstrumentType.GUARANTEE,"NotificationRuleDetail.OutgoingGuaranteeSection");
	sectionHeaderLable.put(InstrumentType.STANDBY_LC,"NotificationRuleDetail.OutgoingStandbyLCSection");
	sectionHeaderLable.put(InstrumentType.PAYABLES_MGMT,"NotificationRuleDetail.PayablesManagementSection");
	sectionHeaderLable.put(InstrumentType.DOMESTIC_PMT,"NotificationRuleDetail.PaymentSection");
	sectionHeaderLable.put(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT,"NotificationRuleDetail.ReceivablesFinanceSection");
	
	sectionHeaderLable.put(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT,"NotificationRuleDetail.ReceivablesManagementSection");
	sectionHeaderLable.put(InstrumentType.REFINANCE_BA,"NotificationRuleDetail.RefinanceSection");
	sectionHeaderLable.put(InstrumentType.REQUEST_ADVISE,"NotificationRuleDetail.RequesttoAdviseSection");
	sectionHeaderLable.put(InstrumentType.SHIP_GUAR,"NotificationRuleDetail.ShippingGuaranteeSection");
	sectionHeaderLable.put(InstrumentType.SUPPLIER_PORTAL,"NotificationRuleDetail.SupplierPortalSection");
	
	sectionHeaderLable.put(InstrumentType.XFER_BET_ACCTS,"NotificationRuleDetail.TransferBetweenAccountsSection");
	sectionHeaderLable.put(InstrumentType.MESSAGES,"NotificationRuleDetail.MailMessagesSection");
	sectionHeaderLable.put(InstrumentType.SUPP_PORT_INST_TYPE,"NotificationRuleDetail.SupplierInvoicesSection");
	sectionHeaderLable.put(InstrumentType.HTWOH_INV_INST_TYPE,"NotificationRuleDetail.HosttoHostInvoiceSection");
  
	HashMap<String, String> sectionNoteLable = new HashMap<String, String>();
	sectionNoteLable.put(InstrumentType.AIR_WAYBILL,"NotificationRuleDetail.AirwaybillSectionNote");
	sectionNoteLable.put(InstrumentType.APPROVAL_TO_PAY,"NotificationRuleDetail.ApprovaltoPaySectionNote");
	sectionNoteLable.put(InstrumentType.BILLING,"NotificationRuleDetail.BillingSectionNote");
	sectionNoteLable.put(InstrumentType.CLEAN_BA,"NotificationRuleDetail.CleanAdvanceSectionNote");
	sectionNoteLable.put(InstrumentType.EXPORT_COL,"NotificationRuleDetail.DirectSendCollectionSectionNote");
	
	sectionNoteLable.put(InstrumentType.EXPORT_BANKER_ACCEPTANCE,"NotificationRuleDetail.ExportBankersAcceptanceSectionNote");
	sectionNoteLable.put(InstrumentType.NEW_EXPORT_COL,"NotificationRuleDetail.ExportCollectionSectionNote");
	sectionNoteLable.put(InstrumentType.EXPORT_DEFERRED_PAYMENT,"NotificationRuleDetail.ExportDeferredPaymentSectionNote");
	sectionNoteLable.put(InstrumentType.EXPORT_DLC,"NotificationRuleDetail.ExportDocumentaryLCSectionNote");
	sectionNoteLable.put(InstrumentType.INDEMNITY,"NotificationRuleDetail.ExportIndemnitySectionNote");
	
	sectionNoteLable.put(InstrumentType.EXPORT_TRADE_ACCEPTANCE,"NotificationRuleDetail.ExportTradeAcceptanceSectionNote");
	sectionNoteLable.put(InstrumentType.IMPORT_BANKER_ACCEPTANCE,"NotificationRuleDetail.ImportBankersAcceptanceSectionNote");
	sectionNoteLable.put(InstrumentType.IMPORT_COL,"NotificationRuleDetail.ImportCollectionSectionNote");
	sectionNoteLable.put(InstrumentType.IMPORT_DEFERRED_PAYMENT,"NotificationRuleDetail.ImportDeferredPaymentSectionNote");
	sectionNoteLable.put(InstrumentType.IMPORT_DLC,"NotificationRuleDetail.ImportDocumentaryLCSectionNote");
	
	sectionNoteLable.put(InstrumentType.IMPORT_TRADE_ACCEPTANCE,"NotificationRuleDetail.ImportTradeAcceptanceSectionNote");
	sectionNoteLable.put(InstrumentType.INCOMING_GUA,"NotificationRuleDetail.IncomingGuaranteeSectionNote");
	sectionNoteLable.put(InstrumentType.INCOMING_SLC,"NotificationRuleDetail.IncomingStandbyLCSectionNote");
	sectionNoteLable.put(InstrumentType.FUNDS_XFER,"NotificationRuleDetail.InternationalPaymentSectionNote");
	sectionNoteLable.put(InstrumentType.LOAN_RQST,"NotificationRuleDetail.LoanRequestSectionNote");
	
	sectionNoteLable.put(InstrumentType.GUARANTEE,"NotificationRuleDetail.OutgoingGuaranteeSectionNote");
	sectionNoteLable.put(InstrumentType.STANDBY_LC,"NotificationRuleDetail.OutgoingStandbyLCSectionNote");
	sectionNoteLable.put(InstrumentType.PAYABLES_MGMT,"NotificationRuleDetail.PayablesManagementSectionNote");
	sectionNoteLable.put(InstrumentType.DOMESTIC_PMT,"NotificationRuleDetail.PaymentSectionNote");
	sectionNoteLable.put(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT,"NotificationRuleDetail.ReceivablesFinanceSectionNote");
	
	sectionNoteLable.put(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT,"NotificationRuleDetail.ReceivablesManagementSectionNote");
	sectionNoteLable.put(InstrumentType.REFINANCE_BA,"NotificationRuleDetail.RefinanceSectionNote");
	sectionNoteLable.put(InstrumentType.REQUEST_ADVISE,"NotificationRuleDetail.RequesttoAdviseSectionNote");
	sectionNoteLable.put(InstrumentType.SHIP_GUAR,"NotificationRuleDetail.ShippingGuaranteeSectionNote");
	sectionNoteLable.put(InstrumentType.SUPPLIER_PORTAL,"NotificationRuleDetail.SupplierPortalNote");
	
	sectionNoteLable.put(InstrumentType.XFER_BET_ACCTS,"NotificationRuleDetail.TransferBetweenAccountsSectionNote");
	sectionNoteLable.put(InstrumentType.MESSAGES,"NotificationRuleDetail.MailMessagesNote");
	sectionNoteLable.put(InstrumentType.SUPP_PORT_INST_TYPE,"NotificationRuleDetail.SupplierPortalInvoicesNote1");
	sectionNoteLable.put(InstrumentType.HTWOH_INV_INST_TYPE,"NotificationRuleDetail.HosttoHostInvoiceNote1");
  
	HashMap<String, String> sectionDialogTitle = new HashMap<String, String>();
	sectionDialogTitle.put(InstrumentType.AIR_WAYBILL,resMgr.getTextEscapedJS("NotificationRuleDetail.AirwaybillSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.APPROVAL_TO_PAY,resMgr.getTextEscapedJS("NotificationRuleDetail.ApprovaltoPaySectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.BILLING,resMgr.getTextEscapedJS("NotificationRuleDetail.BillingSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.CLEAN_BA,resMgr.getTextEscapedJS("NotificationRuleDetail.CleanAdvanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.EXPORT_COL,resMgr.getTextEscapedJS("NotificationRuleDetail.DirectSendCollectionSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	
	sectionDialogTitle.put(InstrumentType.EXPORT_BANKER_ACCEPTANCE,resMgr.getTextEscapedJS("NotificationRuleDetail.ExportBankersAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.NEW_EXPORT_COL,resMgr.getTextEscapedJS("NotificationRuleDetail.ExportCollectionSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.EXPORT_DEFERRED_PAYMENT,resMgr.getTextEscapedJS("NotificationRuleDetail.ExportDeferredPaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.EXPORT_DLC,resMgr.getTextEscapedJS("NotificationRuleDetail.ExportDocumentaryLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.INDEMNITY,resMgr.getTextEscapedJS("NotificationRuleDetail.ExportIndemnitySectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	
	sectionDialogTitle.put(InstrumentType.EXPORT_TRADE_ACCEPTANCE,resMgr.getTextEscapedJS("NotificationRuleDetail.ExportTradeAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.IMPORT_BANKER_ACCEPTANCE,resMgr.getTextEscapedJS("NotificationRuleDetail.ImportBankersAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.IMPORT_COL,resMgr.getTextEscapedJS("NotificationRuleDetail.ImportCollectionSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.IMPORT_DEFERRED_PAYMENT,resMgr.getTextEscapedJS("NotificationRuleDetail.ImportDeferredPaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.IMPORT_DLC,resMgr.getTextEscapedJS("NotificationRuleDetail.ImportDocumentaryLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	
	sectionDialogTitle.put(InstrumentType.IMPORT_TRADE_ACCEPTANCE,resMgr.getTextEscapedJS("NotificationRuleDetail.ImportTradeAcceptanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.INCOMING_GUA,resMgr.getTextEscapedJS("NotificationRuleDetail.IncomingGuaranteeSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.INCOMING_SLC,resMgr.getTextEscapedJS("NotificationRuleDetail.IncomingStandbyLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.FUNDS_XFER,resMgr.getTextEscapedJS("NotificationRuleDetail.InternationalPaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.LOAN_RQST,resMgr.getTextEscapedJS("NotificationRuleDetail.LoanRequestSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	
	sectionDialogTitle.put(InstrumentType.GUARANTEE,resMgr.getTextEscapedJS("NotificationRuleDetail.OutgoingGuaranteeSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.STANDBY_LC,resMgr.getTextEscapedJS("NotificationRuleDetail.OutgoingStandbyLCSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.PAYABLES_MGMT,resMgr.getTextEscapedJS("NotificationRuleDetail.PayablesManagementSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.DOMESTIC_PMT,resMgr.getTextEscapedJS("NotificationRuleDetail.PaymentSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT,resMgr.getTextEscapedJS("NotificationRuleDetail.ReceivablesFinanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	
	sectionDialogTitle.put(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT,resMgr.getTextEscapedJS("NotificationRuleDetail.ReceivablesManagementSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.REFINANCE_BA,resMgr.getTextEscapedJS("NotificationRuleDetail.RefinanceSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.REQUEST_ADVISE,resMgr.getTextEscapedJS("NotificationRuleDetail.RequesttoAdviseSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.SHIP_GUAR,resMgr.getTextEscapedJS("NotificationRuleDetail.ShippingGuaranteeSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.SUPPLIER_PORTAL,resMgr.getTextEscapedJS("NotificationRuleDetail.SupplierPortalDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	
	sectionDialogTitle.put(InstrumentType.XFER_BET_ACCTS,resMgr.getTextEscapedJS("NotificationRuleDetail.TransferBetweenAccountsSectionDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.MESSAGES,resMgr.getTextEscapedJS("NotificationRuleDetail.MailMessagesDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.SUPP_PORT_INST_TYPE,resMgr.getTextEscapedJS("NotificationRuleDetail.SupplierPortalInvoicesDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	sectionDialogTitle.put(InstrumentType.HTWOH_INV_INST_TYPE,resMgr.getTextEscapedJS("NotificationRuleDetail.HosttoHostInvoiceDialogTitle",TradePortalConstants.TEXT_BUNDLE));
	
	HashMap<String, Integer> instrTransArrayLength = new HashMap<String, Integer>();
	instrTransArrayLength.put(InstrumentType.AIR_WAYBILL, new Integer(AIR_TransArray.length));
	instrTransArrayLength.put(InstrumentType.APPROVAL_TO_PAY, new Integer(ATP_TransArray.length));
	instrTransArrayLength.put(InstrumentType.BILLING, new Integer(BIL_TransArray.length));
	instrTransArrayLength.put(InstrumentType.CLEAN_BA, new Integer(CBA_TransArray.length));
	instrTransArrayLength.put(InstrumentType.EXPORT_COL, new Integer(EXP_OCO_TransArray.length));
	
	instrTransArrayLength.put(InstrumentType.EXPORT_BANKER_ACCEPTANCE, new Integer(EXP_DBA_TransArray.length));
	instrTransArrayLength.put(InstrumentType.NEW_EXPORT_COL, new Integer(EXP_COL_TransArray.length));
	instrTransArrayLength.put(InstrumentType.EXPORT_DEFERRED_PAYMENT, new Integer(EXP_DFP_TransArray.length));
	instrTransArrayLength.put(InstrumentType.EXPORT_DLC, new Integer(EXP_DLC_TransArray.length));
	instrTransArrayLength.put(InstrumentType.INDEMNITY, new Integer(LOI_TransArray.length));
	
	instrTransArrayLength.put(InstrumentType.EXPORT_TRADE_ACCEPTANCE, new Integer(EXP_TAC_TransArray.length));
	instrTransArrayLength.put(InstrumentType.IMPORT_BANKER_ACCEPTANCE, new Integer(IMP_DBA_TransArray.length));
	instrTransArrayLength.put(InstrumentType.IMPORT_COL, new Integer(IMC_TransArray.length));
	instrTransArrayLength.put(InstrumentType.IMPORT_DEFERRED_PAYMENT, new Integer(IMP_DFP_TransArray.length));
	instrTransArrayLength.put(InstrumentType.IMPORT_DLC, new Integer(IMP_DLC_TransArray.length));
	
	instrTransArrayLength.put(InstrumentType.IMPORT_TRADE_ACCEPTANCE, new Integer(IMP_TAC_TransArray.length));
	instrTransArrayLength.put(InstrumentType.INCOMING_GUA, new Integer(INC_GUA_TransArray.length));
	instrTransArrayLength.put(InstrumentType.INCOMING_SLC, new Integer(INC_SLC_TransArray.length));
	instrTransArrayLength.put(InstrumentType.FUNDS_XFER, new Integer(FTRQ_TransArray.length));
	instrTransArrayLength.put(InstrumentType.LOAN_RQST, new Integer(LRQ_TransArray.length));
	
	instrTransArrayLength.put(InstrumentType.GUARANTEE, new Integer(GUA_TransArray.length));
	instrTransArrayLength.put(InstrumentType.STANDBY_LC, new Integer(SLC_TransArray.length));
	instrTransArrayLength.put(InstrumentType.PAYABLES_MGMT, new Integer(PYB_TransArray.length));
	instrTransArrayLength.put(InstrumentType.DOMESTIC_PMT, new Integer(FTDP_TransArray.length));
	instrTransArrayLength.put(InstrumentType.RECEIVABLES_FINANCE_INSTRUMENT, new Integer(RFN_TransArray.length));
	
	instrTransArrayLength.put(InstrumentType.RECEIVABLES_PAYBLES_MANAGEMENT, new Integer(REC_TransArray.length));
	instrTransArrayLength.put(InstrumentType.REFINANCE_BA, new Integer(RBA_TransArray.length));
	instrTransArrayLength.put(InstrumentType.REQUEST_ADVISE, new Integer(RQA_TransArray.length));
	instrTransArrayLength.put(InstrumentType.SHIP_GUAR, new Integer(SHP_TransArray.length));
	instrTransArrayLength.put(InstrumentType.SUPPLIER_PORTAL, new Integer(SUPPLIER_PORTAL_TransArray.length));
	
	instrTransArrayLength.put(InstrumentType.XFER_BET_ACCTS, new Integer(FTB_ATransArray.length));
	instrTransArrayLength.put(InstrumentType.MESSAGES, new Integer(MESSAGES_TransArray.length));
	instrTransArrayLength.put(InstrumentType.SUPP_PORT_INST_TYPE, new Integer(LRQ_INV_TransArray.length));
	instrTransArrayLength.put(InstrumentType.HTWOH_INV_INST_TYPE, new Integer(HTOH_TransArray.length));
  
  	int DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
  	int yy=-1;
	int xx=0;
	int notifEmailIndex = 0;
	int rowIndex = 0; 
	int numItems          = 0; 
	int transItems = 0;
	int criterionRuleCount = 0;
	int iLoop = 0;
	int sectionNum = 0;
	int uidx = 0;
	String sectionCategory = "";
	String tableRowIdPrefix="";
	String defaultSelection = "";
	String userEmailValues = "";
	String tempValue;
	String defSendNotifSetting = "";
	String defSendEmailSetting = "";
	String defAddlEmailAddresses = "";
	String defNotifEmailFerquency = "";
	String defUserIds = "";
	String defUserNames = "";
	String defApplytoAllTransBtn = "";
	String defClearAllTransBtn = "";
	String [] defUserIdArray = new String[24];
	String [] tempUserIdArray = new String[24];
	String          refDataHome       = "goToRefDataHome";
	String          oid;
	String          uOid;
	String          userListOid;
	String          setting;
	String          transactionType;
	String          instrumentType;
	String          sendNotifSetting;
	String          sendEmailSetting; 
	String          addEmailAddress;
	String 		  notifEmailFerquency;
	String 		  notifyEmailRow = "emailDefault";
	String 		  notifyEmail = "defaultEmail";
	String[] userIDArray = new String[24];
	StringBuilder dbUserIDArray = new StringBuilder();
	String[] transArray = {};
	String links ="";
	String buttonPressed ="";
	int sectionCounter = 0;//dyanInstrCatArray.length;
	buttonPressed = defaultDoc.getAttribute("/In/Update/ButtonPressed");

	Vector emailVector = new Vector();
	Vector transVector;
	Vector instrVector;
	Vector defListVector = new Vector();
	Vector error = null;
	error = defaultDoc.getFragments("/Error/errorlist/error");
  
  	//System.out.println("buttonPressed:"+defaultDoc.getFragments("In/Notifcations/One"));
  	NotificationRuleWebBean notificationRule = beanMgr.createBean(NotificationRuleWebBean.class, "NotificationRule");
      	
	if(hasEditRights){                 
     	isReadOnly = false;
    }
      	
    boolean rawOidValue = false; 
      	
    if (StringFunction.isBlank(notificationRuleOid)) {
    	notificationRuleOid = defaultDoc.getAttribute("/Out/NotificationRule/notification_rule_oid");
           
       	if(notificationRuleOid==null){
             	notificationRuleOid = defaultDoc.getAttribute("/In/NotificationRule/notification_rule_oid");
       	}
        
       	if (!StringFunction.isBlank(notificationRuleOid)&& !"0".equals(notificationRuleOid)){
        	rawOidValue = true;
        }
    }
	if(StringFunction.isNotBlank(notificationTemplateId)){
		notifTemplateOid = EncryptDecrypt.decryptStringUsingTripleDes(notificationTemplateId, userSession.getSecretKey());
	}
	if(StringFunction.isNotBlank(notifTemplateOid)){
		notificationRuleOid = notificationTemplateId;
	}
	//System.out.println("notifTemplateOid :"+notifTemplateOid+"\t notificationRuleOid:"+notificationRuleOid);
   	if (StringFunction.isNotBlank(notificationRuleOid)) {
       	if (!rawOidValue && !"0".equals(notificationRuleOid)){   
         	notificationRuleOid = EncryptDecrypt.decryptStringUsingTripleDes(notificationRuleOid, userSession.getSecretKey());
       	}
           //System.out.println("notificationRuleOid():"+notificationRuleOid);
       	notificationRule.setAttribute("notification_rule_oid", notificationRuleOid);
         notificationRule.getDataFromAppServer();
       
         // Make notification rule Template read only if this user is not the same ownership level
       	// as the notification rule
         //ownershipLevel = notificationRule.getAttribute("ownership_level");
         //if (!StringFunction.isBlank(ownershipLevel) && !userOwnerLevel.equals(ownershipLevel)){
         //  	isReadOnly = true;   
         //}
    } else {
      	notificationRuleOid = "0";
    }
      	
   	if (isReadOnly){
         showSave = false;
         showDelete = false;
         showSaveClose=false;
         showSaveAs = false;
    }
   	
    if(notificationRuleOid.equals("0")){
        showDelete = false;
        showSaveAs = false;
    }
    System.out.println("defaultDoc:"+defaultDoc);
      	//Now check to see if we came back with errors - other wise we need to display the list.
        String maxError = defaultDoc.getAttribute("/Error/maxerrorseverity");

        if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) > 0) {
          	getDataFromDoc = true;
          
          	//Disable Delet and SaveAs Buttons by Prateep
          	showDelete = false;
          	showSaveAs = false;
          	notificationRule.populateFromXmlDoc(defaultDoc.getComponent("/In"));
          	notificationRuleOid = notificationRule.getAttribute("notification_rule_oid");

          	if (StringFunction.isBlank(notificationRuleOid)) {
            	notificationRuleOid = "0"; 
          	} 
        }
        
     	// Get notification rule attributes used by other tab pages
        emailFrequency =  StringFunction.xssCharsToHtml(notificationRule.getAttribute("email_frequency"));
        if (StringFunction.isBlank(emailFrequency)){
            emailFrequency = TradePortalConstants.NOTIF_RULE_TRANSACTION;
        }
        
     	emailForMsg    =  StringFunction.xssCharsToHtml( notificationRule.getAttribute("email_for_message"));
        if (StringFunction.isBlank(emailForMsg)){
            emailForMsg = TradePortalConstants.NOTIF_RULE_NONE;
        }

        templateInd   =  StringFunction.xssCharsToHtml(notificationRule.getAttribute("template_ind"));
        if (TradePortalConstants.INDICATOR_YES.equals(templateInd)){
        	isTemplate = TradePortalConstants.INDICATOR_YES;
        }
        
        if (getDataFromDoc){
            // Populate notifyRuleCriterion[] bean with info from document handler
            DocumentHandler doc2 = defaultDoc.getComponent("/In");
            instrVector = doc2.getFragments("/NotificationRule/NotificationRuleCriterionList");
            if(instrVector !=null && instrVector.size()>0){
	            numItems = instrVector.size();
			}
            int transCriteriaStartCount = 0;
            
            NotificationRuleCriterionWebBean notifyRuleInstrTransCriterion[] = new NotificationRuleCriterionWebBean[numItems];
            if(numItems > 0){
	           // for (iLoop=0; iLoop<dyanInstrCatArray.length; iLoop++){
	            	//transCriteriaStartCount = instrTransCriteriaFieldStartCount[iLoop];
	            	//transArray = instrTransArrays.get(dyanInstrCatArray[iLoop]);
	            	for(int tLoop = 0; tLoop < numItems; tLoop++){
						
	            		notifyRuleInstrTransCriterion[tLoop] = beanMgr.createBean(NotificationRuleCriterionWebBean.class, "NotificationRuleCriterion");
	
	                  	DocumentHandler notifyRuleInstrTransCriterionDoc = (DocumentHandler) instrVector.elementAt(transCriteriaStartCount);
	                  
	                  	try { instrumentType = notifyRuleInstrTransCriterionDoc.getAttribute("/instrument_type_or_category"); }
	    	            catch (Exception e) { instrumentType = ""; }
	            		notifyRuleInstrTransCriterion[tLoop].setAttribute("instrument_type_or_category", instrumentType);
	            		
	            		try { oid = notifyRuleInstrTransCriterionDoc.getAttribute("/criterion_oid"); }
	          			catch (Exception e) { oid = ""; }
	            		notifyRuleInstrTransCriterion[tLoop].setAttribute("criterion_oid", oid);
						
	          			if((StringFunction.isNotBlank(oid) && !"0".equals(oid)) && !dataList.contains(instrumentType)){
							dataList.add(instrumentType);
						}
	                  	try { transactionType = notifyRuleInstrTransCriterionDoc.getAttribute("/transaction_type_or_action"); }
	    	            catch (Exception e) { transactionType = ""; }
	                  	notifyRuleInstrTransCriterion[tLoop].setAttribute("transaction_type_or_action", transactionType);
	
	    	            try { sendNotifSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/send_notif_setting"); }
	    	            catch (Exception e) { sendNotifSetting = ""; }
	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_notif_setting", sendNotifSetting);
	    	            
	    	            try { sendEmailSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/send_email_setting"); }
	    	            catch (Exception e) { sendEmailSetting = ""; }
	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_email_setting", sendEmailSetting);
	    	            
	    	            try { addEmailAddress = notifyRuleInstrTransCriterionDoc.getAttribute("/additional_email_addr"); }
	    	            catch (Exception e) { addEmailAddress = ""; }
	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("additional_email_addr", addEmailAddress);
	    	            
	    	            try { notifEmailFerquency = notifyRuleInstrTransCriterionDoc.getAttribute("/notify_email_freq"); }
	    	            catch (Exception e) { notifEmailFerquency = ""; }
	    	            notifyRuleInstrTransCriterion[tLoop].setAttribute("notify_email_freq", notifEmailFerquency);
	    	            
	                    // The hashtable key consists of the instrumentCategory (IMP,EXP,etc) plus the 
	                    // transactionType (ISS,AMD,etc) and criterionType (A,C,etc)
	                    String htKey = "";
	                    htKey = "";
	                    htKey = instrumentType+"_"+transactionType;
	                    //htKey = instrumentType+"_"+transactionType;
	
	                    criterionRuleTransList.put(htKey, notifyRuleInstrTransCriterion[tLoop]);
	            		transCriteriaStartCount++;
						}
	            	
	            //}
        	}//end of if(numItems > 0)
        } else
        if (notificationRuleOid != null){  
        	// Populate from the database
            String sql = new String();
        	QueryListView userEmailqueryListView = null;
        	QueryListView transQueryListView = null;
        	QueryListView mailQueryListView = null;
        	QueryListView supplierQueryListView = null;
        	QueryListView htohQueryListView = null;
        	QueryListView queryListView = null;
        	
        	String htKey = "";
			
			Vector vVector = null;
        	DocumentHandler transCriterionList = new DocumentHandler();
        	DocumentHandler mailCriterionList = new DocumentHandler();
        	DocumentHandler supplierCriterionList = new DocumentHandler();
        	DocumentHandler htohCriterionList = new DocumentHandler();
                  
        	queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
        	sql = "select criterion_oid, instrument_type_or_category from notification_rule_criterion where p_notify_rule_oid = ?";
        	Debug.debug(sql);
              
        	queryListView.setSQL(sql,new Object[]{notificationRuleOid});
        	queryListView.getRecords();
              
        	DocumentHandler criterionList = new DocumentHandler();
        	criterionList = queryListView.getXmlResultSet();
        	//Debug.debug(criterionList.toString());
        
        	instrVector = criterionList.getFragments("/ResultSetRecord");
        	numItems = instrVector.size();

        	NotificationRuleCriterionWebBean NotificationRuleCriterion[] = new NotificationRuleCriterionWebBean[numItems]; 
          	try {
            	for (iLoop=0;iLoop<dyanInstrCatArray.length;iLoop++) {
                  	transQueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
                	sql = "select instrument_type_or_category, criterion_oid, transaction_type_or_action, send_notif_setting, "
                			+ " send_email_setting, p_notify_rule_oid, additional_email_addr, notify_email_freq"
                			+ " from notification_rule_criterion where p_notify_rule_oid = ? and instrument_type_or_category = ?";
                      
                	transQueryListView.setSQL(sql,new Object[]{notificationRuleOid, dyanInstrCatArray[iLoop]});
                	transQueryListView.getRecords();
                      
                	transCriterionList = transQueryListView.getXmlResultSet();
                	//Debug.debug(transCriterionList.toString());
                
                	transVector = transCriterionList.getFragments("/ResultSetRecord");
                	transItems = transVector.size();
					if(transItems>0){
						dataList.add(dyanInstrCatArray[iLoop]);
					}
                	NotificationRuleCriterionWebBean notifyRuleInstrTransCriterion[] = new NotificationRuleCriterionWebBean[transItems];

                	for (int tLoop=0;tLoop<transItems;tLoop++) {
                  		notifyRuleInstrTransCriterion[tLoop] = beanMgr.createBean(NotificationRuleCriterionWebBean.class, "NotificationRuleCriterion");
                  		DocumentHandler notifyRuleInstrTransCriterionDoc = (DocumentHandler) transVector.elementAt(tLoop);
                      
                  		try { instrumentType = notifyRuleInstrTransCriterionDoc.getAttribute("/INSTRUMENT_TYPE_OR_CATEGORY"); }
    		            catch (Exception e) { instrumentType = ""; }
                  		notifyRuleInstrTransCriterion[tLoop].setAttribute("instrument_type_or_category", instrumentType);
                  		
                  		try { oid = notifyRuleInstrTransCriterionDoc.getAttribute("/CRITERION_OID"); }
                		catch (Exception e) { oid = ""; }
						if(TradePortalConstants.INDICATOR_YES.equals(isTemplate)){
                  			notifyRuleInstrTransCriterion[tLoop].setAttribute("criterion_oid", "");
						}else{
							notifyRuleInstrTransCriterion[tLoop].setAttribute("criterion_oid", oid);
						}
                		
                        try { transactionType = notifyRuleInstrTransCriterionDoc.getAttribute("/TRANSACTION_TYPE_OR_ACTION"); }
    		            catch (Exception e) { transactionType = ""; }
                        notifyRuleInstrTransCriterion[tLoop].setAttribute("transaction_type_or_action", transactionType);

    		            try { sendNotifSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/SEND_NOTIF_SETTING"); }
    		            catch (Exception e) { sendNotifSetting = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_notif_setting", sendNotifSetting);
    		            
    		            try { sendEmailSetting = notifyRuleInstrTransCriterionDoc.getAttribute("/SEND_EMAIL_SETTING"); }
    		            catch (Exception e) { sendEmailSetting = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("send_email_setting", sendEmailSetting);
    		            
    		            try { addEmailAddress = notifyRuleInstrTransCriterionDoc.getAttribute("/ADDITIONAL_EMAIL_ADDR"); }
    		            catch (Exception e) { addEmailAddress = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("additional_email_addr", addEmailAddress);
    		            
    		            try { notifEmailFerquency = notifyRuleInstrTransCriterionDoc.getAttribute("/NOTIFY_EMAIL_FREQ"); }
    		            catch (Exception e) { notifEmailFerquency = ""; }
    		            notifyRuleInstrTransCriterion[tLoop].setAttribute("notify_email_freq", notifEmailFerquency);
                  
                  		// The hashtable key consists of the instrumentCategory (IMP,EXP,etc) plus the 
                  		// transactionType (ISS,AMD,etc) and criterionType (A,C,etc)
                      	htKey = "";
                      	htKey = instrumentType+"_"+transactionType;

                      	criterionRuleTransList.put(htKey, notifyRuleInstrTransCriterion[tLoop]);
                      	
                      	/* userEmailqueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
	                	sql = "select notify_rule_user_list_oid, p_user_oid as user_oid from notify_rule_user_list where p_criterion_oid = ?";
	                	userEmailqueryListView.setSQL(sql,new Object[]{notifyRuleInstrTransCriterionDoc.getAttribute("criterion_oid")});
	                	userEmailqueryListView.getRecords();
	                	
	                	userEmailCriterionList = userEmailqueryListView.getXmlResultSet();
	                
	                	emailVector = userEmailCriterionList.getFragments("/ResultSetRecord");
	                	numItems = emailVector.size();
	                	
	                	NotifyRuleUserListWebBean notifyRuleUserList[] = new NotifyRuleUserListWebBean[numItems];
	                	
	                	userIDArray = new String[24];
	                	for (int eLoop=0;eLoop<numItems;eLoop++) {
	                		notifyRuleUserList[eLoop] = beanMgr.createBean(NotifyRuleUserListWebBean.class, "NotifyRuleUserList");
	                  		DocumentHandler notifyRuleUserListDoc = (DocumentHandler) emailVector.elementAt(eLoop);
	                  		userIDArray[eLoop] = notifyRuleUserListDoc.getAttribute("/USER_OID");
	                	}//eLoop */
            		}//tLoop
            	}
            	
            	/* mailQueryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
            	sql = "select criterion_oid, transaction_type_or_action, send_notif_setting, "
            			+ " send_email_setting, p_notify_rule_oid, additional_email_addr, notify_email_freq"
            			+ " from notification_rule_criterion where p_notify_rule_oid = ? and instrument_type_or_category = ?";
                  
            	mailQueryListView.setSQL(sql,new Object[]{notificationRuleOid, instrumentCatagory[iLoop]});
            	mailQueryListView.getRecords();
                  
            	mailCriterionList = mailQueryListView.getXmlResultSet();
            	Debug.debug(transCriterionList.toString());
            
            	transVector = mailCriterionList.getFragments("/ResultSetRecord");
            	numItems = transVector.size();

            	NotificationRuleCriterionWebBean notifyRuleInstrTransCriterion[] = new NotificationRuleCriterionWebBean[numItems]; */
          	} catch (Exception e) { 
        	  	e.printStackTrace(); 
        	} finally {
          		try {
            		if (transQueryListView != null) {
            			transQueryListView.remove();
            		} 
            		if (queryListView != null) {
            			queryListView.remove();
            		}
            	} catch (Exception e) { 
            		System.out.println("Error removing queryListView in NotificationRuleDetail.jsp"); 
            	}
          	}       
        }
        
        String notifyRuleUsersSql = "select USER_OID, LOGIN_ID, USER_IDENTIFIER from users where activation_status=? and p_owner_org_oid = ? and email_addr is not null";
        DocumentHandler notifyRuleUsersList = DatabaseQueryBean.getXmlResultSet(notifyRuleUsersSql, false, new Object[]{TradePortalConstants.ACTIVE, userSession.getOwnerOrgOid()});
		//Srinivasu_D IR#T36000048156 Rel9.5 04/23/2016 - holding actual users available in users table
		if(notifyRuleUsersList != null){
		  Vector v = notifyRuleUsersList.getFragments ("/ResultSetRecord");         
          for (int i = 0; i < v.size(); i++)
         {
            DocumentHandler doc = (DocumentHandler)v.elementAt (i);         
            setupUserList.add(doc.getAttribute("/USER_OID"));
          }
			
		}
		//Srinivasu_D IR#T36000048156 Rel9.5 04/23/2016 - End
     	//System.out.println("notifyRuleUsersList;"+notifyRuleUsersList);
     	String userEmailsOptions = "<option  value=\"\"></option>";
     	userEmailsOptions += ListBox.createOptionList(notifyRuleUsersList, "USER_OID", "USER_IDENTIFIER", "", null);
	    if(StringFunction.isNotBlank(notifTemplateOid)){     	
		secureParms.put("oid","0");
		}else{
			secureParms.put("oid",notificationRuleOid);
		}
	
		secureParms.put("owner_oid",userSession.getOwnerOrgOid());
		secureParms.put("ownership_level",userSession.getOwnershipLevel());
		secureParms.put("login_oid", userSession.getUserOid());
		secureParms.put("login_rights", userSession.getSecurityRights());

		// Used for the "Added By" column
		if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
		  	secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
		else
		  	secureParms.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);
		
		secureParms.put("login_rights",loginRights);
		secureParms.put("opt_lock", notificationRule.getAttribute("opt_lock"));
%>


<%-- ============================== HTML Starts HERE =================================== --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<div class="pageMain">
  <div class="pageContent">
    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="RefDataHome.NotificationRule"/>
      <jsp:param name="helpUrl" value="customer/notify_rules.htm"/>
    </jsp:include>

<%
  String subHeaderTitle = "";
  if( StringFunction.isBlank(notificationRuleOid) || notificationRuleOid.equals("0")) {
    subHeaderTitle = resMgr.getText("NotificationRuleDetail.NewNotificationRule",TradePortalConstants.TEXT_BUNDLE);
  }
  else {
    subHeaderTitle = notificationRule.getAttribute("name");
  } 
  
  isReadOnly = false;
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      


<%-- ============================== Display tab specific info ================================= --%>
<form name="NotificationRuleDetailForm" id="NotificationRuleDetailForm" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">

<input type=hidden value="" name=buttonName id=buttonName>
<input type=hidden value="<%=userSession.getOwnerOrgOid() %>" name=owner_oid>
<input type=hidden name=template_ind id=template_ind value="<%=TradePortalConstants.INDICATOR_NO%>">
<input type=hidden name=source_template_notif_oid id=source_template_notif_oid value="<%=notifTemplateOid%>">
	<div class="formArea">
		<jsp:include page="/common/ErrorSection.jsp" />
		<div class="formContent">		
			<%= widgetFactory.createSectionHeader("1", "NotificationRuleDetail.EmailAlertsDefaults", null, true) %>
			<%=widgetFactory.createNote("NotificationRuleDetail.TranInfo")%>
			<div class="columnLeftNoBorder">
				<%= widgetFactory.createTextField( "name", "NotificationRuleDetail.RuleName", 
					notificationRule.getAttribute("name"), "35", isReadOnly, true, false, "", "", "" ) %>	
			</div>
			<div class="columnRight">		
				<%= widgetFactory.createTextField( "description", "NotificationRuleDetail.Description", 
					notificationRule.getAttribute("description"), "25", isReadOnly, true, false, "class='char35'", "", "" ) %>
			</div>
			<div class="clear:both"></div>
			
			<%        
			secureParms.put("name", notificationRule.getAttribute("name"));	 
			%>
			&nbsp;&nbsp;&nbsp;<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailAlertMessage") %>
			<br>
			
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_TRANSACTION" ,"NotificationRuleDetail.WithReceipt",
					TradePortalConstants.NOTIF_RULE_TRANSACTION,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_TRANSACTION), isReadOnly, "", "")%>                          
			<br>
			
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_DAILY" ,"NotificationRuleDetail.Daily",
				TradePortalConstants.NOTIF_RULE_DAILY,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_DAILY), isReadOnly, "", "")%>                         
			<br>
			
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_BOTH" ,"NotificationRuleDetail.Both",
				TradePortalConstants.NOTIF_RULE_BOTH,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_BOTH), isReadOnly, "", "")%>                         
			<br>
			  
			<%=widgetFactory.createRadioButtonField("emailFrequency" ,"TradePortalConstants.NOTIF_RULE_NOEMAIL" ,"NotificationRuleDetail.NoEmail",
				TradePortalConstants.NOTIF_RULE_NOEMAIL,emailFrequency.equals(TradePortalConstants.NOTIF_RULE_NOEMAIL), isReadOnly, "", "")%>
			<br>
			
			<input type="hidden" name="notificationRuleOid" id="notificationRuleOid" value="<%=notificationRuleOid%>">
			<input type="hidden" value="<%=isReadOnly%>" name="isReadOnly" id="isReadOnly">
			<input type="hidden" name="defaultApplyToAllGroup" id="defaultApplyToAllGroup" value="">
            <input type="hidden" name="defaultClearToAllGroup" id="defaultClearToAllGroup" value="">
            <input type="hidden" name="defaultUpdateRecipientsOnly" id="defaultUpdateRecipientsOnly" value="">
            <input type="hidden" name="defaultUpdateEmailsOnly" id="defaultUpdateEmailsOnly" value="">
            <input type="hidden" name="defaultNotificationUserIds" id="defaultNotificationUserIds" value="">
            	
			<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.DefaultSectionNote") %>	
			<table>
				<tr>
					<td width="20%">
						<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendPortalNotificatios") %>
					</td>
					<td width="18%">
						<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
					</td>
					<td width="35%">
						<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailRecipients") %>
					</td>
					<td width="27%">
						<%=widgetFactory.createSubLabel("NotificationRuleDetail.AddlEmailRecipients", "subLabelNarrowLineHight") %>
					</td>
				</tr>
				<tr>
					<td width="20%" style="vertical-align: top;">
						<%=widgetFactory.createRadioButtonField("defaultNotifySetting","defaultNotifySettingAlways",
								"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultNotifySetting","defaultNotifySettingNone",
								"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultNotifySetting","defaultNotifySettingChrgsDocsOnly",
								"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
					</td>
					
					<td width="18%" style="vertical-align: top;">
						<%=widgetFactory.createRadioButtonField("defaultEmailSetting","defaultEmailSettingAlways",
								"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultEmailSetting","defaultEmailSettingNone",
								"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
						<br>
						<%=widgetFactory.createRadioButtonField("defaultEmailSetting","defaultEmailSettingChrgsDocsOnly",
								"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
					</td>
					<td width="35%" style="vertical-align: top;">
						<div id="notifUsersRowsDefault" style="height: auto; overflow-y: auto; overflow-x: hidden; max-height: 135px;">
							<table border="1" id="NotifUsersTableDefault" style="width:90%">
								<%tableRowIdPrefix="NotifUsersTableDefault"; %>
								<%sectionNum = 0; %>
								<tbody>
										<%@ include file="fragments/NotificationRuleDetail-UserEmailRows.frag" %>  
								</tbody>
							</table>
						</div>
					<%
						if (!isReadOnly) {
							if( StringFunction.isNotBlank(notificationRuleOid) && !notificationRuleOid.equals("0")) {
						%>
						<div>
							<button data-dojo-type="dijit.form.Button" type="button" id="updateUsersDefault">
								<%=resMgr.getText("NotificationRuleDetail.UpdateUsers",TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									local.updateUserRecipientsDefault('<%=dynaInstrTypesStr.toString()%>', '<%=dynaInstrTransTypesStr.toString()%>');
								</script>
							</button>
						</div>
						<%} %>
						<div>
							<button data-dojo-type="dijit.form.Button" type="button" id="add2MoreUsersDefault">
								<%=resMgr.getText("NotificationRuleDetail.Add2Users",TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									local.add2MoreUserEmailsDefault();
								</script>
							</button>
						</div>
					<%
						}
					%>  
					</td>
					<td width="27%" align="left" style="vertical-align: top;">
						<%= widgetFactory.createTextArea("defaultAdditionalEmailAddr", "", "", isReadOnly, false, false, "maxlength='300' rows='6'", "", "none") %>
						<%if( StringFunction.isNotBlank(notificationRuleOid) && !notificationRuleOid.equals("0")) { %>			
						<button data-dojo-type="dijit.form.Button"  name="UpdateEmailAddrDefault" id="UpdateEmailAddrDefault" type="button">
							<%=resMgr.getText("NotificationRuleDetail.UpdateEmailOnly",TradePortalConstants.TEXT_BUNDLE)%>
							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								local.updateAddnEmailsDefault('<%=dynaInstrTypesStr.toString()%>', '<%=dynaInstrTransTypesStr.toString()%>');
							</script>
						</button>
						<br>
					<%} %>
						<button data-dojo-type="dijit.form.Button"  name="ApplytoAllGroupsDefault" id="ApplytoAllGroupsDefault" type="button">
							<%=resMgr.getText("NotificationRuleDetail.ApplytoAllGroups.Button",TradePortalConstants.TEXT_BUNDLE)%>
							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								local.applyToAllGroups('<%=dynaInstrTypesStr.toString()%>', '<%=dynaInstrTransTypesStr.toString()%>');
							</script>
						</button>
						<button data-dojo-type="dijit.form.Button"  name="ClearAllDefault" id="ClearAllDefault" type="button">
							<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
							<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								local.clearAllDefaultSection();
							</script>
						</button>
					</td>
				</tr>
			</table>
	
		

			</div><%-- End div for Default Section Header --%>
      		<%@ include file="fragments/NotificationRuleDetail-AllInstrumentsSection.frag" %>
      		<%
      			if(
			      	  (TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corporateOrg.getAttribute("dual_auth_disc_response")) ||
			      	  TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrg.getAttribute("dual_auth_disc_response"))||
			      	  TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrg.getAttribute("dual_auth_disc_response")) ||
			      	  TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrg.getAttribute("dual_auth_disc_response")) ||
			      	  TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(corporateOrg.getAttribute("dual_auth_disc_response"))) 
			      	  ||
			      	  (TradePortalConstants.DUAL_AUTH_REQ_INSTRUMENT_DEFAULTS.equals(corporateOrg.getAttribute("dual_settle_instruction")) ||
			      	  TradePortalConstants.DUAL_AUTH_REQ_ONE_USER.equals(corporateOrg.getAttribute("dual_settle_instruction"))||
			      	  TradePortalConstants.DUAL_AUTH_REQ_TWO_USERS.equals(corporateOrg.getAttribute("dual_settle_instruction")) ||
			      	  TradePortalConstants.DUAL_AUTH_REQ_TWO_WORKGROUPS.equals(corporateOrg.getAttribute("dual_settle_instruction")) ||
			      	  TradePortalConstants.DUAL_AUTH_REQ_PANEL_AUTH.equals(corporateOrg.getAttribute("dual_settle_instruction")))
			      	  ||
			      	  TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("dual_auth_arm"))
			      	  || !filterInstruments
		      	){ 
      		%>
      			<%@ include file="fragments/NotificationRuleDetail-MailMessagesSection.frag" %>
      		<%
      			}
      		
	      		if(TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_supplier_portal")) || !filterInstruments/*  &&
	          			InstrumentServices.canCreateInstrumentType(InstrumentType.SUPPLIER_PORTAL) */){ 
      		%>
      			<%@ include file="fragments/NotificationRuleDetail-SupplierPortalInvoicesSection.frag" %>
      		<%
	      		}
	      		
	      		if(TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_h2h_rec_inv_approval")) ||
	          			TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_h2h_pay_inv_approval")) ||
	          			TradePortalConstants.INDICATOR_YES.equals(corporateOrg.getAttribute("allow_h2h_pay_crn_approval")) ||
	          			!filterInstruments){ 
      		%>
      			<%@ include file="fragments/NotificationRuleDetail-CreditNoteApprSection.frag" %>
      		<%
	      		}
      		%>
      		<div id='notificationRuleDetailDialogId'></div>
		</div><%--formContent--%>
	</div><%--formArea--%>  

<%= formMgr.getFormInstanceAsInputField("NotificationRuleDetailForm", secureParms) %>

<%
  defaultDoc.removeAllChildren("/Error");
  formMgr.storeInDocCache("default.doc", defaultDoc);
%>
<div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'NotificationRuleDetailForm'">
        <jsp:include page="/common/RefDataSidebar.jsp">
           <jsp:param name="showSaveButton" value="<%= showSave %>" />
           <jsp:param name="saveCloseOnClick" value="none" />
           <jsp:param name="saveOnClick" value="none" />
             <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
             <jsp:param name="showDeleteButton" value="<%=showDelete %>" />   
           <jsp:param name="showHelpButton"   value="true" />
           <jsp:param name="showLinks"         value="true"/>
             <jsp:param name="links"         value="<%=links%>"/>
           <jsp:param name="cancelAction"     value="goToRefDataHome" />
           <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
           <jsp:param name="error" value="<%= error%>" /> 
        </jsp:include>
</div>

</form>
</div>
</div>
<div id="saveAsDialogId" style="display: none;"></div>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
 <jsp:include page="/common/RefDataSidebarFooter.jsp" /> 
<script type="text/javascript">
	var local = {};

	function preSaveAndClose(){
	 	  document.forms[0].buttonName.value = "SaveAndClose";
	 	  return true;
	}
</script>
	<script type="text/javascript" src="/portal/js/page/notifyRuleDetail.js"></script>
</body>
</html>