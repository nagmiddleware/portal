<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
                 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>
   
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<%
   Debug.debug("******************");
   //ctq portal refresh comment out
   //userSession.setCurrentPrimaryNavigation("NavigationBar.RefData");

   /****************************************************************
    * THIS SCRIPTLET WILL FIND OUT IF WE ARE EDITING A PHRASE
    * CREATING A NEW ONE OR RETURNING FROM AN ERROR FROM A PREVIOUS
    * VERSION OF THIS PAGE.        WE WILL ALSO SET THE PAGE GLOBALS
    * AND POPULATE THE SECURE PARAMETER HASHTABLE THAT WILL BE SENT
    * TO THE FORM MANAGER.
    ****************************************************************/

   /**********
   * GLOBALS *
   **********/
   boolean isNew = false; //tells if this is a new phrase
   boolean isReadOnly = false; //tell if the page is read only
   boolean getDataFromDoc = false;
   boolean showSave = true;
   boolean showSaveClose = true;
   boolean showDelete = true;
   boolean showSaveAs = true;

   String oid = "";
   String loginRights;  

   beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.PhraseWebBean", "Phrase");
   PhraseWebBean editPhrase = (PhraseWebBean)beanMgr.getBean("Phrase");

   Hashtable secureParams   = new Hashtable();

   DocumentHandler doc      = null;
   
   //Get WidgetFactory instance..
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   String   refDataHome       = "goToRefDataHome";                //used for the close button and Breadcrumb link
   String buttonPressed ="";
   //Get security rights
   loginRights = userSession.getSecurityRights();
   isReadOnly = !SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PHRASE);
   Debug.debug("MAINTAIN_PHRASES, isReadOnly == " + SecurityAccess.hasRights(loginRights, SecurityAccess.MAINTAIN_PHRASE) +", " + isReadOnly);

   /*
    * THE FOLLOWING CODE WAS TAKEN FROM MARK WEITZ'S USER DETAIL PAGE
    */

   // Get the document from the cache.  We'll use it to repopulate the screen if the
   // user was entering data with errors.
   doc = formMgr.getFromDocCache();
   buttonPressed = doc.getAttribute("/In/Update/ButtonPressed");
   
   Vector error = null;
      error = doc.getFragments("/Error/errorlist/error");
   Debug.debug("doc from cache is " + doc.toString());

   if (doc.getDocumentNode("/In/Phrase") != null ) {
   // The doc does exist so check for errors.

       String maxError = doc.getAttribute("/Error/maxerrorseverity");
       getDataFromDoc = true;
       oid = doc.getAttribute("/Out/Phrase/phrase_oid");
            //if this was an update not an insert, we need to get from in
          if (oid == null ) {
              oid = doc.getAttribute("/In/Phrase/phrase_oid");
         }
       
       if (maxError != null && maxError.compareTo(String.valueOf(ErrorManager.WARNING_ERROR_SEVERITY)) < 0) {
       //no errors, get data from db
           
      } 
       else {
       // We've returned from a save/update/delete but have errors.
       // We will display data from the cache.  Determine if we're
       // in insertMode by looking for a '0' oid in the input section
       // of the doc.
           getDataFromDoc = true;
           if(doc.getAttribute("/In/Phrase/phrase_oid").equals("0"))
           {
               Debug.debug("Create Phrase Error");
               isNew = true;//else it stays false
           }

           if (!isNew)
           // Not in insert mode, use oid from input doc.
           {
               Debug.debug("Update Phrase Error");
               oid = doc.getAttribute("/In/Phrase/phrase_oid");
           }
       }
   }

   /*
    * END OF MARK'S CODE
    */

    Debug.debug("Done Plagiarizing");

    if (getDataFromDoc)
    {

        // We create a new docHandler to work around an aparent bug in AMSEntityWebbean
        // This hurts performance and should be removed as soon as populateFromXmlDoc(doc,path) works
        DocumentHandler doc2 = doc.getComponent("/In");
        Debug.debug("/In  " + doc2);
        editPhrase.populateFromXmlDoc(doc2);
        if (!isNew)
        {
            editPhrase.setAttribute("phrase_oid",oid);
            oid = editPhrase.getAttribute("phrase_oid");
        }
    }
    else if (request.getParameter("oid") != null || (oid!=null && !"".equals(oid.trim()) && !"0".equals(oid)) ) //EDIT PHRASE
    {
        isNew = false;
        if(oid!=null && !"".equals(oid.trim()) && !"0".equals(oid)){
            //We are back in this page from a successful save.
        }
        else if(request.getParameter("oid")!=null){
                  Debug.debug("Oid in request if"+request.getParameter("oid"));
                  oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
        }/* else{
            oid = oid;//EncryptDecrypt.decryptStringUsingTripleDes(oid, userSession.getSecretKey());
        } */
        Debug.debug(oid);
        
        editPhrase.getById(SQLParamFilter.filter(oid));
        
        Debug.debug("Editing phrase with oid: " + editPhrase.getAttribute("phrase_oid")); //DEBUG
        Debug.debug("isReadOnly == " + isReadOnly);
        //Make sure that a user can only modify phrases from his org
        String phraseOwnerOid = editPhrase.getAttribute("owner_org_oid");
        if (!phraseOwnerOid.equals(userSession.getOwnerOrgOid()))
            isReadOnly = true;
        Debug.debug("isReadOnly == " + isReadOnly);
    }
    
    else //NEW PHRASE
     {
        isNew = true;
        oid = null; 
        showSaveAs = true;
     }

    // Auto save the form when time-out if not readonly.  
    // (Header.jsp will check for auto-save setting of the corporate org).
    String onLoad = "";

    String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO : TradePortalConstants.INDICATOR_YES;

    if (isReadOnly)
    {
        showSave = false;
        showDelete = false;
        showSaveClose = false;
        showSaveAs = false;
    }
    else if (isNew){
        showDelete = false;
        showSaveAs = false;
    }

%>

<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="autoSaveFlag"             value="<%=autoSaveFlag%>" />
</jsp:include>

<div class="pageMain">        <%-- Page Main Start --%>
  <div class="pageContent">   <%--  Page Content Start --%>


    <jsp:include page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="Phrase"/>
      <jsp:param name="helpUrl" value="customer/phrase_detail.htm"/>
    </jsp:include>

<%--cquinton 10/29/2012 ir#7015 remove return link--%>
<% 
  String subHeaderTitle = "";
  if (isNew) {
    oid = "0";
    editPhrase.setAttribute("phrase_oid",oid);
    subHeaderTitle = resMgr.getText("PhraseDetail.NewPhrase",TradePortalConstants.TEXT_BUNDLE);  
  }
  else{
    subHeaderTitle = editPhrase.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      

<form name="PhraseDetailForm" id="PhraseDetailForm" data-dojo-type="dijit.form.Form" method="post" action="<%=formMgr.getSubmitAction(response)%>">


  <input type=hidden value="" name=buttonName>
  <input type=hidden value="" name=saveAsFlag>
  <input type=hidden value="<%=StringFunction.xssCharsToHtml(oid) %>" name=oid>
  
       
       <%
           

            /*******************
            * START SECUREPARAMS
            ********************/
            secureParams.put("oid",oid);
            secureParams.put("owner_oid",userSession.getOwnerOrgOid());
            secureParams.put("ownership_level",userSession.getOwnershipLevel());

            // Used for the "Added By" column
            if(userSession.getSecurityType().equals(TradePortalConstants.ADMIN))
                  secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_ADMIN);
            else
                  secureParams.put("ownership_type", TradePortalConstants.OWNER_TYPE_NON_ADMIN);

            secureParams.put("login_oid",userSession.getUserOid());
            secureParams.put("login_rights",loginRights);
            secureParams.put("opt_lock", editPhrase.getAttribute("opt_lock"));
            /*****************
            * END SECUREPARAMS
            ******************/
        %>
      


<div class="formArea">
<jsp:include page="/common/ErrorSection.jsp" />

<div class="formContent">           <%--  Form Content Start --%>
            <div class="dijitTitlePaneContentOuter dijitNoTitlePaneContentOuter">
      <div class="dijitTitlePaneContentInner">             
        <%
        /********************************
         * START PHRASE NAME INPUT FIELD
         ********************************/
        Debug.debug("Building Phrase Name field");
       // out.print(InputField.createTextField("nameField", editPhrase.getAttribute("name"), "25", "25", "ListText", isReadOnly));
        %>
        <%=widgetFactory.createTextField("nameField","PhraseDetail.PhraseName",
                    editPhrase.getAttribute("name") ,"25",isReadOnly,true,false, "style='width: 275px'", "", "") %>
        <%/********************************
         * END PHRASE NAME INPUT FIELD
         ********************************/
         %>
        
            <%
            /********************************
             * START PHRASE CATEGORY DROPDOWN
             ********************************/
            Debug.debug("Building Phrase Category field");

            Vector codesToExclude = null;

            // Sometimes, certain codes are to be excluded from the dropdown
            // Those codes are placed into the codesToExclude vector
            if (userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE))
             {
               codesToExclude = new Vector(2);
               codesToExclude.addElement(TradePortalConstants.PHRASE_CAT_PMT_INST);
               codesToExclude.addElement(TradePortalConstants.PHRASE_CAT_GUAR_BANK);
             }

            String options = Dropdown.createSortedRefDataOptions(TradePortalConstants.PHRASE_CATEGORY, 
                                                                 editPhrase.getAttribute("phrase_category"), 
                                                                 resMgr.getResourceLocale(), codesToExclude);
            Debug.debug(options);

           // if (isNew)
            //    options = "<option value='' selected> </option>" + options;
           // out.print(InputField.createSelectField("phraseCat", "", "", options, "ListText", isReadOnly));
           %>
            <%= widgetFactory.createSelectField("phraseCat", "PhraseDetail.PhraseCategory", 
                              " ", options, isReadOnly, true, false,"style='width: 275px'", "", "") %>
           <%/********************************
             * END PHRASE CATEGORY DROPDOWN
             ********************************/
            %>
        <%
            if (!(userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE))) //DON'T SHOW THIS DROPDOWN TO CORPORATE USERS
            {
        
            /********************************
             * START PHRASE TYPE DROPDOWN
             ********************************/
            Debug.debug("Building Phrase Type field");
            String options2 = Dropdown.createSortedRefDataOptions("PHRASE_TYPE", editPhrase.getAttribute("phrase_type"), resMgr.getResourceLocale());
         //   out.print(InputField.createSelectField("phraseType", "", "", options2, "ListText", isReadOnly));
         %>   
         
         <%= widgetFactory.createSelectField("phraseType", "PhraseDetail.PhraseType", 
                              "", options2, isReadOnly, true, false,"style='width: 275px'", "", "") %>
         <% /********************************
             * END PHRASE TYPE DROPDOWN
             ********************************/
            }// THIS BRACE CLOSES THE "if (!(userLevel.equals("CORPORATE")))" STATEMENT ABOVE
            else
                secureParams.put("phraseType", "MODIF");//This is the default value for corporate users
            %>
            
            
        <%
        /********************************
         * START PHRASE TEXT TEXT AREA
         ********************************/
        Debug.debug("Building Phrase Text field");
        if (!isReadOnly) //we are editing an existing phrase
        {
            //out.print("<textarea name='phraseText' wrap='PHYSICAL' cols='90' rows='15' maxlength='4000' class='ListText'>");
            //out.print(editPhrase.getAttribute("phrase_text"));
            //out.print("</textarea>");
      %>
            <%=widgetFactory.createTextArea("phraseText","PhraseDetail.PhraseText",
                        editPhrase.getAttribute("phrase_text") , 
                        false, true, false, "rows='10'", "", "") %> 
      <%
        }
        else if (isReadOnly) //we are viewing an existing phrase
        {
            %>
             <%=widgetFactory.createTextArea("phraseText","PhraseDetail.PhraseText",
                        editPhrase.getAttribute("phrase_text") , 
                        true, true, false, "rows='10'", "", "") %>
            <%
        }
        /********************************
         * END PHRASE TEXT TEXT AREA
         ********************************/
        %>
   </div>
  </div>
  </div>          <%--  Form Content End --%>
</div><%--formArea--%>

 <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'PhraseDetailForm'">
<jsp:include page="/common/RefDataSidebar.jsp">
      <jsp:param name="isReadOnly" value="<%=isReadOnly%>" />
      <jsp:param name="showSaveButton" value="<%= showSave %>" />
      <jsp:param name="saveOnClick" value="none" />
      <jsp:param name="showSaveAsButton" value="<%=showSaveAs%>" />
      <jsp:param name="showDeleteButton" value="<%= showDelete %>" />
      <jsp:param name="showSaveCloseButton" value="<%= showSaveClose%>" />
      <jsp:param name="saveCloseOnClick" value="none" />
      <jsp:param name="showHelpButton" value="true" />
      <jsp:param name="cancelAction" value="PhraseCancel" />
      <jsp:param name="helpLink" value="customer/phrase_detail.htm" />
      <jsp:param name="saveAsDialogPageName" value="PhraseDetailSaveAs.jsp" />
      <jsp:param name="saveAsDialogId" value="saveAsDialogId" />
      <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
      <jsp:param name="error" value="<%=error%>" />
      <jsp:param name="showLinks" value="false" />
</jsp:include>
</div> <%-- Side Bar End--%>

  <%= formMgr.getFormInstanceAsInputField("PhraseDetailForm", secureParams) %>
</form>

</div>            <%--  Page Content End --%> 
</div>            <%--  Page Main End --%>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>
<%
  //save behavior defaults to without hsm
  
  //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
  
  // String saveOnClick = "common.setButtonPressed('SaveTrans','0'); document.forms[0].submit();";
  // String saveCloseOnClick = "common.setButtonPressed('SaveAndClose','0'); document.forms[0].submit();";
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp"/>

 
 <div id="saveAsDialogId" style="display: none;"></div>
 
<%
  String focusField = "nameField";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          if(focusField){ 
                  focusField.focus();
            }
        }
      });
  });
  
  function submitSaveAs() 
  {
          if(window.document.saveAsForm.newProfileName.value == ""){
            return false;
          }
            document.PhraseDetailForm.nameField.value=window.document.saveAsForm.newProfileName.value;
            document.PhraseDetailForm.oid.value="0";
            <%-- Sets a hidden field with the 'button' that was pressed.  This --%>
            <%-- value is passed along to the mediator to determine what --%>
            <%-- action should be done. --%>
            document.PhraseDetailForm.saveAsFlag.value = "Y";
            document.PhraseDetailForm.buttonName.value = "SaveAndClose";
            document.PhraseDetailForm.submit();
            hideDialog("saveAsDialogId");
   }
  
  
	function preSaveAndClose(){
	  	 	 
	 	  document.forms[0].buttonName.value = "SaveAndClose";
	 	  return true;
	
	}






</script>
<%
  }
%>


</body>

</html>

<%
  /**********
   * CLEAN UP
   **********/

   beanMgr.unregisterBean("Phrase");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
