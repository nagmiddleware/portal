
<%--
*******************************************************************************
                               ATP Creation Rule Detail

  Description:
  This page is used to maintain ATP creation rules.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2007                         
 *     CGI
 *     All rights reserved
--%>
<%@ page
      import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.tradeportal.busobj.*,java.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
      scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
      class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
      scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
      boolean isReadOnly = false;

      boolean isViewInReadOnly = false;

      String defnName = "";

      String options = "", optionsATP = "", optionsLC = "", optionsATPBB = "", optionsLCBB = "";
      String storeATP = "", storeLC = "", storeATPBB = "", storeLCBB = "";
      String fieldOptions = "";
      String defaultText;
      String link;

      String oid = null;
      String definitionOid = "";
      
        String buttonPressed = "";
        Vector error = null;

      // These booleans help determine the mode of the JSP.  
      boolean getDataFromDoc = false;
      boolean retrieveFromDB = false;
      boolean insertMode = false;
      boolean isNew=false;

      boolean showSave = false;
      boolean showDelete = false;
      boolean showSaveClose =false;
      boolean showClose= true;

      DocumentHandler doc;

      // Must initialize these 2 doc, otherwise get a compile error.
      DocumentHandler templatesDocATP = null, templatesDocLC = null;
      DocumentHandler opBankOrgsDocATP = null, opBankOrgsDocLC = null;


      WidgetFactory widgetFactory = new WidgetFactory(resMgr);

      InvOnlyCreationRuleWebBean rule = beanMgr.createBean(InvOnlyCreationRuleWebBean.class, "InvOnlyCreateRule");
      InvoiceDefinitionWebBean invDefinition = beanMgr.createBean(InvoiceDefinitionWebBean.class, "InvoiceDefinition");
      
      String loginLocale;
      String loginRights;
      String viewInReadOnly;

      viewInReadOnly = request.getParameter("fromHomePage");
      
      // DK IR T36000019543 Rel8.3 08/06/2013 starts      
          CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
          corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
          corporateOrg.getDataFromAppServer();
          
          boolean allow_loan_request = true;
          boolean allow_approval_to_pay = true;          

          if (TradePortalConstants.INDICATOR_NO.equals(corporateOrg.getAttribute("allow_loan_request")))
        	  allow_loan_request = false;
          if (TradePortalConstants.INDICATOR_NO.equals(corporateOrg.getAttribute("allow_approval_to_pay")))
        	  allow_approval_to_pay = false;          
       // DK IR T36000019543 Rel8.3 08/06/2013 ends
      
      
      //If we got to this page from the Home Page, then we need to explicitly be in Read only mode.
      //Also, the close button action needs to take us back to the home page.  This flag will help
      //us keep track of where to go.

      if ((InstrumentServices.isNotBlank(viewInReadOnly))
                  && (viewInReadOnly.equals("true"))) {
            isViewInReadOnly = true;
      }

      // Get the webbeans for the login in user and his security profile.  Set the
      // readonly access appropriately.
      loginLocale = userSession.getUserLocale();
      loginRights = userSession.getSecurityRights();
      
      String ruleTypeSecurityAccFlag = null;
      
      String ruleType = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("RuleType"), userSession.getSecretKey());
      

      
      if (ruleType != null){
            if (ruleType.equalsIgnoreCase(InstrumentType.LOAN_RQST)) {
                  if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LR_CREATE_RULES)      ) {
                        isReadOnly = false;
                  } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_LR_CREATE_RULES)) {
                        isReadOnly = true;
                  }     
            } else if (ruleType.equalsIgnoreCase(InstrumentType.APPROVAL_TO_PAY)){
                  if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES)     ) {
                        isReadOnly = false;
                  } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_ATP_INV_CREATE_RULES)) {
                        isReadOnly = true;
                  }     
            }
      }
      
      // We should only be in this page for corporate users.  If the logged in
      // user's level is not corporate, send back to ref data page.  This
      // condition should never occur but is caught just in case.
      String ownerLevel = userSession.getOwnershipLevel();
      if (!ownerLevel.equals(TradePortalConstants.OWNER_CORPORATE)) {
  		//if (false){
			formMgr.setCurrPage("RefDataHome");
            String physicalPage = NavigationManager.getNavMan()
                        .getPhysicalPage("RefDataHome", request);
%>
			<jsp:forward page='<%= physicalPage %>' />
<%
      return;
      }

      if (isViewInReadOnly)
            isReadOnly = true;

      Debug.debug("login_rights is " + loginRights);

      // Get the document from the cache.  We'll use it to repopulate the screen 
      // if the user was entering data with errors.
      doc = formMgr.getFromDocCache();
       buttonPressed =doc.getAttribute("/In/Update/ButtonPressed");
      error = doc.getFragments("/Error/errorlist/error");
        

      Debug.debug("doc from cache is " + doc.toString());

      if (doc.getDocumentNode("/In/InvOnlyCreateRule") == null) {
            // The document does not exist in the cache.  We are entering the page from
            // the listview page (meaning we're opening an existing item or creating 
            // a new one.)

            if (request.getParameter("oid") != null) {
        		oid = EncryptDecrypt.decryptStringUsingTripleDes(request.getParameter("oid"), userSession.getSecretKey());
            } else {
                  oid = null;
            }

            if ((oid == null) || (oid.equals(""))) {
                  getDataFromDoc = false;
                  retrieveFromDB = false;
                  insertMode = true;
                  oid = "0";
            } else if (oid.equals("0")) {
                  getDataFromDoc = false;
                  retrieveFromDB = false;
                  insertMode = true;
            } else {
                  // We have a good oid so we can retrieve.
                   Debug.debug("We have a good oid so we can retrieve." + oid);
                  getDataFromDoc = false;
                  retrieveFromDB = true;
                  insertMode = false;
            }
      } else {
            // The doc does exist so check for errors.  If highest severity < WARNING,
            // its a good update.
            retrieveFromDB = false;
            getDataFromDoc = true;

            String maxError = doc.getAttribute("/Error/maxerrorseverity");
            oid = doc.getAttribute("/Out/InvOnlyCreateRule/inv_only_create_rule_oid");
          if (oid == null ) {
            oid = doc.getAttribute("/In/InvOnlyCreateRule/inv_only_create_rule_oid");
          }
            
             if (maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY)) < 0 ) {
                  // We've returned from a save/update/delete that was successful
                  // We shouldn't be here.  Forward to the RefDataHome page.
                  
            } else {
                  // We've returned from a save/update/delete but have errors.
                  // We will display data from the cache.  Determine if we're
                  // in insertMode by looking for a '0' oid in the input section
                  // of the doc.
                  String newOid = doc
                              .getAttribute("/In/InvOnlyCreateRule/inv_only_create_rule_oid");

                  if (newOid.equals("0")) {
                        insertMode = true;
                        oid = "0";
                  } else {
                        // Not in insert mode, use oid from input doc.
                        insertMode = false;
                        oid = doc
                                    .getAttribute("/In/InvOnlyCreateRule/inv_only_create_rule_oid");
                  }
            }
      }

      if (retrieveFromDB) {
            // Attempt to retrieve the item.  It should always work.  Still, we'll
            // check for a blank oid -- this indicates record not found.  Set to 
            // insert mode and display error.

            getDataFromDoc = false;
            rule.setAttribute("inv_only_create_rule_oid", oid);
            rule.getDataFromAppServer();
            
            // DK IR T36000014974 Rel8.2 03/18/2013 Starts
            ruleType = rule.getAttribute("instrument_type_code");
            if (ruleType != null){
                  if (ruleType.equalsIgnoreCase(InstrumentType.LOAN_RQST)) {
                        if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LR_CREATE_RULES)      ) {
                              isReadOnly = false;
                        } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_LR_CREATE_RULES)) {
                              isReadOnly = true;
                        }     
                  } else if (ruleType.equalsIgnoreCase(InstrumentType.APPROVAL_TO_PAY)){
                        if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES)     ) {
                              isReadOnly = false;
                        } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_ATP_INV_CREATE_RULES)) {
                              isReadOnly = true;
                        }     
                  }
            } 
         // DK IR T36000014974 Rel8.2 03/18/2013 Ends
            
            
            if (rule.getAttribute("inv_only_create_rule_oid").equals("")) {
                  insertMode = true;
                  oid = "0";
                  getDataFromDoc = false;
            } else {
                  insertMode = false;
                  ruleType = rule.getAttribute("instrument_type_code");
            }
         

      }

      if (getDataFromDoc) {
            // Populate the user bean with the input doc.
            try {
                  rule.populateFromXmlDoc(doc.getComponent("/In"));
                  ruleType = rule.getAttribute("instrument_type_code");
                  
            } catch (Exception e) {
                  out.println("Contact Administrator: Unable to get ATP Creation Rule attributes "
                              + " for oid: " + oid + " " + e.toString());
            }

      }
      
      Hashtable secureParms = new Hashtable();
      String CurrentOrgOid = userSession.getOwnerOrgOid();
      String existingOrgOid = rule.getAttribute("owner_org_oid");

      if (InstrumentServices.isNotBlank(existingOrgOid)) {
            secureParms.put("owner_org_oid", existingOrgOid);

            if (!CurrentOrgOid.equals(existingOrgOid)) {
                  isReadOnly = true;
            }

      } else {
            secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
      }

      // This println is useful for debugging the state of the jsp.
      Debug.debug("retrieveFromDB: " + retrieveFromDB
                  + " getDataFromDoc: " + getDataFromDoc + " insertMode: "
                  + insertMode + " oid: " + oid);

      if(insertMode && !isReadOnly)
      {
            showSave = true;
            showSaveClose = true;
            showDelete = false;
      }else if(!insertMode && !isReadOnly){
            showSave = true;
        showSaveClose = true;
        showDelete = true;
      }else if(isReadOnly){
            showSave = false;
        showSaveClose = false;
        showDelete = false;
      }
      
%>

<%
      // Retrieve the data used to populate some of the dropdowns.
      StringBuilder sqlATP = new StringBuilder();
	  StringBuilder sqlLC = new StringBuilder();

      QueryListView queryListView = null;

      try {
            queryListView = (QueryListView) EJBObjectFactory
                        .createClientEJB(formMgr.getServerLocation(),
                                    "QueryListView");

            sqlATP.append("select t.template_oid, name ");
            sqlLC.append("select t.template_oid, name ");
            sqlATP.append(" from template t, instrument i ");
            sqlLC.append(" from template t, instrument i ");
 
            sqlATP.append(" where t.p_owner_org_oid in (?");
            sqlLC.append(" where t.p_owner_org_oid in (?");
           
            List<Object> sqlParamsLC = new ArrayList();
            List<Object> sqlParamsATP = new ArrayList();
            sqlParamsLC.add(userSession.getOwnerOrgOid());
            sqlParamsATP.add(userSession.getOwnerOrgOid());
            // Also include templates from the user's actual organization if using subsidiary access
            if (userSession.showOrgDataUnderSubAccess()) {
                  sqlATP.append(",?");
                  sqlLC.append(",?");
                  sqlParamsLC.add( userSession.getSavedUserSession().getOwnerOrgOid());
                  sqlParamsATP.add(userSession.getSavedUserSession().getOwnerOrgOid());
            }


            sqlATP.append(") and i.instrument_oid = t.c_template_instr_oid");
            sqlLC.append(") and i.instrument_oid = t.c_template_instr_oid");
            sqlATP.append(" and i.instrument_type_code = ?");
            sqlLC.append(" and i.instrument_type_code = ?");
            sqlATP.append(" order by ");
            sqlLC.append(" order by ");
            sqlATP.append(resMgr.localizeOrderBy("name"));
            sqlLC.append(resMgr.localizeOrderBy("name"));
 
            sqlParamsATP.add(InstrumentType.APPROVAL_TO_PAY);
            sqlParamsLC.add(InstrumentType.LOAN_RQST);
            queryListView.setSQL(sqlATP.toString(),sqlParamsATP);
            queryListView.getRecords();
            templatesDocATP = queryListView.getXmlResultSet();

            queryListView.setSQL(sqlLC.toString(),sqlParamsLC);
            queryListView.getRecords();
            templatesDocLC = queryListView.getXmlResultSet();
            

            //out.println("\n templatesDocATP="+templatesDocATP.toString());
            //out.println("\n templatesDocLC="+templatesDocLC.toString());
            
            Debug.debug("sqlATP.toString()" + sqlATP.toString());
            Debug.debug("sqlLC.toString()" + sqlLC.toString());

            
            Debug.debug("templatesDocATP.toString()" + templatesDocATP.toString());
            Debug.debug("templatesDocLC.toString()" + templatesDocLC.toString());

            sqlATP = new StringBuilder();
            sqlLC = new StringBuilder();
            sqlATP.append("select b.organization_oid, b.name ");
            sqlLC.append("select b.organization_oid, b.name ");
            sqlATP.append(" from corporate_org c, operational_bank_org b");
            sqlLC.append(" from corporate_org c, operational_bank_org b");
           	sqlATP.append(" where c.organization_oid = ?");
            sqlLC.append(" where c.organization_oid = ?");
            sqlATP.append(" and b.organization_oid in (c.a_first_op_bank_org, c.a_second_op_bank_org, ");
            sqlLC.append("  and b.organization_oid in (c.a_first_op_bank_org, c.a_second_op_bank_org, ");
            sqlATP.append(" c.a_third_op_bank_org, c.a_fourth_op_bank_org)");
            sqlLC.append(" c.a_third_op_bank_org, c.a_fourth_op_bank_org)");
             sqlATP.append(" order by ");
            sqlLC.append(" order by ");
            sqlATP.append(resMgr.localizeOrderBy("b.name"));
            sqlLC.append(resMgr.localizeOrderBy("b.name"));
            //out.println("\n sqlATP2="+sqlATP.toString());
            //out.println("\n sqlLC2="+sqlLC.toString());

                        
            Debug.debug("sqlATP.toString()" + sqlATP.toString());
            Debug.debug("sqlLC.toString()" + sqlLC.toString());

            
            queryListView.setSQL(sqlATP.toString(),new Object[]{userSession.getOwnerOrgOid()});
            queryListView.getRecords();
            opBankOrgsDocATP = queryListView.getXmlResultSet();

            queryListView.setSQL(sqlLC.toString(),new Object[]{userSession.getOwnerOrgOid()});
            queryListView.getRecords();
            opBankOrgsDocLC = queryListView.getXmlResultSet();

            //out.println("\n opBankOrgsDocATP="+opBankOrgsDocATP);
            //out.println("\n opBankOrgsDocLC="+opBankOrgsDocLC);
            Debug.debug(opBankOrgsDocATP.toString());
            Debug.debug(opBankOrgsDocLC.toString());

      } catch (Exception e) {
            e.printStackTrace();
      } finally {
            try {
                  if (queryListView != null) {
                        queryListView.remove();
                  }
            } catch (Exception e) {
                  System.out.println("error removing querylistview in CreationRuleDetail.jsp");
                             
            }
      }  


      try {
            // In insert mode we are passed a po upload definition oid.  Otherwise we
            // use the oid from the ATP Creation Rule webbean.
       if(true){
 //       if(TradePortalConstants.PO_UPLOAD_TEXT.equals(uploadFormat)){
            if (insertMode) {
                  
            	    definitionOid = request.getParameter("UploadDefinition");
                   String defOidFromPoUploadPage = doc.getAttribute("/Out/oid");

                   
                  if (InstrumentServices.isBlank(definitionOid)) {
                       
                	  if (InstrumentServices.isBlank(doc
                                    .getAttribute("/Out/oid"))) {
                              // In the case where we're in insert mode but have errors, the upload defn
                              // oid needs to be retrieved from the document rather than the request.
                              // We'll assume that a blank definition means we had errors.
                              definitionOid = doc.getAttribute("/In/InvOnlyCreateRule/invoice_def");
                                         
                        } else {
                              // We're coming from the PO Upload Definition page and the user has
                              // selected to create an ATP creation rule for a PO Upload Definition
                              // Get the PO upload def oid from the results of the mediator
             				definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(doc.getAttribute("/Out/oid"), userSession.getSecretKey());
                        }
                  } else {
                        // Otherwise, we found an encrypted oid from the request.
        				definitionOid = EncryptDecrypt.decryptStringUsingTripleDes(definitionOid, userSession.getSecretKey());
                  }
            } else {
                  // Not in insert mode, get the oid from the webbean.
                  definitionOid = rule.getAttribute("invoice_def");
            }
            
            invDefinition.setAttribute("inv_upload_definition_oid",definitionOid);
            invDefinition.getDataFromAppServer();

            fieldOptions = invDefinition.buildINVFieldOptionList(resMgr);
            fieldOptions += invDefinition.buildINVGoodsFieldOptionList(resMgr);
            defnName = invDefinition.getAttribute("name");
         }
 
      } catch (Exception e) {
            e.printStackTrace();
      }
%>

<%-- ********************* HTML for page begins here *********************  --%>


<%
      // Auto save the form when time-out if not readonly.  
      // (Header.jsp will check for auto-save setting of the corporate org).
      String autoSaveFlag = isReadOnly ? TradePortalConstants.INDICATOR_NO: TradePortalConstants.INDICATOR_YES;
%>


<%-- Body tag included as part of common header --%>
<jsp:include page="/common/Header.jsp">
      <jsp:param name="includeNavigationBarFlag"
            value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="includeErrorSectionFlag"
            value="<%=TradePortalConstants.INDICATOR_YES%>" />
      <jsp:param name="autoSaveFlag" value="<%=autoSaveFlag%>" />
</jsp:include>

<jsp:include page="/common/ButtonPrep.jsp" />

<div class="pageMain">
<div class="pageContent"><jsp:include
      page="/common/PageHeader.jsp">
      <jsp:param name="titleKey" value="CreationRuleDetail.CreationRule" />
      <jsp:param name="helpUrl" value="customer/lc_creation_rule.htm" />
</jsp:include> <%
  //cquinton 11/1/2012 ir#7015 remove return link
  String subHeaderTitle = "";
  subHeaderTitle = resMgr.getText( "CreationRuleDetail.NewCreationRule",   TradePortalConstants.TEXT_BUNDLE); 
  if(!insertMode){
        subHeaderTitle = rule.getAttribute("name");
  }
%>
    <jsp:include page="/common/PageSubHeader.jsp">
      <jsp:param name="title" value="<%= subHeaderTitle %>" />
    </jsp:include>      
            <jsp:include page="/common/ErrorSection.jsp" />

            <form name="InvOnlyCreateRuleDetailForm" method="post" id="InvOnlyCreateRuleDetailForm" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
                  <input type=hidden value="" name=buttonName>
                  <%
                        // Store values such as the userid, his security rights, and his org in a secure
                        // hashtable for the form.  The mediator needs this information.
                        secureParms.put("login_oid", userSession.getUserOid());
                        secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
                        secureParms.put("login_rights", loginRights);
                        secureParms.put("opt_lock", rule.getAttribute("opt_lock"));
                        secureParms.put("inv_only_create_rule_oid", oid);
                        secureParms.put("invoice_def", definitionOid);
                        secureParms.put("ownership_level", userSession.getOwnershipLevel());
                        
                        
                        
                  %>

                  <%=formMgr.getFormInstanceAsInputField("InvOnlyCreateRuleDetailForm", secureParms)%>
                  

<div class="formArea">
                  <div class="formContent">
                        <%=widgetFactory.createSectionHeader("CreationRuleDetail.General", "CreationRuleDetail.General")%>
                        <div class="columnLeft">
                        <input type="hidden" name="CreationRule" value="" id="CreationRule" />
                        <%-- Rel 9.2 XSS CID 11487 --%>
                        <input type=hidden value="<%=StringFunction.xssCharsToHtml(oid)%>" name="oid">
                        <%
                        // This block is to select the default ruleType when new rule is created
                        if( ruleType == null ){
                              // set the default value      
                              if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LR_CREATE_RULES) && 
                                                SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES)) {
                                          ruleType = InstrumentType.APPROVAL_TO_PAY;
                                    } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES)){
                                          ruleType = InstrumentType.APPROVAL_TO_PAY;
                                    }else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LR_CREATE_RULES)) {
                                          ruleType = InstrumentType.LOAN_RQST;
                                    }
                        } else {
                        	
                        	if (ruleType.equalsIgnoreCase(InstrumentType.APPROVAL_TO_PAY)&&!SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES))
                        		isReadOnly = true;
                        	
                        	if (ruleType.equalsIgnoreCase(InstrumentType.LOAN_RQST)&&!SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LR_CREATE_RULES))
                        		isReadOnly = true;
                         }
                     // DK IR T36000019543 Rel8.3 08/06/2013 starts 
                        if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LR_CREATE_RULES) && 
                                    SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES) && allow_loan_request && allow_approval_to_pay) {
                              ruleTypeSecurityAccFlag = "BOTH";
                        } else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_ATP_INV_CREATE_RULES) && allow_approval_to_pay)    {
                              ruleTypeSecurityAccFlag = "ATP_ONLY";
                        }else if (SecurityAccess.hasRights(loginRights,SecurityAccess.MAINTAIN_LR_CREATE_RULES) && allow_loan_request){
                              ruleTypeSecurityAccFlag = "LR_ONLY";
                        }else{
                              ruleTypeSecurityAccFlag = "NO_ACCESS";
                        }
                     // DK IR T36000019543 Rel8.3 08/06/2013 ends 

                        %>
                        <%-- Rel 9.2 XSS CID 11296, 11410 --%>
                        <input type="hidden" name="InstrumentType" value="<%=StringFunction.xssCharsToHtml(ruleType)%>" id ="InstrumentType">
                        <input type="hidden" name="InvoiceDef" value="<%=StringFunction.xssCharsToHtml(definitionOid)%>" id="InvoiceDef" />  
                        <%=widgetFactory.createLabel("CreationRuleDetail.RuleType","CreationRuleDetail.RuleType", isReadOnly, false,false, "")%>
                        <div class="formItem">
                <%
                
                if("BOTH".equals(ruleTypeSecurityAccFlag) || isReadOnly == true){
                             	 boolean ATPCreationRuleChecked = true;
                                          if (ruleType != null) {
                                                if (ruleType.equalsIgnoreCase(InstrumentType.LOAN_RQST)) {
                                                      ATPCreationRuleChecked = false;
                                                }
                                          }
               %>
                               	<%if(isReadOnly != true || InstrumentType.APPROVAL_TO_PAY.equalsIgnoreCase(ruleType)){%>
	                                   <%=widgetFactory.createRadioButtonField("CreationRule",
	                                   "CreationRule", "InvOnlyRuleDetail.ATPCreationRule", InstrumentType.APPROVAL_TO_PAY, ATPCreationRuleChecked,
	                                   isReadOnly, "onClick='setcreateRuleType(\""+ InstrumentType.APPROVAL_TO_PAY + "\");'", "")%> <br/>
    							  <%}%>
    							  
    							   <%if(isReadOnly != true || InstrumentType.LOAN_RQST.equalsIgnoreCase(ruleType)){%>
	                                   <%=widgetFactory.createRadioButtonField("CreationRule",
	                                   "LRCreationRule", "InvOnlyRuleDetail.LRCreationRule", InstrumentType.LOAN_RQST, !ATPCreationRuleChecked,
	                                   isReadOnly, "onClick='setcreateRuleType(\"" + InstrumentType.LOAN_RQST + "\");'", "")%>
                                 <%}%>
                        <%}else if("ATP_ONLY".equals(ruleTypeSecurityAccFlag)){ %>
                                          <%=widgetFactory.createRadioButtonField("InstrumentType", "CreationRule", "InvOnlyRuleDetail.ATPCreationRule", InstrumentType.APPROVAL_TO_PAY, true,isReadOnly, "onClick='setcreateRuleType(\"" + InstrumentType.APPROVAL_TO_PAY + "\");'", "")%> 
                        <%}else if("LR_ONLY".equals(ruleTypeSecurityAccFlag)){%>
                                          <%=widgetFactory.createRadioButtonField("InstrumentType", "LRCreationRule", "InvOnlyRuleDetail.LRCreationRule", InstrumentType.LOAN_RQST, true,isReadOnly, "onClick='setcreateRuleType(\"" + InstrumentType.LOAN_RQST + "\");'", "")%>
                        <%}%>
                        
                              
                  </div>            
                  <div style="clear:both"></div>
                              <%=widgetFactory.createTextField("Name",
                              "CreationRuleDetail.RuleName", rule.getAttribute("name"),
                              "35", isReadOnly, true, false, "class = 'char35'", "", "")%>
      
                              <%=widgetFactory.createTextField("description",
                              "CreationRuleDetail.Description",
                              rule.getAttribute("inv_only_create_rule_desc"), "35", isReadOnly, true,
                              false, "class = 'char35'", "", "")%>
            
                        </div><%-- END ColumnLeft --%>


                        <div class="columnRight">
                              <%=widgetFactory.createTextField("POUploadDefinition",
                              "InvOnlyRuleDetail.InvoiceDefinition", defnName, "22",
                              true)%>


                              <%
                                    optionsATP = ListBox.createOptionList(templatesDocATP,
                                                "TEMPLATE_OID", "NAME", rule.getAttribute("inv_only_create_rule_template"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    optionsLC = ListBox.createOptionList(templatesDocLC,
                                                "TEMPLATE_OID", "NAME", rule.getAttribute("inv_only_create_rule_template"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    

                                    // always provide the default text, since the field is not mandatory and user can deselect an already
                                    // selected template during an edit session 
                                    
                                    defaultText = resMgr.getText(
                                            "CreationRuleDetail.selectTemplate",
                                            TradePortalConstants.TEXT_BUNDLE);
                                    
                                    String tempOptions = "";
                                    if(InstrumentType.APPROVAL_TO_PAY.equalsIgnoreCase(ruleType)){
                                    	tempOptions = optionsATP;
                                    }else if(InstrumentType.LOAN_RQST.equalsIgnoreCase(ruleType)){
                                    	tempOptions = optionsLC;
                                    }
                                    
                                   //Making the drowp down as not required --ANZ Issue 57 -#T36000017604 -RPasupulati- Start.
                                    out.println(widgetFactory.createSelectField("TemplateChooser",
                                                "CreationRuleDetail.Template", defaultText, tempOptions, isReadOnly, false,
                                                false, "class = 'char30' ", "", ""));
                                    
                                  //ANZ Issue 57 -#T36000017604 -RPasupulati- Ends.
                                    
                                    
                                    optionsATPBB = ListBox.createOptionList(opBankOrgsDocATP,
                                                "ORGANIZATION_OID", "NAME",
                                                rule.getAttribute("op_bank_org_oid"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    optionsLCBB = ListBox.createOptionList(opBankOrgsDocLC,
                                                "ORGANIZATION_OID", "NAME",
                                                rule.getAttribute("op_bank_org_oid"),
                                                // Encrypt the OID
                                                userSession.getSecretKey());
                                    
                                    // jgadela 09/19/2013 Rel 8.3 IR T36000020905 - Issue 4 [START]- If there is only one branch for the customer,  preloading it. No default text
                                    Vector v = opBankOrgsDocATP.getFragments ("/ResultSetRecord");
                                    int numItems = v.size ();

                                    if (insertMode && numItems >1 ) {
                                    	     defaultText = resMgr.getText("CreationRuleDetail.selectBranch",
                                                      TradePortalConstants.TEXT_BUNDLE);
                                    } else {
                                          defaultText = "";
                                    }
                                    // jgadela 09/19/2013 Rel 8.3 IR T36000020905 - Issue 4 [END]

                                    
                                    out.println(widgetFactory.createSelectField("BankBranchChooser",
                                                "CreationRuleDetail.BankBranch", defaultText, optionsATPBB, isReadOnly, true,
                                                false, "class = 'char30'", "", ""));
                              %>
                              <%=widgetFactory.createTextField("CreationRuleDetail.CCY",
                              "CreationRuleDetail.CCY",
                              userSession.getBaseCurrencyCode(), "3", true, false, false,
                              "", "", "inline")%>

                              <%
                                    String displayMaximumAmount;
                                    // Don't format the amount if we are reading data from the doc
                                    /*if (doc.getDocumentNode("/In/LCCreationRule") == null){
                                          displayMaximumAmount = TPCurrencyUtility.getDisplayAmount(rule.getAttribute("maximum_amount"),userSession.getBaseCurrencyCode(), loginLocale);
                                    }else{
                                          displayMaximumAmount = rule.getAttribute("maximum_amount");
                                    }*/
                                    displayMaximumAmount = rule.getAttribute("inv_only_create_rule_max_amt");
                                    %>
                              <%//widgetFactory.createTextField("MaxAmount","CreationRuleDetail.MaximumAmount", displayMaximumAmount, "19", isReadOnly,false, false, "", "regExp:'[0-9]+'", "inline")%>
                              
                              <%=widgetFactory.createAmountField("MaxAmount","CreationRuleDetail.MaximumAmount",displayMaximumAmount,"",isReadOnly,false,false,"","","inline")%>
                        </div><%-- ColumnRight --%>
                  </div>

                  <%=widgetFactory.createSectionHeader(
                              "InvOnlyRuleDetail.GroupingCriteria",
                              "InvOnlyRuleDetail.GroupingCriteria")%>

                        <div class="formItem">
                        <%=resMgr.getText("InvOnlyRuleDetail.IncludeInvs",TradePortalConstants.TEXT_BUNDLE)%>
                        
                        
                        <%=widgetFactory.createTextField("MinDays", "",
                              rule.getAttribute("due_or_pay_min_days"), "3",
                              isReadOnly, false, false, "", "regExp:'[0-9]+'", "none")%>&nbsp;&nbsp;
                              <%=resMgr.getText("CreationRuleDetail.DaysLessThan",TradePortalConstants.TEXT_BUNDLE)%>
                        
                        <%=widgetFactory.createTextField("MaxDays", "",
                              rule.getAttribute("due_or_pay_max_days"), "3",
                              isReadOnly, false, false, "", "regExp:'[0-9]+'", "none")%>
                              
                              <%=resMgr.getText("CreationRuleDetail.DaysFrom",
                                                                              TradePortalConstants.TEXT_BUNDLE)%>
           </div>
<div style="clear:both"></div>
                  <div style="font-style:italic">     <%=widgetFactory.createLabel("CreationRuleDetail.InValue",
                              "CreationRuleDetail.InValue", isReadOnly, false, false, "")%></div>

                        <span class="asterisk">*</span>
                  <table class="formDocumentsTable" width="75%">
                  <thead>
                              <tr>
                              <th >
                              <%-- <%=widgetFactory.createLabel(
                              "CreationRuleDetail.PODataItem",
                              "CreationRuleDetail.PODataItem", true, false, false,
                              "")%>  --%>
                              <%=resMgr.getText("InvOnlyRuleDetail.InvDataItem",TradePortalConstants.TEXT_BUNDLE)%>
                              <%//= widgetFactory.createLabel("","CreationRuleDetail.PODataItem",isReadOnly, false, false, "", "") %></th>
                                    <th>&nbsp;</th>
                                    <th><%//= widgetFactory.createLabel("","CreationRuleDetail.Value",isReadOnly, false, false, "", "") %></b>                         
                                    <%=resMgr.getText("CreationRuleDetail.Value",TradePortalConstants.TEXT_BUNDLE)%></b></th>
                                    <th>&nbsp;</th>
                              </tr>

                  </thead>
                              <%
                                    // There are 6 rows of criteria.  Display each of them (except if we're in
                                    // readonly mode and neither the data nor value fields have values.
                                    String dataAttribute = "";
                                    String valueAttribute = "";

                                    for (int x = 1; x <= 6; x++) {
                                          dataAttribute = rule.getAttribute("inv_data_item" + x );
                                          valueAttribute = rule.getAttribute("inv_data_item_val" + x);
                                          if (isReadOnly && InstrumentServices.isBlank(dataAttribute)
                                                      && InstrumentServices.isBlank(valueAttribute)) {
                                                // Do nothing
                                          } else {
                              %>
                              <tr>

                                    <td width="30%" align="center">
                                          <%
                                                // For the data item dropdowns, we'll add a blank entry.  This allows
                                                            // the user to deselect a value.  We'll use the fieldOptions list
                                                            // and attempt to 'select' one of the entries before building the
                                                            // select html.
                                                            defaultText = " ";

                                                            options = invDefinition.selectFieldInList(fieldOptions,dataAttribute);
                                                            out.println(widgetFactory.createSelectField("InvoiceDataItem" + x, "",defaultText, options, isReadOnly));
                                          %>
                                    </td>

                                    <td width="15%" align="center"><%=widgetFactory.createLabel("CreationRuleDetail.Equal","CreationRuleDetail.Equal", isReadOnly, false,false, "")%></td>
                                    <td width="45%" align="center">
                                          <%-- A width of 40 allows for about 35 characters to be typed --%>
                                          <%--Rel8.3 Correcting the width to 35 characters --%>
                                          <%=widgetFactory.createTextArea("InvoiceDataItemValue" + x, "",valueAttribute,isReadOnly,false,false,"class = 'char25'","","","35")%>
                                                <%//=widgetFactory.createTextField("Value" + x,"",valueAttribute, "70", isReadOnly, false, false, "class = 'char25'", "", "")%>
                                    </td>

                                    <%
                                          // Display the "and" text for rows 1-5.
                                                      if (x != 7) {
                                    %>
                                    <td width="10%" align="center"><%=widgetFactory.createLabel("CreationRuleDetail.And","CreationRuleDetail.And", isReadOnly, false,false, "")%></td>
                                    <%
                                          } // end if x!=6
                                                } // end if isReadOnly and both data and value are blank
                                    %>
                              </tr>
                              <%
                                    } // end for
                              %>
                        </table>

                        <%
                              if (isViewInReadOnly) {
                                    link = "goToTradePortalHome";
                              } else {
                                    link = "goToRefDataHome";
                              }
                        %>
                  </div>
      </div>
<div style="clear:both";></div>

</div>

<% if("NO_ACCESS".equals(ruleTypeSecurityAccFlag)){
      
		showSave = false;
        showSaveClose = false;
        showDelete = false;
      
}%>

<div class="formSidebar" data-dojo-type="widget.FormSidebar"
      data-dojo-props="form: 'InvOnlyCreateRuleDetailForm'">
      <jsp:include page="/common/RefDataSidebar.jsp">
            <jsp:param name="showSaveButton" value="<%=showSave%>" />
            <jsp:param name="saveOnClick" value="none" />
            <jsp:param name="showSaveCloseButton" value="<%=showSaveClose%>" />
             <jsp:param name="saveCloseOnClick" value="none" />
            <jsp:param name="showDeleteButton" value="<%=showDelete%>" />
            <jsp:param name="cancelAction" value="goToRefDataHome" />
            <jsp:param name="showHelpButton" value="false" />
            <jsp:param name="showLinks" value="true" />
            <jsp:param name="buttonPressed" value="<%=buttonPressed%>" />
        <jsp:param name="error" value="<%= error%>" />   
      </jsp:include>
</div>
</form>

</div>
</div>

<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 Start --%>
<jsp:include page="/common/Footer.jsp">
   <jsp:param name="autoSaveFlag"           value="<%=autoSaveFlag%>" />
   <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%-- KMehta on 12 May 2014 @ Rel 90 for IR T36000025449 End --%>

<%
  //save behavior defaults to without hsm
  
 //(R9.2)XSS fix.Dont pass action for save and save and close buttons. Values for these actions are locally resolved in RefDataSideBarFooter.jsp
   //String saveOnClick = "common.setButtonPressed('SaveTrans','0');  document.forms[0].submit();";
  //String saveCloseOnClick = "common.setButtonPressed('SaveCloseTrans','0'); document.forms[0].submit();";
%>
<jsp:include page="/common/RefDataSidebarFooter.jsp" />
 
</body>
</html>
<%
  String focusField = "Name";
  if (!isReadOnly && InstrumentServices.isNotBlank(focusField)) {
%>
<script>
  require(["dijit/registry", "dojo/ready"],
    function(registry, ready ) {
      ready(function() {
        var focusFieldId = '<%= focusField %>';
        if ( focusFieldId ) {
          var focusField = registry.byId(focusFieldId);
          focusField.focus();
        }
      });
  });
</script>
<%
  }
%>

<%
      try {
            StringBuilder acs = new StringBuilder();
            String[] arr = null;


            if (optionsATP.length() > 0) {
                  int count = 0;
                  arr = optionsATP.split("</option>");
                  acs = new StringBuilder("data: [");
                  //    out.println("array length ="+arr.length);
                  for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                         int start = str.indexOf("=");
                         int end = str.indexOf(">");
                         String key = str.substring(start + 2, end - 1);
                         String val = str.substring(end + 1, str.length());
                         acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
      
                  }
                   acs.deleteCharAt(acs.length() - 1);
                   acs.append("]");
                   storeATP = acs.toString();
             }
            if (optionsLC.length() > 0) {
                  arr = optionsLC.split("</option>");
                  acs = new StringBuilder("data: [");
                  int count = 0;
                  //    out.println("array length ="+arr.length);
                  for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                         int start = str.indexOf("=");
                         int end = str.indexOf(">");
                         String key = str.substring(start + 2, end - 1);
                         String val = str.substring(end + 1, str.length());
 
                        acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
                        //          out.println(acs);

                  }
                  acs.deleteCharAt(acs.length() - 1);
                   acs.append("]");
                   storeLC = acs.toString();

            }

            if (optionsATPBB.length() > 0) {
                  int count = 0;
                  arr = optionsATPBB.split("</option>");
                  acs = new StringBuilder("data: [");
                  //    out.println("array length ="+arr.length);
                  for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                        int start = str.indexOf("=");
                         int end = str.indexOf(">");
                         String key = str.substring(start + 2, end - 1);
                         String val = str.substring(end + 1, str.length());
                         acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
                   }
                   acs.deleteCharAt(acs.length() - 1);
                   acs.append("]");
                   storeATPBB = acs.toString();
             }

            if (optionsLCBB.length() > 0) {
                  arr = optionsLCBB.split("</option>");
                  acs = new StringBuilder("data: [");
                  int count = 0;
                   for (count = 0; count < arr.length - 1; count++) {
                        String str = arr[count];
                         int start = str.indexOf("=");
                         int end = str.indexOf(">");
                         String key = str.substring(start + 2, end - 1);
                         String val = str.substring(end + 1, str.length());
                         acs.append("{name:\"" + val + "\", id:\"" + key + "\"}");
                        acs.append(",");
     
                  }
                   acs.deleteCharAt(acs.length() - 1);
                   acs.append("]");
                   storeLCBB = acs.toString();
             }

      } catch (Exception ex) {
            ex.printStackTrace();
      }
%>

<script>

function setcreateRuleType(ruleType) {
	
      console.log('started setcreateRuleType');
      document.getElementById('InstrumentType').value = ruleType;
       var tcStore = new dojo.store.Memory({
    });
       var bbStore = new dojo.store.Memory({
   });
      
      if (ruleType == '<%= InstrumentType.LOAN_RQST%>'){
            console.log("rule type = LRQ");
            tcStore = new dojo.store.Memory({
              <%=storeLC%>
          });
             bbStore = new dojo.store.Memory({
               <%=storeLCBB%>
         });
      } else if (ruleType == '<%= InstrumentType.APPROVAL_TO_PAY%>'){
            console.log("rule type = ATP");
            tcStore = new dojo.store.Memory({
              <%=storeATP%>
          });
             bbStore = new dojo.store.Memory({
               <%=storeATPBB%>
         });      
      }
      
      var ATPstore = new dojo.store.Memory({
        data: [
            {name:"ATP1", id:"A1"},
            {name:"ATP2", id:"A2"},
            {name:"ATP3", id:"A3"},
            {name:"ATP4", id:"A4"},
            {name:"ATP5", id:"A5"}]
    });

var tc = dijit.byId("TemplateChooser");
<%-- var bb = dijit.byId("BankBranchChooser"); --%>
tc.attr('value','');
<%-- bb.attr('value','');// Nar IR-T36000014357 --%>
tc.store = tcStore;
<%-- bb.store = bbStore; // Nar IR-T36000014357 --%>
console.log('completed setcreateRuleType');
}

</script>
<%
      // Finally, reset the cached document to eliminate carryover of 
      // information to the next visit of this page.
      formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
