
<%--
**********************************************************************************
  Direct Debit Transactions Home

  Description:  
     This page is used as the main Direct Debits page.

**********************************************************************************
--%> 

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*, com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*, com.ams.tradeportal.html.*,
                 com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.busobj.util.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
 
<%


   StringBuffer      dynamicWhereClause = new StringBuffer();
   StringBuffer      onLoad             = new StringBuffer();
   Hashtable         secureParms        = new Hashtable();
   String            helpSensitiveLink  = null;
   String            userSecurityRights = null;
   String            userSecurityType   = null;
   String            current2ndNav      = null;
   String            userOrgOid         = null;
   String            userOid            = null;
   String            formName           = "";
   StringBuffer      newLink            = new StringBuffer();
   StringBuffer orgList = new StringBuffer();
   boolean           canViewDirectDebitInstruments = false;  
   WidgetFactory widgetFactory = new WidgetFactory (resMgr);
   boolean orgListDropDown = false;

   String 	     loginLocale 		  = null;
   
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");
                                         
   StringBuffer newSearchReceivableCriteria = new StringBuffer();                                     

   //cquinton 1/18/2013 remove old close action behavior

   userSecurityRights           = userSession.getSecurityRights();
   userSecurityType             = userSession.getSecurityType();
   userOrgOid                   = userSession.getOwnerOrgOid();
   userOid                      = userSession.getUserOid();

   CorporateOrganizationWebBean org = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
   org.getById(userOrgOid);
   
   canViewDirectDebitInstruments = SecurityAccess.hasRights(userSecurityRights,  SecurityAccess.ACCESS_DIRECTDEBIT_AREA); 
   
   
   // Now perform data setup for whichever is the current tab.  Each block of
   // code sets tab specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current tab as its context
   PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");



	   formName     = "DirectDebit-TransactionForm";
       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/invoices.htm",  resMgr, userSession);
       %>
       <%@ include file="/receivables/fragments/DirectDebit-TranSearchErrorParms.frag" %>
 

<%-- ********************* HTML for page begins here *********************  --%>


			<form id="ExistingDirectDebitsForm" name="ExistingDirectDebitsForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">

  			<div id="ExistingDirectDebitsSearchDialogContent" class="dialogContent fullwidth">      		
	  				<input type=hidden value="" name=buttonName>

					
							
							
							<%-- ************** Data retrieval page setup begins here ****************  --%>
							
							<%
							   final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", TradePortalConstants.TEXT_BUNDLE);
							   final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork", TradePortalConstants.TEXT_BUNDLE);
							   final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork",  TradePortalConstants.TEXT_BUNDLE);
							   String			 instrumentId 		   = "";
							   String			 refNum			       = "";
							   String			 amountFrom     	   = "";
							   String			 amountTo              = "";
							   String 			 currency              = "";
							   String 			 dayFrom 			   = "";
							   String 			 monthFrom 			   = "";
							   String 			 yearFrom 			   = "";
							   String 			 dayTo 				   = "";
							   String 			 monthTo 			   = "";
							   String 			 yearTo 			   = "";
							   String			 searchType		       = null;
							   String 			 creditAcct		       = null;
							   Vector            instrumentTypes       = null;
							   String 			 link 			       = null;
							   String 			 linkText 		       = "";   
							   StringBuffer 	 newSearchCriteria     = new StringBuffer();                                                          
							
							   DocumentHandler   hierarchyDoc                 = null;
							   StringBuffer      dropdownOptions              = new StringBuffer();
							   int               totalOrganizations           = 0;
							   StringBuffer      extraTags                    = new StringBuffer();
							            
							   String            userDefaultWipView           = null; 
							   String            selectedWorkflow             = null;
							   String            selectedStatus               = "";
							   String            selectedOrg                  = null;  
							   String            linkParms                    = "";   
							   
							   DataGridFactory dgFac = new DataGridFactory(resMgr, userSession, formMgr, response);
							   
							   String gridHtml = "", gridLayout = "";
							   userDefaultWipView = userSession.getDefaultWipView();
							   loginLocale = userSession.getUserLocale();
							   
							   // Construct the Corporate Organization
							   CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
							   corpOrg.getById(userOrgOid);
							   
							   // Store the user's oid and security rights in a secure hashtable for the form
							   secureParms.put("instrumentType",InstrumentType.DIRECT_DEBIT_INSTRUCTION);
							   secureParms.put("bankBranch",corpOrg.getAttribute("first_op_bank_org")); 
							   

							   secureParms.put("UserOid",userSession.getUserOid());
							   secureParms.put("SecurityRights",userSecurityRights); 
							   secureParms.put("clientBankOid",userSession.getClientBankOid()); 
							   secureParms.put("ownerOrg",userSession.getOwnerOrgOid());
							
							   
							   // This is the query used for populating the workflow drop down list; it retrieves
							   // all active child organizations that belong to the user's current org.
							   StringBuilder sqlQuery = new StringBuilder();
							   sqlQuery.append("select organization_oid, name");
							   sqlQuery.append(" from corporate_org");
							   sqlQuery.append(" where activation_status = ?");
							   sqlQuery.append(" start with organization_oid = ?");
							   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
							   sqlQuery.append(" order by ");
							   sqlQuery.append(resMgr.localizeOrderBy("name"));
							   List<Object> sqlParamsDd = new ArrayList();
							   sqlParamsDd.add(TradePortalConstants.ACTIVE);
							   sqlParamsDd.add(userOrgOid);
							
							   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false,sqlParamsDd);
								  
							   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
							   totalOrganizations = orgListDoc.size();
							   
							   // Now perform data setup for whichever is the current tab.  Each block of
							   // code sets tab specific parameters (for data retrieval or dropdowns) as
							   // well as setting common things like the appropriate help link and formName
							
							
							       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);
							       
							       // Determine the organization to select in the dropdown
							       selectedOrg = StringFunction.xssCharsToHtml(request.getParameter("historyOrg"));
							       if (selectedOrg == null)
							       {
							       	  selectedOrg = (String) session.getAttribute("historyOrg");
							       	  if (selectedOrg == null)
							          {
							            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
							          }
							       }
							
							   	  session.setAttribute("historyOrg", selectedOrg);
								  selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
							       
							   	  String newSearch = StringFunction.xssCharsToHtml(request.getParameter("NewSearch"));
								  String newDropdownSearch = StringFunction.xssCharsToHtml(request.getParameter("NewDropdownSearch"));
							      Debug.debug("New Search is " + newSearch);
							      Debug.debug("New Dropdown Search is " + newDropdownSearch);
							   	  searchType   = StringFunction.xssCharsToHtml(request.getParameter("SearchType"));
							   		
							   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
							      	{
							         	// Default search type for instrument status on the transactions history page is ACTIVE.
							         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
							     	}
							   		
							   		if (searchType == null) 
								    {
							   		  	 searchType = TradePortalConstants.SIMPLE;
							        	
							      	}	
							      
							         if (searchType.equals(TradePortalConstants.ADVANCED))
							      	{
							         	linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
							         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
							         	linkText   = resMgr.getText("InstSearch.BasicSearch", TradePortalConstants.TEXT_BUNDLE);
							
							         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "advanced", resMgr, userSession);
							      	}
							      	else
							      	{
							        	linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
							         	link       = formMgr.getLinkAsUrl("goToReceivablesManagement", linkParms, response);
							         	linkText   = resMgr.getText("InstSearch.AdvancedSearch", TradePortalConstants.TEXT_BUNDLE);
							
							         	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/inst_hist.htm", "basic", resMgr, userSession);
							     	 }
							
								 // Determine the organizations to select for listview
							     DocumentHandler orgDoc = null;
							        	 
							
								         for (int i = 0; i < totalOrganizations; i++)
								         {
								    	    orgDoc = (DocumentHandler) orgListDoc.get(i);
							        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
							            	if (i < (totalOrganizations - 1) )
							            		{
								             		 orgList.append(",");
							    	       		}
							        	 }
								 
								 
								 if (selectedOrg.equals(ALL_ORGANIZATIONS))
							     	 {
							        	 // Build a comma separated list of the orgs
									     dynamicWhereClause.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
							      	 }
							      	 else
							     	 {
							        	 dynamicWhereClause.append(" and i.a_corp_org_oid = "+  selectedOrg);
							      	 }
							     	 
							     	 // Based on the statusType dropdown, build the where clause to include one or more
							     	 // instrument statuses.
							     	 Vector statuses = new Vector();
							     	 int numStatuses = 0;
							
							      	selectedStatus = StringFunction.xssCharsToHtml(request.getParameter("instrStatusType"));
							        if (selectedStatus == null) 
							     	 {
							       		  selectedStatus = (String) session.getAttribute("instrStatusType");
							     	 }
							
							      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
							      	{
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
							      	}
							
							      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
							      	{
							       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
							        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
							         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
							      	}
							
							      	else // default is ALL (actually not all since DELeted instruments
							        	{
							        	 selectedStatus = TradePortalConstants.STATUS_ALL;
							      	}
							
							     	session.setAttribute("instrStatusType", selectedStatus);
							     	
							     	if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
							     	{
							        	 dynamicWhereClause.append(" and i.instrument_status in (");
								         numStatuses = statuses.size();
							    	     for (int i=0; i<numStatuses; i++)
							    	     {
							        	     dynamicWhereClause.append("'");
							            	 dynamicWhereClause.append( (String) statuses.elementAt(i) );
							             	 dynamicWhereClause.append("'");
								             if (i < numStatuses - 1)
								             {
							               		dynamicWhereClause.append(", ");
							             	 }
							         	 }
							         	 dynamicWhereClause.append(") ");
							      	}
							%>


					<%@ include file="/dialog/fragments/ExistingDirectDebitSearch.frag"%>
					
					<input type="hidden" name="mode" value="CREATE_NEW_INSTRUMENT"/>
					<input type="hidden" name="copyType" value="Instr"/>
					<input type="hidden" name="bankBranch" />
					<input type="hidden" name="instrumentType" value="DDI"/>
					<input type="hidden" name="transactionType" value=""/>
					<input type="hidden" name="copyInstrumentOid" />

					<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm", secureParms) %>

  				</div>
			</form>
    

	<%@ include file="/dialog/fragments/ExistingDirectDebitSearchFooter.frag"%>
	

	<script type="text/javascript">
		var rowKeys = "",
			instType = "";
	
		function selectDirectDebit(bankBranchArray, newInstrumentType){
			rowKeys = getSelectedGridRowKeys("existingDirectDebitSearchGridId");
			instType= newInstrumentType;
			
			if(rowKeys == ""){
				alert("Please pick a record and click 'Select'");
				return;
			}
		
    if ( bankBranchArray.length == 1 ) {
		createNewDDBankBranchSelected(bankBranchArray[0]);
	} else {
			require(["t360/dialog"], function(dialog ) {
		      dialog.open('bankBranchSelectorDialog', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
		                  'bankBranchSelectorDialog.jsp',
		                  null, null, <%-- parameters --%>
		                  'select', this.createNewDDBankBranchSelected);
		    });
		}
	}
		
		function createNewDDBankBranchSelected(bankBranchOid) {
			
			if ( document.getElementsByName('ExistingDirectDebitsForm').length > 0 ) {
		        var theForm = document.getElementsByName('ExistingDirectDebitsForm')[0];
		      	theForm.copyInstrumentOid.value=rowKeys;
		      	theForm.bankBranch.value=bankBranchOid;
		    		  
		      	theForm.submit();
		      } else {
		        	alert('Problem in createNewInstrument: cannot find ExistingDirectDebitsForm');
		      }
		}  	

		function closeDirectDebitDialog(){
			require(["t360/dialog"], function(dialog) {
				dialog.hide('copyDirectDebitDialog');
			});
		}
		
		function shuffleFilterDDI_DLG_(linkValue){
	  		console.log("inside shuffleFilter(): "+linkValue);
	  		
	  		require(["dojo/dom","dijit/registry","dojo/domReady!"],function(dom,registry){
  	  			if(linkValue=='Advance'){
  	  				dom.byId("advanceDirectDebitFilter").style.display='block';
  	  				dom.byId("basicDirectDebitFilter").style.display='none';
  	  				
  	  				<%-- clearing Basic Filter --%>
  	  				registry.byId("DDI_DLG_InstrumentIdBasic").value='';
  	        		registry.byId("DDI_DLG_CreditAcctBasic").value='';
  	 	        	registry.byId("DDI_DLG_ReferenceBasic").value='';
  	  			}
  	  			
  	  			if(linkValue=='Basic'){
  	  				dom.byId("advanceDirectDebitFilter").style.display='none';
  	  				dom.byId("basicDirectDebitFilter").style.display='block';
  	  				
  	  				<%-- clearing Advance Filter --%>
  	  				registry.byId("DDI_DLG_InstrumentIdAdvance").value='';
  		        	registry.byId("DDI_DLG_CreditAcctAdvance").value='';
  		        	registry.byId("DDI_DLG_ReferenceAdvance").value='';
  		        	registry.byId("DDI_DLG_AmountTo").value='';
  		        	registry.byId("DDI_DLG_AmountFrom").value='';
  		        	
  		        	this.resetDatesDDI_DLG_();
  	  			}
    		});
		}
		require(["dojo/dom","dojo/domReady!"],function(dom){
			dom.byId("advanceDirectDebitFilter").style.display='none';
		});
	</script>	
<%
   formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
