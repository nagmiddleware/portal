
<%--
 * Phrasedetails dialog to get new phrase name
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>



<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<div class="dialogContent padded">
  <form name="saveAsForm"> 
  
  
  <%= widgetFactory.createTextField( "newProfileName", "PhraseDetail.PhraseName", "", "25", false, true, false,  "", "","" ) %>                          
 	   <div> 
 	   <table><tr> <td>
 	   <div class="formItem">
 	   <button data-dojo-type="dijit.form.Button" type="button"  onClick="submitSaveAs()" id="saveAs" >
        		<%=resMgr.getText("SecurityProfileDetail.SaveAsTitle", TradePortalConstants.TEXT_BUNDLE)%>
         		     		</button>
     		</div>
     		</td>
 	   <td>
 	    <div class="formItem">
 	   
 	   
 	     <button data-dojo-type="dijit.form.Button" type="button" id="PhrashCloseButton" onClick="hideDialog('saveAsDialogId')"  ><%= resMgr.getText("common.cancel", 
                             TradePortalConstants.TEXT_BUNDLE) %>
                         
                             </button>
      
 	  
 	   </div>
 	   </td></tr> </table>
 	
        </div>
  </form> 
   <%= widgetFactory.createHoverHelp("saveAs", "SaveAsHoverText")%>
 	  <%= widgetFactory.createHoverHelp("PhrashCloseButton", "CloseHoverText")%>
 	  <%= widgetFactory.createHoverHelp("saveAsDialogIdId", "CloseHoverText")%>
  
</div>