<%--
*******************************************************************************
  Existing Payments Search Page

  Description:
     This page is used as the payments search grid page.

*******************************************************************************
--%>

<%--
 *	   Dev Owner: Sandeep
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,java.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"            scope="session"></jsp:useBean>
<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>


<%-- ********************* JavaScript for page begins here *********************  --%>



<%-- ************** Data retrieval page setup begins here ****************  --%>

<%


	WidgetFactory widgetFactory = new WidgetFactory(resMgr);


	StringBuffer      onLoad                = new StringBuffer();
   Hashtable         secureParms           = new Hashtable();
   String            helpSensitiveLink     = null;
   String            current2ndNav      = null;
   String            formName              = null;

   String            userOrgOid            = null;
   String            userOid               = null;
   String            userSecurityRights    = null;
   String            userSecurityType      = null;
   // Set the current primary navigation
   userSession.setCurrentPrimaryNavigation("NavigationBar.Transactions");

   //cquinton 1/18/2013 remove old close action behavior

   // Retrieve the user's security rights, security type, organization oid, and user oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();

   //CR-586 Vshah [BEGIN]
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();

   String confInd = "";
   if (userSession.getSavedUserSession() == null)
       confInd = thisUser.getAttribute("confidential_indicator");
   else
   {
       if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
           confInd = TradePortalConstants.INDICATOR_YES;
       else
           confInd = thisUser.getAttribute("subsid_confidential_indicator");
   }
   if (StringFunction.isBlank(confInd))
      confInd = TradePortalConstants.INDICATOR_NO;
   //CR-586 Vshah [END]


   // This is the query used for populating the workflow drop down list; it retrieves
   // all active child organizations that belong to the user's current org.


   // Now perform data setup for whichever is the current secondary navigation.  Each block of
   // code sets page specific parameters (for data retrieval or dropdowns) as
   // well as setting common things like the appropriate help link and formName


   // Set the performance statistic object to have the current page as its context
    PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");

   // ***********************************************************************
   // Data setup for the Cash Mgmt. Pending Transactions page
   // ***********************************************************************

   	helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/payment_trans_history.htm",  resMgr, userSession);
   	Debug.debug("*** form " + formName);
%>

<%-- ********************* HTML for page begins here *********************  --%>


	
			<form id="ExistingPaymentsForm" name="ExistingPaymentsForm" method="POST" data-dojo-type="dijit.form.Form" action="<%= formMgr.getSubmitAction(response) %>">
				<div id="ExistingPaymentSearchDialogContent" class="dialogContent fullwidth">      

					<input type=hidden value="" name=buttonName>

					<%-- ************** Data retrieval page setup begins here ****************  --%>

					<%
					   final String      ALL_ORGANIZATIONS            = resMgr.getText("AuthorizedTransactions.AllOrganizations", TradePortalConstants.TEXT_BUNDLE);
					   final String      ALL_WORK                     = resMgr.getText("PendingTransactions.AllWork", TradePortalConstants.TEXT_BUNDLE);
					   final String      MY_WORK                      = resMgr.getText("PendingTransactions.MyWork", TradePortalConstants.TEXT_BUNDLE);
					   String			 instrumentId 		   = "";
					   String			 refNum			   = "";
					   String			 amountFrom     	   = "";
					   String			 amountTo                  = "";
					   String 			 currency                  = "";
					   String			 searchCondition 	   = TradePortalConstants.SEARCH_ALL_INSTRUMENTS;
					   String			 searchType		   = null;
					   String 			 loginLocale 		   = userSession.getUserLocale();
					   String 			 instrumentType		   = null;
					   Vector            		 instrumentTypes           = null;
					   String 			 options                   = "";
					   String 			 otherParty                = "";
					   String 			 link 			   = null;
					   String 			 linkText 		   = null;
					   boolean 			 pmtD_Organization  =false;
					   StringBuffer 		 newSearchCriteria         = new StringBuffer();

					   DocumentHandler   hierarchyDoc                 = null;
					   StringBuffer      dynamicWhereClause           = new StringBuffer();
					   StringBuffer      dropdownOptions              = new StringBuffer();
					   int               totalOrganizations           = 0;
					   StringBuffer      extraTags                    = new StringBuffer();
					   StringBuffer      newLink                      = new StringBuffer();
					   StringBuffer      newUploadLink                = new StringBuffer();

					   String            userDefaultWipView           = null;
					   String            selectedWorkflow             = null;
					   String            selectedStatus               = "";

					   String linkParms   = null;
					   StringBuffer dynamicWhere = dynamicWhereClause;
					   String            selectedOrg                  = null;
					   String loginRights = userSecurityRights; // PPRAKASH IR NGUJ013070808

					   userDefaultWipView           = userSession.getDefaultWipView();

					   StringBuilder sqlQuery = new StringBuilder();
					   sqlQuery.append("select organization_oid, name");
					   sqlQuery.append(" from corporate_org");
					   sqlQuery.append(" where activation_status = ?");
					   sqlQuery.append(" start with organization_oid =? ");
					   sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
					   sqlQuery.append(" order by ");
					   sqlQuery.append(resMgr.localizeOrderBy("name"));

					   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false,TradePortalConstants.ACTIVE,userOrgOid);

					   Vector orgListDoc = hierarchyDoc.getFragments("/ResultSetRecord");
					   totalOrganizations = orgListDoc.size();

					   // Now perform data setup for whichever is the current secondary navigation.  Each block of
					   // code sets specific parameters (for data retrieval or dropdowns) as
					   // well as setting common things like the appropriate help link and formName


					   // Set the performance statistic object to have the current secondary navigation as its context


					       formName     = "ExistingPaymentsForm";
					       helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/cashManagement.htm",  resMgr, userSession);


					       // Determine the organization to select in the dropdown
					       selectedOrg = StringFunction.xssCharsToHtml(request.getParameter("historyOrg"));
					       if (selectedOrg == null)
					       {
					       	  selectedOrg = (String) session.getAttribute("historyOrg");
					       	  if (selectedOrg == null)
					          {
					            selectedOrg = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());

					          }
					        }

					      	session.setAttribute("historyOrg", selectedOrg);
						selectedOrg = EncryptDecrypt.decryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());

					   	String newSearch = StringFunction.xssCharsToHtml(request.getParameter("NewSearch"));
						String newDropdownSearch = StringFunction.xssCharsToHtml(request.getParameter("NewDropdownSearch"));

					   	searchType   = StringFunction.xssCharsToHtml(request.getParameter("SearchType"));

					   		if ((newSearch != null) && (newSearch.equals(TradePortalConstants.INDICATOR_YES)))
					      	{
					         	// Default search type for instrument status on the transactions history page is ACTIVE.
					         	session.setAttribute("instrStatusType", TradePortalConstants.STATUS_ACTIVE);
					     	}

					   		if (searchType == null)
						    {
					   				  searchType = TradePortalConstants.SIMPLE;
					        
					      	}
					      	
						 // Determine the organizations to select for listview
					     	 if (selectedOrg.equals(ALL_ORGANIZATIONS))
					     	 {
					        	 // Build a comma separated list of the orgs
							 DocumentHandler orgDoc = null;
					        	 StringBuffer orgList = new StringBuffer();

						         for (int i = 0; i < totalOrganizations; i++)
						         {
						    	        orgDoc = (DocumentHandler) orgListDoc.get(i);
					        		orgList.append(orgDoc.getAttribute("ORGANIZATION_OID"));
					            		if (i < (totalOrganizations - 1) )
					            		{
						             		 orgList.append(", ");
					    	       		}
					        	 }

					         	 dynamicWhere.append(" and i.a_corp_org_oid in (" + orgList.toString() + ")");
					      	 }
					      	 else
					     	 {
					        	 dynamicWhere.append(" and i.a_corp_org_oid = "+  selectedOrg);
					      	 }
					     	 // Based on the statusType dropdown, build the where clause to include one or more
					     	 // instrument statuses.
					     	 Vector statuses = new Vector();
					     	 int numStatuses = 0;

					      	selectedStatus = StringFunction.xssCharsToHtml(request.getParameter("instrStatusType"));

					     	 if (selectedStatus == null)
					     	 {
					       		  selectedStatus = (String) session.getAttribute("instrStatusType");
					     	 }

					      	if (TradePortalConstants.STATUS_ACTIVE.equals(selectedStatus) )
					      	{
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
					      	}

					      	else if (TradePortalConstants.STATUS_INACTIVE.equals(selectedStatus) )
					      	{
					       		 statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
					        	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
					         	 statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
					      	}

					      	else // default is ALL (actually not all since DELeted instruments
					           // never show up (handled by the SQL in the listview XML)
					      	{
					        	 selectedStatus = TradePortalConstants.STATUS_ALL;
					      	}

					     	session.setAttribute("instrStatusType", selectedStatus);


					     	if (!TradePortalConstants.STATUS_ALL.equals(selectedStatus))
					     	{
					        	 dynamicWhere.append(" and i.instrument_status in (");

						         numStatuses = statuses.size();

					    	     for (int i=0; i<numStatuses; i++)
					    	     {
					        	     dynamicWhere.append("'");
					            	 dynamicWhere.append( (String) statuses.elementAt(i) );
					             	 dynamicWhere.append("'");

						             if (i < numStatuses - 1)
						             {
					               		dynamicWhere.append(", ");
					             	 }
					         	 }

					         	 dynamicWhere.append(") ");
					      	}

					      	//CR-586 Vshah [BEGIN]---
					      	//For Users where the new User Profile level "Access to Confidential Payment Instruments/Templates" Indicator is not selected (set to No),
						//on the Cash Management/Payment Transactions listviews, no transactions will appear that are designated as Confidential Payments
						if (TradePortalConstants.INDICATOR_NO.equals(confInd))
						{
						    //IAZ IR-RDUK091546587 Use INSTRUMENTS table with Instrument History View for Conf Indicator Check
							dynamicWhereClause.append(" and ( i.confidential_indicator = 'N' OR ");
							dynamicWhereClause.append(" i.confidential_indicator is null) ");
						}
					  	//CR-586 Vshah [END]
					%>

					<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-TransType.frag" %>
					<%@ include file="/cashManagement/fragments/CashMgmt-TranSearch-SearchParms.frag" %>



				<%-- ********************* HTML for page begins here *********************  --%>

				    <%@ include file="/dialog/fragments/ExistingPaymentSearch.frag" %>

					<%
						  secureParms.put("login_oid", userSession.getUserOid());
						  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
						  secureParms.put("login_rights", loginRights);

					    secureParms.put("userOid",userSession.getUserOid());
					    secureParms.put("securityRights",userSession.getSecurityRights());
					    secureParms.put("clientBankOid",userSession.getClientBankOid());
					    secureParms.put("ownerOrg",userSession.getOwnerOrgOid());

					      if (userSession.getSavedUserSession() == null) {
					        secureParms.put("AuthorizeAtParentViewOrSubs", TradePortalConstants.INDICATOR_YES);
					      }
					%>
					<%= formMgr.getFormInstanceAsInputField("NewInstrumentForm", secureParms) %>
  				</div><%-- end of formContentNoSidebar div --%>
			</form>




<%	String gridLayout = dgFactory.createGridLayout("existingPaymentSearchDataGridId","ExistingPaymentSearchDataGrid"); %>

<script type="text/javascript">
	var formName = 'ExistingPaymentsForm',
	buttonName = '<%=TradePortalConstants.BUTTON_SELECT%>',
	rowKeys = "",
	instType = "";

	function selectPayment(bankBranchArray, newInstrumentType){

	<%--get array of rowkeys from the grid--%>
	rowKeys = getSelectedGridRowKeys("existingPaymentSearchDataGridId");
	var items=getSelectedGridItems("existingPaymentSearchDataGridId");

	if(rowKeys == ""){
		alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("InstSearch.Select",TradePortalConstants.TEXT_BUNDLE)) %>');
		return;
	}
	
    if ( bankBranchArray.length == 1 ) {
		createNewPaymentBankBranchSelected(bankBranchArray[0]);
	} else {
		require(["t360/dialog"], function(dialog ) {
	      dialog.open('bankBranchSelectorDialog', '<%=resMgr.getText("bankBranchSelectorDialog.title", TradePortalConstants.TEXT_BUNDLE)%>',
	                  'bankBranchSelectorDialog.jsp',
	                  null, null, <%-- parameters --%>
	                  'select', this.createNewPaymentBankBranchSelected);
	    });
	 }
	}

	function createNewPaymentBankBranchSelected(bankBranchOid) {
		if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
	        var theForm = document.getElementsByName('NewInstrumentForm')[0];
	      	theForm.copyInstrumentOid.value=rowKeys;
	      	theForm.bankBranch.value=bankBranchOid;
	      	theForm.mode.value='CREATE_NEW_INSTRUMENT';
	      	theForm.copyType.value='Instr';
	      	theForm.instrumentType.value=instType;
	      	theForm.transactionType.value=' ';

	      	theForm.submit();
	      } else {
	        	alert('Problem in createNewInstrument: cannot find ExistingPaymentsForm');
	   }
	}

	function closePaymentDialog(){
		require(["t360/dialog"], function(dialog) {
			dialog.hide('copyPaymentsDialog');
		});
	}

	require(["t360/datagrid", "dojo/domReady!"],function( t360grid ) {
		var gridLayout = <%= gridLayout %>,<%-- ALL_ORGANIZATIONS --%>
		<%-- SSikhakolli - Rel 9.0 IR# T36000028855 06/16/2014 - Begin  --%>
		<%-- Added 'selectedStatuses' to init Search criteria --%>
		initSearchParms = "userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>&confInd=<%=confInd%>&status=<%=selectedStatus%>";
		<%-- SSikhakolli - Rel 9.0 IR# T36000028855 06/16/2014 - End  --%>
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("ExistingPaymentSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	  	var grid = t360grid.createDataGrid("existingPaymentSearchDataGridId",viewName,gridLayout,initSearchParms);
		grid.set('autoHeight',10); 
	});

	function filterPaymentsOnEnter(fieldId, filterType){
		<%-- 
	  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
	  	 --%>
	  	require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	filterPayments(filterType);
		        }
		    });
		});
	}

  	function filterPaymentsChange() {
  		require(["dojo/dom","dojo/domReady!"],function(dom) {
  		var filterType = "Basic";
		if (dom.byId("advancdePaymentFilter").style.display == 'block') {
		   filterType = "Advanced";
	  	}
		filterPayments(filterType);
  	});
	}

  	function filterPayments(buttonId) {
  		require(["dojo/dom","dojo/domReady!"],function(dom) {
	  		var organization = "";
			var searchParms="";

			<% if (pmtD_Organization) {%>
			organization = dijit.byId("pmtD_Organization").attr('value');
			<%}else { %>
			organization = "<%=EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey())%>";			
			<%}%>
			var status = dijit.byId("pmtD_instrStatusType").attr('value');
	         searchParms="userOrgOid="+organization;
		       	searchParms+="&confInd=<%=StringFunction.xssCharsToHtml(confInd)%>";
		        searchParms+="&status="+status;

	        if(buttonId=="Advanced"){
	        	var instrumentType = dijit.byId("pmtD_InstrumentTypeAdvance").attr('value'),
		        	currency = dijit.byId("pmtD_Currency").attr('value'),
		        	amountFrom = dijit.byId("pmtD_AmountFrom").get('value'),
		        	amountTo = dijit.byId("pmtD_AmountTo").get('value'),
		        	otherParty = dom.byId("pmtD_OtherParty").value;

			
				 if (isNaN(amountFrom))
		        	amountFrom = "";
		        
		        if (isNaN(amountTo))
		        	amountTo = "";
				

		        searchParms+="&instrumentType="+instrumentType;
				searchParms+="&currency="+currency;
				searchParms+="&amountFrom="+amountFrom;
				searchParms+="&amountTo="+amountTo;
				searchParms+="&otherParty="+otherParty;
				searchParms+="&searchType=Advanced";
	        }else if(buttonId=="Basic"){
	        	var instrumentType = dijit.byId("pmtD_InstrumentTypeBasic").attr('value'),
	        		instrumentId = dom.byId("pmtD_InstrumentId").value,
	 	        	refNum = dom.byId("pmtD_RefNum").value;

	 	        searchParms+="&instrumentType="+instrumentType;
	 	        searchParms+="&instrumentId="+instrumentId;
				searchParms+="&refNum="+refNum;
				searchParms+="&searchType=Basic";
	        }

			searchDataGrid("existingPaymentSearchDataGridId", "ExistingPaymentSearchDataView", searchParms);
  		});	<%-- end require --%>
	}

  	function selectRadio(){}

  	function shuffleFilter(linkValue){
  		require(["dojo/dom","dijit/registry","dojo/domReady!"],
  		      function(dom,registry){
	  			if(linkValue=='Advance'){
	  				dom.byId("advancdePaymentFilter").style.display='block';
	  				dom.byId("basicPaymentFilter").style.display='none';

	  				<%-- clearing Basic Filter --%>
	  				registry.byId("pmtD_InstrumentTypeBasic").value='';
	        		registry.byId("pmtD_InstrumentId").value='';
	 	        	registry.byId("pmtD_RefNum").value='';
	  			}

	  			if(linkValue=='Basic'){
	  				dom.byId("advancdePaymentFilter").style.display='none';
	  				dom.byId("basicPaymentFilter").style.display='block';

	  				<%-- clearing Advance Filter --%>
	  				registry.byId("pmtD_InstrumentTypeAdvance").value='';
		        	registry.byId("pmtD_Currency").value='';
		        	registry.byId("pmtD_AmountFrom").value='';
		        	registry.byId("pmtD_AmountTo").value='';
		        	registry.byId("pmtD_OtherParty").value='';
	  			}
  		});
  	}
  	require(["dojo/dom","dojo/domReady!" ], function(dom) {
  		dom.byId("advancdePaymentFilter").style.display='none';
  	});
</script>

  <div id="bankBranchSelectorDialog" ></div>



<%   formMgr.storeInDocCache("default.doc", new DocumentHandler());

   // Store the current secondary navigation in the session.
   session.setAttribute("paymentTransactions2ndNav", current2ndNav);
%>
