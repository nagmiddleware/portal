<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
				 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%

Debug.debug("***START*******creditNoteUnApplySearchDialog***** START***");


WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String returnAction     = null;
String onLoad           = "document.FilterForm.bankName.focus();";
String options = "";
String amountType = "";
String loginLocale = StringFunction.xssCharsToHtml(request.getParameter("loginLocale"));
String dateType = "";
String userOrgOid = StringFunction.xssCharsToHtml(request.getParameter("userOrgOid"));
String currency = StringFunction.xssCharsToHtml(request.getParameter("currency"));
 

String gridHtml="";
String gridLayout="";

String creditNoteOid = request.getParameter("creditNoteOid");
creditNoteOid = EncryptDecrypt.decryptStringUsingTripleDes(creditNoteOid,userSession.getSecretKey());
CreditNotesWebBean crNoteBean  = beanMgr.createBean(CreditNotesWebBean.class, "CreditNotes");
crNoteBean.getById(creditNoteOid);

System.out.println("creditNoteOid fetched -->" + creditNoteOid);
//Rel 9.2 XSS CID 11574
if(null != creditNoteOid){
	creditNoteOid = StringFunction.xssCharsToHtml(creditNoteOid);
}

String amount = crNoteBean.getAttribute("amount");
String appliedAmount = crNoteBean.getAttribute("credit_note_applied_amount");
DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
gridHtml = dgFactory.createDataGrid("InvoicesForCreditNoteUnApplyDataGridId","InvoicesForCreditNoteUnApplyDataGrid",null);
gridLayout = dgFactory.createGridLayout("InvoicesForCreditNoteUnApplyDataGrid");

%>



 <div class="dialogContent fullwidth">
 <table cellspacing="1" cellpadding="1" style="width:90%">
			
        	 <tr align="left">
				<td align="left"><b><%=resMgr.getText("CreditNoteSearch.CreditNoteId", TradePortalConstants.TEXT_BUNDLE)%><%=crNoteBean.getAttribute("invoice_id") %></b></td>
			</tr>
 		</table><div style="clear:both;"></div>
 		<div class="dialogContent fullwidth">
   	<%=widgetFactory.createWideSubsectionHeader("InvoicesForCreditNoteUnApply.already") %>  
 	<%=gridHtml%>  
</div>


<script type="text/javascript">

var initSearchParms="";
var loginLocale="";
var gridLayout = <%=gridLayout%>;
<%-- Rel 9.2 XSS CID 11574, 11575, 11579 --%>
initSearchParms = "userOrgOid=<%=StringFunction.escapeQuotesforJS(userOrgOid)%>&currency=<%=StringFunction.escapeQuotesforJS(currency)%>&creditNoteOid=<%=StringFunction.escapeQuotesforJS(creditNoteOid)%>";
console.log("initSearchParms: " + initSearchParms);
 
require(["t360/datagrid"],  function(t360){

	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoicesForCreditNoteUnApplyDataView",userSession.getSecretKey())%>";  
    var myGrid = t360.createDataGrid("InvoicesForCreditNoteUnApplyDataGridId", viewName,gridLayout,initSearchParms);
    myGrid.set('autoHeight',10); 
    
});

function UnApplyCreditNotes(){

  require(["dojo/ready","dijit/registry","dojo/_base/array","t360/dialog"], 

	function(ready,registry,baseArray,dialog) {
		
		  console.log('UnApplyCreditNotes()');
			
		  ready(function(){
			
			  var rowKeys = getSelectedGridRowKeys("InvoicesForCreditNoteUnApplyDataGridId");	
				if(rowKeys.length>=1){			
					dialog.doCallback('creditNoteUnApplySearchDialog','select', rowKeys);
					closePopup();
				} else {
					alert('Select Invoice(s) to un-apply credit notes'); 
					return false;
				}
				
			});
			});
	 
  }

 	  
  function closePopup() {
	  require(["t360/dialog"], function(dialog) {
		  dialog.hide('creditNoteUnApplySearchDialog');
	  });
	  
  }
  
  var dialogName=dijit.byId("creditNoteUnApplySearchDialog");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","creditNoteUnApplySearchDialogCloseButton");

</script>

<%=widgetFactory.createHoverHelp("creditNoteUnApplySearchDialogCloseButton", "common.Cancel") %>

</body>
</html>
