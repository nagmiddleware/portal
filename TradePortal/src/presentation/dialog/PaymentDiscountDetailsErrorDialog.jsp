<%--
 * Srinivasu Doddapaneni - Rel9.3 CR-997 - Payment Discount Details Popup 04/07/2015
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.*, 
                 com.ams.tradeportal.common.*, java.util.*"%>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"      scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
   
<div id="_errorSection" class="errorSection">
   
<%
  DocumentHandler   xmlDoc             = formMgr.getFromDocCache();
  DocumentHandler   errorDoc           = null;
  Vector            warningMessageList = new Vector();
  Vector            errorMessageList   = new Vector();
  Vector            infoMessageList    = new Vector();
  Vector            errorList          = null;
  int               severityLevel      = 0;
  int               errorListSize      = 0;
   errorList = (Vector)session.getAttribute("errorDoc");		
  
//  errorList     = xmlDoc.getFragments("/Error/errorlist/error");
  errorListSize = errorList.size();

  if (errorListSize > 0) {

    for (int i = 0; i < errorListSize; i++) {
      errorDoc = (DocumentHandler) errorList.elementAt(i);

      severityLevel = errorDoc.getAttributeInt("/severity");

      if (severityLevel == ErrorManager.ERROR_SEVERITY) {
        errorMessageList.addElement(errorDoc.getAttribute("/message"));
      }
      else if (severityLevel == ErrorManager.WARNING_ERROR_SEVERITY) {
        warningMessageList.addElement(errorDoc.getAttribute("/message"));
      }
      else if (severityLevel == ErrorManager.INFO_ERROR_SEVERITY) {
        infoMessageList.addElement(errorDoc.getAttribute("/message"));
      }
    }

    // Sort the error messages so they are displayed in a logical order
    Collections.sort(errorMessageList);
    Collections.sort(warningMessageList);
    Collections.sort(infoMessageList);

    // -- This If condition was added because the on more than 1 page the error section 
    // -- of the document handler is being displayed on the web page.  Instead of 
    // -- re-writing the same code, this was updated so that multiple pages can call
    // -- this jsp.  If the Parameter "htmlPage" is NOT defined then the display of
    // -- errors defaults to the original html code.  The logic in this file, is what
    // -- we want to reuse, it's the display of the Html that varies.

    String htmlPage = null;
    htmlPage = request.getParameter("htmlPage");     					
 
    if( ( htmlPage != null) && 
        ( htmlPage.equals( TradePortalConstants.AUTHORIZATION_ERRORS_HTML )) ) {       	
%> 
 
  <%@ include file="/transactions/fragments/Authorization_Errors_Html.frag" %>
 
<%
    }
    else {  //default to the original html definition(s)
      //combine errors and warnings becuase they have same background
      if ( errorMessageList.size() > 0 || warningMessageList.size() > 0 ) {
%>
<div class='PayDisDetailsError'>
  <div class="errorsAndWarnings">
<%
        for (int j = 0; j < errorMessageList.size(); j++) {
%>
    <div class="errorText"><%= errorMessageList.elementAt(j) %></div>
<%
        }
        for (int k = 0; k < warningMessageList.size(); k++) {
%>
    <div class="warnText"><%= warningMessageList.elementAt(k) %></div>
<%
        }
%>
  </div>
<%
      }
      //but infos are in a different section
      if ( infoMessageList.size() > 0 ) {
%>
  <div id="infoSection" class="infos">
<%
        for (int n = 0; n < infoMessageList.size(); n++) {
%>
    <div id="infoTextMessage"  class="infoText"><%= infoMessageList.elementAt(n) %></div>
<%
        }
%>
  </div>
<%
      }
    }
  }
%>

</div>
</div>