<%--
 *  Copy Selected Instrument.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.html.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, 
                 com.ams.tradeportal.busobj.*, com.ams.tradeportal.html.*, java.util.*" %>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 


 //read in parameters
 String copySelectedInstrumentDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
 System.out.println("copySelectedInstrumentDialogId:"+copySelectedInstrumentDialogId);
 String instrumentType = StringFunction.xssCharsToHtml(request.getParameter("instrumentType"));
 String bankBranchSelectorDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
  String isNonCC = StringFunction.xssCharsToHtml(request.getParameter("nonCC"));
  if(isNonCC == null || isNonCC == ""){
	isNonCC = TradePortalConstants.INDICATOR_NO;
  }
  String dropdownOptions = null;
  boolean showExpress    = true;
  boolean showFixed    = SecurityAccess.hasRights(userSession.getSecurityRights(),
          SecurityAccess.CREATE_FIXED_PAYMENT_TEMPLATE);
  
  if ( userSession.getOwnershipLevel().equals(TradePortalConstants.OWNER_CORPORATE) )
  {
    if(!userSession.hasSavedUserSession() ||
       !userSession.getSavedUserSessionSecurityType().equals(TradePortalConstants.ADMIN) )
    {
    	if(!instrumentType.equals(InstrumentType.APPROVAL_TO_PAY) && !instrumentType.equals(InstrumentType.IMPORT_DLC)
    			&& !instrumentType.equals(TradePortalConstants.SIMPLE_SLC) && !instrumentType.equals(InstrumentType.REQUEST_ADVISE)){
    		showExpress = false;
    	}
    }
  }

  if(!instrumentType.equals(InstrumentType.DOMESTIC_PMT) && !instrumentType.equals(InstrumentType.DOM_PMT)){
		showFixed = false;	  
  }
  System.out.println("showExpress:"+showExpress+"\tshowFixed:"+showFixed);
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = "";

  String ownerOrg = userSession.getOwnerOrgOid();
  CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(ownerOrg);
  String[] opBankOid = {corpOrg.getAttribute("first_op_bank_org"),
                        corpOrg.getAttribute("second_op_bank_org"),
                        corpOrg.getAttribute("third_op_bank_org"),
                        corpOrg.getAttribute("fourth_op_bank_org")};

  //do a local query to get names for operational bank orgs
  //could do this as dataview, but this is simpler - we don't expect this to 
  //change on this dialog via search.
  DocumentHandler oboListDoc = null;
  Vector opBankDocVec = new Vector();
  java.util.List<Object> sqlParamsLst = new java.util.ArrayList();
  StringBuilder in = new StringBuilder("?");
  sqlParamsLst.add(opBankOid[0]);
  
  for (int oboCount=1; oboCount<4; oboCount++) {
    if (InstrumentServices.isBlank(opBankOid[oboCount])) {
      break;
    }
    else {
        in.append(", ? ");
        sqlParamsLst.add(opBankOid[oboCount]);
    }
  }
  try {
	    StringBuffer sql = new StringBuffer();
	    sql.append("select ORGANIZATION_OID, NAME");
	    sql.append(" from OPERATIONAL_BANK_ORG");
	    sql.append(" where ORGANIZATION_OID in (").append(in).append(")");
	    sql.append(" order by name");
		if(StringFunction.isNotBlank(in.toString())){
	    oboListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParamsLst);
		}
	 if(null != oboListDoc){
    	opBankDocVec = oboListDoc.getFragments("/ResultSetRecord");
    }
  } catch (AmsException e) {
    //todo: should throw an error
    Debug.debug("queryListView threw an exception");
    e.printStackTrace();
  }
%>
<%

/*Kyriba CR 268
Get all External Bank's for this particular corporate customer.
*/
String serverLocation = JPylonProperties.getInstance().getString("serverLocation");
ClientServerDataBridge csdb = resMgr.getCSDB();
long longOldCorpOrgOid = Long.parseLong(ownerOrg.trim());

CorporateOrganization transOriginal = (CorporateOrganization)EJBObjectFactory.createClientEJB(serverLocation,
				   "CorporateOrganization", longOldCorpOrgOid, csdb);
ComponentList externalBankComponenet = (ComponentList) transOriginal.getComponentHandle("ExternalBankList");
int extBankTotal = externalBankComponenet.getObjectCount();

BusinessObject externalBankBusinessObj = null;
String externalBankBranches = "";
String externalBankOid = null;
DocumentHandler extBankDoc = null;
java.util.List<Object> sqlParamsLst1 = new java.util.ArrayList();

for(int i=0; i<extBankTotal; i++){
	
	externalBankComponenet.scrollToObjectByIndex(i);
	externalBankBusinessObj = externalBankComponenet.getBusinessObject();
	externalBankOid = externalBankBusinessObj.getAttribute("op_bank_org_oid");		
	  
	      if (!InstrumentServices.isBlank(externalBankOid)) {
	          if ( externalBankBranches.length()>0 ) {
	        	  externalBankBranches += ",";
	          }
	          externalBankBranches += "?";
	          sqlParamsLst1.add(externalBankOid);
	      }
	      else {
	          break; //if a blank is found there will be no more
	      }		
}
//Build query to get all external bank's 
try {
    StringBuffer extBanksql = new StringBuffer();
    extBanksql.append("select ORGANIZATION_OID, NAME");
    extBanksql.append(" from OPERATIONAL_BANK_ORG");
    extBanksql.append(" where ORGANIZATION_OID in (").append(externalBankBranches).append(")");
    extBanksql.append(" order by name");
	if(StringFunction.isNotBlank(externalBankBranches)){
    extBankDoc = DatabaseQueryBean.getXmlResultSet(extBanksql.toString(), true, sqlParamsLst1);
    }

  } catch (AmsException e) {
    //todo: should throw an error
    Debug.debug("Dialog:Exception in Getting External Bank");
    e.printStackTrace();
  }
  %>
<div id="copySelectedInstrumentDialogContent" class="dialogContent">
	<%=widgetFactory.createRadioButtonField("copy", "CopyToInstrument", "copyInstrDialog.CopyToInstrument","I", false, false, "onClick=\"resetTemplate()\"", "") %>
	<br>
	<div class="formItemWithIndent3">
		<%=widgetFactory.createLabel("","bankBranchSelectorDialog.instructions",false,false,false,"")%>
	</div>
	<div class="formItemWithIndent4">
		<div id="bankBranchSelectorDialogContent" class="dialogContent">
  <%
	dropdownOptions = Dropdown.createSortedOptions(oboListDoc, "ORGANIZATION_OID", "NAME", "", null, resMgr.getResourceLocale());
	//dropdownOptions = "<option value=\"\"> </option>"+dropdownOptions;
	

  
  if(null != opBankDocVec && opBankDocVec.size() > 0) {
  		if(extBankTotal == 0 ){%>
  	<div class="formItemWithIndent3">
  	&nbsp;&nbsp;<%
  			out.print(widgetFactory.createSelectField( "OperationalBankDropDown", "", " ", dropdownOptions, 
  				  false, false, false, "onChange=setBankOid(this)","","none"));
  	%></div>
  	<%}else{%>
  	
  
	  <div>
	  <%=widgetFactory.createRadioButtonField("BankSearchRadio","OperationalBankOrgRadio",
						"Client Bank Name","OperationalBankOrgRadio", false,  false, "onClick=\"showBankDropDown('OperationalBankOrgRadio')\"","") %>	
	  </div>
	  <div id="operationalbankorg_div_id" class="formItemWithIndent4">
	  <%
	 	out.print(widgetFactory.createSelectField( "OperationalBankDropDown", "", " ", dropdownOptions, 
	    false, false, false, "onChange=setBankOid(this)","","none"));
	  %>
	  </div>
  	<%} 
  	}
  
  dropdownOptions = "";
  dropdownOptions = Dropdown.createSortedOptions(extBankDoc, "ORGANIZATION_OID", "NAME", "", null, resMgr.getResourceLocale());
  //dropdownOptions = "<option value=\"\"> </option>"+dropdownOptions;
  
  if(extBankTotal > 0){
	  if(null == opBankDocVec || opBankDocVec.size() == 0) {%>
		  <div class="formItemWithIndent3">
		 &nbsp;&nbsp;<%  out.print(widgetFactory.createSelectField( "ExternalBankDropDown", "", " ", dropdownOptions, 
		    false, false, false, "onChange=setBankOid(this)","","none"));
		 %>
		 </div>
	  <%} else {%>
	  <div>
	   <%=widgetFactory.createRadioButtonField("BankSearchRadio","ExternalBankRadio",
						"NewInstrument.Dialog.Bank","ExternalBankRadio", false,  false, "onClick=\"showBankDropDown('ExternalBankRadio')\"","") %>
	  </div>
	  <div id="externalbank_div_id" class="formItemWithIndent4">
	  <%
	 
	  out.print(widgetFactory.createSelectField( "ExternalBankDropDown", "", " ", dropdownOptions, 
	    false, false, false, "onChange=setBankOid(this)","","none"));
	  %>
	  </div>
  <%}
	  }
 
%>
  <%= gridHtml %>
   <div id="manualInstId" class="">
    <% if(TradePortalConstants.INDICATOR_NO.equals(isNonCC)) { %>

 <%= widgetFactory.createTextField( "manualInstrumentId", "ConvertBankBranch.Header2","", "16", false, true, false, "","","")%>

<% } %>
</div>
</div>
	</div>
	<%=widgetFactory.createRadioButtonField("copy", "CopyToTemplate", "copyInstrDialog.CopyToTemplate","T", false, false, "onClick=\"resetCopyGrid('copyGrid')\"", "") %>
	<br>
	<div class="formItemWithIndent3">
	<%= widgetFactory.createTextField( "TemplateName", "NewInstTemplate1.TemplateName", "", "35", false, true, false, "class='char36' onClick='resetCopyGrid()'", "", "") %>

<%
	  String clientBankOid = userSession.getClientBankOid();
      ClientBankWebBean clientBank = beanMgr.createBean(ClientBankWebBean.class,"ClientBank");
      clientBank.getById(clientBankOid);      

      if(TradePortalConstants.INDICATOR_YES.equals(clientBank.getAttribute("template_groups_ind"))){
   

		//Retrieve the data used to populate some of the dropdowns.
		// This data will be used in the default users dropdown
		StringBuffer sqlSet = new StringBuffer();
		QueryListView queryListView = null;
		DocumentHandler queryDoc = null;
		
		try {
		    queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView");
		
		    // Get a list of the users for the client bank
		    sqlSet.append("select PAYMENT_TEMPLATE_GROUP_OID, NAME");
		    sqlSet.append(" from PAYMENT_TEMPLATE_GROUP");
		    sqlSet.append(" where P_OWNER_ORG_OID in (?,?)");
		    Debug.debug(sqlSet.toString());
		    
		    queryListView.setSQL(sqlSet.toString(), new Object[]{userSession.getUserOid(),userSession.getOwnerOrgOid()});
		    queryListView.getRecords();
		    queryDoc = queryListView.getXmlResultSet();
		    Debug.debug("queryDoc -->: " + queryDoc.toString());
		
		} catch (Exception e) {
		    e.printStackTrace();
		} 
		finally {
		  try {
		        if (queryListView != null) {
		            queryListView.remove();
		        }
		      } 
		      catch (Exception e) {
		           System.out.println("error removing querylistview in NewTemplate.jsp");
		      }
		}  // try/catch/finally block
		
		
		/********************************
		 * START Payment Template Group TYPE DROPDOWN
		 ********************************/
		 Debug.debug("Building Payment Template Group field");
		 	
		 String groupTmplateOptions = Dropdown.createSortedOptions(queryDoc,"PAYMENT_TEMPLATE_GROUP_OID", "NAME", 
		 										"", 
		 										resMgr.getResourceLocale());
		 Debug.debug(groupTmplateOptions);
		%> 
		<%= widgetFactory.createSelectField( "TemplateGroup", "NewInstTemplate1.TemplateGroup", " ", groupTmplateOptions, false, false, false, "class='char36' onChange='resetCopyGrid()'", "", "") %>
<% } %>
</div>

<% if(showExpress) { %>
	<%=widgetFactory.createCheckboxField("Express", "NewInstTemplate1.IsExpress",false, false ) %>
<%} %>

<% if(showFixed) { %>
	<%=widgetFactory.createCheckboxField("FixedPayment", "NewInstTemplate1.IsFixed",false, false) %>
<% } %>	

	<div class="formItemWithIndent3">
		<button data-dojo-type="dijit.form.Button"  name="Continue" id="Continue" type="button">
			<%=resMgr.getText("CopySelected.Continue",TradePortalConstants.TEXT_BUNDLE) %>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				continueCopy();     					
			</script>
		</button>
		<%=widgetFactory.createHoverHelp("Continue","CopySelectedHoverText") %>
		<button data-dojo-type="dijit.form.Button"  name="Cancel" id="Cancel" type="button">
			<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				closeBBDialog();    					
			</script>
		</button> 
		<%=widgetFactory.createHoverHelp("Cancel","common.Cancel") %> 
	</div>
</div>

<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>
 require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {
	  <%-- When dialog loads hide the drop downs --%>
	  hideBankDropDown();
  });
  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {

<%
  String copyGridLayout = dgFactory.createGridLayout("copyGrid", "CopySelectedInstrumentDataGrid");
%>
    var gridLayout = <%= copyGridLayout %>;

<%
  //get the grid data
  //this reevaluates from the user session data rather than taking passed in bank branch
  //values for security
  StringBuffer bbGridData = new StringBuffer();
  if ( oboListDoc != null ) {
    Vector oboList = oboListDoc.getFragments("/ResultSetRecord");
    for ( int oboIdx = 0; oboIdx<oboList.size(); oboIdx++ ) {
      DocumentHandler oboDoc = (DocumentHandler) oboList.get(oboIdx);
      String orgOid = oboDoc.getAttribute("/ORGANIZATION_OID");
      String orgName = oboDoc.getAttribute("/NAME");
      bbGridData.append("{ ");
      bbGridData.append("rowKey:'").append(orgOid).append("'");
      bbGridData.append(", ");
      bbGridData.append("BankBranch:'").append(StringFunction.xssCharsToHtml(orgName)).append("'");
      bbGridData.append(" }");
      if ( oboIdx < oboList.size()-1) {
        bbGridData.append(", ");
      }
    }
  }
%>
    var gridData = [ <%= bbGridData.toString() %> ];

    <%--create a local memory grid--%>
    t360grid.createMemoryDataGrid("copyGrid", gridData, gridLayout);
  });

  <%-- on select perform execute the select callback function --%>
  function continueCopy() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
    	var bankBranchOid = getSelectedGridRowKeys("copyGrid");
    	var copyType;
    	var templateName = registry.byId("TemplateName").value;
       
        var templateGroup = "";
        if(registry.byId("TemplateGroup")){
    	   templateGroup = registry.byId("TemplateGroup").value;
        }
        var rowKeys = selectedBankOid;
	    var manualInstId = null;
	    <%-- Rel 9.2 XSS CID 11279 --%>
	    var isNonCCVal = "<%=StringFunction.escapeQuotesforJS(isNonCC)%>";  
     
        var flagExpress;
        var flagFixed;
    	if(registry.byId("CopyToInstrument").checked){
    		copyType= registry.byId("CopyToInstrument").value;			
			if (!rowKeys) {
				alert("Please select the Bank/Branch or press 'Cancel'");
					return;
			  }
			  <%-- 
    		if(bankBranchOid == null || bankBranchOid == ""){
    			alert('<%=resMgr.getText("CopySelected.PromptMessage3",TradePortalConstants.TEXT_BUNDLE) %>');
    			return false;
    		} --%>
			
			if(isNonCCVal == "<%=TradePortalConstants.INDICATOR_NO%>"){
			  var selectField = dijit.byId('manualInstrumentId');
			  manualInstId = selectField.attr('value');
		}
		if((isNonCCVal == "<%=TradePortalConstants.INDICATOR_NO%>") && (manualInstId == null || manualInstId == "")){
		    alert("Instrument Id is required for Converted instruments.");
			return;
		}
    	}else if (registry.byId("CopyToTemplate").checked){
    		copyType = registry.byId("CopyToTemplate").value;
    		if(templateName == null || templateName ==""){
    			<%-- IR T36000048558 Rel9.5 05/09/2016 --%>
    			alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("CopySelected.PromptMessage2",TradePortalConstants.TEXT_BUNDLE)) %>');
    			return false;
    		}
    	}else if(!registry.byId("CopyToInstrument").checked && !registry.byId("CopyToTemplate").checked){
    		alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("CopySelected.PromptMessage1",TradePortalConstants.TEXT_BUNDLE)) %>');
    		return false;
    	}
      
      if (<%=showExpress%>) { 
    	  if(registry.byId("Express").checked){
    		  flagExpress = "<%=TradePortalConstants.INDICATOR_ON%>";
    	  }
      }
      if (<%=showFixed%>) { 
    	  if(registry.byId("FixedPayment").checked){
    		  flagFixed = "<%=TradePortalConstants.INDICATOR_ON%>";
    	  }
      }
      

	   
	
      dialog.doCallback('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(copySelectedInstrumentDialogId))%>', 'select',rowKeys, copyType, templateName, templateGroup, flagExpress+"/"+flagFixed+"/"+manualInstId);
    });
  }

  function closeBBDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
    	<%-- Rel 9.2 XSS CID 11345 --%>
      dialog.hide('<%=StringFunction.escapeQuotesforJS(copySelectedInstrumentDialogId)%>');
    });
  }

	require(["dijit/registry","dojo/ready"], function(registry,ready) {
	    ready(function(){
			var myGrid = registry.byId("copyGrid");
			myGrid.on("SelectionChanged", function(){
				registry.byId("CopyToInstrument").set('checked', true);
		    }, true);
	    });
	});
	
	function resetTemplate(){
		require(["dijit/registry"], function(registry) { 
			if (registry.byId("TemplateGroup")){
			  registry.byId("TemplateGroup").set("value","");
			}
			registry.byId("TemplateName").set("value","");
			registry.byId("CopyToInstrument").set('checked', true);
		});
	}
	
	function resetCopyGrid(){
	  	require(["dijit/registry"], function(registry) { 
		 	<%-- var myGrid = registry.byId("copyGrid"); --%>
		 	<%-- myGrid.selection.clear(); --%>
			registry.byId('OperationalBankDropDown').attr('value','');
			registry.byId('ExternalBankDropDown').attr('value','');
			if (registry.byId("TemplateGroup")){
			  registry.byId("CopyToInstrument").set("value","");
			}
	      	registry.byId("CopyToTemplate").set('checked', true);
			registry.byId("OperationalBankOrgRadio").set('checked', false);
			registry.byId("ExternalBankRadio").set('checked', false);
	    });
	}	



	var selectedBankOid = "";
 
<%-- Rel 9.2 XSS CID 11337 --%>
	var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(copySelectedInstrumentDialogId)%>");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","bankBranchSelectorDialogCloseLink");
	dialogCloseButton.setAttribute("title","");
	
	<%-- Based upon the radio button selection, show the appropriate dropdown --%>
	function showBankDropDown(temp){
		if('OperationalBankOrgRadio' == temp){
			document.getElementById("operationalbankorg_div_id").style.display = 'block' ;
			document.getElementById("externalbank_div_id").style.display = 'none' ;
			dijit.byId('OperationalBankDropDown').attr('displayValue','');
		}else if('ExternalBankRadio'== temp){
			document.getElementById("operationalbankorg_div_id").style.display = 'none' ;
			document.getElementById("externalbank_div_id").style.display = 'block' ;
			dijit.byId('ExternalBankDropDown').attr('displayValue','');
		}
		resetTemplate();
	}
	<%-- Hide the dropdown when dialog loads. --%>
	function hideBankDropDown(){
		document.getElementById("operationalbankorg_div_id").style.display = 'none' ;
		document.getElementById("externalbank_div_id").style.display = 'none' ;
		
	}
	<%-- Set the selected external/oper bank oid to an common var --%>
	function setBankOid(thisObj){
		if(thisObj){
			selectedBankOid = thisObj.value;
		}
		require(["dijit/registry"], function(registry) { 
			if (registry.byId("TemplateGroup")){
			  registry.byId("TemplateGroup").set("value","");
			}
			registry.byId("TemplateName").set("value","");
			registry.byId("CopyToInstrument").set('checked', true);
		});
	}
	
</script>
	<%=widgetFactory.createHoverHelp("copySelectedInstrumentDialogCloseLink", "common.Cancel") %>
