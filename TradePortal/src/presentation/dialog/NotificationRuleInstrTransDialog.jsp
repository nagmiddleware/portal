<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.amsinc.ecsg.html.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	int tLoop; 
	boolean isReadOnly = false;
	boolean isTemplate = false;
	String defaultSelection = "";
	String tableRowIdPrefix ="";
	String [] defUserIdArray = new String[24];
	//read in parameters
  	String sectionNum = StringFunction.xssCharsToHtml(request.getParameter("sectionNum"));
  	String criteriaStartCount = StringFunction.xssCharsToHtml(request.getParameter("criteriaStartCount"));
  	String transArrayStr = StringFunction.xssCharsToHtml(request.getParameter("transArrayStr"));
  	String instrType = StringFunction.xssCharsToHtml(request.getParameter("instrType"));
  	String uiType = StringFunction.xssCharsToHtml(request.getParameter("uiType"));
  	String notificationRuleDetailDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
    
    int sNum = Integer.valueOf(sectionNum);
    String [] transArray = transArrayStr.split(",");
    String notifyEmail;
    final String [] SUPPLIER_PORTAL_Lable_TransArray = {"SP_CNF", "SP_LF"};
	//Srinivasu_D IR#T36000048387 CR#927b Rel9.5 05/02/2016 - Added  instrument specific transaction type codes - Start
	final String [] EXP_DLC_Lable_TransArray = {"EXP_ARU", "EXP_ADJ", "EXP_ADV", "EXP_AMD", "EXP_ASN", "EXP_CHG", "EXP_PCS", "EXP_PCE", "EXP_DEA", "EXP_EXM", "EXP_EXP", "EXP_EXT", "EXP_PAY", "EXP_PRE", "EXP_REA", "EXP_RED", "EXP_RVV", "EXP_TRN", "EXP_PYT"};
    final String [] RQA_Lable_TransArray = {"RQA_ARU", "RQA_ADJ", "RQA_ADV", "RQA_AMD", "RQA_ASN", "RQA_CHG", "RQA_PCS", "RQA_PCE", "RQA_DEA", "RQA_EXM", "RQA_EXP", "RQA_EXT", "RQA_ISS", "RQA_PAY", "RQA_REA", "RQA_RED", "RQA_RVV", "RQA_TRN", "RQA_PYT"};
	 final String [] LOI_TransArray = {"LOI_ARU", "LOI_ADJ", "LOI_AMD", "LOI_CHG", "LOI_CRE", "LOI_DEA", "LOI_EXP", "LOI_PAY", "LOI_REA"};
    int notifEmailIndex = 0;
    int rowIndex = 0;
    int spIndex = 0;
	int expIndex = 0;
	int rqaIndex = 0;
	int loiIndex = 0;
	//Srinivasu_D IR#T36000048387 CR#927b Rel9.5 05/02/2016 - End
    int DEFAULT_NOTIF_RULE_USER_EMAIL_COUNT = 2;
    
    /* out.print(", in the dialog transArray[0]: "+transArray[0]);
    out.print(", in the dialog transArray[1]: "+transArray[1]); */
    
    String notifyRuleUsersSql = "select USER_OID, EMAIL_ADDR, LOGIN_ID, USER_IDENTIFIER from users where activation_status=? and p_owner_org_oid = ? and email_addr is not null";
    DocumentHandler notifyRuleUsersList = DatabaseQueryBean.getXmlResultSet(notifyRuleUsersSql, false, new Object[]{TradePortalConstants.ACTIVE, userSession.getOwnerOrgOid()});
 	
 	String userEmailsOptions = "<option  value=\"\"></option>";
 	userEmailsOptions += ListBox.createOptionList(notifyRuleUsersList, "USER_OID", "USER_IDENTIFIER", "", null);
%>

	<div id="NotificationRuleDetailDialog" class="dialogContent midwidth">
		<%=widgetFactory.createNote("NotificationRuleDetail.TransDialogNote")%> 
	<%
		if(uiType.equals("ALL")){
			for (tLoop = 0; tLoop < transArray.length; tLoop++){
				if(InstrumentType.SUPPLIER_PORTAL.equals(instrType)){
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+SUPPLIER_PORTAL_Lable_TransArray[spIndex])%>
	<%
				spIndex++;
				} else if (InstrumentType.EXPORT_DLC.equals(instrType) || InstrumentType.INCOMING_GUA.equals(instrType) || InstrumentType.INCOMING_SLC.equals(instrType)){
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+EXP_DLC_Lable_TransArray[expIndex])%>
	<%			
				expIndex++;
				} else if (InstrumentType.REQUEST_ADVISE.equals(instrType)){
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+RQA_Lable_TransArray[rqaIndex])%>

	<%			rqaIndex++;
				} else if (InstrumentType.INDEMNITY.equals(instrType)){
	%>
					<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+LOI_TransArray[loiIndex])%>

	<%			loiIndex++;
				}else{
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+StringFunction.xssCharsToHtml(transArray[tLoop]))%>
	<%
				}
	%>
				<table>
					<tr>
						<td width="20%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendPortalNotificatios") %>
						</td>
						<td width="18%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
						</td>
						<td width="35%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailRecipients") %>
						</td>
						<td width="27%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.AddlEmailRecipients", "subLabelNarrowLineHight") %>
						</td>
					</tr>
					<tr>
						<td width="20%" style="vertical-align: top;">
							<%=widgetFactory.createRadioButtonField("transNotifySetting_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"transNotifySettingAlways_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transNotifySetting_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"transNotifySettingNone_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transNotifySetting_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"transNotifySettingChrgsDocsOnly_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
						</td>
						
						<td width="18%" style="vertical-align: top;">
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"transEmailSettingAlways_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"transEmailSettingNone_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE,false, isReadOnly)%>
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"transEmailSettingChrgsDocsOnly_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]),
									"NotificationRuleDetail.ChargesDocuments",TradePortalConstants.NOTIF_RULE_CHRGS_DOCS_ONLY,false,isReadOnly)%>
						</td>
						<td width="35%" style="vertical-align: top;">
							<div id="notifUsersRowsDialog_<%=instrType%>_<%=StringFunction.xssCharsToHtml(transArray[tLoop])%>" style="height: auto; overflow-y: auto; overflow-x: hidden; max-height: 135px;">
								<table border="1" id="NotifUsersTableDialog_<%=instrType%>_<%=StringFunction.xssCharsToHtml(transArray[tLoop])%>" style="width:90%">
									<%tableRowIdPrefix="NotifUsersTableDialog_"+instrType+"_"+transArray[tLoop]; %>
									<tbody>
									<%
										notifyEmail = "trans"+instrType+transArray[tLoop]+"_EMAIL";
										notifEmailIndex = 0;
										rowIndex = 0;
									%>
										<%@ include file="/refdata/fragments/NotificationRuleDialog-UserEmailRows.frag" %>  
									</tbody>
								</table>
							</div>
						<%
							if (!isReadOnly) {
						%>
							<div>
								<button data-dojo-type="dijit.form.Button" type="button" id="add2MoreTransUsers_<%=instrType%>_<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArray[tLoop]))%>">
									<%=resMgr.getText("NotificationRuleDetail.Add2Users",TradePortalConstants.TEXT_BUNDLE)%>
									<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									add2MoreTransUserEmails(<%=tLoop%>, '<%=instrType%>', '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArray[tLoop]))%>');
								</script>
								</button>
							</div>
						<%
							}
						%>  
						</td>
						<td width="27%" align="center" style="vertical-align: top;">
							<%= widgetFactory.createTextArea("transAdditionalEmailAddr_"+instrType+"_"+transArray[tLoop], "", "", isReadOnly, false, false, "maxlength='300' rows='6'", "", "none") %>
							<button data-dojo-type="dijit.form.Button"  name="ClearAllTrans_<%=instrType%>_<%=transArray[tLoop]%>" id="ClearAllTrans_<%=instrType%>_<%=transArray[tLoop]%>" type="button">
								<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								clearTransCategorySection(<%=tLoop%>, '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(instrType))%>', '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArray[tLoop]))%>',
								'<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(criteriaStartCount))%>');
							</script>
							</button>
						</td>
					</tr>
				</table>
	<%
			}
		}//if uiType is empty
		else if(uiType.equals("MESSAGES") || uiType.equals("OTHERS")){
			for (tLoop = 0; tLoop < transArray.length; tLoop++){
	%>
				<%=widgetFactory.createWideSubsectionHeader("NotificationRuleDetail.TransactionTitlefor"+StringFunction.xssCharsToHtml(transArray[tLoop]))%>
				<table>
					<tr>
						<td width="30%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.SendEmail") %>
						</td>
						<td width="8%">
							&nbsp;
						</td>
						<td width="35%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.EmailRecipients") %>
						</td>
						<td width="27%">
							<%=widgetFactory.createSubLabel("NotificationRuleDetail.AddlEmailRecipients", "subLabelNarrowLineHight") %>
						</td>
					</tr>
					<tr>
						<td width="30%" style="vertical-align: top;">
						<%
							if(uiType.equals("MESSAGES")){
						%>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrType+"_"+transArray[tLoop],
									"transEmailSettingAlways_"+instrType+"_"+transArray[tLoop],
									"NotificationRuleDetail.Always",TradePortalConstants.NOTIF_RULE_ALWAYS, false, isReadOnly)%>                         
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrType+"_"+transArray[tLoop],
									"transEmailSettingNone_"+instrType+"_"+transArray[tLoop],
									"NotificationRuleDetail.None",TradePortalConstants.NOTIF_RULE_NONE, false, isReadOnly)%>
						<%
							} else if(uiType.equals("OTHERS")){
						%>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrType+"_"+transArray[tLoop],
									"transEmailSettingAlways_"+instrType+"_"+transArray[tLoop],
									"NotificationRuleDetail.Yes",TradePortalConstants.NOTIF_RULE_YES, false, isReadOnly)%>                         
							<br>
							<%=widgetFactory.createRadioButtonField("transEmailSetting_"+instrType+"_"+transArray[tLoop],
									"transEmailSettingNone_"+instrType+"_"+transArray[tLoop],
									"NotificationRuleDetail.No",TradePortalConstants.NOTIF_RULE_NONE, false, isReadOnly)%>
						
							<br><br>
							<%=widgetFactory.createLabel("", "NotificationRuleDetail.EmailInterval", false, false, false, "inline") %>
							<%= widgetFactory.createTextField("transNotifyEmailInterval_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]), "", "", "5", 
									isReadOnly,false,false,"","regExp:'^[1-9][0-9]*0$',invalidMessage:'"+resMgr.getText("NotificationRuleDetail.InvalidNotificationEmailFreqValue",TradePortalConstants.TEXT_BUNDLE)+"'","none") %>
							<br>
						<%
							}
						%>
						</td>
						
						<td width="8%" style="vertical-align: top;">
							&nbsp;
						</td>
						<td width="35%" style="vertical-align: top;">
							<div id="notifUsersRowsDialog_<%=instrType%>_<%=transArray[tLoop]%>" style="height: auto; overflow-y: auto; overflow-x: hidden; max-height: 135px;">
								<table border="1" id="NotifUsersTableDialog_<%=instrType%>_<%=transArray[tLoop]%>" style="width:90%">
									<tbody>
									<%
										notifyEmail = "trans"+instrType+transArray[tLoop]+"_EMAIL";
										notifEmailIndex = 0;
										rowIndex = 0;
									%>
										<%@ include file="/refdata/fragments/NotificationRuleDialog-UserEmailRows.frag" %>  
									</tbody>
								</table>
							</div>
						<%
							if (!isReadOnly) {
						%>
							<div>
								<button data-dojo-type="dijit.form.Button" type="button" id="add2MoreTransUsers_<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(instrType))%>_<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArray[tLoop]))%>">
									<%=resMgr.getText("NotificationRuleDetail.Add2Users",TradePortalConstants.TEXT_BUNDLE)%>
									<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
									add2MoreTransUserEmails(<%=tLoop%>, '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(instrType))%>', '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArray[tLoop]))%>');
								</script>
								</button>
							</div>
						<%
							}
						%>  
						</td>
						<td width="27%" align="center" style="vertical-align: top;">
							<%= widgetFactory.createTextArea("transAdditionalEmailAddr_"+instrType+"_"+StringFunction.xssCharsToHtml(transArray[tLoop]), "", "", isReadOnly, false, false, "maxlength='300' rows='6'", "", "none") %>
							<button data-dojo-type="dijit.form.Button"  name="ClearAllTrans_<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(instrType))%>_<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArray[tLoop]))%>" id="ClearAllTrans_<%=instrType%>_<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArray[tLoop]))%>" type="button">
								<%=resMgr.getText("NotificationRuleDetail.ClearAll.Button",TradePortalConstants.TEXT_BUNDLE)%>
								<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
								clearTransCategorySection(<%=tLoop%>, '<%=instrType%>', '<%=transArray[tLoop]%>',
								'<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(criteriaStartCount))%>');
							</script>
							</button>
						</td>
					</tr>
				</table>
	<%
			}
		}
	%>
		<div class='formItem'>
			<button data-dojo-type="dijit.form.Button"  name="SaveCloseButtonDialog<%=sNum-1%>" id="SaveCloseButtonDialog<%=sNum-1%>" type="button">
				<%=resMgr.getText("common.SaveCloseText",TradePortalConstants.TEXT_BUNDLE)%>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					updateTransNotifySettings(<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(String.valueOf(sNum)))%>, '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(criteriaStartCount))%>', '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(transArrayStr))%>', '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(instrType))%>', '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(uiType))%>');
			 	</script>
			</button>
			<button data-dojo-type="dijit.form.Button"  name="CancelDialog<%=sNum-1%>" id="CancelDialog<%=sNum-1%>" type="button">
				<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					closeTransNotifySettingDialog();    					
				</script>
			</button> 
		</div>
	</div><%-- end dialogid div --%>
	
<script type='text/javascript'>
	function clearTransCategorySection(sectionNum, instrType, transType,criteriaStartCount) {	console.log("sectionNum:"+sectionNum);
		if (!confirm(" 'Clear All' will Remove All Selected Users and Entered Email Addresses. Do you want to continue?")){
    		return false;
    	}else{
			var uiType = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(uiType))%>';
			require(["dijit/registry"],function(registry) {
				if(uiType == 'ALL'){
					registry.byId('transNotifySettingAlways_'+instrType+'_'+transType).setChecked(false);
					registry.byId('transNotifySettingNone_'+instrType+'_'+transType).setChecked(false);
					registry.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transType).setChecked(false);
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transType).setChecked(false);
					registry.byId('transEmailSettingNone_'+instrType+'_'+transType).setChecked(false);
					registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transType).setChecked(false);
				}else if(uiType == 'MESSAGES'){
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transType).setChecked(false);
					registry.byId('transEmailSettingNone_'+instrType+'_'+transType).setChecked(false);
				}else if(uiType == 'OTHERS'){
					registry.byId('transEmailSettingAlways_'+instrType+'_'+transType).setChecked(false);
					registry.byId('transEmailSettingNone_'+instrType+'_'+transType).setChecked(false);
					registry.byId("transNotifyEmailInterval_"+instrType+'_'+transType).setValue('');
				}
		    	dojo.byId("clear_tran_ind"+criteriaStartCount).value = "Y";
				registry.byId("transAdditionalEmailAddr_"+instrType+'_'+transType).setValue('');
		    	
				var userEmailIdx = 0;
				var myTable = dojo.byId('NotifUsersTableDialog_'+instrType+'_'+transType);
		    	for(e=1; e<=(myTable.rows.length)*2; e++){
		    		dijit.byId("trans"+instrType+transType+"_EMAIL"+userEmailIdx).setValue('');
		    		userEmailIdx++;
				}
			});
    	}
  	}

  	function add2MoreTransUserEmails(secNum, instrType, transType) {
  		require(["dijit/registry", "t360/common","dojo/parser","dojo/ready"],function(registry, common,parser,ready) {
  			ready(function(){
		        var isReadOnly = '<%=isReadOnly%>'; // If require use isReadOnly while creating widgets
		        var tbl = document.getElementById('NotifUsersTableDialog_'+instrType+'_'+transType);// To identify the table in which the row will get insert
		    	var tblLength = tbl.rows.length;// Table length
		    	var emailIdCount = (tblLength)*2;//Current total email count

		    	for(i=tblLength;i<tblLength+1;i++){
		    		if(tblLength < 12){//Only add new row if the tblLenth less than 12. 
			       	 	var newRow = tbl.insertRow(i);//Creation of new row
			       	 	newRow.id = 'NotifUsersTableDialog_'+instrType+'_'+transType+"_"+i;//Provide id for newly created row
			       	 	for(j=0;j<2;j++){//Creates two cells in a row and attaches two FilteringSelects to those
			    			var cell=newRow.insertCell(j);//Creates new cell in a row
			    			cell.innerHTML='<select data-dojo-type=\"dijit.form.FilteringSelect\" class=\"char9\" name=\"trans'+instrType+transType+"_EMAIL"+emailIdCount+'\" id=\"trans'+instrType+transType+"_EMAIL"+emailIdCount+'\"></select>';
			    			emailIdCount++;
			    		}
			       	 	parser.parse(newRow.id);//Parse the new row
			       	 	var firstEmailUserSelect = registry.byId('trans'+instrType+transType+'_EMAIL0');
			       	 	if(firstEmailUserSelect){//If the widget is available then only get the store from it.
			       	 		var userStore = firstEmailUserSelect.store;//Get first email user store
				       	 	for(j=0;j<2;j++){//Set store for newly created widgets
				       	 		emailIdCount--;
					       	 	registry.byId('trans'+instrType+transType+'_EMAIL'+emailIdCount).set("store",userStore);
								registry.byId('trans'+instrType+transType+'_EMAIL'+emailIdCount).attr('displayedValue', "");
		    				}
			       	 		
			       	 	}
			       	 	
		       	 	}else{//Disable the button
			       	 	var addButton = registry.byId('add2MoreTransUsers_'+instrType+'_'+transType);
			            if ( addButton ) {
			            	addButton.set('disabled',true);
			            }
		       	 	}
		    	}
	  		});
  		});
  	}
	
	function updateTransNotifySettings(secNum, criteriaStartCount, transArrayStr, instrType, uiType){
		require(["dijit/registry", "t360/dialog"],function(registry, dialog) {
			var transArray = transArrayStr.split(',');
    		var userEmailStartCount = criteriaStartCount * 24;
			var notifSetting;
			var emailSetting;
			var additionalEmail;
			var transNotifyEmailFreq;
			
			for(i=0; i<transArray.length; i++){
				notifSetting = '';
				emailSetting = '';
				additionalEmail = '';
				userEmail = '';
				transNotifyEmailFreq = '';
				
				if(uiType == 'ALL'){
					if (registry.byId('transNotifySettingAlways_'+instrType+'_'+transArray[i]).checked){
						notifSetting = registry.byId('transNotifySettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transNotifySettingNone_'+instrType+'_'+transArray[i]).checked){
			    		notifSetting = registry.byId('transNotifySettingNone_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).checked){
			    		notifSetting = registry.byId('transNotifySettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).value;
			    	}
			    	
			    	if (registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingChrgsDocsOnly_'+instrType+'_'+transArray[i]).value;
			    	}
			    	
			    	dojo.byId("send_notif_setting"+criteriaStartCount).value = notifSetting;
	    			dojo.byId("send_email_setting"+criteriaStartCount).value = emailSetting;
				}else if(uiType == 'MESSAGES'){
					if (registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).value;
			    	}
					
					dojo.byId("send_email_setting"+criteriaStartCount).value = emailSetting;
				}else if(uiType == 'OTHERS'){
					if (registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingAlways_'+instrType+'_'+transArray[i]).value;
			    	}else if (registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).checked){
			    		emailSetting = registry.byId('transEmailSettingNone_'+instrType+'_'+transArray[i]).value;
			    	}
					
					transNotifyEmailFreq = registry.byId("transNotifyEmailInterval_"+instrType+'_'+transArray[i]).value;
	    			dojo.byId("notify_email_freq"+criteriaStartCount).value = transNotifyEmailFreq;
					dojo.byId("send_email_setting"+criteriaStartCount).value = emailSetting;
				}
		    	
		    	additionalEmail = registry.byId("transAdditionalEmailAddr_"+instrType+'_'+transArray[i]).value;
    			dojo.byId("additional_email_addr"+criteriaStartCount).value = additionalEmail;
    			
    			var userIds = '';
    			for(j = 0; j < 24; j++){
    				userEmail = '';
    				if(registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j)){
    					userEmail = registry.byId('trans'+instrType+transArray[i]+'_EMAIL'+j).value;
    					dojo.byId('email_id'+userEmailStartCount).value = userEmail;
    					
    					userIds = userIds + userEmail;
        				if(j != 23){
        					userIds = userIds + ',';
        				}
    				}else{
    					if(j != 23){
        					userIds = userIds + ',';
        				}
    				}
    				userEmailStartCount++;
  				} 
    			dojo.byId('notificationUserIds'+criteriaStartCount).value = userIds;
    			criteriaStartCount++;
			}
			
			dialog.close('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>');
			document.getElementById('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>').hidden = true;
		});
 	}
  	
	function closeTransNotifySettingDialog(){
		require(["dijit/registry", "t360/dialog"],function(registry, dialog) {
			dialog.close('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>');
			document.getElementById('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(notificationRuleDetailDialogId))%>').hidden = true;
		});
	}
</script>
