<%--
 *  PO Definition Selector Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *	   Copyright   2012                         
 *     CGI, Incorporated 
 *     All rights reserved
--%>
	<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
	                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>
	
	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
	<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

	<% 
	   

	    String 	loginRights 		= userSession.getSecurityRights();
	    String userSec = "";
	   	boolean canCreate = false;
	   	
	    if (userSession.getSavedUserSession() == null) {
	    	userSec = userSession.getSecurityType();
	   }else {
		   userSec = userSession.getSavedUserSessionSecurityType();
	   }
	   
	    if(TradePortalConstants.ADMIN.equals(userSec) && SecurityAccess.hasRights(loginRights,
	              SecurityAccess.MAINTAIN_INVOICE_DEFINITION)){
		  canCreate    = true;
		}



		//read in parameters
		String poUploadDefnSelectorDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
 		
		WidgetFactory widgetFactory = new WidgetFactory(resMgr);
		DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
		String gridHtml = "";
		
		StringBuffer sql = new StringBuffer();
		QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
        
		sql.append("select inv_upload_definition_oid, name ");
        sql.append(" from invoice_definitions");
        sql.append(" where a_owner_org_oid in (?");
        List<Object> sqlParamsInv = new ArrayList();
        sqlParamsInv.add(userSession.getOwnerOrgOid());
			
          // AList<Object> sqlParams = new ArrayList();lso include PO defs from the user's actual organization if using subsidiary access
         if(userSession.showOrgDataUnderSubAccess())
         {
         	sql.append(",?");
         	sqlParamsInv.add(userSession.getSavedUserSession().getOwnerOrgOid());
         }
      	 sql.append(")");
         
         sql.append(" order by ");
         sql.append(resMgr.localizeOrderBy("name"));
         Debug.debug("InvDefinitionSelectorDialog being processed dialogId " + sql.toString());

         DocumentHandler poUplDefnListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true ,sqlParamsInv);
         
         
         // System.out.println (poUplDefnListDoc);
		 Vector<DocumentHandler> poUplDefnList = new Vector();
	
		if (null != poUplDefnListDoc ){
			poUplDefnList = poUplDefnListDoc.getFragments("/ResultSetRecord");
		}

		if(poUplDefnList.size() != 0 && poUplDefnList.size() > 1){
	%>
		<div id="poUploadDefnSelectorDialogContent" class="dialogContent">
			<%=widgetFactory.createLabel("", "InvOnlyRuleDefnSelector.InvDefnInstruction", false, false, false, "")%>
	  	<%
	  		gridHtml = dgFactory.createDataGrid("invDefnGrid","InvDefinitionSelectorDataGrid",null);
		%>		
	  	<%= gridHtml %>	  	
   		</div>
  		
  		<%--the following script loads the datagrid.  note that this dialog is a DialogSimple to allow scripts to execute after it loads on the page--%>
		<script type='text/javascript'>
  			require(["t360/datagrid", "dojo/domReady!"],function( t360grid ) {
		<%
  				String poUploadDefnGridLayout = dgFactory.createGridLayout("invDefnGrid", "InvDefinitionSelectorDataGrid");
		%>
    			var gridLayout = <%= poUploadDefnGridLayout %>;
    	<%
	    	 	//get the grid data
	    	  	//this reevaluates from the user session data rather than taking passed in bank branch
	    	  	//values for security
	    	  	StringBuffer poUplDefnGridData = new StringBuffer();
    
    			
	    		for ( int poIdx = 0; poIdx<poUplDefnList.size(); poIdx++ ) {
					DocumentHandler poDoc = (DocumentHandler) poUplDefnList.get(poIdx);
					String poOid = poDoc.getAttribute("/INV_UPLOAD_DEFINITION_OID");
					poOid = EncryptDecrypt.encryptStringUsingTripleDes(poOid, userSession.getSecretKey());
					String poName = poDoc.getAttribute("/NAME");
					poUplDefnGridData.append("{ ");
					poUplDefnGridData.append("rowKey:'").append(poOid).append("'");
					poUplDefnGridData.append(", ");
					poUplDefnGridData.append("INVDEFINITIONNAME:'").append(poName).append("'");
					poUplDefnGridData.append(" }");
	  	      
					if ( poIdx < poUplDefnList.size()-1) {
	  	        		poUplDefnGridData.append(", ");
	  	      		}
	  	    	}//end of for
     	%>
	    		var gridData = [ <%= poUplDefnGridData.toString() %> ];
				<%--create a local memory grid--%>
				t360grid.createMemoryDataGrid("invDefnGrid", gridData, gridLayout);
	  		});
  		</script>  		
	<%
		}else if(poUplDefnList.size() == 0){
	%>
		<div id="poUploadDefnSelectorDialogContent" class="dialogContent">
			
		<%
			if (canCreate){
		%>
	 			<br>
				<%=widgetFactory.createLabel("", "InvOnlyRuleDefnSelector.CreateInvDefinitionMsg", false, false, false, "")%>
				<br>
				<div class="gridFooter">
				<button id="CreatePOUploadDefn" data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
					<%= resMgr.getText("InvOnlyRuleDefnSelector.CreateInvDefinition", TradePortalConstants.TEXT_BUNDLE) %>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						openURL("<%=formMgr.getLinkAsUrl("selectInvoiceDefinition",response)%>");  
					</script>
				</button>
				<%=widgetFactory.createHoverHelp("CreatePOUploadDefn","NewHoverText") %>
		<%
			}else{
		%>
 	 			<br>
				<%=widgetFactory.createLabel("", "InvOnlyRuleDefnSelector.NoInvDefinitionMsg", false, false, false, "")%>
				<br>
		<% 
			}
 		%>
 				<button id="POUploadDefnSelector_cancel" data-dojo-type="dijit.form.Button" type="button" class="gridFooterAction">
					<%= resMgr.getText("POUploadDefnSelector.cancel", TradePortalConstants.TEXT_BUNDLE) %>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						closeInvDialog();
					</script>
				</button>
				<%=widgetFactory.createHoverHelp("POUploadDefnSelector_cancel","common.Cancel") %>
				
				<div style="clear:both;"></div>
			</div>
		</div>
	<%
		}
	%>

<script type='text/javascript'>
  <%-- on select perform execute the select callback function --%>
  function selectInv() {
    <%-- todo: add external event handler to datagrid footer items --%>
    console.log ("Inside selectInv");
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      <%--get array of rowkeys from the grid--%>
      var rowKeys = getSelectedGridRowKeys("invDefnGrid");
      if (rowKeys && rowKeys.length == 1 ) {
        <%-- bankBranchSelectorDialogSelect(rowKeys[0]); --%>
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(poUploadDefnSelectorDialogId)%>', 'select',rowKeys[0]);
      }
    });
  }

  function closeInvDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(poUploadDefnSelectorDialogId)%>');
    });
  }

  	function openURL(URL){
  		console.log ("open url " + URL);
		document.location.href  = URL;
		return false;
	}  	
</script>