<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.text.SimpleDateFormat, com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, 
  com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
  com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, 
  com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 
   

  //read in parameters
//   String announcementOid = StringFunction.xssCharsToHtml(request.getParameter("announcementOid"));

  //todo: not sure if we need to get data based on user again.  
  //at a minimum we allow this dialog to be used by working on a specific announcement oid.
  //if that is specified, go ahead and pull in the data for display.
  //note that we don't need the criticality as the page that opened this
  // dialog needed that to specify the dialog style class

  String announcementTitle = StringFunction.xssCharsToHtml(request.getParameter("title"));
  String announcementSubject = StringFunction.xssCharsToHtml(request.getParameter("subject"));
		  
  boolean isCritical = Boolean.valueOf(StringFunction.xssCharsToHtml(request.getParameter("critical")));
/*   IR T36000010804 Made changes since the new line character was not getting preserved using the query
     Used AnnouncementWebBean instead to fetch the subject data; and preserved the new line character
	 @ Komal M
*/
  beanMgr.registerBean ("com.ams.tradeportal.busobj.webbean.AnnouncementWebBean",
          "Announcement");
  AnnouncementWebBean announcementBean =
          (AnnouncementWebBean)beanMgr.getBean("Announcement");
%>
<%WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>
<div class="dialogContent midwidth padded">

<%
  if ( announcementTitle!=null && announcementSubject!= null) {
    
    if (isCritical) {
%>
  <div class="announceTitle critical"><%= announcementTitle %></div>
  <div class="announceText criticalExists">
    <%= widgetFactory.formatStringWithBR(announcementSubject)%>
  </div>
<%
    } else {
%> 
  <div class="announceTitle"><%= announcementTitle %></div>
  <div class="announceText">
    <%= widgetFactory.formatStringWithBR(announcementSubject)%>
  </div>
<%
    }
  }
  else {
    //get all announcements that are applicable for the bank group
    int announcementCount = 0;
    boolean criticalExists = false;

    //get the user's bank org group and client bank
    String clientBankOid = userSession.getClientBankOid();
    String bogOid = userSession.getBogOid();


    //get the user date - which is specific to the user timezone!
    String currentGMTTime = DateTimeUtility.getGMTDateTime(true,false);//do not use override date
    Date myDate = TPDateTimeUtility.getLocalDateTime(
      currentGMTTime, userSession.getTimeZone());
    SimpleDateFormat isoDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    String myIsoDate = isoDateFormatter.format(myDate);


    //first look for criticals - we do this in 2 steps becuase ordering by 
    //critical_ind doesn't work with nulls...
    StringBuffer aSql = new StringBuffer();
    aSql.append("select a.announcement_oid,a.title, a.subject ");
    aSql.append("from announcement a, announcement_bank_group b ");
    aSql.append("where a.announcement_oid = b.p_announcement_oid(+) "); //outer join
    //get bank announcements that apply to the bank group
    aSql.append("and ( (a.p_owner_org_oid = ? and a.ownership_level='BANK' and ");
    aSql.append(       "(a.all_bogs_ind='Y' or b.a_bog_oid = ?) ) or ");
    aSql.append(      "(a.p_owner_org_oid = ? and a.ownership_level='BOG') ) ");
    aSql.append("and a.announcement_status = 'ACTIVE' ");
    aSql.append("and a.critical_ind = 'Y' ");
    aSql.append("and a.start_date <= to_date(?, 'YYYY-MM-DD')");
    aSql.append("and a.end_date >= to_date(?, 'YYYY-MM-DD')");
    aSql.append("order by a.start_date desc, a.ownership_level");
	

	Object sqlParams[] = new Object[5];
	sqlParams[0] = clientBankOid;
	sqlParams[1] = bogOid;
	sqlParams[2] = bogOid;
	sqlParams[3] = myIsoDate;
	sqlParams[4] = myIsoDate;
    DocumentHandler aListDoc = DatabaseQueryBean.getXmlResultSet(aSql.toString(), false, sqlParams);
    if ( aListDoc != null ) {
      Vector aList = aListDoc.getFragments("/ResultSetRecord");
      for (int idx = 0; idx<aList.size(); idx++ ) {
        DocumentHandler aDoc = (DocumentHandler) aList.get(idx);
        announcementCount++;
        criticalExists = true;
        announcementBean.setAttribute("announcement_oid", aDoc.getAttribute("/ANNOUNCEMENT_OID"));
        announcementBean.getDataFromAppServer();
        announcementTitle = aDoc.getAttribute("/TITLE");
        if ( announcementTitle == null ) announcementTitle = "";
        announcementSubject = announcementBean.getAttribute("subject"); 
        		
        if ( announcementSubject == null ) announcementSubject = "";
%>

  <div class="announceTitle critical"><%= StringFunction.xssCharsToHtml(announcementTitle) %></div>
  <div class="announceText critical">
    <%= widgetFactory.formatStringWithBR(StringFunction.xssCharsToHtml(announcementSubject))%>
  </div>
<%
      }
    }

    //now get the non-criticals
    aSql = new StringBuffer();
    aSql.append("select a.announcement_oid,a.title, a.subject ");
    aSql.append("from announcement a, announcement_bank_group b ");
    aSql.append("where a.announcement_oid = b.p_announcement_oid(+) "); //outer join
    //get bank announcements that apply to the bank group
    aSql.append("and ( (a.p_owner_org_oid = ? and a.ownership_level='BANK' and ");
    aSql.append(       "(a.all_bogs_ind='Y' or b.a_bog_oid = ?) ) or ");
    aSql.append(      "(a.p_owner_org_oid = ? and a.ownership_level='BOG') ) ");
    aSql.append("and a.announcement_status = 'ACTIVE' ");
    aSql.append("and (critical_ind != 'Y' or critical_ind is null) ");
    aSql.append("and a.start_date <= to_date(?, 'YYYY-MM-DD')");
    aSql.append("and a.end_date >= to_date(?, 'YYYY-MM-DD')");
    aSql.append("order by a.start_date desc, a.ownership_level");
  
	Object sqlParams1[] = new Object[5];
	sqlParams1[0] = clientBankOid;
	sqlParams1[1] = bogOid;
	sqlParams1[2] = bogOid;
	sqlParams1[3] = myIsoDate;
	sqlParams1[4] = myIsoDate;
	
    aListDoc = DatabaseQueryBean.getXmlResultSet(aSql.toString(), false, sqlParams1);
    if ( aListDoc != null ) {
      Vector aList = aListDoc.getFragments("/ResultSetRecord");
      for (int idx = 0; idx<aList.size(); idx++ ) {
        DocumentHandler aDoc = (DocumentHandler) aList.get(idx);
        announcementCount++;
        announcementBean.setAttribute("announcement_oid", aDoc.getAttribute("/ANNOUNCEMENT_OID"));
        announcementBean.getDataFromAppServer();
        announcementTitle = aDoc.getAttribute("/TITLE");
        if ( announcementTitle == null ) announcementTitle = "";
        announcementSubject = announcementBean.getAttribute("subject");  
        		
        if ( announcementSubject == null ) announcementSubject = "";

        if ( criticalExists ) {
%>

  <div class="announceTitle criticalExists"><%= StringFunction.xssCharsToHtml(announcementTitle)  %></div>
  <div class="announceText criticalExists">
    <%= widgetFactory.formatStringWithBR(StringFunction.xssCharsToHtml(announcementSubject))%>
  </div>
<%
        }
        else {
%>
  <div class="announceTitle"><%= StringFunction.xssCharsToHtml(announcementTitle) %></div>
  <div class="announceText">
    <%= widgetFactory.formatStringWithBR(StringFunction.xssCharsToHtml(announcementSubject))%>
  </div>
<%
        }
      }
    }

    //if nothing found just display the following.  this
    //should never happen!
    if ( announcementCount <= 0 ) {
      announcementTitle = "No Announcement!";
      announcementSubject = "Please try opening the dialog again.";
%>
  <div class="announceTitle"><%= StringFunction.xssCharsToHtml(announcementTitle) %></div>
  <div class="announceText">
    <%= widgetFactory.formatStringWithBR(StringFunction.xssCharsToHtml(announcementSubject))%>
  </div>
<%
    }
  }
%>
</div>

