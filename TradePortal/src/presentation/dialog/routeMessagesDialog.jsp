<%--
 *  Route Messages Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%

//read in parameters
 // String routeMessagesDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
String routeMessagesDialogId = request.getParameter("dialogId"); //Rel 9.3 XSS CID-11407, 11423
  //input: selected messages
  String selectedMessages = request.getParameter("selectedMessages");
%>

<div id="routeMessagesDialogContent">

  <p>Select a recipient Person or Organisation:</p>
  <p>John Doe, (usr ID), ABC Company</p>

  <%-- on route items, execute the select callback function --%>
  <button onclick="routeMessagesDialogRouteItems('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(selectedMessages))%>','dest1');hideDialog('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(routeMessagesDialogId))%>');">Route Item(s) - dest1</button>
  <button onclick="routeMessagesDialogRouteItems('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(selectedMessages))%>','dest2');hideDialog('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(routeMessagesDialogId))%>');">Route Item(s) - dest2</button>
  <a onclick="hideDialog('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(routeMessagesDialogId))%>');">Cancel</a>

  <p>----------------------------</p>
  <p>Selected items(s): 3</p>
  <p>Subject</p>
  <p>Selected messages: <%=StringFunction.xssCharsToHtml(selectedMessages)%></p>

</div>
