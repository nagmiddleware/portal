
<%--
 *  Template Search Dialog content
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>
<%


//read in parameters
	
	//Pull the bankBranchArray value which is sending from 360/menu.js where this page is calling as a open dialog.
	//bankBranchArray is a  array of Bank/Branch OIDs we need to get the array of values.
	String bankBranchArrayTemplate = StringFunction.xssCharsToHtml(request.getParameter("bankBranchArray"));
	String[] bankBranchArrayTemplateArray = new String[0];
	
	if(bankBranchArrayTemplate == null){
		//create some javascript variables that can be used by the menu event handlers events
		  //
		  //  bankBranchArray - array of operational bank org oids associated with the user's corp org
		  	
		  String tmpOwnerOrg = userSession.getOwnerOrgOid();
		  CorporateOrganizationWebBean tmpCorpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
		  tmpCorpOrg.getById(tmpOwnerOrg);
	
		  String[] tmpOpBankOid = {tmpCorpOrg.getAttribute("first_op_bank_org"),
				  tmpCorpOrg.getAttribute("second_op_bank_org"),
				  tmpCorpOrg.getAttribute("third_op_bank_org"),
				  tmpCorpOrg.getAttribute("fourth_op_bank_org")};
		  
		  bankBranchArrayTemplate = "";
		  for (int bbaIdx=0; bbaIdx<4; bbaIdx++)
		  {
		     if (tmpOpBankOid[bbaIdx] != null && !"".equals(tmpOpBankOid[bbaIdx])) {
		          if ( bankBranchArrayTemplate.length()>0 ) {
		        	  bankBranchArrayTemplate += ",";
		          }
		          bankBranchArrayTemplate += tmpOpBankOid[bbaIdx];
		      }
		      else {
		          break; //if a blank is found there will be no more
		      }
		  }
	}
	
	if(bankBranchArrayTemplate != null){
		bankBranchArrayTemplateArray = bankBranchArrayTemplate.split(",");	
	}
	
	Debug.debug("*** bankBranchArrayTemplate: "+bankBranchArrayTemplate);
	Debug.debug("*** bankBranchArrayTemplateArray.length: "+bankBranchArrayTemplateArray.length);
	
	String templateSearchDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
    String createTemplFromTemplFlag = StringFunction.xssCharsToHtml(request.getParameter("createTemplFromTemplFlag"));
	System.out.println("createTemplFromTemplFlag:"+createTemplFromTemplFlag);
	//think this is what is necessary to limit initially
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	
	  String       gridLayout      = "";
	  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	String gridHtml = "";
	Debug.debug("***START**************** CONVERSION TEMPLATE SEARCH***************START***");
	boolean newTemplate = true;     // indicates what page we came from (i.e.,
	                                // doing new template or new transaction)
	StringBuffer dynamicWhere  = new StringBuffer();;
	String link = "";
	String temp = ""; 
	int dbCount			  = 0; 
	String OwnerOrgOid 	  = null; 
	String parentOrgID    = userSession.getOwnerOrgOid();
	String bogID          = userSession.getBogOid();
	String clientBankID   = userSession.getClientBankOid();
	String globalID       = userSession.getGlobalOrgOid();
	String ownershipLevel = userSession.getOwnershipLevel();
	String pUserOid 	  = null;
	String loginLocale = userSession.getUserLocale();
	String loginRights = userSession.getSecurityRights();
	String standbys_use_guar_form_only = null;
	String standbys_use_either_form = null;
	String sqlTemp = null;
	String bankBranch;
	String isFixed = null;	
	
//If we are in the transaction area the parameters will be passed in via this document
  HttpSession theSession = request.getSession(false);
  String inTransactionArea = (String) theSession.getAttribute("inNewTransactionArea");
  String templateListViewXML = "TemplateSearchListView.xml";
  OwnerOrgOid = userSession.getOwnerOrgOid();
  pUserOid = userSession.getUserOid();
  dbCount = DatabaseQueryBean.getCount("AUTHORIZED_TEMPLATE_GROUP_OID", "USER_AUTHORIZED_TEMPLATE_GROUP", " p_user_oid = ?",false, new Object[]{userSession.getUserOid()});
  // Now read in the search parameters to redisplay to the user.
  String templateName = StringFunction.xssCharsToHtml(request.getParameter("TemplateName"));
  boolean OrgDataUnderSubAccess = userSession.showOrgDataUnderSubAccess();
  Long organization_oid = new Long( userSession.getOwnerOrgOid() );
  Vector instrumentTypes = Dropdown.getActualEditableInstrumentTypes(formMgr, organization_oid.longValue(), loginRights, userSession.getSecurityType(), ownershipLevel);  
	  UserWebBean loggedUser = beanMgr.createBean(UserWebBean.class, "User");
	  loggedUser.setAttribute("user_oid", userSession.getUserOid());
	  loggedUser.getDataFromAppServer();
	  String confInd = "";
	  if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
	      confInd = TradePortalConstants.INDICATOR_YES;
	  else if (userSession.getSavedUserSession() == null)
	      confInd = loggedUser.getAttribute("confidential_indicator");
	  else   
	  {
	      if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
	         confInd = TradePortalConstants.INDICATOR_YES;
	      else	
	         confInd = loggedUser.getAttribute("subsid_confidential_indicator");  
	  }   
CorporateOrganizationWebBean corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
corporateOrg.getDataFromAppServer();
standbys_use_guar_form_only = corporateOrg.getAttribute("standbys_use_guar_form_only");
standbys_use_either_form = corporateOrg.getAttribute("standbys_use_either_form");%>
<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);

%>
<%-- ********************* HTML for page begins here *********************  --%>
<div id="templateSearchDialogContent234" class="dialogContent fullwidth">

  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">

      <%= widgetFactory.createSearchTextField("TemplateSearchName","TemplateSearch.TemplateSearch","30","onKeydown='Javascript: filterTemplatesOnEnter(\"TemplateSearchName\");'")%>

    </span>
    <span class="searchActions">
      <button data-dojo-type="dijit.form.Button" type="button" id="templateSearchButton" name="templateSearchButton"> 
      <%=resMgr.getText("common.search", TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          searchBasic();return false;
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("templateSearchButton","SearchHoverText") %>
    </span>
    <div style="clear:both"></div>
  </div>

<%  
  gridHtml = dgFactory.createDataGrid("instruSearchGrid1","InstrumentTemplateSearchDataGrid",null);
  gridLayout = dgFactory.createGridLayout("InstrumentTemplateSearchDataGrid");
%>
<%= gridHtml %>
 

</div>

<script type='text/javascript'>
var createTemplFromTemplFlag = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(createTemplFromTemplFlag))%>';
  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {
    var gridLayout = <%=gridLayout%>;   
    var initSearchParms = "standbys_use_guar_form_only=<%=standbys_use_guar_form_only%>&standbys_use_either_form=<%=standbys_use_either_form%>&OrgDataUnderSubAccess=<%=OrgDataUnderSubAccess%>&dbCount=<%=dbCount%>&confInd=<%=confInd%>&convCenter=Y";
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InstrumentTemplateSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var myGrid = t360grid.createDataGrid("instruSearchGrid1", viewName,gridLayout, initSearchParms);
    <%-- cquinton 10/16/2012 ir#6448 --%>
    <%-- because its on a dialog explicitly set to 10 rows so it doesn't take over the browser --%>
    myGrid.set('autoHeight',10); 

  });
</script>
<script type='text/javascript'>
	function filterTemplatesOnEnter(fieldId){
		<%-- 
	  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
	  	 --%>
	  	require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	searchBasic();
		        }
		    });
		});
	}

  var rowKeys ="";
  function searchBasic(){
		require(["dojo/dom", "dojo/domReady!"],
	      function(dom){
	        var searchParms ="standbys_use_guar_form_only=<%=standbys_use_guar_form_only%>&standbys_use_either_form=<%=standbys_use_either_form%>&OwnerOrgOid=<%=OwnerOrgOid%>&OrgDataUnderSubAccess=<%=OrgDataUnderSubAccess%>&dbCount=<%=dbCount%>&confInd=<%=confInd%>";
	        var templateName = dom.byId("TemplateSearchName").value;
	        if (templateName != "")	
		        searchParms+="&templateName="+templateName;        
	        if (searchParms != "") {
	          var strLen = searchParms.length;
	          searchParms = searchParms.slice(0,strLen);      
	        }
	        searchDataGrid("instruSearchGrid1", "InstrumentTemplateSearchDataView",searchParms);
	      });
  }
  
  <%-- on select perform execute the select callback function --%>
  function selectTS() {
    <%-- todo: add external event handler to datagrid footer items --%>
    
    <%-- if array length is equal to one then no need to call the BB popup, and pass the BBOID to target page --%>
    <%-- if the array length is more than one then open the BB popup. --%>
    var BBSelectorArray = '<%=bankBranchArrayTemplateArray.length%>';
    <%-- MEer T36000035658 To fix CID-11250 XSS issue --%>
    var BBOID = '<%=StringFunction.escapeQuotesforJS(bankBranchArrayTemplate)%>';

    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
	 function(registry, query, on, dialog ) {
      <%--get array of rowkeys from the grid--%>
      rowKeys = getSelectedGridRowKeys("instruSearchGrid1");
      if(rowKeys == ""){
	  		alert("Please pick a record and click 'Select'");
	  		return;
	  }
		 <%--  	
      if(createTemplFromTemplFlag == 'Y'){
    	  dialog.doCallback('<%=StringFunction.escapeQuotesforJS(templateSearchDialogId)%>', 'select',rowKeys[0]);
      }else{
	    if(BBSelectorArray == 1){
	    	  createNewInstrumentBankBranchSelectedValue(BBOID);
	      }else{  --%>
    	  	dialog.open('bankBranchSelectorDialog', bankBranchSelectorDialogTitle,
	              'ExternalOpOrgDialog.jsp',
	              null, null, <%-- parameters --%>
	              'select', this.createNewInstrumentBankBranchSelectedValue);
      		<%-- } --%>
     <%--  }     --%>
    });
  }

  function createNewInstrumentBankBranchSelectedValue(bankBranchOid,manualInstrumentOid) {
		
		
		
		if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
	        var theForm = document.getElementsByName('NewInstrumentForm')[0];
	      	theForm.copyInstrumentOid.value=rowKeys[0];
	      	theForm.bankBranch.value=bankBranchOid;
			theForm.manualInstrumentOid.value=manualInstrumentOid;
	      	theForm.mode.value='CREATE_NEW_INSTRUMENT';
	        theForm.copyType.value='Templ';
  	 	    theForm.conversionCenterMenuInd.value="Y";
			theForm.ConvTransInd.value='Y';	
	    	
	  		
	    		  
	      	theForm.submit();
	      } else {
	        	alert('Problem in createNewInstrument: cannot find ExistingInstrumentsForm');
	      }
	}
  
  function closeTSDialog() {
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(templateSearchDialogId)%>');
    });
  }

  	var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(templateSearchDialogId)%>");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","templateSearchDialogCloseLink");
	dialogCloseButton.setAttribute("title","");
</script>
	<%=widgetFactory.createHoverHelp("templateSearchDialogCloseLink", "common.Cancel") %>
