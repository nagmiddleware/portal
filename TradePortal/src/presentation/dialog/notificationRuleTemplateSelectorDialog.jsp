<%--
 *  Notification Rule Template content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Srinivasu Doddapaneni
 *	   CR 927b Rel9.5 10/01/2016 - Template selector
 *	   Copyright   2012                         
 *     CGI, Incorporated 
 *     All rights reserved
--%>
	<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
	                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>
	
	<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
	<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
	<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
	<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

	<% 
	    
		//read in parameters
		String notificationTemplateSelectorDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
		
		WidgetFactory widgetFactory = new WidgetFactory(resMgr);
		DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
		String gridHtml = "";
		
		StringBuilder sql = new StringBuilder();
		QueryListView queryListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView"); 
		List<Object> sqlParamsNoti = new ArrayList();
		
		  sql.append("select notification_rule_oid, description ");
		  sql.append(" from notification_rule");
		  sql.append(" where p_owner_org_oid in ( ?"  );
		  sqlParamsNoti.add(userSession.getClientBankOid());
  		     // Also include Notification Templates from the user's actual organization if using subsidiary access
  		  if(userSession.showOrgDataUnderSubAccess()){
      		sql.append(",?");
      		sqlParamsNoti.add(userSession.getSavedUserSession().getOwnerOrgOid());
   		  }

		sql.append(") and template_ind = ?");
		sql.append(" order by ");
		sql.append(resMgr.localizeOrderBy("description"));
		sqlParamsNoti.add(TradePortalConstants.INDICATOR_YES);
		//System.out.println("sql :"+sql.toString()+"\t sqlParamsNoti:"+sqlParamsNoti);
		DocumentHandler notificationTemDefListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true,sqlParamsNoti);
		//System.out.println("notificationTemDefListDoc :"+notificationTemDefListDoc);
		Vector<DocumentHandler> notificationTempList = new Vector();
		
		if ( notificationTemDefListDoc != null ){
			notificationTempList = notificationTemDefListDoc.getFragments("/ResultSetRecord");
		}

		if(notificationTempList.size() != 0 && notificationTempList.size() >=1){	
	%>
	<br>
	&nbsp;&nbsp;&nbsp;<%=widgetFactory.createSubLabel("NotificationTemplate.NotificationTemplateInstruction")%>
		<div id="notificationTemplateSelectorDialogContent" class="notificationTempldialogContent">
		
		<%
	  		gridHtml = dgFactory.createDataGrid("notificationTemplateDefnGrid","NotificationTemplateSelectorDataGrid",null);
		%>
	  		<%= gridHtml %>
	  		
	  		
  		</div>
  		
  		<%--the following script loads the datagrid.  note that this dialog is a DialogSimple to allow scripts to execute after it loads on the page--%>
		<script type='text/javascript'>
  			require(["t360/datagrid", "dojo/domReady!"],function( t360grid ) {
		<%
  				String notiRuleGridLayout = dgFactory.createGridLayout("notificationTemplateDefnGrid", "NotificationTemplateSelectorDataGrid");
		%>
    			var gridLayout = <%= notiRuleGridLayout %>;
    	<%
	    	 	//get the grid data
	    	  	//this reevaluates from the user session data rather than taking passed in bank branch
	    	  	//values for security
	    	  	StringBuffer notiRuleGridData = new StringBuffer();
	    	

	    		for ( int ruleIdx = 0; ruleIdx<notificationTempList.size(); ruleIdx++ ) {
					DocumentHandler poDoc = (DocumentHandler) notificationTempList.get(ruleIdx);
					String ruleOid = poDoc.getAttribute("/NOTIFICATION_RULE_OID");
					ruleOid = EncryptDecrypt.encryptStringUsingTripleDes(ruleOid, userSession.getSecretKey());
					String ruleDesc = poDoc.getAttribute("/DESCRIPTION");
					notiRuleGridData.append("{ ");
					notiRuleGridData.append("rowKey:'").append(ruleOid).append("'");
					notiRuleGridData.append(", ");
					notiRuleGridData.append("NotificationTemplateSelector:'").append(StringFunction.xssCharsToHtml(ruleDesc)).append("'");
					notiRuleGridData.append(" }");
	  	      
					if ( ruleIdx < notificationTempList.size()-1) {
	  	        		notiRuleGridData.append(", ");
	  	      		}
	  	    	}//end of for
    	     
    	%>
	    		var gridData = [ <%= notiRuleGridData.toString() %> ];
				<%--create a local memory grid--%>
				t360grid.createMemoryDataGrid("notificationTemplateDefnGrid", gridData, gridLayout);
	  		});
  		</script>
	<%
		}
	%>

<script type='text/javascript'>
  <%-- on select perform execute the select callback function --%>
  function selectNTemplate() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      <%--get array of rowkeys from the grid--%>
      var rowKeys = getSelectedGridRowKeys("notificationTemplateDefnGrid");
      if (rowKeys && rowKeys.length == 1 ) {
        //bankBranchSelectorDialogSelect(rowKeys[0]);
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(notificationTemplateSelectorDialogId)%>', 'select',rowKeys[0]);
      }else{
    	  alert('<%= resMgr.getText("NotificationTemplate.selectwarningmessage", TradePortalConstants.TEXT_BUNDLE) %>');
      }
    });
  }

  function closeNTDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
		dialog.doCallback('<%=StringFunction.escapeQuotesforJS(notificationTemplateSelectorDialogId)%>', 'select',null);
		 dialog.hide('<%=StringFunction.escapeQuotesforJS(notificationTemplateSelectorDialogId)%>');
	    
    });
  }

  	function openURL(URL){
		document.location.href  = URL;
		return false;
	}

  	var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(notificationTemplateSelectorDialogId)%>");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","NotificationTemplateSelectorDialogCloseLink");
	dialogCloseButton.setAttribute("title","");
</script>
	<%=widgetFactory.createHoverHelp("NotificationTemplateSelectorDialogCloseLink", "common.NotiCancel") %>
