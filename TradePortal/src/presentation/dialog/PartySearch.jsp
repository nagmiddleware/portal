<%--
/******************************************************************************
 * PartySearch.jsp
 *
 * Filtering:
 * This page originally displays a listview containing all possible parties,
 * but the parties can be filtered using a text box and a link that redisplays
 * the page with the filter text passed as a url parameter.  JavaScript is used
 * to dynamically build that link.
 *
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/
--%>
<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
		 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<%
  Debug.debug("***START********************PARTY*SEARCH**************************START***");


  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  String returnAction = "";
  String partyType        = null;
  String partyTypeCode    = null;
  String itemid				= null;
  String unicodeIndicator = null;  // CR-486 05/24/10
  boolean userFlag   =false;
  String tempOwnerOrgOid = null;
  String userSecurityRights = null;
  //cquinton 2/5/2013 remove newLink variable - we submit the form instead
  String partySearchDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));

  boolean showCreateButton = true;

  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = "";
  String gridLayout = ""; 

  //Hashtable secureParams = new Hashtable();

  //returnAction is used to determine whether new button is displayed
  //maybe should be renamed -- i.e. showNew
  returnAction = StringFunction.xssCharsToHtml(request.getParameter("returnAction"));
  // filterText      = StringFunction.xssCharsToHtml(request.getParameter("filterText"));
  partyType       = StringFunction.xssCharsToHtml(request.getParameter("partyType"));
  itemid 			= StringFunction.xssCharsToHtml(request.getParameter("itemid"));
  unicodeIndicator = StringFunction.xssCharsToHtml(request.getParameter("unicodeIndicator"));   // CR-486 05/24/10


  //Resolve the party type code to search for
  if (partyType.equals(TradePortalConstants.ADVISING_BANK) ||
    partyType.equals(TradePortalConstants.TRANSFEREE_NEGOT) ||
    partyType.equals(TradePortalConstants.DESIG_BANK) ||
    partyType.equals(TradePortalConstants.COLLECTING_BANK) ||
    partyType.equals(TradePortalConstants.OVERSEAS_BANK) ||
    partyType.equals(TradePortalConstants.OPENING_BANK) ||
    partyType.equals(TradePortalConstants.ASSIGNEE_BANK) ||
    partyType.equals(TradePortalConstants.ADVISE_THROUGH_BANK) ||
    partyType.equals(TradePortalConstants.REIMBURSING_BANK) ||
    partyType.equals(TradePortalConstants.APPLICANT_BANK) ||
    partyType.equals(TradePortalConstants.BENEFICIARY_BANK) ||
	partyType.equals(TradePortalConstants.ORDERING_PARTY_BANK) || //IAZ IR-PMUK012737723 02/03/10 Add   
	partyType.equals(TradePortalConstants.PAYEE_BANK))
  {
    partyTypeCode = TradePortalConstants.PARTY_TYPE_BANK;
  }
  else if (partyType.equals(TradePortalConstants.BENEFICIARY) ||
         partyType.equals(TradePortalConstants.APPLICANT) ||
         partyType.equals(TradePortalConstants.FREIGHT_FORWARD) ||
         partyType.equals(TradePortalConstants.TRANSFEREE) ||
         partyType.equals(TradePortalConstants.ASSIGNEE_01) ||
         partyType.equals(TradePortalConstants.DRAWEE_BUYER) ||
         partyType.equals(TradePortalConstants.DRAWER_SELLER) ||
         partyType.equals(TradePortalConstants.RELEASE_TO_PARTY) ||
         partyType.equals(TradePortalConstants.AGENT) ||
	   partyType.equals(TradePortalConstants.NOTIFY_PARTY) ||
	   partyType.equals(TradePortalConstants.OTHER_CONSIGNEE) ||
	   partyType.equals(TradePortalConstants.CASE_OF_NEED) ||
	   partyType.equals(TradePortalConstants.PAYEE) ||
	   partyType.equals(TradePortalConstants.PAYER) ||
	    partyType.equals(TradePortalConstants.ORDERING_PARTY) || //Vshah CR507
	   partyType.equals(TradePortalConstants.ATP_SELLER) )//rkrishnap CR 375-D ATP-ISS 07/26/2007
  {
    partyTypeCode = TradePortalConstants.PARTY_TYPE_CORP;
  }
  //rkrishnap CR 375-D ATP-ISS 07/26/2007 Begin
  else if (partyType.equals(TradePortalConstants.INSURING_PARTY))
  {
    partyTypeCode = "'"+TradePortalConstants.PARTY_TYPE_CORP+"','"+TradePortalConstants.PARTY_TYPE_BANK+"'";
  }
  //rkrishnap CR 375-D ATP-ISS 07/26/2007 End
  else
  {
    Debug.debug("INVALID PARTY TYPE -> " + partyType);
    partyTypeCode = "";
  }

  gridHtml = dgFactory.createDataGrid("PartySearchNewDataGridId","PartySearchNewDataGrid",null);
  gridLayout = dgFactory.createGridLayout("PartySearchNewDataGridId", "PartySearchNewDataGrid");
%>

<div class="dialogContent fullwidth">
  
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
  
      <%= widgetFactory.createSearchTextField("filterText", "PartySearch.SearchPartyName", "30",
            "onKeydown='Javascript: filterPartySearchOnEnter(\"filterText\");'")%>
      
    </span>
    <span class="searchActions">

      <button data-dojo-type="dijit.form.Button" id="SearchPartyButton" type="button" class="gridFooterAction">
       <%=resMgr.getText("common.search",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          searchParty();return false;
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("SearchPartyButton", "SearchHoverText") %>
     
<%
  //rbhaduri - 31st March 06 - NNUG031554019 - Add Begin - If corporate customer is not alllowed to add bank
  //parties, the create party button should not be visible

  if (partyTypeCode.equals(TradePortalConstants.PARTY_TYPE_BANK)) {
    CorporateOrganizationWebBean corporateOrg = null;
    corporateOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,"CorporateOrganization");
    corporateOrg.setAttribute("organization_oid", userSession.getOwnerOrgOid());
    corporateOrg.getDataFromAppServer();
    showCreateButton = corporateOrg.getAttribute("allow_create_bank_parties").equals(TradePortalConstants.INDICATOR_YES);
  }
  //rbhaduri - 31st March 06 - NNUG031554019 - Add End

  if (showCreateButton) {
    // Make sure the user has the correct security right
    userSecurityRights = userSession.getSecurityRights();
    if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.CREATE_NEW_TRANSACTION_PARTY) 
        &&(SecurityAccess.hasRights(userSecurityRights, SecurityAccess.MAINTAIN_PARTIES)
        ||SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_PARTIES))) {
      if (!returnAction.equals("selectTransactionParty")) {
        // If the user was on a PartyDetailNew page or PartyDetail page,
        // and clicks on "Select the Designated Bank"
        // to navigate to this page again, do not display "Create a Party",
        // i.e. no nested "Create a Party".  That is too much trouble to achieve.
        showCreateButton = false;
      }
    }
    else {
      showCreateButton = false;
    }
  }
  if (showCreateButton) {
    //cquinton 2/5/2013 remove newLink variable - we submit the form instead
%>
      <button data-dojo-type="dijit.form.Button" type="button" id="newPartyButton">
        <%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          partySearchDialog.newParty();return false;
        </script>
      </button>
      <%=widgetFactory.createHoverHelp("newPartyButton", "NewPartyHoverText") %>

<%
  }
%>
    </span>
    <div style="clear:both"></div>

  </div>  

  <%=gridHtml %>
			
  <%--<%= formMgr.getFormInstanceAsInputField("PartySearchChooseForm", secureParams) %>--%>
</div>
  	
<script type="text/javascript">

  var partySearchDialog = {};

  require(["t360/common"], function(common) {

    <%-- cquinton 2/5/2013 when transition to party detail page, submit the form
      so we save existing data --%>
    partySearchDialog.newParty = function() {
      <%-- note: this assumes first form is the instrument one! --%>
      common.setButtonPressed('NewParty','0');
      document.forms[0].submit();
    };

  });


function filterPartySearchOnEnter(fieldID){
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchParty();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterPartySearchOnEnter() --%>
  var gridLayout = <%=gridLayout%>;
  var initSearchParms="partyType=<%=StringFunction.escapeQuotesforJS(partyType)%>&partyTypeCode=<%=partyTypeCode%>&userFlag=<%=userFlag%>&tempOwnerOrgOid=<%=tempOwnerOrgOid%>&unicodeIndicator=<%=StringFunction.escapeQuotesforJS(unicodeIndicator)%>";

  console.log("initSearchParms: "+initSearchParms);
  console.log('gridLayout='+gridLayout);
  require(["t360/datagrid"],  function(t360){
	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("PartySearchNewDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>  
    var myGrid = t360.createDataGrid("PartySearchNewDataGridId",viewName,gridLayout,initSearchParms);	
    <%-- cquinton 10/16/2012 ir#6448 --%>
    <%-- because its on a dialog explicitly set to 10 rows so it doesn't take over the browser --%>
    myGrid.set('autoHeight',10); 
  });

function searchParty() {
			
			 require(["dojo/dom"],
				      function(dom){
			    	
			    
			    	var filterText=(dom.byId("filterText").value).toUpperCase();
			    	
			    	var searchParams = initSearchParms+"&filterText="+filterText;
			    	
			        console.log("SearchParmas: " + searchParams);
			        searchDataGrid("PartySearchNewDataGridId", "PartySearchNewDataView",
			        		searchParams);
			      });
	
}

  <%-- cquinton - please do NOT write code like this --%>
  <%--  this does not allow the dialog to be used generically --%>
  <%--  use the dialog callback facility instead!!! --%>
  function chooseParty() {

    require(["dojo/ready","dijit/registry","dojo/_base/array","dojo/query","dojo/domReady!","dojo/on","t360/dialog"], 
				function(ready,registry,baseArray,query,dom, on, dialog) {
			
			myGrid = registry.byId("PartySearchNewDataGridId");
			items = myGrid.selection.getSelected();
			var selection = getSelectedGridRowKeys('PartySearchNewDataGridId');
			var itemsObj = String("<%=StringFunction.escapeQuotesforJS(itemid)%>");
			var itemsArr = itemsObj.split(',');
			 if(selection == ""){
			  				alert("Please select a party or press 'Cancel'");
			  				return;
			  			}
      <%--  get the data of each column of selected grid and set the data to fields in the form --%>
      var selectedRowKey = items[0].i.rowKey;

			var arr = [];
			<%-- "rowKey","contact_name","contact_title","contact_phone","contact_fax","contact_email","fax_1","fax_2","phone_number","PartyName", --%>
			<%-- "AddressLine1","AddressLine2","AddressLine3","AddressLine4","BranchCode","DesignatedBank","City","Country","VendorId" --%>
			
			 arr[0] = items[0].i.rowKey;
			 arr[1] = items[0].i.contact_name;
			 arr[2] = items[0].i.contact_title;
			 arr[3] = items[0].i.contact_phone;
			 arr[4] = items[0].i.contact_fax;
			 arr[5] = items[0].i.contact_email;
			 arr[6] = items[0].i.fax_1;
			 arr[7] = items[0].i.fax_2;
			 arr[8] = items[0].i.phone_number;
			 arr[9] = items[0].i.SearchPartyName;
			 arr[10] = items[0].i.AddressLine1;
			 arr[11] = items[0].i.AddressLine2;
			 arr[12] = items[0].i.AddressLine3;
			 arr[13] = items[0].i.AddressLine4;
			 arr[14] = items[0].i.BranchCode;
			 arr[15] = items[0].i.PartyOID;
			 arr[16] = items[0].i.City;
			 arr[17] = items[0].i.Country1;
			 arr[18] = items[0].i.VendorId;
			 arr[19] = items[0].i.Country;
			 arr[20] = items[0].i.OTLCustomerId;
			 arr[21] = items[0].i.DesignatedBank;
			 arr[22] = items[0].i.UserDefinedField1;
			 arr[23] = items[0].i.UserDefinedField2;
			 arr[24] = items[0].i.UserDefinedField3;
			 arr[25] = items[0].i.UserDefinedField4;
			matchPartyPatternsByPageName(itemsArr, arr, section);

      <%-- cquinton 1/4/2013 do callback now if it exists --%>
      <%--  all of the above should be in the callback! --%>
      dialog.doCallback('<%=StringFunction.escapeQuotesforJS(partySearchDialogId)%>','select', selectedRowKey);

      dialog.hide('<%=StringFunction.escapeQuotesforJS(partySearchDialogId)%>');

    });

  }

  <%--cquinton 1/7/2013 close the dialog to remove widgets, callbacks instead of just hiding--%>
  function cancelOne(){
    require(["t360/dialog"], function(dialog) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(partySearchDialogId)%>');
    });
  }

  var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(partySearchDialogId)%>");
  var dialogCloseButton=dialogName.closeButtonNode;
  dialogCloseButton.setAttribute("id","PartySearchDialogCloseButton");	
</script>
<%=widgetFactory.createHoverHelp("PartySearchDialogCloseButton", "common.Cancel") %>
