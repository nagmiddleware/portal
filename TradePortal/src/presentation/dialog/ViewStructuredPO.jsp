<%--
*******************************************************************************
                            View Structured PO Page

  Description:  The View PO page is used to view the list of POs  assigned to 
  				the transaction  Currently, this page is accessible only to Import 
                LC - Issue , Import LC - Amend , ATP transaction types.


*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.busobj.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,java.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

	
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
   String gridLayout = null;
   String gridHtml = null;
   TransactionWebBean              transaction        = null;
   StringBuffer                    dynamicWhereClause = null;
   Hashtable                       secureParms        = null;
   String                          userSecurityType   = null;
   String                          transactionType    = null;
   String                          transactionOid     = null;
   String                          userOrgOid         = null;
   String                          link               = null;
   String                          shipmentOid         = null;
   int                             numberOfRows       = 0;
   boolean 						   fromAddStrcuPO     = false;

   // Get the user's security type and organization oid
   userSecurityType = userSession.getSecurityType();
   userOrgOid       = userSession.getOwnerOrgOid();

   // Get the transaction oid from the current transaction web bean
   transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

   transactionOid  = transaction.getAttribute("transaction_oid");

   DocumentHandler xmlDoc = formMgr.getFromDocCache();
   shipmentOid         = request.getParameter("shipmentOid");//xmlDoc.getAttribute("/In/SearchForPO/shipment_oid");
  
  // Set a bunch of secure parameters necessary for the Remove Structured PO mediator
   secureParms = new Hashtable();
   secureParms.put("transactionOid", transactionOid);
   secureParms.put("shipmentOid", shipmentOid);
%>

<%-- ********************* HTML for page begins here *********************  --%>

<form name="ViewStructuredPOForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
<div class=POPopUpContent>
<%= formMgr.getFormInstanceAsInputField("ViewStructuredPOForm", secureParms) %>
<input type=hidden value="" name=buttonName>

<%@ include file="/transactions/fragments/StructuredPOSearch.frag"%>

<%
gridLayout = dgFactory.createGridLayout("viewStructurePODataDataGridId", "ViewStructurePODataGrid");
gridHtml = dgFactory.createDataGrid("viewStructurePODataDataGridId","ViewStructurePODataGrid",null);
%>
<%=gridHtml%>

</div>
</form>

    <script type='text/javascript'>
    
		var gridLayout = <%= gridLayout %>;			
						
		var intSearchParms="userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";			
		    intSearchParms+="&transactionOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(transactionOid, userSession.getSecretKey())%>";
		    intSearchParms+="&shipmentOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(shipmentOid, userSession.getSecretKey())%>";
		
		var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("ViewStructurePODataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
		createDataGrid("viewStructurePODataDataGridId", viewName, gridLayout, intSearchParms);
		
		function filterPOs(){
			require(["dojo/dom","dojo/domReady!" ], function(dom) {
			console.log("Inside filterPOs()");
			var benName = dom.byId("BeneName").value;
			var poNumber = dom.byId("PONum").value;
			var currency = dom.byId("Currency").value;
			var amountFrom = dom.byId("AmountFrom").value;
			var amountTo = dom.byId("AmountTo").value;
			
			var searchParms = intSearchParms;
			    searchParms+="&beneficiaryName="+benName;
			    searchParms+="&poNumber="+poNumber;
			    searchParms+="&currency="+currency;
			    searchParms+="&amountFrom="+amountFrom;
			    searchParms+="&amountTo="+amountTo;
											
			console.log("searchParms="+searchParms);
			searchDataGrid("viewStructurePODataDataGridId", "ViewStructurePODataView", searchParms);
		});
		}
		
		function closeStructurePODataItem(){
			
		require(["dijit/registry"],
		 function(registry) {
			hideDialog("ViewStructurePODialog");
		});
		}

		var dialogName=dijit.byId("ViewStructurePODialog");
		var dialogCloseButton=dialogName.closeButtonNode;
		dialogCloseButton.setAttribute("id","ViewStructurePODialogCloseButton");	
	</script>
	<%=widgetFactory.createHoverHelp("ViewStructurePODialogCloseButton", "common.Cancel") %>	
</body>
</html>
<%
   DocumentHandler xml = formMgr.getFromDocCache();

   xml.removeComponent("/In/StructuredPOList");
   xml.removeComponent("/Error");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", xml);
%>
