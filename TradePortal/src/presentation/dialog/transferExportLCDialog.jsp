<%--
 *  Transfer Export LC Dialog content.
 *
 *  This dialog is very similar to instrument search. For now we
 *  use a separate dialog due to the complexity of figuring out how to
 *  reuse existing instrument search.  This extra jsp might be able to
 *  be removed in the future if determined how to dynamically modify
 *  instrument search instead.
 *
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 *
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%


  //read in parameters
  String transferExportLCDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
  //think this is what is necessary to limit initially
  //String[] bankBranchSelectorDialogBankBranchArray = StringFunction.xssCharsToHtml(request.getParameter("bankBranchArray"));
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  //cquinton 2/12/2013 add refdata manager for datepattern lookup
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

  //think this is what is necessary to limit initially
  //String[] bankBranchSelectorDialogBankBranchArray = StringFunction.xssCharsToHtml(request.getParameter("bankBranchArray"));
   Debug.debug("***START********************INSTRUMENT SEARCH**************************START***");

  boolean newTemplate = true;     // indicates what page we came from (i.e.,
                                  // doing new template or new transaction)
  String loginLocale = userSession.getUserLocale();
  String loginRights = userSession.getSecurityRights();

  String options;
  String defaultText;

  // Parameters that drive logic for this page.
  String selectMode;
  String cancelAction;
  String prevStepAction;
  String nextStepForm;
  String searchCondition = "";
  String copyType;
  String titleKey;
  String bankBranch;
  String showNavBar;
  String showChildInstruments;

  StringBuffer dynamicWhere = new StringBuffer();
  StringBuffer newSearchCriteria = new StringBuffer();

  String       NewDropdownOptions = "";
  StringBuffer statusOptions   = new StringBuffer();
  StringBuffer statusExtraTags = new StringBuffer();
  String       selectedStatus  = "";
  String       gridHtml        = "";
  String       gridLayout      = "";
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);

  final String STATUS_ALL      = resMgr.getText("common.StatusAll",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_ACTIVE   = resMgr.getText("common.StatusActive",
                                                TradePortalConstants.TEXT_BUNDLE);
  final String STATUS_INACTIVE = resMgr.getText("common.StatusInactive",
                                                TradePortalConstants.TEXT_BUNDLE);
  // W Zhu 6/8/07 POUH060841116 Add STATUS_STRICTLY_ACTIVE
  // This status is passed in from invoking page directly and will not be part of
  // the visible Option field so we do not need to get from language-specific
  // text resource file.
  final String STATUS_STRICTLY_ACTIVE = "Strictly Active";

  CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(userSession.getOwnerOrgOid());

  // Based on the search type, read in the search parameters the user may have
  // previously used.  We use these to build the dynamic search criteria and
  // also to redisplay to the user

  String instrumentId = "";
  String refNum = "";
  String bankRefNum = "";
  String vendorId = "";
  String instrumentType = "";
  String currency = "";
  String amountFrom = "";
  String amountTo = "";
  String otherParty = "";
  String dayFrom = "";
  String monthFrom = "";
  String yearFrom = "";
  String dayTo = "";
  String monthTo = "";
  String yearTo = "";
  String helpSensitiveLink  = null;

  String searchType = "";
  Vector instrumentTypes = null;
  boolean showInstrumentDropDown = false;
  String mode = "";

  //cquinton 2/12/2013 lookup locale specific datePattern description from refData manager
  String datePattern = userSession.getDatePattern();
  String datePatternDisplay = refData.getDescr("DATEPATTERN", datePattern, loginLocale);
  String dateWidgetOptions = "placeHolder:'" + datePatternDisplay + "'";

  DocumentHandler doc = null;
  // First read in the parameters that drive how the page operates.

  cancelAction = StringFunction.xssCharsToHtml(request.getParameter("CancelAction"));
  if (cancelAction == null)
     Debug.debug("***Warning: CancelAction parameter not specified");

  prevStepAction = StringFunction.xssCharsToHtml(request.getParameter("PrevStepAction"));

  nextStepForm = StringFunction.xssCharsToHtml(request.getParameter("NextStepForm"));
  if (nextStepForm == null)
     Debug.debug("***Warning: NextStepForm parameter not specified");

  selectMode = StringFunction.xssCharsToHtml(request.getParameter("SelectMode"));
  if (selectMode == null) selectMode = TradePortalConstants.INDICATOR_NO;

  showNavBar = StringFunction.xssCharsToHtml(request.getParameter("ShowNavBar"));
  if (showNavBar == null)
  {
     showNavBar = TradePortalConstants.INDICATOR_YES;
  }

  // Indicates whether or not instruments belonging to the child organization
  // of the user's organization should be displayed in the list
  showChildInstruments = StringFunction.xssCharsToHtml(request.getParameter("ShowChildInstruments"));
  if(showChildInstruments == null)
   {
      showChildInstruments = TradePortalConstants.INDICATOR_NO;
   }

  // Check if this is a new Search (based on passed in NewSearch parameter)
  // If so, clear the listview information so it does not
  // retain any page/sort order/ sort column/ search type information
  // resulting from previous searches

  // A new search can also result from the corp org or status type dropdown
  // being reselected. In this case, NewDropdownSearch parameter indicates a
  // pseudo new search -- the search type is retained but the rest of the
  // search clause is not

  String newSearch = StringFunction.xssCharsToHtml(request.getParameter("NewSearch"));
  String newDropdownSearch = StringFunction.xssCharsToHtml(request.getParameter("NewDropdownSearch"));
  Debug.debug("New Search is " + newSearch);
  Debug.debug("New Dropdown Search is " + newDropdownSearch);

  if (newSearch != null && newSearch.equals(TradePortalConstants.INDICATOR_YES))
  {
       // Default search type for instrument status on the Instrument Search page is ALL.
      session.setAttribute("instrSearchStatusType", STATUS_ALL);
  }

  searchType = StringFunction.xssCharsToHtml(request.getParameter("SearchType"));

  if (searchType == null){
	  searchType = TradePortalConstants.SIMPLE;
      
  }
 
  copyType = StringFunction.xssCharsToHtml(request.getParameter("CopyType"));
  if (copyType == null) copyType = "";

  bankBranch = StringFunction.xssCharsToHtml(request.getParameter("BankBranch"));
  titleKey = StringFunction.xssCharsToHtml(request.getParameter("TitleKey"));
  if (titleKey == null)
  {
     titleKey = "";
  }

  /************************
   * ERSKINE'S CODE START *
   ************************/

  //If we are in the transaction area the parameters will be passed in via this document
  HttpSession theSession = request.getSession(false);
  String inTransactionArea = (String) theSession.getAttribute("inNewTransactionArea");

  if (inTransactionArea != null && inTransactionArea.equals(TradePortalConstants.INDICATOR_YES)){
    doc = formMgr.getFromDocCache();
    Debug.debug("Getting data from doc cache ->\n" + doc.toString(true));

	mode    = doc.getAttribute("/In/mode");
    cancelAction   = TradePortalConstants.TRANSACTION_CANCEL_ACTION;
    selectMode     = TradePortalConstants.INDICATOR_NO;
    copyType       = doc.getAttribute("/In/copyType");
    bankBranch     = doc.getAttribute("/In/bankBranch");

    // Change the title of the page based on what we're in the process of creating
    if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
       titleKey = "InstSearch.HeadingNewTemplateStep2";
    else
       titleKey = "InstSearch.HeadingNewTransStep2";

    Debug.debug("Getting data from doc cache2 ->\n" + doc.toString(true));

    if (mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
    {
        nextStepForm = "NewTransInstrSearchForm2";
        prevStepAction = "newTransactionStep1";
    }
    else
    {
        nextStepForm = "NewTransInstrSearchForm";
        if (mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
            prevStepAction = "newTemplate";
        else
            prevStepAction = "newTransactionStep1";

       //IAZ CR-586 08/16/10 Begin
       String isFixed = doc.getAttribute("/In/isFixed");
       String isExpress = doc.getAttribute("/In/isExpress");
       if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isFixed))
  	 	dynamicWhere.append(" and i.instrument_type_code = '" + InstrumentType.DOM_PMT + "'");
  	   if(TradePortalConstants.INDICATOR_ON.equalsIgnoreCase(isExpress ))
  	 	dynamicWhere.append(" and i.instrument_type_code in ('" + TradePortalConstants.SIMPLE_SLC + "'"
  	 	 + ", '" + InstrumentType.APPROVAL_TO_PAY + "'"
  	 	 + ", '" + InstrumentType.REQUEST_ADVISE + "'"
  	 	 + ", '" + InstrumentType.IMPORT_DLC + "')");
       //IAZ CR-586 08/16/10 End

    }

    theSession.setAttribute("prevStepAction", "goToInstrumentSearch");
    Debug.debug("Getting data from doc cache 3->\n" + doc.toString());
  }
  /************************
   *  ERSKINE'S CODE END  *
   ************************/
  else
  {
     doc = formMgr.getFromDocCache();

     if (doc.getDocumentNode("/In/InstrumentSearchInfo/SelectMode") != null)
     {
        selectMode   = doc.getAttribute("/In/InstrumentSearchInfo/SelectMode");
        cancelAction = doc.getAttribute("../CancelAction");
        nextStepForm = doc.getAttribute("../NextStepForm");
        showNavBar   = doc.getAttribute("../ShowNavBar");
        showChildInstruments = doc.getAttribute("../ShowChildInstruments");
        if(showChildInstruments == null)
          {
             showChildInstruments = TradePortalConstants.INDICATOR_NO;
          }
     }
  }


  String searchOnInstrumentType = doc.getAttribute("/In/InstrumentSearchInfo/SearchInstrumentType");

  if (searchOnInstrumentType == null)
  	searchOnInstrumentType = "";

  // determine what type of search is required - this is used by
  // SearchParms and SearchType for sqlWhereClause and Instrument Type
  // dropdowns
  if (searchOnInstrumentType.equals(InstrumentType.EXPORT_DLC))
  	searchCondition = TradePortalConstants.SEARCH_EXP_DLC_ACTIVE;
  else if (searchOnInstrumentType.equals(TradePortalConstants.ALL_EXPORT_DLC)) {
  	searchCondition = TradePortalConstants.ALL_EXPORT_DLC;
  }
  else if (searchOnInstrumentType.equals(InstrumentType.EXPORT_COL)) {
  	searchCondition = InstrumentType.EXPORT_COL;
  }
  else if (searchOnInstrumentType.equals(InstrumentType.NEW_EXPORT_COL)) {
  	searchCondition = InstrumentType.NEW_EXPORT_COL;
  }
  else if (searchOnInstrumentType.equals(TradePortalConstants.IMPORT)) {
  	searchCondition = TradePortalConstants.IMPORT;
  }
  else if(searchOnInstrumentType.equals(InstrumentType.LOAN_RQST)) {
  	searchCondition = InstrumentType.LOAN_RQST;
   
  }
  else if(searchOnInstrumentType.equals(TradePortalConstants.IMPORTS_ONLY))
	searchCondition = TradePortalConstants.IMPORTS_ONLY;
  else if(mode != null && mode.equals(TradePortalConstants.EXISTING_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_NEW_TRANSACTION;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_INSTR))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else if(mode != null && mode.equals(TradePortalConstants.NEW_TEMPLATE))
  	searchCondition = TradePortalConstants.SEARCH_FOR_COPY_INSTRUMENT;
  else
      searchCondition = TradePortalConstants.SEARCH_ALL_INSTRUMENTS;

  Debug.debug("CancelAction is " + cancelAction);
  Debug.debug("PrevStepAction is " + prevStepAction);
  Debug.debug("NextStepForm is " + nextStepForm);
  Debug.debug("SelectMode is " + selectMode);
  Debug.debug("Mode is " + mode);
  Debug.debug("SearchType is " + searchType);
  Debug.debug("CopyType is " + copyType);
  Debug.debug("BankBranch is " + bankBranch);
  Debug.debug("ShowNavBar is " + showNavBar);
  Debug.debug("SearchCondition is " + searchCondition);

  // Create a link to the basic or advanced instrument search
  String linkParms;
  String linkText;
  String link;
    Debug.debug("Getting data from doc cache 5->\n");

  linkParms = "&SelectMode=" + selectMode
              + "&CancelAction=" + cancelAction
              + "&PrevStepAction=" + prevStepAction
              + "&NextStepForm=" + nextStepForm
              + "&CopyType=" + copyType
              + "&TitleKey=" + titleKey
              + "&BankBranch=" + bankBranch
              + "&ShowNavBar=" + showNavBar;
  linkText = "";

  if (searchType.equals(TradePortalConstants.ADVANCED)) {
    linkParms += "&SearchType=" + TradePortalConstants.SIMPLE;
    link = formMgr.getLinkAsUrl("goToInstrumentDialogSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.BasicSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   						"advanced",resMgr, userSession);
   } else {
    linkParms += "&SearchType=" + TradePortalConstants.ADVANCED;
    link = formMgr.getLinkAsUrl("goToInstrumentDialogSearch",
                                linkParms, response);
    linkText = resMgr.getText("InstSearch.AdvancedSearch",
                              TradePortalConstants.TEXT_BUNDLE);
    helpSensitiveLink = OnlineHelp.createContextSensitiveLink("customer/instr_search.htm",
	   				"basic", resMgr, userSession);
   }

   if(showChildInstruments.equals(TradePortalConstants.INDICATOR_YES))
    {
      // Append the dynamic where clause to view the instruments of the user's
      // organizations and all of its children.
      dynamicWhere.append(" and i.a_corp_org_oid in ( ");
      dynamicWhere.append(" select organization_oid ");
      dynamicWhere.append(" from corporate_org ");
      dynamicWhere.append(" start with organization_oid =  ");
      dynamicWhere.append(userSession.getOwnerOrgOid());
      dynamicWhere.append(" connect by prior organization_oid = p_parent_corp_org_oid ");
      dynamicWhere.append(" ) ");
    }
   else
    {
      // Append the dynamic where clause to view only the user
      // organization's instruments
      dynamicWhere.append(" and i.a_corp_org_oid = "+  userSession.getOwnerOrgOid());
    }

     // Based on the statusType dropdown, build the where clause to include one or more
    // instrument statuses.
    Vector statuses = new Vector();
    int numStatuses = 0;

    selectedStatus = StringFunction.xssCharsToHtml(request.getParameter("instrSearchStatusType"));

    if (selectedStatus == null)
    {
       selectedStatus = (String) session.getAttribute("instrSearchStatusType");
    }

    if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
    }
    else if (STATUS_ACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_ACTIVE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_PENDING);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_EXPIRED);
    }
    else if (STATUS_INACTIVE.equals(selectedStatus) )
    {
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CANCELLED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_CLOSED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_DEACTIVATED);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_ALL);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_BANK_SIDE);
       statuses.addElement(TradePortalConstants.INSTR_STATUS_LIQUIDATE_CUST_SIDE);
    }
    else // default is ALL (actually not all since DELeted instruments
         // never show up (handled by the SQL in the listview XML)
    {
       selectedStatus = STATUS_ALL;
    }

    session.setAttribute("instrSearchStatusType", selectedStatus);

    // If the desired status is ALL, there's no point in building the where
    // clause for instrument status.  Otherwise, build it using the vector
    // of statuses we just set up.

    if (!STATUS_ALL.equals(selectedStatus))
    {
       if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
           dynamicWhere.append(" and (o.transaction_status = '");
           dynamicWhere.append(TradePortalConstants.TRANS_STATUS_AUTHORIZED);
           dynamicWhere.append("' or i.instrument_status in (");
       } else {
           dynamicWhere.append(" and i.instrument_status in (");
       }

       numStatuses = statuses.size();

       for (int i=0; i<numStatuses; i++) {
         dynamicWhere.append("'");
         dynamicWhere.append( (String) statuses.elementAt(i) );
         dynamicWhere.append("'");

         if (i < numStatuses - 1) {
            dynamicWhere.append(", ");
         }
       }

       if (STATUS_STRICTLY_ACTIVE.equals(selectedStatus) ) {
          dynamicWhere.append(")) ");
       } else {
          dynamicWhere.append(") ");
       }
    }

         UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
         thisUser.setAttribute("user_oid", userSession.getUserOid());
         thisUser.getDataFromAppServer();
         String confInd = "";
         if (TradePortalConstants.ADMIN.equals(userSession.getSecurityType()))
         	confInd = TradePortalConstants.INDICATOR_YES;
         else if (userSession.getSavedUserSession() == null)
            confInd = thisUser.getAttribute("confidential_indicator");
         else
         {
            if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
                confInd = TradePortalConstants.INDICATOR_YES;
            else
                confInd = thisUser.getAttribute("subsid_confidential_indicator");
         }

         if (!TradePortalConstants.INDICATOR_YES.equals(confInd))
        	dynamicWhere.append(" and i.confidential_indicator = 'N'");

   // Now include a file which reads the search criteria and build the
   // dynamic where clause.
%>
   <%@ include file="/transactions/fragments/InstSearch-InstType.frag" %>
   <%@ include file="/transactions/fragments/InstSearch-SearchParms.frag" %>

 <%
   // Build the status dropdown options (hardcoded in ALL (default), ACTIVE, INACTIVE order),
   // re-selecting the most recently selected option.

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("' ");
   if (STATUS_ALL.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ALL);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("' ");
   if (STATUS_ACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_ACTIVE);
   statusOptions.append("</option>");

   statusOptions.append("<option value='");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("' ");
   if (STATUS_INACTIVE.equals(selectedStatus)) {
      statusOptions.append(" selected ");
   }
   statusOptions.append(">");
   statusOptions.append(STATUS_INACTIVE);
   statusOptions.append("</option>");

   // Upon changing the status selection, automatically go back to the Instrument Search
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)
   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToInstrumentSearch", response));
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrSearchStatusType='+this.options[this.selectedIndex].value\"");
%>
<script LANGUAGE="JavaScript">

  function checkForSelection() {
    <%--
    // Before going to the next step, the user must have selected one of the
    // radio buttons.  If not, give an error.  Since the selection radio button
    // is dynamic, it is possible the field doesn't even exist.  First check
    // for the existence of the field and then check if a selection was made.
    --%>

    numElements = document.InstrumentSearchResults.elements.length;
    i = 0;
    foundField = false;

    <%-- First determine if the field even exists. --%>
    for (i=0; i < numElements; i++) {
       if (document.InstrumentSearchResults.elements[i].name == "selection") {
          foundField = true;
       }
    }

    if (foundField) {
        var size=document.InstrumentSearchResults.selection.length;
        <%-- Handle the case where there are two or more radio buttons --%>
        for (i=0; i<size; i++)
        {
            if (document.InstrumentSearchResults.selection[i].checked)
            {
                return true;
            }
        }
        <%-- Handle the case where there is only one radio button --%>
        if (document.InstrumentSearchResults.selection.checked)
        {
            return true;
        }
    }
    <%-- Handle the case where no button is checked --%>
    var msg = "<%=resMgr.getText("InstSearch.SelectOne",
                                  TradePortalConstants.TEXT_BUNDLE)%>"
    alert(msg);

   <%-- narayan IR# POUJ092282592 --- Add below one line to make variable formSubmitted to FALSE
   when user hit the button Next Step, system actually submits the page and making the value of Boolean variable formSubmitted to TRUE.
   Hence after clicking to Next Step whenever user clicks on search system won't do anything (means system won't search / submit the page again)
   because it thinks that this page is already submitted and Trade Portal don't allow to form submission twice.
   --%>
    formSubmitted = false;
    return false;
  }

   <%--Enable form submission by enter.  event.which works for Netscape and event.keyCode works for IE --%>
         function enterSubmit(event, myform) {
            if (event && event.which == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else if (event && event.keyCode == 13) {
               setButtonPressed('null', 0);
               myform.submit();
            }
            else {
               return true;
            }
         }
</script>
<%
  // Store values such as the userid, his security rights, and his org in a
  // secure hashtable for the form.  The mediator needs this information.
  Hashtable secureParms = new Hashtable();
  secureParms.put("login_oid", userSession.getUserOid());
  secureParms.put("owner_org_oid", userSession.getOwnerOrgOid());
  secureParms.put("login_rights", loginRights);
%>

<%-- ********************** First form begins here ***********************  --%>
<%-- Supports processing of Filter button.                                  --%>

<form name="InstrumentFilter" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">

<%     
       // If the invoking page force searching for strictly active instrument,
       // do not display the Instrument Status option.
       if (!STATUS_STRICTLY_ACTIVE.equals(selectedStatus) )
       {
%>

 <div id="transferExportLCDialogContent234" class="dialogContent fullwidth">
            <%
            out.println(widgetFactory.createLabel("","InstrumentHistory.Status",false, false, false, "inline"));
            %>

<%= widgetFactory.createCheckboxField("Active", "common.StatusActive", true, false, false,"onClick='searchInstruments();'","","inline") %>
<%= widgetFactory.createCheckboxField("Inactive", "common.StatusInactive", false, false,false,"onClick='searchInstruments();'","","inline") %>

<div style="clear:both"></div>

<%
        } 
%>
<div id="transferAdvancedInstrumentFilter" style="padding-top: 5px;">
  <div class="formItem">
		    <input type ="hidden" name="instrumentType" id="instrumentType" value="<%=InstrumentType.EXPORT_DLC%>" >

	    <% options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, currency, loginLocale); %>
		<%=widgetFactory.createSearchSelectField("TransferELCAdvCurrency","InstSearch.Currency"," ", options, "onChange='searchInstruments(\"Advanced\");' class='char30'")%>
		<span class="formItemWithIndent4"><%=resMgr.getText("InstSearch.AmountFrom",TradePortalConstants.TEXT_BUNDLE)%></span>
		<%=widgetFactory.createNumberField("TransferELCAdvAmountFrom","","","20",false,false,false,"onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvAmountFrom\", \"Advanced\");' class='char5'","constraints: {pattern: '###################'}","none")%>
		<span class="formItemWithIndent4"><%=resMgr.getText("InstSearch.To",TradePortalConstants.TEXT_BUNDLE)%>
		</span><%=widgetFactory.createNumberField("TransferELCAdvAmountTo","","","20",false,false,false,"onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvAmountTo\", \"Advanced\");' class='char5'","constraints: {pattern: '###################'}","none")%>
		<span class="formItemWithIndent4"><%=widgetFactory.createSearchTextField("TransferELCAdvVendorId","InstSearch.VendorId","15","onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvVendorId\", \"Advanced\");' class='char10'")%>
		</span><div class="title-right inline">
		<button data-dojo-type="dijit.form.Button" type="button" id="searchTransferExportLcButton">
	            <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  searchInstruments("Advanced");return false;
	      		</script>
	        </button><br>&nbsp;&nbsp;&nbsp;
		<span id="basicFilterHoverText"><a href="javascript:shuffleFilter('Basic');">Basic</a></span>
		</div>
	</div>
	 <%=widgetFactory.createHoverHelp("basicFilterHoverText", "common.BasicSearchHypertextLink") %>
	 <%=widgetFactory.createHoverHelp("searchTransferExportLcButton", "SearchHoverText") %>

		<div class="formItem">
		<%=widgetFactory.createSearchDateField("TransferELCAdvDateFrom","InstSearch.ExpiryDateFrom","onChange='searchInstruments(\"Advanced\");' class='char10'",dateWidgetOptions)%>
		<%=widgetFactory.createSearchDateField("TransferELCAdvDateTo","InstSearch.To","onChange='searchInstruments(\"Advanced\");' class='char10'",dateWidgetOptions)%>
		<%=widgetFactory.createSearchTextField("TransferELCAdvOtherParty","InstSearch.OtherParty","30","onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvOtherParty\", \"Advanced\");' class='char10'")%>
  </div>
  <div style="clear: both;"></div>
</div>
  <input type="hidden" name="NewSearch" value="Y">
  <div id="transferBasicInstrumentFilter" style="padding-top: 5px;">
  <div class="formItem">
  	<%=widgetFactory.createSearchTextField("TransferELCAdvInstrumentId","InstSearch.InstrumentID","16", "onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvInstrumentId\", \"Basic\");' class='char8'")%>
  	<%
      		// Based on search condition (for specific instrument type searches only),
      		// print the name of the instrument type.  Also store this value so it is
     		// picked up on return to the page.
      		String instrTypeText = "";
      		if (searchCondition.equals(TradePortalConstants.SEARCH_EXP_DLC_ACTIVE))
         	instrTypeText = "CorpCust.ExportLC";
     		else if (searchCondition.equals(InstrumentType.LOAN_RQST))
         			instrTypeText = "CorpCust.LoanRequests";
      		else
         	instrTypeText = instrumentType;
   	%>
   <%=widgetFactory.createSubLabel("InstSearch.InstrumentType")%>
   &nbsp;&nbsp;
  <%=widgetFactory.createSubLabel("CorpCust.ExportLC1")%><%--
   <%=resMgr.getText("CorpCust.ExportLC", TradePortalConstants.TEXT_BUNDLE)%> --%>
   &nbsp;&nbsp;&nbsp;&nbsp;
  <%=widgetFactory.createSearchTextField("TransferELCAdvRefNum","InstSearch.ApplRefNumber","30","onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvRefNum\", \"Basic\");' class='char10'")%>
  <div class="title-right inline">
	    	<button data-dojo-type="dijit.form.Button" type="button" id="searchTransferExportLcButton1">
	            <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	            <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  searchInstruments("Basic");return false;
	      		</script>
	        </button><br>&nbsp;
	         <%=widgetFactory.createHoverHelp("searchTransferExportLcButton1", "SearchHoverText") %>
		<span id="AdvanceHoverText"><a href="javascript:shuffleFilter('Advanced');"><%= resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a></span>
		<%=widgetFactory.createHoverHelp("AdvanceHoverText", "common.AdvancedSearchHypertextLink") %>
  </div>
  </div>
  <div class="formItem">
  <%=widgetFactory.createSearchTextField("TransferELCAdvBankRefNum","InstSearch.OrigBanksRefNo","30","onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvBankRefNum\", \"Basic\");' class='char10'")%>
  <%=widgetFactory.createSearchTextField("TransferELCAdvVendorIdBasic","InstSearch.VendorId","15","onKeydown='Javascript: filterInstrumentsOnEnter(\"TransferELCAdvVendorIdBasic\", \"Basic\");' class='char10'")%>
 </div>
 <div style="clear: both;"></div>
</div>
   <%
  gridHtml = dgFactory.createDataGrid("tradeSearchGridId","TradeSearchDataGrid", null);
  gridLayout = dgFactory.createGridLayout("TradeSearchDataGrid");
  %>
  <%=gridHtml%>
</div>
<%=widgetFactory.createHoverHelp("transferExport_CloseButton", "common.Cancel") %>

</form>
<form name="InstrumentSearchResults" method="post"
      action="<%=formMgr.getSubmitAction(response)%>">
  <input type="hidden" value="" name=buttonName>

  <input type="hidden" name="SelectMode" value="<%=selectMode%>">
  <input type="hidden" name="CancelAction" value="<%=cancelAction%>">
  <input type="hidden" name="PrevStepAction" value="<%=prevStepAction%>">
  <input type="hidden" name="NextStepForm" value="<%=nextStepForm%>">
  <input type="hidden" name="SearchType" value="<%=searchType%>">
  <input type="hidden" name="CopyType" value="<%=copyType%>">
  <input type="hidden" name="TitleKey" value="<%=titleKey%>">
  <input type="hidden" name="BankBranch" value="<%=bankBranch%>">
  <input type="hidden" name="ShowNavBar" value="<%=showNavBar%>">
</form>

<script>
  <%-- variables for Search Basic and Advanced. --%>
  <%-- Sandeep Rel-8.3 IR#T36000019387 07/31/2013 - Begin --%>
  var basicSearchEnabled = true;
  <%-- Sandeep Rel-8.3 IR#T36000019387 07/31/2013 - End --%>
  
  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {

    var gridLayout = <%=gridLayout%>;
    var instrumentType = "<%=InstrumentType.EXPORT_DLC%>";
    var initParms="instrumentType="+instrumentType;
    initParms+="&userOrgOid=<%=userSession.getOwnerOrgOid()%>&confInd=<%=confInd%>&transferExportLCFlag=Y&statusActive=Active&statusInactive=";

    <%-- because its on a dialog explicitly set to 10 rows so it doesn't take over the browser --%>
    var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("TradeSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
    var myGrid = t360grid.createDataGrid("tradeSearchGridId",viewName,gridLayout,initParms);
    myGrid.set('autoHeight',11);
  });
  var rowKeys ="";
  function selectInstrument() {
	    <%-- todo: add external event handler to datagrid footer items --%>
	    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
	        function(registry, query, on, dialog ) {
	      <%--get array of rowkeys from the grid--%>
	      rowKeys = getSelectedGridRowKeys("tradeSearchGridId");
	      if(rowKeys == ""){
		  		alert("Please pick a record and click 'Select'");
		  		return;
		  }
	      dialog.open('bankBranchSelectorDialog', bankBranchSelectorDialogTitle,
	              'bankBranchSelectorDialog.jsp',
	              null, null, <%-- parameters --%>
	              'select', this.createNewInstrumentBankBranchSelectedValue);
	    });
	  }

  function createNewInstrumentBankBranchSelectedValue(bankBranchOid) {
		console.log("inside createNewInstrumentBankBranchSelectedValue() - bankBranchOid: "+bankBranchOid);
		console.log("rowkeys: "+rowKeys+"==="+rowKeys[0])

		if ( document.getElementsByName('NewInstrumentForm').length > 0 ) {
	        var theForm = document.getElementsByName('NewInstrumentForm')[0];
	      	theForm.copyInstrumentOid.value=rowKeys[0];
	      	theForm.bankBranch.value=bankBranchOid;
	      	theForm.mode.value='CREATE_NEW_INSTRUMENT';
	        theForm.copyType.value='TRANSFER';

	    	console.log("theForm.copyInstrumentOid.value: "+theForm.copyInstrumentOid.value);
	  		console.log("theForm.bankBranch.value: "+theForm.bankBranch.value);

	      	theForm.submit();
	      } else {
	        	alert('Problem in createNewInstrument: cannot find ExistingInstrumentsForm');
	      }
	}

  function searchInstruments(buttonId) {
	  require(["dijit/registry","dojo/date/locale","dojo/dom","dojo/domReady!"],
		    function(registry,locale,dom){
			var inactive = dom.byId("Inactive").checked;
			var active = dom.byId("Active").checked;
			var statusActive = "";
			var statusInactive = "";
			if (inactive)
				statusInactive = "Inactive";
			else
				statusInactive = "";
			if (active)
				statusActive = "Active";
			else
				statusActive = "";

	        var searchParms="confInd=<%=confInd%>";
	        searchParms+="&userOrgOid=<%=userSession.getOwnerOrgOid()%>";
		    searchParms+="&statusActive="+statusActive+"&statusInactive="+statusInactive;
		    if(basicSearchEnabled==true)
		    	buttonId="Basic";
		    else
		    	buttonId="Advanced";

	        if(buttonId=="Advanced"){
	        	var instrumentType = "<%=InstrumentType.EXPORT_DLC%>";
		        var currency = registry.byId("TransferELCAdvCurrency").value;
		        var amountFrom = dom.byId("TransferELCAdvAmountFrom").value;
		        var amountTo = dom.byId("TransferELCAdvAmountTo").value;
		        var dateFrom;
		        var dateTo;
		        if(dom.byId("TransferELCAdvDateFrom").value != "")
		        	dateFrom = locale.format(dijit.byId("TransferELCAdvDateFrom").value, {selector: "date", datePattern: "MM/dd/yyyy"});
		        else
		        	dateFrom = "";

		       	if(dom.byId("TransferELCAdvDateTo").value != "")
		        	dateTo = locale.format(dijit.byId("TransferELCAdvDateTo").value, {selector: "date", datePattern: "MM/dd/yyyy"});
		       	else
		       		dateTo = "";
		       
		        var otherParty = dijit.byId("TransferELCAdvOtherParty").value;
		        var vendorId = dijit.byId("TransferELCAdvVendorId").value;

		        if (isNaN(amountFrom))
		        		amountFrom="";

		        if (isNaN(amountTo))
	        		amountTo="";

		        searchParms+="&instrumentType="+instrumentType;
				searchParms+="&currency="+currency;
				searchParms+="&amountFrom="+amountFrom;
				searchParms+="&amountTo="+amountTo;
				searchParms+="&dateFrom="+dateFrom;
				searchParms+="&dateTo="+dateTo;
				searchParms+="&otherParty="+otherParty;
				searchParms+="&vendorId="+vendorId;
				searchParms+="&searchType=Advanced";
	        }else{
	        	var instrumentType = "<%=InstrumentType.EXPORT_DLC%>";
	        	var instrumentId = document.getElementById("TransferELCAdvInstrumentId").value;

	 	        var refNum = document.getElementById("TransferELCAdvRefNum").value;
	 	        var bankRefNum = document.getElementById("TransferELCAdvBankRefNum").value;
	 	        var vendorId = document.getElementById("TransferELCAdvVendorIdBasic").value;

	 	        searchParms+="&instrumentType="+instrumentType;
	 	        searchParms+="&instrumentId="+instrumentId;
				searchParms+="&refNum="+refNum;
				searchParms+="&bankRefNum="+bankRefNum;
				searchParms+="&vendorId="+vendorId;
				searchParms+="&searchType=Basic";
	        }
		        searchParms+="&encrypted=true";
		        searchParms+="&loginLocale=<%=loginLocale%>";
		        searchParms+="&resourceLocale=<%=resMgr.getResourceLocale()%>";
		        searchParms+="&fromDocCache=<%=formMgr.getFromDocCache()%>";
		        searchParms+="&transferExportLCFlag=Y";

	        console.log("searchParms="+searchParms);

			searchDataGrid("tradeSearchGridId", "TradeSearchDataView", searchParms);

			console.log("Query fired");
		});
	}

  function shuffleFilter(linkValue){
		console.log("inside shuffleFilter(): "+linkValue);
		require(["dojo/dom","dijit/registry","dojo/domReady!"],
		      function(dom,registry){
	  			if(linkValue=='Advanced'){
	  				dom.byId("transferAdvancedInstrumentFilter").style.display='block';
	  				dom.byId("transferBasicInstrumentFilter").style.display='none';

	  				<%-- clearing Basic Filter --%>
	  				dom.byId("TransferELCAdvInstrumentId").value='';
	 	        	dom.byId("TransferELCAdvRefNum").value='';
	 	        	dom.byId("TransferELCAdvBankRefNum").value='';
	 	 	        dom.byId("TransferELCAdvVendorIdBasic").value='';
	 	 	      	basicSearchEnabled=false;
	  			}

	  			if(linkValue=='Basic'){
	  				dom.byId("transferAdvancedInstrumentFilter").style.display='none';
	  				dom.byId("transferBasicInstrumentFilter").style.display='block';

	  				<%-- clearing Advance Filter --%>
	  				dom.byId("TransferELCAdvCurrency").value='';
		        	dom.byId("TransferELCAdvAmountFrom").value='';
		        	dom.byId("TransferELCAdvAmountTo").value='';
		        	dom.byId("TransferELCAdvOtherParty").value='';
					dom.byId("TransferELCAdvVendorId").value='';
					basicSearchEnabled=true;
	  			}
		});
	}
  var dialogName=dijit.byId("transferExportLCDialog");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","transferExport_CloseButton");
  function filterInstrumentsOnEnter(fieldId, filterType){
		<%-- 
	  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
	  	 --%>
	  	require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	if(filterType=="Basic")
		        		basicSearchEnabled=true;
		        	else
		        		basicSearchEnabled=false;
		        	searchInstruments(filterType);
		        }
		    });
		});
	}
  require(["dojo/dom","dojo/domReady!" ], function(dom) {
	  dom.byId("transferAdvancedInstrumentFilter").style.display='none';
  });

function hideGrid(){
		require(["t360/dialog"],
				function(dialog) {
		dialog.hide('transferExportLCDialog');
	});
		}
</script>