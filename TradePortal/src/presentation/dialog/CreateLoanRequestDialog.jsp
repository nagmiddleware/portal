<%--
 *  Bank Branch Selector Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 

   String formName = "CreateLRQGroupSelectionOnDetailForm";  
   String fromInvoiceGroupDetail = (String)session.getAttribute("fromInvoiceGroupDetail");
   String currentTab = (String)session.getAttribute("currentInvoiceMgmtTab");
    if (!"GD".equals(fromInvoiceGroupDetail) &&
		   TradePortalConstants.INV_GROUP_TAB.equals(currentTab)) {
      formName = "CreateLRQGroupSelectionForm";
   }
   
  //read in parameters
   String bankBranchSelectorDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
   String cancelLink=formMgr.getLinkAsUrl("goToInvoiceManagement", response); 
   WidgetFactory widgetFactory  = new WidgetFactory(resMgr,userSession);
   Hashtable secureParms        = new Hashtable();
   secureParms.put("SecurityRights",  userSession.getSecurityRights());
   secureParms.put("UserOid",         userSession.getUserOid());
   secureParms.put("ownerOrg",        userSession.getOwnerOrgOid());
   secureParms.put("clientBankOid",   userSession.getClientBankOid());
   secureParms.put("baseCurrencyCode", userSession.getBaseCurrencyCode());
   String selectedCount = StringFunction.xssCharsToHtml(request.getParameter("selectedCount"));
   String  selectedStatus   = "";
   String  loginLocale      = null;
   Vector  codesToExclude   = null;
   codesToExclude = new Vector();
   DocumentHandler  doc = formMgr.getFromDocCache();  
   loginLocale=userSession.getUserLocale();
   String gridHtml = "";

   String ownerOrg = userSession.getOwnerOrgOid();
  CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(ownerOrg);
  
  String bbGridData1 = request.getParameter("orglist");//do not use StringFunction.xssCharsToHtml
%>


<div id="createLoanReqContent" class="dialogContent">
<form id="<%=formName%>" method="post" name="<%=formName%>" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
 <input type=hidden value="" name=buttonName>
 <%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>

  <div class="formItem">
   <table border='0'>
   <tr>
	
	<td nowrap>
	<%=resMgr.getText("UploadInvoices.SelectBankBranch",TradePortalConstants.TEXT_BUNDLE) %>
	</td>
	<td>
	<%-- MEer Rel 9.3 XSS CID-11319 , 11227--%>
	<%
	 out.print(widgetFactory.createSelectField("bankBranch", "",  "", StringFunction.xssCharsToHtml(bbGridData1.toString()), false,
                                               false, false, "", "", ""));   
	%>
	</td>
	</tr>	
	</table>	

	
	</div>
  
  <br><br><br>

    <div class="formItem">
	
	<button data-dojo-type="dijit.form.Button"  name="Route" id="Route" type="button">
		<%=resMgr.getText("UploadInvoiceAction.Continue",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			assignOpOrg();     					
		</script>
	</button> 

	<button data-dojo-type="dijit.form.Button"  name="Close" id="Close" type="button">
		<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			cancelOne();     					
		</script>
	</button> 

	
 </form>
</div>

<script type='text/javascript'>

function assignOpOrg() {
require(["dijit/registry", "t360/dialog"],
        function(registry, dialog ) {   
	 var userBankBranch = registry.byId("bankBranch").value;
	 dialog.doCallback('<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDialogId)%>', 'select',userBankBranch);
	
    });
}


function cancelOne(){
    require(["t360/dialog"], 
                function(dialog) {
    dialog.hide('InvoiceGroupListDialogID');

});
    }
 
</script>
</div>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/Error", "");
  
   formMgr.storeInDocCache("default.doc", doc);
%>

