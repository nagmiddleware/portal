<%--
*******************************************************************************
                            Remove Structured PO Page

  Description:  The Remove PO page is used to present the user with 
                a list of POs assigned to the transaction that is 
                currently being edited from which he/she can select to remove
                from the transaction. This page can only be accessed in edit 
                mode and only by organizations that support Structured purchase order 
                processing and by users having the necessary security right to 
                do the same. Currently, this page is accessible only to Import 
                LC - Issue ,Import LC - Amend, Also ATP transaction types.

                This page contains a listview of POs, each having 
                its own checkbox. Once a user selects one or more POs
                and presses the Delete Selected Items button, the selected POs
                are removed from the transaction and reset to 
                unassigned.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.busobj.util.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,java.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
   String gridLayout = null;
   String gridHtml = null;
   TransactionWebBean              transaction        = null;
   StringBuffer                    dynamicWhereClause = null;
   Hashtable                       secureParms        = null;
   String                          userSecurityType   = null;
   String                          transactionType    = null;
   String                          transactionOid     = null;
   String                          userOrgOid         = null;
   String                          link               = null;
   String                          shipmentOid         = null;
   int                             numberOfRows       = 0;
   boolean 						   fromAddStrcuPO     = false;
   
   	String form_name = "";
	String ins_type = request.getParameter("ins_type");

	if ("ATP".equals(ins_type)) {
		form_name = "TransactionATP";
	} else if ("IMPORTLC".equals(ins_type)) {
		form_name = "TransactionIMP_DLC";
	} 
	

   // Get the user's security type and organization oid
   userSecurityType = userSession.getSecurityType();
   userOrgOid       = userSession.getOwnerOrgOid();

   // Get the transaction oid from the current transaction web bean
   transaction = (TransactionWebBean) beanMgr.getBean("Transaction");

   transactionType = transaction.getAttribute("transaction_type_code");
   transactionOid  = transaction.getAttribute("transaction_oid");

   DocumentHandler xmlDoc = formMgr.getFromDocCache();
   shipmentOid         = request.getParameter("shipmentOid");//xmlDoc.getAttribute("/In/SearchForPO/shipment_oid");
   
   // Set a bunch of secure parameters necessary for the Remove PO Line Items mediator
   secureParms = new Hashtable();
   secureParms.put("UserOid", userSession.getUserOid());
   secureParms.put("transactionOid", transactionOid);
   secureParms.put("shipmentOid", shipmentOid);
%>

<%-- ********************* HTML for page begins here *********************  --%>


<form name="RemoveStructuredPOForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
<%= formMgr.getFormInstanceAsInputField("RemoveStructuredPOForm", secureParms) %>
<jsp:include page="/common/ErrorSection.jsp" /> 
<div class="POPopUpContent">
<input type=hidden value="" name=buttonName>

<%@ include file="/transactions/fragments/StructuredPOSearch.frag"%>

<%
gridLayout = dgFactory.createGridLayout("removeStructurePODataDataGridId", "RemoveStructurePODataGrid");
gridHtml = dgFactory.createDataGrid("removeStructurePODataDataGridId","RemoveStructurePODataGrid",null);
%>
<%=gridHtml%>
 
</div>
</form>
    
 <script type='text/javascript'>
 
    var gridLayout = <%= gridLayout %>;							
	var initSearchParms="userOrgOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey())%>";			
		initSearchParms+="&transactionOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(transactionOid, userSession.getSecretKey())%>";
		initSearchParms+="&shipmentOid=<%=EncryptDecrypt.encryptStringUsingTripleDes(shipmentOid, userSession.getSecretKey())%>";
	
	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("RemoveStructurePODataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	createDataGrid("removeStructurePODataDataGridId", viewName, gridLayout, initSearchParms);
		
	function filterPOs(){
		require(["dojo/dom","dojo/domReady!" ], function(dom) {
		console.log("Inside filterPOs()");
		
		var benName = dom.byId("BeneName").value;
		var poNumber = dom.byId("PONum").value;
		var currency = dom.byId("Currency").value;
		var amountFrom = dom.byId("AmountFrom").value;
		var amountTo = dom.byId("AmountTo").value;
								
		var searchParms=initSearchParms;
		searchParms+="&beneficiaryName="+benName;
		searchParms+="&poNumber="+poNumber;
		searchParms+="&currency="+currency;
		searchParms+="&amountFrom="+amountFrom;
		searchParms+="&amountTo="+amountTo;
		
		console.log("searchParms="+searchParms);

		searchDataGrid("removeStructurePODataDataGridId", "RemoveStructurePODataView", searchParms);
	});
	}
		
		function deleteStructurePOData(){
			var rowKeys = getSelectedGridRowKeys("removeStructurePODataDataGridId"),
			    buttonName = 'RemoveStructuredPOButton',
		         formName = '<%=form_name%>';
			
			if(rowKeys == ""){
				alert("Please select record(s).");
				return;
			}
			
			submitFormWithParms(formName, buttonName, "checkbox", rowKeys);

		}
		
		function closeStructurePODataItems(){
			require(["dijit/registry"],
					 function(registry) {
						hideDialog("RemoveStructurePODialog");
					});
		}

		var dialogName=dijit.byId("RemoveStructurePODialog");
		var dialogCloseButton=dialogName.closeButtonNode;
		dialogCloseButton.setAttribute("id","RemoveStructurePODialogCloseButton");
	</script>
	<%=widgetFactory.createHoverHelp("RemoveStructurePODialogCloseButton", "common.Cancel") %>		
</body>
</html>
<%
   DocumentHandler xml = formMgr.getFromDocCache();

   xml.removeComponent("/In/StructuredPOList");
   xml.removeComponent("/Error");

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   formMgr.storeInDocCache("default.doc", xml);
%>
