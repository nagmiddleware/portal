<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%

  //read in parameters
  String announcementOid = StringFunction.xssCharsToHtml(request.getParameter("announcementOid"));

%>

<div id="testDialogContent">

  <div id="testGrid"></div>

</div>

<script type='text/javascript'>
  <%-- add datagrid to dialog --%>
  require(["dojo/store/Memory","dojo/data/ObjectStore","dojox/grid/DataGrid"], 
    function(Memory,ObjectStore,DataGrid) {
      var someData = [
        {id:1, name:"One"},
        {id:2, name:"Two"}
      ];
      var myStore = new Memory({data: someData});
      <%-- now create the object store for the datagrid --%>
      var objStore = new ObjectStore({ objectStore: myStore });
      var myGrid = new DataGrid({
        store: objStore,
            style: "width: 250px; height: 100px;",
        structure: [
            { name: "DialogGridColumn", field: "name", width: "200px" }
          ]
      }, "testGrid");
      myGrid.startup();
    }
    );
</script>
