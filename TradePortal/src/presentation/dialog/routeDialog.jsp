<%--
 *  Route Messages Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the
 *  footer and is included on the page when the menu is displayed.
--%>

<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*, com.amsinc.ecsg.html.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%


  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,response);
  String gridHtml = "";
%>



<%
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   QueryListView queryListView = null;
   CorporateOrganizationWebBean corpOrg = null;
   boolean fromListView = false;
   boolean fromMessages = false;
   boolean multipleRelatedOrgs = false;
   boolean routeToUserIsSelected = false;
   boolean routeToOrgIsSelected = false;
   StringBuffer query = new StringBuffer();
   DocumentHandler orgListDoc = null;
   DocumentHandler userListDoc = null;
   String orgDropdownOptions = null;
   String userDropdownOptions = null;
   String routeSelectionErrors = null;
   String userOidSelected = null;
   String orgOidSelected = null;
   int orgCount = 0;

   String routeDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
   String selectedCount = request.getParameter("selectedCount");
   String fromListViewValue =request.getParameter("fromListView");
   String fromWhere = request.getParameter("fromWhere");
   String rowKey = request.getParameter("rowKey");
   String gridDisplayValue1 = request.getParameter("gridDisplayValue1");//Rel 9.3 XSS CID-11400, 11424
   String gridDisplayValue2 = request.getParameter("gridDisplayValue2");
   String gridDisplayValue3 = request.getParameter("gridDisplayValue3");


   Debug.debug("Route listview " + fromListViewValue);
   //out.println("\n\n********fromListView" +  fromListViewValue);
   //out.println("\n\n********fromWhere" +  fromWhere);
   if (fromListViewValue != null && fromListViewValue.equals(TradePortalConstants.INDICATOR_YES))
   	  fromListView = true;

   if(!fromListView){
	   selectedCount = "1";
   }
   try
   {
      // determine the rows to populate organization drop down:
      // find only ACTIVE organizations

      queryListView = (QueryListView)
	 	EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");


      corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class,   "CorporateOrganization");

      corpOrg.getById(userSession.getOwnerOrgOid());

      String routingSetting = corpOrg.getAttribute("route_within_hierarchy");
      java.util.List<Object> sqlParamsLst = new java.util.ArrayList();

      // Based on corporate customer setting, determine the list of orgs that can be routed to
      if(routingSetting.equals(TradePortalConstants.ROUTE_ALL))
       {
         //  This query retrieves the list of organizations across the entire hierarchy, even "sibling"
         //  and "cousin" organizations.
         query.append("select organization_oid, name");
	 	 query.append(" from corporate_org");
		 query.append(" where activation_status=?");
		 query.append("  start with organization_oid =");
		 query.append("   (select organization_oid ");
		 query.append("    from corporate_org");
		 query.append("    where (p_parent_corp_org_oid is null or p_parent_corp_org_oid = '') ");
	 	 query.append("    start with organization_oid = ?   connect by prior p_parent_corp_org_oid = organization_oid) ");
	 	 query.append(" connect by prior organization_oid = p_parent_corp_org_oid");
	 	 sqlParamsLst.add(TradePortalConstants.ACTIVE);
	 	 sqlParamsLst.add(userSession.getOwnerOrgOid());
	 
       }
      else
       {
         // Retrieve the user's organizations and all of its children and grandchildren
	 	query.append("select organization_oid, name");
	 	query.append(" from corporate_org");
	 	query.append(" where activation_status=?");
	 	query.append("  start with organization_oid = ? connect by prior organization_oid = p_parent_corp_org_oid");

         // We also want the direct parent organization of the user's organization, so
         // union that in
         query.append(" union ");
	 	 query.append("select organization_oid, name");
	 	 query.append(" from corporate_org");
	 	 query.append(" where activation_status=?");
         query.append(" and organization_oid = ");
         query.append("   (select p_parent_corp_org_oid ");
         query.append("    from corporate_org ");
         query.append("    where organization_oid = ? ");
         query.append(")");
         
         sqlParamsLst.add(TradePortalConstants.ACTIVE);
    	 sqlParamsLst.add(userSession.getOwnerOrgOid());
    	 sqlParamsLst.add(TradePortalConstants.ACTIVE);
    	 sqlParamsLst.add(userSession.getOwnerOrgOid());
       }

     Debug.debug("Query is : " + query);

	 queryListView.setSQL(query.toString(), sqlParamsLst);
	 queryListView.getRecords();

	 orgCount = queryListView.getRecordCount();

	 // if there is more than one organization row returned, display the Organization dropdown list
	 // otherwise, display only the user list since this would be a standalone dropdown
	 if (orgCount > 1)
	 {
	   multipleRelatedOrgs = true;

	   // create the drop down list of related organizations with
	   // the default set to empty
       orgListDoc = queryListView.getXmlResultSet();
       orgDropdownOptions = ListBox.createOptionList(orgListDoc, "ORGANIZATION_OID", "NAME", orgOidSelected, userSession.getSecretKey());
     Debug.debug("ORG DROP DOWNS " + orgDropdownOptions);
	 }

	 // select the users associated with the list of selected organizations
	 // use a different SQL for single organization and multiple related organizations
	 query.delete(0, query.length());
	
	 sqlParamsLst = new java.util.ArrayList();
	 if (multipleRelatedOrgs)
	 {
	   // for multiple related organizations, append the organization name and selct
	   // the users from the list of organizations previously retrieved
	   query.append("select last_name || ', ' || first_name || ' ' || middle_initial || ");
	   query.append("' - '|| user_identifier || ' - ' || name user_descr, user_oid ");

	   query.append("from users, corporate_org where p_owner_org_oid in (");

	   // append all organization oids
	   for (int i = 0; i < orgCount; i++)
	   {
	   	 queryListView.scrollToRow(i);
	   	 query.append("?");
	   	 sqlParamsLst.add(queryListView.getRecordValue("ORGANIZATION_OID"));
		 query.append(", ");
	   }

	   query.setCharAt( query.length() -2,')');
	   query.append(" and organization_oid = p_owner_org_oid");
	   query.append(" and users.activation_status='" + TradePortalConstants.ACTIVE + "'");

	 }
	 else
	 {
	   // for stand alone, select the users belonging to the single organization
	   query.append("select last_name || ', ' || first_name || ' ' || middle_initial || ");
	   query.append("' - '|| user_identifier user_descr, user_oid ");
	   query.append("from users where p_owner_org_oid = ? and users.activation_status='" + TradePortalConstants.ACTIVE + "'");
	   sqlParamsLst.add(userSession.getOwnerOrgOid());
	  // sqlParamsLst.add(TradePortalConstants.ACTIVE);

	 }

	 Debug.debug("User query is: " + query.toString());
	 // create the User drop down options with a blank default value
	 queryListView.setSQL(query.toString(), sqlParamsLst);
	 queryListView.getRecords();
	 userListDoc = queryListView.getXmlResultSet();
	 userDropdownOptions = ListBox.createOptionList(userListDoc, "USER_OID", "USER_DESCR", userOidSelected, userSession.getSecretKey());
     Debug.debug("USER DROP DOWNS " + userDropdownOptions);
     queryListView.remove();
   }
   catch (Exception e)
   {
     e.printStackTrace();
   }


%>
<script LANGUAGE="JavaScript">
  function pickUser(selectField) {
    <%-- When the user drop down box has been clicked, --%>
    <%-- set the first RouteToUser to 'Y' and the second to 'N' for the radio buttons --%>
    require(["dijit/registry"],
            function(registry) {
    	if(selectField == "RouteUserOid"){
	    	if(registry.byId(selectField).value!=""){
	    		registry.byId("RouteToUser").set('checked', true);
	    		registry.byId("RouteCorporateOrgOid").set("value","");
	    	}else{
	    		registry.byId("RouteToUser").set('checked', false);

	    	}
    	}else if(selectField == "RouteCorporateOrgOid"){
	    	if(registry.byId(selectField).value!=""){
	    		registry.byId("RouteToUser2").set('checked', true);
	    		registry.byId("RouteUserOid").set("value","");
	    	}else{
	    		registry.byId("RouteToUser2").set('checked', false);
	    	}
    	}

    });

  }

  function doValidate(radioButtonSelected){
	  require(["dijit/registry"],
	            function(registry) {
	    	if(radioButtonSelected == "RouteToUser"){
		    		registry.byId("RouteCorporateOrgOid").set("value","");
		    }else if(radioButtonSelected == "RouteToUser2"){
		    		registry.byId("RouteUserOid").set("value","");
		    }

	    });
  }

</script>
<div id="routeDialogContent" class="dialogContent">


<%
if (multipleRelatedOrgs)
{
%>
  <input type="hidden" name="multipleOrgs" value="Y">
	&nbsp;
  	<%=widgetFactory.createRadioButtonField("RouteToUser","RouteToUser","routeDialog.selectRecipientPerson",TradePortalConstants.INDICATOR_YES,routeToUserIsSelected, false, " onClick=\"doValidate('RouteToUser')\"", "") %>
	<br>
	<div class="formItemWithIndent3">
  	<%=widgetFactory.createSelectField("RouteUserOid", "", " ", userDropdownOptions, false, false, false, "class='char36' onChange=\"pickUser('RouteUserOid')\"","","") %>
	</div>
	&nbsp;
  	<%=widgetFactory.createRadioButtonField("RouteToUser","RouteToUser2","routeDialog.selectRecipientOrg",TradePortalConstants.INDICATOR_NO,routeToOrgIsSelected, false, " onClick=\"doValidate('RouteToUser2')\"", "") %>
  	<br>
  	<div class="formItemWithIndent3">
  	<%=widgetFactory.createSelectField("RouteCorporateOrgOid", "", " ", orgDropdownOptions, false, false, false,  "class='char36' onChange=\"pickUser('RouteCorporateOrgOid')\"","","") %>
	</div>
<%
}
else
{
%>

  <%=widgetFactory.createSelectField("RouteUserOid", "RouteItem.selectRecipientPerson", " ", userDropdownOptions, false,false,false,"class='char36'","","")%>

<%
}
%>



  <div class="formItem">
	<button data-dojo-type="dijit.form.Button"  name="Route" id="Route" type="button">
		<%=resMgr.getText("common.RouteItem(s)Text",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			routeItem();
		</script>
	</button>&nbsp;&nbsp;
	<%=widgetFactory.createHoverHelp("Route","RouteMessageHoverText") %>
	<button data-dojo-type="dijit.form.Button"  name="Close" id="Close" type="button">
		<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			closeDialog();
		</script>
	</button>
	<%=widgetFactory.createHoverHelp("Close","common.Cancel") %>
</div>

<div class="subHeaderDivider"></div>
<div class="formItem">
<%=widgetFactory.createSubLabel("routeDialog.selectedItems") %>
<%=widgetFactory.createSubLabel(StringFunction.xssCharsToHtml(selectedCount)) %>
</div>
<%
    gridHtml = dgFactory.createDataGrid("routeGrid","RouteDataGrid",null);
%>

<%= gridHtml %>


<script type='text/javascript'>
<%
String routeGridLayout = "";
if (fromWhere != null && (fromWhere.equals(TradePortalConstants.FROM_TRADE) || fromWhere.equals(TradePortalConstants.FROM_CM) || fromWhere.equals(TradePortalConstants.FROM_CM_FVD) || fromWhere.equals(TradePortalConstants.FROM_DDI))){
  	routeGridLayout = dgFactory.createGridLayout("RouteDataGrid");
}else if (fromWhere != null && fromWhere.equals(TradePortalConstants.FROM_MESSAGES_DEBIT_FUND) || fromWhere.equals(TradePortalConstants.FROM_MESSAGES)){
	routeGridLayout = dgFactory.createGridLayout("RouteMessagesDataGrid");
}else if(fromWhere != null && fromWhere.equals(TradePortalConstants.FROM_PAYREMIT)){
	routeGridLayout = dgFactory.createGridLayout("RoutePayRemitDataGrid");
}
%>

<%
if (fromListView)
{

	String gridID=fromWhere;
	if (TradePortalConstants.FROM_TRADE.equals(fromWhere))
	{
		gridID = "tradePendGrid";
	}
	else if (TradePortalConstants.FROM_CM.equals(fromWhere))
	{
		gridID = "pmtPendGrid";
	}
	else if (TradePortalConstants.FROM_CM_FVD.equals(fromWhere))
	{
		gridID = "pmtFutureGrid";
	}
	else if (TradePortalConstants.FROM_DDI.equals(fromWhere))
	{
		gridID = "ddPendGrid";
	}
	else if (TradePortalConstants.FROM_MESSAGES.equals(fromWhere))
	{
		gridID = "MailMessagesDataGridId";
	}
	else if (TradePortalConstants.FROM_PAYREMIT.equals(fromWhere))
	{
		gridID = "matchNoticesGrid";
	}
	else if (TradePortalConstants.FROM_MESSAGES_DEBIT_FUND.equals(fromWhere))
	{
		gridID = "DebitFundMailMsgDataGridId";
	}
%>
	var parentDataGridID = '<%=StringFunction.escapeQuotesforJS(gridID)%>';
	startMemoryDataGrid("routeGrid", populateMemoryDataGrid(parentDataGridID), <%= routeGridLayout %>);
<%
}else{%>
	startMemoryDataGrid("routeGrid", populateMemoryDataGrid(""), <%= routeGridLayout %>);
<%}

%>

function populateMemoryDataGrid(parentDataGridID)
{
   var myStore;
	<%-- Srinivasu_D IR#T36000041788 Rel9.3 07/21/2015 - Reverting below changes as causing issues. --%>
   require(["dijit/registry","dojo/dom","dojo/_base/lang", "dojo/_base/array", "dojo/store/Memory" ],
   function(registry, dom, lang, array, Memory)
  <%--  require(["dijit/registry","dojo/dom","dojo/_base/lang", "dojo/_base/array", "dojo/store/Memory", "dojox/html/entities" ], --%>
  <%--  function(registry, dom, lang, array, Memory, entities)    --%>
   {
	myStore = new Memory({data: []});
   	var grid = registry.byId(parentDataGridID);
   	if (grid)
   	{
		var items = grid.selection.getSelected();
		if (items.length)
		{
        		array.forEach(items, function(selectedItem)
        		{
         			if (selectedItem != null)
          			{
					<% if (fromWhere != null)
					{
						if (fromWhere.equals(TradePortalConstants.FROM_TRADE) || fromWhere.equals(TradePortalConstants.FROM_CM) || fromWhere.equals(TradePortalConstants.FROM_CM_FVD))
						{
						%>
							myStore.put({ Instrument: grid.store.getValue(selectedItem,'InstrumentType')+' - '+grid.store.getValue(selectedItem,'InstrumentID')});
						<%
						}else if(fromWhere.equals(TradePortalConstants.FROM_DDI))
						{
						%>
							myStore.put({ Instrument: grid.store.getValue(selectedItem,'InstrumentType')+' - '+grid.store.getValue(selectedItem,'InstrumentID')});
						<%
						}else if(fromWhere.equals(TradePortalConstants.FROM_MESSAGES) || fromWhere.equals(TradePortalConstants.FROM_MESSAGES_DEBIT_FUND))
						{
							%>
							<%-- myStore.put({ Subject: entities.encode(grid.store.getValue(selectedItem,'Subject'))}); --%>
							myStore.put({ Subject: grid.store.getValue(selectedItem,'Subject')});

							<%
						}else if(fromWhere.equals(TradePortalConstants.FROM_PAYREMIT))
						{
							%>
							myStore.put({ PaymentReference: grid.store.getValue(selectedItem,'PaymentReference'), BuyerName: grid.store.getValue(selectedItem,'BuyerName'), Status: grid.store.getValue(selectedItem,'Status')});

							<%		
						}
					}
						%>
          			}
        		});
      		}
   	}else{
   		<% if (fromWhere != null)
					{
						if (fromWhere.equals(TradePortalConstants.FROM_TRADE) || fromWhere.equals(TradePortalConstants.FROM_CM) || fromWhere.equals(TradePortalConstants.FROM_CM_FVD))
						{
							String instr = StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue2))+" - "+ StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue1));						
						%>
							myStore.put({ Instrument: '<%=StringFunction.escapeQuotesforJS(instr)%>' });
						<%
						}else if(fromWhere.equals(TradePortalConstants.FROM_DDI))
						{
							String instr =  StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue2))+" - "+ StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue1));
						%>
							myStore.put({ Instrument: '<%=StringFunction.escapeQuotesforJS(instr)%>' });
						<%
						}else if(fromWhere.equals(TradePortalConstants.FROM_MESSAGES) || fromWhere.equals(TradePortalConstants.FROM_MESSAGES_DEBIT_FUND))
						{
							%>
							myStore.put({ Subject: '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue1))%>'});

							<%
						}else if(fromWhere.equals(TradePortalConstants.FROM_PAYREMIT))
						{
							%>
							myStore.put({ PaymentReference: '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue1))%>', BuyerName: '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue2))%>', Status: '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(gridDisplayValue3))%>' });

							<%		
						}
					}
						%>
		}
  });
  return myStore;
}

function routeItem() {
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
	    	var routeToUser="";
	    	var multipleOrgs;
	    	var multipleRelatedOrgsFlag = <%=multipleRelatedOrgs%>;
    		if(multipleRelatedOrgsFlag){
    			multipleOrgs = 'Y';
    		}else{
    			multipleOrgs = 'N';
    		}
    		var routeUserOid = registry.byId("RouteUserOid").value;

    		var rowKey = '<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(rowKey))%>';
    		var routeCorporateOrgOid = "";
    		var rowKeyValue = new Array();

    		if(multipleRelatedOrgsFlag){
		    	if(registry.byId("RouteToUser").checked){
		    		routeToUser = registry.byId("RouteToUser").value;
		    	}else if(registry.byId("RouteToUser2").checked){
		    		routeToUser = registry.byId("RouteToUser2").value;
		    	}

    			routeCorporateOrgOid = registry.byId("RouteCorporateOrgOid").value;

	    		if(!registry.byId("RouteToUser").checked && !registry.byId("RouteToUser2").checked){
	    			alert("<%=resMgr.getText("RouteItem.PromptMessage1",TradePortalConstants.TEXT_BUNDLE) %>");
	    			return false;
	    		}else if(registry.byId("RouteToUser").checked && registry.byId("RouteUserOid").value == ""){
	    			alert("<%=resMgr.getText("RouteItem.RouteItemPerson",TradePortalConstants.TEXT_BUNDLE) %>");
	    			return false;
	    		}else if(registry.byId("RouteToUser2").checked && registry.byId("RouteCorporateOrgOid").value == ""){
	    			alert("<%=resMgr.getText("RouteItem.OrganizationName",TradePortalConstants.TEXT_BUNDLE) %>");
	    			return false;
	    		}
    		}
    		rowKeyValue = rowKey.split(",");
    		dialog.doCallback('<%=StringFunction.escapeQuotesforJS(routeDialogId)%>', 'select',routeToUser, routeUserOid, routeCorporateOrgOid, multipleOrgs, rowKeyValue );
    	});
}



function closeDialog()
{
    hideDialog('<%=StringFunction.escapeQuotesforJS(routeDialogId)%>');
}

var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(routeDialogId)%>");
var dialogCloseButton=dialogName.closeButtonNode;
dialogCloseButton.setAttribute("id","routeItemDialogCloseLink");
dialogCloseButton.setAttribute("title","");
</script>
<%=widgetFactory.createHoverHelp("routeItemDialogCloseLink", "common.Cancel") %>

</div>
