<%--
 * Ravindra - CR-708B Initial Version
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*,
	com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,  com.amsinc.ecsg.html.*,
        com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*,java.util.*" %>
	

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%


   String invoiceDateDay = null;
   String invoiceDateMonth = null;
   String invoiceDateYear = null; 
   String payment_date = null;
   DocumentHandler  doc = formMgr.getFromDocCache();  
   String globalNavigationTab = userSession.getCurrentPrimaryNavigation();
   //String formName = "ApplyPaymentDateToSelectionOnDetailForm";
   String formName = "InvoiceGroupingDialogForm";
   String buttonName = "GroupInvoices";
   
   String cancelLink=formMgr.getLinkAsUrl("goToInvoiceOffersTransactionsHome", response); 
   WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
   Hashtable secureParms        = new Hashtable();
   String options = Dropdown.createSortedRefDataOptions(TradePortalConstants.CURRENCY_CODE, "", userSession.getUserLocale());
   secureParms.put("SecurityRights", userSession.getSecurityRights());
   secureParms.put("UserOid",        userSession.getUserOid());
   secureParms.put("ownerOrg",        userSession.getOwnerOrgOid());
%>

<%-- check for security --%>


<div id="assignPaymentDateContent" class="dialogContent">
<form id="<%=formName%>" method="post" name="<%=formName%>" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
 <input type=hidden value="" name=buttonName>
 <%= formMgr.getFormInstanceAsInputField(formName, secureParms) %>
  <div class="formItem">
    <%= widgetFactory.createTextField( "GroupName", "InvoicesOffered.InvoiceGroupName", "", "30", false, false, false,  "", "","" ) %>
    <%= widgetFactory.createLabel("InvoiceGroupCondDesc","InvoicesOffered.InvoiceGroupCondDesc", false, false, false,"")%>
  	<div style="clear:both;"></div>
  </div>
  <div class="formItem">
  	<%= widgetFactory.createSelectField( "GroupCurrency", "InvoiceSearch.Currency", "", options, false, false, false, "style=\"width: 50px;\"", "", "inline")%>
  	<%= widgetFactory.createAmountField( "GroupAmount", "InvoicesOffered.Amount", "", "", false, false, false, "style=\"width: 126px;\"", "" , "inline") %>
  	<div style="clear:both;"></div>
  </div>
  
    <div class="formItemWithIndent1">
		<button data-dojo-type="dijit.form.Button"  name="Route" id="Route" type="button">
			<%=resMgr.getText("InvoicesOffered.GroupInvoices",TradePortalConstants.TEXT_BUNDLE) %>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			applyInvoiceGrouping();     					
		</script>
		</button> 
	 
		<button data-dojo-type="dijit.form.Button"  name="cancel" id="cancel" type="button">
			<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			cancelOne();     					
		</script>
		</button> 
 	 </div> 
 </form>
</div>

<script type='text/javascript'>

function applyInvoiceGrouping() {
require(["dijit/registry", "t360/dialog"],
        function(registry, dialog ) {    	
	<%-- IR T36000018837 start - if amount/group name is blank throw error --%>
	var amt = registry.byId('GroupAmount').value;
	var name = registry.byId('GroupName').value;
	
	if("" ==  amt){
		alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS(
				"SupplierPortal.AmountRequired",
				TradePortalConstants.TEXT_BUNDLE))%>');
		return;
	}
	else if("" ==  name){
		alert('<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS(
				"SupplierPortal.NameRequired",
				TradePortalConstants.TEXT_BUNDLE))%>');
		return;
	}
	<%-- IR T36000018837 end --%>
	else{
	submitFormWithParms('<%=formName%>', '<%=buttonName%>', "", '');
		}
    });
}

function cancelOne(){
    
    require(["t360/dialog"], 
                function(dialog) {
    dialog.hide('InvoiceGroupingDialogID');

});
    }
 
</script>
</div>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   doc = formMgr.getFromDocCache();
   doc.setAttribute("/Error", "");
  
   formMgr.storeInDocCache("default.doc", doc);
%>
