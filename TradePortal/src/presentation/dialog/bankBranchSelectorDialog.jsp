<%--
 *  Bank Branch Selector Dialog content.
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<% 

  //read in parameters
  String bankBranchSelectorDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));	//Rel 9.3 XSS CID-11518, Rel9.4 XSS CID 12219
  String bankBranchSelectorDataGrid = StringFunction.xssCharsToHtml(request.getParameter("branchGridId"));
  if(StringFunction.isBlank(bankBranchSelectorDataGrid)){
	  bankBranchSelectorDataGrid="BankBranchSelectorDataGrid";
  }
  
  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = "";
//jgadela  R92 - SQL Injection FIX
  List<Object> sqlParamsLst = new ArrayList();

  String ownerOrg = userSession.getOwnerOrgOid();
  CorporateOrganizationWebBean corpOrg = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(ownerOrg);
  String[] opBankOid = {corpOrg.getAttribute("first_op_bank_org"),
                        corpOrg.getAttribute("second_op_bank_org"),
                        corpOrg.getAttribute("third_op_bank_org"),
                        corpOrg.getAttribute("fourth_op_bank_org")};

  //do a local query to get names for operational bank orgs
  //could do this as dataview, but this is simpler - we don't expect this to 
  //change on this dialog via search.
  DocumentHandler oboListDoc = null;
  StringBuffer in = new StringBuffer("?");
  sqlParamsLst.add(opBankOid[0]);
  for (int oboCount=1; oboCount<4; oboCount++) {
    if (StringFunction.isBlank(opBankOid[oboCount])) {
      break;
    }
    else {
        in.append(", ").append("?");
        sqlParamsLst.add(opBankOid[oboCount]);
    }
  }
  try {
    StringBuffer sql = new StringBuffer();
    sql.append("select ORGANIZATION_OID, NAME");
    sql.append(" from OPERATIONAL_BANK_ORG");
    sql.append(" where ORGANIZATION_OID in (").append(in).append(")");
    sql.append(" order by name");

    oboListDoc = DatabaseQueryBean.getXmlResultSet(sql.toString(), true, sqlParamsLst);

  } catch (AmsException e) {
    //todo: should throw an error
    Debug.debug("queryListView threw an exception");
    e.printStackTrace();
  }
%>

<div id="bankBranchSelectorDialogContent" class="dialogContent">

  <div class="instruction">
    <%= resMgr.getText("bankBranchSelectorDialog.instructions", TradePortalConstants.TEXT_BUNDLE) %>
  </div>

<%
  gridHtml = dgFactory.createDataGrid(bankBranchSelectorDataGrid,bankBranchSelectorDataGrid,null);
%>
  <%= gridHtml %>

</div>

<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>

  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {

<%
  String bbGridLayout = dgFactory.createGridLayout(bankBranchSelectorDataGrid, bankBranchSelectorDataGrid);
%>
    var gridLayout = <%= bbGridLayout %>;
var gridId=  "<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDataGrid)%>";
<%
  //get the grid data
  //this reevaluates from the user session data rather than taking passed in bank branch
  //values for security
  StringBuffer bbGridData = new StringBuffer();
  if ( oboListDoc != null ) {
    Vector oboList = oboListDoc.getFragments("/ResultSetRecord");
    for ( int oboIdx = 0; oboIdx<oboList.size(); oboIdx++ ) {
      DocumentHandler oboDoc = (DocumentHandler) oboList.get(oboIdx);
      String orgOid = oboDoc.getAttribute("/ORGANIZATION_OID");
      String orgName = oboDoc.getAttribute("/NAME");
      bbGridData.append("{ ");
      bbGridData.append("rowKey:'").append(orgOid).append("'");
      bbGridData.append(", ");
      bbGridData.append("BankBranch:'").append(StringFunction.xssCharsToHtml(orgName)).append("'");
      bbGridData.append(" }");
      if ( oboIdx < oboList.size()-1) {
        bbGridData.append(", ");
      }
    }
  }
%>
    var gridData = [ <%= bbGridData.toString() %> ];

    <%--create a local memory grid--%>
    t360grid.createMemoryDataGrid(gridId, gridData, gridLayout);
  });

  <%-- on select perform execute the select callback function --%>
  function selectBB() {
	  var gridId=  "<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDataGrid)%>";
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
    	
      <%--get array of rowkeys from the grid--%>
      var rowKeys = getSelectedGridRowKeys(gridId);
         if (rowKeys && rowKeys.length == 1 ) {
        <%-- bankBranchSelectorDialogSelect(rowKeys[0]); --%>
        dialog.doCallback('<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDialogId)%>', 'select',rowKeys[0]);
      }else{
    	  alert("Please select the Bank/Branch or press 'Cancel'");
			return;
      }
    });
  }

  function closeBBDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDialogId)%>');
    });
  }

	var dialogName=dijit.byId("<%=StringFunction.escapeQuotesforJS(bankBranchSelectorDialogId)%>");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","bankBranchSelectorDialogCloseLink");
	dialogCloseButton.setAttribute("title","");
</script>
	<%=widgetFactory.createHoverHelp("bankBranchSelectorDialogCloseLink", "common.Cancel") %>