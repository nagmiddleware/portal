<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 
 *     All rights reserved
--%>
<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
				 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 		 com.amsinc.ecsg.frame.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%
Debug.debug("***START********************Buyer*SEARCH**************************START***");


/******************************************************************************
 * BankSearch.jsp
 *
 * Filtering:
 * This page originally displays a listview containing all possible Bank/Branch Code and branch address for a given bank,
 * but user may also choose to narrow down the results by searching by City, Province, Bank Name, or Branch Name 
 * which will filter the existing results and link that redisplays the page with the filter text passed as a url parameter.
 * JavaScript is used to dynamically build that link.
 *
 * Selecting:
 * JavaScript is used to detect which radio button is selected, and the value
 * of that button is passed as a url parameter to page from which the search
 * page was called.
 ******************************************************************************/
WidgetFactory widgetFactory = new WidgetFactory(resMgr);
String  newLink     = formMgr.getLinkAsUrl("selectARMMatchRule",response); 
String cancelAction ="PaymentMatchResponse";
%>

     
      <div class="dialogContent fullwidth">

<%-- Form Content Area starts here --%>
 <div>
  <div class="searchDetail lastSearchDetail">
    <span class="searchCriteria">
	<%=widgetFactory.createRadioButtonField("FilterText","filterText1","BuyerSearchDialog.BuyerName","",true,false,"onChange=preSearch();style='display:inline';","") %>
       	   <span class="formItemWithIndent4"> 
         <%=widgetFactory.createTextField("BuyerName","","","30",false,false,false,"onKeydown='Javascript: filterBuyerOnEnter(\"BuyerName\");'","","none" )%>
         </span> 
         <%=widgetFactory.createRadioButtonField("FilterText","filterText2","BuyerSearchDialog.BuyerId","",false,false,"onChange=preSearch();style='display:inline';","") %>
         	<span class="formItemWithIndent3"> 
        <%=widgetFactory.createTextField("BuyerId","","","30",false,false,false,"DISABLED onKeydown='filterBuyerOnEnter(\"BuyerId\");'","","none" )%>
        </span>
          </span>
          <span class="searchActions">
 	        <button data-dojo-type="dijit.form.Button" type="button" id="Search">Search
				<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
					search();
				</script>
			</button>
            <button data-dojo-type="dijit.form.Button" type="button" id="newButton">
        		<%=resMgr.getText("transaction.New",TradePortalConstants.TEXT_BUNDLE)%>
         		<script type="dojo/method" data-dojo-args="evt">
						newBuyer();return false;
      			</script>
    	    </button>
 		</span>
		 <div style="clear:both"></div>
    </div>
    <%=widgetFactory.createHoverHelp("Search", "SearchHoverText") %>
    <%=widgetFactory.createHoverHelp("buyerSearchDialogCloseButton", "common.Cancel") %>
 <%
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = dgFactory.createDataGrid("buyerSearchGrid","BuyerSearchDataGrid",null);
  String gridLayout = dgFactory.createGridLayout("buyerSearchGrid", "BuyerSearchDataGrid");  

%>
 </div>
<%= gridHtml %>
</div>
<%=widgetFactory.createHoverHelp("newButton","NewHoverText") %>

<script>
require(["t360/datagrid", "dojo/domReady!"],
	      function( t360grid ) {
	  var gridLayout = <%= gridLayout %>;
	  var initSearchParms = "parentOrgID=<%=StringFunction.xssCharsToHtml(userSession.getOwnerOrgOid())%>";
	  var searchParms = "";
	
	  <%--create the grid, attaching it to the div id specified in dataGrid.jsp--%>
	  console.log('initSearchParms='+initSearchParms);
	  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BuyerSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	  var buyerSearchGrid = 
		  t360grid.createDataGrid("buyerSearchGrid", viewName, gridLayout, initSearchParms);
	  buyerSearchGrid.set('autoHeight',10);
});
	  function search(){
		  require(["dijit/registry"],function(registry){
			  if(registry.byId("filterText1").checked){
				  var filterText1=(registry.byId("BuyerName").get('value')).toUpperCase();
				  console.log("BuyerName Checked");
				  searchParms="filterText1="+filterText1;
				  }else if(registry.byId("filterText2").checked){
				  var filterText2=(registry.byId("BuyerId").get('value')).toUpperCase();
				  searchParms="filterText2="+filterText2;
				  console.log("BuyerName Checked");
				  }
			  	searchParms=initSearchParms+"&"+searchParms;
				  console.log("SearchParms: " + searchParms);
				  searchDataGrid("buyerSearchGrid", "BuyerSearchDataView",searchParms);});
	  }
	
	  function chooseBuyer() {
		  console.log('hello');
		  require(["dijit/registry", "t360/dialog"],function(registry, dialog){
			  
			  var item = registry.byId('buyerSearchGrid').selection.getSelected();
			  console.log('item='+item);
			  if (item.length == 0){
					alert('No Buyer selected');
					return;
				} else {
					var buyerDetails = new Array();
					buyerDetails[0] = item[0].i.BuyerName;
					buyerDetails[1] = item[0].i.BuyerId;
					console.log(buyerDetails[0] + '		'+ buyerDetails[0]);
					dialog.doCallback('buyerSearchDialog','select', buyerDetails);
					dialog.hide('buyerSearchDialog');
				}
		  });
	  }
	  
	  function cancelDialog(){
		require(["t360/dialog"], function(dialog) {
			dialog.hide('buyerSearchDialog');
		});
	  }
	  
	  function filterBuyerOnEnter(fieldId){
			<%-- 
		  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
		  	 --%>
		  	require(["dojo/on","dijit/registry"],function(on, registry) {
			    on(registry.byId(fieldId), "keypress", function(event) {
			        if (event && event.keyCode == 13) {
			        	dojo.stopEvent(event);
			        	search();
			        }
			    });
			});
		}
	  
	  function preSearch(){
		  require(["dojo/dom", "dojo/domReady!"], function(dom) {
			  
			  if(dom.byId("filterText1").checked){
				  console.log("BuyerName Checked");
				  
				  dom.byId("BuyerId").value="";
				  dijit.byId("BuyerId").set('disabled',true);
				  dijit.byId("BuyerName").set('disabled',false);
				  
				  
			  }else if(dom.byId("filterText2").checked){
				  console.log("BuyerId Checked");
				  
				  dom.byId("BuyerName").value="";
				  dijit.byId("BuyerName").set('disabled',true);
				  dijit.byId("BuyerId").set('disabled',false);
				  
				  
			  }
			  
		  });
	  }
	  var dialogName=dijit.byId("buyerSearchDialog");
		var dialogCloseButton=dialogName.closeButtonNode;
		dialogCloseButton.setAttribute("id","buyerSearchDialogCloseButton");
		
		function newBuyer(){
			require(["dijit/registry", "dojo/on", "dojo/ready"],
	          function(registry, on, ready ) {
				ready(function() {
				console.log('page ready');
	            console.log('NewLink='+'<%=newLink%>');
	            var newPartyButton = registry.byId("newButton");
				if ( newPartyButton ) {
					newPartyButton.on("click", function() {
					window.location = "<%=newLink%>";
					});
				}
				});
	         });
			}
		

		</script>

</body>
</html>