<%--
 * All Transactions
 *  Please do not put any calling page specific functionality
 *  here - it should be placed on the calling page and passed in and
 *  executed as a callback function.
 *  Also note that the wrapping div for this dialog is included in the 
 *  footer and is included on the page when the menu is displayed.
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<div class="formItem">
<%


  //read in parameters
  String allTransactionsDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = "";
  String gridLayout="";

  final String ALL = "common.all";
  final String ALL_WORK = "common.allWork";
  final String MY_WORK = "common.myWork";

  String  userOid        = userSession.getUserOid();
  String  userOrgOid	 = userSession.getOwnerOrgOid();
  String  loginLocale    = userSession.getUserLocale(); 	
  String  loginRights    = userSession.getSecurityRights();
  String  defaultWIPView = userSession.getDefaultWipView();
  String  currentSecurityType = userSession.getSecurityType();
//build alltran options
  String currentAllTranWork = MY_WORK;
  String currentAllTranGroup = TradePortalConstants.INSTR_GROUP__ALL;
  String currentAllTranInstrType = ALL;
  String currentAllTranStatus = TradePortalConstants.STATUS_ALL;
  DocumentHandler hierarchyDoc = null;
  int totalOrganizations = 0;
  ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();
  if (!TradePortalConstants.ADMIN.equals(currentSecurityType)) {
	  StringBuffer sqlQuery = new StringBuffer();
	    // This is the query used for populating the option drop down list; 
	    // it retrieves all active child organizations that belong to the user's 
	    // org in addition to the user's org itself.  It is used by both the mail
	    // and notification.
	    sqlQuery.append("select organization_oid, name");
	    sqlQuery.append(" from corporate_org");
	    sqlQuery.append(" where activation_status = ?");
	    sqlQuery.append(" start with organization_oid = ?");
	    sqlQuery.append(" connect by prior organization_oid = p_parent_corp_org_oid");
	    sqlQuery.append(" order by ");
	    sqlQuery.append(resMgr.localizeOrderBy("name"));
	    
	    Object sqlParams[] = new Object[2];
	    sqlParams[0] = TradePortalConstants.ACTIVE; 
	    sqlParams[1] = userOrgOid;

	    hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery.toString(), false, sqlParams);	    
	    totalOrganizations = hierarchyDoc.getFragments("/ResultSetRecord").size();
  }

  StringBuffer allTranWork = new StringBuffer();
  // If the user has rights to view child organization work, retrieve the 
  // list of dropdown options from the query listview results doc (containing
  // an option for each child org) otherwise, simply use the user's org 
  // option to the dropdown list (in addition to the default 'My Work' option).
  if(!userSession.hasSavedUserSession()) {
    allTranWork.append("<option value=\"").
      append(EncryptDecrypt.encryptStringUsingTripleDes(MY_WORK, userSession.getSecretKey())).
      append("\" selected >");
    allTranWork.append(resMgr.getText( MY_WORK, TradePortalConstants.TEXT_BUNDLE));
    allTranWork.append("</option>");
  }
  if (SecurityAccess.hasRights(loginRights, 
                               SecurityAccess.VIEW_CHILD_ORG_WORK)) {
    allTranWork.append(Dropdown.getUnsortedOptions(hierarchyDoc, 
                                                    "ORGANIZATION_OID", "NAME", "",
                                                    currentAllTranWork, userSession.getSecretKey()));
    // Only display the 'All Work' option if the user's org has child organizations
    if (totalOrganizations > 1) {
      allTranWork.append("<option value=\"");
      allTranWork.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_WORK, userSession.getSecretKey()));
      allTranWork.append("\"");
      if (ALL_WORK.equals(currentAllTranWork)) {
        allTranWork.append(" selected");
      }
      allTranWork.append(">");
      allTranWork.append(resMgr.getText( ALL_WORK, TradePortalConstants.TEXT_BUNDLE));
      allTranWork.append("</option>");
    }
  }
  else {
    allTranWork.append("<option value=\"");
    allTranWork.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
    allTranWork.append("\"");
    if (userOrgOid.equals(currentAllTranWork)) {
      allTranWork.append(" selected");
    }
    allTranWork.append(">");
    allTranWork.append(userSession.getOrganizationName());
    allTranWork.append("</option>");
  }

%>
    <%= widgetFactory.createSearchSelectField("AllTranWorkDialog","Home.AllTransactions.Show", "", 
          allTranWork.toString(), " class=\"char15\"" )%>

<%
  StringBuffer allTranGroups = new StringBuffer();
  allTranGroups.append("<option value='").
    append(TradePortalConstants.INSTR_GROUP__ALL).
    append("'>").
    append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__ALL, loginLocale)).
    append("</option>");
  allTranGroups.append("<option value='").
    append(TradePortalConstants.INSTR_GROUP__TRADE).
    append("'>").
    append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__TRADE, loginLocale)).
    append("</option>");
  allTranGroups.append("<option value='").
    append(TradePortalConstants.INSTR_GROUP__PAYMENT).
    append("'>").
    append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__PAYMENT, loginLocale)).
    append("</option>");
  allTranGroups.append("<option value='").
    append(TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT).
    append("'>").
    append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__DIRECT_DEBIT, loginLocale)).
    append("</option>");
  allTranGroups.append("<option value='").
    append(TradePortalConstants.INSTR_GROUP__RECEIVABLES).
    append("'>").
    append(refData.getDescr("INSTRUMENT_GROUP", TradePortalConstants.INSTR_GROUP__RECEIVABLES, loginLocale)).
    append("</option>");

%>
    <%= widgetFactory.createSearchSelectField( "AllTranGroupDialog", "Home.AllTransactions.criteria.InstrumentGroup", "", 
          allTranGroups.toString(), " class=\"char10\"" )%>

<%
  StringBuffer allTranInstrTypes = new StringBuffer();
  allTranInstrTypes.append("<option value='").append(ALL).append("'").
    append(" selected >").
    append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
    append("</option>");
  //popupate the transaction type drop down with all instrument types based on what the user can see
  Vector viewableInstruments = Dropdown.getViewableInstrumentTypes(formMgr, 
    new Long(userOrgOid), loginRights, userSession.getOwnershipLevel(), new Long(userOid), "AllTranInstrTypeDialog");
  String viewableInstrTypes = Dropdown.getInstrumentList("",  //selected instr type
    loginLocale, viewableInstruments );
  allTranInstrTypes.append(viewableInstrTypes);
  //todo: all add to top
  //this is labelled tran type, but it is the set of instrument types
%>
    <%= widgetFactory.createSearchSelectField( "AllTranInstrTypeDialog", "Home.AllTransactions.criteria.InstrumentType", "", 
          allTranInstrTypes.toString()) %>

<%
  StringBuffer allTranStatus = new StringBuffer();
  allTranStatus.append("<option value='").append(ALL).append("'").
    append(" selected >").
    append(resMgr.getText( ALL, TradePortalConstants.TEXT_BUNDLE)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZE_FAILED, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_AUTHORIZED).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_AUTHORIZED, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_CANCELLED_BY_BANK, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_DELETED).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_DELETED, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PARTIALLY_REJECTED_PAYMENT, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_PROCESSED_BY_BANK, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_READY_TO_AUTHORIZE, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_BY_BANK, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_REJECTED_PAYMENT, loginLocale)).
    append("</option>");
  allTranStatus.append("<option value='").
    append(TradePortalConstants.TRANS_STATUS_STARTED).
    append("'>").
    append(refData.getDescr("TRANSACTION_STATUS", TradePortalConstants.TRANS_STATUS_STARTED, loginLocale)).
    append("</option>");
%>
    <%=widgetFactory.createSearchSelectField( "AllTranStatusDialog", "Home.AllTransactions.criteria.Status", "",
          allTranStatus.toString(), " class=\"char21\"" )%>
  </div>
<div class="dialogContent fullwidth">
<%
  gridHtml = dgFactory.createDataGrid("allTranGridDialog","HomeAllTransactionsDataGrid",null);
  gridLayout = dgFactory.createGridLayout("allTranGridDialog","HomeAllTransactionsDataGrid"); 
%>
  <%= gridHtml %>


</div>

<%--the following script loads the datagrid.  note that this dialog
    is a DialogSimple to allow scripts to execute after it loads on the page--%>
<script type='text/javascript'>

  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {
	  var gridLayout = <%=gridLayout%>;   
	  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("HomeAllTransactionsDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	    t360grid.createDataGrid("allTranGridDialog",viewName,gridLayout,"");
  });

</script>
