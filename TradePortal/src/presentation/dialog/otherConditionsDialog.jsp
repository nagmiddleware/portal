<%--
*******************************************************************************
                              Other Conditions PopUp

  Description: 
  	This PopUp will open when user clicks on Other Conditions/ Other Details/ Instructions to Bank
   	On Click of save the call JSP will be submitted
*******************************************************************************
--%>

<%--
 *		Dev Sandeep
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,java.math.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" 
   scope="session">
</jsp:useBean>


<%-- ************** Data retrieval page setup begins here ****************  --%>

<%

	WidgetFactory widgetFactory = new WidgetFactory(resMgr);	
	//String dialogIdName = request.getParameter("dialogId");
	String otherConditionsDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
	String otherConditionsFieldData = StringFunction.xssCharsToHtml(request.getParameter("otherConditionsFieldData"));
%>
	<div class="formContent">
		<br><br>
		<%-- <%= widgetFactory.createTextArea("AddlConditionsTextDailog", "", otherConditionsFieldData, false ) %> --%>
		<%= widgetFactory.createTextArea("AddlConditionsTextDailog", "", otherConditionsFieldData, false ,false, false, "style='width:auto;' rows='10' cols='60'", "","" ) %>
     	<span class="formItem title-right">

     		
     		<button data-dojo-type="dijit.form.Button" id="SaveTheButton" type="button" >
        	<%=resMgr.getText("common.SaveText", TradePortalConstants.TEXT_BUNDLE)%>
         	<script type="dojo/method" data-dojo-event="onClick" id="SaveButton" data-dojo-args="evt">
	        	saveOtherConditions();
	      	</script>
     		</button>
     		<%= widgetFactory.createHoverHelp("SaveTheButton", "SaveHoverText")%>
     		
     		<button data-dojo-type="dijit.form.Button" type="button" id="CancelButton" ><%= resMgr.getText("common.cancel", 
                             TradePortalConstants.TEXT_BUNDLE) %>
                <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        		closeOtherConditionsDialog();
	      		</script>         
 			</button>
 			<%= widgetFactory.createHoverHelp("CancelButton", "CloseHoverText")%>
     	</span>
     	<br><br><br>
	</div>
	        			
	<script type="text/javascript">
		<%-- on select perform execute the select callback function --%>
		function saveOtherConditions() {
		  	<%-- todo: add external event handler to datagrid footer items --%>
		  	require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],function(registry, query, on, dialog ) {
		    	<%--get array of rowkeys from the grid--%>
		      	
		    	var otherConditionsData = registry.byId('AddlConditionsTextDailog').value;
		    	
		    	dialog.hide('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(otherConditionsDialogId))%>');
		    	dialog.doCallback('<%= StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(otherConditionsDialogId))%>', 'SaveTrans', otherConditionsData);
		  	});
		}
	  
		function closeOtherConditionsDialog() {
	    	<%-- todo: add external event handler to datagrid footer items --%>
	    	require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],function(registry, query, on, dialog ) {
	      		dialog.hide('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(otherConditionsDialogId))%>');
	    	});
	  	}
		
		var dialogName=dijit.byId('<%=StringFunction.escapeQuotesforJS(otherConditionsDialogId)%>');
		var dialogCloseButton=dialogName.closeButtonNode;
		dialogCloseButton.setAttribute("id","OtherConditionsDialogCloseButton");	
	</script>
	
	<%=widgetFactory.createHoverHelp("OtherConditionsDialogCloseButton", "common.Cancel") %>