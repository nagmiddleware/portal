<%--
 * Ravindra - CR-708B Initial Version
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.util.*, com.ams.tradeportal.busobj.webbean.*, com.ams.tradeportal.common.*,
	com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.frame.*,  com.amsinc.ecsg.html.*,
        com.amsinc.ecsg.web.*, com.ams.tradeportal.html.*" %>
	

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%

		
   WidgetFactory widgetFactory = new WidgetFactory(resMgr,userSession);
   String formName = "AssignFutureValueDateDialogForm";   
   String dialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));
   String datePattern = userSession.getDatePattern();
   String dateWidgetOptions = "constraints:{datePattern:'"+datePattern+"'},placeHolder:'" + datePattern + "'"; 
  %>

<%-- check for security --%>


<div id="assignFutureValueDateContent" class="dialogContent">
<form id="<%=formName%>" method="post" name="<%=formName%>" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
 <%--input type=hidden value="" name=buttonName--%>
 <%-- formMgr.getFormInstanceAsInputField(formName, secureParms) --%>
  <div style="float: left; width: 49%;">
  <div class="formItem">
    <%= widgetFactory.createDateField("FutureValueDate", "InvoicesOffered.FutureValueDate", "", false,false, false, "class='char10'",dateWidgetOptions, "")%>  
  </div>
    <div class="formItem">
	<button data-dojo-type="dijit.form.Button"  name="Route" id="Route" type="button">
		<%=resMgr.getText("InvoicesOffered.AssignFutureValueDate",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			applyFutureValueDate();     					
		</script>
	</button> 
 
	<button data-dojo-type="dijit.form.Button"  name="cancel" id="cancel" type="button">
		<%=resMgr.getText("common.cancel",TradePortalConstants.TEXT_BUNDLE) %>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			cancelOne();     					
		</script>
	</button> 
	</div> 	  
  </div>
                <div style="float: right;width: 40%;padding-left: 5%;padding-right: 5%;">
                    <%=resMgr.getText("InvoicesOffered.FutureValueDateNotice",TradePortalConstants.TEXT_BUNDLE) %>
                </div>
 </form>
</div>

<script type='text/javascript'>

function applyFutureValueDate() {
	require(["dijit/registry", "t360/dialog","dojo/dom" ],
	        function(registry, dialog,dom ) {     		
            var futureValueDate = dom.byId("FutureValueDate").value;
            	<%-- registry.byId("FutureValueDate");		 --%>
	     dialog.doCallback('<%=StringFunction.escapeQuotesforJS(StringFunction.xssCharsToHtml(dialogId))%>', 'selectFVD',futureValueDate);
    });
}

function cancelOne(){
    
    require(["t360/dialog"], 
                function(dialog) {
    dialog.hide('<%=StringFunction.escapeQuotesforJS(dialogId)%>');

});
    }
 
</script>
</div>
</body>
</html>

<%
  /**********
   * CLEAN UP
   **********/

   // Finally, reset the /Error portion of the cached document 
   // to eliminate carryover of errors from one page to another
   // The rest of the data is not eliminated since it will be
   // use by the Route mediator and other transaction pages
   
   //doc = formMgr.getFromDocCache();
   //doc.setAttribute("/Error", "");
  
   //formMgr.storeInDocCache("default.doc", doc);
%>
