<%--
*******************************************************************************
                                  Admin User Detail

  Description:
     This page is used to maintain corporate users.  It supports insert,
  update and delete.  Errors found during a save are redisplayed on the same 
  page.  Successful updates return the user to the ref data home page.

  Assumption: The organization and level of the org are always known.  
  Therefore, for a new user that information is passed in to this page.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" 
   scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" 
   scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%


  String adminUserOrgSelectionDialogId = StringFunction.xssCharsToHtml(request.getParameter("dialogId"));

  WidgetFactory widgetFactory = new WidgetFactory(resMgr);
  DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
  String gridHtml = "";
  String options;
  String loginRights    = userSession.getSecurityRights();

  DocumentHandler orgDoc = new DocumentHandler();
  String ownershipLevel = userSession.getOwnershipLevel();

  StringBuilder sql = new StringBuilder();

  QueryListView queryListView = null;

  try {

      queryListView = (QueryListView) EJBObjectFactory.createClientEJB(
				formMgr.getServerLocation(), "QueryListView"); 

      // Now get a list of organizations.  This is a little tricky.  By selecting
      // an org, the user is implicitly selecting an ownership level as well.  To
      // accomplish this, we'll get a list of organizations (which the user can 
      // view) and rather than selecting just the org's oid, we'll select the org's
      // oid and the ownership level.  This will be one value appended together
      // with a '/' separator.  (Thus, it might be 100/GLOBAL).

    sql = new StringBuilder();
      List<Object> sqlParamsListOrg = new ArrayList();
     if (ownershipLevel.equals(TradePortalConstants.OWNER_GLOBAL)) {
         // For global org users, the list of orgs is the global org plus all
         // client banks
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_GLOBAL);
         sql.append("' as OwnerOrgAndLevel, name from global_organization");
         sql.append(" where activation_status=?");
         sql.append(" union ");
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BANK);
         sql.append("' as OwnerOrgAndLevel, name from client_bank");
         sql.append(" where activation_status=?");
         
         sqlParamsListOrg.add(TradePortalConstants.ACTIVE);
         sqlParamsListOrg.add(TradePortalConstants.ACTIVE);
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BANK)) {
         // For Client Bank users, the list of orgs is the user's client bank
         // plus all BOGs for that client bank
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BANK);
         sql.append("' as OwnerOrgAndLevel, name from client_bank");
         sql.append(" where organization_oid = ?");
         sql.append(" and activation_status=?");
         sql.append(" union ");
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BOG);
         sql.append("' as OwnerOrgAndLevel, name from bank_organization_group");
         sql.append(" where p_client_bank_oid = ?");
         sql.append(" and activation_status=?");

         sqlParamsListOrg.add( userSession.getClientBankOid());
         sqlParamsListOrg.add(TradePortalConstants.ACTIVE);
         sqlParamsListOrg.add(userSession.getClientBankOid());
         sqlParamsListOrg.add(TradePortalConstants.ACTIVE);
         
      } else if (ownershipLevel.equals(TradePortalConstants.OWNER_BOG)) {
         // For BOG users, the list of orgs is only the user's bog.  (Note
         // this is provided only for consistency.  The org field is not a
         // visible field for BOG users.)
         sql.append("select organization_oid || '/");
         sql.append(TradePortalConstants.OWNER_BOG);
         sql.append("' as OwnerOrgAndLevel, name from bank_organization_group");
         sql.append(" where organization_oid = ?" );
         sqlParamsListOrg.add(userSession.getBogOid());
      }
      sql.append(" order by name");

      Debug.debug(sql.toString());
      queryListView.setSQL(sql.toString(),sqlParamsListOrg);

      queryListView.getRecords();

      orgDoc = queryListView.getXmlResultSet();
      Debug.debug(orgDoc.toString());


      // Special navigation logic: if there is only one org, there's no
      // point in making the user select it.  (This is always true for a BOG user
      // -- he can only add users for his BOG.)  Simply pass this org/level to
      // the AdminUserDetail page.
      if (queryListView.getRecordCount() ==1) {
        String orgAndLevel = orgDoc.getAttribute("/ResultSetRecord(0)/OWNERORGANDLEVEL");
        orgAndLevel = EncryptDecrypt.encryptStringUsingTripleDes(orgAndLevel, userSession.getSecretKey());
        formMgr.setCurrPage("AdminUserDetail");
        String physicalPage = 
                    NavigationManager.getNavMan().getPhysicalPage("AdminUserDetail", request);
%>
        <jsp:forward page='<%= physicalPage %>'>
          <jsp:param name="OrgAndLevel" value="<%=orgAndLevel%>" />
        </jsp:forward>
<%
      }
      

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
          if (queryListView != null) {
              queryListView.remove();
          }
      } catch (Exception e) {
          System.out.println("error removing querylistview in AdminUserOrgSelection.jsp");
      }
  }  // try/catch/finally block



  //SECURE PARAMETERS
  Hashtable secParms = new Hashtable();
  secParms.put("userOid", userSession.getUserOid());
  secParms.put("securityRights", loginRights);

%>

<%-- ********************* HTML for page begins here *********************  --%>

<form name="AdminUserOrgSelection" method="post" 
      action="<%= request.getContextPath()+NavigationManager.getNavMan().getPhysicalPage("ValidateAdminUserOrgSelection", request) %>">
  <input type=hidden value="" name=buttonName>
  <input type="hidden" name="OrgAndLevel" />
<%
  String onLoad = "document.AdminUserOrgSelection.OrgAndLevel.focus();";
%>


  <div id="adminUserOrgSelectionDialogContent" class="dialogContent">

  <div class="instruction">
    <%= resMgr.getText("NewAdminUser.SelectTheOrg", TradePortalConstants.TEXT_BUNDLE) %>
  </div>

<%
  gridHtml = dgFactory.createDataGrid("adminUserSelectionGrid","AdminUserSelectorDataGrid",null);
%>
  <%= gridHtml %>

</div>
</form>
</body>
</html>

<%
  formMgr.storeInDocCache("default.doc", new DocumentHandler());
  Debug.debug("***END************NEW ADMIN USER****************END***");
%>

<script type='text/javascript'>

  require(["t360/datagrid", "dojo/domReady!"],
      function( t360grid ) {

<%
  String bbGridLayout = dgFactory.createGridLayout("adminUserSelectionGrid", "AdminUserSelectorDataGrid");
%>
    var gridLayout = <%= bbGridLayout %>;

<%
  //get the grid data
  //this reevaluates from the user session data rather than taking passed in bank branch
  //values for security
  StringBuffer adminUserSelectionGridData = new StringBuffer();
  if ( orgDoc != null ) {
    Vector oboList = orgDoc.getFragments("/ResultSetRecord");
    for ( int oboIdx = 0; oboIdx<oboList.size(); oboIdx++ ) {
      DocumentHandler oboDoc = (DocumentHandler) oboList.get(oboIdx);
      String orgOid = oboDoc.getAttribute("/OWNERORGANDLEVEL");
      String orgName = oboDoc.getAttribute("/NAME");
      adminUserSelectionGridData.append("{ ");
      adminUserSelectionGridData.append("rowKey:'").append(orgOid).append("'");
      adminUserSelectionGridData.append(", ");
      adminUserSelectionGridData.append("OrganisationName:'").append(StringFunction.xssCharsToHtml(orgName)).append("'");
      adminUserSelectionGridData.append(" }");
      if ( oboIdx < oboList.size()-1) {
    	  adminUserSelectionGridData.append(", ");
      }
    }
  }
%>
    var gridData = [ <%= adminUserSelectionGridData.toString() %> ];

    <%--create a local memory grid--%>
    var grid= t360grid.createMemoryDataGrid("adminUserSelectionGrid", gridData, gridLayout);
	 grid.set('autoHeight',10);
  });

  <%-- on select perform execute the select callback function --%>
  function selectBB() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      <%--get array of rowkeys from the grid--%>
      var rowKeys = getSelectedGridRowKeys("adminUserSelectionGrid");
      if (rowKeys == null ||rowKeys == "") {
    	  alert("<%= resMgr.getText("AdminUserSelector.AlertWarning", TradePortalConstants.TEXT_BUNDLE) %>");
    	  return false;
         }
      if (rowKeys && rowKeys.length == 1 ) {
    	  dialog.doCallback('<%=StringFunction.escapeQuotesforJS(adminUserOrgSelectionDialogId)%>', 'select',rowKeys[0]);
      }
    });
  }

  function closeBBDialog() {
    <%-- todo: add external event handler to datagrid footer items --%>
    require(["dijit/registry", "dojo/query", "dojo/on", "t360/dialog", "dojo/domReady!"],
        function(registry, query, on, dialog ) {
      dialog.hide('<%=StringFunction.escapeQuotesforJS(adminUserOrgSelectionDialogId)%>');
    });
  }

  
	  var dialogName=dijit.byId("adminUserOrgSelectionDialogId");
	  var dialogCloseButton=dialogName.closeButtonNode;
	  dialogCloseButton.setAttribute("id","adminUserOrgSelectionDialogIdClose");
  
</script>
<%= widgetFactory.createHoverHelp("adminUserOrgSelectionDialogIdClose","common.Cancel")%>