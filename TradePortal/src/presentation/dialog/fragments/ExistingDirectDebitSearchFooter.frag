<%--
*******************************************************************************
  DirectDebit History List View Footer

  Description: Everything that should be loaded at the end of the page
               should be included here.  This includes non-visible html 
               and javascript that should execute on page load.

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>


<%
   gridLayout = dgFac.createGridLayout("existingDirectDebitSearchGridId","ExistingDirectDebitSearchDataGrid");
   String orgId = EncryptDecrypt.encryptStringUsingTripleDes(selectedOrg, userSession.getSecretKey());
  
%>

<script type="text/javascript">
	require(["t360/datagrid", "dojo/domReady!"],function( t360grid ) {	

	  var gridLayout = <%= gridLayout %>;
	  var initSearchParms = "active=true&inactive=false&organisation=<%=orgId%>";

	  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("ExistingDirectDebitSearchDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
	  var myGrid = t360grid.createDataGrid("existingDirectDebitSearchGridId",viewName,gridLayout,initSearchParms);
          <%-- cquinton 10/16/2012 ir#6448 --%>
          <%-- because its on a dialog explicitly set to 10 rows so it doesn't take over the browser --%>
          myGrid.set('autoHeight',10); 
	});

	function filterDDsOnEnterDDI_DLG_(fieldId, filterType){
		<%-- 
	  	* The following code enabels the SEARCH button functionality when ENTER key is pressed in search boxes
	  	 --%>
	  	require(["dojo/on","dijit/registry"],function(on, registry) {
		    on(registry.byId(fieldId), "keypress", function(event) {
		        if (event && event.keyCode == 13) {
		        	dojo.stopEvent(event);
		        	searchDirectDebitHistoryDDI_DLG_(filterType);
		        }
		    });
		});	
	}
	
	
	
  	function searchDirectDebitHistoryDDI_DLG_(buttonId){
  		require(["dojo/dom","dojo/date/locale","dojo/domReady!"],	function(dom,locale){

		
			 var inact = dom.byId("DDI_DLG_InActive").checked,
				act = dom.byId("DDI_DLG_Active").checked,
				org='',
				searchParms='';
	    
			if (<%=orgListDropDown%>)
				org = dijit.byId('DDI_DLG_org').attr('value');
			else
				org = "<%=orgId%>";
			
			searchParms+="inactive="+inact;
			searchParms+="&active="+act;
        	searchParms+="&organisation="+org;

	        if(buttonId=="Basic"){
		    	var instId = dom.byId("DDI_DLG_InstrumentIdBasic").value,
		    		caId = dijit.byId("DDI_DLG_CreditAcctBasic").value,
		    		refId = dom.byId("DDI_DLG_ReferenceBasic").value;
		        	
	        	searchParms+="&searchType=S";
	        	searchParms+="&InstrumentId="+instId;
	       		searchParms+="&CreditAcct="+caId;
	        	searchParms+="&Reference="+refId;
			}
			
			if(buttonId=="Advanced"){
		    	var instId = dom.byId("DDI_DLG_InstrumentIdAdvance").value,
		    		caId = dijit.byId("DDI_DLG_CreditAcctAdvance").value,
		    		refId = dom.byId("DDI_DLG_ReferenceAdvance").value;
					dateFrom = dijit.byId("DDI_DLG_FromDate").value;
					dateTo = dijit.byId("DDI_DLG_ToDate").value; 
					if (dateFrom)
						dateFrom = locale.format(dateFrom, {selector: "date", datePattern: "MM/dd/yyyy"});
					if (dateTo)
						dateTo = locale.format(dateTo, {selector: "date", datePattern: "MM/dd/yyyy"});
	        		
	        	searchParms+="&searchType=A";
		    	searchParms+="&InstrumentId="+instId;
	       		searchParms+="&CreditAcct="+caId;
	        	searchParms+="&Reference="+refId;
	        	
	        	if (dateFrom && dateFrom.toString() != "NaN/NaN/0NaN")
	        		searchParms+="&fromDate="+dateFrom;
	        	if (dateTo && dateTo.toString() != "NaN/NaN/0NaN")
	        		searchParms+="&toDate="+dateTo;
	        	
		       	
				var amountFrom = dijit.byId("DDI_DLG_AmountFrom").get('value');
		        var amountTo = dijit.byId("DDI_DLG_AmountTo").get('value');

				
				 if (!isNaN(amountFrom))
		        	searchParms+="&fromAmount="+amountFrom;
		        
		        if (!isNaN(amountTo))
		        	searchParms+="&toAmount="+amountTo;
				
	       		

			}

	  		searchDataGrid("existingDirectDebitSearchGridId", "ExistingDirectDebitSearchDataView",searchParms);
    	});	
  	}
  
	function resetDatesDDI_DLG_() {
			dijit.byId('DDI_DLG_FromDate').reset();
			dijit.byId('DDI_DLG_ToDate').reset();
	}
</script>
