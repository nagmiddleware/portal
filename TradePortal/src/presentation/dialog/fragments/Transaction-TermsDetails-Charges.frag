<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
*******************************************************************************
       Transaction Terms Detail Page - Commissions and Charges Section

  Description:     
    This jsp's sole function is to display the Commissions and Charges section 
  which is essentially displaying data in a list view format.  This jsp is 
  called by:  	<%@ include file="Transaction-TermsDetails-Charges.jsp" %>
  There are 2 essential assumptions made for this page to work properly:
  There is a transaction webBean that can be reached to retrieve data from &
  that a resMgr is available.
  Please note to get data - a DatabaseQueryBean is used to get data into a
  DocumentHandler format.  Then by creating a vector from the Fragments of the 
  DocHandler we scroll through the fragments to display the data.  The hard
  part of this is building the sql statement, after that the rest is pretty
  much straight HTML.
*******************************************************************************
--%>

<%
     /***************************************************************************************************
      * Start of Commissions And Charges Section 
      *********************************/  Debug.debug("****** Start of Commissions And Charges Section ******");

  int    xMod;
%>



<table class="formDocumentsTable" >


    <%
     /****************************
      * Building list data (loop)
      ***************************/		Debug.debug("*** Building List data - loop ***");


  try{
	 query = new StringBuffer();

	 query.append("select charge_type, settlement_curr_code, settlement_curr_amount, ");
	 query.append("settlement_how_type, acct_number ");
	 query.append("from fee ");
	 query.append("where p_transaction_oid = ?");

   	 Debug.debug("Query is : " + query);
	 dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), true, new Object[]{transaction_oid});

	 if(dbQuerydoc != null)
        {
         Debug.debug("**** dbQuerydoc --> " + dbQuerydoc.toString());

 	   listviewVector = dbQuerydoc.getFragments("/ResultSetRecord/");
        }
	 else
	  {
	   listviewVector = new Vector(1);
	  }

	//This loop is designed to scroll through the ResultSetRecords and display the data.
	Debug.debug("Walk the tree to display the text in the listview.");
	
	//Temp Fix : Blank row has been added to resolve issue related to dialog title overlaping with close icon.
 	if(listviewVector != null && listviewVector.size()==0){
	 %>
		<table border=0 width=350>
			<tr><td style='width:15%'>&nbsp;</td><td style='width:15%'>&nbsp;</td><td style='width:15%'>&nbsp;</td><td style='width:15%'>&nbsp;</td><td style='width:40%'>&nbsp;</td></tr>
			<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
		</table>
	 <%
 	}else{
	for( int x=0; x < listviewVector.size(); x++)
	{
 	   Debug.debug("**** listview Vector : index "+ x +" : " + listviewVector.elementAt(x).toString() );
	   xMod = x % 2;
	   myDoc = ((DocumentHandler)listviewVector.elementAt(x));

 
%>	     
	 <tr>
	     <td>

<%
	   	  attribute   = myDoc.getAttribute("/CHARGE_TYPE");
	   	  try {
 	   	  	displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CHARGE_TYPE,
						                  	      attribute);
          }
	   	  // W Zhu 3/10/08 NCUH121502535 begin 
	   	  // Refresh ReferenceDataManager if charge type is already in the db but not in the cache.
	   	  // This could happen if a new charge type is sent from OTL.  Usually the the unpackager would refresh the cache but
	   	  // that does not work if the agent is on a different weblogic server.
          catch (AmsException e) {
       	      displayText = attribute; // Display charge_type code value instead of blank.
	          int refdataCount = DatabaseQueryBean.getCount("code", "refdata", "table_type = ? and locale_name is null and code = ? ",false, new Object[]{"CHARGE_TYPE",attribute});
              if (refdataCount > 0 ) {
	              ReferenceDataManager.getRefDataMgr().refresh();
			   	  try {
 	   				  	displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.CHARGE_TYPE,
						                  	      attribute);
	        	  }
	       		  catch (AmsException e2) {
       			  }
       		  }
          }
	   	  // W Zhu 3/10/08 NCUH121502535 end
        	   


%>		  <%= displayText %>
		 </td>

<td>

<%
		  displayText  = myDoc.getAttribute("/SETTLEMENT_CURR_CODE");
		  currencyCode = displayText;
		  
%>		  <%= displayText %>
		
              

<%
		  attribute   = myDoc.getAttribute("/SETTLEMENT_CURR_AMOUNT");
		  displayText = TPCurrencyUtility.getDisplayAmount(attribute, currencyCode, loginLocale);

%>		  <%= displayText %>
</td>

<td nowrap>

<%
		  attribute   = myDoc.getAttribute("/SETTLEMENT_HOW_TYPE");
		  displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.SETTLEMENT_HOW_TYPE,
						                  	      attribute);
%>		  <%=resMgr.getText("TransactionQuickView.Settledvia", 
                               TradePortalConstants.TEXT_BUNDLE)%> <%= displayText %>
		
      	      </td>

<td>

<%
		  displayText = myDoc.getAttribute("/ACCT_NUMBER");
%>		  <%=resMgr.getText("TransactionQuickView.AC", 
                               TradePortalConstants.TEXT_BUNDLE)%> <%= displayText %>
		
              </td>
      	      
    	  </tr>
<%
 	} //End of for loop...
 }//Else block

     }catch(Exception e)
     {
	Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Fee table *******");
     	e.printStackTrace();
     }

%>
 
</table>