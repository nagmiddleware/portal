<%--
 *
 *     Copyright  � 2008                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%--
**********************************************************************************
                        Direct Debit Transaction History Tab

  Description:
    Contains HTML to create the History tab for the Direct Debit Transaction History page

  This is not a standalone JSP.  It MUST be included using the following tag:
  <%@ include file="Transactions-HistoryListView.jsp" %>
*******************************************************************************
--%>
<%
	TermsPartyWebBean payerParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

	String corporateOrgOid = userSession.getOwnerOrgOid();
	String userCheck;
	boolean isBankUser = true;
	String payerAcctList="";
	String  acctOid ="";
	Vector availabilityChecks = new Vector(2);
	
	availabilityChecks.add("!deactivate_indicator");
	availabilityChecks.add("available_for_direct_debit");
	
	QueryListView queryTemplateListView = (QueryListView) EJBObjectFactory.createClientEJB(formMgr.getServerLocation(), "QueryListView");
	
	String sqlString = "select name from client_bank, users where user_oid = "
                                                                + userSession.getUserOid()
                                                                + " and p_owner_org_oid = organization_oid";

	queryTemplateListView.setSQL(sqlString);
	queryTemplateListView.getRecords();
	
	if (queryTemplateListView.getRecordCount() == 0){
		isBankUser = false;
	}
	
	if (userSession.hasSavedUserSession()){
		isBankUser = true;
	}

	if(isBankUser){
    	userCheck = null;
    }else{
    	userCheck = userSession.getUserOid();
    }

	payerParty.loadAcctChoices(corporateOrgOid, formMgr.getServerLocation(), resMgr, availabilityChecks, userCheck);
    payerAcctList = StringFunction.xssHtmlToChars(payerParty.getAttribute("acct_choices"));

    DocumentHandler acctOptions = new DocumentHandler(payerAcctList, true);
    String options = Dropdown.createSortedAcctOptionsOid(acctOptions, acctOid, loginLocale);
    options = "<option value=\"\"></option>"+options;
%>
<%


   StringBuffer statusExtraTags = new StringBuffer();

   // Upon changing the status selection, automatically go back to the Transactions Home
   // page with the selected status type.  Force a new search to occur (causes cached
   // "where" clause to be reset.)

   statusExtraTags.append("onchange=\"location='");
   statusExtraTags.append(formMgr.getLinkAsUrl("goToDirectDebitsSearch", response));
   statusExtraTags.append("&amp;current2ndNav=");
   statusExtraTags.append(current2ndNav);
   statusExtraTags.append("&NewDropdownSearch=Y");
   statusExtraTags.append("&amp;instrStatusType='+this.options[this.selectedIndex].value\"");
%>

	<div class="gridSearch">	                                       
	
	<%
      	// When the Instrument History tab is selected, display 
      	// Organizations dropdown or the single organization.
      	if ((SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_WORK)) && (totalOrganizations > 1)){
      	 	orgListDropDown = true;
          	dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME",  selectedOrg, userSession.getSecretKey()));
          	dropdownOptions.append("<option value=\"");
          	dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_ORGANIZATIONS, userSession.getSecretKey()));
          	dropdownOptions.append("\"");

          	if (selectedOrg.equals(ALL_ORGANIZATIONS)) {
             	dropdownOptions.append(" selected");
          	}

          	dropdownOptions.append(">");
          	dropdownOptions.append(ALL_ORGANIZATIONS);
          	dropdownOptions.append("</option>");

         	extraTags = new StringBuffer("");
          	extraTags.append("onchange=\"location='");
          	extraTags.append(formMgr.getLinkAsUrl("goToDirectDebitsSearch", response));
          	extraTags.append("&amp;current2ndNav=");
          	extraTags.append(current2ndNav);
          	extraTags.append("&NewDropdownSearch=Y");
          	extraTags.append("&historyOrg='+this.options[this.selectedIndex].value\"");
    %>
		<%=widgetFactory.createSearchSelectField("DDI_DLG_org", "ExistingDirectDebitSearch.Show", "", dropdownOptions.toString(), "onChange='searchDirectDebitHistoryDDI_DLG_()'")%>
	<%	} %>	
		<div class="searchItem">
		<label><%=resMgr.getText("ExistingDirectDebitSearch.Status", TradePortalConstants.TEXT_BUNDLE)%></label>
		<%= widgetFactory.createCheckboxField("DDI_DLG_Active", "common.StatusActive", true, false, false,"onClick='searchDirectDebitHistoryDDI_DLG_();'","","none") %>
        <%= widgetFactory.createCheckboxField("DDI_DLG_InActive", "common.StatusInactive", false, false,false,"onClick='searchDirectDebitHistoryDDI_DLG_();'","","none") %>
		</div>
		<div style="clear:both"></div>
	</div>
     <div class="subHeaderDivider"></div>  
		<%@ include file="/dialog/fragments/ExistingDirectDebitSearch-BasicFilter.frag"%>
		<%@ include file="/dialog/fragments/ExistingDirectDebitSearch-AdvanceFilter.frag"%>
  		
		<%
			  gridHtml = dgFac.createDataGrid("existingDirectDebitSearchGridId","ExistingDirectDebitSearchDataGrid",null);
		%> 

		<%= gridHtml%>
		<input type=hidden name=SearchType value=<%=searchType%>>