<%--
*******************************************************************************
                    Cash Management Transaction Search Basic Filter include file

  Description:
    This is a JSP meant to be included using the <%@ include file="x.jsp" %> 
  tag.  It creates the HTML for displaying the Basic Filter fields for the  
  Cash Management Instrument Search.  As it is included with <%@ include %> rather than with
  the <jsp:include> directive, it is not a standalone servlet.  

  NOTE: Since this JSP is copied into the including page at compile time of
  the including page, changes to this page do not automatically force the
  including pages to be recompiled.  If you make changes to this page, you must
  delete the including page's servlet in order to see the changes.

*******************************************************************************
--%>

	<input type="hidden" name="NewSearch" value="Y">

	<div id="basicPaymentFilter" style="padding-top: 5px;"> 
	 	<div class="searchCriteria">

			<%=widgetFactory.createSearchTextField("pmtD_InstrumentId","CashMgmtTransactionSearch.TransactionID","20", "class='char15' onKeydown='filterPaymentsOnEnter(\"pmtD_InstrumentId\", \"Basic\");'")%>
		
		  <% 
	        	options = Dropdown.getInstrumentList(instrumentType, loginLocale, instrumentTypes );
	        %>

			<%=widgetFactory.createSearchSelectField("pmtD_InstrumentTypeBasic","CashMgmtTransactionSearch.TransactionType"," ", options, "onKeydown='Javascript: filterPaymentsOnEnter(\"pmtD_InstrumentTypeBasic\", \"Basic\");'")%>
		
			
			<%=widgetFactory.createSearchTextField("pmtD_RefNum","CashMgmtTransactionSearch.PrimaryRefNum","30", "class='char15' onKeydown='filterPaymentsOnEnter(\"pmtD_RefNum\", \"Basic\");'")%>

	      	    </div>
	    	
    	<div class="title-right" style="text-align: center">
	    	<button data-dojo-type="dijit.form.Button" id="pmtD_searchButtonBasic" type="button">
	            <%=resMgr.getText("common.SearchGreyText", TradePortalConstants.TEXT_BUNDLE)%>
	         		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	                  filterPayments("Basic");return false;
	      		</script>
	        </button>
			<%=widgetFactory.createHoverHelp("pmtD_searchButtonBasic","SearchHoverText") %>
			<br>
			<span id="pmtD_searchLinkAdv" ><a href="javascript:shuffleFilter('Advance');"><%=resMgr.getText("ExistingDirectDebitSearch.Advanced", TradePortalConstants.TEXT_BUNDLE)%></a></span>
			<%=widgetFactory.createHoverHelp("pmtD_searchLinkAdv","common.AdvancedSearchHypertextLink") %>
		</div>
		<div style="clear: both;"></div>
	</div>