<%    /*****************************************************
      * Action Buttons For Mail Message Detail page 
      *******************************************************/                 
%>
	<div class="formItem sidebarButtons">		

<%-- ------------ --%>
<%-- Route Button --%>
<%-- ------------ --%>
<%--		If the Message Status is NOT sent to bank AND either:			--%>
<%--		it's not a discrepancy notice and we have the right to route a message, --%>
<%--		Otherwise it is a discrepancy and the user has the right to route a discrepancy message. --%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<% if( !messageStatus.equals(TradePortalConstants.SENT_TO_BANK) &&
      ((!isDiscrepancy && hasRouteRights ) || (isDiscrepancy && hasRouteDiscrepRights )) ) {
   
	String routeButtonName;

	if(isReadOnly){ 	//Just route to the next page: Route/RouteItems
		routeButtonName = "Route";

	}else{			//Route to: RouteAndSave/RouteItems
		routeButtonName = "RouteAndSave";
	}  
%>
		<button data-dojo-type="dijit.form.Button"  name=<%=routeButtonName%> id=<%=routeButtonName%> type="button" data-dojo-props="" >
			<%=resMgr.getText("Mail.Route", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				routeMail();
				return false;       					
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp(routeButtonName,"RouteHoverText") %>
<% } %>

<%-- ------------------- --%>
<%-- Send to Bank Button --%>
<%-- ------------------- --%>
<%--		If the user has the security right to send a message and we're Not in read only mode	--%>
<%-- 		then the Send to Bank button is displayed.						--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<% if( messageCanSendToBank && hasSendRights ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="SendToBank" id="SendToBank" type="button" data-dojo-props="" >
			<%=resMgr.getText("common.SendToBankText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				//setButtonPressed('SendToBank', '0');
				//document.forms[0].submit();
			clickButton('SendToBank');
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp("SendToBank","SendtoBankHoverText") %>
<% } %> 

<%-- ----------- --%>
<%-- Save Button --%>
<%-- ----------- --%>
<%--		If this page is Read Only, then we dont need to display the save as Draft button.	--%>
<%--		We alsways call the save function :'Save as Draft' per webpage definition.		--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<% if( !isReadOnly ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="SaveAsDraft" id="SaveAsDraft" type="button" data-dojo-props="" >
			<%=resMgr.getText("common.SaveasDraftAndCloseText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				//setButtonPressed('SaveAsDraft', '0');
				//document.forms[0].submit();
					clickButton('SaveAsDraft');
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("SaveAsDraft","SaveDraftHoverText") %>
<% } %>

<%-- -------------------- --%>
<%-- Reply to Bank Button --%>
<%-- -------------------- --%>
<%--		If the message is a Discrepancy Notice then the has to have a security right to create/	--%>
<%--		modify Discrepancy(s).  If this message is a Mail Message than the user needs the right --%>
<%--		to create / edit Mail Messages.  Either way this has to be a read only page to start.	--%>

<%-- 		It should be noted that Replying to Bank is either done by Submission or by a link...   --%>
<%--		If we are dealing with a discrepancy then the button is a submit button, other wise	--%>
<%--		we are dealing with a link that brings the user back to this jsp in a different mode.	--%>

<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<%    

//Added following if condition not to show ReplyToBank button to the Parent org if the
// child mail message is accessed directly and not through Subsidiary Access
if ((!isUsersOrgOid) && (!userSession.hasSavedUserSession()))
{
	displayDiscrepancyReplyToBankButton = false;
}


	if( isMsgFromBank && isDiscrepancy && hasEditDiscrepRights && displayDiscrepancyReplyToBankButton) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="ReplyToBank" id="ReplyToBank" type="button" data-dojo-props="" >
			<%=resMgr.getText("common.ReplytoBankText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				setButtonPressed('ReplyToBank', '0');
				document.forms[0].submit();       					
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp("ReplyToBank","ReplytoBankHoverText") %>

<%    }else if(isMsgFromBank && !isDiscrepancy && hasEditRights) {

	  urlParm = "&isReply=true" + "&relatedInstID=" + completeInstId + "&subject=" + java.net.URLEncoder.encode(subject) + 
		    "&sequenceNumber=" + sequenceNumber;
%>
    	
    	<button data-dojo-type="dijit.form.Button"  name="ReplyToBank" id="ReplyToBank" type="button" data-dojo-props="" >
			<%=resMgr.getText("common.ReplytoBankText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				openURL("<%=formMgr.getLinkAsUrl( goToMailMessageDetail, urlParm, response )%>");        					
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp("ReplyToBank","ReplytoBankHoverText") %>
<%    }  %>

<%-- ------------------- --%>
<%-- Edit Message Button --%>
<%-- ------------------- --%>
<%-- 		This is a simple Link to bring the user back to this jsp in a different mode.		--%>
<%-- 		The user needs to access this same message, except in edit mode - not readOnly mode.	--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<% if( isReadOnly && !isCurrentUser && hasEditRights && !isMsgFromBank) {

	  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid , userSession.getSecretKey());
	  urlParm = "&isEdit=true" + "&mailMessageOid=" + encryptVal2;
%>
    	<button data-dojo-type="dijit.form.Button"  name="Edit" id=Edit type="button" data-dojo-props="" >
			<%=resMgr.getText("common.EditMessageText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				openURL("<%=formMgr.getLinkAsUrl( goToMailMessageDetail, urlParm, response )%>");        					
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp("Edit","EditHoverText") %>
<% } %>


<%-- ------------- --%>
<%-- Delete Button --%>
<%-- ------------- --%>
<%-- 		This is a submit button that will call the delete Mediator functionality.	--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<% if( (!isMessageOidNull && hasDeleteRights) ||
       (isDiscrepancy && hasDeleteDiscrepRights)) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="Delete" id="Delete" type="button" data-dojo-props="" >
			<%=resMgr.getText("common.DeleteText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				var confirmDelete = confirm("<%=resMgr.getText("Messages.confirmDeleteMessage",TradePortalConstants.TEXT_BUNDLE)%>");
            
				if(confirmDelete==true){                        
                  setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE%>', '0');
                  document.forms[0].submit();
				}
				return false;   					
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp("Delete","DeleteText") %>
<% } %>   

<%-- --------------------- --%>
<%-- Cancel / Close Button --%>
<%-- --------------------- --%>
<%-- 		The buttone text is determined by the cancelFlag variable - otherwise the button always --%>
<%-- 		performs the same action regardless of the text. 					--%>
<%-- 		This is a link that will route the user to how they got to this page in the first place --%>
<%--		by looking at the fromPage variable.							--%>

<%--    	<button data-dojo-type="dijit.form.Button"  name="Close" id="Close" type="button" data-dojo-props="" >--%>
<%--			<%//=resMgr.getText(cancelFlag, TradePortalConstants.TEXT_BUNDLE)%>--%>
<%--			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">--%>
<%--				openURL("<%//=formMgr.getLinkAsUrl( fromPage, response )%>");        					--%>
<%--	 		</script>--%>
<%--		</button> --%>
<% 
	// "Attach a Document" button
	if ((!isReadOnly) &&(SecurityAccess.hasRights(loginRights, SecurityAccess.ATTACH_DOC_MESSAGE))) {
   		session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
%>
    	<button data-dojo-type="dijit.form.Button"  name="AttachDocuments" id="AttachDocuments" type="button" data-dojo-props="" >
			<%=resMgr.getText("common.AttachADocumentText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				setButtonPressed('AttachDocuments', '0');
				document.forms[0].submit();       					
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp("AttachDocuments","AttachDocumentHoverText") %>
<% } %>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<%
	//"Delete a Document" button
	if ((!isReadOnly) && (SecurityAccess.hasRights(loginRights, SecurityAccess.DELETE_DOC_MESSAGE))) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="DeleteADocumentButton" id="DeleteADocumentButton" type="button" data-dojo-props="" >
			<%=resMgr.getText("common.DeleteADocumentText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS%>', '0');
				if(confirmDocumentDelete()){
					document.forms[0].submit();
				}  else{
					return false;
				}  					
	 		</script>
		</button> 
		<%=widgetFactory.createHoverHelp("DeleteADocumentButton","DeleteDocumentHoverText") %>
<% } %>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
	<button data-dojo-type="dijit.form.Button"  name="Cancel" id="Cancel" type="button" data-dojo-props="" >
		<%=resMgr.getText("common.cancel", TradePortalConstants.TEXT_BUNDLE)%>
		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
			closeMMDialog();
		</script>
	</button>
	<%=widgetFactory.createHoverHelp("Cancel","common.Cancel") %>

</div>

<script language="JavaScript">
 
 function openURL(URL){
	 document.location.href  = URL;
	 return false;
 }
 
 function confirmDocumentDelete(){
		<%-- Changes done for PortalRefresh IR - T36000004336 - Sandeep Sikhakolli --%>
		<%-- Changes are required as the existing logic does not work for the dojo checkboxes. --%>
		var returnValue;
		require(["dijit/registry"],
			function(registry){
	    		var confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", TradePortalConstants.TEXT_BUNDLE)%>";
	    		
	    
			    <%-- Find any checked checkbox and flip a flag if one is found --%>
			    var isAtLeastOneDocumentChecked = Boolean.FALSE;      
			    console.log(document.forms[0].AttachedDocument);
			    if (document.forms[0].AttachedDocument != null) {
			       if (document.forms[0].AttachedDocument.length != null) {
			          for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
			        	  var widgetID = document.forms[0].AttachedDocument[i].value;
			        	  if(registry.byId(widgetID).checked){
			             	isAtLeastOneDocumentChecked = true;
			             }
			          }
			       }
			       else if (registry.byId("AttachedDocument1").checked==true) {
			          isAtLeastOneDocumentChecked = true;
			       }
			    }
			    <%-- Setting the values--%>
			    if (isAtLeastOneDocumentChecked==true) {
				       if (confirm(confirmMessage)==true) {
				    	   if (document.forms[0].AttachedDocument != null) {
				    		     if (document.forms[0].AttachedDocument.length != null) {
					                for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
					                	var widgetID = document.forms[0].AttachedDocument[i].value;
					                	  if(registry.byId(widgetID).checked){
							        		  document.forms[0].AttachedDocument[i].value=registry.byId(widgetID).value;
							        	}else{
							        		document.forms[0].AttachedDocument[i].value="";
							        	}
					                }
					             }
					             else if (registry.byId("AttachedDocument1").checked==true) {
					                document.forms[0].AttachedDocument.value = registry.byId("AttachedDocument1").value;
					             }
					          }
				    	   returnValue = true;
			    }else{
			    	returnValue = false;
			    }
		}else {
			alert("<%=resMgr.getText("common.DeleteAttachedDocumentNoneSelected", TradePortalConstants.TEXT_BUNDLE)%>");
			returnValue = false;
		}

		});
		if (returnValue==true) {
			formSubmitted = true;
		    return true;
		}else{
			formSubmitted = false;
		    return false;
		}
	}  
	

 </script>