<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 
 *     All rights reserved
--%>

<%@ page import="com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.common.*,
                 com.ams.tradeportal.html.*,java.util.*,com.amsinc.ecsg.util.*,
				 com.ams.tradeportal.busobj.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
		 		 com.amsinc.ecsg.frame.*,java.math.BigDecimal" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>

<%
  
	Debug.debug("***START********************INVOICE*SEARCH*FOR CREDIT NOTE***** START***");


	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
	String returnAction     = null;
	String onLoad           = "document.FilterForm.bankName.focus();";
	String options = "";
	String amountType = "";
	String loginLocale = StringFunction.xssCharsToHtml(request.getParameter("loginLocale"));
	String dateType = "";
	String userOrgOid = StringFunction.xssCharsToHtml(request.getParameter("userOrgOid"));

	 
	 
	String gridLayout = "";
 	String gridHtml="";
	 
	
	String creditNoteOid =request.getParameter("creditNoteOid");
	creditNoteOid = EncryptDecrypt.decryptStringUsingTripleDes(creditNoteOid,userSession.getSecretKey());
	CreditNotesWebBean crNoteBean  = beanMgr.createBean(CreditNotesWebBean.class, "CreditNotes");
	crNoteBean.getById(creditNoteOid);
 	
	String amount = crNoteBean.getAttribute("amount");
	String appliedAmount = crNoteBean.getAttribute("credit_note_applied_amount");
	
	if (StringFunction.isBlank(amount))
		amount = "0";
	
	if (StringFunction.isBlank(appliedAmount))
		appliedAmount = "0";
	
	String creditNoteSeller= crNoteBean.getAttribute("seller_id");
	String creditNoteCurrency=StringFunction.xssCharsToHtml(crNoteBean.getAttribute("currency"));//CID 11658 RPasupulati
	
	String availableAmount = "0";
	
	if (StringFunction.isNotBlank(amount) && StringFunction.isNotBlank(appliedAmount)){

		BigDecimal crAmount = new BigDecimal(amount);
		BigDecimal crAppliedAmount = new BigDecimal(appliedAmount);
 		availableAmount = ""+ crAmount.subtract(crAppliedAmount);
  	}
	
	if(StringFunction.isNotBlank(appliedAmount)){
		if(appliedAmount.startsWith("-")){
			appliedAmount = appliedAmount.substring(1);
		}else{
			appliedAmount = appliedAmount;
		}
	}else{
		appliedAmount = "0.00";
	}
	
	String ruleOid ="";
	/*String ruleOid =crNoteBean.getMatchingRule();
	ArMatchingRuleWebBean_Base rule = beanMgr.createBean(ArMatchingRuleWebBean_Base.class, "ArMatchingRule");
    rule.setAttribute("ar_matching_rule_oid", ruleOid);
    rule.getDataFromAppServer();
    String tpName = rule.getAttribute("buyer_name");*/
    String tpName = "";
	String sellerName =  crNoteBean.getAttribute("seller_name");
 	
	DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
	gridHtml = dgFactory.createDataGrid("InvoicesForCreditNoteApplyDataGridId","InvoicesForCreditNoteApplyDataGrid",null);
	gridLayout = dgFactory.createGridLayout("InvoicesForCreditNoteApplyDataGrid");
 	Vector  codesToExclude   = new Vector();;
	
	codesToExclude.add("CNA");
	codesToExclude.add("FNA");
	
	String subHeader = resMgr.getText("InvoicesForCreditNoteDialog.AvailableInvoice",TradePortalConstants.TEXT_BUNDLE);
  %>

 
 <div class="dialogContent fullwidth">
  
   		<table cellspacing="1" cellpadding="1" style="width:90%">
			
			<thead>
			<tr  align="left">
				<th align="left">
					<%=resMgr.getText("InvoicesForCreditNoteDialog.Supplier", TradePortalConstants.TEXT_BUNDLE)%><%=sellerName %>
				</th>
				<th align="left">
					<%=resMgr.getText("InvoicesForCreditNoteDialog.Amount", TradePortalConstants.TEXT_BUNDLE) %>
				</th>
				<th align="left">
					<%=resMgr.getText("InvoicesForCreditNoteDialog.AppliedAmount", TradePortalConstants.TEXT_BUNDLE) %>
				</th>
				 <th align="left">
					<%=resMgr.getText("InvoicesForCreditNoteDialog.AvailableAmount", TradePortalConstants.TEXT_BUNDLE) %>
				 </th>
			
			</tr> 
			</thead>
          	 <tr align="left">
				<td align="left"><b><%=resMgr.getText("CreditNoteSearch.CreditNoteId", TradePortalConstants.TEXT_BUNDLE)%><%=crNoteBean.getAttribute("invoice_id") %></b></td>
				<td align="left"><b><%=TPCurrencyUtility.getDisplayAmountWithCurrency(amount,creditNoteCurrency, loginLocale) %></b></td>
				<td align="left"><b><%=TPCurrencyUtility.getDisplayAmountWithCurrency(appliedAmount,creditNoteCurrency, loginLocale) %></b></td>
				<td align="left"><b><%=TPCurrencyUtility.getDisplayAmountWithCurrency(availableAmount,creditNoteCurrency, loginLocale)%></b></td>
			</tr>
 		</table>
  
 	<div style="clear:both;"></div>
	
	<div class="dialogContent fullwidth">
 	
	<div class="searchHeader">
 		   
   		<%=widgetFactory.createWideSubsectionHeader(subHeader) %>  
  	 <div style="clear:both;"></div>
 	 
 	<div class="searchDetail">
	
	<table cellspacing="0" cellpadding="0" style="width:90%" border="0">
		<tr>
				<td>
 			   		<%=widgetFactory.createSearchTextField("InvoiceID", "InvoiceSearch.InvoiceID", "35", "class='char10'") %>

			  	</td>
				<td>  	
 			  		<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.AMOUNT_TYPE, amountType, loginLocale,codesToExclude);
			  		out.println(widgetFactory.createSearchSelectField("amountType", "InvoiceSearch.AmountType", " ", options,"'class='char10'"));
 		  			%>
				</td>
				<td>  
		   			<%=widgetFactory.createSearchAmountField("amountFrom", "InvoiceSearch.From", "", "class='char10'","") %>
		  		
		  		</td>
		  		<td>  
		  			<%=widgetFactory.createSearchAmountField("amountTo", "InvoiceSearch.To", "", "class='char10'","") %>
			   </td>
			   
			    <td align ="right>
 					<span class="searchActions">
		  			<button data-dojo-type="dijit.form.Button" type="button" id="searchParty">Search
						<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">searchMatchInvoices();</script>
				  	</button>
				  	</span>
		
				  	<%=widgetFactory.createHoverHelp("searchParty", "SearchHoverText") %>
				  	 <div style="clear:both;"></div>
				  	</div>
				 </td>
	  	</tr>
	  	
	  	<tr>
				<td></td>

				<td>	  
		  		
		  			<%options = Dropdown.createSortedRefDataOptions(TradePortalConstants.DATE_TYPE, dateType, loginLocale);
		  			out.println(widgetFactory.createSearchSelectField("dateType", "InvoiceSearch.DateType", " ", options, "class='char10'"));
		  			%>
 	  			</td>

	  	    	<td>
		  	    	<%=widgetFactory.createSearchDateField("dateFrom","InvoiceSearch.From","class='char10'","") %>
	  	    	</td>

				<td>
					<%=widgetFactory.createSearchDateField("dateTo","InvoiceSearch.To","class='char10'","") %>
				</td>
	  		</span>

	  		</tr>
  		</table>  		 
  	<div style="clear:both;"></div>
 	</div>
	
	

    <%=gridHtml%>  
    </div>
</div>







<script type="text/javascript">



function filterInvoiceOnEnter(fieldID){
	
	require(["dojo/on","dijit/registry"],function(on, registry) {
	    on(registry.byId(fieldID), "keypress", function(event) {
	        if (event && event.keyCode == 13) {
	        	dojo.stopEvent(event);
	        	searchMatchInvoices();
	        }
	    });
	});	<%-- end require --%>
}<%-- end of filterInvoiceOnEnter() --%>


var initSearchParms="";
var loginLocale="";
var gridLayout = <%=gridLayout%>;
initSearchParms = "userOrgOid=<%=StringFunction.escapeQuotesforJS(userOrgOid)%>&creditNoteOid=<%=StringFunction.escapeQuotesforJS(creditNoteOid)%>&creditNoteCurrency=<%=StringFunction.escapeQuotesforJS(creditNoteCurrency)%>&creditNoteSeller=<%=StringFunction.escapeQuotesforJS(creditNoteSeller)%>&tradingPartnerName=<%=StringFunction.escapeQuotesforJS(tpName)%>&sellerName=<%=StringFunction.escapeQuotesforJS(sellerName)%>&tpOid=<%=StringFunction.escapeQuotesforJS(ruleOid)%>";

console.log("initSearchParms: " + initSearchParms);

 
require(["t360/datagrid"],  function(t360){
	var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("InvoicesForCreditNoteApplyDataView",userSession.getSecretKey())%>";  
    var myGrid = t360.createDataGrid("InvoicesForCreditNoteApplyDataGridId", viewName,gridLayout,initSearchParms);
    myGrid.set('autoHeight',10); 
});



<%--provide a search function that collects the necessary search parameters--%>
function searchMatchInvoices() {
	
 
  
  require(["dojo/dom"],

 		  function(dom){
  	
  	var invoiceID=(dom.byId("InvoiceID").value).toUpperCase();
   	
  	
  	var amountType=(dijit.byId("amountType").value).toUpperCase();
  	var amountFrom=(dom.byId("amountFrom").value).toUpperCase();
  	var amountTo=(dom.byId("amountTo").value).toUpperCase();
  	
  	var dateType=(dijit.byId("dateType").value).toUpperCase();
  	var fromDate=(dom.byId("dateFrom").value).toUpperCase();
  	var toDate=(dom.byId("dateTo").value).toUpperCase();
  	
     
      <%-- var filterText=filterTextCS.toUpperCase(); --%>
	var searchParms = initSearchParms+"&invoiceID="+invoiceID+"&amountType="+amountType+"&amountFrom="+amountFrom+"&amountTo="+amountTo+
	"&dateType="+dateType+"&fromDate="+fromDate+"&toDate="+toDate;
	 
      
     
	console.log("SearchParms: " + searchParms);
    searchDataGrid("InvoicesForCreditNoteApplyDataGridId", "InvoicesForCreditNoteApplyDataView",
                     searchParms);
    });
	}



  function chooseInvoices () {
	 
	 require(["dojo/ready","dijit/registry","dojo/_base/array","t360/dialog"], 

			function(ready,registry,baseArray,dialog) {
			console.log('choose invoices');
			
			ready(function(){
				
				var rowKeys = getSelectedGridRowKeys("InvoicesForCreditNoteApplyDataGridId");	
				if(rowKeys.length >=1){
					dialog.doCallback('creditNoteApplySearchDialog','select', rowKeys);
					closePopup();
				} else {
					alert('Select Invoice(s) to be manually matched');
					return false;
				}
				
			});
			});
	 
  }

 	  
  function closePopup() {
	  require(["t360/dialog"], function(dialog) {
		  dialog.hide('creditNoteApplySearchDialog');
	  });
	  
  }
  
  var dialogName=dijit.byId("creditNoteApplySearchDialog");
	var dialogCloseButton=dialogName.closeButtonNode;
	dialogCloseButton.setAttribute("id","creditNoteApplySearchDialogCloseButton");
	
 	
</script>

<%=widgetFactory.createHoverHelp("creditNoteApplySearchDialogCloseButton", "common.Cancel") %>

</body>
</html>
