

<%--
 * Phrasedetails dialog to get new phrase name
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.web.*, com.amsinc.ecsg.util.*, com.amsinc.ecsg.frame.*, com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.common.*, com.ams.tradeportal.common.cache.*, com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session"></jsp:useBean>



<% WidgetFactory widgetFactory = new WidgetFactory(resMgr); %>


<div class="dialogContent padded">
  <form name="saveAsForm"> 
  
  
  <%= widgetFactory.createTextField( "newProfileName", "NotificationRuleDetail.RuleName", "", "35", false, true, false,  "", "","" ) %>                          
 	   <div> 
 	   <table><tr> <td>
 	   <div class="formItem">
 	   <button data-dojo-type="dijit.form.Button" type="button" id="saveAs" >
        		<%=resMgr.getText("SecurityProfileDetail.SaveAsTitle", TradePortalConstants.TEXT_BUNDLE)%>
        		<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
	        		submitSaveAs();
	      		</script>
       </button>
     		</div>
     		</td>
 	   <td>
 	    <div class="formItem">
 	   <button data-dojo-type="dijit.form.Button" type="button" id="CloseButtonSaveAs" onClick="hideDialog('saveAsDialogId')"  ><%= resMgr.getText("common.cancel", 
                             TradePortalConstants.TEXT_BUNDLE) %>
                         
                             </button>
          <%= widgetFactory.createHoverHelp("saveAs", "SaveAsHoverText")%>
 	  <%= widgetFactory.createHoverHelp("CloseButtonSaveAs", "CloseHoverText")%>

 	   </div>
 	   </td></tr> </table>
 	
        </div>
  </form> 
  
</div>
           
