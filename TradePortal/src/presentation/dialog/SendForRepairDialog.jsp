<%--
*******************************************************************************
                           Repair Reason Dialog Page for Transaction

  Description:  this page is used to send repair reason to all user for correction
                required in transaction.
                this page is also used to send mail to users to make correction.
*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page
	import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.html.*,com.ams.util.*"%>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager"
	scope="session">
</jsp:useBean>

<jsp:useBean id="userSession"
	class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
	scope="session">
</jsp:useBean>

<%
    WidgetFactory widgetFactory = new WidgetFactory(resMgr);
    DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr, response);
    String gridLayout = null;
	String gridHtml = null;	
%>

<%-- ********************* HTML for page begins here *********************  --%>

<div class="POPopUpContent">

<input type=hidden name="buttonName" value="">  
<%= widgetFactory.createTextArea("repairReasonText", "CheckFlowTransaction.RepairReason", "", false, true, false, "style='width:auto;' rows='10' cols='100'", "","", "1000" ) %>
<div style="clear:both;"></div>
<%= widgetFactory.createLabel("SendEmailNotification", "CheckFlowTransaction.SendEmailNotification", false, false, false, "none")%>
<%--IR T36000020338 Rel 8.3 DataGrid does not get displayed in IE7. Hence styles are being applied.--%>
<div class="sendForRepairGrid" >
<%
gridLayout = dgFactory.createGridLayout("repairTransactionUsersDataGridId", "RepairTransactionUsersDataGrid");
gridHtml = dgFactory.createDataGrid("repairTransactionUsersDataGridId","RepairTransactionUsersDataGrid",null);
%>
<%=gridHtml%>
</div>
</div>



    
 <script type='text/javascript'>
		var gridLayout = <%= gridLayout %>;								
		var initSearchParms= "";
		
		<%-- Rel 8.3 jgadela 07/09/2013  - IR T36000018959 moved creatDataGird mehtod to inside dojo ready --%>
		require(["t360/datagrid", "dojo/domReady!"],
			      function( t360grid ) {
				  var gridLayout = <%=gridLayout%>;   
				  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("RepairTransactionUsersDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
				  createDataGrid("repairTransactionUsersDataGridId", viewName,gridLayout, initSearchParms);
				  var myGrid = dijit.byId("repairTransactionUsersDataGridId");
					if(myGrid){
			    		myGrid.set('autoHeight',10);
					}
			  });
		
		function SendForRepair(){
		  require(["t360/common"], function(common) {
			var rowKeys = getSelectedGridRowKeys("repairTransactionUsersDataGridId"),
			buttonName = 'SendForRepair',
			formName = document.forms[0].name;
			var repairReasonText = dijit.byId("repairReasonText").value;
			<%-- Rel 8.3 IR#T36000021490 - Length validation -START --%>
			var repairReasonTextTemp = repairReasonText.replace(/\n/g, "\n\r");
			if(repairReasonTextTemp.length>1000){
				alert("<%=StringFunction.escapeQuotesforJS(resMgr.getText("EmailTrigger.RepairReasonValidationMessage", TradePortalConstants.TEXT_BUNDLE))%>");
				return;
			}
			<%-- Rel 8.3 IR#T36000021490 - Length validation  END --%>
			var myForm = document.getElementsByName(formName)[0];
			common.addParmToForm(myForm, 'repairReason', repairReasonText);
			common.submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
			
		  });
	
		}
		
		function CancelSendForRepair(){
			require(["dijit/registry"],
					 function(registry) {																		
						hideDialog("SendForRepairDialog");
			});

		}
	
	</script>   
	<%=widgetFactory.createHoverHelp("AddStructurePODialogCloseButton", "common.Cancel") %>	
</body>
</html>

<%
formMgr.storeInDocCache("default.doc", new DocumentHandler());
%>
