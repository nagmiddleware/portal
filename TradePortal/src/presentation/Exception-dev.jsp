<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="java.util.*,com.amsinc.ecsg.web.*,com.amsinc.ecsg.frame.*, com.ams.tradeportal.common.*, com.amsinc.ecsg.util.*" %>
<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>
<jsp:useBean id="resMgr"  class="com.amsinc.ecsg.util.ResourceManager" scope="session"></jsp:useBean>


<jsp:include page="/common/Header.jsp">
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_NO%>" />
  <jsp:param name="includeTimeoutForward" value="false" />
  <jsp:param name="checkChangePassword" value="<%= TradePortalConstants.INDICATOR_NO %>" />
</jsp:include>

<META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>


 <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <td width="20" class="ColorGrey" nowrap height="25"> <img src="/portal/images/Blank_15.gif" width="15" height="15">  </td>
      <td class="ColorGrey" valign="middle" align="left" nowrap height="25"> 
        <p class="HeadingText"><%= resMgr.getText("ExceptionPage.Heading",TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td class="ColorGrey" colspan="2" height="25" align="right" width="100%">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="60" nowrap>&nbsp;</td>
      <td> 
        <p class="ListText"><%= resMgr.getText("ExceptionPage.SystemEncounteredError", TradePortalConstants.TEXT_BUNDLE) %></p>
        <p class="ListText"><%= resMgr.getText("ExceptionPage.Click", TradePortalConstants.TEXT_BUNDLE) %>
	  <%= formMgr.getLinkAsHref("goToTradePortalHome", resMgr.getText("ExceptionPage.Here", TradePortalConstants.TEXT_BUNDLE), response) %>
        <%= resMgr.getText("ExceptionPage.ToGoToHomePage", TradePortalConstants.TEXT_BUNDLE) %></p>
        <p class="ListText"><%= resMgr.getText("ExceptionPage.PleaseProvide", TradePortalConstants.TEXT_BUNDLE) %></p>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="60" nowrap>&nbsp;</td>
      <td><p class="ListText">
         <%= resMgr.getText("ExceptionPage.Server", TradePortalConstants.TEXT_BUNDLE) %>:
         <%
           try 
	    {
	      PropertyResourceBundle prb = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");
  	      out.print(prb.getString("serverName"));
	    }
	   catch(Exception e)
	    {	
	     out.print("Unknown");
	    }
          %>
</font>
<br>
      </td>
    </tr>
    <tr>
      <td width="60" nowrap>&nbsp;</td>
      <td><p class="ListText">
         <%= resMgr.getText("ExceptionPage.ExceptionType", TradePortalConstants.TEXT_BUNDLE) %>
         <%= request.getAttribute("javax.servlet.error.exception_type") %>
      </td>
    </tr>
    <tr>
      <td width="60" nowrap>&nbsp;</td>
      <td><p class="ListText">
         <%= resMgr.getText("ExceptionPage.ExceptionMessage", TradePortalConstants.TEXT_BUNDLE) %>
         <%= request.getAttribute("javax.servlet.error.message") %>
      </td>
    </tr>
    <tr>
      <td width="60" nowrap>&nbsp;</td>
      <td><p class="ListText">
         <%= resMgr.getText("ExceptionPage.StackTrace", TradePortalConstants.TEXT_BUNDLE) %></p>
         <pre>
           <%
            Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
   
            if(throwable != null)
                 throwable.printStackTrace(response.getWriter());
          %>
         </pre>
      </td>
    </tr>
    <tr>
        <td width="60" nowrap>&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="30" class="BankColor">
    <tr> 
      <td width="100%">&nbsp;</td>
      <td>&nbsp;</td>
      <td width="20" nowrap>&nbsp;</td>
    </tr>
  </table>
