<%--cquinton 9/20/2012 ir#4556 after moving all pre-header html to footer
    this extra doctype is not required
<!DOCTYPE HTML>
--%>
<%--
*******************************************************************************
                               Messages Home Page

  Description:  The Messages Home page is used for the messages subsystem.  It
                displays the mail and notifications pages.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.dataview.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%--cquinton 9/20/2012 ir#4556 move Header.jsp here commonly so html page bottom is accurately within body tag--%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<%--cquinton 9/20/2012 ir#4556 move javascript to page bottom to avoid flash of page with no styling--%>

<%
Hashtable routeMessagesSecParms = new Hashtable();
routeMessagesSecParms.put("UserOid", userSession.getUserOid());
routeMessagesSecParms.put("SecurityRights", userSession.getSecurityRights());
%>

<%--cquinton 9/20/2012 ir#4556 move route message form to page bottom to avoid flash of page with no styling--%>

<%-- ************** Data retrieval page setup begins here ****************  --%>
<%-- Vshah - 08/02/2012 - Rel8.1 - Portal-refresh --%>
<%--  including TransactionCleanup.jsp to remove lock on instrument which has been placed by this user... --%>
<%--  this is required because (with Portal-refresh) user can navigate anywhere from MenuBar when on Instrument(detail) page. --%>
<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>


<%
	WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   DocumentHandler   xmlDoc             = null;
   Hashtable         secureParms        = new Hashtable();
   String            current2ndNav      = null;
   String            tab1Value          = null;
   String            tab2Value          = null;
   String            userOid            = null;
   StringBuffer      dropdownOptions    = new StringBuffer();
   StringBuffer      extraTags          = new StringBuffer();
   String            defaultValue       = null;
   String            defaultText        = null;
   DocumentHandler   hierarchyDoc       = null;
   int               totalOrganizations = 0;

   String            userOrgOid         = null;
   String            userOrgName        = null;
   String            userSecurityRights = null;
   String            userSecurityType   = null;
   
   String            currentFolder                    = null;
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   Map savedSearchQueryData =null;

   // Set the current primary navigation
   userSession.setCurrentPrimaryNavigation("NavigationBar.Messages");
   String  loginLocale    = userSession.getUserLocale();
   ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

   // Determine secondary navigation to display to the user based on either a
   // passed in parameter, what's in the session, or Mail as the default.
   current2ndNav = request.getParameter("current2ndNav");
   if (current2ndNav == null)
   {
      current2ndNav = (String) session.getAttribute("messages2ndNav");

      if (current2ndNav == null)
      {
         current2ndNav = TradePortalConstants.NAV__MAIL;
      }
   }
   //IR 20876 -start
   if (current2ndNav.equals(TradePortalConstants.NAV__MAIL))
   {
      currentFolder = request.getParameter("currentFolder");

      if (currentFolder == null || "".equals(currentFolder.trim()))
      {
         currentFolder = (String) session.getAttribute("currentFolder");

         if (currentFolder == null || "".equals(currentFolder.trim()))
         {
            currentFolder = TradePortalConstants.INBOX_FOLDER;
         }
      }
   }
   String searchNav = "msgHome."+current2ndNav+currentFolder; 
   //IR 20876 -end
%>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<%
  //cquinton 1/18/2013 remove old close action behavior

  //cquinton 1/18/2012 set return page
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
  }
  else {
    userSession.addPage("goToMessagesHome");
  }

  //IR T36000026794 Rel9.0 07/24/2014 starts
  session.setAttribute("startHomePage", "MessagesHome");
  session.setAttribute("fromPage", "MessagesHome");//IR 31548
  session.removeAttribute("fromTPHomePage");
  //IR T36000026794 Rel9.0 07/24/2014 End
   // Retrieve the user's security rights, security type, organization oid, and user oid
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgName        = userSession.getOrganizationName();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   
   com.ams.tradeportal.common.cache.Cache reCertCache = (com.ams.tradeportal.common.cache.Cache)com.ams.tradeportal.common.cache.TPCacheManager.getInstance().getCache(TradePortalConstants.CLIENT_BANK_CACHE);
   DocumentHandler CBCResult = (DocumentHandler)reCertCache.get(userSession.getClientBankOid());		
   String isDisplayPreDebitNotify = CBCResult.getAttribute("/ResultSetRecord(0)/PRE_DEBIT_FUNDING_OPTION_IND");
   
   CorporateOrganizationWebBean org1  = null;
   String preDebitEnabledAtCorpLevel = TradePortalConstants.INDICATOR_NO;
   if(TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)) {
      org1 = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
     org1.getById(userOrgOid);
     preDebitEnabledAtCorpLevel = org1.getAttribute("pre_debit_funding_option_ind");
   }
   
   //Vshah - 10/07/2010 - KGUK100633061 - Begin
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();

   String confInd = "";
   if (userSession.getSavedUserSession() == null)
     confInd = StringFunction.xssCharsToHtml(thisUser.getAttribute("confidential_indicator"));
   else
   {
     if (TradePortalConstants.ADMIN.equals(userSession.getSavedUserSession().getSecurityType()))
    	confInd = TradePortalConstants.INDICATOR_YES;
     else
       	confInd = StringFunction.xssCharsToHtml(thisUser.getAttribute("subsid_confidential_indicator"));
   }
   //Vshah - 10/07/2010 - KGUK100633061 - End


   // This is the query used for populating the option drop down list;
   // it retrieves all active child organizations that belong to the user's
   // org in addition to the user's org itself.  It is used by both the mail
   // and notification.
   //jgadela R90 IR T36000026319 - SQL INJECTION FIX
   String sqlQuery= "select organization_oid, name from corporate_org where activation_status = ?  start with organization_oid = ? "
   +" connect by prior organization_oid = p_parent_corp_org_oid order by "+resMgr.localizeOrderBy("name");

   hierarchyDoc = DatabaseQueryBean.getXmlResultSet(sqlQuery, false, TradePortalConstants.ACTIVE, userOrgOid);

   totalOrganizations = hierarchyDoc.getFragments("/ResultSetRecord").size();

    PerformanceStatistic perfStat = (PerformanceStatistic) session.getAttribute("performanceStatistic");



   if (TradePortalConstants.NAV__MAIL.equals(current2ndNav)) {
	   if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	       	savedSearchQueryData = (Map)searchQueryMap.get("MailMessagesDataView");
	       }
%>
     <%@ include file="fragments/MessagesMail.frag" %>
<%
   } else if (TradePortalConstants.NAV__NOTIFICATIONS.equals(current2ndNav)) {
	   if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	       	savedSearchQueryData = (Map)searchQueryMap.get("NotificationsDataView");
	       }
%>
     <%@ include file="fragments/MessagesNotifications.frag" %>
<%
   }else  {
	   if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
	       	savedSearchQueryData = (Map)searchQueryMap.get("DebitFundingMailMsgDataView");
	       }
%>
    <%@ include file="fragments/MessagesDebitFunding.frag" %>
<%  
   }

   if((perfStat != null) && (perfStat.isLinkAction())) {
           perfStat.setContext(current2ndNav);
   }

   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   xmlDoc = formMgr.getFromDocCache();

   xmlDoc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", xmlDoc);

   // Store the current secondary navigation  in the session.
   session.setAttribute("messages2ndNav",current2ndNav);
   
%>

<form method="post" name="RouteMessagesForm" action="<%=formMgr.getSubmitAction(response)%>">
<input type="hidden" name="RouteUserOid" />
<input type="hidden" name="RouteCorporateOrgOid" />
<input type="hidden" name="RouteToUser"/>
<input type="hidden" name="RouteToOrg" />
<input type="hidden" name="multipleOrgs" />
<input type="hidden" name="routeAction"  />
<input type="hidden" name="fromListView" />

<%= formMgr.getFormInstanceAsInputField("RouteMessagesForm",routeMessagesSecParms) %>
</form>

<div id="routeDialog" style="display:none;"></div>

<%-- ********************* JavaScript for page begins here *********************  --%>

<script>


  <%--
  // This function is used to display a popup message confirming that the user
  // wishes to delete the selected items in the listview.
  --%>
  function confirmDelete() {
    var   confirmMessage = "<%=resMgr.getText("Mail.PopupMessage",
                                              TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage))
             {
                formSubmitted = false;
                return false;
             } else {
       return true;
    }
  }



</script>

</body>
<script>
<%-- Prateep Gedupudi Adding following code to update the inbox,draft,sent count vial ajax on change of display option start  --%>
<%
if (TradePortalConstants.NAV__MAIL.equals(current2ndNav)) {
%> 
require(["dojo/ready","dijit/registry", "dojo/on","dojo/_base/xhr"], function(ready,registry, on,xhr){
	ready(function(){
		var MailOption=registry.byId("MailOption");
		var currentFolder='<%=currentFolder%>';
		var inboxCountText='<%=resMgr.getText("Mail.Inbox", TradePortalConstants.TEXT_BUNDLE)%>';
		var inboxCountUnreadText='<%=resMgr.getText("Mail.Unread", TradePortalConstants.TEXT_BUNDLE)%>';
		var draftsCountText='<%=resMgr.getText("Mail.Drafts", TradePortalConstants.TEXT_BUNDLE)%>';
		var sentToBankCountText='<%=resMgr.getText("Mail.SentToBank", TradePortalConstants.TEXT_BUNDLE)%>';
		on(MailOption, "change", function(MailOption){
			
			xhr.get({
		          url: "/portal/messages/MessagesCountDetails.jsp?currentMailOption="+MailOption+"&currentFolder="+currentFolder,	                            
		          preventCache: true,
		          handleAs: "json",
		          load: function(result) {
		       	   console.log(result.inboxCount+";"+result.draftsCount+";"+result.sentToBankCount);
		       	   
			       	   if(document.getElementById("inboxCount")){
			       			document.getElementById("inboxCount").innerText=" ("+result.inboxCount+" "+inboxCountUnreadText+")";
			       	   }
			       	   else if(document.getElementById("inbox_change")){
			       			document.getElementById("inbox_change").innerText=inboxCountText+" ("+result.inboxCount+" "+inboxCountUnreadText+")";
			       	   }
			       	   
			       	   if(document.getElementById("draftsCount")){
			       			document.getElementById("draftsCount").innerText=" ("+result.draftsCount+")";  
			       	   }
			       	   else if(document.getElementById("draft_change")){
			       			document.getElementById("draft_change").innerText=draftsCountText+" ("+result.draftsCount+")";  
			       	   }
			       	   
			       	   if(document.getElementById("sentToBankCount")){
			       			document.getElementById("sentToBankCount").innerText=" ("+result.sentToBankCount+")";  
			       	   }
			       	   else if(document.getElementById("sentbank_change")){
			       			document.getElementById("sentbank_change").innerText=sentToBankCountText+" ("+result.sentToBankCount+")";  
			       	   }
		       	    
		          },
			   	  error: function(error){
					console.log(error);	
					<%-- todo Handle the error by using JavaScript Error object --%>
					}	
		      	});
			
			
		},true);
  		 
  	});
});

<%}%>
<%-- Prateep Gedupudi Adding following code to update the inbox,draft,sent count vial ajax on change of display option start  --%>
 
</script>

</html>
