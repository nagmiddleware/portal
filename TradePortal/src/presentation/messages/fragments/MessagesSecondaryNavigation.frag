<%--
*******************************************************************************
  Messages Secondary Navigation

  Description:  Common place to render the secondary navigation for
                all pages associated with MessagesHome.

  Variables:
    current2ndNav - MAIL or NOTIFICATIONS or DEBIT FUNDING MAIL (TradePortalConstants)
        

*******************************************************************************
--%>
<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>

<div class="secondaryNav">

  <div class="secondaryNavTitle">
    <%=resMgr.getText("SecondaryNavigation.Messages", TradePortalConstants.TEXT_BUNDLE)%>
  </div>

  <%
     String mailLinkSelected          = TradePortalConstants.INDICATOR_NO;
     String notificationsLinkSelected = TradePortalConstants.INDICATOR_NO;
     String debitFundMailLinkSelected = TradePortalConstants.INDICATOR_NO;
     String helpLink = "";
     
     if (TradePortalConstants.NAV__MAIL.equals(current2ndNav))
     {
       mailLinkSelected = TradePortalConstants.INDICATOR_YES;
       helpLink = OnlineHelp.createContextSensitiveLink("customer/mail_list_view.htm",resMgr,userSession);
     }
     else if (TradePortalConstants.NAV__NOTIFICATIONS.equals(current2ndNav))
     {
       notificationsLinkSelected = TradePortalConstants.INDICATOR_YES;
       helpLink = OnlineHelp.createContextSensitiveLink("customer/notifications.htm",resMgr,userSession);
     }else{
    	 debitFundMailLinkSelected = TradePortalConstants.INDICATOR_YES;
         helpLink = OnlineHelp.createContextSensitiveLink("customer/predebitnotifications.htm",resMgr,userSession);
     }

     String linkParms = "current2ndNav=" + TradePortalConstants.NAV__MAIL;
   %>
   <jsp:include page="/common/NavigationLink.jsp">
     <jsp:param name="linkTextKey"  value="SecondaryNavigation.Messages.Mail" />
     <jsp:param name="linkSelected" value="<%= mailLinkSelected %>" />
     <jsp:param name="action"       value="goToMessagesHome" />
     <jsp:param name="linkParms"    value="<%= linkParms %>" />
   </jsp:include>
   <%
     linkParms = "current2ndNav=" + TradePortalConstants.NAV__NOTIFICATIONS;
   %>
   <jsp:include page="/common/NavigationLink.jsp">
     <jsp:param name="linkTextKey"  value="SecondaryNavigation.Messages.Notifications" />
     <jsp:param name="linkSelected" value="<%= notificationsLinkSelected %>" />
     <jsp:param name="action"       value="goToMessagesHome" />
     <jsp:param name="linkParms"    value="<%= linkParms %>" />
   </jsp:include>
   <%if(TradePortalConstants.INDICATOR_YES.equals(isDisplayPreDebitNotify)
		   &&  TradePortalConstants.INDICATOR_YES.equals(preDebitEnabledAtCorpLevel)){
     linkParms = "current2ndNav=" + TradePortalConstants.NAV__PRE_DEBIT_FUNDING;
   %>
   <jsp:include page="/common/NavigationLink.jsp">
     <jsp:param name="linkTextKey"  value="SecondaryNavigation.Messages.DebitFunding" />
     <jsp:param name="linkSelected" value="<%= debitFundMailLinkSelected %>" />
     <jsp:param name="action"       value="goToMessagesHome" />
     <jsp:param name="linkParms"    value="<%= linkParms %>" />
   </jsp:include>
	<% }%>
	<div id="secondaryNavHelp" class="secondaryNavHelp">
    	<%=helpLink%>
	</div>
	<%=widgetFactory.createHoverHelp("secondaryNavHelp","HelpImageLinkHoverText") %>
	
  <div style="clear:both;"></div>
  
</div>
