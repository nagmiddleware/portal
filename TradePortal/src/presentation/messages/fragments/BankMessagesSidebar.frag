<%    /*****************************************************
      * Action Buttons For Bank Mail Message Detail page
      *******************************************************/
%>
<%
  boolean showLinks = false;
  boolean showTips = true;
  boolean showTop = false;
  boolean showDeleteAttachDocButton = false;
  //WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  if(request.getParameter("showLinks")!=null)
    if(request.getParameter("showLinks").equals("true"))
      showLinks = true;

  if(request.getParameter("showTips")!=null)
    if(request.getParameter("showTips").equals("false"))
      showTips = false;

  if(request.getParameter("showTop")!=null)
	    if(request.getParameter("showTop").equals("true"))
	      showTop = true;

   StringBuilder query = new StringBuilder();
   
	List<Object> sqlParams = new ArrayList();
   query.append("select doc_image_oid, image_id, doc_name, hash ");
   query.append("from document_image ");
   if ((mailMessageOid != null) && (mailMessageOid.trim().length() > 0)) {
     query.append("where mail_message_oid = ? ");
     sqlParams.add(mailMessageOid);
   }
   else {
     query.append("where mail_message_oid = ? ");
     sqlParams.add(messageOid.toString());
   }
   query.append(" and p_transaction_oid is null");
   query.append(" order by image_id");
   Debug.debug("Query is : " + query);
   DocumentHandler dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false, sqlParams);

			if (dbQuerydoc != null) {
				if (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0) {
					showDeleteAttachDocButton = true;
				}
			}

%>

<%= widgetFactory.createSidebarQuickLinks(showLinks, showTips, showLinks || showTop, userSession) %>
      <ul class="sidebarButtons">

<% if( messageCanSendToCust && hasSendRights ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="SendToCust" id="SendToCust" type="button" data-dojo-props="iconClass:'send'" >
			<%=resMgr.getText("common.SendToCustText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					registry.byId("SendToCust").set('disabled', true);
					common.setButtonPressed('SendToCust', '0');
					document.forms[0].submit();
				});
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("SendToCust","SendtoBankHoverText") %>
<% } %>

<% if( !isReadOnly && !messageStatus.equals(TradePortalConstants.SENT_TO_CUST) ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="SaveAsDraft" id="SaveAsDraft" type="button" data-dojo-props="iconClass:'saveDraftClose'" >
			<%=resMgr.getText("mailMessage.SaveDraft", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					registry.byId("SaveAsDraft").set('disabled', true);
					common.setButtonPressed('SaveAsDraft', '0');
					document.forms[0].submit();
				});
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("SaveAsDraft","SaveDraftHoverText") %>
<% } %>

<%-- -------------------- --%>
<%-- Reply to Customer Button --%>
<%-- -------------------- --%>

<%


	if( isMsgFromCust) {
		  urlParm = "&isReply=true" + "&relatedInstID=" + completeInstId + "&subject=" + java.net.URLEncoder.encode(subject) +"&instrumentOid="+instrumentOid+"&bankInstId="+ bankInstId
				     +"&customerName="+customerName+"&customerOid="+customerOid;
		  		%>

		    	<button data-dojo-type="dijit.form.Button"  name="ReplyToCustk" id="ReplyToCust" type="button" data-dojo-props="iconClass:'reply'" >
					<%=resMgr.getText("common.ReplytoCustText", TradePortalConstants.TEXT_BUNDLE)%>
					<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
						openURL("<%=formMgr.getLinkAsUrl( goToBankMailMessageDetail, urlParm, response )%>");
			 		</script>
				</button>
				<%=widgetFactory.createHoverHelp("ReplyToCust","ReplytoBankHoverText") %>

<%    }%>

<%-- ------------------- --%>
<%-- Edit Message Button --%>
<%-- ------------------- --%>


<% if( !messageStatus.equals(TradePortalConstants.SENT_TO_CUST) && isReadOnly && !isCurrentUser && hasEditRights && !isMsgFromCust) {

	  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid , userSession.getSecretKey());
	  urlParm = "&isEdit=true" + "&mailMessageOid=" + encryptVal2;
%>
    	<button data-dojo-type="dijit.form.Button"  name="Edit" id=Edit type="button" data-dojo-props="iconClass:'edit'" >
			<%=resMgr.getText("common.EditMessageText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				openURL("<%=formMgr.getLinkAsUrl( goToBankMailMessageDetail, urlParm, response )%>");
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("Edit","EditHoverText") %>
<% } %>


<% if( (!isMessageOidNull && hasDeleteRights) ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="Delete" id="Delete" type="button" data-dojo-props="iconClass:'delete'" >
			<%=resMgr.getText("common.DeleteText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				var confirmDelete = confirm("<%=resMgr.getText("Messages.confirmDeleteMessage",TradePortalConstants.TEXT_BUNDLE)%>");
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					
				if(confirmDelete==true){
						registry.byId("Delete").set('disabled', true);
						common.setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE%>', '0');
						document.forms[0].submit();
	            }
				return false;
				});
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("Delete","DeleteHoverText") %>
<% } 

  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString();
  returnParms+="&returning=true";
  String closeLink = formMgr.getLinkAsUrl(returnAction,returnParms,response);
%>
    	<button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" data-dojo-props="iconClass:'close'" >
				<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          openURL("<%= closeLink %>");
        </script>
			</button>
      <%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %>
 
<%
	
	if ((!isReadOnly) &&(SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_ATTACH_DOC) &&  !messageStatus.equals(TradePortalConstants.SENT_TO_CUST))) {
   		session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
%>
    	<button data-dojo-type="dijit.form.Button"  name="AttachDocuments" id="AttachDocuments" type="button" data-dojo-props="iconClass:'attachDocument'" >
			<%=resMgr.getText("common.AttachADocumentText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					registry.byId("AttachDocuments").set('disabled', true);
					common.setButtonPressed('AttachDocuments', '0');
					document.forms[0].submit();
				});
			</script>
		</button>
		<%=widgetFactory.createHoverHelp("AttachDocuments","AttachDocumentHoverText") %>
<% }

	if ((!isReadOnly) && ( SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_DELETE_DOC))) {
	//Added by dillip dt 19-Feb-2013
	if(showDeleteAttachDocButton){
%>
    	<button data-dojo-type="dijit.form.Button"  name="DeleteADocumentButton" id="DeleteADocumentButton" type="button" data-dojo-props="iconClass:'deleteDocument'" >
			<%=resMgr.getText("common.DeleteADocumentText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
						common.setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS%>', '0');
					if(confirmDocumentDelete()){
						document.forms[0].submit();
					}  else{
						return false;
					}
});					
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("DeleteADocumentButton","DeleteDocumentHoverText") %>
<% }} %>
</ul>

<script language="JavaScript">

 function openURL(URL){
    if (isActive =='Y') {
	    if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
        }
    }
	 document.location.href  = URL;
	 return false;
 }

 function confirmDocumentDelete(){
		
		var returnValue;
		require(["dijit/registry"],
			function(registry){
	    		var confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", TradePortalConstants.TEXT_BUNDLE)%>";


			    <%-- Find any checked checkbox and flip a flag if one is found --%>
			    var isAtLeastOneDocumentChecked = false;
			    console.log(document.forms[0].AttachedDocument);
			    if (document.forms[0].AttachedDocument != null) {
			       if (document.forms[0].AttachedDocument.length != null) {
			          for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
			        	  var widgetID = document.forms[0].AttachedDocument[i].value;
			        	  if(registry.byId(widgetID).checked){
			             	isAtLeastOneDocumentChecked = true;
			             }
			          }
			       }
			       else if (registry.byId("AttachedDocument1").checked==true) {
			          isAtLeastOneDocumentChecked = true;
			       }
			    }
			    <%-- Setting the values--%>
			    if (isAtLeastOneDocumentChecked==true) {
				       if (confirm(confirmMessage)==true) {
				    	   if (document.forms[0].AttachedDocument != null) {
				    		     if (document.forms[0].AttachedDocument.length != null) {
					                for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
					                	var widgetID = document.forms[0].AttachedDocument[i].value;
					                	  if(registry.byId(widgetID).checked){
							        		  document.forms[0].AttachedDocument[i].value=registry.byId(widgetID).value;
							        	}else{
							        		document.forms[0].AttachedDocument[i].value="";
							        	}
					                }
					             }
					             else if (registry.byId("AttachedDocument1").checked==true) {
					                document.forms[0].AttachedDocument.value = registry.byId("AttachedDocument1").value;
					             }
					          }
				    	   returnValue = true;
			    }else{
			    	returnValue = false;
			    }
		}else {
			alert("<%=resMgr.getText("BankMail.DeleteDocument.None", TradePortalConstants.TEXT_BUNDLE)%>");
			returnValue = false;
		}

		});
		if (returnValue==true) {
			formSubmitted = true;
		    return true;
		}else{
			formSubmitted = false;
		    return false;
		}
	}
 
 

 </script>