<%--
*******************************************************************************
                              Bank Messages Home Page

  Description:  The Messages Home page is used to display all messages sent by
                the user organization's bank, all messages  by
                Bank users in the organization or related child organizations,
                and all draft messages that the belong to the user, his/her
                organization, or child organization users (assuming the user
                has the appropriate security rights).

*******************************************************************************
--%>
<%--
*
*     Copyright  � 2015
*     CGI, Incorporated
*     All rights reserved
--%>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
// Use the constants from BankMailMessagesDataView.java
 
  StringBuilder      sentToCustFolderTotalWhereClause = new StringBuilder();
  StringBuilder      sentToCustFolderTotal            = null;
  StringBuilder      dynamicWhereClause               = new StringBuilder();
  StringBuilder      draftsFolderTotalWhereClause     = new StringBuilder();
  StringBuilder      draftsFolderTotal                = null;
  StringBuilder      inboxFolderTotalWhereClause      = new StringBuilder();
  StringBuilder      inboxFolderTotal                 = null;
  StringBuilder      tempSql                          = null;
  StringBuilder      newLink                          = new StringBuilder();
  String            currentMailOption                = null;
  String            onLoad                           = null;
  

  String  gridId="BankMailMessagesDataGridId";
//Set tps indicator to yes. 
  userSession.setCustNotIntgTPS(true);  

  int               folderTotal                      = 0;

 
  
    currentMailOption = request.getParameter("currentMailOption");
      if ( StringFunction.isBlank(currentMailOption) )
      {
         currentMailOption = (String) session.getAttribute("currentMailOption");
         
         if ( StringFunction.isBlank(currentMailOption) )
         {
            currentMailOption = EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey());
         }
      }
	  currentMailOption = EncryptDecrypt.decryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey());
	  String ownerShip = userSession.getOwnershipLevel();
	 
	  
	  java.util.List<Object> inBoxSqlParamsLst = new java.util.ArrayList();
	  java.util.List<Object> draftSqlParamsLst = new java.util.ArrayList();
	  java.util.List<Object> sentSqlParamsLst = new java.util.ArrayList();
	  
	 
	  inboxFolderTotalWhereClause.append("a.a_assigned_to_corp_org_oid = b.organization_oid"); 
	  inboxFolderTotalWhereClause.append(" AND b.cust_is_not_intg_tps = ? ");
	  inBoxSqlParamsLst.add(TradePortalConstants.INDICATOR_YES);
	  inboxFolderTotalWhereClause.append(" AND b.a_bank_org_group_oid = c.organization_oid ");
	  if (StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid() )) {
			
			inboxFolderTotalWhereClause.append(" and c.organization_oid not in ( ");
			inboxFolderTotalWhereClause.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
			inBoxSqlParamsLst.add(userSession.getBankGrpRestrictRuleOid());
          inboxFolderTotalWhereClause.append(") ");
   }
	  if ( TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) ) {
		  inboxFolderTotalWhereClause.append(" AND b.a_client_bank_oid = ? ");
		  inBoxSqlParamsLst.add(userSession.getOwnerOrgOid());
      } else {
    	  inboxFolderTotalWhereClause.append( " AND b.a_bank_org_group_oid = ? ");
    	  inBoxSqlParamsLst.add(userSession.getOwnerOrgOid());
    		
      }
	 
	  inboxFolderTotalWhereClause.append("AND a.message_status in (?,?,?) and a.unread_flag = ? ");
	  inBoxSqlParamsLst.add(TradePortalConstants.REC);
	  inBoxSqlParamsLst.add(TradePortalConstants.REC_ASSIGNED);
	  inBoxSqlParamsLst.add(TradePortalConstants.SENT_ASSIGNED);
	  inBoxSqlParamsLst.add( TradePortalConstants.INDICATOR_YES);
      folderTotal = DatabaseQueryBean.getCount("a.bank_mail_message_oid", "bank_mail_message a,corporate_org b,bank_organization_group c", inboxFolderTotalWhereClause.toString(),
                                               false, inBoxSqlParamsLst );          
      inboxFolderTotal = new StringBuilder();
      inboxFolderTotal.append("(");
      inboxFolderTotal.append(folderTotal);
      inboxFolderTotal.append(" ");
      inboxFolderTotal.append(resMgr.getText("Mail.Unread", TradePortalConstants.TEXT_BUNDLE));
      inboxFolderTotal.append(")");
   
      	draftsFolderTotalWhereClause.append("a.a_assigned_to_corp_org_oid = b.organization_oid"); 
      	draftsFolderTotalWhereClause.append(" AND b.cust_is_not_intg_tps = ? ");
      	draftsFolderTotalWhereClause.append(" AND b.a_bank_org_group_oid = c.organization_oid ");
      	
      	draftSqlParamsLst.add(TradePortalConstants.INDICATOR_YES);
      if (StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid() )) {
			draftsFolderTotalWhereClause.append(" and c.organization_oid not in ( ");
			draftsFolderTotalWhereClause.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
			draftSqlParamsLst.add(userSession.getBankGrpRestrictRuleOid());
			draftsFolderTotalWhereClause.append(") ");
      	}
      
	  if ( TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) ) {
		  draftsFolderTotalWhereClause.append(" AND b.a_client_bank_oid = ? ");
		  draftSqlParamsLst.add(userSession.getOwnerOrgOid());
      } else {
    	  draftsFolderTotalWhereClause.append( " AND b.a_bank_org_group_oid = ? ");
    	  draftSqlParamsLst.add(userSession.getOwnerOrgOid());
    		
      }
      draftsFolderTotalWhereClause.append("AND a.message_status = ? "); 
      draftSqlParamsLst.add(TradePortalConstants.DRAFT);
      folderTotal = DatabaseQueryBean.getCount("bank_mail_message_oid", "bank_mail_message a,corporate_org b,bank_organization_group c", draftsFolderTotalWhereClause.toString(),
                                               false, draftSqlParamsLst );
      draftsFolderTotal = new StringBuilder();

      draftsFolderTotal.append("(");
      draftsFolderTotal.append(folderTotal);
      draftsFolderTotal.append(")");
      
      sentToCustFolderTotalWhereClause.append("a.a_assigned_to_corp_org_oid = b.organization_oid"); 
      sentToCustFolderTotalWhereClause.append(" AND b.cust_is_not_intg_tps = ? ");
      sentSqlParamsLst.add(TradePortalConstants.INDICATOR_YES);
      sentToCustFolderTotalWhereClause.append(" AND b.a_bank_org_group_oid = c.organization_oid ");
      if (StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid() )) {
			sentToCustFolderTotalWhereClause.append(" and c.organization_oid not in ( ");
			sentToCustFolderTotalWhereClause.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
			sentSqlParamsLst.add(userSession.getBankGrpRestrictRuleOid());
			sentToCustFolderTotalWhereClause.append(") ");
   }
    
	  if ( TradePortalConstants.OWNER_BANK.equals(userSession.getOwnershipLevel()) ) {
		  sentToCustFolderTotalWhereClause.append(" AND b.a_client_bank_oid = ? ");
		  sentSqlParamsLst.add(userSession.getOwnerOrgOid());
      } else {
    	  sentToCustFolderTotalWhereClause.append( " AND b.a_bank_org_group_oid = ? ");
    	  sentSqlParamsLst.add(userSession.getOwnerOrgOid());
    		
      }
      sentToCustFolderTotalWhereClause.append("AND a.message_status = ? "); 
      sentSqlParamsLst.add(TradePortalConstants.SENT_TO_CUST);
      folderTotal = DatabaseQueryBean.getCount("bank_mail_message_oid", "bank_mail_message a,corporate_org b,bank_organization_group c", sentToCustFolderTotalWhereClause.toString(),
                                               false,sentSqlParamsLst);
      sentToCustFolderTotal = new StringBuilder();
      sentToCustFolderTotal.append("(");
      sentToCustFolderTotal.append(folderTotal);
      sentToCustFolderTotal.append(")");

      onLoad = "document.MessageListForm.MailOption.focus();";

%>


<%-- ********************* HTML for page begins here *********************  --%>

<div class="pageMainNoSidebar">
<div class="pageContent">
<%
String helpUrl = "admin/admin_mail_listview.htm";
String pageTitleKey=resMgr.getText("BankMail.MailMessages",TradePortalConstants.TEXT_BUNDLE);
String helpLink = OnlineHelp.createContextSensitiveLink("customer/mail_list_view.htm",resMgr,userSession);

if (TradePortalConstants.SENT_TO_CUST_FOLDER.equals(currentFolder)){
        helpUrl = "admin/sent_to_customer_listview.htm";
   }else if(TradePortalConstants.DRAFTS_FOLDER.equals(currentFolder)){
        helpUrl = "admin/updatecentre_drafts_listview.htm";
   }
%>

<jsp:include page="/common/PageHeader.jsp">
   <jsp:param name="titleKey" value="<%=pageTitleKey%>" />
   <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
</jsp:include>

<form name="BankMessageListForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
  <jsp:include page="/common/ErrorSection.jsp" />
  <div class="formContentNoSidebar">
    <input type=hidden value="" id =buttonName name=buttonName>

<%
      secureParms.put("SecurityRights", userSecurityRights);
      secureParms.put("UserOid",        userOid);
  %>
    <div class="searchHeader">
      <span class="searchHeaderCriteria">
<%= formMgr.getFormInstanceAsInputField("BankMessageListForm", secureParms) %>
  <%
	  List<Object> sqlParamsDropDown = new ArrayList();
  
		  dropdownOptions.append("SELECT a.organization_oid, a.name");
		  dropdownOptions.append(" FROM corporate_org a,bank_organization_group b"); 
		  dropdownOptions.append(" WHERE a.activation_status = ? ");
		  dropdownOptions.append(" AND a.cust_is_not_intg_tps = ? ");
		  dropdownOptions.append(" AND a.a_bank_org_group_oid = b.organization_oid ");
		  sqlParamsDropDown.add(TradePortalConstants.ACTIVE);
		  sqlParamsDropDown.add(TradePortalConstants.INDICATOR_YES);
		  if (StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid() )) {
				
				dropdownOptions.append(" and b.organization_oid not in ( ");
				dropdownOptions.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
				sqlParamsDropDown.add(userSession.getBankGrpRestrictRuleOid());
              dropdownOptions.append(") ");
				
			}
				if ( TradePortalConstants.OWNER_BOG.equals(ownerShip) ) {
					dropdownOptions.append(" AND a.a_bank_org_group_oid = ? ");
					sqlParamsDropDown.add(userSession.getOwnerOrgOid());
				} else {
					dropdownOptions.append(" AND a.a_client_bank_oid  = ? "); 
					sqlParamsDropDown.add(userSession.getOwnerOrgOid());
						
				}
		dropdownOptions.append(" order by ");
		dropdownOptions.append(resMgr.localizeOrderBy("a.name"));
		
	  	 DocumentHandler bankMsgDropDown =  DatabaseQueryBean.getXmlResultSet(dropdownOptions.toString(),false,sqlParamsDropDown);
	  	 
              String msgdropdownOptions = (Dropdown.getUnsortedOptions(bankMsgDropDown, "NAME", "NAME", "", userSession.getSecretKey()));
              defaultText = resMgr.getText("All", TradePortalConstants.TEXT_BUNDLE);
             
                
              	 extraTags.append("onchange=\"location='");
                 extraTags.append(formMgr.getLinkAsUrl("goToBankMessagesHome", response));
                 extraTags.append("&amp;current2ndNav=" + TradePortalConstants.NAV__MAIL);
                 extraTags.append("&amp;currentFolder=" + currentFolder);
                 extraTags.append("&amp;currentMailOption='+this.options[this.selectedIndex].value\"");
                 String searchlable;
                 if (currentFolder.equals(TradePortalConstants.SENT_TO_CUST_FOLDER)){
                	 searchlable ="BankMail.SentTo";
                	 session.setAttribute("currentFolder", "S");
                 }else if(currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER)){
                	 searchlable ="BankMail.To";
                	 session.setAttribute("currentFolder", "D");
                 }else{
                	 searchlable =  "BankMail.From";
                	 session.setAttribute("currentFolder", "I");
                 }
                 
                 out.print(widgetFactory.createSearchSelectField("SelCustId",searchlable,defaultText,msgdropdownOptions.toString(),
                         "onChange='searchMail(\"Select\");'"));

              %>
<%
                
                
               final String ALL_MAILS                  = "Mail.All";
               final String READ_MAILS                 = "Mail.Read";
               final String UNREAD_MAILS               = "Mail.show.Unread";
               StringBuffer Options= new StringBuffer();
               
               
               Options.append("<option value=\"").append(EncryptDecrypt.encryptStringUsingTripleDes(ALL_MAILS, userSession.getSecretKey())).append("\"");
               Options.append(">");
               Options.append(resMgr.getText(ALL_MAILS, TradePortalConstants.TEXT_BUNDLE));
               Options.append("</option>");
               
               Options.append("<option value=\"").append(EncryptDecrypt.encryptStringUsingTripleDes(READ_MAILS, userSession.getSecretKey())).append("\"");
               Options.append(">");
               Options.append(resMgr.getText(READ_MAILS, TradePortalConstants.TEXT_BUNDLE));
               Options.append("</option>");
    
               Options.append("<option value=\"").append(EncryptDecrypt.encryptStringUsingTripleDes(UNREAD_MAILS, userSession.getSecretKey())).append("\"");
               Options.append(">");
               Options.append(resMgr.getText(UNREAD_MAILS, TradePortalConstants.TEXT_BUNDLE));
               Options.append("</option>");
               
               out.print(widgetFactory.createSearchSelectField("readunreadOption","Mail.ReadUnread","",Options.toString(),
                         "onChange='searchMails(\"Select\");'"));         

  
  
              %>
                      <%
                         if (currentFolder.equals(TradePortalConstants.INBOX_FOLDER))
                         {
                      %>
                            <span class="messagesFolderOpen inbox">
                            <%=resMgr.getText("Mail.Inbox", TradePortalConstants.TEXT_BUNDLE)%>
                            <span id="inboxCount">
                              <%= inboxFolderTotal.toString() %>
                            </span>


                                 </span>
                       <%
                         }
                         else
                         {
                            newLink = new StringBuilder();

                            newLink.append(formMgr.getLinkAsUrl("goToBankMessagesHome", response));
                            newLink.append("&current2ndNav=" + TradePortalConstants.NAV__MAIL);
                            newLink.append("&currentFolder=" + TradePortalConstants.INBOX_FOLDER);
                            newLink.append("&currentMailOption=");
                            newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey()));
                       %>
                            <span class="messagesFolderClosed inbox"><a href="<%= newLink.toString() %>" id="inbox_change" >
                          <%=resMgr.getText("Mail.Inbox", TradePortalConstants.TEXT_BUNDLE)%>
                          <%= inboxFolderTotal.toString() %>
                          </a>
                            </span>

                      <%
                         }
                      if (currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER))
                         {
                          

                      %>
                              <span class="messagesFolderOpen drafts">
                              <%=resMgr.getText("Mail.Drafts", TradePortalConstants.TEXT_BUNDLE)%>

                            <span id="draftsCount">
                              <%= draftsFolderTotal.toString() %>
                            </span></span>
                     <%
                         }
                         else
                         {

                            newLink = new StringBuilder();

                            newLink.append(formMgr.getLinkAsUrl("goToBankMessagesHome", response));
                            newLink.append("&current2ndNav=" + TradePortalConstants.NAV__MAIL);
                            newLink.append("&currentFolder=" + TradePortalConstants.DRAFTS_FOLDER);
                            newLink.append("&currentMailOption=");
                            newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey()));
                           
                           

                      %>
                              <span class="messagesFolderClosed drafts">
                            <a href="<%= newLink.toString() %>" id="draft_change">
                              <%=resMgr.getText("Mail.Drafts", TradePortalConstants.TEXT_BUNDLE)%>
                              <%= draftsFolderTotal.toString() %>
                            </a>
                            </span>
                      <%
                         } 

                         if (currentFolder.equals(TradePortalConstants.SENT_TO_CUST_FOLDER))
                         {
                         
                      %>
                              <span class="messagesFolderOpen sentToBank">
                              <%=resMgr.getText("BankMail.SentToCustomer", TradePortalConstants.TEXT_BUNDLE)%>
                              <span id="sentToBankCount"><%= sentToCustFolderTotal.toString() %></span>
                            </span>
                    <%
                         }
                         else
                         {
                        	 newLink = new StringBuilder();

                             newLink.append(formMgr.getLinkAsUrl("goToBankMessagesHome", response));
                             newLink.append("&current2ndNav=" + TradePortalConstants.NAV__MAIL);
                             newLink.append("&currentFolder=" + TradePortalConstants.SENT_TO_CUST_FOLDER);
                             newLink.append("&currentMailOption=");
                             newLink.append(EncryptDecrypt.encryptStringUsingTripleDes("All", userSession.getSecretKey()));                         
                           
                      %>
                              <span class="messagesFolderClosed sentToBank">
                            <a href="<%= newLink.toString() %>" id="sentcust_change">
                              <%=resMgr.getText("BankMailSend.Customer", TradePortalConstants.TEXT_BUNDLE)%>
                              <%= sentToCustFolderTotal.toString() %>
                            </a>
                            </span>
                      <%
                         }
                      %>



              <%
                 
                 {
                    newLink = new StringBuilder();
                    newLink.append(formMgr.getLinkAsUrl("goToBankMailMessageDetail", response));
              %>
      </span>
      <span class="searchHeaderActions">
        <jsp:include page="/common/gridShowCount.jsp">
          <jsp:param name="gridId" value="<%=gridId%>" />
        </jsp:include>

        <button data-dojo-type="dijit.form.Button"  name="New" id="New" type="button" >
          <%=resMgr.getText("common.New",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
            openURL("<%=newLink.toString()%>");
          </script>
        </button>
        <%=widgetFactory.createHoverHelp("New","NewHoverText") %>
        <span id="messageMailHeaderRefresh" class="searchHeaderRefresh"></span>
        <%=widgetFactory.createHoverHelp("messageMailHeaderRefresh","RefreshImageLinkHoverText") %>
        
        <span id="mailGridEdit" class="gridHeaderEdit"></span>
        <%=widgetFactory.createHoverHelp("mailGridEdit", "CustomizeListImageLinkHoverText") %>
      </span>

<%
  }
%>
      <div style="clear:both"></div>
    </div>

              <%
                 if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.ROUTE_MESSAGE) ||
                     SecurityAccess.hasRights(userSecurityRights, SecurityAccess.ROUTE_DISCREPANCY_MSG)) {
              %>
                    <%-- fromListView indicates that the Route request comes from a listview
                         and fromMessages indicate that the Route request is from Messages and not Transaction --%>
                    <input type="hidden" name="fromListView" value="<%=TradePortalConstants.INDICATOR_YES%>">
                    <input type="hidden" name="fromMessages" value="<%=TradePortalConstants.INDICATOR_YES%>">
              <% }%>

                   			<%
                   			String BankMailMessagesDataGrid;
                   			if (currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER)||currentFolder.equals(TradePortalConstants.SENT_TO_CUST_FOLDER)){
                   				BankMailMessagesDataGrid = "BankSendMailMessageDataGrid";
                   			}else{
                   				BankMailMessagesDataGrid = "BankMailMessagesDataGrid";
                   			}
                   			 		DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
                                    String gridHtml = dgFactory.createDataGrid("BankMailMessagesDataGridId",BankMailMessagesDataGrid,null);
                                    String gridLayout = dgFactory.createGridLayout("BankMailMessagesDataGridId", BankMailMessagesDataGrid);
                              %>
                              <%=gridHtml%>
  </div>
</form>

<%--cquinton 3/16/2012 Portal Refresh - start--%>

</div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="<%=gridId%>" />
</jsp:include>

<%--the route messages dialog--%>
<div id="routeDialog" style="display:none;"></div>

<script type="text/javascript">

  function openURL(URL){
    if (isActive =='Y') {
	    if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
        }
    }
    document.location.href  = URL;
    return false;
  }
  var gridLayout=<%=gridLayout%>;
var currentFolder = savedSearchQueryData["currentFolder"]; 

function initSearch(){
	require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"], 
		      function(dom, registry, t360grid) {
		
	var savedSort = savedSearchQueryData["sort"];  
	
	var currentMailOption = savedSearchQueryData["currentMailOption"]; 
	if("" == savedSort || null == savedSort || "undefined" == savedSort){
	        savedSort = '-DateAndTime';
	 }
	if("" == currentFolder || null == currentFolder || "undefined" == currentFolder)
		currentFolder="<%=StringFunction.escapeQuotesforJS(currentFolder)%>";

	if("" == currentMailOption || null == currentMailOption || "undefined" == currentMailOption){
		
			currentMailOption=registry.byId("SelCustId").get('value');

		}
		else{
			registry.byId("SelCustId").set('value',currentMailOption);
		}
		
	
  var initSearchParms = "currentFolder="+currentFolder+"&currentMailOption="+currentMailOption;
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("BankMailMessagesDataView",userSession.getSecretKey())%>";
 	createDataGrid("BankMailMessagesDataGridId", viewName,gridLayout, initSearchParms,savedSort);
	});
	}
	
  function searchMail(mailType){
    var currentFolder;
    if('<%=StringFunction.escapeQuotesforJS(currentFolder)%>' != "") {
      currentFolder='<%=StringFunction.escapeQuotesforJS(currentFolder)%>';
    }

    if(mailType == "Sent"){
      currentFolder = 'S';
    }
    if(mailType == "Drafts"){
      currentFolder = 'D';
    }
    if(mailType == "Inbox"){
      currentFolder = 'I';
    }

    var searchParms = "currentFolder="+currentFolder;
    currentMailOption = dijit.byId('SelCustId').value;
    generateURL(currentMailOption);
    if(currentMailOption && currentMailOption.length>0) {
      searchParms=searchParms+"&currentMailOption="+currentMailOption;
    }

    console.log("searchParms: "+searchParms);
    searchDataGrid("BankMailMessagesDataGridId", "BankMailMessagesDataView",searchParms);
  }

      function deleteMail(){
           var formName = "BankMessageListForm";
          var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';
            var   confirmMessage = "Are you sure you want to delete the selected item(s)?";
          <%--get array of rowkeys from the grid--%>
          var rowKeys = getSelectedGridRowKeys("BankMailMessagesDataGridId");
          if(rowKeys == ""){
                  alert("You need to select one or more items in order to 'delete'.");
                  return;
            }
         if (!confirm(confirmMessage)){
              return false;
        }else{
     	 <%--submit the form with rowkeys
          becuase rowKeys is an array, the parameter name for
          each will be checkbox + an iteration number -
          i.e. checkbox0, checkbox1, checkbox2, etc --%>
        submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
      }
           
      
           
      }

  function generateURL(currentMailOption){

      var hrefLink = document.getElementById("inbox_change");
      var link = "";
      if(hrefLink!=null){
         link=hrefLink.href;
         }
      var i;

      if (link.indexOf("?") > 0)
      {
        var arrinboxParams = link.split("?");
        var arrinboxURLParams = arrinboxParams[1].split("&");
        var arrinboxParamNames = new Array(arrinboxURLParams.length);
        var arrinboxParamValues = new Array(arrinboxURLParams.length);
        i = 0;
        var encodedinboxSearchParms = "";
       for (i=0;i<arrinboxURLParams.length;i++)
        {
            if (arrinboxURLParams[i] != "")
            {
                var opinboxIndex =  arrinboxURLParams[i].indexOf("=");
                var inboxkey = arrinboxURLParams[i].substring(0,opinboxIndex);
                var inboxvalue = arrinboxURLParams[i].substring(opinboxIndex+1);
                if ("currentMailOption" == inboxkey)
                {
                     inboxvalue = currentMailOption;
                }
              }
            encodedinboxSearchParms = encodedinboxSearchParms + inboxkey + "=" + inboxvalue + "&";
         }
         document.getElementById("inbox_change").href=arrinboxParams[0].concat("?"+encodedinboxSearchParms);
        }



       link ="";
      hrefLink = document.getElementById("draft_change");
            if(hrefLink!=null){
         link=hrefLink.href;
         }

      if (link.indexOf("?") > 0)
      {
        var arrdraftParams = link.split("?");
        var arrdraftURLParams = arrdraftParams[1].split("&");
        var arrdraftParamNames = new Array(arrdraftURLParams.length);
        var arrdraftParamValues = new Array(arrdraftURLParams.length);
        i = 0;
        var encodeddraftSearchParms = "";
      for (i=0;i<arrdraftURLParams.length;i++)
        {
            if (arrdraftURLParams[i] != "")
            {
                var opdraftIndex =  arrdraftURLParams[i].indexOf("=");
                var draftkey = arrdraftURLParams[i].substring(0,opdraftIndex);
                var draftvalue = arrdraftURLParams[i].substring(opdraftIndex+1);
                if ("currentMailOption" == draftkey)
                {
                     draftvalue = currentMailOption;
                }
              }

            encodeddraftSearchParms = encodeddraftSearchParms + draftkey + "=" + draftvalue + "&";
         }
          document.getElementById("draft_change").href=arrdraftParams[0].concat("?"+encodeddraftSearchParms);
        }


        link ="";
      hrefLink = document.getElementById("sentcust_change");
        if(hrefLink!=null){
         link=hrefLink.href;
         }
      if (link.indexOf("?") > 0)
      {
        var arrbanksentParams = link.split("?");
        var arrbanksentURLParams = arrbanksentParams[1].split("&");
        var arrbanksentParamNames = new Array(arrbanksentURLParams.length);
        var arrbanksentParamValues = new Array(arrbanksentURLParams.length);
        i = 0;
        var encodedbanksentSearchParms = "";
      
     for (i=0;i<arrbanksentURLParams.length;i++)
        {
            if (arrbanksentURLParams[i] != "")
            {
                var opSentCustIndex =  arrbanksentURLParams[i].indexOf("=");
                var sentCustkey = arrbanksentURLParams[i].substring(0,opSentCustIndex);
                var sentCustvalue = arrbanksentURLParams[i].substring(opSentCustIndex+1);
                if ("currentMailOption" == sentCustkey)
                {
                	sentCustvalue = currentMailOption;
                }
              }

            encodedbanksentSearchParms = encodedbanksentSearchParms + sentCustkey + "=" + sentCustvalue + "&";
         }
      
         document.getElementById("sentcust_change").href=arrbanksentParams[0].concat("?"+encodedbanksentSearchParms);
        }
  }

          require(["dojo/query", "dojo/aspect","dijit/registry","dojo/ready","dojo/dom", "t360/dialog", "t360/popup", "dojo/on", "t360/datagrid","dojo/_base/array","dojo/domReady!"], 
    		  function(query, aspect,registry,ready,dom,dialog,popup,on,datagrid, baseArray){
            ready(function(){
            	initSearch();
                  var bgrid=registry.byId("BankMailMessagesDataGridId");
                  aspect.after(bgrid, "_onFetchComplete", function(){
                        if (<%=currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER)%> == true)
                              dom.byId("draftsCount").innerHTML= " ("+this.store._numRows+")";
                        if (<%=currentFolder.equals(TradePortalConstants.SENT_TO_CUST_FOLDER)%> == true)
                              dom.byId("sentToBankCount").innerHTML= " ("+this.store._numRows+")";

                  });
                  
                  var myGrid = registry.byId("BankMailMessagesDataGridId");
      	        	dojo.connect(myGrid, 'onStyleRow' , this, function(row) {
      	               var item = myGrid.getItem(row.index);

      	                if (item) {
      	                    var type = myGrid.store.getValue(item, "UnreadFlag");

      	                    if (type == 'Y') {
      	                          row.customStyles += "font-weight: bold;";
      	                    }
      	                }

      	                myGrid.focus.styleRow(row);
      	                myGrid.edit.styleRow(row);
      	            });
      	          if(document.getElementById('tempMsgDiv')){
        	        	document.getElementById('tempMsgDiv').focus();
        	        }
        	    
              });
              
              query('#mailGridEdit').on("click", function() {
                  var columns = datagrid.getColumnsForCustomization('BankMailMessagesDataGridId');
                  var parmNames = [ "gridId", "gridName", "columns" ];
                  var parmVals = [ "BankMailMessagesDataGridId", "BankMailMessagesDataGrid", columns ];
                  popup.open(
                    'mailGridEdit', 'gridCustomizationPopup.jsp',
                    parmNames, parmVals,
                    null, null);  <%-- callbacks --%>
                });
              
              query('#messageMailHeaderRefresh').on("click", function() {
                  searchMail("I");
                });
             
        });

      
</script>
