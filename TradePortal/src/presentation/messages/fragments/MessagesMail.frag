<%--
*******************************************************************************
                               Messages Home Page

  Description:  The Messages Home page is used to display all messages sent by
                the user organization's bank, all messages routed to a user by
                other users in the organization or related child organizations,
                and all draft messages that the belong to the user, his/her
                organization, or child organization users (assuming the user
                has the appropriate security rights).

*******************************************************************************
--%>
<%--
*
*     Copyright  � 2001
*     American Management Systems, Incorporated
*     All rights reserved
--%>

<%-- ************** Data retrieval page setup begins here ****************  --%>

<%
// Use the constants from MailMessagesDataView.java
  //cquinton 4/9/2013 Rel PR ir#15704 use text keys instead of localized text
 final String MY_MAIL_AND_UNASSIGNED = "Mail.MeAndUnassigned";
final String MY_MAIL_ONLY           = "Mail.Me";
 final String ALL_MAIL               = "Mail.AllMail";
 
 final String ALL_MAILS = "Mail.AllMailMESS";
 final String MAIL_READ  = "Mail.ReadMailMESS";
 final String MAIL_UNREAD = "Mail.UnreadMailMESS";


  StringBuffer      sentToBankFolderTotalWhereClause = new StringBuffer();
  StringBuffer      sentToBankFolderTotal            = null;
  StringBuffer      dynamicWhereClause               = new StringBuffer();
  StringBuffer      draftsFolderTotalWhereClause     = new StringBuffer();
  StringBuffer      draftsFolderTotal                = null;
  StringBuffer      inboxFolderTotalWhereClause      = new StringBuffer();
  StringBuffer      inboxFolderTotal                 = null;
  StringBuffer      tempSql                          = null;
  StringBuffer      newLink                          = new StringBuffer();
  String            currentMailOption                = null;
  String            onLoad                           = null;

  String  gridId="MailMessagesDataGridId";

  int               folderTotal                      = 0;

  //Naveen IR-T36000011007(ANZ- 721) 02/02/2013- Added for Route button navigation
  // DK IR T36000026794 Rel9.0 05/29/2014 starts
  session.setAttribute("fromPage", "MessagesHome");
  /*String    fromPage          = (String) session.getAttribute("fromPage");
  if(fromPage == null) {
	  session.setAttribute("fromPage", "MessagesHome");
	  session.removeAttribute("fromTPHomePage");
  }*/
  //DK IR T36000026794 Rel9.0 05/29/2014 ends
   if (current2ndNav.equals(TradePortalConstants.NAV__MAIL))
   {
   /*   IR 20876 -start - moved to MessagesHome.jsp
      currentFolder = request.getParameter("currentFolder");

      if (currentFolder == null || "".equals(currentFolder.trim()))
      {
         currentFolder = (String) session.getAttribute("currentFolder");

         if (currentFolder == null || "".equals(currentFolder.trim()))
         {
            currentFolder = TradePortalConstants.INBOX_FOLDER;
         }
      }  IR 20876 - end */

      currentMailOption = request.getParameter("currentMailOption");

      if (currentMailOption == null || "".equals(currentMailOption.trim()))
      {
         currentMailOption = (String) session.getAttribute("currentMailOption");
         // If this is the first time accessing this page, default the mail option dropdown
         // list to the 'Me and Unnassigned' option
         if (currentMailOption == null || "".equals(currentMailOption.trim()))
         {
            currentMailOption = EncryptDecrypt.encryptStringUsingTripleDes(MailMessagesDataView.MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey());
         }
      }
//commented by dillip defect#720 on 16-Feb-2013
     // session.setAttribute("currentMailOption", currentMailOption);
     // session.setAttribute("currentFolder",     currentFolder);
//Ended Here
      currentMailOption = EncryptDecrypt.decryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey());

      //cquinton 4/9/2013 Rel PR ir#15704 - convert between options on different folders
      if ( TradePortalConstants.INBOX_FOLDER.equals(currentFolder) ) {
        if ( MailMessagesDataView.MY_MAIL_ONLY.equals(currentMailOption) ) {
          currentMailOption = MailMessagesDataView.MY_MAIL_AND_UNASSIGNED;
        }
      }
      else {
        if ( MailMessagesDataView.MY_MAIL_AND_UNASSIGNED.equals(currentMailOption) ) {
          currentMailOption = MailMessagesDataView.MY_MAIL_ONLY;
        }
      }
      
      //jgadela R91 IR T36000026319 - SQL INJECTION FIX
	  List<Object> sqlParamsInboxFolder = new ArrayList();
	  List<Object> sqlParamsDrafsFolder = new ArrayList();
	  List<Object> sqlParamsSentToBankFolder = new ArrayList();

      // The following code constructs the sql to use for the dynamic where clause listview
      // component and each of the folder totals adjacent to the folder images; these totals
      // (as well as the listview) depend on which mail option is currently selected in the
      // dropwdown list.

     if (currentMailOption.equals(MailMessagesDataView.MY_MAIL_ONLY))
      {
         dynamicWhereClause.append("and a.a_assigned_to_user_oid = ");
         dynamicWhereClause.append(userOid);

         inboxFolderTotalWhereClause.append("(a_assigned_to_user_oid = ? ");
         inboxFolderTotalWhereClause.append(" or (a_assigned_to_corp_org_oid = ? ");
         inboxFolderTotalWhereClause.append(" and a_assigned_to_user_oid is null))");

         draftsFolderTotalWhereClause.append("a_assigned_to_user_oid = ? ");

         sentToBankFolderTotalWhereClause.append(draftsFolderTotalWhereClause.toString());
         //jgadela R91 IR T36000026319 - SQL INJECTION FIX
         sqlParamsInboxFolder.add(userOid);
         sqlParamsInboxFolder.add(userOrgOid);
         
         sqlParamsDrafsFolder.add(userOid);
         sqlParamsSentToBankFolder.add(userOid);
      }
      else if (currentMailOption.equals(MailMessagesDataView.MY_MAIL_AND_UNASSIGNED))
      {
         dynamicWhereClause.append("and (a.a_assigned_to_user_oid = ");
         dynamicWhereClause.append(userOid);
         dynamicWhereClause.append(" or (a.a_assigned_to_corp_org_oid = ");
         dynamicWhereClause.append(userOrgOid);
         dynamicWhereClause.append(" and a.a_assigned_to_user_oid is null))");

         inboxFolderTotalWhereClause.append("(a_assigned_to_user_oid = ? ");
         inboxFolderTotalWhereClause.append(" or (a_assigned_to_corp_org_oid = ? ");
         inboxFolderTotalWhereClause.append(" and a_assigned_to_user_oid is null))");

         draftsFolderTotalWhereClause.append("a_assigned_to_user_oid = ? ");
       //IR 20876 start -there is no ME and Unassigned dropdown value for sent to bank folder
         sentToBankFolderTotalWhereClause.append(draftsFolderTotalWhereClause.toString());
        //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    	 sqlParamsInboxFolder.add(userOid);
         sqlParamsInboxFolder.add(userOrgOid);
         
         sqlParamsDrafsFolder.add(userOid);
         sqlParamsSentToBankFolder.add(userOid);
       //IR 20876-end
      }
      else if (currentMailOption.equals(MailMessagesDataView.ALL_MAIL))
      {
         String tempSql1 = " select organization_oid from corporate_org where activation_status = 'TradePortalConstants.ACTIVE' start with organization_oid = "
         			+userOrgOid+" connect by prior organization_oid = p_parent_corp_org_oid)";

         dynamicWhereClause.append("and a.a_assigned_to_corp_org_oid in (");
         dynamicWhereClause.append(tempSql1);

         inboxFolderTotalWhereClause.append("a_assigned_to_corp_org_oid in (");
         inboxFolderTotalWhereClause.append(" select organization_oid from corporate_org where activation_status = ? start with organization_oid = ? connect by prior organization_oid = p_parent_corp_org_oid)");

         draftsFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());

         sentToBankFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());
         
         //jgadela R91 IR T36000026319 - SQL INJECTION FIX
    	 sqlParamsInboxFolder.add(TradePortalConstants.ACTIVE);
         sqlParamsInboxFolder.add(userOrgOid);
         
         sqlParamsDrafsFolder.add(TradePortalConstants.ACTIVE);
         sqlParamsDrafsFolder.add(userOrgOid);
         
         sqlParamsSentToBankFolder.add(TradePortalConstants.ACTIVE);
         sqlParamsSentToBankFolder.add(userOrgOid);
      }
      else
      {
         dynamicWhereClause.append("and a.a_assigned_to_corp_org_oid = ");
         dynamicWhereClause.append(currentMailOption);

         inboxFolderTotalWhereClause.append("a_assigned_to_corp_org_oid = ? ");

         draftsFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());

         sentToBankFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());
         
         //jgadela R91 IR T36000026319 - SQL INJECTION FIX
         sqlParamsInboxFolder.add(currentMailOption);
         sqlParamsDrafsFolder.add(currentMailOption);
         sqlParamsSentToBankFolder.add(currentMailOption);
      }

     

      inboxFolderTotalWhereClause.append(" and message_status in ('");
      inboxFolderTotalWhereClause.append(TradePortalConstants.REC);
      inboxFolderTotalWhereClause.append("', '");
      inboxFolderTotalWhereClause.append(TradePortalConstants.REC_ASSIGNED);
      inboxFolderTotalWhereClause.append("', '");
      inboxFolderTotalWhereClause.append(TradePortalConstants.SENT_ASSIGNED);
      inboxFolderTotalWhereClause.append("') ");
      inboxFolderTotalWhereClause.append("and unread_flag = 'Y'");
    //CR 913 - Rel 9.0 Filter mail messages for funding_amount and funding_date is NULL
      inboxFolderTotalWhereClause.append(" and funding_amount is NULL");
      inboxFolderTotalWhereClause.append(" and funding_date is NULL ");

      draftsFolderTotalWhereClause.append(" and message_status = '");
      draftsFolderTotalWhereClause.append(TradePortalConstants.DRAFT);
      draftsFolderTotalWhereClause.append("' ");

      sentToBankFolderTotalWhereClause.append(" and message_status = '");
      sentToBankFolderTotalWhereClause.append(TradePortalConstants.SENT_TO_BANK);
      sentToBankFolderTotalWhereClause.append("' ");

      onLoad = "document.MessageListForm.MailOption.focus();";

      // Retrieve the number of Unread messages in the Inbox folder based on the option selected
      // in the 'Show mail for:' dropdown list
      folderTotal = DatabaseQueryBean.getCount("message_oid", "mail_message", inboxFolderTotalWhereClause.toString(),
                                               false, sqlParamsInboxFolder);

      inboxFolderTotal = new StringBuffer();
      inboxFolderTotal.append("(");
      inboxFolderTotal.append(folderTotal);
      inboxFolderTotal.append(" ");
      inboxFolderTotal.append(resMgr.getText("Mail.Unread", TradePortalConstants.TEXT_BUNDLE));
      inboxFolderTotal.append(")");

      // Retrieve the number of Draft messages in the Drafts folder based on the option selected
      // in the 'Show mail for:' dropdown list
      folderTotal = DatabaseQueryBean.getCount("message_oid", "mail_message", draftsFolderTotalWhereClause.toString(),
                                               false, sqlParamsDrafsFolder);

      draftsFolderTotal = new StringBuffer();
      draftsFolderTotal.append("(");
      draftsFolderTotal.append(folderTotal);
      draftsFolderTotal.append(")");

      // Retrieve the number of Sent to Bank messages in the Sent to Bank folder based on the option selected
      // in the 'Show mail for:' dropdown list
      folderTotal = DatabaseQueryBean.getCount("message_oid", "mail_message", sentToBankFolderTotalWhereClause.toString(),
                                               false, sqlParamsSentToBankFolder);

      sentToBankFolderTotal = new StringBuffer();
      sentToBankFolderTotal.append("(");
      sentToBankFolderTotal.append(folderTotal);
      sentToBankFolderTotal.append(")");
%>

      <%-- ********************* JavaScript for page begins here *********************  --%>

<%
   }
%>

<%-- ********************* HTML for page begins here *********************  --%>

<%--cquinton 9/20/2012 ir#4556 move header to MessagesHome.jsp so common html is correct--%>

<div class="pageMainNoSidebar">
<div class="pageContent">
<%-- ctq portal refresh secondary navigation start --%>
<%@ include file="MessagesSecondaryNavigation.frag" %>
<%-- ctq portal refresh secondary navigation end --%>

<form name="MessageListForm" method="POST" action="<%= formMgr.getSubmitAction(response) %>">
  <jsp:include page="/common/ErrorSection.jsp" />
  <div class="formContentNoSidebar">
    <input type=hidden value="" id =buttonName name=buttonName>

<%
   if (current2ndNav.equals(TradePortalConstants.NAV__MAIL))
   {
      // Store the user's oid and security rights in a secure hashtable for the form
      secureParms.put("SecurityRights", userSecurityRights);
      secureParms.put("UserOid",        userOid);
   }
%>
    <div class="searchHeader">
      <span class="searchHeaderCriteria">
<%= formMgr.getFormInstanceAsInputField("MessageListForm", secureParms) %>
  <%
     if (current2ndNav.equals(TradePortalConstants.NAV__MAIL))
     {
  %>
              <%
                 if (currentFolder.equals(TradePortalConstants.INBOX_FOLDER))
                 {
                    dropdownOptions.append("<option value=\"");
                    dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(MailMessagesDataView.MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey()));
                    dropdownOptions.append("\"");

                    if (MailMessagesDataView.MY_MAIL_AND_UNASSIGNED.equals(currentMailOption)) {
                       dropdownOptions.append(" selected");
                    }

                    dropdownOptions.append(">");
                    dropdownOptions.append(resMgr.getText(MailMessagesDataView.MY_MAIL_AND_UNASSIGNED, TradePortalConstants.TEXT_BUNDLE));
                    dropdownOptions.append("</option>");
                 }
                 else
                 {
                    dropdownOptions.append("<option value=\"");
                    dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(MailMessagesDataView.MY_MAIL_ONLY, userSession.getSecretKey()));
                    dropdownOptions.append("\"");

                    if (MailMessagesDataView.MY_MAIL_ONLY.equals(currentMailOption)) {
                       dropdownOptions.append(" selected");
                    }

                    dropdownOptions.append(">");
                    dropdownOptions.append(resMgr.getText(MailMessagesDataView.MY_MAIL_ONLY, TradePortalConstants.TEXT_BUNDLE));
                    dropdownOptions.append("</option>");
                 }

                 // If the user has rights to view child organization mail, retrieve the list of dropdown
                 // options from the DatabaseQueryBean results doc (containing an option for each child org);
                 // otherwise, simply use the user's org option to build the dropdown list (in addition
                 // to the default 'Me' or 'Me and Unassigned' option).
                 if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.VIEW_CHILD_ORG_MESSAGES))
                 {
                    dropdownOptions.append(Dropdown.getUnsortedOptions(hierarchyDoc, "ORGANIZATION_OID", "NAME",
                                                                       currentMailOption, userSession.getSecretKey()));

                    // Only display the 'All Mail' option if the user's org has child organizations
                    if (totalOrganizations > 1)
                    {
                       dropdownOptions.append("<option value=\"");
                       dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(MailMessagesDataView.ALL_MAIL, userSession.getSecretKey()));
                       dropdownOptions.append("\"");

                       if (MailMessagesDataView.ALL_MAIL.equals(currentMailOption)) {
                          dropdownOptions.append(" selected");
                       }

                       dropdownOptions.append(">");
                       dropdownOptions.append(resMgr.getText(MailMessagesDataView.ALL_MAIL, TradePortalConstants.TEXT_BUNDLE));
                       dropdownOptions.append("</option>");
                    }
                 }
                 else
                 {
                    dropdownOptions.append("<option value=\"");
                    dropdownOptions.append(EncryptDecrypt.encryptStringUsingTripleDes(userOrgOid, userSession.getSecretKey()));
                    dropdownOptions.append("\"");

                    if (currentMailOption.equals(userOrgOid))
                    {
                       dropdownOptions.append(" selected");
                    }


                    dropdownOptions.append(">");
                    dropdownOptions.append(userOrgName);
                    dropdownOptions.append("</option>");
                 }

                 extraTags.append("onchange=\"location='");
                 extraTags.append(formMgr.getLinkAsUrl("goToMessagesHome", response));
                 extraTags.append("&amp;current2ndNav=" + TradePortalConstants.NAV__MAIL);
                 extraTags.append("&amp;currentFolder=" + currentFolder);
                 extraTags.append("&amp;currentMailOption='+this.options[this.selectedIndex].value\"");

                 out.print(widgetFactory.createSearchSelectField("MailOption","Mail.Show","",dropdownOptions.toString(),
                         "onChange='searchMail(\"Select\");'"));

              %>
               <%-- CR49930 - Start--%> 
              <%
                
                 	StringBuffer dropdownMailOptions = new StringBuffer();
        	     	boolean userCanFilterOrg = true;
      
       				dropdownMailOptions.append("<option value='"+ALL_MAILS+"' selected='true' >");
        			dropdownMailOptions.append(resMgr.getText( ALL_MAILS, TradePortalConstants.TEXT_BUNDLE));
        			dropdownMailOptions.append("</option>");
     
        			dropdownMailOptions.append("<option value='N' selected='true' >");
        			dropdownMailOptions.append(resMgr.getText( MAIL_READ, TradePortalConstants.TEXT_BUNDLE));
        			dropdownMailOptions.append("</option>");
      	
        			dropdownMailOptions.append("<option value='Y' selected='true' >");
        			dropdownMailOptions.append(resMgr.getText( MAIL_UNREAD, TradePortalConstants.TEXT_BUNDLE));
        			dropdownMailOptions.append("</option>");
     		 %>          
        	<%=widgetFactory.createSearchSelectField("MailReadUnread", "Mail.ReadUnreadMessMail", "", 
        				dropdownMailOptions.toString(),"onChange='searchMail(\"Select\");'")%>  
        
             

                      <%
                         if (currentFolder.equals(TradePortalConstants.INBOX_FOLDER))
                         {
                            //cquinton 1/22/2013 ir#10297 add folder specific icons
                      %>
                            <span class="messagesFolderOpen inbox">
                            <%=resMgr.getText("Mail.Inbox", TradePortalConstants.TEXT_BUNDLE)%>
                            <span id="inboxCount">
                              <%= inboxFolderTotal.toString() %>
                            </span>


                                 </span>
                       <%
                         }
                         else
                         {
                            newLink = new StringBuffer();

                            newLink.append(formMgr.getLinkAsUrl("goToMessagesHome", response));
                            newLink.append("&current2ndNav=" + TradePortalConstants.NAV__MAIL);
                            newLink.append("&currentFolder=" + TradePortalConstants.INBOX_FOLDER);
                            newLink.append("&currentMailOption=");
                            newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey()));
                            //cquinton 1/22/2013 ir#10297 add folder specific icons
                       %>
                            <span class="messagesFolderClosed inbox"><a href="<%= newLink.toString() %>" id="inbox_change" >
                          <%=resMgr.getText("Mail.Inbox", TradePortalConstants.TEXT_BUNDLE)%>
                          <%= inboxFolderTotal.toString() %>
                          </a>
                            </span>

                      <%
                         }
                      if (currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER))
                         {
                            //cquinton 1/22/2013 ir#10297 add folder specific icons

                      %>
                              <span class="messagesFolderOpen drafts">
                              <%=resMgr.getText("Mail.Drafts", TradePortalConstants.TEXT_BUNDLE)%>

                            <span id="draftsCount">
                              <%= draftsFolderTotal.toString() %>
                            </span></span>
                     <%
                         }
                         else
                         {

                            newLink = new StringBuffer();

                            newLink.append(formMgr.getLinkAsUrl("goToMessagesHome", response));
                            newLink.append("&current2ndNav=" + TradePortalConstants.NAV__MAIL);
                            newLink.append("&currentFolder=" + TradePortalConstants.DRAFTS_FOLDER);
                            newLink.append("&currentMailOption=");
                            newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey()));
                            //cquinton 1/22/2013 ir#10297 add folder specific icons

                      %>
                              <span class="messagesFolderClosed drafts">
                            <a href="<%= newLink.toString() %>" id="draft_change">
                              <%=resMgr.getText("Mail.Drafts", TradePortalConstants.TEXT_BUNDLE)%>
                              <%= draftsFolderTotal.toString() %>
                            </a>
                            </span>
                      <%
                         }

                         if (currentFolder.equals(TradePortalConstants.SENT_TO_BANK_FOLDER))
                         {
                            //cquinton 1/22/2013 ir#10297 add folder specific icons
                      %>
                              <span class="messagesFolderOpen sentToBank">
                              <%=resMgr.getText("Mail.SentToBank", TradePortalConstants.TEXT_BUNDLE)%>
                              <span id="sentToBankCount"><%= sentToBankFolderTotal.toString() %></span>
                            </span>
                    <%
                         }
                         else
                         {
                            newLink = new StringBuffer();

                            newLink.append(formMgr.getLinkAsUrl("goToMessagesHome", response));
                            newLink.append("&current2ndNav=" + TradePortalConstants.NAV__MAIL);
                            newLink.append("&currentFolder=" + TradePortalConstants.SENT_TO_BANK_FOLDER);
                            newLink.append("&currentMailOption=");
                            newLink.append(EncryptDecrypt.encryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey()));
                            //cquinton 1/22/2013 ir#10297 add folder specific icons
                      %>
                              <span class="messagesFolderClosed sentToBank">
                            <a href="<%= newLink.toString() %>" id="sentbank_change">
                              <%=resMgr.getText("Mail.SentToBank", TradePortalConstants.TEXT_BUNDLE)%>
                              <%= sentToBankFolderTotal.toString() %>
                            </a>
                            </span>
                      <%
                         }
                      %>
      </span>
      <span class="searchHeaderActions">
        <jsp:include page="/common/gridShowCount.jsp">
          <jsp:param name="gridId" value="<%=gridId%>" />
        </jsp:include>

    <%
    	if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.CREATE_MESSAGE)) {
    		newLink = new StringBuffer();
    		newLink.append(formMgr.getLinkAsUrl("goToMailMessageDetail", response));
    %>
    		<button data-dojo-type="dijit.form.Button"  name="New" id="New" type="button" >
    			<%=resMgr.getText("common.New",TradePortalConstants.TEXT_BUNDLE)%>
    			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
    				openURL("<%=newLink.toString()%>");
    			</script>
    		</button>
    		<%=widgetFactory.createHoverHelp("New","NewHoverText") %>
    <%
    	}
    %>
        <span id="messageMailHeaderRefresh" class="searchHeaderRefresh"></span>
        <%=widgetFactory.createHoverHelp("messageMailHeaderRefresh","RefreshImageLinkHoverText") %>
        
        <%-- SSikhakolli - Rel-9.1 CR-944 IR-T36000031676 - Adding Grid Customization Popup --%>
        <span id="mailGridEdit" class="gridHeaderEdit"></span>
        <%=widgetFactory.createHoverHelp("mailGridEdit", "CustomizeListImageLinkHoverText") %>
      </span>

      <div style="clear:both"></div>
    </div>

              <%
                 if (SecurityAccess.hasRights(userSecurityRights, SecurityAccess.ROUTE_MESSAGE) ||
                     SecurityAccess.hasRights(userSecurityRights, SecurityAccess.ROUTE_DISCREPANCY_MSG)) {
              %>
                    <%-- fromListView indicates that the Route request comes from a listview
                         and fromMessages indicate that the Route request is from Messages and not Transaction --%>
                    <input type="hidden" name="fromListView" value="<%=TradePortalConstants.INDICATOR_YES%>">
                    <input type="hidden" name="fromMessages" value="<%=TradePortalConstants.INDICATOR_YES%>">
              <% }%>

  <%
     }
  %>

                   <%
                                    DataGridFactory dgFactory = new DataGridFactory(resMgr, userSession, formMgr,beanMgr, response);
                                    String gridHtml = dgFactory.createDataGrid("MailMessagesDataGridId","MailMessagesDataGrid",null);
                                    String gridLayout = dgFactory.createGridLayout("MailMessagesDataGridId", "MailMessagesDataGrid");
                              %>
                              <%=gridHtml%>
  </div>
</form>

<%--cquinton 3/16/2012 Portal Refresh - start--%>

</div>
</div>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<%@ include file="/common/GetSavedSearchQueryData.frag" %>
<jsp:include page="/common/gridShowCountFooter.jsp">
  <jsp:param name="gridId" value="<%=gridId%>" />
</jsp:include>

<%--the route messages dialog--%>
<div id="routeDialog" style="display:none;"></div>

<script type="text/javascript">

  function openURL(URL){
    if (isActive =='Y') {
	    if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
        }
    }
    document.location.href  = URL;
    return false;
  }
  var gridLayout=<%=gridLayout%>;
<%
  //cquinton 4/9/2013 Rel PR ir#15704 fix issues with org defaulting
  String encryptedCurrentMailOption = EncryptDecrypt.encryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey());
%> 
var currentFolder = savedSearchQueryData["currentFolder"]; 
function initSearch(){
	require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"], 
		      function(dom, registry, t360grid) {
		
	var savedSort = savedSearchQueryData["sort"];  
	
	var currentMailOption = savedSearchQueryData["currentMailOption"]; 
	if("" == savedSort || null == savedSort || "undefined" == savedSort){
	        savedSort = '0';
	 }
	if("" == currentFolder || null == currentFolder || "undefined" == currentFolder)
		currentFolder="<%=currentFolder%>";
	else{}
	<%-- 	registry.byId("status").set('value',currentFolder); --%>
	if("" == currentMailOption || null == currentMailOption || "undefined" == currentMailOption){
	 <%-- IR 20876 -use value from registry --%>
	 <%-- 	currentMailOption='<%=encryptedCurrentMailOption%>'; --%>
		currentMailOption=registry.byId("MailOption").get('value');

	}
	else{
		registry.byId("MailOption").set('value',currentMailOption);
	}
	
  var initSearchParms = "currentFolder="+currentFolder+"&currentMailOption="+currentMailOption;
  var viewName = "<%=EncryptDecrypt.encryptStringUsingTripleDes("MailMessagesDataView",userSession.getSecretKey())%>";  <%--Rel9.2 IR T36000032596 --%>
 	createDataGrid("MailMessagesDataGridId", viewName,gridLayout, initSearchParms,savedSort);
	});
	}
	
  function searchMail(mailType){
  require(["dojo/dom", "dijit/registry", "t360/datagrid", "dojo/domReady!"], 
		      function(dom, registry, t360grid){
    var currentFolder;
    if('<%=currentFolder%>' != "") {
      currentFolder='<%=currentFolder%>';
    }

    if(mailType == "Sent"){
      currentFolder = 'S';
    }
    if(mailType == "Drafts"){
      currentFolder = 'D';
    }
    if(mailType == "Inbox"){
      currentFolder = 'I';
    }

    var searchParms = "currentFolder="+currentFolder;
    
    currentMailOption = dijit.byId('MailOption').value;
    
    var readStatusVal = registry.byId("MailReadUnread").attr('value');
    
   

    generateURL(currentMailOption);
    <%--cquinton 4/9/2013 Rel PR ir#15704 do not default org if missing, just pass nothing--%>
    
    if(currentMailOption && currentMailOption.length>0) {
      searchParms=searchParms+"&currentMailOption="+currentMailOption+"&UnreadFlag="
			                     +readStatusVal;
    }

    console.log("searchParms: "+searchParms);
    searchDataGrid("MailMessagesDataGridId", "MailMessagesDataView",searchParms);
  });}
	
      function deleteMail(){
           var formName = "MessageListForm";
          var buttonName = '<%=TradePortalConstants.BUTTON_DELETE%>';
          <%-- IR T36000048558 Rel9.5 05/09/2016 --%>
            var   confirmMessage = "<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("Mail.PopupMessage",
            TradePortalConstants.TEXT_BUNDLE)) %>";
            var selectMessage = "<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("Mail.SelectMessage",
            TradePortalConstants.TEXT_BUNDLE)) %>";
          <%--get array of rowkeys from the grid--%>
          var rowKeys = getSelectedGridRowKeys("MailMessagesDataGridId");
          if(rowKeys == ""){
                  alert(selectMessage);
                  return;
            }
            if (!confirm(confirmMessage)){
                  return false;
            }else{
          <%--submit the form with rowkeys
              becuase rowKeys is an array, the parameter name for
              each will be checkbox + an iteration number -
              i.e. checkbox0, checkbox1, checkbox2, etc --%>
            submitFormWithParms(formName, buttonName, "checkbox", rowKeys);
          }
      }

  function generateURL(currentMailOption){

      var hrefLink = document.getElementById("inbox_change");
      var link = "";
      if(hrefLink!=null){
         link=hrefLink.href;
         }
      var i;

      if (link.indexOf("?") > 0)
      {
        var arrinboxParams = link.split("?");
        var arrinboxURLParams = arrinboxParams[1].split("&");
        var arrinboxParamNames = new Array(arrinboxURLParams.length);
        var arrinboxParamValues = new Array(arrinboxURLParams.length);
        i = 0;
        var encodedinboxSearchParms = "";
       for (i=0;i<arrinboxURLParams.length;i++)
        {
            if (arrinboxURLParams[i] != "")
            {
                var opinboxIndex =  arrinboxURLParams[i].indexOf("=");
                var inboxkey = arrinboxURLParams[i].substring(0,opinboxIndex);
                var inboxvalue = arrinboxURLParams[i].substring(opinboxIndex+1);
                if ("currentMailOption" == inboxkey)
                {
                     inboxvalue = currentMailOption;
                }
              }
            encodedinboxSearchParms = encodedinboxSearchParms + inboxkey + "=" + inboxvalue + "&";
         }
         document.getElementById("inbox_change").href=arrinboxParams[0].concat("?"+encodedinboxSearchParms);
        }



       link ="";
      hrefLink = document.getElementById("draft_change");
            if(hrefLink!=null){
         link=hrefLink.href;
         }

      if (link.indexOf("?") > 0)
      {
        var arrdraftParams = link.split("?");
        var arrdraftURLParams = arrdraftParams[1].split("&");
        var arrdraftParamNames = new Array(arrdraftURLParams.length);
        var arrdraftParamValues = new Array(arrdraftURLParams.length);
        i = 0;
        var encodeddraftSearchParms = "";
      for (i=0;i<arrdraftURLParams.length;i++)
        {
            if (arrdraftURLParams[i] != "")
            {
                var opdraftIndex =  arrdraftURLParams[i].indexOf("=");
                var draftkey = arrdraftURLParams[i].substring(0,opdraftIndex);
                var draftvalue = arrdraftURLParams[i].substring(opdraftIndex+1);
                if ("currentMailOption" == draftkey)
                {
                     draftvalue = currentMailOption;
                }
              }

            encodeddraftSearchParms = encodeddraftSearchParms + draftkey + "=" + draftvalue + "&";
         }
          document.getElementById("draft_change").href=arrdraftParams[0].concat("?"+encodeddraftSearchParms);
        }


        link ="";
      hrefLink = document.getElementById("sentbank_change");
        if(hrefLink!=null){
         link=hrefLink.href;
         }
      if (link.indexOf("?") > 0)
      {
        var arrbanksentParams = link.split("?");
        var arrbanksentURLParams = arrbanksentParams[1].split("&");
        var arrbanksentParamNames = new Array(arrbanksentURLParams.length);
        var arrbanksentParamValues = new Array(arrbanksentURLParams.length);
        i = 0;
        var encodedbanksentSearchParms = "";
      for (i=0;i<arrbanksentURLParams.length;i++)
        {
            if (arrbanksentURLParams[i] != "")
            {
                var opbanksentIndex =  arrbanksentURLParams[i].indexOf("=");
                var banksentkey = arrbanksentURLParams[i].substring(0,opbanksentIndex);
                var banksentvalue = arrbanksentURLParams[i].substring(opbanksentIndex+1);
                if ("currentMailOption" == banksentkey)
                {
                     banksentvalue = currentMailOption;
                }
              }
            encodedbanksentSearchParms = encodedbanksentSearchParms + banksentkey + "=" + banksentvalue + "&";
         }
         document.getElementById("sentbank_change").href=arrbanksentParams[0].concat("?"+encodedbanksentSearchParms);
        }
  }

      function routeMail(){
            openRouteDialog('routeDialog', routeItemDialogTitle,'MailMessagesDataGridId','<%=TradePortalConstants.INDICATOR_YES%>','<%=TradePortalConstants.FROM_MESSAGES%>',getSelectedGridRowKeys('MailMessagesDataGridId')) ;
            return false;
      }

      require(["dojo/query", "dojo/aspect","dijit/registry","dojo/ready","dojo/dom", "t360/dialog", "t360/popup", "dojo/on", "t360/datagrid","dojo/_base/array","dojo/domReady!"], 
    		  function(query, aspect,registry,ready,dom,dialog,popup,on,datagrid, baseArray){
            ready(function(){
            	initSearch();
                  var bgrid=registry.byId("MailMessagesDataGridId");
                  aspect.after(bgrid, "_onFetchComplete", function(){
                        if (<%=currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER)%> == true)
                              dom.byId("draftsCount").innerHTML= " ("+this.store._numRows+")";
                        if (<%=currentFolder.equals(TradePortalConstants.SENT_TO_BANK_FOLDER)%> == true)
                              dom.byId("sentToBankCount").innerHTML= " ("+this.store._numRows+")";

                  });
                  
                  <%--
                  	SSikhakolli - Rel-8.4.0.2 IR#T36000028198 06/11/2014 - Begin.
                  	This code has been moved from Messages Home Jsp to here.
                  --%>
                   <%--CR49930 - Start--%> 
                  var myGrid = registry.byId("MailMessagesDataGridId");
      	        	dojo.connect(myGrid, 'onStyleRow' , this, function(row) {
      	               var item = myGrid.getItem(row.index);

      	                if (item) {
      	                    var type = myGrid.store.getValue(item, "UnreadFlag");

      	                    if (type == 'Y') {
      	                          row.customStyles += "font-weight: bold;";
      	                    }
      	                }

      	                myGrid.focus.styleRow(row);
      	                myGrid.edit.styleRow(row);
      	            });
      	        if(document.getElementById('tempMsgDiv')){
      	        	document.getElementById('tempMsgDiv').focus();
      	        }
      	      <%--SSikhakolli - Rel-8.4.0.2 IR#T36000028198 06/11/2014 - End.--%>
            });
            
            <%-- SSikhakolli - Rel-9.1 CR-944 IR-T36000031676 - Adding Grid Customization Popup --%>
            query('#mailGridEdit').on("click", function() {
                var columns = datagrid.getColumnsForCustomization('MailMessagesDataGridId');
                var parmNames = [ "gridId", "gridName", "columns" ];
                var parmVals = [ "MailMessagesDataGridId", "MailMessagesDataGrid", columns ];
                popup.open(
                  'mailGridEdit', 'gridCustomizationPopup.jsp',
                  parmNames, parmVals,
                  null, null);  <%-- callbacks --%>
              });
            
            query('#messageMailHeaderRefresh').on("click", function() {
                searchMail("I");
              });
      });
      
</script>

<%--cquinton 3/16/2012 Portal Refresh - end--%>
