<%    /*****************************************************
      * Action Buttons For Mail Message Detail page
      *******************************************************/
%>
<%
  boolean showLinks = false;
  boolean showTips = true;
  boolean showTop = false;
  boolean showDeleteAttachDocButton = false;
  //WidgetFactory widgetFactory = new WidgetFactory(resMgr);

  if(request.getParameter("showLinks")!=null)
    if(request.getParameter("showLinks").equals("true"))
      showLinks = true;

  if(request.getParameter("showTips")!=null)
    if(request.getParameter("showTips").equals("false"))
      showTips = false;

  if(request.getParameter("showTop")!=null)
	    if(request.getParameter("showTop").equals("true"))
	      showTop = true;
// Added code to check if we need to display the 'Delete a Document' button by Dillip
   StringBuilder query = new StringBuilder();
   //jgadela R90 IR T36000026319 - SQL INJECTION FIX
	List<Object> sqlParams = new ArrayList();
   query.append("select doc_image_oid, image_id, doc_name, hash ");
   query.append("from document_image ");
   if ((mailMessageOid != null) && (mailMessageOid.trim().length() > 0)) {
     query.append("where mail_message_oid = ? ");
     sqlParams.add(mailMessageOid);
   }
   else {
     query.append("where mail_message_oid = ? ");
     sqlParams.add(messageOid.toString());
   }
   query.append(" and p_transaction_oid is null");
   query.append(" order by image_id");
   Debug.debug("Query is : " + query);
   DocumentHandler dbQuerydoc = DatabaseQueryBean.getXmlResultSet(query.toString(), false, sqlParams);

			if (dbQuerydoc != null) {
				if (dbQuerydoc.getFragments("/ResultSetRecord/").size() > 0) {
					showDeleteAttachDocButton = true;
				}
			}
//Ended Here
%>
<%--PR IR-T36000006128 Rakesh Begin--%>
<%= widgetFactory.createSidebarQuickLinks(showLinks, showTips, showLinks || showTop, userSession) %>
      <ul class="sidebarButtons">

<%-- ------------ --%>
<%-- Route Button --%>
<%-- ------------ --%>
<%--		If the Message Status is NOT sent to bank AND either:			--%>
<%--		it's not a discrepancy notice and we have the right to route a message, --%>
<%--		Otherwise it is a discrepancy and the user has the right to route a discrepancy message. --%>

<% if( !messageStatus.equals(TradePortalConstants.SENT_TO_BANK) &&
      ((!isDiscrepancy && !isSettlementReq && hasRouteRights ) || (isDiscrepancy && hasRouteDiscrepRights ) || (isSettlementReq && hasRouteSITRights)) ) {

	String routeButtonName;

	if(isReadOnly){ 	//Just route to the next page: Route/RouteItems
		routeButtonName = "Route";

	}else{			//Route to: RouteAndSave/RouteItems
		routeButtonName = "RouteAndSave";
	}
%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
			<button data-dojo-type="dijit.form.Button"  name=<%=routeButtonName%> id="RouteMessages" type="button" data-dojo-props="iconClass:'route'" >
                  <%=resMgr.getText("Mail.Route", TradePortalConstants.TEXT_BUNDLE)%>
                  <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
                  var isMessageOidNull = <%=isMessageOidNull%>;
                  var isNewReply = <%=isNewReply%>;
                  if( isMessageOidNull && !isNewReply ) {
                        routeMail();
                        return false;
                  }else{
                        openRouteDialog('routeDialog', routeItemDialogTitle,'','<%=TradePortalConstants.INDICATOR_NO%>','<%=TradePortalConstants.FROM_MESSAGES%>','<%=encryptedMailMessageOid%>','<%=StringFunction.xssCharsToHtml(subject)%>','','') ;
                        return false;
                  }
                  </script>
            </button>

		<%=widgetFactory.createHoverHelp("RouteMessages","RouteHoverText") %>
<% } %>

<%-- ------------------- --%>
<%-- Send to Bank Button --%>
<%-- ------------------- --%>
<%--		If the user has the security right to send a message and we're Not in read only mode	--%>
<%-- 		then the Send to Bank button is displayed.						--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the state of the button to disabled. This is done to prevent double click. --%>
<% if( messageCanSendToBank && hasSendRights ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="SendToBank" id="SendToBank" type="button" data-dojo-props="iconClass:'send'" >
			<%=resMgr.getText("common.SendToBankText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					registry.byId("SendToBank").set('disabled', true);
					common.setButtonPressed('SendToBank', '0');
					document.forms[0].submit();
				});
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("SendToBank","SendtoBankHoverText") %>
<% } %>

<%-- ----------- --%>
<%-- Save Button --%>
<%-- ----------- --%>
<%--		If this page is Read Only, then we dont need to display the save as Draft button.	--%>
<%--		We alsways call the save function :'Save as Draft' per webpage definition.		--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the state of the button to disabled. This is done to prevent double click. --%>
<% if( !isReadOnly ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="SaveAsDraft" id="SaveAsDraft" type="button" data-dojo-props="iconClass:'saveDraftClose'" >
			<%=resMgr.getText("mailMessage.SaveDraft", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					registry.byId("SaveAsDraft").set('disabled', true);
					common.setButtonPressed('SaveAsDraft', '0');
					document.forms[0].submit();
				});
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("SaveAsDraft","SaveDraftHoverText") %>
<% } %>

<%-- -------------------- --%>
<%-- Reply to Bank Button --%>
<%-- -------------------- --%>
<%--		If the message is a Discrepancy Notice then the has to have a security right to create/	--%>
<%--		modify Discrepancy(s).  If this message is a Mail Message than the user needs the right --%>
<%--		to create / edit Mail Messages.  Either way this has to be a read only page to start.	--%>

<%-- 		It should be noted that Replying to Bank is either done by Submission or by a link...   --%>
<%--		If we are dealing with a discrepancy then the button is a submit button, other wise	--%>
<%--		we are dealing with a link that brings the user back to this jsp in a different mode.	--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>

<%

//Added following if condition not to show ReplyToBank button to the Parent org if the
// child mail message is accessed directly and not through Subsidiary Access
if ((!isUsersOrgOid) && (!userSession.hasSavedUserSession()))
{
	displayDiscrepancyReplyToBankButton = false;
}


	if( isMsgFromBank && ( (isDiscrepancy && hasEditDiscrepRights && displayDiscrepancyReplyToBankButton) || 
	               (isSettlementReq && hasEditSITRights && displayDiscrepancyReplyToBankButton) ) && !isPreDebitFun) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="ReplyToBank" id="ReplyToBank" type="button" data-dojo-props="iconClass:'reply'" >
			<%=resMgr.getText("common.ReplytoBankText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				<%--IR 13682 --%>
				<%--setButtonPressed('ReplyToBank', '0');
				document.forms[0].submit();--%>
				submitReplyToBank();
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("ReplyToBank","ReplytoBankHoverText") %>

<%    }else if(isMsgFromBank && !isDiscrepancy && hasEditRights && !isPreDebitFun && !isSettlementReq) {

	  urlParm = "&isReply=true" + "&relatedInstID=" + completeInstId + "&subject=" + java.net.URLEncoder.encode(subject) +
		    "&sequenceNumber=" + sequenceNumber + "&instrumentOid=" +instrumentOid;
%>

    	<button data-dojo-type="dijit.form.Button"  name="ReplyToBank" id="ReplyToBank" type="button" data-dojo-props="iconClass:'reply'" >
			<%=resMgr.getText("common.ReplytoBankText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				openURL("<%=formMgr.getLinkAsUrl( goToMailMessageDetail, urlParm, response )%>");
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("ReplyToBank","ReplytoBankHoverText") %>
<%    }  %>

<%-- ------------------- --%>
<%-- Edit Message Button --%>
<%-- ------------------- --%>
<%-- 		This is a simple Link to bring the user back to this jsp in a different mode.		--%>
<%-- 		The user needs to access this same message, except in edit mode - not readOnly mode.	--%>

<% if( !messageStatus.equals(TradePortalConstants.SENT_TO_BANK) && isReadOnly && !isCurrentUser && hasEditRights && !isMsgFromBank) {

	  encryptVal2 = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid , userSession.getSecretKey());
	  urlParm = "&isEdit=true" + "&mailMessageOid=" + encryptVal2;
%>
    	<button data-dojo-type="dijit.form.Button"  name="Edit" id=Edit type="button" data-dojo-props="iconClass:'edit'" >
			<%=resMgr.getText("common.EditMessageText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				openURL("<%=formMgr.getLinkAsUrl( goToMailMessageDetail, urlParm, response )%>");
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("Edit","EditHoverText") %>
<% } %>


<%-- ------------- --%>
<%-- Delete Button --%>
<%-- ------------- --%>
<%-- 		This is a submit button that will call the delete Mediator functionality.	--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the state of the button to disabled. This is done to prevent double click. --%>

<% if( (!isMessageOidNull && hasDeleteRights && !isSettlementReq && !isDiscrepancy ) ||
       (isDiscrepancy && hasDeleteDiscrepRights) || (isSettlementReq && hasDeleteSITRights) ) {
%>
    	<button data-dojo-type="dijit.form.Button"  name="Delete" id="Delete" type="button" data-dojo-props="iconClass:'delete'" >
			<%=resMgr.getText("common.DeleteText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				var confirmDelete = confirm("<%=StringFunction.asciiToUnicode(resMgr.getTextEscapedJS("Messages.confirmDeleteMessage",TradePortalConstants.TEXT_BUNDLE))%>");
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					
				if(confirmDelete==true){
						registry.byId("Delete").set('disabled', true);
						common.setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE%>', '0');
						document.forms[0].submit();
	            }
				return false;
				});
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("Delete","DeleteHoverText") %>
<% } %>

<%-- --------------------- --%>
<%-- Cancel / Close Button --%>
<%-- --------------------- --%>
<%-- 		The buttone text is determined by the cancelFlag variable - otherwise the button always --%>
<%-- 		performs the same action regardless of the text. 					--%>
<%-- 		This is a link that will route the user to how they got to this page in the first place --%>
<%--		by looking at the fromPage variable.							--%>

<%--    	<button data-dojo-type="dijit.form.Button"  name="Close" id="Close" type="button" data-dojo-props="iconClass:'close'" >--%>
<%--			<%//=resMgr.getText(cancelFlag, TradePortalConstants.TEXT_BUNDLE)%>--%>
<%--			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">--%>
<%--				openURL("<%//=formMgr.getLinkAsUrl( fromPage, response )%>");        					--%>
<%--	 		</script>--%>
<%--		</button> --%>
<%
  //cquinton 1/18/2013 replace old close action behavior with page flow logic
  SessionWebBean.PageFlowRef backPage = userSession.peekPageBack();
  String returnAction = backPage.getLinkAction();
  String returnParms = backPage.getLinkParametersString();
  returnParms+="&returning=true";
  String closeLink = formMgr.getLinkAsUrl(returnAction,returnParms,response);
%>
    	<button data-dojo-type="dijit.form.Button" name="CloseButton" id="CloseButton" type="button" data-dojo-props="iconClass:'close'" >
				<%=resMgr.getText("common.CloseText",TradePortalConstants.TEXT_BUNDLE)%>
        <script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
          openURL("<%=closeLink%>");
        </script>
			</button>
      <%=widgetFactory.createHoverHelp("CloseButton","CloseHoverText") %>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%> 
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the state of the button to disabled. This is done to prevent double click. --%>     
<%
	// "Attach a Document" button
	if ((!isReadOnly) &&(SecurityAccess.hasRights(loginRights, SecurityAccess.ATTACH_DOC_MESSAGE))) {
   		session.setAttribute("documentImageFileUploadPageOriginator", formMgr.getCurrPage());
%>
    	<button data-dojo-type="dijit.form.Button"  name="AttachDocuments" id="AttachDocuments" type="button" data-dojo-props="iconClass:'attachDocument'" >
			<%=resMgr.getText("common.AttachADocumentText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
					registry.byId("AttachDocuments").set('disabled', true);
					common.setButtonPressed('AttachDocuments', '0');
					document.forms[0].submit();
				});
			</script>
		</button>
		<%=widgetFactory.createHoverHelp("AttachDocuments","AttachDocumentHoverText") %>
<% } %>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the type from "submit" to "button" to avoid submitting the form twice in IE--%>
<%--PMitnala Rel 8.3 IR#T36000019038 Changing the state of the button to disabled. This is done to prevent double click. --%>
<%
	//"Delete a Document" button
	if ((!isReadOnly) && (SecurityAccess.hasRights(loginRights, SecurityAccess.DELETE_DOC_MESSAGE))) {
	//Added by dillip dt 19-Feb-2013
	if(showDeleteAttachDocButton){
%>
    	<button data-dojo-type="dijit.form.Button"  name="DeleteADocumentButton" id="DeleteADocumentButton" type="button" data-dojo-props="iconClass:'deleteDocument'" >
			<%=resMgr.getText("common.DeleteADocumentText", TradePortalConstants.TEXT_BUNDLE)%>
			<script type="dojo/method" data-dojo-event="onClick" data-dojo-args="evt">
				require(["dijit/registry", "t360/common"],
      				function(registry, common) {
						registry.byId("DeleteADocumentButton").set('disabled',true);
						common.setButtonPressed('<%=TradePortalConstants.BUTTON_DELETE_ATTACHED_DOCUMENTS%>', '0');
					if(confirmDocumentDelete()){
						document.forms[0].submit();
					}  else{
						return false;
					}
});					
	 		</script>
		</button>
		<%=widgetFactory.createHoverHelp("DeleteADocumentButton","DeleteDocumentHoverText") %>
<% }} %>
</ul>

<script language="JavaScript">

 function openURL(URL){
    if (isActive =='Y') {
	    if (URL != '' && URL.indexOf("javascript:") == -1) {
	        var cTime = (new Date()).getTime();
	        URL = URL + "&cTime=" + cTime;
	        URL = URL + "&prevPage=" + context;
        }
    }
	 document.location.href  = URL;
	 return false;
 }

 function confirmDocumentDelete(){
		<%-- Changes done for PortalRefresh IR - T36000004336 - Sandeep Sikhakolli --%>
		<%-- Changes are required as the existing logic does not work for the dojo checkboxes. --%>
		var returnValue;
		require(["dijit/registry"],
			function(registry){
	    		var confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage", TradePortalConstants.TEXT_BUNDLE)%>";


			    <%-- Find any checked checkbox and flip a flag if one is found --%>
			    var isAtLeastOneDocumentChecked = Boolean.FALSE;
			    console.log(document.forms[0].AttachedDocument);
			    if (document.forms[0].AttachedDocument != null) {
			       if (document.forms[0].AttachedDocument.length != null) {
			          for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
			        	  var widgetID = document.forms[0].AttachedDocument[i].value;
			        	  if(registry.byId(widgetID).checked){
			             	isAtLeastOneDocumentChecked = true;
			             }
			          }
			       }
			       else if (registry.byId("AttachedDocument1").checked==true) {
			          isAtLeastOneDocumentChecked = true;
			       }
			    }
			    <%-- Setting the values--%>
			    if (isAtLeastOneDocumentChecked==true) {
				       if (confirm(confirmMessage)==true) {
				    	   if (document.forms[0].AttachedDocument != null) {
				    		     if (document.forms[0].AttachedDocument.length != null) {
					                for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
					                	var widgetID = document.forms[0].AttachedDocument[i].value;
					                	  if(registry.byId(widgetID).checked){
							        		  document.forms[0].AttachedDocument[i].value=registry.byId(widgetID).value;
							        	}else{
							        		document.forms[0].AttachedDocument[i].value="";
							        	}
					                }
					             }
					             else if (registry.byId("AttachedDocument1").checked==true) {
					                document.forms[0].AttachedDocument.value = registry.byId("AttachedDocument1").value;
					             }
					          }
				    	   returnValue = true;
			    }else{
			    	returnValue = false;
			    }
		}else {
			alert("<%=resMgr.getText("common.DeleteAttachedDocumentNoneSelected", TradePortalConstants.TEXT_BUNDLE)%>");
			returnValue = false;
		}

		});
		if (returnValue==true) {
			formSubmitted = true;
		    return true;
		}else{
			formSubmitted = false;
		    return false;
		}
	}
 
 <%-- IR 13682 start- Make sure submit is not triggered twice --%>
 <%--PMitnala Rel 8.3 IR#T36000019038 Changing the state of the button to disabled. This is done to prevent double click. --%>
 function submitReplyToBank(){
		require(["t360/common","dijit/registry"], function(common,registry) {
				registry.byId("ReplyToBank").set('disabled', true);
				common.setButtonPressed('ReplyToBank', '0');
				document.forms[0].submit();
		});
	  }
 <%-- IR 13682 end --%>

 </script>
