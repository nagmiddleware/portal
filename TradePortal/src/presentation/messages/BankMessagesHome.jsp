<%--
*******************************************************************************
                            Bank Messages Home Page

  Description:  The Messages Home page is used for the messages subsystem.  It
                displays the mail and notifications pages.

*******************************************************************************
--%>

<%--
 *
 *     Copyright   2015
 *     CGI, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.dataview.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*,java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>

<%--cquinton 9/20/2012 ir#4556 move Header.jsp here commonly so html page bottom is accurately within body tag--%>
<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>

<jsp:include page="/transactions/TransactionCleanup.jsp"></jsp:include>


<%
   WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   DocumentHandler   xmlDoc             = null;
   Hashtable         secureParms        = new Hashtable();
   String            current2ndNav      = null;
   String            tab1Value          = null;
   String            tab2Value          = null;
   String            userOid            = null;
   StringBuffer      dropdownOptions    = new StringBuffer();
   StringBuffer      extraTags          = new StringBuffer();
   String            defaultValue       = null;
   String            defaultText        = null;
   DocumentHandler   hierarchyDoc       = null;
   int               totalOrganizations = 0;

   String            userOrgOid         = null;
   String            userOrgName        = null;
   String            userSecurityRights = null;
   String            userSecurityType   = null;
   
   String            currentFolder                    = null;
   Map searchQueryMap =  userSession.getSavedSearchQueryData();
   Map savedSearchQueryData =null;

   // Set the current primary navigation
   userSession.setCurrentPrimaryNavigation("NavigationBar.Messages");
   String  loginLocale    = userSession.getUserLocale();
   ReferenceDataManager refData = ReferenceDataManager.getRefDataMgr();

   
   current2ndNav =TradePortalConstants.NAV__MAIL;
   
 
   if (current2ndNav.equals(TradePortalConstants.NAV__MAIL))
   {
      currentFolder = request.getParameter("currentFolder");

      if (currentFolder == null || "".equals(currentFolder.trim()))
      {
         currentFolder = (String) session.getAttribute("currentFolder");

         if (currentFolder == null || "".equals(currentFolder.trim()))
         {
            currentFolder = TradePortalConstants.INBOX_FOLDER;
         }
      }
   }
   String searchNav = "msgHome."+current2ndNav+currentFolder; 
   //IR 20876 -end
%>
<jsp:include page="/common/SearchCriteriaCleanup.jsp">
<jsp:param name="searchCriteriaSection" value="<%=searchNav%>"/>
</jsp:include>

<%
 
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
  }
  else {
    userSession.addPage("goToBankMessagesHome");
  }

  session.setAttribute("startHomePage", "BankMessagesHome");
  session.setAttribute("fromPage", "BankMessagesHome");//IR 31548
  session.removeAttribute("fromTPHomePage");
  
   userSecurityRights = userSession.getSecurityRights();
   userSecurityType   = userSession.getSecurityType();
   userOrgName        = userSession.getOrganizationName();
   userOrgOid         = userSession.getOwnerOrgOid();
   userOid            = userSession.getUserOid();
   
   UserWebBean thisUser = beanMgr.createBean(UserWebBean.class, "User");
   thisUser.setAttribute("user_oid", userOid);
   thisUser.getDataFromAppServer();
  
    if(searchQueryMap!=null && !searchQueryMap.isEmpty()){
       	savedSearchQueryData = (Map)searchQueryMap.get("BankMailMessagesDataView");
       }
%>
 <%@ include file="fragments/BankMessagesMail.frag" %>
<%
  
   xmlDoc = formMgr.getFromDocCache();

   xmlDoc.removeAllChildren("/");

   formMgr.storeInDocCache("default.doc", xmlDoc);

   // Store the current secondary navigation  in the session.
   session.setAttribute("messages2ndNav",current2ndNav);
   
%>

<%-- ********************* JavaScript for page begins here *********************  --%>

<script>

  function confirmDelete() {
    var   confirmMessage = "<%=resMgr.getText("Mail.PopupMessage",
                                              TradePortalConstants.TEXT_BUNDLE) %>";

            if (!confirm(confirmMessage))
             {
                formSubmitted = false;
                return false;
             } else {
       return true;
    }
  }



</script>

</body>
</html>
