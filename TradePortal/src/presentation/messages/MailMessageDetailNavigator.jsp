<%--
*******************************************************************************
                       Mail Message Detail Navigator Page

  Description:
	This Page is only used to redirect the user to the page they came 
  from based on the variable stored on the session called 'fromPage'.  This
  was put onto the session because this is not the only jsp that needed to 
  know how the user got to the Message Detail.jsp.  Since the user could have 
  come from one of 3 places, this page was required to determine where thay 
  came from and send them to one of those places.

*******************************************************************************
--%>

<%--
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.html.*" %>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<%-- ************** Message Navigator : Redirecting Page ****************  --%>

<%
Debug.debug("-----START------------------ Inside Message Navigator.jsp --------------------START----");

String fromPage = (String) session.getAttribute("fromPage");

  if( fromPage == null)   fromPage = "";


  if( fromPage.equals("MessagesHome") ) {

	formMgr.setCurrPage("MessagesHome");
%>
     	<jsp:forward page='<%=NavigationManager.getNavMan().getPhysicalPage("MessagesHome", request)%>' />

<%

  }else if( fromPage.equals("InstrumentSummary") ) {

	formMgr.setCurrPage("InstrumentSummary");
%>

<%--    IR T36000011247 Made changes for sent to bank navigation
        @ Komal M Start 06/02/2013
 --%>
   
     	<jsp:forward page='<%=NavigationManager.getNavMan().getPhysicalPage("InstrumentSummary", request)%>'>
     		<jsp:param name="returning" value="true" />
     	</jsp:forward>

<%
  }else if( fromPage.equals("TradePortalHome") ) {

	formMgr.setCurrPage("TradePortalHome");
%>
     	<jsp:forward page='<%=NavigationManager.getNavMan().getPhysicalPage("TradePortalHome", request)%>' />

<%

  }else{			//We should never get here.
Debug.debug("---------- Made it into the Navigators default page, dont know where to go... -----------");
	formMgr.setCurrPage("MessagesHome");
%>
     	<jsp:forward page='<%=NavigationManager.getNavMan().getPhysicalPage("MessagesHome", request)%>' />

<%

}


Debug.debug("-------END------------------------- Message Navigator.jsp ----------------------------END----");

%>

</body>
</html>


