<%--
*******************************************************************************
                              Mail Message Page

  Description:
    This page is designed to display the dynamic Mail Message page.  The page
  is dynamic because it can be displayed in one of 9 different views.  In some
  views the buttons (like 'Reply to Bank'), the button is a submit button,
  in other views the button is a link.  Pay close attention to that, because
  it will affect whether or not a mediator is called.  For example, the Route
  Button sometimes will call the RouteDeleteMediator and other times it will
  call the MailMessage Mediator.  Generally there are 2 modes the Mail Message
  Mediator is called in =>
  Message Mode:  'C' = 'Create' a new message.
             'U' = 'Update' an existing message.
  This informs the mediator whether or not we are updating a preExisting message.

    This Particular jsp is creating an ejb instead of creating a webBean.
  This was intentional because the un_read flag needs to be updated prior to
  leaving the page when a message is read for the first time.  The only way to
  accomplish this is to use an ejb rather than a webBean.

  The basic views this page be displayed in are as follows:

  View                        Condition                     Read Only Status (in general)
  ---------------------------------------------------------------------------------------------------
  New Mail Message            Message oid passed in is null       False
                        isReply flag == false
                        isEditFlage == false

  Draft Mail Message          Message oid was passed in           False
                        MessageStatus = 'DRAFT'
                        isEdit = 'true' OR Message is assigned to you
                              OR Message is assgined to no-One && is assigned to your corporate Org

  Mail Message Rec From Bank  Message oid was passed in           True
                        Message was assigned to someone else (&& you have viewing rights)

  Mail Message Sent To Bank   Message oid was passed in           True
                        Message Status is 'SENT_TO_BANK'
                        isReply flag is not true

  MailMessage Rec From Bank   Message oid was passed in           True
                        Message Status is 'REC'
                        Discrepancy Flag is set to 'N' or ''
*******************************************************************************
--%>

<%--
 *     Dev Owner: Rakesh
 *     Copyright   2001
 *     American Management Systems, Incorporated
 *     All rights reserved
--%>
<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*,com.ams.tradeportal.busobj.*,
                 com.ams.tradeportal.html.*, java.text.*,java.util.*" %>

<jsp:useBean id="formMgr"     class="com.amsinc.ecsg.web.FormManager"            scope="session"></jsp:useBean>
<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean" scope="session"></jsp:useBean>
<jsp:useBean id="resMgr"      class="com.amsinc.ecsg.util.ResourceManager"       scope="session"></jsp:useBean>
<jsp:useBean id="beanMgr"     class="com.amsinc.ecsg.web.BeanManager"          scope="session"></jsp:useBean>

<%
      WidgetFactory widgetFactory = new WidgetFactory(resMgr);
   Debug.debug("***START******************** Mail Message Page *********************START***");
	//Variables to ensure Subject and InstrumentId is displayed as desired.DPatra 01-DEC-2012
	boolean isSubjOrInstrIdReadOnly=true;
	boolean isSubjOrInstrIdReq=false;

  //URL Parameter variables
  String    reply                   = request.getParameter("isReply");        //Reply button pressed
  String    edit                    = request.getParameter("isEdit");         //Edit button pressed
  String    relatedInstID           = request.getParameter("relatedInstID");
  String    subject                 = request.getParameter("subject");
  String    mailMessageOid          = request.getParameter("mailMessageOid");
  String    instrumentId          	= request.getParameter("instrumentOid");
  String    custName          		= request.getParameter("customerName");
  String    custOid          		= request.getParameter("customerOid");
  String    bankInId				= request.getParameter("bankInstId");
  String instrumentSearchDialogTitle=resMgr.getText("InstSearch.HeadingGeneric",TradePortalConstants.TEXT_BUNDLE) +" - "+resMgr.getText("BankMail.Customer",TradePortalConstants.TEXT_BUNDLE)+" - ";
  //cquinton 1/18/2013 remove old close action logic


  String    loginLocale             = userSession.getUserLocale();
  String    loginTimezone           = userSession.getTimeZone();
  String    loginRights             = userSession.getSecurityRights();
  String    userOid                 = userSession.getUserOid();
  String    corpOrgOid        = userSession.getOwnerOrgOid();
  String    clientBankOid           = userSession.getClientBankOid();
  String    goToBankMailMessageDetail   = "goToBankMailMessageDetail";
  Long            messageOid        = null;
  String    messageMode       = TradePortalConstants.MESSAGE_CREATE;
  
  String    instrumentOid="";
  String    instrumentType="";
  String    instrumentTypeCode="";
  //CR 375-D Krishna ATP-Response
  String    transactionStatus = "";
  String    completeInstId          = "";
  String    bankInstId          = "";
  String    customer          = "";
  String    displayInstrumentID     = "";
  String 	selectedOption = " ";
  int       stringLength            = 0;
  boolean       displayDiscrepancyReplyToBankButton = false;
  TermsPartyWebBean    counterParty  = null;
  String    counterPartyOid          = null;
  String    counterPartyName         = null;
  DocumentHandler defaultDoc        = formMgr.getFromDocCache();
  Hashtable       secureParms       = new Hashtable();
  ClientServerDataBridge csdb             = resMgr.getCSDB();

  String    serverLocation          = JPylonProperties.getInstance().getString("serverLocation");

  BankMailMessage     message;          //Business object - needed to do this since we needed to be able
                              //to update the un_read flag in the db with-in the jsp.
  InstrumentWebBean instrument            = beanMgr.createBean(InstrumentWebBean.class, "Instrument");
  UserWebBean     routedByUser            = beanMgr.createBean(UserWebBean.class, "User");
  TransactionWebBean transaction    = beanMgr.createBean(TransactionWebBean.class, "Transaction");


  String    messageStatus           = "";
  String    messageText       = "";
 
  String    date;
  

  DocumentHandler docImageList = null;          //used to display a list of document Image
                                    //links if the handler is non null.
  DocumentHandler myDoc;
  Vector listviewVector = null;                 //list of returned records formatted for easy looping.

  if( relatedInstID == null )       relatedInstID     = "";
  if( subject == null )       subject           = "";
  if( mailMessageOid == null )  mailMessageOid  = "";
  if(custName == null) custName = "";
  if(custOid == null)  custOid = "";
  if(bankInId == null) bankInId = "";
  String    fromPage          = (String) session.getAttribute("fromPage");
  if(fromPage == null) {
	  session.setAttribute("fromPage", "BankMessagesHome");
  }
   
  if( InstrumentServices.isBlank(mailMessageOid) ){
      mailMessageOid = (String) session.getAttribute("mailMessageOid");
      

      if( InstrumentServices.isBlank( mailMessageOid ) ) {
            mailMessageOid    = defaultDoc.getAttribute("/In/BankMailMessage/bank_mail_message_oid");
          
            if( InstrumentServices.isBlank( mailMessageOid ) ){
                  mailMessageOid    = defaultDoc.getAttribute("/In/mailMessageOid");
                       if( InstrumentServices.isBlank( mailMessageOid ) )
                                    mailMessageOid = "";
            }
      }else{
            HttpSession theSession    = request.getSession(false);
            theSession.removeAttribute("mailMessageOid");
      }

  }else{
      mailMessageOid = EncryptDecrypt.decryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
    
  }

  if( InstrumentServices.isNotBlank( mailMessageOid ) )
  {
      messageOid  = new Long(mailMessageOid);

      message      = (BankMailMessage)EJBObjectFactory.createClientEJB( serverLocation,
                                                      "BankMailMessage", messageOid.longValue(), csdb);

      messageStatus      = StringFunction.xssCharsToHtml(message.getAttribute("message_status"));
      subject      = StringFunction.xssCharsToHtml(message.getAttribute("message_subject"));
      messageText = StringFunction.xssCharsToHtml(message.getAttribute("message_text"));
    
           messageMode = TradePortalConstants.MESSAGE_UPDATE;
      
  }else{
      message      = (BankMailMessage)EJBObjectFactory.createClientEJB( serverLocation,
                                                                    "BankMailMessage", csdb);
      message.newObject();
      messageOid  = new Long( message.getAttribute("bank_mail_message_oid") );

  }

  try{
      if( InstrumentServices.isNotBlank( mailMessageOid ) ) {
    	
          String sql = "select doc_image_oid, form_type, image_id, image_format, doc_name, hash "+
                 "from document_image where mail_message_oid = ? "+ " and form_type <> ?";
                 docImageList = DatabaseQueryBean.getXmlResultSet(sql, false, mailMessageOid, TradePortalConstants.DOC_IMG_FORM_TYPE_PORTAL_ATTACHED);

         if( docImageList != null ) {
            Debug.debug("*** Returned Resultset is: " + docImageList.toString() );

            listviewVector = docImageList.getFragments("/ResultSetRecord/");
         }
      }
  }catch(Exception e){
      Debug.debug("******* Error in calling DatabaseQueryBean: Related to the Document Image table *******");
     e.printStackTrace();
  }



  
  boolean   isMessageOidNull  = InstrumentServices.isBlank(mailMessageOid);

  boolean   isNewReply;

  if(reply == null)
   {
      if(defaultDoc.getAttribute("/In/isNewReply") != null)
            isNewReply=true;
      else
            isNewReply=false;
   }
  else
   {
      isNewReply = reply.equals("true");
   }


  boolean   isEdit;

  if(edit == null)
   {
      if(defaultDoc.getAttribute("/In/isEdit") != null)
            isEdit=true;
      else
            isEdit=false;
   }
  else
   {
      isEdit = edit.equals("true");
   }

  boolean   isCurrentUser           = message.getAttribute("assigned_to_user_oid").equals(userOid);
  boolean   hasEditRights           = SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_CREATE_REPLY);
  boolean   hasSendRights           = SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_SEND_TO_CUST);
  boolean   hasDelDocMsg            = SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_DELETE_DOC);
  boolean   hasDeleteRights         = SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_DELETE);
  
  boolean   isUnAssigned            = InstrumentServices.isBlank(message.getAttribute("assigned_to_user_oid"));
  boolean   isUsersOrgOid;
  boolean   isStatus          = InstrumentServices.isBlank( messageStatus );
  boolean   messageIsReply;
  boolean   isMsgFromCust;                                                  //message.Message_Source_Type
  boolean   isRelatedInst           = InstrumentServices.isBlank( relatedInstID );      //is : Related Inst passed in
  boolean   isReadOnly        = true;
  boolean       messageCanSendToCust    = false;
  
  MediatorServices mediatorServices = null;                                       //Populate Error section
  String    messageSourceType = message.getAttribute("message_source_type");
  String    headerPrefix            = "";
  String    headerSuffix            = "";
  String    headerTitle             = "";
  String    headerSuffixDate        = "";
                              //Either equals: "Close", "Cancel New Repsonse", or "Cancel New Message"
  String    cancelFlag        = "common.CloseText";
  String    cancelImg         = "common.CloseImg";
  String    cancelWidth       = "";

  userSession.setCurrentPrimaryNavigation("NavigationBar.Messages");
 
  boolean returning = false;
  if ( "true".equals( request.getParameter("returning") ) ) {
    userSession.pageBack(); //to keep it correct
    returning = true;
  }
  else {
    
    userSession.addPage("goToBankMailMessageDetail", request);
  }


  if( message.isDeleted() ) {
      Debug.debug("***** Message is deleted - Can't Edit it ****** ");
      isEdit = false;
      mediatorServices = new MediatorServices();
        mediatorServices.getErrorManager().setLocaleName(resMgr.getResourceLocale());
      mediatorServices.getErrorManager().issueError(TradePortalConstants.ERR_CAT_1,
                                                    TradePortalConstants.DELETED_BY_ANOTHER_USER);
      mediatorServices.addErrorInfo();

      defaultDoc.setComponent("/Error", mediatorServices.getErrorDoc());
      formMgr.storeInDocCache("default.doc", defaultDoc);
      }
  if (InstrumentServices.isBlank( instrumentOid ) ){
	  SessionWebBean.PageFlowRef previousPage = userSession.peekPageBack();
	  String linkAction = previousPage.getLinkAction();
	  if ("goToInstrumentSummary".equals(linkAction))
		  instrumentOid = (String)session.getAttribute(TradePortalConstants.INSTRUMENT_SUMMARY_OID);
  }

  if ( isNewReply && InstrumentServices.isNotBlank(relatedInstID) ) {
     
	  displayInstrumentID = relatedInstID;
	  completeInstId = relatedInstID;
	 
  }else{
	 
	  completeInstId = message.getAttribute("complete_instrument_id");
	     displayInstrumentID = completeInstId;
	      }
  if(isNewReply && StringFunction.isNotBlank(bankInId)){
	  bankInstId = bankInId;
	  
  }
  else{
	  
	  bankInstId = message.getAttribute("bank_instrument_id"); 
	  
  }if(isNewReply && StringFunction.isNotBlank(custName)){
	  selectedOption = custOid;
  }
   counterPartyOid = instrument.getAttribute("a_counter_party_oid");
   if (!InstrumentServices.isBlank(counterPartyOid))
   {
      counterParty = beanMgr.createBean(TermsPartyWebBean.class, "TermsParty");

      counterParty.getById(counterPartyOid);

      counterPartyName = counterParty.getAttribute("name");
   }
   else
   {
      counterPartyName = "";
   }
   if( StringFunction.isNotBlank(defaultDoc.toString()) && !isNewReply){

	   if( defaultDoc.getAttribute("/In/BankMailMessage/message_subject") != null )
	   {
	       subject = defaultDoc.getAttribute("/In/BankMailMessage/message_subject");
	   }

	   if( defaultDoc.getAttribute("/In/BankMailMessage/message_text") != null )
	    {
	       messageText = defaultDoc.getAttribute("/In/BankMailMessage/message_text");
	    }
	   if( defaultDoc.getAttribute("/In/Instrument/complete_instrument_id") != null )
	   {
	 	  completeInstId  = defaultDoc.getAttribute("/In/Instrument/complete_instrument_id");
	 	  displayInstrumentID = completeInstId;
	   }
	   if( defaultDoc.getAttribute("/In/MailMessage/bank_instrument_id") != null )
	    {
	 	  bankInstId = defaultDoc.getAttribute("/In/MailMessage/bank_instrument_id");
	    }
	  
	   if( StringFunction.isNotBlank(defaultDoc.getAttribute("/In/BankMailMessage/assigned_to_corp_org_oid")) )
	   {
	 	  selectedOption = defaultDoc.getAttribute("/In/BankMailMessage/assigned_to_corp_org_oid");
	 	
	   }else{
	 	  selectedOption = " "; 
	   }
	   
	   }
  
 //SSikhakolli - Rel-9.3.5 CR-1029 IR# T36000043404 - Benig
   //if isNewReply and page submit got error out then read from cache and populate data
   String maxError = defaultDoc.getAttribute("/Error/maxerrorseverity");
   if (isNewReply && maxError != null && maxError.compareTo(String.valueOf(ErrorManager.ERROR_SEVERITY))>=0) {
	   if( defaultDoc.getAttribute("/In/BankMailMessage/message_subject") != null ){
	       subject = defaultDoc.getAttribute("/In/BankMailMessage/message_subject");
	   }
	   if( defaultDoc.getAttribute("/In/BankMailMessage/message_text") != null ){
	       messageText = defaultDoc.getAttribute("/In/BankMailMessage/message_text");
	   }
	   if( defaultDoc.getAttribute("/In/Instrument/complete_instrument_id") != null ){
	 	  completeInstId  = defaultDoc.getAttribute("/In/Instrument/complete_instrument_id");
	 	  displayInstrumentID = completeInstId;
	   }
	   if( defaultDoc.getAttribute("/In/MailMessage/bank_instrument_id") != null ){
	 	  bankInstId = defaultDoc.getAttribute("/In/MailMessage/bank_instrument_id");
	   }
	  
	   if( StringFunction.isNotBlank(defaultDoc.getAttribute("/In/BankMailMessage/assigned_to_corp_org_oid")) ){
	 	  selectedOption = defaultDoc.getAttribute("/In/BankMailMessage/assigned_to_corp_org_oid");
	   }else{
	 	  selectedOption = " "; 
	   }
   }
 //SSikhakolli - Rel-9.3.5 CR-1029 IR# T36000043404 - End
 
   if( StringFunction.isBlank(message.getAttribute("is_reply")) ||
      message.getAttribute("is_reply").equals(TradePortalConstants.INDICATOR_NO ) ) {
                        messageIsReply = false;
  }else{                messageIsReply = true;
                        messageMode = TradePortalConstants.MESSAGE_UPDATE;
  }

  if( StringFunction.isBlank(messageSourceType) ||
      messageSourceType.equals(TradePortalConstants.BANK) )
                        isMsgFromCust = false;
  else                        isMsgFromCust = true;

  if( StringFunction.isBlank(message.getAttribute("assigned_to_corp_org_oid")) ||
      corpOrgOid.equals(message.getAttribute("assigned_to_corp_org_oid")) )
                        isUsersOrgOid = false;
 
  else                        isUsersOrgOid = true;


  CorporateOrganizationWebBean corpOrg  = beanMgr.createBean(CorporateOrganizationWebBean.class, "CorporateOrganization");
  corpOrg.getById(message.getAttribute("assigned_to_corp_org_oid"));
  
  if(StringFunction.isNotBlank(completeInstId)){
  java.util.List<Object> instSqlParamsLst = new java.util.ArrayList();					
  String sqlInstQuery = "SELECT INSTRUMENT_OID FROM instrument WHERE COMPLETE_INSTRUMENT_ID = ?  ";
  instSqlParamsLst.add(completeInstId);
  DocumentHandler results = DatabaseQueryBean.getXmlResultSet(sqlInstQuery, false, instSqlParamsLst);
 

  if(results != null)
   {
      instrumentOid = results.getAttribute("/ResultSetRecord(0)/INSTRUMENT_OID");
   }
  }
  
  String customerName = corpOrg.getAttribute("name");
  String customerOid =  corpOrg.getAttribute("organization_oid");

  if(!SecurityAccess.hasRights(userSession.getSecurityRights(), SecurityAccess.BANK_MAIL_CREATE_REPLY) ||
     messageStatus.equals(TradePortalConstants.REC) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_CUST) ||
     messageStatus.equals(TradePortalConstants.REC_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_ASSIGNED)) {

       isReadOnly = true;                               
       
  }else if( isCurrentUser || ( isUsersOrgOid) ||
          isEdit || isMessageOidNull) {

       isReadOnly = false;
  }

  if(messageSourceType.equals( TradePortalConstants.PORTAL ) ||
     messageStatus.equals(TradePortalConstants.REC) ||
     messageStatus.equals(TradePortalConstants.SENT_TO_CUST) ||
     messageStatus.equals(TradePortalConstants.REC_DELETED) ||
     messageStatus.equals(TradePortalConstants.REC_ASSIGNED)) {

            messageCanSendToCust = false;

  }else if( isCurrentUser || (isUnAssigned && isUsersOrgOid) ||
          isEdit || isMessageOidNull || (messageStatus.equals(TradePortalConstants.DRAFT) && !isReadOnly ) ) {

            messageCanSendToCust = true;
  }
  secureParms.put( "bank_mail_message_oid",mailMessageOid);
  secureParms.put( "instrumentOid", instrumentOid + "/");
  secureParms.put( "mode", TradePortalConstants.EXISTING_INSTR );
  secureParms.put( "clientBankOid", clientBankOid );
  secureParms.put( "userOid", userOid );
  secureParms.put( "ownerOrg", corpOrgOid );
  secureParms.put( "securityRights", loginRights );
  secureParms.put( "mailMessageOid", mailMessageOid );
  secureParms.put( "startPage", "BankMailMessageDetail" );
 
 
  //rkrishna CR 375-D ATP 07/26/2007 Begin
  if(instrumentTypeCode.equals(InstrumentType.APPROVAL_TO_PAY))
  secureParms.put( "transactionType", TransactionType.APPROVAL_TO_PAY_RESPONSE );
  else
  //rkrishna CR 375-D ATP 07/26/2007 End
  secureParms.put( "transactionType", TransactionType.DISCREPANCY );
  secureParms.put( "validationState", TradePortalConstants.VALIDATE_NOTHING );
  secureParms.put( "MessageMode", messageMode);
  secureParms.put( "timeZone", loginTimezone);
  secureParms.put( "user_oid", userOid);
  secureParms.put( "mailMessageOid", mailMessageOid);
  secureParms.put( "complete_instrument_id", completeInstId);

  secureParms.put( "instrument_type_code", instrumentTypeCode);
  secureParms.put( "fromListView", TradePortalConstants.INDICATOR_NO);


  String    urlParm;
  String    attribute;
  String    displayText;
  String    encryptedMailMessageOid;
  String    encryptVal2;
  String    encryptVal3;

  //cquinton 9/15/2011 Rel 7.1 ppx240
  encryptedMailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
  secureParms.put("MessageOid", encryptedMailMessageOid);
  secureParms.put("fromMessages", TradePortalConstants.INDICATOR_YES);
  secureParms.put("SecurityRights", loginRights);
  secureParms.put("UserOid", userOid);



  if( isNewReply ) {
       secureParms.put( "is_reply", TradePortalConstants.INDICATOR_YES);
       secureParms.put( "bankmessage_customer", message.getAttribute("assigned_to_corp_org_oid"));
  }else{
       secureParms.put( "is_reply", TradePortalConstants.INDICATOR_NO);
       secureParms.put( "bankmessage_customer", corpOrgOid);
  }
 
	String newSubject="";
	int lastIndex=0;
  	lastIndex= subject.indexOf(":");
 	if(lastIndex != -1) {
		newSubject = subject.substring(0, lastIndex);
  	}
  	else {
  		newSubject = subject;
  	}
%>

<%-- ********************************** HTML STARTS HERE **************************************** --%>

<jsp:include page="/common/Header.jsp">
   <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
   <jsp:param name="includeErrorSectionFlag"  value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>
<jsp:include page="/common/ButtonPrep.jsp" />
<script language="JavaScript">

   function confirmDocumentDelete()
   {
      var   confirmMessage = "<%=resMgr.getText("PendingTransactions.PopupMessage",TradePortalConstants.TEXT_BUNDLE) %>";
      var isAtLeastOneDocumentChecked = Boolean.FALSE;
      if (document.forms[0].AttachedDocument != null) {
         if (document.forms[0].AttachedDocument.length != null) {
            for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
               if (document.forms[0].AttachedDocument[i].checked==true) {
                  isAtLeastOneDocumentChecked = true;
               }
            }
         }
         else if (document.forms[0].AttachedDocument.checked==true) {
            isAtLeastOneDocumentChecked = true;
         }
      }

      if (isAtLeastOneDocumentChecked==true) {
         if (confirm(confirmMessage)==false)
         {
            if (document.forms[0].AttachedDocument != null) {
               if (document.forms[0].AttachedDocument.length != null) {
                  for (i=0;i<document.forms[0].AttachedDocument.length;i++) {
                     if (document.forms[0].AttachedDocument[i].checked==true) {
                     document.forms[0].AttachedDocument[i].checked = false;
                     }
                  }
               }
               else if (document.forms[0].AttachedDocument.checked==true) {
                  document.forms[0].AttachedDocument.checked = false;
               }
            }
            formSubmitted = false;
            return false;
         }
         else
         {
             formSubmitted = true;
             return true;
         }
      }
      else {
        
         alert("<%=resMgr.getText("common.DeleteAttachedDocumentNoneSelected", TradePortalConstants.TEXT_BUNDLE) %>");
         formSubmitted = false;
         return false;
      }
  }


</script>

      <div class="pageMain">
            <div class="pageContent">
                  <form id="BankMessagesDetail" name="BankMessagesDetail" method="post" data-dojo-type="dijit.form.Form" action="<%=formMgr.getSubmitAction(response)%>">
                        <input type="hidden" name="buttonName" value="">

    <%
     /***************************
      * Title Bar
      **************************/
      Debug.debug("*** Title Bar ***");
    %>

     
		<%
            String helpUrl = "/customer/mail_message.htm";
	    	String tracer = resMgr.getText("Message.CreateTracers", TradePortalConstants.TEXT_BUNDLE);
    	    String correspondence = resMgr.getText("Message.Correspondence", TradePortalConstants.TEXT_BUNDLE);

			  if( isMessageOidNull && !isNewReply ) {
            	  helpUrl = "/customer/new_mail_message.htm";
                  headerPrefix = resMgr.getText("Message.NewMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                  headerTitle = resMgr.getText("Message.NewMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                  cancelFlag = "common.CancelNewMessageText";
                  cancelImg  = "common.CancelNewMessageImg";
                  cancelWidth = "140";
				  isSubjOrInstrIdReadOnly=false;
				  isSubjOrInstrIdReq=true;

            }else if( !isMessageOidNull && (messageStatus.equals(TradePortalConstants.DRAFT)) && !messageIsReply )
            {
                 headerPrefix = resMgr.getText("Message.DraftMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
                 headerTitle = resMgr.getText("Message.DraftMailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
				  isSubjOrInstrIdReadOnly=false;
				  isSubjOrInstrIdReq=true;

            }else if( isMessageOidNull && isNewReply ) {
                        headerPrefix = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
                        cancelFlag = "common.CancelNewResponseText";
                        cancelImg  = "common.CancelNewResponseImg";
                        cancelWidth = "140";

                        date = DateTimeUtility.getCurrentDateTime();
                        SimpleDateFormat isoFormat= new SimpleDateFormat("dd MMM yyyy hh:mm aa");
                        Date convertedDate = TPDateTimeUtility.getLocalDateTime( date, loginTimezone );
                  date = TPDateTimeUtility.formatDateTime( isoFormat.format(convertedDate), "1", loginLocale);
                  headerSuffixDate = date;
            }else if( (messageStatus.equals(TradePortalConstants.DRAFT)) && !isMessageOidNull && messageIsReply) {
                        headerPrefix = resMgr.getText("Message.DraftResponseHeader", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.DraftResponseHeader", TradePortalConstants.TEXT_BUNDLE);
            }else if( messageIsReply ) {
                        headerPrefix = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
                        headerTitle = resMgr.getText("Message.ResponseToMailMessage", TradePortalConstants.TEXT_BUNDLE);
            }else{
                  headerPrefix = resMgr.getText("Message.MailMessageTitle", TradePortalConstants.TEXT_BUNDLE); 
                  //headerPrefix = newSubject;
                  headerTitle = resMgr.getText("Message.MailMessageHeader", TradePortalConstants.TEXT_BUNDLE);
            }

            if( isMsgFromCust ) {
            	headerSuffix = resMgr.getText("BankMessage.ReceivedFromCust", TradePortalConstants.TEXT_BUNDLE) + " - "+customerName;

           		isReadOnly = true;
           	 	date = message.getAttribute("last_update_date");
                SimpleDateFormat isoFormat= new SimpleDateFormat("dd MMM yyyy");
                Date convertedDate = TPDateTimeUtility.getLocalDateTime( date, loginTimezone );
         		date = TPDateTimeUtility.formatDateTime( isoFormat.format(convertedDate), "1", loginLocale);
                headerSuffixDate = date;
            }else if( messageStatus.equals(TradePortalConstants.SENT_TO_CUST) ) {
    			helpUrl = "/customer/sent_to_bank_detail.htm";
                headerSuffix = resMgr.getText("BankMail.SendToCustomerMessage", TradePortalConstants.TEXT_BUNDLE);
                headerPrefix = "";
                
                SimpleDateFormat isoFormat= new SimpleDateFormat("dd MMM yyyy hh:mm aaa");//Ex: 25 Sep 2015 09:12 PM
                Date lastUpdateDate = TPDateTimeUtility.getLocalDateTime( message.getAttribute("last_update_date"), loginTimezone );
                headerSuffixDate = TPDateTimeUtility.formatDateTime( isoFormat.format(lastUpdateDate), "1", loginLocale) + ":";
            }else{
                headerSuffix = "";
            }
            
      %>
      					 <jsp:include page="/common/PageHeader.jsp">
                           <jsp:param name="titleKey" value="<%=headerTitle%>" />
                           <jsp:param name="helpUrl"  value="<%=helpUrl%>" />
                         </jsp:include>

                        <div class="subHeaderDivider"></div>

                        <div class="pageSubHeader">
                          <span class="pageSubHeaderItem">
                              <% if(!headerPrefix.equals("")){ %>
                                    <span><%=headerPrefix%></span>
                              <% } if(!headerSuffix.equals("")){ %>
                                    <% if(!headerPrefix.equals("")){ %>
                                          <span>&nbsp;-&nbsp;</span>
                                    <% } %>
                                    <span><%=headerSuffix%></span>
                              <% } if(!headerSuffixDate.equals("")){ %>
                                    <span>&nbsp;-&nbsp;</span>
                                    <span><%=headerSuffixDate%></span>
                              <% } 
                            
                              %>
                          </span>

        		 <div style="clear:both;"></div>
                        </div>

                  <div class="formArea">
                        <jsp:include page="/common/ErrorSection.jsp" />

                 
                        <div class="formContent formBorder">
                              <br/>
                        <%  
                        
                        secureParms.put("bankmessage_customer",selectedOption);
                        String ownershipLevel = userSession.getOwnershipLevel();
                        StringBuffer sqlQuery = new StringBuffer();
                        java.util.List<Object> custSqlParamsLst = new java.util.ArrayList();
							sqlQuery.append("SELECT DISTINCT a.organization_oid, a.name");
							sqlQuery.append(" FROM corporate_org a,bank_organization_group b"); 
							sqlQuery.append(" WHERE a.activation_status = ? ");
							sqlQuery.append(" AND a.cust_is_not_intg_tps = ? ");
							sqlQuery.append(" AND a.a_bank_org_group_oid = b.organization_oid ");
							custSqlParamsLst.add(TradePortalConstants.ACTIVE);
							custSqlParamsLst.add(TradePortalConstants.INDICATOR_YES);
						 	if (StringFunction.isNotBlank(userSession.getBankGrpRestrictRuleOid() )) {
                                sqlQuery.append(" and b.organization_oid not in ( ");
                                sqlQuery.append("select BANK_ORGANIZATION_GROUP_OID from BANK_GRP_FOR_RESTRICT_RULES where P_BANK_GRP_RESTRICT_RULES_OID= ? ");
                                custSqlParamsLst.add(userSession.getBankGrpRestrictRuleOid());
                                sqlQuery.append(") ");
                         }
				   if ( TradePortalConstants.OWNER_BOG.equals(ownershipLevel) ) {
				          	sqlQuery.append(" AND a.a_bank_org_group_oid = ? ");
				          	custSqlParamsLst.add(userSession.getOwnerOrgOid());
				   } else {
				          	sqlQuery.append(" AND a.a_client_bank_oid  = ? "); 
				          	 custSqlParamsLst.add(userSession.getOwnerOrgOid());
				         
				   }
						   sqlQuery.append(" order by ");
						   sqlQuery.append(resMgr.localizeOrderBy("a.name"));

						   	DocumentHandler corpCustomersDoc  = DatabaseQueryBean.getXmlResultSet( sqlQuery.toString(), false,custSqlParamsLst);
							StringBuffer      customerOptions    = new StringBuffer();
							 
							if(!isMessageOidNull){
	                        	
	                        	selectedOption =  corpOrg.getAttribute("organization_oid");
	                        	
	                        }
							if(messageStatus.equals(TradePortalConstants.DRAFT) && !isMessageOidNull && messageIsReply){
							   		secureParms.put( "bankmessage_customer", selectedOption);
							}
							
							customerOptions.append(Dropdown.createSortedOptions(corpCustomersDoc, "ORGANIZATION_OID",  "NAME",selectedOption, resMgr.getResourceLocale()));
							
                        if((!isMessageOidNull && (messageStatus.equals(TradePortalConstants.DRAFT)) && !messageIsReply) || messageStatus.equals(TradePortalConstants.SENT_TO_CUST)||isMessageOidNull||messageStatus.equals(TradePortalConstants.DRAFT)){
                        	   
                        String bankMessageCustomer = "";
                        if(isNewReply) {
                        	bankMessageCustomer = "BankMail.ResponseToCustomer";
                        }else if(messageStatus.equals(TradePortalConstants.SENT_TO_CUST)){
                        	bankMessageCustomer = "BankMail.SentToCustomer";	
                        }else{
                        	bankMessageCustomer = "common.SendToCustText";
                        }
                        
                        
                        %>
                        
						
							<%=widgetFactory.createSelectField("bankmessage_customer",bankMessageCustomer, " ", customerOptions.toString(), isSubjOrInstrIdReadOnly,
               					isSubjOrInstrIdReq, false, "style='width:280px;'onChange ='reSetInst();'", "", "")%> 
               				
               				
               				<%}
                        
                                    if( messageStatus.equals(TradePortalConstants.SENT_TO_CUST) || (isMessageOidNull && !isNewReply)){
                                          if( isNewReply ){
                                          String re = resMgr.getText("Message.Re", TradePortalConstants.TEXT_BUNDLE);
                                          if(!subject.startsWith(re)){
                                                subject = re + "  " + StringFunction.escapeQuotesforJS(subject);
                                          }
                                          if(subject.length() > 60){
                                          subject = StringFunction.escapeQuotesforJS(subject.substring(0, 56)) + "...";
                                          }

                                                secureParms.put("message_subject", subject);
                                          }
                                          if( messageIsReply || isReadOnly ){
                                               
                                                secureParms.put("message_subject", subject);
                                              
                                          }

                                    }

								
                                   
		 %>				<%=widgetFactory.createTextField("message_subject", "Message.Subject", StringFunction.xssCharsToHtml(subject), "60", isSubjOrInstrIdReadOnly,
               					isSubjOrInstrIdReq, false, "style='width:420px;'", "", "")%>

                           
                              <div>
                                    <%
                                          String ItemID1 = "complete_instrument_id,bank_instrument_id";
                                          String section1="BankInstrumentId";
                                          String instrId = "";
                                          String instruButton = "";
					  
                                          instruButton = widgetFactory.createInstrumentSearchButton(ItemID1,section1,isReadOnly,TradePortalConstants.SEARCH_ALL_INSTRUMENTS_FOR_MESSAGES, false);
                                          if( isReadOnly ){
                                        	  StringBuffer addlParms = new StringBuffer();
                                              addlParms.append("&instrument_oid=");
                                              addlParms.append(EncryptDecrypt.encryptStringUsingTripleDes(instrumentOid, userSession.getSecretKey()));
                                              addlParms.append("&bankMailMessageOid=");
												if(encryptedMailMessageOid!=null){
													addlParms.append(encryptedMailMessageOid);
												}
                                              String instrLink = formMgr.getLinkAsHref("goToInstrumentSummary", displayInstrumentID, addlParms.toString(), response);

                                          
                                                instrId = "Message.RelatedToInstrumentID";

                                    %>

                                         	<%=widgetFactory.createTextField("complete_instrument_id", instrId, instrLink, "19",true, false, false, "", "", "inline", instruButton)%>
                                         
											
											<%=widgetFactory.createTextField("bank_instrument_id", "BankMail.BankInstrumentId",bankInstId, "19",true, false, false, "", "", "inline", "")%>
                                    <%
                                          }
                                          if( !isReadOnly ){
                                                instrId = "Message.ReferenceAnInstrumentID";
                                              
                                                if(isSubjOrInstrIdReadOnly==true)
	                    							{

	                    						%>

                                                	 <%=widgetFactory.createTextField("complete_instrument_id", instrId, completeInstId, "19",isSubjOrInstrIdReadOnly, isSubjOrInstrIdReq, false, "", "", "inline", instruButton)%>
												
												<%
													}
	                                                else if (isSubjOrInstrIdReadOnly==false)
													{
												%>
												

												 <%=widgetFactory.createTextField("complete_instrument_id", instrId,completeInstId, "19", isSubjOrInstrIdReadOnly, false, false, "",	"", "inline", instruButton)%>

												

												<%
													}
												%>
												 
                                 			 
												<div class="formItem inline readOnly" >
													<label for="bank_instrument_id"><%=resMgr.getText("BankMail.BankInstrumentId", TradePortalConstants.TEXT_BUNDLE) %></label>
													<br><div class="fieldValue" id="bank_instrument_id_display"><%=StringFunction.xssCharsToHtml(bankInstId)%></div>
												</div>
												 <input type="hidden" name="bank_instrument_id" id='bank_instrument_id' value="<%=StringFunction.xssCharsToHtml(bankInstId)%>">
												 
                                    <%        secureParms.put( "bank_instrument_id", bankInstId);
                                                if( !isReadOnly && !isNewReply && !messageIsReply ) {
                                    %>
                                                <input type="hidden" name="NewSearch" value="Y">

                                    <%
                                                      // Send flag indicating that user is able to view messages of their children
                                                if(SecurityAccess.hasRights(loginRights,SecurityAccess.VIEW_CHILD_ORG_MESSAGES )){
                                    %>
                                                      <input type="hidden" name="ShowChildInstruments" value="<%=TradePortalConstants.INDICATOR_YES%>">
                                    <%
                                                      }
                                                }
                                          }
                                    %>

                              <div>
                                   
                                    <div style="clear: both;"></div>
                              </div>

                              <%
                                   /***************************
                                    * Document Image Links
                                    **************************/
                                    Debug.debug("*** Document Image Links ***");

                                    if( (docImageList != null) && (listviewVector != null) ) {
                              %>
                                          <%= widgetFactory.createLabel("", "Message.RelatedDocuments", false, false, false, "") %>
                              <%
                                          int liTotalDocs = listviewVector.size();
                                          int liDocCounter = liTotalDocs;
                                          String  displayTextPDF = "";
                                          encryptedMailMessageOid = EncryptDecrypt.encryptStringUsingTripleDes( mailMessageOid, userSession.getSecretKey() );
                                          for(int y=0; y < ((liTotalDocs+2)/3); y++) {
                                          for (int x=0; x < 3 ; x++) {
                                                      if (liDocCounter==0) break;

                                                      myDoc       = ((DocumentHandler)listviewVector.elementAt((y*3)+x));
                                                  String imageHash = myDoc.getAttribute("/HASH");
                                                  String encryptedImageHash = "";

                                                  if ( imageHash != null && imageHash.length()>0 ) {
                                                      encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( imageHash, userSession.getSecretKey() );
                                                } else {
                                                      
                                                        encryptedImageHash = EncryptDecrypt.encryptStringUsingTripleDes( TradePortalConstants.NO_HASH, userSession.getSecretKey() );
                                                    }

                                                  String imageFormat = myDoc.getAttribute("/IMAGE_FORMAT");

                                                  if (imageFormat.equals(TradePortalConstants.PDF_IMAGE)) {
                                                          
                                                            displayText = myDoc.getAttribute("/DOC_NAME");

                                                            urlParm = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) + "&hash=" + encryptedImageHash;
                                                       
                                                            displayTextPDF = resMgr.getText("Message.getImage", TradePortalConstants.TEXT_BUNDLE);
                              %>
                                                            <div class="formItem">
                                                                  <%= widgetFactory.createSubLabel(displayText+" ") %>
                                                                  <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParm)%>' target='_blank'><%= displayTextPDF %></a>
															</div>	  
                              <%
                                                }else {
                                                            displayText = getDisplayText( myDoc );

                                                            //add hash parm
                                                            urlParm = buildURLParameters( listviewVector, myDoc, encryptedMailMessageOid, subject, displayText, encryptedImageHash, userSession );
                              %>
                                                            <div class="formItem">
                                                                  <%= widgetFactory.createSubLabel(displayText+" ") %>
                                                            <a href="<%= formMgr.getLinkAsUrl("goToDocumentView", urlParm, response)   %>">Image</a>
                                                            <%= widgetFactory.createSubLabel(" | ") %>
                              <%
                                                            displayTextPDF = resMgr.getText("Message.getImage", TradePortalConstants.TEXT_BUNDLE);
                                                            //add imageHash parm
                                                            urlParm = "image_id=" + EncryptDecrypt.encryptStringUsingTripleDes(myDoc.getAttribute("/IMAGE_ID"), userSession.getSecretKey()) + "&hash=" + encryptedImageHash;
                              %>
                                                            <a href='<%= response.encodeURL("/portal/documentimage/ViewPDF.jsp?"+urlParm)%>' target='_blank'><%= displayTextPDF %></a>
                                                      </div>
                              <%
                                                      }
                                                      liDocCounter--;
                                          }

                                          if (liDocCounter==0) {
                                                      for (int z=liTotalDocs % 3; z > 0; z--) {}
                                                }
                                    }
                                    } 
                              %>

<%
                 /***************************
                  * Subject
                  **************************/
                  Debug.debug("*** Subject ***");
                 

                  if( isNewReply ){
                  String re = resMgr.getText("Message.Re", TradePortalConstants.TEXT_BUNDLE);
                  if(!subject.startsWith(re)){
                        subject = re + "  " + subject;
                  }
                  if(subject.length() > 60){
                  subject = subject.substring(0, 56) + "...";
                  }

                        secureParms.put("message_subject", subject);
                  }
                  if( messageIsReply || isReadOnly ){
                    
                        secureParms.put("message_subject", subject);
                  }

                  if( !messageStatus.equals(TradePortalConstants.SENT_TO_BANK) && !(isMessageOidNull && !isNewReply)){
         
                  }
                 /***************************************
                  * Message Text : Multi Line Text Area
                  **************************************/
                  Debug.debug("*** Message Text : Multi Line Text Area ***");

            %>
				 <%@ include file="fragments/Messages-Documents.frag" %>
                  <%= widgetFactory.createTextArea("message_text", "Message.Message", messageText, isReadOnly, false, false, "maxlength=4000 rows=3", "", "") %>
                  <%if(!isReadOnly) {	%>
				  		<%=widgetFactory.createHoverHelp("message_text","MessageTextAreaHoverText") %>
				  <% } %>


            <%
                 /***************************************
                  * Message Text : Multi Line DiscrepancyText Area
                  **************************************/
               
            
%>                <br>
</div>
                  </div><%--closes formContent area--%>
                  </div><%--closes formArea area--%>

                  <div class="formSidebar" data-dojo-type="widget.FormSidebar" data-dojo-props="form: 'BankMessagesDetail'">
                        <%@ include file="fragments/BankMessagesSidebar.frag" %>
                  </div> <%--closes sidebar area--%>
<%
                  secureParms.put("opt_lock", message.getAttribute("opt_lock"));

                // Put the isEdit flag into the input document so that after
                // the edit button is pressed subsequent pages (until final submit)
                // will also be in edit mode
                if(isEdit)
                        secureParms.put("isEdit", "true");
                  if(isNewReply)
                  secureParms.put("isNewReply", "true");
%>
                  <%= formMgr.getFormInstanceAsInputField("BankMessagesDetailForm", secureParms) %>
                  <div id="instrumentSearchDialog"></div>
            </form>
      </div> <%--closes pageContent area--%>
</div> <%--closes pageMain area--%>

<jsp:include page="/common/Footer.jsp">
  <jsp:param name="displayFooter" value="<%=TradePortalConstants.INDICATOR_YES%>" />
  <jsp:param name="includeNavigationBarFlag" value="<%=TradePortalConstants.INDICATOR_YES%>" />
</jsp:include>


<%

   Debug.debug("Default Doc data is: " + defaultDoc.toString() );

   if( (isCurrentUser || (isUsersOrgOid )) &&
       !(message.getAttribute("unread_flag")).equals(TradePortalConstants.INDICATOR_NO) ) {

      message.setAttribute("unread_flag", TradePortalConstants.INDICATOR_NO);
      message.save();                                     //This may increment opt_lock
  }                                                 //This will ensure the mediator
                                                    //has the right numeric value.
  message.remove();

   Debug.debug("***END******************** Mail Message Page *********************END***");
%>


</body>
</html>
<%
   // Finally, reset the cached document to eliminate carryover of
   // information to the next visit of this page.
   defaultDoc.removeAllChildren("/");
   formMgr.storeInDocCache("default.doc", defaultDoc);
%>

<%!
   
   public String buildURLParameters (Vector listviewVector, DocumentHandler myDoc, String encryptedMailMessageOid,
                                     String subject, String displayText, String encryptedImageHash, SessionWebBean userSession ) {

      String urlParm = "";

      String imageId = myDoc.getAttribute("/IMAGE_ID");
      String encryptedImageId = EncryptDecrypt.encryptStringUsingTripleDes( imageId, userSession.getSecretKey() );

      String encryptedDisplayText = EncryptDecrypt.encryptStringUsingTripleDes( displayText , userSession.getSecretKey());

      urlParm    = "&image_id=" + encryptedImageId +
                   "&hash=" + encryptedImageHash +
                   "&formType=" + encryptedDisplayText +
                   "&fromMessages=Y" + "&messageSubject=" + subject +
                   "&mailMessageOid=" + encryptedMailMessageOid;

      return urlParm;
   }


   //This method only focuses on building a display value of the formtype.  The returned
   //result will be used as the link text.

   public String getDisplayText (DocumentHandler myDoc) {
      String attribute;
      String displayText = "";

      try {
         attribute   = myDoc.getAttribute("/FORM_TYPE");
         displayText = ReferenceDataManager.getRefDataMgr().getDescr(TradePortalConstants.FORM_TYPE,
                                                     attribute);
      }catch(AmsException e) {            //This should never happen
            Debug.debug("*********** Call to Refdata Mgr failed to get description for a documemt image. **********");
            Debug.debug("***** The problematic FormType is : " + myDoc.getAttribute("/FORM_TYPE") );
      }

      return displayText;
   }
%>
<script type=text/javascript>

function SearchInstrument(identifierStr, sectionName , InstrumentType) {
    <%--todo: add dialog require here, but need to walk back to event handler from the menus!!! --%>
    itemid = String(identifierStr);
      section = String(sectionName);
      IntType=String(InstrumentType);
     var custID =dijit.byId('bankmessage_customer').value;
     var custName =dijit.byId('bankmessage_customer').displayedValue;
     if(custID == ""){
    	 alert("A Send to Customer must be entered before an Instrument Search can take place.");
     }else{
    require(["t360/dialog"], function(dialog ) {
      dialog.open('instrumentSearchDialog', '<%=instrumentSearchDialogTitle%>'+custName,
                  'instrumentSearchDialogID.jsp',
                  ['itemid','section','IntType','custID'], [itemid,section,IntType,custID], //parameters
                  'select', null);
   

    });
     }
  }
function reSetInst(){
	//Reseting bank instrument and complete instrument ID's
	dijit.byId('complete_instrument_id').setValue('');
	document.getElementById('bank_instrument_id_display').innerHTML = '';
	document.getElementById('bank_instrument_id').value = '';
}

</script>