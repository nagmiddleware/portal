<%--
*******************************************************************************
                               Messages Count Details

  Description: In messageshome based on change of display option we have to update the count in Inbox, Draft, Sent.
			   In old app onchange of dropdown complet page reloads,so java code is there in main jsp. Here in new app we are loading 
			   the grid via ajax onchange of dispaly option. So we need to update respective Inbox, Draft, Sent items count. Writing DB 
			   code in this jsp and sending back result via ajax response.

*******************************************************************************
--%>

<%@ page import="com.amsinc.ecsg.frame.*,com.amsinc.ecsg.util.*,
                 com.amsinc.ecsg.html.*,com.amsinc.ecsg.web.*,com.ams.tradeportal.dataview.*,
                 com.ams.tradeportal.common.*,com.ams.tradeportal.busobj.webbean.*,
                 com.ams.tradeportal.busobj.util.*, com.ams.tradeportal.html.*, java.util.*" %>

<jsp:useBean id="resMgr" class="com.amsinc.ecsg.util.ResourceManager"
    scope="session">
</jsp:useBean>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
</jsp:useBean>

<jsp:useBean id="beanMgr" class="com.amsinc.ecsg.web.BeanManager" scope="session">
</jsp:useBean>

<jsp:useBean id="userSession" class="com.ams.tradeportal.busobj.webbean.SessionWebBean"
   scope="session">
</jsp:useBean>
<%


//IR 20876 start
//Use the constants from MailMessagesDataView.java
//final String      MY_MAIL_AND_UNASSIGNED = resMgr.getText("Mail.MeAndUnassigned", TradePortalConstants.TEXT_BUNDLE);
//final String      MY_MAIL_ONLY           = resMgr.getText("Mail.Me",              TradePortalConstants.TEXT_BUNDLE);
//final String      ALL_MAIL               = resMgr.getText("Mail.AllMail",         TradePortalConstants.TEXT_BUNDLE);
 
//IR 20876 end

StringBuffer      sentToBankFolderTotalWhereClause = new StringBuffer();
StringBuffer      sentToBankFolderTotal            = null;
StringBuffer      dynamicWhereClause               = new StringBuffer();
StringBuffer      draftsFolderTotalWhereClause     = new StringBuffer();
StringBuffer      draftsFolderTotal                = null;
StringBuffer      inboxFolderTotalWhereClause      = new StringBuffer();
StringBuffer      inboxFolderTotal                 = null;
StringBuffer      tempSql                          = null;
StringBuffer      newLink                          = new StringBuffer();
String            currentMailOption                = null;
String            currentFolder                    = null;


String            userOid            = null;
String            userOrgOid         = null;
String            userOrgName        = null;
String            userSecurityRights = null;
String            userSecurityType   = null;

userSecurityRights = userSession.getSecurityRights();
userSecurityType   = userSession.getSecurityType();
userOrgName        = userSession.getOrganizationName();
userOrgOid         = userSession.getOwnerOrgOid();
userOid            = userSession.getUserOid();

int inboxCount=0;
int draftsCount=0;
int sentToBankCount=0;

	currentFolder = request.getParameter("currentFolder");

    if (currentFolder == null)
    {
       currentFolder = (String) session.getAttribute("currentFolder");

       if (currentFolder == null)
       {
          currentFolder = TradePortalConstants.INBOX_FOLDER;
       }
    }

    currentMailOption = request.getParameter("currentMailOption");


    if (currentMailOption == null || "".equals(currentMailOption.trim()))
    {
       //currentMailOption = (String) session.getAttribute("currentMailOption");
       // If this is the first time accessing this page, default the mail option dropdown
       // list to the 'Me and Unnassigned' option
       if (currentMailOption == null || "".equals(currentMailOption.trim()))
       {
          currentMailOption = EncryptDecrypt.encryptStringUsingTripleDes(MailMessagesDataView.MY_MAIL_AND_UNASSIGNED, userSession.getSecretKey());
       }
    }

    //session.setAttribute("currentMailOption", currentMailOption);
    //
    //session.setAttribute("currentFolder",     currentFolder);

    currentMailOption = EncryptDecrypt.decryptStringUsingTripleDes(currentMailOption, userSession.getSecretKey());

 
    // The following code constructs the sql to use for the dynamic where clause listview
    // component and each of the folder totals adjacent to the folder images; these totals
    // (as well as the listview) depend on which mail option is currently selected in the
    // dropwdown list.
	//jgadela R90 IR T36000026319 - SQL INJECTION FIX
	List<Object> sqlParamsInbox = new ArrayList();
	List<Object> sqlParamsDrafts = new ArrayList();
	List<Object> sqlParamsSentToBankF = new ArrayList();
   if (currentMailOption.equals(MailMessagesDataView.MY_MAIL_ONLY))
    {
       dynamicWhereClause.append("and a.a_assigned_to_user_oid = ");
       dynamicWhereClause.append(userOid);

       inboxFolderTotalWhereClause.append("(a_assigned_to_user_oid = ? ");
       inboxFolderTotalWhereClause.append(" or (a_assigned_to_corp_org_oid = ? ");
       inboxFolderTotalWhereClause.append(" and a_assigned_to_user_oid is null))");

       draftsFolderTotalWhereClause.append("a_assigned_to_user_oid = ? ");
       draftsFolderTotalWhereClause.append(userOid);
       
       sentToBankFolderTotalWhereClause.append(draftsFolderTotalWhereClause.toString());
       sqlParamsInbox.add(userOid);
       sqlParamsInbox.add(userOrgOid);
       sqlParamsDrafts.add(userOid);
       sqlParamsSentToBankF.add(userOid);
    }
    else if (currentMailOption.equals(MailMessagesDataView.MY_MAIL_AND_UNASSIGNED))
    {
       dynamicWhereClause.append("and (a.a_assigned_to_user_oid = ");
       dynamicWhereClause.append(userOid);
       dynamicWhereClause.append(" or (a.a_assigned_to_corp_org_oid = ");
       dynamicWhereClause.append(userOrgOid);
       dynamicWhereClause.append(" and a.a_assigned_to_user_oid is null))");

       inboxFolderTotalWhereClause.append("(a_assigned_to_user_oid = ? ");
       inboxFolderTotalWhereClause.append(" or (a_assigned_to_corp_org_oid = ? ");
       inboxFolderTotalWhereClause.append(" and a_assigned_to_user_oid is null))");

       draftsFolderTotalWhereClause.append("a_assigned_to_user_oid = ? ");
     //IR 20876 start -there is no ME and Unassigned dropdown value for sent to bank folder
      sentToBankFolderTotalWhereClause.append(draftsFolderTotalWhereClause.toString());
     
      sqlParamsInbox.add(userOid);
      sqlParamsInbox.add(userOrgOid);
      sqlParamsDrafts.add(userOid);
      sqlParamsSentToBankF.add(userOid);
   /*  sentToBankFolderTotalWhereClause.append("(a_assigned_to_user_oid = ");
       sentToBankFolderTotalWhereClause.append(userOid);
    sentToBankFolderTotalWhereClause.append(" or (a_assigned_to_corp_org_oid = ");
       sentToBankFolderTotalWhereClause.append(userOrgOid);
       sentToBankFolderTotalWhereClause.append(" and a_assigned_to_user_oid is null))");*/
  	//IR 20876 start
    }
    else if (currentMailOption.equals(MailMessagesDataView.ALL_MAIL))
    {
       tempSql = new StringBuffer();
       tempSql.append(" select organization_oid");
       tempSql.append(" from corporate_org");
       tempSql.append(" where activation_status = '");
       tempSql.append(TradePortalConstants.ACTIVE);
       tempSql.append("' start with organization_oid = ? ");
       tempSql.append(" connect by prior organization_oid = p_parent_corp_org_oid)");

       dynamicWhereClause.append("and a.a_assigned_to_corp_org_oid in (");
       dynamicWhereClause.append(tempSql.toString());

       inboxFolderTotalWhereClause.append("a_assigned_to_corp_org_oid in (");
       inboxFolderTotalWhereClause.append(tempSql.toString());

       draftsFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());

       sentToBankFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());
       
       sqlParamsInbox.add(userOid);
       sqlParamsDrafts.add(userOid);
       sqlParamsSentToBankF.add(userOid);
    }
    else
    {
       dynamicWhereClause.append("and a.a_assigned_to_corp_org_oid = ");
       dynamicWhereClause.append(currentMailOption);

       inboxFolderTotalWhereClause.append("a_assigned_to_corp_org_oid = ? ");

       draftsFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());

       sentToBankFolderTotalWhereClause.append(inboxFolderTotalWhereClause.toString());
       
       sqlParamsInbox.add(currentMailOption);
       sqlParamsDrafts.add(currentMailOption);
       sqlParamsSentToBankF.add(currentMailOption);
    }

    /* if (currentFolder.equals(TradePortalConstants.INBOX_FOLDER))
    {
       dynamicWhereClause.append(" and message_status in ('");
       dynamicWhereClause.append(TradePortalConstants.REC);
       dynamicWhereClause.append("', '");
       dynamicWhereClause.append(TradePortalConstants.REC_ASSIGNED);
       dynamicWhereClause.append("', '");
       dynamicWhereClause.append(TradePortalConstants.SENT_ASSIGNED);
       dynamicWhereClause.append("') ");
    }
    else if (currentFolder.equals(TradePortalConstants.DRAFTS_FOLDER))
    {
       dynamicWhereClause.append(" and message_status = '");
       dynamicWhereClause.append(TradePortalConstants.DRAFT);
       dynamicWhereClause.append("' ");
    }
    else if (currentFolder.equals(TradePortalConstants.SENT_TO_BANK_FOLDER))
    {
       dynamicWhereClause.append(" and message_status = '");
       dynamicWhereClause.append(TradePortalConstants.SENT_TO_BANK);
       dynamicWhereClause.append("' ");
    } */

    inboxFolderTotalWhereClause.append(" and message_status in ('");
    inboxFolderTotalWhereClause.append(TradePortalConstants.REC);
    inboxFolderTotalWhereClause.append("', '");
    inboxFolderTotalWhereClause.append(TradePortalConstants.REC_ASSIGNED);
    inboxFolderTotalWhereClause.append("', '");
    inboxFolderTotalWhereClause.append(TradePortalConstants.SENT_ASSIGNED);
    inboxFolderTotalWhereClause.append("') ");
    inboxFolderTotalWhereClause.append("and unread_flag = 'Y'");
  //CR 913 - Rel 9.0 Filter mail messages for funding_amount and funding_date is NULL
    inboxFolderTotalWhereClause.append(" and funding_amount is NULL");
    inboxFolderTotalWhereClause.append(" and funding_date is NULL ");

    draftsFolderTotalWhereClause.append(" and message_status = '");
    draftsFolderTotalWhereClause.append(TradePortalConstants.DRAFT);
    draftsFolderTotalWhereClause.append("' ");

    sentToBankFolderTotalWhereClause.append(" and message_status = '");
    sentToBankFolderTotalWhereClause.append(TradePortalConstants.SENT_TO_BANK);
    sentToBankFolderTotalWhereClause.append("' ");

    inboxCount = DatabaseQueryBean.getCount("message_oid", "mail_message", inboxFolderTotalWhereClause.toString(),false, sqlParamsInbox);
    draftsCount = DatabaseQueryBean.getCount("message_oid", "mail_message", draftsFolderTotalWhereClause.toString(),false, sqlParamsDrafts);
    sentToBankCount = DatabaseQueryBean.getCount("message_oid", "mail_message", sentToBankFolderTotalWhereClause.toString(),false, sqlParamsSentToBankF);
	
    /* if(MailOption!=null){	
		currentMailOption = EncryptDecrypt.decryptStringUsingTripleDes(MailOption, userSession.getSecretKey());
		
	} */
	
	response.setContentType("application/json");
	
	out.print("{'inboxCount':'"+inboxCount+"','draftsCount':'"+draftsCount+"','sentToBankCount':'"+sentToBankCount+"'}");
	out.flush();
%>

