<%@ page import="com.ams.tradeportal.common.HSMEncryptDecrypt,
			com.amsinc.ecsg.frame.*, com.amsinc.ecsg.util.StringFunction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>HSM Encryption Tester</title>
</head>

<body>
<SCRIPT src="/portal/js/crypto/pidcrypt.js"></SCRIPT>
<SCRIPT src="/portal/js/crypto/pidcrypt_util.js"></SCRIPT>
<SCRIPT src="/portal/js/crypto/asn1.js"></SCRIPT><%-- needed for parsing decrypted rsa certificate --%>
<SCRIPT src="/portal/js/crypto/jsbn.js"></SCRIPT><%-- needed for rsa math operations --%>
<SCRIPT src="/portal/js/crypto/rng.js"></SCRIPT><%-- needed for rsa key generation --%>
<SCRIPT src="/portal/js/crypto/prng4.js"></SCRIPT><%-- needed for rsa key generation --%>
<SCRIPT src="/portal/js/crypto/rsa.js"></SCRIPT><%-- needed for rsa en-/decryption --%>
<SCRIPT src="/portal/js/crypto/md5.js"></SCRIPT><%-- needed for key and iv generation --%>
<SCRIPT src="/portal/js/crypto/aes_core.js"></SCRIPT><%-- needed block en-/decryption --%>
<SCRIPT src="/portal/js/crypto/aes_cbc.js"></SCRIPT><%-- needed for cbc mode --%>
<SCRIPT src="/portal/js/crypto/sha256.js"></SCRIPT>
<SCRIPT src="/portal/js/crypto/tpprotect.js"></SCRIPT><%-- needed for cbc mode --%> 
<SCRIPT>

function protect() 
{
    var theForm = window.document.theForm;
    var dataIn = theForm.input.value;
    var public_key = theForm.public_key.value;
    var iv = theForm.iv.value;
    
    var protector = new TPProtect();
	 theForm.protectedPassword.value = protector.Encrypt({publicKey: public_key, usePem: true, iv: iv, data: dataIn});
	 theForm.submit();
}

var global_public_key = '-----BEGIN PUBLIC KEY-----\n\
<%= HSMEncryptDecrypt.getPublicKey() %>\n\
-----END PUBLIC KEY-----';

var global_iv = '<%= HSMEncryptDecrypt.getNewIV() %>';

</SCRIPT>
<h1>HSM Encryption Tester</h1>
<form id="theForm" name="theForm" method="POST" action="/portal/optional/HSMEncryptionTesterAES.jsp">
<%
	if((request.getAttribute("protectedPassword") == null)
		&& (request.getParameter("protectedPassword") == null))
	{
%>
<input type=hidden name="protectedPassword">
Please input something to encrypt:<br/>
<INPUT name="input" type="text" VALUE="Sample Message" size="67" 
    style="width: 535px"> <BR>
<br />
<INPUT VALUE="Encrypt" TYPE="BUTTON" onClick="protect();"><br />
<br />
RSA Public Key:<br>
<TEXTAREA id="public_key" name="public_key" ROWS="6" COLS="65"></TEXTAREA><BR>
<br />
IV:<br>
<TEXTAREA id="iv" name="iv" ROWS="2" COLS="65"></TEXTAREA><BR>
<br />
<script>
  document.getElementById('public_key').value = global_public_key;
  document.getElementById('iv').value = global_iv;
</script>
<%
	}
	else
	{
		String protectedText = request.getParameter("protectedPassword");
		//Validation to check Cross Site Scripting
		if(protectedText != null){
			protectedText = StringFunction.xssCharsToHtml(protectedText);
		}
		out.println("<H1>Protected Text:</H1><br/>"+protectedText+"<br/><br/>");
		String decryptedText = HSMEncryptDecrypt.decryptForTransport(protectedText);
		out.println("<H1>Decrypted Text:</H1><br/>"+decryptedText+"<br/><br/>");
%>
<INPUT VALUE="Try again.." TYPE="BUTTON" onClick="location.href='/portal/optional/HSMEncryptionTesterAES.jsp'"><br />
<%
	}
%>
</body>
</html>
