<%--
 *  This page is to be used for testing only.
 *  It provides the ability to test logon via SSO.
 *
 *  The page is re-entrant -- primary concept being that we
 *  use the jsp HttpURLConnection to actually do the logon --
 *  and jsp is only executed when a page is rendered.
 *  So the use of various functions and the parameters associated with
 *  it are manipulated by the user on the page.  Clicking a
 *  particular link will re-enter the page and do the special
 *  logic.  this also works well for calculating encrypted values...
 *
 *  Note: although use of iframes was attempted to display actual
 *  logon page, this is not complete.  However http response code is displayed.
--%>
<%@ page import="com.amsinc.ecsg.web.*,com.amsinc.ecsg.util.*,
        com.amsinc.ecsg.html.*,com.amsinc.ecsg.frame.*, java.security.*,
        com.ams.tradeportal.busobj.webbean.*,com.ams.tradeportal.html.*, 
        com.ams.tradeportal.common.*, java.util.*, java.text.*, java.io.*, java.net.*, javax.net.ssl.HttpsURLConnection" %>


<html>
  <head>
    <META HTTP-EQUIV= "expires" content = "Saturday, 07-Oct-00 16:00:00 GMT"></meta>
    <script type="text/javascript">
<%--
  //unused functions below - retained for possible future use
  function makeIFrame(iframeName, width, height) {
    var iframe;
    if (document.createElement && (iframe = document.createElement('iframe'))) {
      iframe.name = iframe.id = iframeName;
      iframe.width = width;
      iframe.height = height;
      iframe.src = 'about:blank';
      document.body.appendChild(iframe);
    }
    return iframe;
  }
  function makeResponseIFrame() {
    var iframe = makeIFrame('responseIFrame',500,500);
    if (iframe) {
      var iframeDoc;
      if (iframe.contentDocument) {
        iframeDoc = iframe.contentDocument;
      }
      else if (iframe.contentWindow) {
        iframeDoc = iframe.contentWindow.document;
      }
      else if (window.frames[iframe.name]) {
        iframeDoc = window.frames[iframe.name].document;
      }
      if (iframeDoc) {
        iframeDoc.open();
	iframeDoc.write( '<html><body>junk<\/body><\/html>' );
        iframeDoc.close();
      }
    }
  }

  //window.onload = function(evt) {
    makeResponseIFrame();
  }
--%>
  </script>
</head>
<body>
<font face="Arial" size="3">
<b><i>Trade Portal</i> URL Logon Testing</b>
<br><br>

<%
	PropertyResourceBundle portalProperties = (PropertyResourceBundle) PropertyResourceBundle.getBundle("TradePortal");

	String serverName = portalProperties.getString("serverName");
        String SSLportNumber;

        if((serverName != null) && (!serverName.equals("")))
         {
            out.print("Server Instance: "+serverName);
         }


        // Get the port that links should be set to for certificates
        // Usually, for the Apache web server, this port is the same as the
        // non-certificate port.  For IIS or WebLogic web server, this port sometimes
        // needs to be different
        try
         {
            SSLportNumber = portalProperties.getString("webServerSSLPort"); 
            if(SSLportNumber.equals(""))
                 SSLportNumber = Integer.toString(request.getServerPort());
         }
        catch(Exception e)
         {
            SSLportNumber = Integer.toString(request.getServerPort());
         }
%>

<br><br>
<font color=red><b>This page is only for testing.   It will NOT be in the production version.</b></font>
<br>
<br>
In production, users will access the Trade Portal by clicking a link of their bank's web site. The link needs to contain certain information in order for the 
user to properly access the system.   To simulate in dev, enter url parameters manually below and client Logon link.
<br>

<jsp:useBean id="formMgr" class="com.amsinc.ecsg.web.FormManager" scope="session">
  <%
    formMgr.init(request.getSession(true), "AmsServlet");
  %>
</jsp:useBean>

<%
  // We always need to initialize the form manager anytime we return to this page.
  // This is because the session timeout/invalidation does something strange to this
  // bean.  Calling init takes care of this problem.  Multiple init's does not 
  // have any adverse affect.
  formMgr.init(request.getSession(true), "AmsServlet");
%>
<hr>
<br>
<% //get a value for the current page so we can reenter with result of various functions
  String thisPageLink = "/portal/optional/TestingURLLogon.jsp";
  String submitUrl = "";
  int responseCode = -1;
  String sessionId = request.getParameter("childSessionId");
  if (sessionId == null ) {
    sessionId = "";
  }
//Validation to check Cross Site Scripting
  if(sessionId != null){
	  sessionId = StringFunction.xssCharsToHtml(sessionId);
  }
%>

<%--utility to help encrypt identification parm--%>
<b>Encryption Utility</b>
<br/>
Enter a user id, date & time, and aes key.
Click the encrypt link to get an encrypted value using the aes key.
Pay close attention to the date & time value to ensure it will be within the acceptable logon period.
<br/>
<br/>
User ID: <input type="text" id="userIdInput" size="30" maxlength="30" value="testuser"/>
<% //default to current gmt date/time
   //note that user can test a delayed date/time by just waiting on this page
  SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  String isoDate = iso.format(GMTUtility.getGMTDateTime());
  isoDate = isoDate.replace(' ','T');
  isoDate = isoDate + "Z";
%>
Date & Time: <input type="text" id="dateTimeInput" size="30" maxlength="30" value="<%=isoDate%>"/>
<br/>
<br/>
AES Key: <input type="text" id="aesKeyInput" size="75" maxlength="66" value="1234123412341234"/>
<br/>
<a id="encryptParmLink" href='<%=thisPageLink+"?function=error"%>' onclick="javascript:modifyEncryptParmLink();">Encrypt URL Parm (AES)</a>
<a id="encryptKeyLink" href='<%=thisPageLink+"?function=error"%>' onclick="javascript:modifyEncryptKeyLink();">Encrypt AES Key (3DES)</a>
<br/>
<br/>

<% if ("encryptParm".equals(request.getParameter("function"))) {
  String userId = request.getParameter("userId");
  String dateTime = request.getParameter("dateTime");
  String aesKey = request.getParameter("aesKey");
  String identification = userId + ";" + dateTime;
  
  System.out.println ("userId -->" + userId);
  System.out.println ("dateTime -->" + dateTime);
  System.out.println ("aesKey -->" + aesKey);
  System.out.println ("identification -->" + identification);
  
  String encryptedUserIdAndTime = EncryptDecrypt.encryptUsingAESKey(identification, aesKey);
  if(encryptedUserIdAndTime != null && encryptedUserIdAndTime.indexOf('+')>=0){
    //not sure about this encoding...
    encryptedUserIdAndTime = encryptedUserIdAndTime.replaceAll("\\+", "%2B");
  }
%>
Encrypted URL Parm Value: <label>identification=<%=encryptedUserIdAndTime%></label>
<br/>
<br/>
<% } %>

<% if ("encryptKey".equals(request.getParameter("function"))) {
  String aesKey = request.getParameter("aesKey");
  // Decode data passed in using base64
  byte[] sourceBytes = EncryptDecrypt.base64StringToBytes(aesKey);
  // Encrypt using real key
  byte[] encryptedBytes = EncryptDecrypt.encryptUsingDes3(sourceBytes);
  // Encode value in base64 to be returned
  String encryptedKey = EncryptDecrypt.bytesToBase64String(encryptedBytes);
%>
Encrypted AES Key Value: <label><%=encryptedKey%></label>
<br/>
<br/>
<% } %>


<%--utility to test logon--%>
<b>Logon Utility</b>
<br/>
Enter logon parameters as they would appear in a url (including &amp; character for separation).
Click Modify Logon Link to set the parameters in the Logon Link, then click Logon.
The logon is attempted from this jsp page, with the response output below.
<br/>
<br/>
<%
  //build main portion of url and add manual logon parms
  String certLogonPage = NavigationManager.getNavMan().getPhysicalPage("CertificateLogon", request);
  String passwordLogonPage = NavigationManager.getNavMan().getPhysicalPage("PasswordLogon", request);
  String tradePortalLogonPage = NavigationManager.getNavMan().getPhysicalPage("TradePortalLogon", request);
  String logonMain = "https://" + request.getServerName() + ":" + SSLportNumber + request.getContextPath() + tradePortalLogonPage;
  String logonParms = "auth=AES256&organization=CB1&branding=blueyellow&brandbutton=N&locale=en_US";
%>
Logon Link: <%=logonMain+"?"%><br/>
Logon Parameters: <input type="text" id="logonParmsInput" size="150" maxlength="500" value="<%=logonParms%>"/>
<a id="logonLink" href='<%=thisPageLink+"?function=error"%>' onclick="javascript:modifyLogonLink();">Logon</a>
<br/>
<br/>

<% if ("logon".equals(request.getParameter("function"))) {
String queryString = request.getQueryString();
submitUrl = logonMain + "?" + queryString;
%>
Submitting Logon...
<br/>
<% } //end of function=logon %>


<%--utility to test session synch--%>
<br/>
<b>Session Sync Utility</b>
<br/>
Click the Invoke Session Sync link to invoke the sessionSync.jsp page.
<br/>
<br/>
Session ID: <input type="text" id="sessionIdInput" size="100" maxlength="100" value="<%=sessionId%>"/>
<a id="sessionSyncLink" href='<%=thisPageLink+"?function=error"%>' onclick="javascript:modifySessionSyncLink();">Invoke Session Sync</a>
<br/>
<br/>

<% if ("sessionSync".equals(request.getParameter("function"))) {
submitUrl = "https://" + request.getServerName() + ":" + SSLportNumber + request.getContextPath() + "/logon/SessionSync.jsp";
%>
Submitting Session Sync...
<br/>
<% } //end of function=sessionSync %>


<%--utility to test session synch--%>
<br/>
<b>Reload Page</b>
<br/>
Click the Reload Page link to start over with a clean slate!
<br/>
<br/>
<a id="reloadPageLink" href='<%=thisPageLink%>'>Reload Page</a>
<br/>
<br/>


<% if ("error".equals(request.getParameter("function"))) { %>
<br/>
!!!!!Error!!!!!
<br/>
<br/>
<% } %>


<%--HTTP connection logic--%>
<% if ( submitUrl != null && submitUrl.length() > 0 ) { %>
<b>URL Response</b>
<br/>
<br/>
<%
//the following code submits the logon (or sessionsync) url -- note that we had to reenter the current page
//to execute the below jsp code -- which programmatically calls the page.
//this allows us to retrieve the response code and then display it!
//in dev/test envs (which is the only place this should be running!)
//when using ssl, the certificate is likely invalid becuase of the hostname verification
//step that weblogic performs -- in order to get around this, the weblogic server
//must have hostname verification turned off -- in the console, select the correct
// server, client on 'SSL', 'Advanced', set 'hostname verification' to 'none'
// and restart the server.

out.println("Opening connection:"+submitUrl);

try {
  URL url = new URL(submitUrl);
  URLConnection urlConn = url.openConnection();

  if ( urlConn instanceof HttpURLConnection ) {
    //out.println("httpurlconnection");
    HttpURLConnection hurlConn = (HttpURLConnection) urlConn;

    //manipulate request properties
    //first attempt to perpetuate the child session id
    if ( sessionId != null ) {
      out.println("setting JSESSIONID="+sessionId);
      hurlConn.setRequestProperty("Cookie","JSESSIONID="+sessionId);
    }

    //get the response
    responseCode = hurlConn.getResponseCode();
  }

  if ( responseCode == 200 ) { //success so we can get the actual page content
    BufferedReader rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

    if ( urlConn instanceof HttpURLConnection ) {
      //out.println("httpurlconnection");
      HttpURLConnection hurlConn = (HttpURLConnection) urlConn;
      Map headerMap = hurlConn.getHeaderFields();
      for(Iterator it = headerMap.keySet().iterator(); it.hasNext(); ) {
        String headerFieldName = (String)it.next();
	String headerFieldValue = hurlConn.getHeaderField(headerFieldName);
	%>
	Header Field: <%=headerFieldName%> = <%=headerFieldValue%><br/>
	<%
	//if we get a set cookie back for JSESSIONID it means we have a
	//new session!
	if ( "Set-Cookie".equalsIgnoreCase(headerFieldName) ) {
	  StringTokenizer strTok1 = new StringTokenizer(headerFieldValue,";");
	  while (strTok1.hasMoreTokens()) { 
	    String cookieField = strTok1.nextToken().trim();
	    if ( cookieField != null && cookieField.length() > 0 ) {
	      if (cookieField.indexOf("=")>0) {
                String cookieName = cookieField.substring(0,cookieField.indexOf("="));
                String cookieValue = cookieField.substring(cookieField.indexOf("=")+1);
		if ( "JSESSIONID".equalsIgnoreCase(cookieName.trim()) ) {
		  sessionId = cookieValue.trim();
		  %>
		  New SessionID: <%=sessionId%><br/>
		  <%
                }
              }
            }
          }
        }
      }
    } 
  } 
  //if ( urlConn instanceof HttpsURLConnection ) {
  //  out.println("httpsurlconnection");
  //}

} catch (MalformedURLException mex ) {
  out.println("malformedurlex:"+mex.toString());
  mex.printStackTrace();
} catch(IOException iex) {
  out.println("ioex:"+iex.toString());
  iex.printStackTrace();
} catch(Exception ex) {
  out.println("ex:"+ex.toString());
  ex.printStackTrace();
}
%>
<br/>
<br/>

<% if ( responseCode != -1 ) { %>
Response Code: <%=responseCode%>
<br/>
<br/>
<% } %>

<%--<iframe onLoad="writeMyPage();calcHeight();" scrolling="NO" frameborder="1" width="1000" height="1000" id="resize">
	You need a Frame Capable browser to view this content.</iframe>--%>
<br/>
<br/>

<% } //end of submitUrl%>

<script language="JavaScript"> 
 
  function modifyLogonLink() {
    var myParms = document.getElementById("logonParmsInput").value;
    var thisLink = '<%=thisPageLink%>';
    var calcValue = thisLink+"?function=logon&"+myParms;
    document.getElementById("logonLink").href=calcValue;
    
  }
  function modifyEncryptParmLink() {
    
    var myUserId = document.getElementById("userIdInput").value;
    var myDateTime = document.getElementById("dateTimeInput").value;
    var myAesKey = document.getElementById("aesKeyInput").value;
    var thisLink = '<%=thisPageLink%>';
    var calcValue = thisLink+"?function=encryptParm&userId="+myUserId+"&dateTime="+myDateTime+"&aesKey="+myAesKey;
    document.getElementById("encryptParmLink").href=calcValue;
    
  }
  function modifyEncryptKeyLink() {
    
    var myAesKey = document.getElementById("aesKeyInput").value;
    var thisLink = '<%=thisPageLink%>';
    var calcValue = thisLink+"?function=encryptKey&aesKey="+myAesKey;
    document.getElementById("encryptKeyLink").href=calcValue;
    
  }
  function modifySessionSyncLink() {
    var mySessionId = document.getElementById("sessionIdInput").value;
    var thisLink = '<%=thisPageLink%>';
    var calcValue = thisLink+"?function=sessionSync&childSessionId="+mySessionId;
    document.getElementById("sessionSyncLink").href=calcValue;
    
  }

  //unused functions below - retained for possible future use
  function makeIFrame() {
    document.createElement('<iframe name="iframeName">');
    var objDoc = window.frames["iframeName"].document;
    objDoc.write( "junk" );
    objDoc.close();
  }
  function writeMyPage() {
    var objDoc = window.frames["resize"].document;
    objDoc.write( "junk" );
    objDoc.close();
  }
  function calcHeight()
  {
    //find the height of the internal page
    var the_height=document.getElementById('resize').contentWindow.document.body.scrollHeight;
    //change the height of the iframe
    document.getElementById('resize').height=the_height;
  }

  function setLogonResponsePage() {
    document.getElementById('resize').contentWindow.document=test;
  }
  function makeHttpObject() {
    try { return new XMLHttpRequest();}
    catch (error) {}
    throw new Error("Could not create HTTP request object.");
  }

  function simpleHttpRequest(url, success, failure) {
    var request = makeHttpObject();
    request.open("GET", url, true);
    request.send(null);
    request.onreadystatechange = function() {
      if (request.readyState == 4 ) {
        if (request.stats == 200 )
	  success(request.responseText);
	else if (failure)
	  failure(request.status, request.statusText);  
      }
    };
  }


</script>
</body>
</html>

<%
	// Because this is just a test page...
	// Kill the session so that the next page 
	// is self-sufficient in terms of the session 
	request.getSession(false).invalidate();
%>

