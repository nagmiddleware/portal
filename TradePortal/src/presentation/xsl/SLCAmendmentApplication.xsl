<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:import href="./templates/DefaultFonts.xsl"/><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 ADD -->
   <xsl:import href="./templates/FOCommon.xsl"/><!-- BSL IR BLUL120541439 12/08/11 Rel 7.1 ADD -->
   <xsl:import href="./templates/SectionA-SLCIssue.xsl"/>
   <xsl:import href="./templates/SectionB.xsl"/>
   <xsl:import href="./templates/SectionC-SLCAmendment.xsl"/>
   <xsl:import href="./templates/SectionN.xsl"/>
   <xsl:import href="./templates/SectionS-ExpandedAmend.xsl"/>
   <xsl:import href="./templates/SectionO.xsl"/>
   <xsl:import href="./templates/SectionJ.xsl"/>
   <xsl:import href="./templates/SectionT.xsl"/>
   <xsl:import href="./templates/SectionM-Amendment.xsl"/>
      
   <xsl:template match="Document">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
         <!-- BSL IR BLUL120541439 12/08/11 Rel 7.1 BEGIN - move simple-page-master into FOCommon.xsl with adjusted margins, extent, and padding -->
            <!--<fo:simple-page-master master-name="normal">
               <fo:region-body margin-bottom="75pt" margin-top="160pt" margin-left="35pt" margin-right="35pt"/>
               <fo:region-before extent="200pt" padding="35pt"/>
               <fo:region-after extent="50pt"/>
            </fo:simple-page-master>-->
            <xsl:call-template name="SimplePageMaster-HeaderBodyFooter"/>
         </fo:layout-master-set>
                 
         <!--<fo:page-sequence master-reference="normal">-->
         <fo:page-sequence master-reference="HeaderBodyFooter">
         <!-- BSL IR BLUL120541439 12/08/11 Rel 7.1 END -->
            <fo:static-content flow-name="xsl-region-before">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionA"/>
               </xsl:element>
            </fo:static-content>
            <fo:static-content flow-name="xsl-region-after">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <fo:block text-align="center">Page: <fo:page-number/></fo:block>
               </xsl:element>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionB"/>
                  <xsl:apply-templates select="./SectionC"/>
                  <xsl:apply-templates select="./SectionN"/>
                  <xsl:apply-templates select="./SectionS"/>
                  <xsl:apply-templates select="./SectionO"/>
                  <xsl:apply-templates select="./SectionJ"/>
                  <xsl:apply-templates select="./SectionT"/>
                  <xsl:apply-templates select="./SectionM"/>
               </xsl:element>
             </fo:flow>
         </fo:page-sequence>
         
      </fo:root>
   </xsl:template>
   
</xsl:stylesheet>
