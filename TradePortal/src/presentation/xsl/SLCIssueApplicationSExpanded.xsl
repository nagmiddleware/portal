<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:import href="./templates/DefaultFonts.xsl"/><!-- BSL Cocoon Upgrade 10/12/11 Rel 7.0 ADD -->
   <xsl:import href="./templates/FOCommon.xsl"/><!-- BSL IR BLUL120541439 12/08/11 Rel 7.1 ADD -->
   <xsl:import href="./templates/SectionA-SLCIssueExpanded.xsl"/>
   <xsl:import href="./templates/SectionB.xsl"/>
   <xsl:import href="./templates/SectionC-SLCIssue.xsl"/>
   <xsl:import href="./templates/SectionD.xsl"/>
   <xsl:import href="./templates/SectionE.xsl"/>
   <xsl:import href="./templates/SectionE0.xsl"/>
   <xsl:import href="./templates/SectionE1-ExpandedSLCIssue.xsl"/>
   <xsl:import href="./templates/SectionE2.xsl"/>
   <xsl:import href="./templates/SectionE3.xsl"/>
   <xsl:import href="./templates/SectionH-ExpandedIssue.xsl"/>
   <xsl:import href="./templates/SectionF-SLCIssue.xsl"/>  
   <xsl:import href="./templates/SectionJ.xsl"/>
   <xsl:import href="./templates/SectionL-ExpandedSLCIssue.xsl"/>
   <xsl:import href="./templates/SectionT1-DLCIssue.xsl"/>
   <xsl:import href="./templates/SectionT2-DLCIssue.xsl"/>
   <xsl:import href="./templates/SectionU1-DLCIssue.xsl"/>
   <xsl:import href="./templates/SectionU3-DLCIssue.xsl"/>
   <xsl:import href="./templates/SectionM-SLCIssue.xsl"/>
      
   <xsl:template match="Document">
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <xsl:call-template name="SimplePageMaster-HeaderBodyFooter"/>
         </fo:layout-master-set>
                 
         <!--<fo:page-sequence master-reference="normal">-->
         <fo:page-sequence master-reference="HeaderBodyFooter">
         <!-- BSL IR BLUL120541439 12/08/11 Rel 7.1 END -->
            <fo:static-content flow-name="xsl-region-before">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionA"/>
               </xsl:element>
            </fo:static-content>
            <fo:static-content flow-name="xsl-region-after">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <fo:block text-align="center">Page: <fo:page-number/></fo:block>
               </xsl:element>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
               	   <xsl:apply-templates select="./SectionB"/>
                  <xsl:apply-templates select="./SectionC"/>
                  <xsl:apply-templates select="./SectionD"/>
                  <xsl:apply-templates select="./SectionE"/>
                  <xsl:apply-templates select="./SectionE0"/>
                  <xsl:apply-templates select="./SectionE1"/>
                  <xsl:apply-templates select="./SectionE2"/>
                  <xsl:apply-templates select="./SectionE3"/>
                  <xsl:apply-templates select="./SectionH"/>                  
                  <xsl:apply-templates select="./SectionF"/>
                  <xsl:apply-templates select="./SectionJ"/>
                  <xsl:apply-templates select="./SectionL"/>
                  <xsl:apply-templates select="./SectionT1"/>
                  <xsl:apply-templates select="./SectionT2"/> 
                  <xsl:apply-templates select="./SectionU1"/>      
                  <xsl:apply-templates select="./SectionU3"/>                   
                  <xsl:apply-templates select="./SectionM"/>
               </xsl:element>
            </fo:flow>
         </fo:page-sequence>
         
      </fo:root>
   </xsl:template>
   
</xsl:stylesheet>
