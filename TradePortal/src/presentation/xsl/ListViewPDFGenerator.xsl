<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <!--  RKazi CR-710 02/02/2012 Rel 8.0 Add -->
   <xsl:import href="./templates/DefaultFonts.xsl"/>
   <xsl:import href="./templates/FOCommon.xsl"/>
   <xsl:import href="./templates/listview/SectionA-ListView.xsl"/>
   <xsl:import href="./templates/listview/SectionB-ListView.xsl"/>
   
   <xsl:variable name="colCount" select="count(/Document/SectionB/ResultSetRecord[1]/field)"></xsl:variable>
   <xsl:variable name="pageWidth" >
      <xsl:call-template name="recursive">
         <xsl:with-param name="colCount" select="$colCount"></xsl:with-param>
         <xsl:with-param name="posi" select="1"></xsl:with-param>
      </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="lineWidth" select="(number($pageWidth) - (number($colCount)*3 )) "></xsl:variable>
   
   <xsl:template match="Document">
      
      <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            
            <fo:simple-page-master master-name="normal" page-height="210mm">
               <xsl:attribute name="page-width">
                  <xsl:choose>
                     <xsl:when test="$pageWidth &lt; 165">
                        <xsl:value-of select="165"/>mm
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="$pageWidth"/>mm
                     </xsl:otherwise>
                  </xsl:choose>
                  
                  </xsl:attribute>
               <fo:region-body margin-bottom="75pt" margin-top="75pt" margin-left="35pt" margin-right="35pt"/>
               <fo:region-before extent="75pt" padding="35pt"/>
               <fo:region-after extent="75pt"/>
            </fo:simple-page-master>
         </fo:layout-master-set>
         
         <fo:page-sequence master-reference="normal">
            <fo:static-content flow-name="xsl-region-before">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <xsl:apply-templates select="./SectionA"/>
               </xsl:element>
            </fo:static-content>
            <fo:static-content flow-name="xsl-region-after">
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily">
                  <fo:block text-align="center">Page: <fo:page-number/></fo:block>
               </xsl:element>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body" >
               <xsl:element name="fo:wrapper" use-attribute-sets="DefaultFontFamily" >
                  <xsl:apply-templates select="./SectionB"/>
               </xsl:element>
            </fo:flow>
         </fo:page-sequence>
         
      </fo:root>
   </xsl:template>
   
   <xsl:template match="section-header" >
      <xsl:param name="start-indent"></xsl:param>
      <fo:block  background-color="#E7E7E7" height="4mm" text-align="left" >
         <fo:inline font-weight="bold"><xsl:value-of select="name"/></fo:inline>
         <fo:leader leader-length="20mm" leader-pattern="space"/>
         <fo:inline><xsl:value-of select="value"/></fo:inline>
      </fo:block>   
      <fo:block>
         <fo:leader leader-length="272mm" leader-pattern="rule" rule-thickness="1pt" color="black"/>
      </fo:block>
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="name" mode="block">
      <fo:block >
         <fo:inline font-weight="bold"><xsl:value-of select="."/> </fo:inline>
      </fo:block>   
   </xsl:template>  
   
   <xsl:template match="value" mode="block">
      <fo:block>
         <fo:inline><xsl:value-of select="."/></fo:inline>
      </fo:block>   
   </xsl:template>  
   
   <xsl:template match="name" mode="inline">
      <fo:inline font-weight="bold"><xsl:value-of select="."/> </fo:inline>
   </xsl:template>  
   
   <xsl:template match="value" mode="inline">
      <fo:inline><xsl:value-of select="."/></fo:inline>
   </xsl:template>  
<!--AAlubala IR#DAUM050538861 Wrap the values of the column - added split template node-->
   <xsl:template match="value" mode="split">
   <!-- BSL IR RNUM050251590 06/07/2012 Rel 8.0 BEGIN -->
         <!--<xsl:choose>
            <xsl:when test="string-length(.) &gt; 20">
               <xsl:call-template name="splitString">
                  <xsl:with-param name="value" select="."/>
                  <xsl:with-param name="startPosition" select="1"/>
                  <xsl:with-param name="endPosition" select="22"/>
               </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
               <fo:block>
               <fo:inline><xsl:value-of select="." /> </fo:inline>
                  </fo:block>
            </xsl:otherwise>
         </xsl:choose>-->
         <!-- PGedupudi IR#T36000039315 -->
      <xsl:param name="maxWordLength" select="15"/>
      <fo:block>
         <xsl:call-template name="splitString">
            <xsl:with-param name="value" select="."/>
            <xsl:with-param name="maxWordLength" select="$maxWordLength"/>
         </xsl:call-template>
      </fo:block>
   <!-- BSL IR RNUM050251590 06/07/2012 Rel 8.0 END -->
   </xsl:template>  
   
   <xsl:template name="recursive">
      <xsl:param name="posi"/>
      <xsl:param name="colCount"/>
      <xsl:param name="list"/>
      <xsl:param name="pAccum" select="0"/>
      
      <xsl:choose>
         <xsl:when test="$colCount != 0 ">
            <xsl:variable name="colNWidth">
               <xsl:for-each select="/Document/SectionB/ResultSetRecord/field[$posi]">
                  <xsl:sort select="nameSize" data-type="number" order="descending"/>
                  <xsl:if test="position() = 1">
                     <xsl:value-of select="nameSize"/>
                  </xsl:if>
               </xsl:for-each>               
               
            </xsl:variable>
            <xsl:variable name="colVWidth">
               <xsl:for-each select="/Document/SectionB/ResultSetRecord/field[$posi]">
                  <xsl:sort select="valueSize" data-type="number" order="descending"/>
                  <xsl:if test="position() = 1">
                     <xsl:value-of select="valueSize"/>
                  </xsl:if>
               </xsl:for-each>               
            </xsl:variable>
            <xsl:variable name="colSizeTemp">
               <xsl:if test="$colNWidth &gt; $colVWidth">
                  <xsl:value-of select="number($colNWidth) + 18"/>
               </xsl:if>
               <xsl:if test="$colNWidth &lt; $colVWidth">
                  <xsl:value-of select="number($colVWidth) +18"/>
               </xsl:if>
               <xsl:if test="$colNWidth = $colVWidth">
                  <xsl:value-of select="number($colVWidth) +18"/>
               </xsl:if>
            </xsl:variable>
            <xsl:call-template name="recursive">
               <xsl:with-param name="posi" select="number($posi+1)"/>
               <xsl:with-param name="pAccum"
                  select="$pAccum + $colSizeTemp"/>
               <xsl:with-param name="colCount" select="number($colCount -1)"> </xsl:with-param>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$pAccum"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
<!-- BSL IR RNUM050251590 06/07/2012 Rel 8.0 - rewrote and moved splitString to FOCommon.xsl -->
<!--AAlubala IR#DAUM050538861 Wrap the values of the column - changed from block to splitString node-->
   <!--<xsl:template name="splitString">
      <xsl:param name="value"/>
      <xsl:param name="startPosition"/>
      <xsl:param name="endPosition"/>
      <xsl:choose>
         <xsl:when test="string-length($value) &gt; $endPosition">
            <fo:block>
               <fo:inline><xsl:value-of select="substring($value,$startPosition,$endPosition)" /> </fo:inline>
            </fo:block>
            
            <xsl:call-template name="splitString">
               <xsl:with-param name="value" select="substring($value,number($endPosition)+1,string-length($value))"/>
               <xsl:with-param name="startPosition" select="$startPosition"/>
               <xsl:with-param name="endPosition" select="$endPosition"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <fo:block>
               <fo:inline><xsl:value-of select="$value" /> </fo:inline>
            </fo:block>   
         </xsl:otherwise>
      </xsl:choose>
      
   </xsl:template>-->
   
</xsl:stylesheet>