<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionG">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>    
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
		  <xsl:apply-templates/>      
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
    
   <xsl:template match="Document/SectionG/ShipmentDetails"> 
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Shipment Details: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionG/ShipmentDetails/FlightNumber">
      <fo:block>
         <fo:inline font-weight="bold">Carrier Name / Flight Number:   </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionG/ShipmentDetails/AirWaybillNumber">
      <fo:block>
         <fo:inline font-weight="bold">Air Waybill Number:   </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/GoodsDescription">
      <fo:block>
         <fo:inline font-weight="bold">Goods Description:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>