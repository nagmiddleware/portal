<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionH1">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>    
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
                  <xsl:apply-templates/>      
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionH1/LoanInstructions"> 
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Loan Instructions: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionH1/LoanInstructions/LoanProceeds">
      <fo:block>
         <fo:inline font-weight="bold">Loan Proceeds: </fo:inline>
			<xsl:apply-templates/>
		</fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionH1/LoanInstructions/LoanProceeds/CurrencyValueAmountInNumbersAndWords">
         <fo:inline><xsl:apply-templates/> </fo:inline>
   </xsl:template> 
 
   <xsl:template match="Document/SectionH1/LoanInstructions/TransactionFinanced">
      <fo:block>
         <fo:inline font-weight="bold">Transaction Being Financed: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>