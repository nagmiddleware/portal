<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="Document/SectionC/SettleInstr/Title">
		<fo:block><fo:leader leader-length="500pt" leader-pattern="space"/></fo:block>
       <fo:block>
          <fo:inline font-weight="bold" text-decoration="underline"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/PresentAmt">
       <fo:block>
          <fo:inline font-weight="bold">Presentation Amount: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
    <xsl:template match="Document/SectionC/SettleInstr/PresentNum">
       <fo:block>
          <fo:inline font-weight="bold">Presentation Number: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
    <xsl:template match="Document/SectionC/SettleInstr/PresentDate">
       <fo:block>
          <fo:inline font-weight="bold">Presentation Date: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/OtherParty">
       <fo:block>
          <fo:inline font-weight="bold">Other Party: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/ApplyPaymentOn">
       <fo:block>
          <fo:inline font-weight="bold">Apply Payment On: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/SubTitle">
		<fo:block><fo:leader leader-length="500pt" leader-pattern="space"/></fo:block>
       <fo:block>
          <fo:inline color="red" text-decoration="underline" font-style="italic"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/Discrepancies">
       <fo:block>
          <fo:inline font-weight="bold"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
</xsl:stylesheet>