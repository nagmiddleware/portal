<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <!--  RKazi CR-710 02/02/2012 Rel 8.0 Add -->
   <xsl:template match="Document/SectionA">
      <xsl:apply-templates select="./Title"/>
      <fo:block><fo:leader leader-pattern="space"/></fo:block>
      <fo:table >
         <fo:table-column column-width="200pt"/>
         <fo:table-column column-width="50pt"/>
         <fo:table-column column-width="250pt"/>
         <fo:table-body>
            <fo:table-row>
               <xsl:apply-templates select="./Logo"/>
               <fo:table-cell/>
               <xsl:apply-templates select="./TransInfo"/>         
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      
   </xsl:template>
   
   <xsl:template match="/Document/SectionA/Title">
      <xsl:apply-templates select="./Line1"/>
   </xsl:template>    
   
   <xsl:template match="/Document/SectionA/Title/Line1">
      <fo:block font-weight="bold" text-align="center" font-size="20pt"><xsl:value-of select="."/></fo:block>
   </xsl:template>
   
</xsl:stylesheet>