<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionB">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <xsl:apply-templates select="./Party[1]"/>
               </fo:table-cell>
            </fo:table-row>
	 </fo:table-body>
      </fo:table>     
   </xsl:template>  
   
   <xsl:template match="Document/SectionB/Party/Name">
      <fo:block><xsl:apply-templates/></fo:block>   
   </xsl:template>  
   
   <xsl:template match="Document/SectionB/Party/AddressLine1">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionB/Party/AddressLine2"> 
      <fo:block><xsl:apply-templates/></fo:block>   
   </xsl:template>     
   
   <xsl:template match="Document/SectionB/Party/AddressStateProvince">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>        
  
   <xsl:template match="Document/SectionB/Party/AddressCountryPostCode">
      <fo:block><xsl:apply-templates/></fo:block> 
   </xsl:template>    
   
   <xsl:template match="Document/SectionB/Party/PhoneNumber">
      <fo:block>
         <fo:inline font-weight="bold">Tel: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>   
   </xsl:template>     
      
   <xsl:template match="Document/SectionB/Party/Fax">
      <fo:block>
         <fo:inline font-weight="bold">Fax: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionB/Party/Swift">
      <fo:block>
         <fo:inline font-weight="bold">Swift: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionB/Party/Telex">
      <fo:block>
         <fo:inline font-weight="bold">Telex: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>