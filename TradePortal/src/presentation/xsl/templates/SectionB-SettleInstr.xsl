<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	
	<xsl:template match="Document/SectionB/Party/Label">
      <fo:block>
         <fo:inline></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionB/Party">
   		<fo:block><xsl:apply-templates select="./Name"/></fo:block>
		<fo:block><xsl:apply-templates select="./AddressLine1"/></fo:block>
		<fo:block><xsl:apply-templates select="./AddressLine2"/></fo:block>
		<fo:block><xsl:apply-templates select="./AddressStateProvince"/></fo:block> 
		<fo:block><xsl:apply-templates select="./AddressCountryPostCode"/></fo:block> 
			
   		<fo:block>
         	<fo:inline font-weight="bold">Tel: </fo:inline>
         	<fo:inline><xsl:apply-templates select="./PhoneNumber"/></fo:inline>
         	<fo:inline font-weight="bold"> Fax: </fo:inline>
         	<fo:inline><xsl:apply-templates select="./Fax"/></fo:inline>
      	</fo:block>
      	
      	<fo:block>
        	<fo:inline font-weight="bold">Swift: </fo:inline>
        	<fo:inline><xsl:apply-templates select="./Swift"/></fo:inline>
      	</fo:block>
   </xsl:template>   
      
</xsl:stylesheet>