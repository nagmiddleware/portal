<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:template match="Document/SectionS">
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionS/IssuingBank">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
               <fo:table-row> 
                  <fo:table-cell>
                     <fo:block>
                        <fo:leader leader-pattern="space"/>
                     </fo:block>
                     <fo:block font-weight="bold">
                        <fo:inline text-decoration="underline">Issuing Bank: </fo:inline>
                     </fo:block>  
                     <xsl:apply-templates/> 
   	          </fo:table-cell>
               </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionS/IssuingBank/IssuedBy">
      <fo:block>
         <fo:inline font-weight="bold">The instrument is to be issued by </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionS/IssuingBank/Date">
      <fo:block>
         <fo:inline font-weight="bold">Validity Date at Overseas Bank: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionS/IssuingBank/OverseasBank">
      <fo:block font-weight="bold">Overseas Bank:</fo:block>
      <fo:block><xsl:apply-templates select="./Name"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine1"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine2"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressStateProvince"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressCountryPostCode"/></fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionS/Validity">
      <fo:table>
         <fo:table-column column-width="300pt"/>
         <fo:table-column column-width="225pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
                  <fo:block font-weight="bold">
                     <fo:inline text-decoration="underline">New Validity: </fo:inline>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell/>
            </fo:table-row>     
            <fo:table-row>   
              	 <fo:table-cell>    
            	   	 <xsl:apply-templates select="./ValidFrom"/>    
            	 </fo:table-cell>       	
            </fo:table-row> 
            <fo:table-row> 
              <fo:table-cell>
            	   <xsl:apply-templates select="./ValidTo"/> 
             </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>      
   </xsl:template>   
   
   <xsl:template match="Document/SectionS/Validity/ValidFrom">
      <fo:block>
         <fo:inline font-weight="bold">Valid From: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionS/Validity/ValidTo">
      <fo:block>
         <fo:inline font-weight="bold">Valid To: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>     
   
   <xsl:template match="Document/SectionS/AgentDetails">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
                  <fo:block font-weight="bold">
                     <fo:inline text-decoration="underline">Agent Details: </fo:inline>
                  </fo:block>  
                  <xsl:apply-templates/> 
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
   
   <xsl:template match="Document/SectionS/AgentDetails/Agent">
      <fo:block font-weight="bold">Agent:</fo:block>
      <fo:block><xsl:apply-templates select="./Name"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine1"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressLine2"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressStateProvince"/></fo:block>
      <fo:block><xsl:apply-templates select="./AddressCountryPostCode"/></fo:block>
      <fo:block><xsl:apply-templates select="./ContactName"/></fo:block>
      <fo:block><xsl:apply-templates select="./PhoneNumber"/></fo:block>
   </xsl:template>   
    
   <xsl:template match="Document/SectionS/AgentDetails/Instructions">
      <fo:block>
         <fo:inline font-weight="bold">Agent Instructions: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>      
   
   <xsl:template match="Document/SectionS/ContractDetails">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Details of Tender/Order/Contract:</fo:inline>
      </fo:block>
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve"><xsl:apply-templates/></fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionS/Terms">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Terms:</fo:inline>
      </fo:block>
      <xsl:apply-templates/>
   </xsl:template>     
      
   <xsl:template match="Document/SectionS/Terms/Customer">
      <fo:block>
         <fo:inline font-weight="bold">Customer Entered Terms: </fo:inline>
      </fo:block>
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>      
     
   <xsl:template match="Document/SectionS/Terms/Bank">
      <fo:block>
         <fo:inline font-weight="bold">Bank Standard Terms: </fo:inline>
      </fo:block>
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
    
   <xsl:template match="Document/SectionS/BankInstructions">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Instructions to Bank:</fo:inline>
      </fo:block>
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <!-- Sandeep - Rel-8.3 CR-752 Dev 07/01/2013 - Begin -->
   <xsl:template match="Document/SectionS/ICCPublications">
      <fo:block>
        <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
        <fo:inline text-decoration="underline">ICC Publications:</fo:inline>
      </fo:block>    
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="Document/SectionS/ICCApplicableRules">
      <fo:block>
         <fo:inline font-weight="bold">ICC Applicable Rules: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionS/ICCPVersion">
      <fo:block>
         <fo:inline font-weight="bold">Version: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionS/ICCPDetails">
      <fo:block>
         <fo:inline font-weight="bold">Details: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <!-- Sandeep - Rel-8.3 CR-752 Dev 07/01/2013 - End -->
   <xsl:template match="Document/SectionS/AutoExtension/ExtAllowed">
      <fo:block>
        <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
        <fo:inline text-decoration="underline">Auto Extension Terms:</fo:inline>
      </fo:block>    
      <fo:block>
         <fo:inline font-weight="bold">Auto Extension: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <xsl:template match="Document/SectionS/AutoExtension/MaxExtAllowed">
      <fo:block>
         <fo:inline font-weight="bold">Maximum Number: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 

   <xsl:template match="Document/SectionS/AutoExtension/AutoExtPeriod">
      <fo:block>
         <fo:inline font-weight="bold">Extension Period: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionS/AutoExtension/AutoExtDays">
      <fo:block>
         <fo:inline font-weight="bold">Number of Days: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 

   <xsl:template match="Document/SectionS/AutoExtend">
      <fo:block>
         <fo:inline font-weight="bold">Auto Extend: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionS/AutoExtension/FinalExpiryDate">
      <fo:block>
         <fo:inline font-weight="bold">Final Expiry Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 

   <xsl:template match="Document/SectionS/AutoExtension/NotifyBeneDays">
      <fo:block>
         <fo:inline font-weight="bold">Notify Beneficiary Days: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
</xsl:stylesheet>