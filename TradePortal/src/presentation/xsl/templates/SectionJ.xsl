<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionJ">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="Document/SectionJ/AdditionalDocuments">
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Additional Documents:</fo:inline>
      </fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false">-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
      
   <xsl:template match="Document/SectionJ/DocumentsRequired">
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Documents Required:</fo:inline>
      </fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false">-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionJ/AmendmentDetails">
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Amendment Details:</fo:inline>
      </fo:block>
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
      <!--<fo:block white-space-collapse="false">-->
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
      <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
</xsl:stylesheet>