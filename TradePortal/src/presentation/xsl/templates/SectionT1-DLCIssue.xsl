<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionT1">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="Document/SectionT1/InstructionsToBank">
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Instructions to Bank:</fo:inline>
      </fo:block>
      <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionT1/InstructionsToBank/Issue">
      <fo:block>
         <fo:inline font-weight="bold">Issue instrument in: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   <xsl:template match="Document/SectionT1/InstructionsToBank/Additional">
      <fo:block>
         <fo:inline font-weight="bold">Additional Instructions:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>    
   
</xsl:stylesheet>