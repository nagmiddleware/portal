<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionU2">  
   <!-- 
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block> 
   -->     
      <fo:table text-align="justify">          
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
                  <xsl:apply-templates/>      
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
    
   <xsl:template match="Document/SectionU2/FXContract">       
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Forward Exchange Contract (FEC): </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>     
   <xsl:template match="Document/SectionU2/FXContract/FecNumber">
      <fo:block>
         <fo:inline font-weight="bold">FEC Number:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   <xsl:template match="Document/SectionU2/FXContract/Rate">
      <fo:block>
         <fo:inline font-weight="bold">Rate: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   <xsl:template match="Document/SectionU2/FXContract/Amount">
      <fo:block>
         <fo:inline font-weight="bold">Amount:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>     
   <xsl:template match="Document/SectionU2/FXContract/MaturityDate">
      <fo:block>
         <fo:inline font-weight="bold">Maturity Date:   </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>     
   
</xsl:stylesheet>