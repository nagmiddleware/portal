<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionH">     
      
      <fo:table>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>  
  
      <fo:table>
         <fo:table-column column-width="550pt"/>
         <fo:table-body>      
            <fo:table-row>      
		<xsl:apply-templates/>      
            </fo:table-row>              
         </fo:table-body>
      </fo:table>
            
   </xsl:template>
  
   
   <xsl:template match="Document/SectionH/PaymentTerms">
      <fo:table-cell>
         <fo:block font-weight="bold">
            <fo:inline text-decoration="underline">Payment Terms:</fo:inline>
         </fo:block>
         <fo:block font-weight="bold">
            <fo:inline>Available By:</fo:inline>
         </fo:block>        
         <xsl:apply-templates/>
      </fo:table-cell>
   </xsl:template>

   <xsl:template match="Document/SectionH/PaymentTerms/Sight">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionH/PaymentTerms/Special">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionH/PaymentTerms/TableLabel">
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionH/PaymentTerms/PayTermsTable">
     
      <fo:block>
      <fo:table text-align="center">
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell padding="6pt" >                 
                  <fo:block>
                     <fo:inline text-decoration="underline">
                        <xsl:apply-templates select="./RowEntry[1]/Column1Label"/>
                     </fo:inline>
                  </fo:block>                
               </fo:table-cell>
               <fo:table-cell padding="6pt" >
                  <fo:block><fo:inline text-decoration="underline"> Tenor Type</fo:inline></fo:block>
               </fo:table-cell>
               <fo:table-cell padding="6pt" >
                  <fo:block><fo:inline text-decoration="underline"> Tenor Details</fo:inline></fo:block>
               </fo:table-cell>    
               <fo:table-cell padding="6pt" >
                  <fo:block> <fo:inline text-decoration="underline">Maturity Date</fo:inline></fo:block>
               </fo:table-cell>             
            </fo:table-row>
            <xsl:apply-templates/>
         </fo:table-body>
      </fo:table>
      </fo:block>
   </xsl:template>   

    <xsl:template match="Document/SectionH/PaymentTerms/PayTermsTable/RowEntry">
      <fo:table-row>
               <fo:table-cell padding="6pt" >
                  <fo:block>
                     <xsl:apply-templates select="./Column1"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="6pt" >
                  <fo:block><xsl:apply-templates select="./TenorType"/> </fo:block>
               </fo:table-cell>
               <fo:table-cell padding="6pt" >
                  <fo:block><xsl:apply-templates select="./TenorDetails"/></fo:block>
               </fo:table-cell>     
               <fo:table-cell padding="6pt" >
                  <fo:block><xsl:apply-templates select="./MaturityDate"/></fo:block>
               </fo:table-cell>            
            </fo:table-row> 
   </xsl:template>  
   
   
</xsl:stylesheet>