<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:variable name="DefaultFonts" select="'Helvetica, ArialUnicodeMS'"/>
	<xsl:attribute-set name="DefaultFontFamily">
		<xsl:attribute name="font-family">
			<xsl:value-of select="$DefaultFonts"/>
		</xsl:attribute>
	</xsl:attribute-set>
</xsl:stylesheet>