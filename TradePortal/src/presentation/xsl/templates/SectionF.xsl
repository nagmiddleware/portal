<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionF">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="300pt"/>
         <fo:table-column column-width="225pt"/>
         <fo:table-body>      
            <fo:table-row>     
           	<fo:table-cell>
            	   <xsl:apply-templates select="./AmountDetail"/>
            	</fo:table-cell>    
            	<fo:table-cell>
            	   <xsl:apply-templates select="./Expiration"/>
            	</fo:table-cell>        
           </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail">
      <xsl:apply-templates/>
   </xsl:template>
            
   <xsl:template match="Document/SectionF/AmountDetail/IssueDate">
       <fo:block>
          <fo:inline font-weight="bold">Issue Date: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail/CurrencyCodeAmount">
      <fo:block>
         <fo:inline font-weight="bold">Amount:</fo:inline>
      </fo:block>
      <fo:block><xsl:apply-templates/></fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail/CurrencyValueAmountInWords">
       <fo:block>
          <fo:inline>[</fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
          <fo:inline>]</fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/AmountDetail/Type">
       <fo:block>
          <fo:inline font-weight="bold">Type: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/AmountDetail/Tolerance">
       <fo:block>
          <fo:inline font-weight="bold">Tolerance: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/AmountDetail/MaximumCreditAmount">
       <fo:block>
          <fo:inline font-weight="bold">Maximum Credit Amount: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/AmountDetail/AdditionalAmountsCovered">
       <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 BEGIN -->
       <!--<fo:block white-space-collapse="false">-->
       <fo:block white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
       <!-- BSL IR RSUL121604111 12/19/11 Rel 7.1 END -->
          <fo:inline font-weight="bold">Additional Amounts Covered: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail/AvailableBy">
       <fo:block>
          <fo:inline font-weight="bold">Available By: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionF/AmountDetail/AvailableWithParty">
       <fo:block>
          <fo:inline font-weight="bold">Available With Party: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/AmountDetail/DraftsRequired">
       <fo:block>
          <fo:inline font-weight="bold">Drafts Required: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/AmountDetail/DraftsDrawnOn">
       <fo:block>
          <fo:inline font-weight="bold">Drafts Drawn On: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>

   <xsl:template match="Document/SectionF/Expiration">
      <xsl:apply-templates/>
    </xsl:template>   

   <xsl:template match="Document/SectionF/Expiration/Date">
      <fo:block>
         <fo:inline font-weight="bold">Expiry Date: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/Expiration/Place">
      <fo:block>
         <fo:inline font-weight="bold">Expiry Place: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionF/Expiration/CharacteristicType">
      <fo:block>
         <fo:inline font-weight="bold">Type: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>

   
</xsl:stylesheet>