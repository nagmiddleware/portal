<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionE1">
      <fo:table>
         <fo:table-column column-width="500pt"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="300pt"/>
         
         <fo:table-body>      
            <fo:table-row>      
               <xsl:apply-templates/>      
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>
   
   
   
   <xsl:template match="Document/SectionE1/BankCharges">
      <fo:table-cell>
         <fo:block font-weight="bold">
            <fo:inline text-decoration="underline">Bank Charges:</fo:inline>
         </fo:block>
         <xsl:apply-templates/>
      </fo:table-cell>
   </xsl:template>
   
   <xsl:template match="Document/SectionE1/BankCharges/AllOurAccount">
      <fo:block>All for Applicant’s Account <xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionE1/BankCharges/AllBankCharges">
      <fo:block>All bank charges outside the country of issuance are for Beneficiary’s account <xsl:apply-templates/></fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionE1/BankCharges/Other">
      <fo:block>See Additional Conditions<xsl:apply-templates/></fo:block>
   </xsl:template>
   
</xsl:stylesheet>