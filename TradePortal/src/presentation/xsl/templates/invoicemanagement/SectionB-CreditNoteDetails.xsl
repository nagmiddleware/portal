<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
    <!--  RKazi CR-710 02/02/2012 Rel 8.0 Add -->
   <xsl:template match="Document/SectionB">
      
     <xsl:apply-templates select="./InvoiceDetails[1]/section-header" />
      
      <fo:table >
        <!-- <fo:table-column column-width="28mm" column-number="1"/>
         <fo:table-column column-width="28mm" column-number="2"/>
         <fo:table-column column-width="32mm" column-number="3"/>
         <fo:table-column column-width="30mm" column-number="4"/>
         <fo:table-column column-width="30mm" column-number="5"/>
         <fo:table-column column-width="30mm" column-number="6"/>
         <fo:table-column column-width="30mm" column-number="7"/>
         <fo:table-column column-width="30mm" column-number="8"/>
         <fo:table-column column-width="30mm" column-number="9"/>
         <fo:table-column column-width="30mm" column-number="10"/-->
         <xsl:for-each select="./InvoiceDetails[1]/*">
               <fo:table-column>
                  <xsl:attribute name="column-width">
                     <xsl:text>30mm</xsl:text>
                  </xsl:attribute>
                  <xsl:attribute name="column-number">
                     <xsl:value-of select="position()"/>
                  </xsl:attribute>
               </fo:table-column>
            </xsl:for-each>
         <fo:table-header>
            <fo:table-row>
               <xsl:for-each select="./InvoiceDetails[1]/field">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"></xsl:apply-templates>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
         </fo:table-header>
         
         <fo:table-body >
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="rule" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row >
               <xsl:for-each select="./InvoiceDetails[1]/field">
                  <fo:table-cell>
                  <!--AAlubala IR#DAUM050538861 Wrap the values of the column - changed from block to split mode-->
                    <!--  <xsl:apply-templates select="./value" mode="invSummSplit"/>-->
                     <fo:block wrap-option="wrap">
                	 <fo:inline><xsl:value-of select="./value" /> </fo:inline>
               		</fo:block>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
         </fo:table-body>
      </fo:table>     
   </xsl:template>  
   
</xsl:stylesheet>