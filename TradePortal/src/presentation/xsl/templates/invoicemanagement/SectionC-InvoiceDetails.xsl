<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <!--  RKazi CR-710 02/02/2012 Rel 8.0 Add -->   
   <xsl:template match="Document/SectionC">
      <fo:block height="4mm" width="180mm">
         <fo:leader leader-pattern="space"/>
      </fo:block>
      
      <fo:table>
         <fo:table-column column-width="131mm" column-number="1"/>
         <fo:table-column column-width="131mm" column-number="2"/>
         <fo:table-column column-width="10mm" column-number="3"/>
         
         <fo:table-body>
            <fo:table-row>
               <xsl:for-each select="./BuyerDetails[1]/section-header">
                  <fo:table-cell>
                     <xsl:apply-templates select="." mode="a"/>
                  </fo:table-cell>
               </xsl:for-each>
               <fo:table-cell>
                  <fo:block background-color="#E7E7E7" height="4mm">
                     <fo:leader leader-length="10mm" leader-pattern="space"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      <fo:table>
         <fo:table-column column-width="132mm" column-number="1"/>
         <fo:table-column column-width="138mm" column-number="2"/>
         <fo:table-body>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:leader leader-length="272mm" leader-pattern="rule" rule-thickness="1pt"
                        color="black"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <fo:table>
                        <fo:table-column column-width="32mm" column-number="1"/>
                        <fo:table-column column-width="82mm" column-number="2"/>
                        <fo:table-body>
                           <fo:table-row>
                              <fo:table-cell>
                                 <fo:block/>
                              </fo:table-cell>
                              <fo:table-cell>
                                 <fo:block/>
                              </fo:table-cell>
                           </fo:table-row>
                           <xsl:for-each select="./BuyerDetails[1]/field">
                             <xsl:if test="position() &lt; 5 ">
                                 <fo:table-row>
                                    <fo:table-cell>
                                       <fo:block>
                                          
                                             <xsl:apply-templates select="./name" mode="inline"/>
                                          
                                       </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                       <fo:block>
                                          <xsl:apply-templates select="./value" mode="split"/>
                                       </fo:block>
                                    </fo:table-cell>
                                 </fo:table-row>
                              </xsl:if>
                           </xsl:for-each>
                        </fo:table-body>
                     </fo:table>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <fo:table>
                        <fo:table-column column-width="48mm" column-number="1"/>
                        <fo:table-column column-width="84mm" column-number="2"/>
                        <fo:table-body>
                           <fo:table-row>
                              <fo:table-cell>
                                 <fo:block/>
                              </fo:table-cell>
                              <fo:table-cell>
                                 <fo:block/>
                              </fo:table-cell>                                 
                           </fo:table-row>
                           <xsl:for-each select="./BuyerDetails[1]/field">
                              <xsl:if test="position() &gt; 4">
                                 <fo:table-row>
                                    <fo:table-cell>
                                       <fo:block>
                                          <xsl:apply-templates select="./name" mode="inline"/>
                                       </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                       <fo:block>
                                          <xsl:apply-templates select="./value" mode="split"/>
                                       </fo:block>
                                    </fo:table-cell>
                                 </fo:table-row>
                              </xsl:if>
                           </xsl:for-each>
                        </fo:table-body>
                     </fo:table>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
      
   </xsl:template>
   
   <xsl:template match="section-header" mode="a">
      <fo:block background-color="#E7E7E7" height="4mm" width="185mm">
         <fo:inline font-weight="bold">
            <xsl:value-of select="name"/>
         </fo:inline>
         <fo:inline>
            <xsl:value-of select="value"/>
         </fo:inline>
         <fo:leader leader-length="10mm" leader-pattern="space"/>
      </fo:block>
   </xsl:template>
   
   
</xsl:stylesheet>