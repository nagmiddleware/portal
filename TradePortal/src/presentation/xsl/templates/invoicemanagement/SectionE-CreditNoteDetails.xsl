<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<!-- vdesingu CR-914A Rel 9.2 -->
	<xsl:template match="Document/SectionE">
		<fo:table>
			<fo:table-column column-width="270mm" column-number="1" />
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block height="4mm">
							<fo:leader leader-length="45mm" leader-pattern="space" />
						</fo:block>
						<xsl:apply-templates select="./CreditNotes/section-header" />
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:table>
								<fo:table-column column-width="74mm"
									column-number="1" />
								<fo:table-column column-width="50mm"
									column-number="2" />
								<fo:table-body>
									<fo:table-row>
										<xsl:for-each select="./CreditNotes/Label/*">
											<fo:table-cell>
												<xsl:apply-templates select="name" mode="block"></xsl:apply-templates>
											</fo:table-cell>
										</xsl:for-each>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:leader leader-length="272mm" leader-pattern="rule"
								rule-thickness="1pt" color="black" />
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>

		<fo:table>
			<fo:table-column column-width="74mm" column-number="1" />
			<fo:table-column column-width="50mm" column-number="2" />
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:table>
								<fo:table-body>
									<xsl:for-each select="./CreditNotes/InvoiceID/*">
										<fo:table-row>
											<fo:table-cell>
												<xsl:apply-templates select="value"
													mode="block"></xsl:apply-templates>
											</fo:table-cell>
										</fo:table-row>
									</xsl:for-each>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:table>
								<fo:table-body>
									<xsl:for-each select="./CreditNotes/CreditAmount/*">
										<fo:table-row>
											<fo:table-cell>
												<xsl:apply-templates select="value"
													mode="block"></xsl:apply-templates>
											</fo:table-cell>
										</fo:table-row>
									</xsl:for-each>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>