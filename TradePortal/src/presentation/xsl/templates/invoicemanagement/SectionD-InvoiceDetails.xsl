<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <!--  RKazi CR-710 02/02/2012 Rel 8.0 Add -->   
   <xsl:template match="Document/SectionD">
      <xsl:for-each select="./InvSummGoodsDetails">
         <fo:table>
            <fo:table-column column-width="272mm" column-number="1"/>
            <fo:table-body>
               <fo:table-row>
                  
                  <fo:table-cell text-align="center">
                     <fo:block height="4mm">
                        <fo:leader leader-length="45mm" leader-pattern="space"/>
                     </fo:block>
                     <xsl:apply-templates select="./section-header"/>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>
         <fo:table>
            <fo:table-column column-width="135mm" column-number="1"/>
            <fo:table-column column-width="135mm" column-number="2"/>
            <fo:table-body>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>
                        <fo:table>
                           <fo:table-column column-width="55mm" column-number="1"/>
                           <fo:table-column column-width="89mm" column-number="2"/>
                           <fo:table-body>
                              <fo:table-row>
                                 <fo:table-cell>
                                    <fo:block/>
                                 </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block/>
                                 </fo:table-cell>
                              </fo:table-row>
                              <xsl:for-each select="./field">
                                 <xsl:if test="position() &lt; 9">
                                    <fo:table-row>
                                       <fo:table-cell>
                                          <fo:block>
                                             <xsl:apply-templates select="./name" mode="inline"/>
                                          </fo:block>
                                       </fo:table-cell>
                                       <fo:table-cell>
                                          <xsl:choose>
                                             <xsl:when test="string-length(./value) &gt; 28">
                                                <xsl:apply-templates select="./value" mode="split"/>
                                             </xsl:when>
                                             <xsl:otherwise>
                                                <fo:block>
                                                   <xsl:apply-templates select="./value" mode="inline"/>
                                                </fo:block>
                                             </xsl:otherwise>
                                          </xsl:choose>
                                       </fo:table-cell>
                                    </fo:table-row>
                                 </xsl:if>
                              </xsl:for-each>
                           </fo:table-body>
                        </fo:table>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block>
                        <fo:table>
                           <fo:table-column column-width="55mm" column-number="1"/>
                           <fo:table-column column-width="82mm" column-number="2"/>
                           <fo:table-body>
                              <fo:table-row>
                                 <fo:table-cell>
                                    <fo:block/>
                                 </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block/>
                                 </fo:table-cell>                                 
                              </fo:table-row>
                              <xsl:for-each select="./field">
                                 <xsl:if test="position() &gt; 8">
                                    <fo:table-row>
                                       <fo:table-cell>
                                          <fo:block>
                                             <xsl:apply-templates select="./name" mode="inline"/>
                                          </fo:block>
                                       </fo:table-cell>
                                       <fo:table-cell>
                                        <!--  <xsl:choose>
                                             <xsl:when test="string-length(./value) &gt; 28">
                                                <xsl:apply-templates select="./value" mode="split"/>
                                             </xsl:when>
                                             <xsl:otherwise> -->
                                                <fo:block>
                                                   <xsl:apply-templates select="./value" mode="inline"/>
                                                </fo:block>
                                            <!-- </xsl:otherwise>
                                          </xsl:choose> -->
                                       </fo:table-cell>
                                    </fo:table-row>
                                 </xsl:if>
                              </xsl:for-each>
                           </fo:table-body>
                        </fo:table>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>
         <!-- Line Item Details -->
         <xsl:for-each select="./LineItems">
            <fo:block>
               <fo:leader leader-pattern="space"/>
            </fo:block>
            
            <fo:table>
               <fo:table-column column-width="0mm"/>
               <fo:table-column column-width="272mm"/>
               <fo:table-body>
                  <fo:table-row>
                     <fo:table-cell text-align="center">
                        <fo:block background-color="#E7E7E7" height="4mm">
                           <fo:leader leader-length="20mm" leader-pattern="space"/>
                        </fo:block>
                     </fo:table-cell>
                     <fo:table-cell text-align="center">
                        <xsl:apply-templates select="./section-header" mode="lineItem"/>
                     </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                     <fo:table-cell text-align="center">
                        <fo:block height="4mm">
                           <fo:leader leader-length="10mm" leader-pattern="rule"
                              rule-thickness="1pt" color="black"/>
                        </fo:block>
                     </fo:table-cell>
                     <fo:table-cell text-align="center">
                        <fo:block>
                           <fo:leader leader-length="272mm" leader-pattern="rule"
                              rule-thickness="1pt" color="black"/>
                        </fo:block>
                     </fo:table-cell>
                  </fo:table-row>
               </fo:table-body>
            </fo:table>
            <fo:table>
               <fo:table-column column-width="0mm"/>
               <fo:table-column column-width="131mm"/>
               <fo:table-column column-width="131mm"/>
               <fo:table-body>
                  <fo:table-row>
                     <fo:table-cell>
                        <fo:block/>
                     </fo:table-cell>
                     <fo:table-cell>
                        <fo:block>
                           <fo:table>
                              <fo:table-column column-width="72mm"/>
                              <fo:table-column column-width="72mm"/>
                              <fo:table-body>
                                 <fo:table-row>
                                    <fo:table-cell>
                                       <fo:block/>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                       <fo:block/>
                                    </fo:table-cell>
                                 </fo:table-row>
                                 <xsl:for-each select="./field">
                                    <xsl:if test="position() &lt; 9">
                                       <fo:table-row>
                                          <fo:table-cell>
                                             <fo:block>
                                                <xsl:apply-templates select="./name" mode="inline"/>
                                             </fo:block>
                                          </fo:table-cell>
                                          <fo:table-cell>
                                             <xsl:choose>
                                                <xsl:when test="string-length(./value) &gt; 28">
                                                   <xsl:apply-templates select="./value" mode="split"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                   <fo:block>
                                                      <xsl:apply-templates select="./value" mode="inline"/>
                                                   </fo:block>
                                                </xsl:otherwise>
                                             </xsl:choose>
                                          </fo:table-cell>
                                       </fo:table-row>
                                    </xsl:if>
                                 </xsl:for-each>
                              </fo:table-body>
                           </fo:table>
                        </fo:block>
                     </fo:table-cell>
                     <fo:table-cell>
                        <fo:block>
                           <fo:table>
                              <fo:table-column column-width="72mm"/>
                              <fo:table-column column-width="72mm"/>
                              <fo:table-body>
                                 <fo:table-row>
                                    <fo:table-cell>
                                       <fo:block/>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                       <fo:block/>
                                    </fo:table-cell>
                                 </fo:table-row>
                                 <xsl:for-each select="./field">
                                    <xsl:if test="position() &gt; 8">
                                       <fo:table-row>
                                          <fo:table-cell>
                                             <fo:block>
                                                <xsl:apply-templates select="./name" mode="inline"/>
                                             </fo:block>
                                          </fo:table-cell>
                                          <fo:table-cell>
                                             <xsl:choose>
                                                <xsl:when test="string-length(./value) &gt; 28">
                                                   <xsl:apply-templates select="./value" mode="split"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                   <fo:block>
                                                      <xsl:apply-templates select="./value" mode="inline"/>
                                                   </fo:block>
                                                </xsl:otherwise>
                                             </xsl:choose>
                                             
                                          </fo:table-cell>
                                       </fo:table-row>
                                    </xsl:if>
                                 </xsl:for-each>
                              </fo:table-body>
                           </fo:table>
                        </fo:block>
                     </fo:table-cell>
                  </fo:table-row>
               </fo:table-body>
            </fo:table>
         </xsl:for-each>
         <!-- Line Item Details -->
      </xsl:for-each>
   </xsl:template>
   
   <xsl:template match="section-header" mode="lineItem">
      <xsl:param name="start-indent"/>
      <fo:block background-color="#E7E7E7" height="4mm" text-align="left">
         <fo:inline font-weight="bold">
            <xsl:value-of select="name"/>
         </fo:inline>
         <fo:leader leader-length="20mm" leader-pattern="space"/>
         <fo:inline>
            <xsl:value-of select="value"/>
         </fo:inline>
      </fo:block>
   </xsl:template>
   
   
</xsl:stylesheet>