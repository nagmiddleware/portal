<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="Document/SectionC/SettleInstr/Title">
		<fo:block><fo:leader leader-length="500pt" leader-pattern="space"/></fo:block>
       <fo:block>
          <fo:inline font-weight="bold" text-decoration="underline"><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/PresentAmt">
       <fo:block>
          <fo:inline font-weight="bold">Presentation Amount: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/LoanStartDate">
       <fo:block>
          <fo:inline font-weight="bold">Loan Start Date: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/MaturityDate">
       <fo:block>
          <fo:inline font-weight="bold">Maturity Date: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/LoanTerms">
       <fo:block>
          <fo:inline font-weight="bold">Loan Terms: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/OtherParty">
       <fo:block>
          <fo:inline font-weight="bold">Other Party: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
   <xsl:template match="Document/SectionC/SettleInstr/ApplyPaymentOn">
       <fo:block>
          <fo:inline font-weight="bold">Apply Payment On: </fo:inline>
          <fo:inline><xsl:apply-templates/></fo:inline>
       </fo:block>
   </xsl:template>
</xsl:stylesheet>