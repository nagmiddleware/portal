<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="Document/SectionG">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>    
      <fo:table text-align="justify">
         <fo:table-column column-width="500pt"/>
         <fo:table-body>      
            <fo:table-row> 
               <fo:table-cell>     
		  <xsl:apply-templates/>      
	       </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>
   </xsl:template>   
    
   <xsl:template match="Document/SectionG/ShipmentDetails"> 
      <fo:block>
         <fo:inline font-weight="bold" text-decoration="underline">Shipment Details: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="Document/SectionG/ShipmentDetails/VesselNameNumber">
      <fo:block>
         <fo:inline font-weight="bold">Vessel Name / Voyage Number:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>   
   
   <xsl:template match="Document/SectionG/ShipmentDetails/BillOfLadingNumber">
      <fo:block>
         <fo:inline font-weight="bold">Bill of Lading Number:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/ContainerNumber">
      <fo:block>
         <fo:inline font-weight="bold">Container Number: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template>  
   
   <xsl:template match="Document/SectionG/ShipmentDetails/FromPortOfLoading">
      <fo:block>
         <fo:inline font-weight="bold">From: Port of Loading: </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/ToPortOfLoading">
      <fo:block>
         <fo:inline font-weight="bold">To: Port of Discharge:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
   <xsl:template match="Document/SectionG/ShipmentDetails/GoodsDescription">
      <fo:block>
         <fo:inline font-weight="bold">Goods Description:  </fo:inline>
         <fo:inline><xsl:apply-templates/></fo:inline>
      </fo:block>
   </xsl:template> 
   
</xsl:stylesheet>