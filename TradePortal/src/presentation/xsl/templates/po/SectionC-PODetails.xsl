<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <!--  RKazi CR-707 04/23/2012 Rel 8.0 Add -->   
   <xsl:template match="Document/SectionC">
      <xsl:apply-templates select="./POSummDetails[1]/section-header" />
      
      <fo:table >
         <!--<fo:table-column column-width="45mm" column-number="1"/>-->
         <fo:table-column column-width="40mm" column-number="1"/>
         <fo:table-column column-width="45mm" column-number="2"/>
         <fo:table-column column-width="45mm" column-number="3"/>
         <fo:table-column column-width="45mm" column-number="4"/>
         <fo:table-column column-width="45mm" column-number="5"/>
         <fo:table-column column-width="45mm" column-number="6"/>
         
         <!-- this table displays the PO summary fields other than buyer and seller user defined fields -->
         <fo:table-body >
            <xsl:if test="count(./POSummDetails[1]/field[position() &lt; 4]) &gt; 0">         
               <fo:table-row>
                  <xsl:for-each select="./POSummDetails[1]/field[position() &lt; 4]">
                     <fo:table-cell>
                        <xsl:apply-templates select="./name" mode="block"/>
                     </fo:table-cell>
                     <fo:table-cell>
                        <xsl:apply-templates select="./value" mode="split"/>
                     </fo:table-cell>
                  </xsl:for-each>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </xsl:if>
            <!--<xsl:if test="count(./POSummDetails[1]/field[position() &gt; 3 and position() &lt; 5]) &gt; 0">
               <fo:table-row>
                  <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 3 and position() &lt; 5]">
                     <fo:table-cell>
                        <xsl:apply-templates select="./name" mode="block"/>
                     </fo:table-cell>
                     <fo:table-cell>
                        <xsl:apply-templates select="./value" mode="split"/>
                     </fo:table-cell>
                  </xsl:for-each>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </xsl:if>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>-->
         </fo:table-body>
      </fo:table>     
      <xsl:if test="count(./POSummDetails[1]/field[position() = 4]) &gt; 0">
         <fo:table>
            <fo:table-column column-width="40mm" column-number="1"/>
            <fo:table-column column-width="225mm" column-number="2"/>
            <!-- this table displays the PO summary goods description -->
            <fo:table-body >
               <fo:table-row>
                  <fo:table-cell>
                     <xsl:apply-templates select="./POSummDetails[1]/field[position() = 4]/name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./POSummDetails[1]/field[position() = 4]/value" mode="split">
                        <xsl:with-param name="maxWordLength" select="75"/>
                     </xsl:apply-templates>
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>
      </xsl:if>
      <!-- this table displays the PO summary fields like buyer and seller user defined fields -->
      <fo:table >
         <!--<fo:table-column column-width="45mm" column-number="1"/>-->
         <fo:table-column column-width="40mm" column-number="1"/>
         <fo:table-column column-width="45mm" column-number="2"/>
         <fo:table-column column-width="45mm" column-number="3"/>
         <fo:table-column column-width="45mm" column-number="4"/>
         <fo:table-column column-width="45mm" column-number="5"/>
         <fo:table-column column-width="45mm" column-number="6"/>
         
         <fo:table-body >
            <xsl:if test="count(./POSummDetails[1]/field[position() &gt; 4 and position() &lt; 8]) &gt; 0">
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            <fo:table-row>
               <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 4 and position() &lt; 8]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>

            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            </xsl:if>
            <xsl:if test="count(./POSummDetails[1]/field[position() &gt; 7 and position() &lt; 11]) &gt; 0">
            <fo:table-row>
               <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 7 and position() &lt; 11]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>

            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            </xsl:if>
            <xsl:if test="count(./POSummDetails[1]/field[position() &gt; 10 and position() &lt; 14]) &gt; 0">
               <fo:table-row>
               <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 10 and position() &lt; 14]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>

            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            </xsl:if>
            <xsl:if test="count(./POSummDetails[1]/field[position() &gt; 13 and position() &lt; 17]) &gt; 0">
            <fo:table-row>
               <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 13 and position() &lt; 17]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            </xsl:if>
            <xsl:if test="count(./POSummDetails[1]/field[position() &gt; 16 and position() &lt; 20]) &gt; 0">
            <fo:table-row>
               <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 16 and position() &lt; 20]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            </xsl:if>
            <xsl:if test="count(./POSummDetails[1]/field[position() &gt; 19 and position() &lt; 23]) &gt; 0">
            <fo:table-row>
               <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 19 and position() &lt; 23]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            </xsl:if>
            <xsl:if test="count(./POSummDetails[1]/field[position() &gt; 22 and position() &lt; 25]) &gt; 0">
            <fo:table-row>
               <xsl:for-each select="./POSummDetails[1]/field[position() &gt; 22 and position() &lt; 25]">
                  <fo:table-cell>
                     <xsl:apply-templates select="./name" mode="block"/>
                  </fo:table-cell>
                  <fo:table-cell>
                     <xsl:apply-templates select="./value" mode="split"/>
                  </fo:table-cell>
               </xsl:for-each>
            </fo:table-row>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
            </xsl:if>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block><fo:leader leader-length="272mm" leader-pattern="space" rule-thickness="1pt" color="black"/></fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-body>
      </fo:table>     
   </xsl:template>  
   
   <xsl:template match="section-header" mode="a">
      <fo:block background-color="#E7E7E7" height="4mm" width="185mm">
         <fo:inline font-weight="bold">
            <xsl:value-of select="name"/>
         </fo:inline>
         <fo:inline>
            <xsl:value-of select="value"/>
         </fo:inline>
         <fo:leader leader-length="10mm" leader-pattern="space"/>
      </fo:block>
   </xsl:template>
   
   
</xsl:stylesheet>
