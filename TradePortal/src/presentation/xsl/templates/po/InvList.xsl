<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="../FOCommon.xsl"/>
   <xsl:template name="InvList">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block font-weight="bold">
         <fo:inline text-decoration="underline">Invoice List:</fo:inline>
      </fo:block>
      <fo:block font-family="Courier" font-size="10pt" font-weight="bold" white-space-collapse="false" white-space-treatment="preserve" linefeed-treatment="preserve">
         <fo:table font-size="9pt">
            <xsl:for-each select="ResultSetRecord[1]/*">
               <fo:table-column>
                  <xsl:attribute name="column-width">
                     <xsl:text>29mm</xsl:text>
                  </xsl:attribute>
                  <xsl:attribute name="column-number">
                     <xsl:value-of select="position()"/>
                  </xsl:attribute>
               </fo:table-column>
            </xsl:for-each>
            <fo:table-header>
               <fo:table-row>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block>
                        <xsl:text>Invoice ID</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block>
                        <xsl:text>Issue Date</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block>
                        <xsl:text>Due/Payment Date</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block>
                        <xsl:text>CCY</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block>
                        <xsl:text>Amount</xsl:text>
                     </fo:block>
                  </fo:table-cell>
                  <fo:table-cell background-color="#EEEECC">
                     <fo:block>
                        <xsl:text>Seller</xsl:text>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>
                        <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black">
                           <xsl:attribute name="leader-length">
                              <xsl:value-of select="174"/>
                              <xsl:text>mm </xsl:text>
                           </xsl:attribute>
                        </fo:leader>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-header>
            <fo:table-body>
               <xsl:apply-templates select="ResultSetRecord" mode="POListRow"/>
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>
   <xsl:template match="ResultSetRecord" mode="POListRow">
      <fo:table-row>
         <xsl:if test="(position() mod 2) = 0">
            <xsl:attribute name="background-color">#E7E7E7</xsl:attribute>
         </xsl:if>
         <xsl:apply-templates select="*" mode="POListRow"/>
      </fo:table-row>
      <fo:table-row>
         <fo:table-cell>
            <fo:block>
               <fo:leader leader-pattern="rule" rule-thickness="1pt" color="black">
                  <xsl:attribute name="leader-length">
                     <xsl:value-of select="174"/>
                     <xsl:text>mm </xsl:text>
                  </xsl:attribute>
               </fo:leader>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
   <xsl:template match="*" mode="POListRow">
      <fo:table-cell>
         <fo:block>
            <xsl:call-template name="splitString">
               <xsl:with-param name="value" select="."/>
               <xsl:with-param name="maxWordLength" select="13"/>
            </xsl:call-template>
         </fo:block>
      </fo:table-cell>
   </xsl:template>
</xsl:stylesheet>