/***************************************************************************/
/*  This script creates DB change control table, which is used to ensure any*/
/*  DB change script is run only once and any execution errors are logged. */
/*  This script file is included by another file and within a PL/SQL block */
/*  Therefore there is no / ending the PL/SQL here                         */
/***************************************************************************/


DECLARE
  MAX_DB_CHANGE_ID_LEN INTEGER;
  MAX_DB_CHANGE_ERR_MSG_LEN INTEGER;
  MAX_DB_CHANGE_COMMENTS_LEN INTEGER;
  STATUS_PENDING char(3);
  STATUS_PROCESSED char(3);
  STATUS_FAILED CHAR(3);
  STATUS_ROLLBACKED char(3);
  STATUS_FAILEDROLLBACK CHAR(3);

BEGIN

  MAX_DB_CHANGE_ID_LEN := 30;
  MAX_DB_CHANGE_ERR_MSG_LEN := 1000;
  MAX_DB_CHANGE_COMMENTS_LEN := 100;
  STATUS_PENDING := 'PND';
  STATUS_PROCESSED := 'PRC';
  STATUS_FAILED := 'FLD';
  STATUS_ROLLBACKED := 'ROL';
  STATUS_FAILEDROLLBACK := 'RFL';

  /* Where this script is called should have control not to create the table again if it already exists. */
  /* EXECUTE IMMEDIATE   */
  /* 'DROP TABLE db_change'; */

/**************************************************************************/
/*  CREATE TABLE */
/**************************************************************************/
  EXECUTE IMMEDIATE
    'CREATE TABLE db_change
      (id VARCHAR2( ' || TO_CHAR(MAX_DB_CHANGE_ID_LEN) || ') not null,
       change_time date,
       status CHAR(3),
       comments VARCHAR2(' || TO_CHAR(MAX_DB_CHANGE_COMMENTS_LEN) || '),
       error_message VARCHAR2(' || TO_CHAR(MAX_DB_CHANGE_ERR_MSG_LEN) || '))';

  EXECUTE IMMEDIATE
    'ALTER TABLE db_change
     ADD     (CONSTRAINT db_change PRIMARY KEY (id))';

/***************************************************************************/
/*  Define Functions  */
/***************************************************************************/

/* Whether a change exists in the db_change, regardless whether it has been successful */
  EXECUTE IMMEDIATE
    'CREATE OR REPLACE FUNCTION ChangeExists(my_id VARCHAR) RETURN BOOLEAN IS
       change_already_exists INTEGER;
    BEGIN
       select count(*)
       into change_already_exists
       from db_change
       where id = my_id;

       if change_already_exists = 0 then
          return FALSE;
       else
          return TRUE;
       end if;
    END;';

/* Whether a change is successfully done already */
  EXECUTE IMMEDIATE
    'CREATE OR REPLACE FUNCTION ChangeIsDone(my_id VARCHAR) RETURN BOOLEAN IS
       change_done INTEGER;
    BEGIN
       select count(*)
       into change_done 
       from db_change
       where id = my_id
         and (status = ''' || STATUS_PROCESSED || ''' or status = ''' || STATUS_FAILEDROLLBACK || ''');

       if change_done = 0 then
          return FALSE;
       else
          return TRUE;
       end if;
    END;';

/* Whether it is OK to start a change, i.e. if the change does not exist yet or it has failed*/
/* Update comments if necessary */
     EXECUTE IMMEDIATE
    'CREATE OR REPLACE FUNCTION OKToStartChange(my_id VARCHAR2, my_comments VARCHAR2) RETURN INTEGER IS 
    BEGIN
       IF NOT ChangeExists(my_id) THEN
          insert into db_change values (my_id, SYSDATE, ''' || STATUS_PENDING || ''', my_comments, NULL);
          commit;
          return 1;
       ELSE
          UPDATE db_change set comments = my_comments where id = my_id and comments <> my_comments;
          commit;
          IF NOT ChangeIsDone(my_id) THEN
             return 1;
          ELSE
             return 0;
          END IF;
       END IF;
    EXCEPTION
       WHEN DUP_VAL_ON_INDEX THEN
          return 0;
    END;';

/* Register the successful execution of a change*/
  EXECUTE IMMEDIATE
    'CREATE OR REPLACE FUNCTION RegisterSuccessfulChange(my_id VARCHAR2) RETURN INTEGER IS 
    BEGIN
       if OKToStartChange(my_id, '''') <= 0 then
          return -1;
       END IF;
       UPDATE db_change
          SET change_time = SYSDATE, status = ''' || STATUS_PROCESSED || ''', error_message = '' 
       WHERE id = my_id;
       commit;
       return 1;

    EXCEPTION
       WHEN OTHERS THEN
          return -1;
    END;';


/* Register the failure of a change */
  EXECUTE IMMEDIATE
    'CREATE OR REPLACE FUNCTION RegisterFailedChange(my_id VARCHAR2, my_error_message VARCHAR2) RETURN INTEGER IS 
       my_error_message2 VARCHAR2(' || TO_CHAR(MAX_DB_CHANGE_ERR_MSG_LEN) || ');
     BEGIN
       Rollback;
       if Not ChangeExists(my_id) then  
          if OKToStartChange(SUBSTR(my_id, 1, ' || TO_CHAR(MAX_DB_CHANGE_ID_LEN) || '), '''') <= 0 then
             return -1;
          END IF;
       end if; /*If the change does exist, do not check its status, overwrite with new error */
       my_error_message2 := SUBSTR(my_error_message, 1, ' || TO_CHAR(MAX_DB_CHANGE_ERR_MSG_LEN) || ');
       UPDATE db_change
          SET change_time = SYSDATE, status = ''' || STATUS_FAILED || ''', error_message=my_error_message2
        WHERE id = SUBSTR(my_id, 1, ' || TO_CHAR(MAX_DB_CHANGE_ID_LEN) || ');
       commit;
       return 1;
       /* No exception handler here since RegisterFailedChange itself is used in exception */
     END;';


/* Register the cancellation of a change */
  EXECUTE IMMEDIATE
    'CREATE OR REPLACE FUNCTION RegisterRollbackedChange(my_id VARCHAR2) RETURN INTEGER IS 
    BEGIN
       UPDATE db_change
          SET change_time = SYSDATE, status = ''' || STATUS_ROLLBACKED || '''
       WHERE id = my_id;
       commit;
       return 1;

    EXCEPTION
       WHEN OTHERS THEN
          return -1;
    END;';


/* Register the failure of rolling back a change */
  EXECUTE IMMEDIATE
    'CREATE OR REPLACE FUNCTION RegisterFailedChangeRollback(my_id VARCHAR2, my_error_message VARCHAR2) RETURN INTEGER IS 
    my_error_message2 VARCHAR2(' || TO_CHAR(MAX_DB_CHANGE_ERR_MSG_LEN) || ');
    BEGIN
       my_error_message2 := SUBSTR(my_error_message, 1, ' || TO_CHAR(MAX_DB_CHANGE_ERR_MSG_LEN) || ');
       UPDATE db_change
          SET change_time = SYSDATE, status = ''' || STATUS_FAILEDROLLBACK || ''', error_message=my_error_message2
       WHERE id = SUBSTR(my_id, 1, ' || TO_CHAR(MAX_DB_CHANGE_ID_LEN) || ');
       commit;
       return 1;
       /* No exception handler here since RegisterFailedChangeCancel itself is used in exception */
    END;';

END;



