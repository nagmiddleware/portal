
SET LINESIZE 300

PROMPT db_change statistics for the changes made within the last two hours, which are
PROMPT generally the changes made by this script execution.

select sum(decode(status, 'PRC', 0, 'ROL', 0, 1)) as COUNT_FTL_FLD, 
sum(decode(status, 'PRC', 1, 0)) as COUNT_PRC, 
sum(decode(status, 'ROL', 1, 0)) as COUNT_FLD_FTL,
count(*) as COUNT_TOTAL
from db_change 
where change_time >= sysdate - 2/24 and change_time <= sysdate ;

select id, substr(comments, 1, 20) as Comments, status, to_char(change_time, 'DD-MON-YYYY HH24:MI:SS') as Change_time, substr(error_message, 1, 100) as Error_message
from db_change
where status<>'PRC' and status <> 'ROL'
and change_time >= sysdate - 2/24 and change_time <= sysdate 
order by change_time;

