PROMPT Rollback &1

DECLARE
  db_change_id db_change.id%TYPE; 
  db_change_my_sqlerrm db_change.error_message%TYPE;
  db_change_return_value INTEGER;
BEGIN
  BEGIN 
     db_change_id := '&1' ;
  EXCEPTION
    WHEN OTHERS THEN
      return;
  END;

  IF ChangeIsDone(db_change_id) then 
