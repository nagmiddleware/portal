
/**************************************************************************/
/* This SQL Script File change Trade Portal Database for Portal 9.4.0.0   */
/* For more information on how this script should be maintained, refer to */
/* the documents titled "PPX DB Change Control Procedure"                 */
/*                                                                        */
/* QUICK NOTES                                                            */
/* 1. Wrap changes in @util/CHANGE_BEGIN and @util/CHANGE_END             */
/* 2. Use EXECUTE IMMEDIATE for DDL                                       */
/* 3. Separate DDL and DML                                                */
/* 4.                                                                     */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                    TEST YOUR SCRIPT!!                                  */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                    TEST YOUR SCRIPT!!                                  */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*                    TEST YOUR SCRIPT!!                                  */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*    To test the script, in Command Window under the directory of the    */
/*    script, run the following on local DB                               */
/*      SQLPLUS tpuser/tpuser  @TradePortalChanges.sql                    */
/*    Then examine the output on command window.  Check db_change table   */
/*    for your change.  Make sure its status = PRC.  Check DB for your    */
/*    specific change.  Make sure your change has taken effect            */
/* 5. Use CHAR for the following cases.  Use VARCHAR2 for all other       */
/*    character datatypes.  Do not use VARCHAR.                           */
/*     - CHAR(10) for UOID                                                */
/*     - CHAR(1)                                                          */
/*     - If the value in the column is always full-length (or null)       */
/*     - To match a referenced existing column.                           */
/* 6. Specify NOT NULL on the Primary key column explicitly. Othrwise the */
/*    primary key is not guaranteed to be not null.                       */
/* 7. All the primary keys should be named.  For example:                 */
/*       CONSTRAINT PK_XXX PRIMARY KEY (UOID)                             */
/* 8. Use default value where applicable, especially for Y/N indicators.  */
/*    NULL value should not take on the meaning of a default value.       */
/* 9. Think about index for performance.  Parent pointer (p_xxx) may need */
/*    one.                                                                */
/*                                                                        */
/*                                                                        */
/**************************************************************************/

/* Do not display the substitution variables for less cluttered results.  */
SET VERIFY OFF 
/* Do not display feedback like 'PL/SQL procedure successfully completed'.*/
/* SET FEEDBACK OFF */
/* Allow DBMS_OUTPUT.PUTLINE being display on stdout */
SET SERVEROUTPUT ON

/* Get timing info to track how long each script takes */
SET TIMING ON

/* spool output to file */
column systime new_value spooltimestamp
select to_char(sysdate,'YYYYMMDDHH24MISS') systime from dual;
spool TradePortalChanges.&spooltimestamp..lst
select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') begintime from dual;

/***********************************************************/
/* Create the DB change table if it does not already exist */
/***********************************************************/
select decode(count(*), 0, 'No                   ', 'Yes                  ') 
       as does_db_change_exists
from user_tables
where table_name = 'DB_CHANGE';

DECLARE
  db_change_exists INTEGER;
BEGIN
  SELECT count(*) 
  INTO   db_change_exists
  FROM   user_tables
  where  table_name = 'DB_CHANGE';
  
  IF db_change_exists = 0 THEN
     @util/CreateDBChange.sql
  END IF;

END;
/

/*******ADD YOUR CHANGE AFTER THIS LINE **************************************/



/*******ADD YOUR CHANGE BEFORE THIS LINE **************************************/



/******************************************************************************/
/* Queries db_change table and list any errors (exception)                    */
/* thrown during the execution of this script file.                           */
/* Syntax errors go to stdout                                                 */
/******************************************************************************/
SET LINESIZE 300
@util/Show_DB_Change.sql

select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') endtime from dual;

quit;

