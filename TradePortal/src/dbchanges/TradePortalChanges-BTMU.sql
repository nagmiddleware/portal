/**************************************************************************/
/* This SQL Script File change Trade Portal Database for version 9.3.5.0. */
/* For more information on how this script should be maintained, refer to */
/* the documents titled "PPX DB Change Control Procedure"                 */
/*                                                                        */
/* QUICK NOTES                                                            */
/* 1. Wrap changes in @util/CHANGE_BEGIN and @util/CHANGE_END             */
/* 2. Use EXECUTE IMMEDIATE for DDL                                       */
/* 3. Separate DDL and DML                                                */
/* 3. TEST YOUR SCRIPT!!                                                  */
/*    e.g. In Command Window under the directory of the script run        */
/*    SQLPLUS tpuser/tpuser  @TradePortalChanges.sql                      */
/**************************************************************************/

/* Do not display the substitution variables for less cluttered results.  */
SET VERIFY OFF 
/* Do not display feedback like 'PL/SQL procedure successfully completed'.*/
/* SET FEEDBACK OFF */
/* Allow DBMS_OUTPUT.PUTLINE being display on stdout */
SET SERVEROUTPUT ON

/* Get timing info to track how long each script takes */
SET TIMING ON

/* spool output to file */
column systime new_value spooltimestamp
select to_char(sysdate,'YYYYMMDDHH24MISS') systime from dual;
spool TradePortalChangesANZ.&spooltimestamp..lst
select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') begintime from dual;

/***********************************************************/
/* Create the DB change table if it does not already exist */
/***********************************************************/
select decode(count(*), 0, 'No                   ', 'Yes                  ') 
       as does_db_change_exists
from user_tables
where table_name = 'DB_CHANGE';

DECLARE
  db_change_exists INTEGER;
BEGIN
  SELECT count(*) 
  INTO   db_change_exists
  FROM   user_tables
  where  table_name = 'DB_CHANGE';
  
  IF db_change_exists = 0 THEN
     @util/CreateDBChange.sql
  END IF;

END;
/



/******ADD YOUR CHANGE AFTER THIS LINE*****************************************/


/*******ADD YOUR CHANGE BEFORE THIS LINE **************************************/

/******************************************************************************/
/* Queries db_change table and list any errors (exception)                    */
/* thrown during the execution of this script file.                           */
/* Syntax errors go to stdout                                                 */
/******************************************************************************/
SET LINESIZE 300
@util/Show_DB_Change.sql

select to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') endtime from dual;

quit;
