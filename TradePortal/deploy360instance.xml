<project name="TradePortal" default="all" basedir=".">

  <!-- Project directories -->
  <property name="config.dir" value="Configuration"/>
  <property name="deploy.dir" value="Deployment"/>
  <property name="prep.dir" value="Preparation"/>
  <property name="scripts.dir" value="Scripts"/>

  <property name="tp.agent" value="PortalAgent"/>
  <property name="tp" value="TradePortal"/>

  <target name="TradePortal" depends="prepare-TradePortal, remove-optional-TradePortal, deploy-app-specific-html-TradePortal, 
                copy-static-TradePortal, deploy-TradePortal, cleanup-TradePortal" if="env.name">
  </target>

  <target name="TradePortalPropertyUpdate" if="env.name">
      <!-- Jar up appSpecific -->
      <mkdir dir="${deploy.dir}/TradePortal/${env.name}/temp/lib"/>
      <jar jarfile="${deploy.dir}/TradePortal/${env.name}/temp/lib/appSpecific.jar" 
           basedir="${config.dir}/TradePortal/${env.name}"
           whenempty="fail">       
          <include name="*.properties"/>
      </jar>      
      <jar jarfile="${deploy.dir}/TradePortal/${env.name}/tradeportal.ear"
           basedir="${deploy.dir}/TradePortal/${env.name}/temp"
           update="yes">
          <include name="**/*.jar"/>
      </jar>
      <delete dir="${deploy.dir}/TradePortal/${env.name}/temp"/>
  </target>


  <!-- This target should be used when a new set of images or other static
       content needs to be updated in the portal without deploying a new
       build.  If this is run in production, the static content must be updated
       on the web server machines as well -->
  <target name="updateWithStatic">
      <mkdir dir="${prep.dir}/TradePortal/${env.name}/expandedear"/>
      <mkdir dir="${prep.dir}/TradePortal/${env.name}/portal"/>
      <mkdir dir="${deploy.dir}/TradePortal/${env.name}/temp/war" />
	
      <echo message="**********************************************" />
      <echo message="* Updating Static Files for ${env.name}" />
      <echo message="*" />
      <tstamp>
        <format property="current.time" pattern="dd MMMM yyyy hh:mm:ss aa" />
      </tstamp>
      <echo message="* Date/Time is: ${current.time}" />
      <echo message="*" />
      <echo message="**********************************************" />

      <!-- Extract the war file from the deployed ear file-->
      <unjar   src="${deploy.dir}/TradePortal/${env.name}/tradeportal.ear" 
               dest="${prep.dir}/TradePortal/${env.name}/expandedear">
      </unjar>

      <!-- Extract files from the war -->
      <unjar src="${prep.dir}/TradePortal/${env.name}/expandedear/war/tradeportal.war"
             dest="${prep.dir}/TradePortal/${env.name}/portal"/>

      <!-- Extract files from the source file containing the updates -->
      <unjar src="${updated.static.path}"
             dest="${prep.dir}/TradePortal/${env.name}/portal"/>  

      <!-- Jar up the war file after updates -->
      <jar jarfile="${deploy.dir}/TradePortal/${env.name}/temp/war/tradeportal.war" 
           basedir="${prep.dir}/TradePortal/${env.name}/portal"
           whenempty="fail">       
          <include name="**/*.*"/>
      </jar>

      <!-- Include the new war files in the EAR -->
      <jar jarfile="${deploy.dir}/TradePortal/${env.name}/tradeportal.ear" 
           basedir="${deploy.dir}/TradePortal/${env.name}/temp"
           whenempty="fail"
           update="true">       
          <include name="**/*.*"/>
      </jar>

      <!-- Remove all of the temporary directories that were created -->
      <delete dir="${prep.dir}/TradePortal/${env.name}/expandedear"/>       
      <delete dir="${prep.dir}/TradePortal/${env.name}/portal"/> 
      <delete dir="${deploy.dir}/TradePortal/${env.name}/temp"/>
  </target>

  <target name="prepare-TradePortal">
      <delete dir="${deploy.dir}/TradePortal/${env.name}"/>
      <mkdir dir="${deploy.dir}/TradePortal/${env.name}"/>
      <mkdir dir="${deploy.dir}/TradePortal/${env.name}/temp/lib"/>
      <mkdir dir="${deploy.dir}/TradePortal/${env.name}/temp/war"/>
      <mkdir dir="${prep.dir}/TradePortal/${env.name}/expandedear"/>
      <mkdir dir="${prep.dir}/TradePortal/${env.name}/portal"/>
      <mkdir dir="${prep.dir}/TradePortal/${env.name}/dtd"/>

      <unjar src="${prep.dir}/TradePortal/${env.name}/Portal360Dist.zip" dest="${prep.dir}/TradePortal/${env.name}"/>

      <property file="${prep.dir}/TradePortal/${env.name}/TradePortalVersion.properties"/>

      <echo message="**********************************************" />
      <echo message="* Deploying Trade Portal for ${env.name}" />
      <echo message="*" />
      <tstamp>
        <format property="current.time" pattern="dd MMMM yyyy hh:mm:ss aa" />
      </tstamp>
      <echo message="* Date/Time is: ${current.time}" />
      <echo message="*" />
      <echo message="* Build Number is: ${version}" />
      <echo message="*" />
      <echo message="**********************************************" />

      <!-- Extract the war file from the ear file-->
      <unjar   src="${prep.dir}/TradePortal/${env.name}/tradeportal.ear" 
               dest="${prep.dir}/TradePortal/${env.name}/expandedear">
      </unjar>

      <!-- Extract files from the war -->
      <unjar src="${prep.dir}/TradePortal/${env.name}/expandedear/war/tradeportal.war"
             dest="${prep.dir}/TradePortal/${env.name}/portal"/>

      <!-- Expanded ear is no longer needed -->
      <delete dir="${deploy.dir}/TradePortal/${env.name}/expandedear"/> 
  </target>

  <target name="remove-optional-TradePortal" unless="keep.optional">
      <!-- Delete out the optional JSP classes -->
      <delete dir="${prep.dir}/TradePortal/${env.name}/portal/WEB-INF/classes/com/ams/tradeportal/jsp/_optional"/>
  </target>

  <target name="deploy-app-specific-html-TradePortal">
      <!-- Copy app specific HTML into war -->      
      <copy todir="${prep.dir}/TradePortal/${env.name}/portal">
        <fileset dir="${config.dir}/TradePortal/${env.name}/appSpecificHtml" includes="**/*.*"/>
      </copy>  
  </target>

  <target name="copy-static-TradePortal" if="doc.root.dir">
      <copy todir="${doc.root.dir}">
        <fileset dir="${prep.dir}/TradePortal/${env.name}">
            <exclude name="portal/WEB-INF/**/*.*"/>
            <include name="portal/**/*.*"/>
        </fileset>
      </copy> 
  </target>

  <target name="deploy-TradePortal">
      <delete dir="${config.dir}/TradePortal/${env.name}/dtd"/>
      <mkdir dir="${config.dir}/TradePortal/${env.name}/dtd"/>
      <!--cquinton 1/4/2012 Rel 7.1 start-->
      <!--overwrite the version file regardless of file timestamps-->
      <copy todir="${config.dir}/TradePortal/${env.name}"
            file="${prep.dir}/TradePortal/${env.name}/TradePortalVersion.properties"
            overwrite="true"
            verbose="true"/>
      <!--cquinton 1/4/2012 Rel 7.1 end-->

      <copy todir="${deploy.dir}/TradePortal/${env.name}"
            file="${prep.dir}/TradePortal/${env.name}/tradeportal.ear"/>
   
      <copy todir="${config.dir}/TradePortal/${env.name}/dtd">
      		<fileset dir="${prep.dir}/TradePortal/${env.name}/dtd">
      			<include name="*.dtd"/>
      		</fileset>
      </copy>		
   
      <!-- Jar up the war file after changes -->
      <jar jarfile="${deploy.dir}/TradePortal/${env.name}/temp/war/tradeportal.war" 
           basedir="${prep.dir}/TradePortal/${env.name}/portal"
           whenempty="fail">       
          <include name="**/*.*"/>
      </jar>

      <!-- Jar up appSpecific -->
      <jar jarfile="${deploy.dir}/TradePortal/${env.name}/temp/lib/appSpecific.jar" 
           basedir="${config.dir}/TradePortal/${env.name}"
           whenempty="fail">       
          <include name="*.properties"/>
      </jar>

      <!-- Include the new appSpecific and war files in the EAR -->
      <jar jarfile="${deploy.dir}/TradePortal/${env.name}/tradeportal.ear" 
           basedir="${deploy.dir}/TradePortal/${env.name}/temp"
           whenempty="fail"
           update="true">       
          <include name="**/*.*"/>
      </jar>
  </target>

  <target name="cleanup-TradePortal">
      <delete dir="${deploy.dir}/TradePortal/${env.name}/temp"/>
      <delete>
         <fileset dir="${prep.dir}/TradePortal/${env.name}" includes="**/*.*"/>
      </delete>
      <delete dir="${prep.dir}/TradePortal/${env.name}/portal"/>
      <delete dir="${prep.dir}/TradePortal/${env.name}/META-INF"/>
      <delete dir="${prep.dir}/TradePortal/${env.name}/expandedear"/>
      <delete dir="${prep.dir}/TradePortal/${env.name}/dtd"/>
  </target>

<!--Shilpa R- CR PPX-166 start-->
  <target name="deployISRA" depends="prepare-ISRA, deploy-ISRA">
  </target>
  
  <target name="prepare-ISRA">
        <!--delete dir="${deploy.dir}/ISRA/${env.name}"/-->
        <mkdir dir="${deploy.dir}/ISRA/${env.name}"/>
        <loadproperties srcFile="${config.dir}/TradePortal/${env.name}/ISRA.properties"/>
        <loadproperties srcFile="${config.dir}/TradePortal/${env.name}/TradePortal.properties"/>
     
  </target>
  
   
  <target name="deploy-ISRA">
      <unjar src="${prep.dir}/ISRA/${env.name}/Portal360Dist.zip" dest="${prep.dir}/ISRA/${env.name}"/>

      <mkdir dir="${prep.dir}/ISRA/${env.name}/temprar"/>
      <!-- Extract files from the rar -->
      <unjar src="${prep.dir}/ISRA/${env.name}/ISRA.rar"
         dest="${prep.dir}/ISRA/${env.name}/temprar"/>
             
      <!-- replace the runtime variables in resorce adapter (ra.xml) using properties file-->
      <copy file="${prep.dir}/ISRA/${env.name}/temprar/META-INF/ra.xml" tofile="${prep.dir}/ISRA/${env.name}/temprar/META-INF/ra_replace.xml" filtering="true" overwrite="true">
        <filterchain>
          <replacetokens>
	    <token key="LogFilePath" value="${LogFilePath}"/>
	    <token key="DomainName" value="${DomainName}"/>
	    <token key="LoggingLevel" value="${LoggingLevel}"/>
	    <token key="LoggingMode" value="${LoggingMode}"/>
	    <token key="PageBufferSize" value="${PageBufferSize}"/>
	    <token key="RPCLogging" value="${RPCLogging}"/>
            <token key="DeploymentInstance" value="${DeploymentInstance}"/>
	    
          </replacetokens>
        </filterchain>
      </copy>
      <copy file="${prep.dir}/ISRA/${env.name}/temprar/META-INF/ra_replace.xml" tofile="${prep.dir}/ISRA/${env.name}/temprar/META-INF/ra.xml" filtering="true" overwrite="true">
      </copy>
      <delete file="${prep.dir}/ISRA/${env.name}/temprar/META-INF/ra_replace.xml"/>
      
      <!-- replace the runtime variables in resorce adapter (weblogic-ra.xml) using properties file-->
      <copy file="${prep.dir}/ISRA/${env.name}/temprar/META-INF/weblogic-ra.xml" tofile="${prep.dir}/ISRA/${env.name}/temprar/META-INF/weblogic-ra_replace.xml" filtering="true" overwrite="true">
        <filterchain>
	  <replacetokens>
            <token key="JNDI" value="${ISRA_JNDI}"/>
          </replacetokens>
        </filterchain>
      </copy>
      <copy file="${prep.dir}/ISRA/${env.name}/temprar/META-INF/weblogic-ra_replace.xml" tofile="${prep.dir}/ISRA/${env.name}/temprar/META-INF/weblogic-ra.xml" filtering="true" overwrite="true">
      </copy>
      <delete file="${prep.dir}/ISRA/${env.name}/temprar/META-INF/weblogic-ra_replace.xml"/>
      
      <jar jarfile="${prep.dir}/ISRA/${env.name}/ISRA.rar" update="true" manifest="${prep.dir}/ISRA/${env.name}/temprar/META-INF/MANIFEST.MF" basedir="${prep.dir}/ISRA/${env.name}/temprar">
           <include name="**/*.*"/>
      </jar>
      
      <delete dir="${prep.dir}/ISRA/${env.name}/temprar"/>
      
     <move tofile="${deploy.dir}/ISRA/${env.name}/${fileName}"
            file="${prep.dir}/ISRA/${env.name}/ISRA.rar"/>
             
  </target>
  
  
  <!--Shilpa R- CR PPX-166 end-->

  <target name="TPAgent" depends="prepare-TPAgent, copy-portal-static-from-agents, deploy-TPAgent">
  </target>

  <target name="prepare-TPAgent">
      <delete dir="${deploy.dir}/TPAgent/${env.name}"/>
      <mkdir dir="${deploy.dir}/TPAgent/${env.name}"/>
      <mkdir dir="${deploy.dir}/TPAgent/${env.name}/dtd"/>
      <mkdir dir="${deploy.dir}/TPAgent/${env.name}/lib"/>
      <mkdir dir="${deploy.dir}/TPAgent/${env.name}/properties"/>
      <unjar src="${prep.dir}/TPAgent/${env.name}/Portal360Dist.zip" dest="${prep.dir}/TPAgent/${env.name}"/>

      <property file="${prep.dir}/TPAgent/${env.name}/TradePortalVersion.properties"/>

      <echo message="**********************************************" />
      <echo message="* Deploying Portal Agents for ${env.name}" />
      <echo message="*" />
      <tstamp>
         <format property="current.time" pattern="dd MMMM yyyy hh:mm:ss aa" />
      </tstamp>
      <echo message="* Date/Time is: ${current.time}" />
      <echo message="*" />
      <echo message="* Build Number is: ${version}" />
      <echo message="*" />
      <echo message="**********************************************" />
  </target>

  <target name="deploy-TPAgent">
      <unjar dest="${deploy.dir}/TPAgent/${env.name}"
             src="${prep.dir}/TPAgent/${env.name}/TPAgent.jar"/>

      <jar jarfile="${deploy.dir}/TPAgent/${env.name}/lib/agentproperties.jar" 
           basedir="${config.dir}/TPAgent/${env.name}"
           whenempty="fail">       
          <include name="*.properties"/>
      </jar>


      <!-- Move weblogic.jar to its proper location -->
      <copy todir="${deploy.dir}/TPAgent/${env.name}/lib"
            file="${prep.dir}/TPAgent/${env.name}/weblogic.jar">
      </copy> 

      <copy todir="${deploy.dir}/TPAgent/${env.name}/properties"
            file="${config.dir}/TPAgent/${env.name}/Logging.xml"/>
  </target>


  <target name="copy-portal-static-from-agents" if="doc.root.dir">
      <!-- This target extracts the static content from the trade portal EAR file -->
      <!-- on the agents server.  Used for environments where the static content -->
      <!-- must be extracted on a Windows box, but portal deployment occurs on a -->
      <!-- Unix box. -->

      <!-- Extract the war file from the ear file-->
      <unjar   src="${prep.dir}/TPAgent/${env.name}/tradeportal.ear" 
               dest="${prep.dir}/TPAgent/${env.name}/expandedear">
      </unjar>

      <!-- Extract files from the war -->
      <unjar src="${prep.dir}/TPAgent/${env.name}/expandedear/war/tradeportal.war"
             dest="${prep.dir}/TPAgent/${env.name}/portal"/>

      <copy todir="${doc.root.dir}">
        <fileset dir="${prep.dir}/TPAgent/${env.name}">
            <exclude name="portal/WEB-INF/**/*.*"/>
            <include name="portal/**/*.*"/>
        </fileset>
      </copy> 

      <copy todir="${doc.root.dir}/portal">
        <fileset dir="${config.dir}/TPAgent/${env.name}/appSpecificHtml">
            <include name="**/*.*" />
        </fileset>
      </copy> 

      <delete dir="${prep.dir}/TPAgent/${env.name}/expandedear"/> 
   </target>



 <target name="OTLReportingApp" depends="prepare-OTLReportingApp, copy-static-OTLReportingApp, 
	       create-static-tar-OTLReportingApp, deploy-OTLReportingApp, cleanup-OTLReportingApp">
  </target>

  <target name="prepare-OTLReportingApp">
      <delete dir="${deploy.dir}/OTLReportingApp/${env.name}"/>
      <mkdir dir="${deploy.dir}/OTLReportingApp/${env.name}"/>
      <mkdir dir="${deploy.dir}/OTLReportingApp/${env.name}/temp/lib"/>
      <mkdir dir="${deploy.dir}/OTLReportingApp/${env.name}/temp/war"/>
      <mkdir dir="${prep.dir}/OTLReportingApp/${env.name}/expandedear"/>
      <mkdir dir="${prep.dir}/OTLReportingApp/${env.name}/wijsp"/>

      <unjar src="${prep.dir}/OTLReportingApp/${env.name}/ReportingDist.zip" dest="${prep.dir}/OTLReportingApp/${env.name}"/>

      <!-- Extract the war file from the ear file-->
      <unjar   src="${prep.dir}/OTLReportingApp/${env.name}/reporting.ear" 
               dest="${prep.dir}/OTLReportingApp/${env.name}/expandedear">
      </unjar>

      <!-- Extract files from the war -->
      <unjar src="${prep.dir}/OTLReportingApp/${env.name}/expandedear/war/reporting.war"
             dest="${prep.dir}/OTLReportingApp/${env.name}/wijsp"/>

      <!-- Expanded ear is no longer needed -->
      <delete dir="${deploy.dir}/OTLReportingApp/${env.name}/expandedear"/> 

  </target>

  <target name="copy-static-OTLReportingApp" if="doc.root.dir">
      <mkdir dir="${doc.root.dir}"/>
      <copy todir="${doc.root.dir}">
        <fileset dir="${prep.dir}/OTLReportingApp/${env.name}">
          <exclude name="wijsp/WEB-INF/**/*.*"/>
          <exclude name="wijsp/**/*.jsp"/>
          <include name="wijsp/**/*.*"/>
	</fileset>
      </copy> 
  </target>

  <target name="create-static-tar-OTLReportingApp" if="tar.file">
      <!-- Create a tar of all the static files -->
      <delete file="${tar.file}"/>
      <tar tarfile="${tar.file}"
           basedir="${prep.dir}/OTLReportingApp/${env.name}">
          <exclude name="wijsp/WEB-INF/**/*.*"/>
          <exclude name="wijsp/**/*.jsp"/>
          <include name="wijsp/**/*.*"/>
      </tar>   
  </target>

  <target name="deploy-OTLReportingApp">
      <copy todir="${deploy.dir}/OTLReportingApp/${env.name}"
            file="${prep.dir}/OTLReportingApp/${env.name}/reporting.ear"/>

      <jar jarfile="${deploy.dir}/OTLReportingApp/${env.name}/temp/lib/appSpecific.jar" 
           basedir="${config.dir}/OTLReportingApp/${env.name}"
           whenempty="fail">       
          <include name="*.properties"/>
      </jar>


      <jar jarfile="${deploy.dir}/OTLReportingApp/${env.name}/reporting.ear" 
           basedir="${deploy.dir}/OTLReportingApp/${env.name}/temp"
           whenempty="fail"
           update="true">       
          <include name="**/*.*"/>
      </jar>

      <delete dir="${deploy.dir}/OTLReportingApp/${env.name}/temp"/>
  </target>

  <target name="cleanup-OTLReportingApp">
      <delete dir="${deploy.dir}/OTLReportingApp/${env.name}/temp"/>
  </target>

  <target name="OTLReportingAgent" depends="prepare-OTLReportingAgent, deploy-OTLReportingAgent">
  </target>

  <target name="prepare-OTLReportingAgent">
      <delete dir="${deploy.dir}/OTLReportingAgent/${env.name}"/>
      <mkdir dir="${deploy.dir}/OTLReportingAgent/${env.name}"/>
  </target>

  <target name="deploy-OTLReportingAgent">
      <unjar src="${prep.dir}/OTLReportingAgent/${env.name}/ReportingDist.zip" dest="${prep.dir}/OTLReportingAgent/${env.name}"/>

      <unjar dest="${deploy.dir}/OTLReportingAgent/${env.name}"
             src="${prep.dir}/OTLReportingAgent/${env.name}/RPAgent.jar"/>

      <jar jarfile="${deploy.dir}/OTLReportingAgent/${env.name}/agentproperties.jar" 
           basedir="${config.dir}/OTLReportingAgent/${env.name}"
           whenempty="fail">       
          <include name="*.properties"/>
      </jar>
      <copy todir="${deploy.dir}/OTLReportingAgent/${env.name}"
            file="${config.dir}/OTLReportingAgent/${env.name}/wibean.orb.properties">
      </copy>  

  </target>

 <target name="TPReporting" depends="prepare-TPReporting, deploy-TPReporting"  if="env.name">
  </target>

  <target name="prepare-TPReporting">
      <delete dir="${deploy.dir}/TPReporting/${env.name}"/>
      <mkdir dir="${deploy.dir}/TPReporting/${env.name}"/>
  </target>

  <target name="deploy-TPReporting">
      <unjar src="${prep.dir}/TPReporting/${env.name}/Portal360Dist.zip" dest="${prep.dir}/TPReporting/${env.name}"/>

      <property file="${prep.dir}/TPReporting/${env.name}/TradePortalVersion.properties"/>

      <echo message="**********************************************" />
      <echo message="* Deploying Portal Reporting for ${env.name}" />
      <echo message="*" />
      <tstamp>
         <format property="current.time" pattern="dd MMMM yyyy hh:mm:ss aa" />
      </tstamp>
      <echo message="* Date/Time is: ${current.time}" />
      <echo message="*" />
      <echo message="* Build Number is: ${version}" />
      <echo message="*" />
      <echo message="**********************************************" />

      <unjar dest="${deploy.dir}/TPReporting/${env.name}"
             src="${prep.dir}/TPReporting/${env.name}/TPReporting.jar"/>

      <!-- Move weblogic.jar to its proper location -->
      <copy todir="${deploy.dir}/TPReporting/${env.name}/lib"
            file="${prep.dir}/TPReporting/${env.name}/weblogic.jar">
      </copy>  

      <!-- Move xbean.jar to its proper location -->
      <copy todir="${deploy.dir}/TPReporting/${env.name}/lib"
            file="${prep.dir}/TPReporting/${env.name}/xbean.jar">
      </copy>  

      <jar jarfile="${deploy.dir}/TPReporting/${env.name}/lib/reportingproperties.jar" 
           basedir="${config.dir}/TPReporting/${env.name}"
           whenempty="fail">       
          <include name="*.properties"/>
      </jar>

  </target>

</project>
