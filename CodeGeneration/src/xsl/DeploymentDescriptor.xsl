<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Generates a BEA WebLogic Deployment Descriptor for the Business Object or Mediator.  The Desployment
*  Descriptor must specify the Transactional requirements of each public method of the object. 
-->

<xsl:template name="DEPLOYMENT_DESCRIPTOR">
<xsl:param name="ObjectType">BusinessObject</xsl:param>
<xsl:variable name="Abstract" select="Foundation.Core.GeneralizableElement.isAbstract/@xmi.value"/>
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/>
<xsl:if test="not($Abstract='true')">
-------------------- START: <xsl:value-of  select="$ClassName"/>.ejb-jar.xml --------------------
<xsl:variable name="OverridePackageName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:PackageName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>

  <xsl:if test="$OverridePackageName">
      <xsl:variable name="PackageName"><xsl:value-of select="$OverridePackageName" /></xsl:variable>
  </xsl:if>
<xsl:text disable-output-escaping="yes">&lt;?xml version="1.0" encoding="UTF-8"?&gt;

&lt;!DOCTYPE ejb-jar PUBLIC '-//Sun Microsystems, Inc.//DTD Enterprise JavaBeans 1.1//EN' 'http://java.sun.com/j2ee/dtds/ejb-jar_1_1.dtd'&gt;
</xsl:text>
<ejb-jar><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><enterprise-beans><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><session><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-name><xsl:value-of select="$ClassName"/></ejb-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><home><xsl:value-of select="$PackageName"/><xsl:value-of select="$BusObjPackage"/>.<xsl:value-of select="$ClassName"/>Home</home><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><remote><xsl:value-of select="$PackageName"/><xsl:value-of select="$BusObjPackage"/>.<xsl:value-of select="$ClassName"/></remote><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-class><xsl:value-of select="$PackageName"/><xsl:value-of select="$BusObjPackage"/>.<xsl:value-of select="$ClassName"/>Bean</ejb-class><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><session-type>Stateful</session-type><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><transaction-type>Container</transaction-type><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-name>debugMode</env-entry-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-type>java.lang.String</env-entry-type><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-value>0</env-entry-value><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></env-entry><xsl:text>&#xa;</xsl:text>
<xsl:if test="$ObjectType='BusinessObject'">
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-name>dataTableName</env-entry-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-type>java.lang.String</env-entry-type><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-value><xsl:call-template name="TABLE_NAME"><xsl:with-param name="ClassName" select="$ClassName"/></xsl:call-template></env-entry-value><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></env-entry><xsl:text>&#xa;</xsl:text>
</xsl:if>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-name>ecsgObjectName</env-entry-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-type>java.lang.String</env-entry-type><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><env-entry-value><xsl:value-of select="$ClassName"/></env-entry-value><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></env-entry><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></session><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text></enterprise-beans><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><assembly-descriptor><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><container-transaction><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-name><xsl:value-of select="$ClassName"/></ejb-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-intf>Remote</method-intf><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-name>*</method-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><trans-attribute>NotSupported</trans-attribute><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></container-transaction>
<xsl:if test="$ObjectType='BusinessObject'"><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><container-transaction><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-name><xsl:value-of select="$ClassName"/></ejb-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-intf>Remote</method-intf><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-name>save</method-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-name><xsl:value-of select="$ClassName"/></ejb-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-intf>Remote</method-intf><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-name>delete</method-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><trans-attribute>Required</trans-attribute><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></container-transaction>
<xsl:call-template name="DEPLOYED_METHODS" >
<xsl:with-param name="ClassName" select="$ClassName" />
</xsl:call-template>
</xsl:if><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text></assembly-descriptor><xsl:text>&#xa;</xsl:text>
  </ejb-jar><xsl:text>&#xa;</xsl:text>
-------------------- FINISH: <xsl:value-of select="$ClassName"/>.ejb-jar.txt --------------------
-------------------- START: <xsl:value-of  select="$ClassName"/>.weblogic-ejb-jar.xml --------------------
<xsl:text disable-output-escaping="yes">&lt;?xml version="1.0" encoding="UTF-8"?&gt;

&lt;!DOCTYPE weblogic-ejb-jar PUBLIC '-//BEA Systems, Inc.//DTD WebLogic 6.0.0 EJB//EN' 'http://www.bea.com/servers/wls600/dtd/weblogic-ejb-jar.dtd'&gt;

</xsl:text>
<weblogic-ejb-jar><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><weblogic-enterprise-bean><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-name><xsl:value-of select="$ClassName"/></ejb-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><stateful-session-descriptor><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><stateful-session-cache><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><max-beans-in-cache>100</max-beans-in-cache><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></stateful-session-cache><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></stateful-session-descriptor><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><enable-call-by-reference>true</enable-call-by-reference><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><jndi-name><xsl:value-of  select="$ClassName"/></jndi-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text></weblogic-enterprise-bean><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><transaction-isolation><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><isolation-level>TransactionReadCommitted</isolation-level><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-name><xsl:value-of  select="$ClassName"/></ejb-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-name>*</method-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text></transaction-isolation><xsl:text>&#xa;</xsl:text>
</weblogic-ejb-jar><xsl:text>&#xa;</xsl:text>
-------------------- FINISH: <xsl:value-of select="$ClassName"/>.weblogic-ejb-jar.txt --------------------
</xsl:if>
</xsl:template>


<!--
*  Loops through each of the Public methods for the class and calls the DEPLOYED_METHOD template to add a
*  declaration for the method to the Deployment descriptor.  After the methods for this Class have been
*  processed, the ANCESTOR_METHODS template is called to add the Ancestor object's methods to the 
*  desployment descriptor.  
-->
<xsl:template name="DEPLOYED_METHODS">
<xsl:param name="ClassName" />
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Operation[Foundation.Core.ModelElement.visibility/@xmi.value='public']">
   <xsl:call-template name="DEPLOYED_METHOD">
	<xsl:with-param name="ClassName" select="$ClassName" />
   </xsl:call-template>
</xsl:for-each>
<xsl:call-template name="ANCESTOR_METHODS"/>
</xsl:template>


<!--
*  Generates the "Control Descriptor" for the specified method.  The "Control Descriptor" defines the Transactional
*  nature of the method and must include the fully qualified parameters for the method when speficying the method's
*  signature.  
-->
<xsl:template name="DEPLOYED_METHOD">
<xsl:variable name="MethodName" select="Foundation.Core.ModelElement.name"/>
<xsl:variable name="TransactionRule">
	<xsl:call-template name="TRANSACTION" />
</xsl:variable>
<xsl:if test="not($TransactionRule = '')" >
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><container-transaction><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><ejb-name><xsl:value-of select="$ClassName" /></ejb-name><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-intf>Remote</method-intf><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-name><xsl:value-of select="$MethodName" /></method-name><xsl:text>&#xa;</xsl:text>
<xsl:call-template name="QUALIFIED_PARMS_AS_XML" />
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></method><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><trans-attribute><xsl:call-template name="TRANSACTION" /></trans-attribute><xsl:text>&#xa;</xsl:text>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></container-transaction><xsl:text>&#xa;</xsl:text>
</xsl:if>
</xsl:template>

<xsl:template name="QUALIFIED_PARMS_AS_XML">
<xsl:if test="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value!='return']">
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-params><xsl:text>&#xa;</xsl:text>

<xsl:for-each select="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value!='return']">

<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><method-param><xsl:call-template name="DATATYPE"><xsl:with-param name="Qualify">Y</xsl:with-param></xsl:call-template></method-param><xsl:text>&#xa;</xsl:text>

</xsl:for-each>
<xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text><xsl:text>&#9;</xsl:text></method-params><xsl:text>&#xa;</xsl:text>
</xsl:if>
</xsl:template>


<!--
*  Generates a list of paramaters for the object's method.  The parameters are fully qualified with their package name.
*  For example, "BigDecimal" would be "java.math.BigDecimal".  The "qualification" logic is in the "DATATYPE" template
*  of the "Method.xsl" stylesheet.
-->
<xsl:template name="QUALIFIED_PARMS">
<xsl:for-each select="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value!='return']">
<xsl:call-template name="DATATYPE"><xsl:with-param name="Qualify">Y</xsl:with-param></xsl:call-template>
<xsl:if test="not(position()=last())">, </xsl:if>
</xsl:for-each>
</xsl:template>


<!--
*  Locates the Ancestor of the Class and calls the DEPLOYED_METHODS template (using the Ancestor class) to 
*  add the Ancestor's methods to the deployment descriptor.  This process occurs recursively so multiple levels
*  of inheritance can be supported.
-->
<xsl:template name="ANCESTOR_METHODS">
<xsl:variable name="GeneralizationID" select="Foundation.Core.GeneralizableElement.generalization/Foundation.Core.Generalization/@xmi.idref"/>
<xsl:choose>
   <xsl:when test="not($GeneralizationID=null)">
   <xsl:variable name="AncestorID" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.Generalization[@xmi.id=$GeneralizationID]/Foundation.Core.Generalization.supertype/Foundation.Core.Class/@xmi.idref"/>
   <xsl:for-each select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AncestorID]">
      <xsl:call-template name="DEPLOYED_METHODS"/>
   </xsl:for-each>
   </xsl:when>
</xsl:choose>
</xsl:template>


<!--
*  Determines the Transactional nature of the method.  This information is stored in the "Java/Transaction" property
*  of the method. 
-->
<xsl:template name="TRANSACTION">
<xsl:variable name="Transaction" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:Transaction']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:value-of select="$Transaction" />
<!--
<xsl:choose>
   <xsl:when test="$Transaction='Required'">Required</xsl:when>
   <xsl:when test="$Transaction='Supported'">Supports</xsl:when>
   <xsl:otherwise>NotSupported</xsl:otherwise>
</xsl:choose>
-->
</xsl:template>


<xsl:include href="Method.xsl"/>
<xsl:include href="TableDefinition.xsl"/>
</xsl:stylesheet>