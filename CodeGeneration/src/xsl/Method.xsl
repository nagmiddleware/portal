<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
  *  Loops through each of the methods (operations) defined for the class and generates an empty function declartion
  *  for the method by calling the METHOD template.  The description of the Method, its Parameters, and its Return
  *  Value will precede the function declaration in JavaDoc format.
-->
<xsl:template name="METHOD_DECLARATIONS">
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Operation">
  <xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
  <xsl:if test="not($Generate='False')" >
  /**
   * <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">3</xsl:with-param></xsl:call-template>
   * <xsl:for-each select="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value!='return']"><xsl:call-template name="PARM_DESCRIPTION"/></xsl:for-each>
   <xsl:call-template name="RETURN_DESCRIPTION"/>
   */
   <xsl:call-template name="DECLARATION"/>
   <xsl:call-template name="RETURN_STATEMENT"/>
  </xsl:if>
</xsl:for-each>
</xsl:template>


<!--
  *  Loops through each of the PUBLIC methods (operations) defined for the class and generates an empty function 
  *  declartion for the method by calling the METHOD template.  This Template is used when generating the Remote
  *  interface for the object since only the Public methods are relevant.
-->
<xsl:template name="PUBLIC_METHODS">
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Operation[Foundation.Core.ModelElement.visibility/@xmi.value='public']">
<xsl:text>&#xa;</xsl:text><xsl:text>&#9;</xsl:text><xsl:call-template name="DECLARATION"/>;
</xsl:for-each>
</xsl:template>


<!--
  *  Generates the function declaration for the method, by determining the scope of the method, the name of the method,
  *  the parameters for the method (PARAMETER template), and the exceptions which the method may throw (EXCEPTION
  *  template)
-->
<xsl:template name="DECLARATION">
<xsl:variable name="MethodName" select="Foundation.Core.ModelElement.name"/>
<xsl:variable name="Scope"      select="Foundation.Core.ModelElement.visibility/@xmi.value"/>
<xsl:value-of select="$Scope"/><xsl:text> </xsl:text><xsl:call-template name="RETURN"/><xsl:text> </xsl:text><xsl:value-of select="$MethodName"/>(<xsl:for-each select="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value!='return']">
         <xsl:call-template name="PARAMETER"/></xsl:for-each>) <xsl:call-template name="EXCEPTION"><xsl:with-param name="Scope" select="$Scope"/></xsl:call-template>
</xsl:template>


<!--
   *  Determines the name and data type of a parameter to the method and writes this out as part of the function
   *  declaration.  The DATATYPE template is used to determine the type of the parameter.
-->
<xsl:template name="PARAMETER">
<xsl:call-template name="DATATYPE"/><xsl:text> </xsl:text><xsl:value-of select="Foundation.Core.ModelElement.name"/>
<xsl:if test="not(position()=last())">, </xsl:if>
<xsl:if test="position()=last()">   </xsl:if>
</xsl:template>


<!--
  *  Outputs the name of the parameter for the method along with a description of it in JavaDoc format.  This
  *  is used as part of the Comment Block preceeding the function declaration.
-->
<xsl:template name="PARM_DESCRIPTION">
<xsl:variable name="ParameterLink" select="Foundation.Core.Parameter.type/Foundation.Core.DataType/@xmi.idref"/>
<xsl:variable name="ParameterType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.DataType[@xmi.id=$ParameterLink]/Foundation.Core.ModelElement.name"/>
   * @param  <xsl:value-of select="Foundation.Core.ModelElement.name"/><xsl:text>  </xsl:text><xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">13</xsl:with-param></xsl:call-template>
</xsl:template>


<!--
  *  Determines the return value for the method.  If no return value was specified in the object model, then "void"
  *  will be used as the return value.
-->
<xsl:template name="RETURN">
<xsl:variable name="ReturnID" select="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value='return']"/>
<xsl:if test="$ReturnID=null">void</xsl:if>
<xsl:if test="not($ReturnID=null)">
   <xsl:for-each select="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value='return']">
      <xsl:call-template name="DATATYPE"/>
   </xsl:for-each>
</xsl:if>
</xsl:template>


<!--
  *  Outputs the data type/class which is retured by the method along with a description of it in JavaDoc format.  This
  *  is used as part of the Comment Block preceeding the function declaration.
-->
<xsl:template name="RETURN_DESCRIPTION">
   * @return <xsl:call-template name="RETURN"/><xsl:text>  </xsl:text><xsl:value-of select="Foundation.Core.BehavioralFeature.parameter/Foundation.Core.Parameter[Foundation.Core.Parameter.kind/@xmi.value='return']/Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
</xsl:template>


<!--
  *  This template is called by the PARAMETER, RETURN, and QUALIFIED_PARMS (Deployment.xsl) templates to determine and
  *  write out the data type of a parameter or return value for a method.  Parameters to a method can be of three 
  *  different types (Regular DataType, Enumerated DataType, or Class) and each of these types must be processed
  *  differently.
  *
  *  The "Qualify" parameter to this function allows the calling template to specify whether the datatype should be
  *  prefixed with its fully qualified path name.  For example, "BigDecimal" would be "java.math.BigDecimal".  This
  *  feature is used when creating the Deployment Descriptor for the object.
-->
<xsl:template name="DATATYPE">
<xsl:param name="Qualify">N</xsl:param>
<xsl:variable name="DatatypeLink"   select="Foundation.Core.Parameter.type/Foundation.Core.DataType/@xmi.idref"/>
<xsl:variable name="EnumeratedLink" select="Foundation.Core.Parameter.type/Foundation.Data_Types.Enumeration/@xmi.idref"/>
<xsl:variable name="ClassLink"      select="Foundation.Core.Parameter.type/Foundation.Core.Class/@xmi.idref"/>
<xsl:if test="not($DatatypeLink=null)">
   <xsl:variable name="DataType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.DataType[@xmi.id=$DatatypeLink]/Foundation.Core.ModelElement.name"/>
   <xsl:if test="$Qualify='Y'"><xsl:call-template name="QUALIFY_DATATYPE"><xsl:with-param name="DataType" select="$DataType"/></xsl:call-template></xsl:if><xsl:value-of select="$DataType"/></xsl:if>
<xsl:if test="not($EnumeratedLink=null)">
   <xsl:variable name="DataType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Data_Types.Enumeration[@xmi.id=$EnumeratedLink]/Foundation.Core.ModelElement.name"/>
   <xsl:if test="$Qualify='Y'"><xsl:call-template name="QUALIFY_DATATYPE"><xsl:with-param name="DataType" select="$DataType"/></xsl:call-template></xsl:if><xsl:value-of select="$DataType"/></xsl:if>
<xsl:if test="not($ClassLink=null)">
   <xsl:for-each select="../../../../../Foundation.Core.Class[@xmi.id=$ClassLink]">
   <xsl:if test="$Qualify='Y'"><xsl:call-template name="QUALIFY_CLASS"/></xsl:if><xsl:value-of select="Foundation.Core.ModelElement.name"/>
   </xsl:for-each></xsl:if>   
<xsl:if test="$ClassLink=null and $DatatypeLink=null and $EnumeratedLink=null">void </xsl:if>
</xsl:template>


<!--
  *  Pre-pends the name of the package in which the Data Type resides in front of the name of the data type.
  *  For example, if the data type was "Long" this function would pre-pend it with "java.lang.".
  *
  *  Note:  The logic for determining each data type's package is just a big case statement with a hard-coded
  *  lookup.  As additional data types are utilized as parameters to functions which must be included in deployment
  *  descriptors, they should be added to the Case statement. 
-->
<xsl:template name="QUALIFY_DATATYPE">
<xsl:choose>
   <xsl:when test="$DataType='BigDecimal'"></xsl:when>
   <xsl:when test="$DataType='BigInteger'"></xsl:when>
   <xsl:when test="$DataType='long'"></xsl:when>
   <xsl:when test="$DataType='int'"></xsl:when>
   <xsl:when test="$DataType='char'"></xsl:when>
   <xsl:when test="$DataType='float'"></xsl:when>
   <xsl:when test="$DataType='double'"></xsl:when>
   <xsl:when test="$DataType='boolean'"></xsl:when>
   <xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:template>


<!--
  *  Pre-pends the package in which a class resides in front of the name of the class.  Parameters specified in
  *  deployment descriptors must be fully qualified in this manner.
-->
<xsl:template name="QUALIFY_CLASS">
<xsl:variable name="ObjectID" select="../../@xmi.id"/>
<xsl:for-each select="../../../Model_Management.Package[@xmi.id=$ObjectID]"><xsl:value-of select="Foundation.Core.ModelElement.name"/></xsl:for-each>
</xsl:template>


<!--
  *  Outputs the exceptions that the Method can throw.  These are specified in an "Exceptions" tag which was added
  *  to the ROSEJAVA.PTY property file.  If the method is "public", then "RemoteException" is added to the list
  *  automatically and does not need to be specified in the object model.
-->
<xsl:template name="EXCEPTION">
<xsl:param name="Scope"/>
<xsl:variable name="Exceptions" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:Exceptions']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:choose>
   <xsl:when test="$Scope='public' and $Exceptions=null">throws RemoteException</xsl:when>
   <xsl:when test="$Scope='public' and $Exceptions!=null">throws RemoteException, <xsl:value-of select="$Exceptions"/></xsl:when>
   <xsl:when test="$Scope!='public' and $Exceptions!=null">throws <xsl:value-of select="Exceptions"/></xsl:when>
</xsl:choose>
</xsl:template>


<!--
  *  Generates a Return statement to go in the body the method declaration.  This is done so that the method will
  *  compile without any manipulation by the developer.
-->
<xsl:template name="RETURN_STATEMENT">
<xsl:variable name="ReturnType"><xsl:call-template name="RETURN"/></xsl:variable>
<xsl:if test="not($ReturnType='void')">
  <xsl:choose>
    <xsl:when test="$ReturnType='long' or $ReturnType='int' or $ReturnType='float' or
                    $ReturnType='double'"><xsl:variable name="DefaultValue">0</xsl:variable></xsl:when>
    <xsl:when test="$ReturnType='char'"><xsl:variable name="DefaultValue">'a'</xsl:variable></xsl:when>
    <xsl:when test="$ReturnType='boolean'"><xsl:variable name="DefaultValue">true</xsl:variable></xsl:when>
    <xsl:otherwise><xsl:variable name="DefaultValue">null</xsl:variable></xsl:otherwise></xsl:choose>
   {
      <xsl:value-of select="$ReturnType"/> returnValue = <xsl:value-of select="$DefaultValue"/>;
      return returnValue;
   } 
</xsl:if>
<xsl:if test="$ReturnType='void'">
   {
   }
</xsl:if>
</xsl:template>


</xsl:stylesheet>