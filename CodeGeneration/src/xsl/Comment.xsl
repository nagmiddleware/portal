<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Formats a Comment (input parm to this template) so that it can be included in the object being
*  processed.  The formatting process involves breaking the the original comment string down into
*  multiple lines so that they can be viewed easily within the resulting source code and providing
*  the proper level of indentation (also a parameter) so that each line of the comment will line
*  up.
-->

<xsl:template name="COMMENT">
<xsl:param name="OriginalComment"/>
<xsl:param name="Indent">1</xsl:param>
<xsl:call-template name="SPLIT_COMMENT_LINE">
   <xsl:with-param name="CurrentLine"/>
   <xsl:with-param name="RemainingComment" select="$OriginalComment"/>
   <xsl:with-param name="FirstLine">Y</xsl:with-param>
   <xsl:with-param name="Indent" select="$Indent"/>
</xsl:call-template>
</xsl:template>


<!--
*  This template is called recursively to split a comment string into multiple lines.  The algorithm
*  basically just looks for the next word (spaces are assumed to separate words) and adds it to the
*  current line.  When the current line reaches more that 70 characters in length, the current line
*  is printed out, and the next line is started.
-->
<xsl:template name="SPLIT_COMMENT_LINE">
<xsl:param name="CurrentLine"/>
<xsl:param name="RemainingComment"/>
<xsl:param name="FirstLine"/>
<xsl:param name="Indent"/>
<xsl:param name="ForceLineBreak"/>
<xsl:choose>
   <!-- Current Line has a newline in it.  Force a line break -->
   <xsl:when test="$ForceLineBreak='true'">
      <xsl:call-template name="PRINT_COMMENT_LINE">
         <xsl:with-param name="CommentLine" select="$CurrentLine"/>
         <xsl:with-param name="FirstLine" select="$FirstLine"/>
         <xsl:with-param name="Indent" select="$Indent"/>
      </xsl:call-template>
      <xsl:call-template name="SPLIT_COMMENT_LINE">
         <xsl:with-param name="RemainingComment" select="$RemainingComment"/>
         <xsl:with-param name="FirstLine">N</xsl:with-param>
         <xsl:with-param name="Indent" select="$Indent"/>
      </xsl:call-template>
   </xsl:when>
   <!-- Current Line is big enough. Print it out and start next Line -->
   <xsl:when test="string-length($CurrentLine)>70">
      <xsl:call-template name="PRINT_COMMENT_LINE">
         <xsl:with-param name="CommentLine" select="$CurrentLine"/>
         <xsl:with-param name="FirstLine" select="$FirstLine"/>
         <xsl:with-param name="Indent" select="$Indent"/>
      </xsl:call-template>
      <xsl:call-template name="SPLIT_COMMENT_LINE">
         <xsl:with-param name="RemainingComment" select="$RemainingComment"/>
         <xsl:with-param name="FirstLine">N</xsl:with-param>
         <xsl:with-param name="Indent" select="$Indent"/>
      </xsl:call-template>
   </xsl:when>
   <!-- We have reached the end of the comment.  If the current line has data in it print it out. -->
   <xsl:when test="string-length($RemainingComment)=0">
      <xsl:if test="string-length($CurrentLine)>0">
         <xsl:call-template name="PRINT_COMMENT_LINE">
            <xsl:with-param name="CommentLine" select="$CurrentLine"/>
            <xsl:with-param name="FirstLine" select="$FirstLine"/>
            <xsl:with-param name="Indent" select="$Indent"/>
         </xsl:call-template>
      </xsl:if>
   </xsl:when>
   <!-- Still room for more words to be added to the Current Line -->
   <xsl:otherwise>
      <!-- Determine which delimiter to use.  Either use the new line character or a space -->
      <xsl:variable name="StringBeforeNewLine" select="substring-before($RemainingComment, '&#xa;')" />
      <xsl:variable name="StringBeforeSpace" select="substring-before($RemainingComment, ' ')" />
      <xsl:choose>
        <!-- Special case where line starts with a new line character -->
        <xsl:when test="substring($RemainingComment,1,1)='&#xa;'">
            <xsl:variable name="Delimiter"><xsl:text>&#xa;</xsl:text></xsl:variable>
            <xsl:variable name="ForceLineBreak">true</xsl:variable>
        </xsl:when>
        <!-- Case when there are no new line characters in the comment -->
        <xsl:when test="string-length($StringBeforeNewLine)=0">
            <xsl:variable name="Delimiter"><xsl:text> </xsl:text></xsl:variable>
            <xsl:variable name="ForceLineBreak">false</xsl:variable>
        </xsl:when>    
        <!-- Case where there is a new line and a space... break it at the new line -->
        <xsl:when test="string-length($StringBeforeSpace) > string-length($StringBeforeNewLine)">
            <xsl:variable name="Delimiter"><xsl:text>&#xa;</xsl:text></xsl:variable>
            <xsl:variable name="ForceLineBreak">true</xsl:variable>
        </xsl:when>
        <!-- Delimiter is the space -->
        <xsl:otherwise>
            <xsl:variable name="Delimiter"><xsl:text> </xsl:text></xsl:variable>
            <xsl:variable name="ForceLineBreak">false</xsl:variable>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:variable name="NextWord" select="substring-before($RemainingComment, $Delimiter)"/>

      <!-- If we don't get data back for the Next Word, either we are at the end of the comment or there are two or
           more spaces next to each other in the comment.  -->
      <xsl:if test="$NextWord=null">
         <xsl:if test="not(substring($RemainingComment,1,1)=$Delimiter)">
            <xsl:variable name="NextWord" select="$RemainingComment"/>
         </xsl:if>
      </xsl:if>
      <xsl:variable name="WhatsLeft" select="substring-after($RemainingComment, $Delimiter)"/>
      <xsl:choose>
         <!-- Already data in the Current Line, so add the next word to it (preceded by a space). -->
         <xsl:when test="string-length($CurrentLine)>0">
            <xsl:call-template name="SPLIT_COMMENT_LINE">
               <xsl:with-param name="CurrentLine" select="concat(concat($CurrentLine, ' '),$NextWord)"/>
               <xsl:with-param name="RemainingComment" select="$WhatsLeft"/>
               <xsl:with-param name="FirstLine" select="$FirstLine"/> 
               <xsl:with-param name="Indent" select="$Indent"/>
               <xsl:with-param name="ForceLineBreak" select="$ForceLineBreak"/>  
            </xsl:call-template>               
         </xsl:when>
         <!-- Just staring a new line, so the Next word will be the first data in it -->
         <xsl:otherwise>
            <xsl:call-template name="SPLIT_COMMENT_LINE">
               <xsl:with-param name="CurrentLine" select="$NextWord"/>
               <xsl:with-param name="RemainingComment" select="$WhatsLeft"/>
               <xsl:with-param name="FirstLine" select="$FirstLine"/>
               <xsl:with-param name="Indent" select="$Indent"/>
               <xsl:with-param name="ForceLineBreak" select="$ForceLineBreak"/>
            </xsl:call-template>   
         </xsl:otherwise>
      </xsl:choose>      
   </xsl:otherwise>
</xsl:choose>
</xsl:template>

<!--
*  Prints out the next line in the Comment.  The level at which to indent the comment and whether it is
*  the first line in the comment are passed in as parameters.  If this is the first line in the comment
*  it is output directly into the resulting code.  No indentation is provided.  It is important, therefore
*  for the Call to the "COMMENT" template to be placed exactly where the user would like to start the
*  comment.  For subsequent lines, the indentation level is used to align the comment.  Various indentation
*  levels are used depending upon whether or not it is a comment for a Class, an Attribute, a Method, a 
*  Paramater to a Method, etc. 
-->
<xsl:template name="PRINT_COMMENT_LINE">
<xsl:param name="CommentLine"/>
<xsl:param name="FirstLine"/>
<xsl:param name="Indent"/>
<xsl:if test="$FirstLine='Y'">
   <xsl:text></xsl:text><xsl:value-of select="$CommentLine"/>
</xsl:if>
<xsl:if test="$FirstLine='N'">
   <xsl:choose>
      <!-- Comment for registering Attributes, Associations, Components -->
      <xsl:when test="$Indent='7'">
         <xsl:text>
         </xsl:text><xsl:value-of select="$CommentLine"/>
      </xsl:when>

      <!-- Comment for Method Declarations--> 
      <xsl:when test="$Indent='3'">
   * <xsl:value-of select="$CommentLine"/>
      </xsl:when>

      <!-- Comment for Parameter Descriptions -->
      <xsl:when test="$Indent='13'">
   *         <xsl:value-of select="$CommentLine"/>
      </xsl:when>

      <!-- Comment for TermsBean attribute factory methods Descriptions -->
      <xsl:when test="$Indent='8'">
        * <xsl:value-of select="$CommentLine"/>
      </xsl:when>

      <!-- Comment for Class Definitions -->
      <xsl:otherwise>
 * <xsl:value-of select="$CommentLine"/>
      </xsl:otherwise>
   </xsl:choose>
</xsl:if>
</xsl:template>
</xsl:stylesheet>