<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
  *  Generates the Remote Interface for Business Objects, Mediators, and any other
  *  EJB objects defined in the object model.
-->

<xsl:template name="REMOTE_INTERFACE">
<xsl:param name="PackageName"/>
<xsl:param name="DefaultAncestorClass">BusinessObject</xsl:param> 
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/> 
-------------------- START: <xsl:value-of  select="$ClassName"/>.java --------------------

<xsl:variable name="OverridePackageName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:PackageName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>

  <xsl:if test="$OverridePackageName">
      <xsl:variable name="PackageName"><xsl:value-of select="$OverridePackageName" /></xsl:variable>
  </xsl:if>

package <xsl:value-of select="$PackageName"/><xsl:value-of select="$RemoteInterfacePackage"/>;
<xsl:call-template name="IMPORTS"></xsl:call-template>

/**
 * <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/></xsl:call-template>
 *<xsl:call-template name="COPYRIGHT"> <xsl:with-param name="ClassName" select="$ClassName"/> </xsl:call-template>*/
public interface <xsl:value-of select="$ClassName"/> extends <xsl:call-template name="ANCESTOR"><xsl:with-param name="DefaultAncestorClass" select="$DefaultAncestorClass"/></xsl:call-template>
{   <xsl:call-template name="PUBLIC_METHODS"/>
}
-------------------- FINISH: <xsl:value-of select="$ClassName"/>.java --------------------
</xsl:template>

<xsl:include href="Imports.xsl"/>
<xsl:include href="Method.xsl"/>
<xsl:include href="Ancestor.xsl"/>

</xsl:stylesheet>