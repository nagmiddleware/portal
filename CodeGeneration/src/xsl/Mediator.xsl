<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
  *  This template generates the Enterprise Java Bean for a Mediator defined within the object
  *  model.  The code generated includes a stubbed out "execute" method and stubbed out methods
  *  for any other operations definedin the object model.
-->

<xsl:template name="MEDIATOR">
<xsl:param name="PackageName"/> 
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/>
<xsl:variable name="Ancestor">
  <xsl:call-template name="ANCESTOR">
    <xsl:with-param name="DefaultAncestorClass">Mediator</xsl:with-param>
  </xsl:call-template>
</xsl:variable> 
-------------------- START: <xsl:value-of  select="$ClassName"/>Bean.java --------------------
<xsl:variable name="OverridePackageName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:PackageName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>

  <xsl:if test="$OverridePackageName">
      <xsl:variable name="PackageName"><xsl:value-of select="$OverridePackageName" /></xsl:variable>
  </xsl:if>

<xsl:call-template name="COPYRIGHT"> <xsl:with-param name="ClassName" select="$ClassName"/> </xsl:call-template>
package <xsl:value-of select="$PackageName"/>;
<xsl:call-template name="IMPORTS"></xsl:call-template>

/*
 * <xsl:value-of select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
 */
public class <xsl:value-of select="$ClassName"/>Bean extends <xsl:value-of select="$Ancestor"/>Bean
{
  /*
   * Called by the processRequest() method of the Ancestor class to process the designated
   * transaction.  The input parameters for the transaction are received in an XML document
   * (inputDoc).  The execute() method is responsible for calling one or more business objects
   * to process the transaction and populating the outputDoc XML document with the results
   * of the transaction. 
   */
   public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc)
          throws RemoteException, AmsException
   {
      /* Get the OID of the object to be processed from the input XML document */
      long		objectOid = inputDoc.getAttributeLong("oid");
      BusinessObject	businessObject = null;

      try
      { 
         /* Create the business object using the object factory and instantiate it */
         businessObject = (BusinessObject)EJBObjectFactory.createServerEJB(getSessionContext(), "BusinessObject", objectOid);

         /* Populate the output XML document with the attributes of the business object */
         outputDoc = businessObject.populateXmlDoc(outputDoc);  
      }
      finally
      {
         /* Capture any errors issued by the business object */
         addErrorInfo(businessObject.getIssuedErrors());
      }       
    
      /* Return the results of the transaction */
      return outputDoc;
   }  
 
   <xsl:call-template name="METHOD_DECLARATIONS"/>
}
-------------------- FINISH: <xsl:value-of select="$ClassName"/>Bean.java --------------------
</xsl:template>

<xsl:include href="Copyright.xsl"/>
<xsl:include href="Imports.xsl"/>
<xsl:include href="Method.xsl"/>
<xsl:include href="Ancestor.xsl"/>

</xsl:stylesheet>