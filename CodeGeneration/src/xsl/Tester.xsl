<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Generates a Tester client application for the Business Object.  The 'Tester' will create a new instance
*  of the class, set attributes on it, and save it to the database.  The intent is to provide the developer
*  with a starting point for unit testing the business object independently of the user interface.
-->
<xsl:template name="TESTER">
<xsl:param name="PackageName"/> 
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/>
<xsl:variable name="Ancestor"><xsl:call-template name="ANCESTOR"/></xsl:variable> 
-------------------- START: <xsl:value-of  select="$ClassName"/>Tester.java --------------------
<xsl:variable name="OverridePackageName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:PackageName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>

  <xsl:if test="$OverridePackageName">
      <xsl:variable name="PackageName"><xsl:value-of select="$OverridePackageName" /></xsl:variable>
  </xsl:if>

package <xsl:value-of select="$PackageName"/><xsl:value-of select="$TesterPackage"/>;
<xsl:call-template name="IMPORTS"></xsl:call-template>import <xsl:value-of select="$PackageName"/><xsl:value-of select="$BusObjPackage"/>.*;


/*
 * Client application to test the <xsl:value-of select="$ClassName"/> Business Object.  Creates a new instance of the
 * object, sets attributes on it, and attempts to save it to the database.  The time required to execute
 * the test is calculated and written to "System.Out".  Multiple iterations of the test can be executed
 * by specifying a command line argument.
 *<xsl:call-template name="COPYRIGHT"> <xsl:with-param name="ClassName" select="$ClassName"/> </xsl:call-template>*/
public class <xsl:value-of select="$ClassName"/>Tester
{

   public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
   {
      /* Initialize the Object Server */
      System.out.println("Tester: Start of Test");
      try{
         ServerDeploy sd = null;
         System.out.println("Tester: Initializing Object Server");
         sd = (ServerDeploy)EJBObjectFactory.createClientEJB("t3://localhost:7001","ServerDeploy");  
         sd.init("t3://localhost:7001");
      }catch (Exception e) {}  
    
      /* Determine how many times to execute the test */
      int timesToExecute = new Integer(argv[0]).intValue();
      if (timesToExecute == 0) { timesToExecute = 1; }

      /* Start timing how long the test takes */
      ExecutionTimer timer = new ExecutionTimer();
      timer.setStart();

      /* Perform the test the number of times specified by the user */
      for(int i=0; !(i >= timesToExecute); i++)
         {executeTest();}
     
      /* Write out how long the test took. */
      timer.setEnd();
      System.out.println("Tester: End of Test");
      System.out.println("Tester: Execution Time per Call: " + timer.getTime() / timesToExecute);
   }


   protected static void executeTest() throws RemoteException, AmsException, RemoveException
   {
      <xsl:value-of select="$ClassName"/> testObject = null;

      /* Test the basic functionaility of the <xsl:value-of select="$ClassName"/> object.  This includes creating a
         new instance, setting attributes on it, and saving it to the database. */
      try{
         /* Create a new instance of the object */
         System.out.println("Tester: Creating instance of class <xsl:value-of select="$ClassName"/>");
         testObject = (<xsl:value-of select="$ClassName"/>)EJBObjectFactory.createClientEJB("t3://localhost:7001","<xsl:value-of select="$ClassName"/>");
         System.out.println("Tester: Calling newObject() method");
         testObject.newObject();

         /* Set attrbutes on the object */<xsl:call-template name="SET_ATTRIBUTES"/>

         /* Save the object to the database */
         System.out.println("Tester: Calling save() method");
         testObject.save();
         System.out.println("Tester: Save complete"); 
  
      /* If an Exception occurs, print out the call stack so we can determine where it came from */
      }catch(Exception e) {
         e.printStackTrace();

      /* Print out any errors that were issued by the Business Object */
      }finally {
         Vector errors = testObject.getIssuedErrors();
         for(int index = 0; !(index >= errors.size()); index++)
         {
            com.amsinc.ecsg.frame.IssuedError errorObject = (com.amsinc.ecsg.frame.IssuedError)errors.elementAt(index);
            System.out.println(errorObject.getErrorText());
         }
         testObject.remove();
      }
   }
}
-------------------- FINISH: <xsl:value-of select="$ClassName"/>Tester.java --------------------
</xsl:template>

<xsl:template name="SET_ATTRIBUTES">
<xsl:call-template name="ANCESTOR_SETS"/>
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Attribute">
   <xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
   <xsl:if test="not($Generate='False')" >
      <xsl:call-template name="SET_ATTRIBUTE"/>
   </xsl:if>
</xsl:for-each>
<xsl:call-template name="ASSOCIATIONS"><xsl:with-param name="ObjectType">Tester</xsl:with-param></xsl:call-template>
</xsl:template>


<!--
* 
-->
<xsl:template name="SET_ATTRIBUTE">
<xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.DataType/@xmi.idref"/>
<xsl:if test="not($AttributeLink=null)"> 
   <xsl:variable name="AttributeType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.DataType[@xmi.id=$AttributeLink]/Foundation.Core.ModelElement.name"/>
</xsl:if>
<xsl:if test="$AttributeLink=null">
   <xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.Class/@xmi.idref"/>
   <xsl:variable name="AttributeType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AttributeLink]/Foundation.Core.ModelElement.name"/>
</xsl:if>
<xsl:variable name="LogicalName" select="Foundation.Core.ModelElement.name"/>
<xsl:if test="not($AttributeType='ObjectIDAttribute')">
   <xsl:variable name="InitialValue" select="Foundation.Core.Attribute.initialValue/Foundation.Data_Types.Expression/Foundation.Data_Types.Expression.body"/>
   <xsl:if test="$InitialValue=null">
   <xsl:variable name="TestValue"><xsl:call-template name="TEST_VALUE"><xsl:with-param name="AttributeType" select="$AttributeType"/></xsl:call-template></xsl:variable>
         System.out.println("Tester: Setting attribute <xsl:value-of select="$LogicalName"/> to '<xsl:value-of select="$TestValue"/>'");
         testObject.setAttribute("<xsl:value-of select="$LogicalName"/>", "<xsl:value-of select="$TestValue"/>");
   </xsl:if></xsl:if>
</xsl:template>


<!--
*  Detemines the Ancestor (if any) of the class being processed.  If there is an ancestor, the SET_ATTRIBUTES template
*  is called for it, so we can generate code that will set any attributes that have been defined on the Ancestor as
*  well.
-->
<xsl:template name="ANCESTOR_SETS">
<xsl:variable name="GeneralizationID" select="Foundation.Core.GeneralizableElement.generalization/Foundation.Core.Generalization/@xmi.idref"/>
<xsl:if test="not($GeneralizationID=null)">
   <xsl:variable name="AncestorID" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.Generalization[@xmi.id=$GeneralizationID]/Foundation.Core.Generalization.supertype/Foundation.Core.Class/@xmi.idref"/>
   <xsl:for-each select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AncestorID]">
      <xsl:call-template name="SET_ATTRIBUTES"/>
   </xsl:for-each>
</xsl:if>
</xsl:template>


<!--
*  Determines the Value to set the attribute to in the test script.  This can be specified in the object model or
*  it will be defaulted to a standard value based upon the type of the attribute.
-->
<xsl:template name="TEST_VALUE">
<xsl:param name="AttributeType"/>
<xsl:variable name="TestValue" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:TestValue']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:if test="$TestValue=null">
  <xsl:choose>
     <xsl:when test="$AttributeType='NumberAttribute'"><xsl:value-of select="$NumberAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='TradePortalDecimalAttribute'"><xsl:value-of select="$TradePortalDecimalAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='TradePortalSignedDecimalAttribute'"><xsl:value-of select="$TradePortalSignedDecimalAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='DecimalAttribute'"><xsl:value-of select="$DecimalAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='AmountAttribute'"><xsl:value-of select="$AmountAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='DateAttribute'"><xsl:value-of select="$DateAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='DateTimeAttribute'"><xsl:value-of select="$DateTimeAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='IndicatorAttribute'"><xsl:value-of select="$IndicatorAttributeTestValue"/></xsl:when>
     <xsl:when test="$AttributeType='ReferenceAttribute'"><xsl:value-of select="$ReferenceAttributeTestValue"/></xsl:when>
     <xsl:otherwise><xsl:value-of select="$TextAttributeTestValue"/></xsl:otherwise>
  </xsl:choose>
</xsl:if>
<xsl:if test="not($TestValue=null)">
   <xsl:value-of select="$TestValue"/>
</xsl:if>
</xsl:template>


<xsl:include href="Imports.xsl"/>
<xsl:include href="Ancestor.xsl"/>
<xsl:include href="Comment.xsl"/>

</xsl:stylesheet>