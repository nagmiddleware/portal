<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">
<!--
  *  Generates the Home interface for Business Objects, Mediators, and any other
  *  EJB objects defined in the object model.
-->
<xsl:template name="HOME_INTERFACE">
<xsl:param name="PackageName"/> 
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/> 
-------------------- START: <xsl:value-of  select="$ClassName"/>Home.java --------------------
<xsl:variable name="OverridePackageName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:PackageName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>

  <xsl:if test="$OverridePackageName">
      <xsl:variable name="PackageName"><xsl:value-of select="$OverridePackageName" /></xsl:variable>
  </xsl:if>

package <xsl:value-of select="$PackageName"/><xsl:value-of select="$HomeInterfacePackage"/>;

import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;
/**
 * <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/></xsl:call-template>
 *<xsl:call-template name="COPYRIGHT"> <xsl:with-param name="ClassName" select="$ClassName"/> </xsl:call-template>*/
public interface <xsl:value-of select="$ClassName"/>Home extends EJBHome
{
   public <xsl:value-of select="$ClassName"/> create()
      throws RemoteException, CreateException, AmsException;

   public <xsl:value-of select="$ClassName"/> create(ClientServerDataBridge csdb)
      throws RemoteException, CreateException, AmsException;
}
-------------------- FINISH: <xsl:value-of select="$ClassName"/>Home.java --------------------
</xsl:template>
</xsl:stylesheet>