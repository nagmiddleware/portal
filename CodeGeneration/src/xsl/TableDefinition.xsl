<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Generates the SQL to create the Database Table in which the Business Object's persistent attributes will
*  be stored.  Both one-to-one and one-to-many, table-to-object mapping is supported.  
*  If a Business Object is inherited from an Ancestor, the attributes defined on the Ancestor will
*  be inclued in the Columns for the table used by the descendent.  No table definition is created is the
*  Business Object is designated as either 'Transient' or 'Abstract'.
-->
<xsl:template name="TABLE_DEFINITION">
<xsl:variable name="ClassName" select="Foundation.Core.ModelElement.name"/> 
<xsl:variable name="Abstract" select="Foundation.Core.GeneralizableElement.isAbstract/@xmi.value"/>
<xsl:variable name="Persistence" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='persistence']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="TableName">
   <xsl:call-template name="TABLE_NAME">
      <xsl:with-param name="ClassName" select="$ClassName"/>
   </xsl:call-template>
</xsl:variable>
<xsl:variable name="ColumnDefinitions">
   <xsl:call-template name="CLASSES_IN_TABLE">
      <xsl:with-param name="TableName" select="$TableName"/>
   </xsl:call-template>
</xsl:variable>
<xsl:variable name="PrimaryKey">
   <xsl:call-template name="PRIMARY_KEY"/>
</xsl:variable>
<xsl:if test="not($Abstract='true' or $Persistence='transient')">
-------------------- START: <xsl:value-of  select="$TableName"/>.sql --------------------
DROP TABLE <xsl:value-of select="$TableName"/>;

CREATE TABLE <xsl:value-of select="$TableName"/>(
<xsl:value-of select="$ColumnDefinitions"/><xsl:text>   </xsl:text>PRIMARY KEY(<xsl:value-of select="$PrimaryKey"/>));

-------------------- FINISH: <xsl:value-of select="$TableName"/>.sql --------------------
</xsl:if>
</xsl:template>


<!--
*  Determines the name of the Database Table in which the object's persistent attributes are stored.  This can be
*  specified explicitly with other DDL information for the Class in the object model.  If not specified, the table
*  name will default to the name of the class.
-->
<xsl:template name="TABLE_NAME">
<xsl:param name="$ClassName"/>
<xsl:variable name="ExplicitTableName" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:TableName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:if test="$ExplicitTableName=null">
   <xsl:value-of select="$ClassName"/>
</xsl:if>
<xsl:if test="not($ExplicitTableName=null)">
   <xsl:value-of select="$ExplicitTableName"/>
</xsl:if>
</xsl:template>


<!--
*  Processes all Classes that reside in the same table as the Class for which table generation was initiated.  This gives
*  the code generator the ability to store multiple classes in the same table, whether they are related through inheritance
*  or not.  Duplicate columns are removed.
-->
<xsl:template name="CLASSES_IN_TABLE">
<xsl:param name="TableName"/>
<xsl:variable name="ExistingColumns"><xsl:call-template name="COLUMNS"/></xsl:variable>
<xsl:for-each select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class">
   <xsl:variable name="ThisTable" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:TableName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
   <xsl:variable name="Abstract" select="Foundation.Core.GeneralizableElement.isAbstract/@xmi.value"/>
   <xsl:variable name="Persistence" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='persistence']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
   <xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
   <xsl:if test="$ThisTable=$TableName"><xsl:if test="not($Abstract='true' or $Persistence='transient')"><xsl:if test="not($Generate='False')">
      <xsl:variable name="NewColumns"><xsl:call-template name="COLUMNS"><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/></xsl:call-template></xsl:variable>
      <xsl:variable name="ExistingColumns"><xsl:value-of select="$ExistingColumns"/><xsl:value-of select="$NewColumns"/></xsl:variable>
   </xsl:if></xsl:if></xsl:if>
</xsl:for-each>
<xsl:value-of select="$ExistingColumns"/>
</xsl:template>


<!--
*  Creates the SQL to declare each of the columns for the Business Object's underlying table.  This info
*  is obtained by looping through the Attributes and Associations of the object.  This processes is also
*  performed on the Ancestor object(s) (if any) so that their attributes can be included in the table
*  definition.
-->
<xsl:template name="COLUMNS">
<xsl:param name="ExistingColumns"/>
<xsl:call-template name="ANCESTOR_COLUMNS"><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/></xsl:call-template>
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Attribute">
   <xsl:variable name="Generate" select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
   <xsl:if test="not($Generate='False')" >
      <xsl:call-template name="COLUMN"><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/></xsl:call-template>
   </xsl:if>
</xsl:for-each>
<xsl:call-template name="ASSOCIATIONS"><xsl:with-param name="ObjectType">TableDefinition</xsl:with-param><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/></xsl:call-template>
</xsl:template>


<!--
*  Creates a line in the SQL script that will define a Database Column for the Attribute being processed.  This is
*  done by calling templates to determine the attribute's Physical Name, Column Type, and Column Length.  The
*  results of these template calls are then formatted into the definition of the Database Column.
-->
<xsl:template name="COLUMN">
<xsl:param name="ExistingColumns"/>
<xsl:variable name="DDLType"       select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:ColumnType']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="DDLLength"     select="Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:Length']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.DataType/@xmi.idref"/>
<xsl:variable name="InitialValue"  select="Foundation.Core.Attribute.initialValue/Foundation.Data_Types.Expression/Foundation.Data_Types.Expression.body"/>
<xsl:if test="not($AttributeLink=null)"> 
   <xsl:variable name="AttributeType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.DataType[@xmi.id=$AttributeLink]/Foundation.Core.ModelElement.name"/>
</xsl:if>
<xsl:if test="$AttributeLink=null">
   <xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.Class/@xmi.idref"/>
   <xsl:variable name="AttributeType" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AttributeLink]/Foundation.Core.ModelElement.name"/>
</xsl:if>
<xsl:variable name="ColumnName">
   <xsl:call-template name="PHYSICAL_NAME">
      <xsl:with-param name="LogicalName" select="Foundation.Core.ModelElement.name"/>
   </xsl:call-template>
</xsl:variable>
<xsl:variable name="ColumnType">
   <xsl:call-template name="COLUMN_TYPE">
      <xsl:with-param name="AttributeType" select="$AttributeType"/><xsl:with-param name="DDLType" select="$DDLType"/>
   </xsl:call-template>
</xsl:variable>
<xsl:variable name="ColumnLength">
   <xsl:call-template name="COLUMN_LENGTH">
      <xsl:with-param name="AttributeType" select="$AttributeType"/><xsl:with-param name="DDLType" select="$DDLType"/><xsl:with-param name="DDLLength" select="$DDLLength"/>
   </xsl:call-template>
</xsl:variable>
<xsl:variable name="DefaultValue">
   <xsl:call-template name="DEFAULT_VALUE">
      <xsl:with-param name="InitialValue" select="$InitialValue"/>
   </xsl:call-template>
</xsl:variable>
<xsl:if test="not($AttributeType='DoubleDispatchAttribute' or $AttributeType='LocalAttribute' or $AttributeType='ComputedAttribute')">
<xsl:variable name="ColumnDefinition">
<xsl:text>   </xsl:text><xsl:value-of select="$ColumnName"/><xsl:text> </xsl:text><xsl:value-of select="$ColumnType"/><xsl:value-of select="$ColumnLength"/><xsl:value-of select="$DefaultValue"/>,
</xsl:variable>
<xsl:if test="not(contains($ExistingColumns, $ColumnDefinition))">
<xsl:if test="$AttributeType != 'LocalAttribute'">
	<xsl:value-of select="$ColumnDefinition"/>
</xsl:if>
</xsl:if>
</xsl:if>
</xsl:template>


<!--
*  Determines the Database Column Type for the attribute (e.g., NUMBER, VARCHAR2, etc.)  If a Column Type has been
*  specified in the DDL information for the attribute, then that is used.  If it has not been specified, then
*  the Column Type is derived based upon the Type of the Attribute (e.g., ObjectIDAttribute, DateAttribute,
*  ParentIDAttribute, etc.)
-->
<xsl:template name="COLUMN_TYPE">
   <xsl:param name="DDLType"/>
   <xsl:param name="AttributeType"/>
   <xsl:if test="$DDLType=null">
      <xsl:choose>
         <xsl:when test="$AttributeType='ObjectIDAttribute' or $AttributeType='ParentIDAttribute' or $AttributeType='OptimisticLockAttribute'">
            <xsl:text>NUMBER</xsl:text>
         </xsl:when>
         <xsl:when test="$AttributeType='DateTimeAttribute' or $AttributeType='DateAttribute'">
            <xsl:text>DATE</xsl:text>
         </xsl:when>
         <xsl:when test="$AttributeType='DecimalAttribute' or $AttributeType='NumberAttribute' or $AttributeType='TradePortalDecimalAttribute' or $AttributeType='TradePortalSignedDecimalAttribute'">
            <xsl:text>NUMBER</xsl:text>
         </xsl:when>
         <xsl:when test="$AttributeType='IndicatorAttribute'">
            <xsl:text>CHAR</xsl:text>
         </xsl:when>
         <xsl:when test="$AttributeType='ClobAttribute'">
            <xsl:text>CLOB</xsl:text>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>VARCHAR2</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:if>
   <xsl:if test="not($DDLType=null)">
      <xsl:value-of select="$DDLType"/>
   </xsl:if>
</xsl:template>


<!--
*  Determines the Length of the Database Column for the attribute.  If a Length has been specified in the DDL
*  information for the attribute, then that is used.  If it has not been specified, then Length is defaulted
*  based upon the Type of the Attribute (e.g., ObjectIDAttribute, DateAttribute, ParentIDAttribute, etc.)
-->
<xsl:template name="COLUMN_LENGTH">
   <xsl:param name="DDLLength"/>
   <xsl:param name="AttributeType"/>
   <xsl:param name="DDLType"/>
   <xsl:if test="$DDLLength=null">
      <xsl:choose>
         <xsl:when test="$DDLType='LONG RAW' or $DDLType='LONG' or $DDLType='DATE' or $DDLType='MLSLABEL'"></xsl:when>
         <xsl:when test="$AttributeType='ObjectIDAttribute' or $AttributeType='ParentIDAttribute' or $AttributeType='OptimisticLockAttribute'">
            <xsl:text>(15)</xsl:text>
         </xsl:when>
         <xsl:when test="$DDLType='CHAR' or $AttributeType='IndicatorAttribute'">
            <xsl:text>(1)</xsl:text>
         </xsl:when>
         <xsl:when test="$AttributeType='DateTimeAttribute' or $AttributeType='DateAttribute' or $AttributeType='ClobAttribute'">
         </xsl:when>
         <xsl:when test="$AttributeType='NumberAttribute'">
            <xsl:value-of select="$NumberAttributeLength"/>
         </xsl:when>
         <xsl:when test="$AttributeType='ReferenceAttribute'">
            <xsl:value-of select="$ReferenceAttributeLength"/>
         </xsl:when>
         <xsl:when test="$AttributeType='DecimalAttribute'">
            <xsl:value-of select="$DecimalAttributeLength"/>
         </xsl:when>
         <xsl:when test="$AttributeType='TradePortalDecimalAttribute'">
            <xsl:value-of select="$TradePortalDecimalAttributeLength"/>
         </xsl:when>
         <xsl:when test="$AttributeType='TradePortalSignedDecimalAttribute'">
            <xsl:value-of select="$TradePortalSignedDecimalAttributeLength"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$TextAttributeLength"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:if>
   <xsl:if test="not($DDLLength=null) and not($AttributeType='ClobAttribute')">
      <xsl:text>(</xsl:text><xsl:value-of select="$DDLLength"/><xsl:text>)</xsl:text>
   </xsl:if>
</xsl:template>

<xsl:template name="DEFAULT_VALUE">
   <xsl:param name="InitialValue"/>
   <xsl:if test="not($InitialValue=null)">
       <xsl:text> DEFAULT </xsl:text><xsl:value-of select="$InitialValue"/>
    </xsl:if>
</xsl:template>



<!--
*  Determines the Ancestor for the Business Object being processed and calls either the "COLUMNS" template or the
*  "PRIMARY_KEY" template for it.  This is done so (1) the Columns defined in the Ancestor can be included in the
*  table used by the Descendent and (2) the Primary Key can be located on the Ancestor if it is not defined on the
*  Descendent object.
-->
<xsl:template name="ANCESTOR_COLUMNS">
<xsl:param name="TemplateToCall">COLUMNS</xsl:param>
<xsl:param name="ExistingColumns"/>

<xsl:variable name="GeneralizationID" select="Foundation.Core.GeneralizableElement.generalization/Foundation.Core.Generalization/@xmi.idref"/>

<xsl:choose>
   <xsl:when test="not($GeneralizationID=null)">
   <xsl:variable name="AncestorID" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.Generalization[@xmi.id=$GeneralizationID]/Foundation.Core.Generalization.supertype/Foundation.Core.Class/@xmi.idref"/>
   <xsl:for-each select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AncestorID]">
      <xsl:if test="$TemplateToCall='COLUMNS'"><xsl:call-template name="COLUMNS"><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/></xsl:call-template></xsl:if>
      <xsl:if test="not($TemplateToCall='COLUMNS')"><xsl:call-template name="PRIMARY_KEY"/></xsl:if>
   </xsl:for-each>
   </xsl:when>
</xsl:choose>

</xsl:template>


<!--
*  Determines the Attribute / Column which will serve as the Primary key for the Table.  This is done by finding the
*  attribute on the Business Object (or its ancestor) that is type 'ObjectIDAttribute' and getting its physical
*  column name.
-->
<xsl:template name="PRIMARY_KEY">
<xsl:variable name="KeyXref" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Core.DataType[Foundation.Core.ModelElement.name='ObjectIDAttribute']/@xmi.id"/>
<xsl:if test="$KeyXref=null">
   <xsl:variable name="KeyXref" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[Foundation.Core.ModelElement.name='ObjectIDAttribute']/@xmi.id"/>
</xsl:if>
<xsl:for-each select="Foundation.Core.Classifier.feature/Foundation.Core.Attribute">
   <xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.DataType/@xmi.idref"/>
   <xsl:if test="$AttributeLink=null">
      <xsl:variable name="AttributeLink" select="Foundation.Core.StructuralFeature.type/Foundation.Core.Class/@xmi.idref"/>
   </xsl:if>
   <xsl:if test="$AttributeLink=$KeyXref">
       <xsl:call-template name="PHYSICAL_NAME">
          <xsl:with-param name="LogicalName" select="Foundation.Core.ModelElement.name"/>
       </xsl:call-template> 
   </xsl:if>
</xsl:for-each>
<xsl:call-template name="ANCESTOR_COLUMNS"><xsl:with-param name="TemplateToCall">PRIMARY_KEY</xsl:with-param></xsl:call-template>
</xsl:template>

<xsl:include href="Attribute.xsl"/>
<xsl:include href="Association.xsl"/>

</xsl:stylesheet>