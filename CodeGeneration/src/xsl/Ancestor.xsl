<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">

<!--
*  Determines the Ancestor of the Class being processed.  This is needed in the Class
*  declaration for the object so that we can specify which object the class "extends".
*  If no Ancestor has been specified in the object model, a default Ancestor (BusinesObject)
*  is used.  
-->

<xsl:template name="ANCESTOR">
<xsl:param name="DefaultAncestorClass">BusinessObject</xsl:param>
<xsl:variable name="GeneralizationID" select="Foundation.Core.GeneralizableElement.generalization/Foundation.Core.Generalization/@xmi.idref"/>
<xsl:choose>
   <xsl:when test="not($GeneralizationID=null)">
   <xsl:variable name="AncestorID" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Generalization[@xmi.id=$GeneralizationID]/Foundation.Core.Generalization.supertype/Foundation.Core.Class/@xmi.idref"/>
   <xsl:variable name="AncestorClass" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AncestorID]/Foundation.Core.ModelElement.name"/>
   <xsl:value-of select="$AncestorClass"/></xsl:when>
   <xsl:otherwise><xsl:value-of select="$DefaultAncestorClass"/></xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>