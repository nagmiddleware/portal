<!--
*  Loops through each Association specified for the object in the object model and calls the "ASSOCIATION" template
*  to register it with the attributeManager service object.  It should be noted that three distinct types of 
*  associations can be registered by this template.  These are:
*
*  (1) Component Pointer:  contains the oid of the component object if there is a one-to-one relationship between
*                          this object (the parent) and the component
*  (2) Parent ID Poiner:   contains the oid of the parent object if this object is a one-to-many component of the
*                          parent
*  (3) Normal Association: contains the oid of the associated object if this object is on the "Many" side of a 
*                          one to many relationship and it is not a component or parent of the associated object
*  
*  In order to understand what is going on in this style sheet, you need to userstand how associations are stored
*  in the XMI document.  Each Class in the model has a list of "Association Ends" to identify the other classes
*  that it is related to.  Instead of containing the Name or ID of the Associated object in the Association End,
*  an Xref ID to an AssociationEnd element (which resides elsewhere in the model) is specified.  The AssociationEnd is
*  actually an element within a Connection.  The Connection contains TWO AssociationEnds: the one that was identified
*  by (and points to) the Class that we are processing, and one that points to the Class that the object we are processing
*  is related to.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/XSL/Transform/1.0">
<xsl:template name="ASSOCIATIONS">
<xsl:param name="ObjectName"></xsl:param>
<xsl:param name="ObjectType">Bean</xsl:param>
<xsl:param name="ExistingColumns"/>
<xsl:for-each select="Foundation.Core.Classifier.associationEnd/Foundation.Core.AssociationEnd">
   <xsl:variable name="AssociationEndLink" select="@xmi.idref"/>
   <xsl:for-each select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Association/Foundation.Core.Association.connection/Foundation.Core.AssociationEnd[@xmi.id=$AssociationEndLink]">
      <xsl:variable name="Generate" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
      <xsl:if test="not($Generate='False')" >
         <xsl:call-template name="ASSOCIATION_END"><xsl:with-param name="ObjectName" select="$ObjectName"/><xsl:with-param name="ObjectType" select="$ObjectType"/><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/><xsl:with-param name="AssociationEndLink" select="$AssociationEndLink"/></xsl:call-template>
      </xsl:if>
   </xsl:for-each>
</xsl:for-each>
</xsl:template>


<!--
*  Called by the ASSOCATIONS template to identify when AssociationEnd in the Connection points to the Object being
*  processed and which AssociationEnd in the Connection points to the associated object.  Once this is determined,
*  the ASSOCIATION template is called (passing it this info) to process the actual association.
-->
<xsl:template name="ASSOCIATION_END">
<xsl:param name="ObjectName"/>
<xsl:param name="ObjectType"/>
<xsl:param name="AssociationEndLink"/>
<xsl:param name="ExistingColumns"/>
<xsl:choose>
   <xsl:when test="../Foundation.Core.AssociationEnd[1]/@xmi.id=$AssociationEndLink">
       <xsl:call-template name="ASSOCIATION"><xsl:with-param name="ObjectName" select="$ObjectName" /><xsl:with-param name="ThisObject">1</xsl:with-param> <xsl:with-param name="AssociatedObject">2</xsl:with-param><xsl:with-param name="ObjectType" select="$ObjectType"/><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/></xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
       <xsl:call-template name="ASSOCIATION"><xsl:with-param name="ObjectName" select="$ObjectName" /><xsl:with-param name="ThisObject">2</xsl:with-param><xsl:with-param name="AssociatedObject">1</xsl:with-param><xsl:with-param name="ObjectType" select="$ObjectType"/><xsl:with-param name="ExistingColumns" select="$ExistingColumns"/></xsl:call-template>
   </xsl:otherwise>
</xsl:choose>
</xsl:template>


<!--
*  Performs the registration of the Association with the attributeManager service object.  The type of Association
*  (or Attribute) to register (if any) is determined by the Cardinality and Aggregation specified for each End of the
*  associative relationship.  For example, if the Cardinalify of THIS object is one-to-many and no Aggregation is
*  specified for the ASSOCIATED object, then a simple association is registered.
-->
<xsl:template name="ASSOCIATION">
<xsl:param name="ThisObject">0</xsl:param>
<xsl:param name="AssociatedObject">0</xsl:param>
<xsl:param name="ObjectName"/>
<xsl:param name="ObjectType"/>
<xsl:param name="ExistingColumns"/>
<xsl:variable name="OidColumnType"> NUMBER(15),
</xsl:variable>
<xsl:variable name="AssociatedIDRef" select="../Foundation.Core.AssociationEnd[position()=$AssociatedObject]/Foundation.Core.AssociationEnd.type/Foundation.Core.Class/@xmi.idref"/>
<xsl:variable name="AssociatedClass" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AssociatedIDRef]/Foundation.Core.ModelElement.name"/>
<xsl:variable name="ClassIsGenerated" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AssociatedIDRef]/Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$Java:Generate']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:if test="not($ClassIsGenerated='False')">
<xsl:variable name="LinkageAttribute">
   <xsl:call-template name="LINKAGE_ATTRIBUTE">
     <xsl:with-param name="ClassName" select="$AssociatedClass"/>
     <xsl:with-param name="AssociatedObject" select="$AssociatedObject"/>
   </xsl:call-template>
</xsl:variable>
<xsl:variable name="RoleName"  select="../Foundation.Core.AssociationEnd[position()=$AssociatedObject]/Foundation.Core.ModelElement.name"/>
<xsl:variable name="ColumnName" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:ColumnName']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="Alias" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:Alias']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:variable name="NullsOK" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='RationalRose$DDL:NullsOK']/Foundation.Extension_Mechanisms.TaggedValue.value"/>
<xsl:if test="$ColumnName!=null"><xsl:variable name="LinkageColumn" select="$ColumnName"/></xsl:if>
<xsl:if test="$ColumnName=null"><xsl:variable name="LinkageColumn" select="$LinkageAttribute"/></xsl:if>
<xsl:variable name="ThisSideAggregation" select="../Foundation.Core.AssociationEnd[position()=$ThisObject]/Foundation.Core.AssociationEnd.aggregation/@xmi.value"/>
<xsl:variable name="OtherSideAggregation" select="../Foundation.Core.AssociationEnd[position()=$AssociatedObject]/Foundation.Core.AssociationEnd.aggregation/@xmi.value"/>
<xsl:variable name="ThisSideCardinality"  select="../Foundation.Core.AssociationEnd[position()=$ThisObject]/Foundation.Core.AssociationEnd.multiplicity"/>
<xsl:variable name="OtherSideCardinality" select="../Foundation.Core.AssociationEnd[position()=$AssociatedObject]/Foundation.Core.AssociationEnd.multiplicity"/>
<!-- Simple Association.  Need to register an attribute on the Business Object that points to the associated object.-->
<xsl:if test="not($ThisSideCardinality='1..1' or $ThisSideCardinality='0..1' or $ThisSideCardinality='1') and $OtherSideAggregation='none'">
   <xsl:if test="$ObjectType='Bean'">
      /* <xsl:value-of select="$LinkageAttribute"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */
      attributeMgr.registerAssociation("<xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$AssociationColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "<xsl:value-of select="$AssociatedClass"/>");
      <xsl:call-template name="REQUIRED_ASSOCIATION"><xsl:with-param name="ObjectName" select="$ObjectName" /><xsl:with-param name="LogicalName" select="$LinkageAttribute"/><xsl:with-param name="NullsOK" select="$NullsOK"/><xsl:with-param name="Alias" select="$Alias"/></xsl:call-template> 
   </xsl:if>
   <xsl:if test="$ObjectType='WebBean'">
<xsl:text>&#xa;</xsl:text>
       /* <xsl:value-of select="$LinkageAttribute"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */
       registerAttribute("<xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$AssociationColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "NumberAttribute");</xsl:if>
   <xsl:if test="$ObjectType='TableDefinition'">
      <xsl:variable name="ColumnDefinition">
         <xsl:text>   </xsl:text><xsl:value-of select="$AssociationColumnPrefix"/><xsl:value-of select="$LinkageColumn"/><xsl:value-of select="$OidColumnType"/>
      </xsl:variable>
      <xsl:if test="not(contains($ExistingColumns, $ColumnDefinition))">
         <xsl:value-of select="$ColumnDefinition"/>
      </xsl:if>
   </xsl:if>
   <xsl:if test="$ObjectType='Tester'">
         System.out.println("Tester: Setting association <xsl:value-of select="$LinkageAttribute"/> to ''");
         testObject.setAttribute("<xsl:value-of select="$LinkageAttribute"/>", "");
   </xsl:if>
</xsl:if>
<!-- Association is to the object's Parent in a One-To-Many relationship.  Need to register an attribute that points
     to the parent object. -->
<xsl:if test="not($ThisSideCardinality='1..1' or $ThisSideCardinality='0..1' or $ThisSideCardinality='1') and not($OtherSideAggregation='none')">
   <xsl:if test="$RoleName!=null">
      <xsl:if test="$ObjectType='Bean'">
      /* Pointer to the parent <xsl:value-of select="$AssociatedClass"/> */
       attributeMgr.registerAttribute("<xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$ParentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "ParentIDAttribute");
      </xsl:if>
      <xsl:if test="$ObjectType='WebBean'">
<xsl:text>&#xa;</xsl:text>
      /* Pointer to the parent <xsl:value-of select="$AssociatedClass"/> */
       registerAttribute("<xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$ParentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "ParentIDAttribute");</xsl:if>
         <xsl:if test="$ObjectType='TableDefinition'">
            <xsl:variable name="ColumnDefinition">
            <xsl:text>   </xsl:text><xsl:value-of select="$ParentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/><xsl:value-of select="$OidColumnType"/>
         </xsl:variable>
         <xsl:if test="not(contains($ExistingColumns, $ColumnDefinition))">
            <xsl:value-of select="$ColumnDefinition"/>
         </xsl:if>      
      </xsl:if>
   </xsl:if>
</xsl:if>
<!-- Association is to a One-to-One component of the object.  Need to register an attribute that points to the
    component object. -->
<xsl:if test="not($ThisSideCardinality='0..*' or $ThisSideCardinality='1..*') and
   ($OtherSideCardinality='1..1' or $OtherSideCardinality='0..1' or $OtherSideCardinality='1') and
   not($ThisSideAggregation='none')">

   <xsl:if test="$ObjectType='Bean'"><xsl:variable name="AssocName"><xsl:call-template name="ASSOC_NAME"><xsl:with-param name="ClassName" select="$LinkageAttribute"/><xsl:with-param name="AssociatedObject" select="$LinkageAttribute"/></xsl:call-template></xsl:variable>
      /* Pointer to the component <xsl:value-of select="$AssociatedClass"/> */
      /* <xsl:value-of select="$AssocName"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */<xsl:if test="$AssocName=''">attributeMgr.registerAttribute("<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "NumberAttribute");
        </xsl:if>
	<xsl:if test="not($AssocName='')">
      attributeMgr.registerAttribute("<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$AssocName"/>", "<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "NumberAttribute");
        </xsl:if>
   </xsl:if>

   <xsl:if test="$ObjectType='WebBean'">
	<xsl:variable name="AssocName">
	    <xsl:call-template name="ASSOC_NAME">
	      <xsl:with-param name="ClassName" select="$LinkageAttribute"/>
	      <xsl:with-param name="AssociatedObject" select="$LinkageAttribute"/>
	    </xsl:call-template>
	</xsl:variable>
	<xsl:if test="$AssocName=''">
<xsl:text>&#xa;</xsl:text><xsl:text>&#xa;</xsl:text>       /* <xsl:value-of select="$LinkageAttribute"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template>  */
       registerAttribute("<xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "NumberAttribute");</xsl:if>
	<xsl:if test="not($AssocName='')">
<xsl:text>&#xa;</xsl:text><xsl:text>&#xa;</xsl:text>       /* <xsl:value-of select="$AssocName"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */
       registerAttribute("<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$AssocName"/>", "<xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "NumberAttribute");</xsl:if>
   </xsl:if>

   <xsl:if test="$ObjectType='TableDefinition'">
      <xsl:variable name="ColumnDefinition">
         <xsl:text>   </xsl:text><xsl:value-of select="$ComponentPtrColumnPrefix"/><xsl:value-of select="$LinkageColumn"/><xsl:value-of select="$OidColumnType"/>
      </xsl:variable>
      <xsl:if test="not(contains($ExistingColumns, $ColumnDefinition))">
         <xsl:value-of select="$ColumnDefinition"/>
      </xsl:if>
   </xsl:if>
</xsl:if>
<!-- This is a One-to-One association that is not a Component relationship.  We just register it as a normal association -->
<xsl:if test="($ThisSideCardinality='1..1' or $ThisSideCardinality='0..1' or $ThisSideCardinality='1') and
   ($OtherSideCardinality='1..1' or $OtherSideCardinality='0..1' or $OtherSideCardinality='1') and
   ($ThisSideAggregation='none' and $OtherSideAggregation='none')">
   <xsl:variable name="ResolveOnOtherSide" select="../Foundation.Core.AssociationEnd[position()=$ThisObject]/Foundation.Core.AssociationEnd.isNavigable/@xmi.value"/>
   <xsl:if test="$ResolveOnOtherSide='false'">
   <xsl:if test="$ObjectType='Bean'">
      /* <xsl:value-of select="$LinkageAttribute"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */
      attributeMgr.registerAssociation("<xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$AssociationColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "<xsl:value-of select="$AssociatedClass"/>");
      <xsl:call-template name="REQUIRED_ASSOCIATION"><xsl:with-param name="Class" select="$ThisObject" /><xsl:with-param name="LogicalName" select="$LinkageAttribute"/><xsl:with-param name="NullsOK" select="$NullsOK"/><xsl:with-param name="Alias" select="$Alias"/></xsl:call-template> 
   </xsl:if>
   <xsl:if test="$ObjectType='WebBean'">
<xsl:text>&#xa;</xsl:text><xsl:text>&#xa;</xsl:text>       /* <xsl:value-of select="$LinkageAttribute"/> - <xsl:call-template name="COMMENT"><xsl:with-param name="OriginalComment" select="../../Foundation.Core.ModelElement.taggedValue/Foundation.Extension_Mechanisms.TaggedValue[Foundation.Extension_Mechanisms.TaggedValue.tag='documentation']/Foundation.Extension_Mechanisms.TaggedValue.value"/><xsl:with-param name="Indent">7</xsl:with-param></xsl:call-template> */
       registerAttribute("<xsl:value-of select="$LinkageAttribute"/>", "<xsl:value-of select="$AssociationColumnPrefix"/><xsl:value-of select="$LinkageColumn"/>", "NumberAttribute");</xsl:if>
   <xsl:if test="$ObjectType='TableDefinition'">
      <xsl:variable name="ColumnDefinition">
         <xsl:text>   </xsl:text><xsl:value-of select="$AssociationColumnPrefix"/><xsl:value-of select="$LinkageColumn"/><xsl:value-of select="$OidColumnType"/>
      </xsl:variable>
      <xsl:if test="not(contains($ExistingColumns, $ColumnDefinition))">
         <xsl:value-of select="$ColumnDefinition"/>
      </xsl:if>
   </xsl:if>
   <xsl:if test="$ObjectType='Tester'">
         System.out.println("Tester: Setting association <xsl:value-of select="$LinkageAttribute"/> to ''");
         testObject.setAttribute("<xsl:value-of select="$LinkageAttribute"/>", "");
   </xsl:if>
</xsl:if>
</xsl:if>
</xsl:if>
</xsl:template>

<!-- 
*  Determines the association name as set in Rose
-->
<xsl:template name="ASSOC_NAME">
<xsl:param name="ClassName"/>
<xsl:param name="AssociatedObject"/>
<xsl:variable name="AssocName" select="../../Foundation.Core.ModelElement.name"/>
<xsl:value-of select="$AssocName" />
</xsl:template>



<!--
*  Determines the name of the attribute that will be used to link the two objects in the association together.  To
*  do this, the template first looks for the name of the "Role" on Associated object.  If no "Role" is specified, it
*  looks for the name of the Association.  If the Association has not been named, it uses the Class name of the
*  Associated object. 
-->
<xsl:template name="LINKAGE_ATTRIBUTE">
<xsl:param name="ClassName"/>
<xsl:param name="AssociatedObject"/>
<xsl:variable name="RoleName"  select="../Foundation.Core.AssociationEnd[position()=$AssociatedObject]/Foundation.Core.ModelElement.name"/>
<xsl:variable name="AssocName" select="../../Foundation.Core.ModelElement.name"/>
<xsl:choose>
   <xsl:when test="not($RoleName=null)">
      <xsl:variable name="LinkageAttribute" select="$RoleName"/>
   </xsl:when>
   <xsl:otherwise>
      <xsl:variable name="LinkageAttribute" select="$ClassName"/>
   </xsl:otherwise>
</xsl:choose>
<xsl:value-of select="$LinkageAttribute"/>
</xsl:template>

<!-- 
*  Determines whether the Attribute is required or not.  If the Attribute has been designated as required, then
*  we need to call the "requiredAttribute()" method on attributeManager to register it as such.  Several types
*  of attributes do not need to be registered as required since they are set internally by the framework.  The
*  "ObjectIDAttribute" is an example of this. 
-->
<xsl:template name="REQUIRED_ASSOCIATION"><xsl:if test="$NullsOK=null"><xsl:variable name="NullsOK" select="$NullsOKDefault"/></xsl:if><xsl:if test="$NullsOK='False'">attributeMgr.requiredAttribute("<xsl:value-of select="$LogicalName"/>");</xsl:if><xsl:if test="$Alias=null or $Alias=''"></xsl:if>
	<xsl:if test="$Alias!=null and $Alias!=''">
      attributeMgr.registerAlias("<xsl:value-of select="$LogicalName"/>",getResourceManager().getText("<xsl:value-of select="$ObjectName" />BeanAlias.<xsl:value-of select="$LogicalName" />",TradePortalConstants.TEXT_BUNDLE));
</xsl:if>
</xsl:template>


<!--
*  Determines whether or not we should resolve the Association or not.
-->
<xsl:template name="RESOLVE_ASSOCIATION">
<xsl:param name="$AssociatedIDRef"/>
<xsl:variable name="ResolveAssoc">N</xsl:variable>
<xsl:variable name="StereotypeId" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement//Foundation.Core.Class[@xmi.id=$AssociatedIDRef]/Foundation.Core.ModelElement.stereotype/Foundation.Extension_Mechanisms.Stereotype/@xmi.idref"/>
<xsl:variable name="StereotypeName" select="/XMI/XMI.content/Model_Management.Model/Foundation.Core.Namespace.ownedElement/Foundation.Extension_Mechanisms.Stereotype[@xmi.id=$StereotypeId]/Foundation.Core.ModelElement.name"/>
<xsl:if test="$StereotypeName=null or $StereotypeName='BusinessObject'">
   <xsl:variable name="ResolveAssoc">Y</xsl:variable>
</xsl:if>
<xsl:value-of select="$ResolveAssoc"/>
</xsl:template>

<xsl:include href="Preferences.xsl" />

</xsl:stylesheet>