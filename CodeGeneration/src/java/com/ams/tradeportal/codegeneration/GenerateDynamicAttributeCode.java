package com.ams.tradeportal.codegeneration;

import java.io.*;
import java.util.*;

/**
 *  This class is used to generate the getAttributesToRegister method of the various
 *  subclasses of DynamicAttributeManager.   The purpose of the getAttributeToRegister method
 *  is to indicate to the bean being managed which attributes should be registered.  The method
 *  returns a vector of jPylon attributes.
 * 
 *  Determing which attributes should be registered for each subclass is determined
 *  by examining a CSV file.  The first row (other than rows that start with "IGNORE") of the CSV
 *  file is a list of all of the subclass names.  The first column is a list of all the attributes.
 *  An 'X' appears in the matrix for everyplace where the attribute is relevant for the subclass.
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class GenerateDynamicAttributeCode
 {
    private static String pathToManagerClasses;
    private static final String STRING_TO_LOOK_FOR = "public Vector getAttributesToRegister()";

    // Vector containing a list of vectors that are the attributes for each subclass
    private static Vector subclassAttributes = new Vector(20,5);    
    
    // Vector containing a list of strings that are the subclass names
    private static Vector subclassNames = new Vector(20,5);    

    /**
     *  Examines the CSV file to populate the two vectors that are attributes
     *  of this class.
     * 
     *  @param csvFilePath - the path to the CSV file being examined
     */
    public static void getAttributesForSubclasses(String csvFilePath)
    {
        try {
  	        File f = new File(csvFilePath);
      	    

	        if(f.exists())
            {
	  	      try(FileReader fr = new FileReader(f)){
	  	        boolean foundStartLine = false;
	  	        boolean onFirstDataRow = false;
    	
		        // Read in characters from file
	 	        char[] c = new char[(int)f.length()];
	 	        fr.read(c,0,(int)f.length());
    	 	    
     	        StringTokenizer lineTokenizer = new StringTokenizer(new String(c),"\n\r");
    	 	    
    	 	    // Loop through, looking at each line
                while(lineTokenizer.hasMoreTokens())
                {
                    StringTokenizer commaTokenizer = new StringTokenizer(lineTokenizer.nextToken(),",", true);
                    String attributeName = null; 
                    
                    int commaCount = 0;
                    boolean ignoringLine = false;
                    
                    // Loop through each comma separated value
                    while(!ignoringLine && commaTokenizer.hasMoreTokens())
                    {
                        String nextToken = commaTokenizer.nextToken();
                        
                        // Get the first character of the token to determine
                        // what to do with the token.  
                        char firstChar = nextToken.charAt(0);

                        // The token separators are returned as tokens.
                        // Ignore any of them.
                        if(firstChar != ',')    
                        {
                            // If 'IGNORE' is found as the first value in a row, ignore the row
                            if( (commaCount == 0) && (nextToken.equals("IGNORE")) )
                                {
                                    ignoringLine = true;
                                    continue;
                                }
                            
                            // If the first (start) line has not been found yet, set some flags.
                            if( !foundStartLine )
                                {
                                    foundStartLine = true;      
                                    onFirstDataRow = true;
                                }
                            
                            // If we're on the first data row, and it's not the first column,
                            // start reading in attribute names
                            if( (onFirstDataRow) && (commaCount !=0))
                                {
                                    // The first line, not the first one, no commas 
                                    subclassNames.addElement(nextToken);
                                    subclassAttributes.addElement(new Vector(25,15));
                                }
                            else
                                {
                                    // We are looking at a row that contains an attribute and several X's
                                    if(commaCount == 0)
                                        {
                                        // We found an attribute name
                                        attributeName = nextToken;
                                        }
                                    else
                                        if(firstChar == 'X')
                                        {
                                            // Add this attribute name to the vector of attributes for the subclass
                                            ((Vector)subclassAttributes.elementAt(commaCount - 1)).addElement(attributeName);
                                        }
                                }  
              
                                
                            }
                            else
                            commaCount++;
                            
                        }
                        attributeName = null;
                        onFirstDataRow = false;

                    
                }
	  	       }
            }
        }
        catch (Exception e)
            {
                
            e.printStackTrace();   
            }    
        
    }
 
 
    /**
     * This method replaces the existing getAttributesToRegister() method code with 
     * code that creates the attributes specified in the CSV file.
     */
    public static void updateManagerClasses(int index, String factoryClassName)
    {
        // Build filename based on subclass name
        String fileName = (String)subclassNames.elementAt(index)+".java";
      
        try {

            
            // Get a handle to the file
  	        File f = new File(pathToManagerClasses+fileName);

	        if(f.exists())
            {
	        	 String fileContents ="";
	  	            try(FileReader fr = new FileReader(f)){

		            // Read in characters from file
	  	            	char[] c = new char[(int)f.length()];
	  	            	fr.read(c,0,(int)f.length());    
	  	            	fileContents = new String(c);
	  	            }
	 	            // Get index of the start of the getAttributesToRegister method
	 	            int startOfMethod = fileContents.indexOf(STRING_TO_LOOK_FOR);
        	 	    
    	 	        String everythingBeforeMethod;
    	 	        String everythingAfterMethod;
        	 	   
    	 	        // Method already exists...
    	 	        if(startOfMethod >= 0)
    	 	        {
     	                // Get all text from the method on (including the method)
	 	                String afterStartOfMethod = fileContents.substring(fileContents.indexOf(STRING_TO_LOOK_FOR), fileContents.length());
            	 	
	 	                // Figure out where the method ends
 	                    int locationOfEndBrace = afterStartOfMethod.indexOf("}");
            	 	    
	 	                // Get a handle to everything before the method
	 	                everythingBeforeMethod = fileContents.substring(0,startOfMethod -1);
            	 	    
	 	                // Get a handle to everything after the method
	 	                everythingAfterMethod = afterStartOfMethod.substring(locationOfEndBrace+1,afterStartOfMethod.length());
    	 	        }
    	 	        else
    	 	        // Method does not already exist...
    	 	        {
    	 	            everythingBeforeMethod = fileContents.substring(0,fileContents.indexOf("{") + 2);
    	 	            everythingAfterMethod = fileContents.substring(fileContents.indexOf("{")+1,fileContents.length());
    	 	        }
        	 	     
        	 	     
        	 	    
        	 	    
                    // Create a stringbuffer to build the updated file, pre-populating it with everything before the method
  	 	            StringBuffer sb = new StringBuffer(everythingBeforeMethod);
        	 	    
	 	            // Replace the method text with this static text:
	 	            sb.append("    public Vector getAttributesToRegister()\n");
	 	            sb.append("     {\n");
	 	            sb.append("        Vector attrList = super.getAttributesToRegister();\n");
        	 	    
                    // Loop through the attributes for this subclass	 	   
	 	            Vector attributes = (Vector) subclassAttributes.elementAt(index);
        	 	    	 	    
	 	            for(int i=0;i<attributes.size();i++)
	 	            {
	 	                // Output a method call to create the attribute by using the factory
	 	                sb.append("        attrList.addElement( ");
	 	                sb.append(factoryClassName);
	 	                sb.append(".create_");
	 	                sb.append(attributes.elementAt(i));
	 	                sb.append("() );\n");
	 	            }
        	 	    
	 	            // End the method
	 	            sb.append("\n        return attrList;\n");
	 	            sb.append("     }\n");
        	 	    
	 	            // Put all the source after the method
                    sb.append(everythingAfterMethod);
                    
                    // Now overwrite the previous manager file with the new data
                    try(FileOutputStream outfile = new FileOutputStream(pathToManagerClasses+fileName);
            	
		            PrintWriter writer = new PrintWriter(outfile)){

                    // Write out the string buffer and close the file
		            writer.print(sb.toString());
		            writer.flush();
                    }
		            System.out.println("Updated file: "+fileName);
	 	        }
	 	    else
	 	        {
	 	        System.out.println("ERROR: File");
	 	        System.out.println(pathToManagerClasses+fileName);
	 	        System.out.println(" does not exist");
    	        }

  	        }
    	 	   
	        catch(Exception e)
	        {
	        if(e.getMessage().indexOf("Access is denied") > 0)
	            System.out.println("ERROR: Access is denied to "+fileName);
	        else
	            {
	            System.out.println("Exception occurred while updating / creating method on file");
	            e.printStackTrace();
	            }
    	        
	        }
        
        
        
        
    }




    public static void main(String argc[])
    {
        String csvFilePath = null;
        String factoryClassName = null;
        
        if(argc.length != 0)
        {
            csvFilePath = argc[0];
        }
        else
        {
            System.out.println("No file path for CSV file specified as the first argument...");
            return;
        }
        
        if(argc.length >= 2)
         {
           factoryClassName = argc[1];  
         }
        else
         {
          System.out.println("No factoryClassName was specified as the second argument...");
         }
   
        if(argc.length >= 3)
         {
           pathToManagerClasses = argc[2];  
         }
        else
         {
          System.out.println("No path to manager classes was specified as the third argument...");
         }
          
        // Read from CSV file 
        // This populates the two vectors that are private static attributes of this class
        getAttributesForSubclasses(csvFilePath);

        // Loop through each file, updating the getAttributesToRegister() method for each
        for(int i=0; i< subclassNames.size(); i++)
        {
            updateManagerClasses(i, factoryClassName);
        }
               
    }
 
 
 }