package com.ams.tradeportal.codegeneration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import com.lotus.xsl.XSLProcessor;

/*
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class CodeGenerator
{
   /* Constants.  Used internally and by objects calling this class. */
   final static String XML_DOCUMENT            = "XML";
   final static String STYLE_SHEETS            = "XSL";
   final static String ENTERPRISE_JAVA_BEANS   = "EJB";
   final static String REMOTE_INTERFACES       = "REMOTE";
   final static String HOME_INTERFACES         = "HOME";
   final static String distMENT_DESCRIPTORS  = "DD";
   final static String WEB_BEANS               = "WEB";
   final static String TABLE_DEFINITIONS       = "SQL";
   final static String TESTERS                 = "TEST";
  // final static String BUILD_SCRIPTS           = "BUILD";
   final static String EXISTING_CODE           = "EXIST";
   
   /* Generation Options */ 
   protected boolean genEnterpriseJavaBeans    = true;
   protected boolean genRemoteInterfaces       = true;
   protected boolean genHomeInterfaces         = true;
   protected boolean gendistmentDescriptors  = true;
   protected boolean genWebBeans               = true;
   protected boolean genTableDefinitions       = true;
   protected boolean genTesters                = true;
  // protected boolean genBuildScripts           = true;
   
   /* Generation Inputs */
   protected String  inputXMLDocument         = ".\\doc\\rose\\";
   protected String  dirStyleSheets           = ".\\dist\\xsl";
   protected String  classToGenerate          = "";
   protected String  dirExistingCode          = ".\\dist";
   
   /* Output Directories */
   protected String  dirEnterpriseJavaBeans   = ".\\dist\\generated";
   protected String  dirRemoteInterfaces      = ".\\dist\\generated";
   protected String  dirHomeInterfaces        = ".\\dist\\generated";
   protected String  dirdistmentDescriptors = ".\\dist\\generated";
   protected String  dirWebBeans              = ".\\dist\\generated";
   protected String  dirTableDefinitions      = ".\\dist\\generated";
   protected String  dirTesters               = ".\\dist\\generated";
   protected String  dirBuildScripts          = ".\\dist\\generated";

  
   public static void main(String argc[])
    {
	CodeGenerator cg = new CodeGenerator();
	if(argc.length > 1)
         {
           cg.classToGenerate = argc[1];
           cg.inputXMLDocument = cg.inputXMLDocument + argc[0];
         }
        else if (argc.length == 1)
         {
           cg.inputXMLDocument = cg.inputXMLDocument + argc[0];
         }

	cg.generate();



    }   


  /*
   *  Generates Enterprise Application Framework code from an XMI (eXtensible Metadata
   *  Interchange) representaion of a Object Model.  The framework objects generated 
   *  include Business Objects, Remote Interfaces, Home Interfaces, distment Descriptors,
   *  and Web Beans.
   */   
   public String generate()
   {
     int objectsGenerated;
     try
     {
        generateFromXML();                   
        objectsGenerated = splitUpFile();

        System.out.println("Output Files Generated: " + objectsGenerated);
     }
     catch(Exception e)
     {
        String errorMessage = e.getClass() + " " + e.getMessage();
        System.out.println(errorMessage);
        return errorMessage;
     }
     return objectsGenerated + " Files Generated.";
   }

   
  /*
   *  Employs XSL style sheets to generate framework objects from an "XML" representation
   *  of the object model.  The generated code is placed in a single file with each
   *  object delimited by a Header and Footer.  The single file is generated because
   *  style sheets can only output to a single source.
   */
   protected void generateFromXML()
      throws java.io.IOException, java.net.MalformedURLException, org.xml.sax.SAXException
   {
      /* Create the IBM XSL Processor. */
      XSLProcessor processor = new XSLProcessor();
      
      /* Put the output from the Code Generation stylesheets into a file in the Stylesheet
         directory called "AllCode.out". */
      PrintWriter pw = new PrintWriter(new FileWriter(dirStyleSheets + "//AllCode.out"));
      
      /* Set parameters in the "CodeGeneration" stylesheet which will determine which types
         of objects are generated. */
      if (! genEnterpriseJavaBeans) {
        processor.setStylesheetParam("GenBean", "'N'");}
      if (! genHomeInterfaces) {
        processor.setStylesheetParam("GenHome", "'N'");}
      if (! genRemoteInterfaces) {
        processor.setStylesheetParam("GenRemote", "'N'");}
      if (! gendistmentDescriptors) {
        processor.setStylesheetParam("Gendist", "'N'");}
      if (! genWebBeans) {
        processor.setStylesheetParam("GenWebBean", "'N'");}
      if (! genTableDefinitions) {
        processor.setStylesheetParam("GenDDL", "'N'");}
      if (! genTesters) {
        processor.setStylesheetParam("GenTester", "'N'");}
      if (classToGenerate.length() > 0) {
        processor.setStylesheetParam("ObjectToGenerate", "'"+classToGenerate+"'");}
      
      /* Apply the StyleSheet to the XML Document, thus generating one large file with
         all of the code in it. */
      processor.process(inputXMLDocument, dirStyleSheets.concat("//CodeGeneration.xsl"), pw);
   }


  /*
   *  Splits the resulting file from the XML code generation process into a separate file
   *  for each object generated.  This function reads through the generated code file
   *  and identifies the Start of each object. 
   */
   protected int splitUpFile()
      throws java.io.IOException
   {
     try(BufferedReader in = new BufferedReader(new FileReader(dirStyleSheets + "//AllCode.out"))){
      int objectCount = 0;
      while (true)
      {
        String lineOfCode = in.readLine();
        if (lineOfCode == null) break;
        if (lineOfCode.startsWith("-------------------- START: "))
        {  
           objectCount++;
           processObject(lineOfCode, in);
        }
      }
      
      return objectCount;
      }
   }


  /*
   *  Writes out the code for a specific object to the appropriate output file.  The name
   *  of the file is included in the Header for the object.
   */
   protected void processObject(String fileHeader, BufferedReader in)
      throws java.io.IOException
   {
      /* Determine the file name for the object being processed.  This is stored in a
         standard "Header" record before the code for the object actually begins. */ 
      String headerString   = fileHeader.substring(28);
      String fileName       = headerString.substring(0, headerString.indexOf(" -"));
      
      /* Based upon the object's file name, determine which diretory to place it in */
      String fileDir        = determineDirectory(fileName);
      
      /* Determine whether we want to attempt to merge in code from an existing class */
      boolean mergeCode = (fileName.endsWith("Bean.java")|| fileName.endsWith("WebBean.java")
                           && dirExistingCode.length()>0 );
 

      /* Open up the output file for the object's code. */
      try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileDir + "//" + fileName)))){


         writeOutCode(in, out);
      }     
   }
 
  
  /*
   * Writes the code for the generated object to the output file being created.  The
   * code is written line for line with no manipulation until the end of the generated
   * object is encountered.
   */
   protected void writeOutCode(BufferedReader in, PrintWriter out)
      throws java.io.IOException
   {
      /* Keep reading through the master file, line by line, and writing it to the output
         file, until the end of the object's code is found. */
      while (true)
      {
        String lineOfCode = in.readLine();
        if (lineOfCode!=null && lineOfCode.startsWith("-------------------- FINISH: ")) break;
        out.println(lineOfCode);
      }
   }


  /*
   *  Based on the type of object being processed, determine which directory it should
   *  be written to.  For example, distment Descriptors are often placed in a separate
   *  directory than java modules are.
   */
   protected String determineDirectory(String fileName)
   {
       System.out.println("File Name: " + fileName);
       if (fileName.endsWith("WebBean.java"))
       { return dirWebBeans; 
          
       }else if (fileName.endsWith("Bean.java"))
       { return dirEnterpriseJavaBeans;
        
       }else if (fileName.endsWith("DD.txt"))
       { return dirdistmentDescriptors; 
          
       }else if (fileName.endsWith(".sql"))
       { return dirTableDefinitions; 
         
       }else if (fileName.endsWith("Home.java"))
       { return dirHomeInterfaces; 
       
       }else if (fileName.endsWith("Tester.java"))
       { return dirTesters;
       
       }else
       { return dirRemoteInterfaces; }
   }
 
 
   /*
    * Sets an instance variable specifying the directory in which generated objects
    * of a certain type (Remote Interfaces, distment Descriptors, etc.) should be
    * placed.
    */
   public void setDirectory(String objectType, String directoryPath)
   {
      if (objectType.equals(ENTERPRISE_JAVA_BEANS))
      {dirEnterpriseJavaBeans = directoryPath;}
      else if (objectType.equals(REMOTE_INTERFACES))
      {dirRemoteInterfaces = directoryPath;}
      else if (objectType.equals(HOME_INTERFACES))
      {dirHomeInterfaces = directoryPath;}
      else if (objectType.equals(distMENT_DESCRIPTORS))
      {dirdistmentDescriptors = directoryPath;}
      else if (objectType.equals(WEB_BEANS))
      {dirWebBeans = directoryPath;}
      else if (objectType.equals(TABLE_DEFINITIONS))
      {dirTableDefinitions = directoryPath;}
      else if (objectType.equals(XML_DOCUMENT))
      {inputXMLDocument = directoryPath;}
      else if (objectType.equals(STYLE_SHEETS))
      {dirStyleSheets = directoryPath;}
      else if (objectType.equals(TESTERS))
      {dirTesters = directoryPath;}
      else if (objectType.equals(EXISTING_CODE))
      {dirExistingCode = directoryPath;}
   }

   /*
    * Sets an instance variable indicating whether the type of object specified
    * (Web Beans, Table Defintions, etc.) should be generated or not.
    */
   public void setGeneration(String objectType, boolean genObject)
   {
      if (objectType.equals(ENTERPRISE_JAVA_BEANS))
      {genEnterpriseJavaBeans = genObject;}
      else if (objectType.equals(REMOTE_INTERFACES))
      {genRemoteInterfaces = genObject;}
      else if (objectType.equals(HOME_INTERFACES))
      {genHomeInterfaces = genObject;}
      else if (objectType.equals(distMENT_DESCRIPTORS))
      {gendistmentDescriptors = genObject;}
      else if (objectType.equals(WEB_BEANS))
      {genWebBeans = genObject;}
      else if (objectType.equals(TABLE_DEFINITIONS))
      {genTableDefinitions = genObject;}
      else if (objectType.equals(TESTERS))
      {genTesters = genObject;}
   
   }

   
   /*
    * Sets an instance variable identifing a specific class to generate.
    * If not specified, all classes defined in the object model are generated.
    */
   public void setClassToGenerate(String className)
   {
       classToGenerate = className;
   }


}