The *.cmd files in this folder are used by developers to run Agents on their local machines.

Here are the steps to follow:

1. RUN WEBLOGIC LOCALLY

3. RUN LOG SERVER LOCALLY

   C:\Proponix-TPx.x.x.x\Agents>runloggerserver
  
4. RUN AGENT SERVER LOCALLY

   C:\Proponix-TPx.x.x.x\Agents>runAgentServer

5. START AGENT

   C:\Proponix-TPx.x.x.x\Agents> startInboundAgent

   or 

   C:\Proponix-TPx.x.x.x\Agents> startOutboundAgent

   or 

   C:\Proponix-TPx.x.x.x\Agents> startMQAgent

6. IF YOU NEED TO, SET UP DATA FOR PROCESSING.  BE CAREFUL IF WORKING WITH SERVER ENVIRONMENT.
   
   Incoming_queue: set status to R.  Make sure other items in status R are changed to some temporary status such as Z.
   Outgoing_queue: set status to S.  Make sure other items in status S are changed to some temporary status such as Z.
