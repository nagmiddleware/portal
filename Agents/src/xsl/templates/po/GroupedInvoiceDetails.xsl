<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:import href="../FOCommon.xsl"/>
   <xsl:template name="GroupedInvoiceDetails" match="EmailAttachment/SectionC/InvoiceDetailsValue">
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:block>
         <fo:inline>Payment Covering:</fo:inline>
      </fo:block>
      <fo:block>
         <fo:leader leader-pattern="space"/>
      </fo:block>
      <fo:table font-size="8pt">
         <fo:table-column column-width="2in"/>
         <fo:table-column column-width="2in"/>
         <fo:table-column column-width="2.2in"/>
         <fo:table-column column-width="2.4in"/>
         <fo:table-header>
            <fo:table-row height="15pt">
               <fo:table-cell>
                  <fo:block>
                     <xsl:text>Invoice Id</xsl:text>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <xsl:text>Currency</xsl:text>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block >
                     <xsl:text>Amount</xsl:text>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <xsl:text>Due/Payment Date</xsl:text>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </fo:table-header>
         <fo:table-body>
            <xsl:apply-templates select="/EmailAttachment/SectionC/InvoiceDetailsValue"/>
         </fo:table-body>
      </fo:table>

   </xsl:template>
   <xsl:template match="/EmailAttachment/SectionC/InvoiceDetailsValue">
      <xsl:variable name="newline">
         <xsl:text>
         </xsl:text>
      </xsl:variable>
      <xsl:for-each select="./EndToEndID">
         <xsl:sort select="./id" order="descending"/>
         <xsl:for-each select="./RowEntry">            
            <xsl:sort select="./Type" order="descending"/>
            <xsl:sort select="./Amount" order="descending"/>
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <xsl:value-of select="./InvoiceID"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <xsl:value-of select="./Currency"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <xsl:value-of select="./Amount"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <xsl:value-of select="./PaymentDate"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </xsl:for-each>
         <fo:table-row>
            <fo:table-cell>
               <fo:block>
                  <xsl:value-of select="$newline"/>
               </fo:block>
            </fo:table-cell>
         </fo:table-row>
         <xsl:if test="./id != '_'">
            <fo:table-row>
               <fo:table-cell>
                  <fo:block>
                     <xsl:text>Subtotal:</xsl:text>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <xsl:value-of select="./ccy"/>
                  </fo:block>
               </fo:table-cell>
               <fo:table-cell>
                  <fo:block>
                     <xsl:value-of select="./subtotal"/>
                  </fo:block>
               </fo:table-cell>
            </fo:table-row>
         </xsl:if>
         <fo:table-row>
            <fo:table-cell>
               <fo:block>
                  <xsl:value-of select="$newline"/>
               </fo:block>
            </fo:table-cell>
         </fo:table-row>

      </xsl:for-each>
      <fo:table-row>
         <fo:table-cell>
            <fo:block>
               <xsl:value-of select="$newline"/>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>

</xsl:stylesheet>
