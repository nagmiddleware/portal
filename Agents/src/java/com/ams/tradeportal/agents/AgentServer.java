/*
 * AgentServer.java
 *
 * Created on June 28, 2001, 9:58 AM
 */
package com.ams.tradeportal.agents;

import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.*;

/*
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 * @author  kyoung
 */
public class AgentServer {

    public static final String START_ACTION = "start";
    public static final String STOP_ACTION = "stop";

    /**
     * Default port to listen on if none specified in the properties file
     */
    public static final int DEFAULT_CONNECTION_PORT = 4001;
    /**
     * Maximum number of threads to start if not specified in the properties
     * file
     */
    public static final int DEFAULT_MAX_THREADS = 20;
    /**
     * Default server to listen to if none is specified in the properties file
     */
    public static final String DEFAULT_SERVER_LOCATION = "127.0.0.1";
    /**
     * Delimiter between instruction parameters
     */
    public static final String INSTRUCTION_DELIMITER = "$$";

    /**
     * Port number the AgentServer listens on
     */
    private int _connectionPort;
    /**
     * Maximum number of threads that this process allows to run
     */
    private int _maxThreads;
    /**
     * Number of threads running at any given time
     */
    private int _activeThreadCount = 0;
    private String _serverLocation;
    private ServerSocket _serverSocket = null;
    /**
     * Hashtable that stores all running threads
     */
    private Hashtable _threadHash = new Hashtable();

    /**
     * Action either start or stop
     */
    private String _action;
    /**
     * Fully qualified class name of the current thread to start
     */
    private String _className;
    /**
     * Process Id of
     */
    private String _processId;

    private boolean _doneProcessing = false;

    /**
     * Creates new AgentServer
     */
    public AgentServer() throws Exception {
        PropertyResourceBundle myProps;
        String propValue;

        myProps = (PropertyResourceBundle) PropertyResourceBundle.getBundle("AgentConfiguration");

        _connectionPort = DEFAULT_CONNECTION_PORT;
        propValue = getPropertyValue(myProps, "agentServerConnectionPort");
        if (propValue != null) {
            try {
                _connectionPort = Integer.parseInt(propValue);
            } catch (NumberFormatException nfEx) {
                // Default Connection Port is used
            }
        }
        System.out.println("Connection Port Used for AgentServer = " + _connectionPort);

        _maxThreads = DEFAULT_MAX_THREADS;
        propValue = getPropertyValue(myProps, "maxAgentServerThreads");
        if (propValue != null) {
            try {
                _maxThreads = Integer.parseInt(propValue);
            } catch (NumberFormatException nfEx) {
                // Default Max Thread Count is used
            }
        }
        System.out.println("Maximum Threads created by AgentServer = " + _maxThreads);

        _serverLocation = DEFAULT_SERVER_LOCATION;
        propValue = getPropertyValue(myProps, "serverLocation");
        if (propValue != null) {
            _serverLocation = propValue;
        }
        System.out.println("Location of the AgentServer = " + _serverLocation);

        attachToPort(_serverLocation, _connectionPort);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        AgentServer agentServer = null;

        try {
            agentServer = new AgentServer();
        } catch (Exception ex) {
            System.out.println("Exception starting AgentServer: " + ex);
            ex.printStackTrace();
        }
        if (agentServer != null) {
            agentServer.process();
        }

    }

    private void process() {
        Socket clientSocket;
        String instructions;

        for (;;) {
            _action = "";
            _className = "";
            _processId = "";

            try {
                clientSocket = _serverSocket.accept();
                instructions = readDataFromSocket(clientSocket);
                if (instructions != null) {
                    processInstructions(instructions);
                    if (_doneProcessing) {
                        return;
                    }
                }
            } catch (IOException ioEx) {
                System.out.println("Could not open client socket" + ioEx);
            }
        }

    }

    /**
     * Establish our connection to the port we are listening on.
     *
     * @param server The name of the server we are listing to.
     * @param port The number of the port we are listening on.
     */
    private void attachToPort(String serverLocation, int port) throws IOException {
        InetAddress serverAddr;

        try {
            serverAddr = InetAddress.getByName(serverLocation);
        } catch (UnknownHostException uhEx) {
            System.out.println("Server Location lookup for: " + serverLocation + " failed");
            throw uhEx;
        }
        try {
            _serverSocket = new ServerSocket(port, 99999999, serverAddr);
        } catch (BindException bEx) {
            System.out.println("Bind to port: " + port + " failed");
            throw bEx;
        } catch (SocketException sEx) {
            System.out.println("Bind to port: " + port + " failed");
            throw sEx;
        }
    }

    private String readDataFromSocket(Socket socket) {
        BufferedReader reader;
        String socketData = null;

        while (socket == null) {
            try {
                wait();
            } catch (InterruptedException iEx) { // Should never happen
                return null;
            }
        }

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ioEx) {
            System.out.println("Could not open socket for input: " + ioEx);
            return null;
        }

        try {
            socketData = reader.readLine();
            if (socketData == null) {
                return null;
            }
        } catch (IOException ioEx) {
            System.out.println("Could not read from client socket" + ioEx);
        }

        try {
            if (reader != null) {
                reader.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ioEx) {
            // should never happen
        }
        return socketData;
    }

    /**
     * This method starts a Thread of the className type.
     *
     * @param className The name of the runnable class to start
     * @param processId A unique Id to ensure when multiple threads of the same
     * type are started, they can be easily identified.
     * @return Indicates whether or the instructions were read correctly
     */
    private void processInstructions(String instructions) {
        Thread processThread;

        if (!readInstructions(instructions)) {
            System.out.println("Agent Server has received in invalid instruction: " + instructions);
            return;
        }

        if (_action.equals(AgentServer.STOP_ACTION)) {
            if (_className.equals("")) {
                stopAllThreads();
            } else {
                processThread = (Thread) this._threadHash.get(_className + _processId);
                if (processThread != null) {
                    stopThread(processThread);
                    this._threadHash.remove(_className + _processId);
                } else {
                    System.out.println("Process with class name:" + _className + " and process id: " + _processId + " is not running");
                }
            }
        } else if (_action.equals(AgentServer.START_ACTION)) {
            startThread(_className, _processId);
        }
    }

    /**
     * This method loops through all the hashtable entries then attempts to stop
     * each thread. After each thread is successfully stopped, the thread itself
     * is stopped.
     *
     */
    private void stopAllThreads() {
        // Grabs all the keys from the hashtable
        Enumeration stopEnum = this._threadHash.keys();
        // Loops through all the hashtable entries to stop each thread
        while (stopEnum.hasMoreElements()) {
            Thread myThread = (Thread) _threadHash.get((String) stopEnum.nextElement());
            this.stopThread(myThread);
        }
        this._doneProcessing = true;
    }

    /**
     * This method stops the Thread that is passed in as a parameter
     *
     * @param thread a running thread.
     */
    private void stopThread(Thread thread) {
        // Signal the thread that is should stop processing
        try {
            // Wait until the thread dies to continue processing
            while (thread.isAlive()) {
                thread.interrupt();
                thread.join(5000);
            }
        } catch (Exception e) {
            System.out.println("System error while stopping thread: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * This method starts a Thread of the className type.
     *
     * @param className The name of the runnable class to start
     * @param processId A unique Id to ensure when multiple threads of the same
     * type are started, they can be easily identified.
     * @return Indicates whether or the instructions were read correctly
     */
    private void startThread(String _className, String _processId) {
        Thread processThread = null;

        // If we have not already exceeded the maximum number of threads
        if (_activeThreadCount < this._maxThreads) {
            // If the thread is not already running
            if (!this._threadHash.containsKey(_className + _processId)) {
                try {
                    // Instantiate a new thread to run
                    Class threadClass = (Class) Class.forName(_className);
                    Runnable instance = (Runnable) threadClass.newInstance();

                    Class[] parameterTypes = new Class[1];
                    parameterTypes[0] = Class.forName("java.lang.String");
                    Object[] arguments = new Object[1];
                    arguments[0] = _processId;

                    Method setAgentId = threadClass.getMethod("setAgentId", parameterTypes);
                    setAgentId.invoke(instance, arguments);

                    processThread = new Thread(instance);

                    parameterTypes[0] = Class.forName("java.lang.Thread");
                    arguments[0] = processThread;
                    Method setMyThread = threadClass.getMethod("setMyThread", parameterTypes);
                    setMyThread.invoke(instance, arguments);

                } // If the class identified by className is not found
                catch (ClassNotFoundException cnfe) {
                    System.out.println("Error finding thread with class name: " + _className + " and process id: " + _processId);
                } // If there is trouble instantiating the class
                catch (InstantiationException ie) {
                    System.out.println("Error instantiating thread with class name: " + _className + " and process id: " + _processId);
                } catch (Exception e) {
                    System.out.println("Error starting thread with class name: " + _className + " and process id: " + _processId);
                    e.printStackTrace();
                }

                if (processThread != null) {
                    // Ensure that if the AgentServer is abnormally stopped, ensure all the subordinate threads are stopped
                    processThread.setDaemon(true);
                    try {
                        // start the thread
                        processThread.start();
                        // add an entry to the _threadHash to hold the newly started thread
                        this._threadHash.put(_className + _processId, processThread);
                    } catch (IllegalThreadStateException itse) {
                        System.out.println("Error starting thread with class name: " + _className + " and process id: " + _processId);
                    }
                }
            } else {
                System.out.println("Process with class name:" + _className + " and process id: " + _processId + " is already running");
            }
        }
    }

    /**
     * This method parses instructions and sets some static methods the rest of
     * the process will use.
     *
     * @param myProps The resource bundle to query for a value.
     * @param instructions A set of instructions to be parsed by this method
     * @return Indicates whether or the instructions were read correctly
     */
    private boolean readInstructions(String instructions) {

        int firstDelimiter = instructions.indexOf(AgentServer.INSTRUCTION_DELIMITER);
        if (firstDelimiter < 1) {
            _action = _action + instructions;
            if (!_action.equals(AgentServer.STOP_ACTION)) {
                return false;
            } else {
                return true;
            }
        }

        int secondDelimiter = instructions.indexOf(AgentServer.INSTRUCTION_DELIMITER, firstDelimiter + 1);
        if (secondDelimiter < 1) {
            return false;
        }

        _action = _action + instructions.substring(0, firstDelimiter);
       _className = _className+instructions.substring (firstDelimiter +
                                           AgentServer.INSTRUCTION_DELIMITER.length (),
                                           secondDelimiter);
       _processId = _processId+instructions.substring (secondDelimiter + 
                                           AgentServer.INSTRUCTION_DELIMITER.length (),
                                           instructions.length ());
              
       return true;
       
    }

    /**
     * Wrapper aroung the PropertyResourceBundle class's getString method. If a
     * MissingResourceException is caught, we return <code>null</code>,
     * otherwise we return the getString method's return value.
     *
     * @param myProps The resource bundle to query for a value.
     * @param key The resource bundle key to find the value of.
     * @return The key's value if one is found, otherwise <code>null</code>.
     */
    private static String getPropertyValue(PropertyResourceBundle myProps, String key) {
        try {
            return myProps.getString(key);
        } catch (MissingResourceException mrEx) {
            return null;
        }
    }

}
