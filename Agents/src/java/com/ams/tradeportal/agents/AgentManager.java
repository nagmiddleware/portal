/*
 * AgentManager.java
 *
 * Created on June 28, 2001, 10:26 AM
 */
package com.ams.tradeportal.agents;

import java.util.*;
import java.io.*;
import java.net.*;

/**
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 * @author  kyoung
 */
public class AgentManager {

    /**
     * Default port to listen on if none specified in the properties file
     */
    public static final int DEFAULT_CONNECTION_PORT = 4001;
    /**
     * Maximum number of threads to start if not specified in the properties
     * file
     */
    public static final String DEFAULT_SERVER_LOCATION = "127.0.0.1";

    private static int _connectionPort;
    private static String _serverLocation;
    private static PropertyResourceBundle myProps;

    /**
     * Creates new AgentManager
     */
    public AgentManager() {
    }

    public static synchronized void main(String args[]) {

        String serverAction;
        String agentName;
        String instanceId;

        try {
            myProps = (PropertyResourceBundle) PropertyResourceBundle.getBundle("AgentConfiguration");
        } catch (Exception e) {
            System.out.println("The AgentConfiguration.properties file can not be read.");
        }

        int numberOfParameters = args.length;
        if (numberOfParameters == 0) {
            AgentManager.outputUsageToConsole();
        } else if (numberOfParameters == 1) {
            serverAction = args[0];
            if (serverAction.equals(AgentServer.STOP_ACTION)) {
                AgentManager.sendInstructionsToAgentServer(serverAction);
            } else {
                AgentManager.outputUsageToConsole();
            }
        } else if (numberOfParameters == 2) {
            AgentManager.outputUsageToConsole();
        } else if (numberOfParameters == 3) {
            serverAction = args[0];
            agentName = args[1];
            instanceId = args[2];

            if ((serverAction.equals(AgentServer.STOP_ACTION)) || (serverAction.equals(AgentServer.START_ACTION))) {
                if ((agentName != null) && (instanceId != null)) {
                    AgentManager.sendInstructionsToAgentServer(serverAction + "$$" + agentName + "$$" + instanceId);
                } else {
                    AgentManager.outputUsageToConsole();
                }
            } else {
                AgentManager.outputUsageToConsole();
            }
        } else {
            AgentManager.outputUsageToConsole();
        }
    }

    private static synchronized void sendInstructionsToAgentServer(String instructions) {
        try {
            AgentManager.deriveConfigurationSettings();
            // Create a client socket
            try (Socket clientSock = new Socket(_serverLocation, _connectionPort);
                    OutputStream stream = clientSock.getOutputStream();
                    PrintWriter writer = new PrintWriter(stream)) {
                // Write the message to the socket
                writer.println(instructions);
                writer.flush();

            }
        } catch (Exception e) {
            System.out.println("Exception caught in AgentManager::sendInstructionsToAgentServer");
            System.out.println(e);
        }
    }

    private static synchronized void deriveConfigurationSettings() {

        _connectionPort = DEFAULT_CONNECTION_PORT;
        String propValue = getPropertyValue(myProps, "agentServerConnectionPort");
        if (propValue != null) {
            try {
                _connectionPort = Integer.parseInt(propValue);
            } catch (NumberFormatException nfEx) {
                // Default Connection Port is used
            }
        }
        System.out.println("Connection Port Used for AgentServer = " + _connectionPort);

        _serverLocation = DEFAULT_SERVER_LOCATION;
        propValue = getPropertyValue(myProps, "serverLocation");
        if (propValue != null) {
            _serverLocation = propValue;
        }
        System.out.println("Location of the AgentServer = " + _serverLocation);
    }

    /**
     * If an error occurs in this class due to invalid input data the message
     * contained in this message is written to the console.
     *
     */
    private static synchronized void outputUsageToConsole() {
        System.out.println("Usage: AgentManager start|stop AgentName InstanceID");
        System.out.println("");
        System.out.println("       - start or stop must be the first parameter.");
        System.out.println("");
        System.out.println("       - if start, the AgentName and InstanceID parameters");
        System.out.println("         must exist.");
        System.out.println("");
        System.out.println("       - if stop, either the AgentName and InstanceID parameters");
        System.out.println("         must exist or neither parameter should exist.  If");
        System.out.println("         neither parameter exists all Agents will be stopped as");
        System.out.println("will the AgentServer.");
        System.out.println("");
        System.out.println("       - AgentName corresponds to a value in the");
        System.out.println("         AgentConfiguration.properties file");
    }

    /**
     * Wrapper aroung the PropertyResourceBundle class's getString method. If a
     * MissingResourceException is caught, we return <code>null</code>,
     * otherwise we return the getString method's return value.
     *
     * @param myProps The resource bundle to query for a value.
     * @param key The resource bundle key to find the value of.
     * @return The key's value if one is found, otherwise <code>null</code>.
     */
    private static synchronized String getPropertyValue(PropertyResourceBundle myProps, String key) {
        try {
            return myProps.getString(key);
        } catch (MissingResourceException mrEx) {
            return null;
        }
    }

}
