package com.ams.tradeportal.agents;

import org.apache.xerces.dom.TextImpl;
import org.w3c.dom.*;
import org.xml.sax.*;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import com.amsinc.ecsg.frame.Logger;

// Referenced classes of package com.ams.tradeportal.agents:
//            AgentDOMParserWrapper

/*
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AgentDOMCount {

    private static final String DEFAULT_PARSER_NAME = "com.ams.tradeportal.agents.AgentDOMParser";
    private static boolean setValidation = true;
    private static boolean setNameSpaces = true;
    private static boolean setSchemaSupport = true;
    private static boolean setDeferredDOM = true;
    private long elements;
    private long attributes;
    private long characters;
    private long ignorableWhitespace;
    private static String loggerCategory;

    static {
        PropertyResourceBundle properties = (PropertyResourceBundle) ResourceBundle.getBundle("AgentConfiguration");
        loggerCategory = properties.getString("loggerServerName");
    }

    public AgentDOMCount() {
    }

    public static boolean count(InputSource inputSource, String agentID) {
        boolean isXmlValidAgainstDtd = false; // default dtd validation to failure
        try {
            String s = DEFAULT_PARSER_NAME;
            AgentDOMParserWrapper agentdomparserwrapper = (AgentDOMParserWrapper) Class.forName(s).newInstance();
            AgentDOMCount agentdomcount = new AgentDOMCount();
            long l = System.currentTimeMillis();
            agentdomparserwrapper.setAgentID(agentID);
            agentdomparserwrapper.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", setDeferredDOM);
            agentdomparserwrapper.setFeature("http://xml.org/sax/features/validation", setValidation);
            agentdomparserwrapper.setFeature("http://xml.org/sax/features/namespaces", setNameSpaces);
            agentdomparserwrapper.setFeature("http://apache.org/xml/features/validation/schema", setSchemaSupport);
            Document document = agentdomparserwrapper.parse(inputSource);
            agentdomcount.traverse(document);
            long l1 = System.currentTimeMillis();
            agentdomcount.printResults(l1 - l, agentID);
            isXmlValidAgainstDtd = agentdomparserwrapper.isXmlValidAgainstDtd(); // get current dtd validation result
        } catch (SAXParseException | SAXNotRecognizedException | SAXNotSupportedException _ex) {
            Logger.log(loggerCategory, agentID, "AgentDOMCount: " + _ex.getMessage(), 5);
        } catch (SAXException saxexception) {
            if (saxexception.getException() != null) {
                Logger.log(loggerCategory, agentID, "AgentDOMCount: " + saxexception.getException().getMessage(), 5);

            } else {
                Logger.log(loggerCategory, agentID, "AgentDOMCount: " + saxexception.getMessage(), 5);

            }
        } catch (Exception exception) {
            Logger.log(loggerCategory, agentID, "AgentDOMCount: " + exception.getMessage(), 5);

        }
        return isXmlValidAgainstDtd; // return current dtd validation result
    }

    public void printResults(long l, String agentID) {
        StringBuilder sb = new StringBuilder();
        sb.append("AgentDOMCount: DTD Validation Statistics: ");
        sb.append(l);
        sb.append(" ms (");
        sb.append(elements);
        sb.append(" elems, ");
        sb.append(attributes);
        sb.append(" attrs, ");
        sb.append(ignorableWhitespace);
        sb.append(" spaces, ");
        sb.append(characters);
        sb.append(" chars)");

        Logger.log(loggerCategory, agentID, sb.toString(), 1);

    }

    public void traverse(Node node) {
        if (node == null) {
            return;
        }
        short word0 = node.getNodeType();
        switch (word0) {
            case 2: // '\002'
            case 6: // '\006'
            case 7: // '\007'
            case 8: // '\b'
            default:
                break;

            case 9: // '\t'
                elements = 0L;
                attributes = 0L;
                characters = 0L;
                ignorableWhitespace = 0L;
                traverse(((Document) node).getDocumentElement());
                break;

            case 1: // '\001'
                elements++;
                NamedNodeMap namednodemap = node.getAttributes();
                if (namednodemap != null) {
                    attributes += namednodemap.getLength();
                }
                NodeList nodelist1 = node.getChildNodes();
                if (nodelist1 == null) {
                    break;
                }
                int j = nodelist1.getLength();
                for (int l = 0; l < j; l++) {
                    traverse(nodelist1.item(l));
                }

                break;

            case 5: // '\005'
                NodeList nodelist = node.getChildNodes();
                if (nodelist == null) {
                    break;
                }
                int i = nodelist.getLength();
                for (int k = 0; k < i; k++) {
                    traverse(nodelist.item(k));
                }

                break;

            case 4: // '\004'
                characters += node.getNodeValue().length();
                break;

            case 3: // '\003'
                if (node instanceof TextImpl) {
                    if (((TextImpl) node).isIgnorableWhitespace()) {
                        ignorableWhitespace += node.getNodeValue().length();
                    } else {
                        characters += node.getNodeValue().length();
                    }
                } else {
                    characters += node.getNodeValue().length();
                }
                break;
        }
    }
}
