/*
 * Main.java
 *
 * Created on October 10, 2006, 10:20 PM
 *
 * @author Phil Cutrone
 *
 * This program runs two queries to determine what images need to be extracted for the customer.
 * The first query is run agains an OTL oracle database to retrieve all images associated to formatted (TradeForm) documents
 * that have send to party that matches the customer id(s) provided.
 * The second query retrieves all scanned images for Document Examination activities that have been flagged as send to the
 * portal customer id that matches the customer id(s) provided.
 * The second query can be turned on and off via a parameter to the program.
 * Both queries can be further filtered by activity type, instrument type, and release date
 * After the each query is complete, the image documents are extracted as PDFs via the GETPDFs.asp page on the image web server.
 * Log Files and PDFs are written to the paths specified in additional parameters to the program.
 */

package com.cgi.proponix.PDFExtract;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Vector;

public class Main {

    private static String dbUser = null;
    private static String dbPassword = null;
    private static String dbConn = null;
    private static String customerIDs = null;
    private static Vector customerIDList = new Vector();
    private static String dateFrom = null;
    private static String dateTo = null;
    private static String productTypes = null;
    private static String activityTypes = null;
    private static String currLogFileDir = null;
    private static String currPdfOutDir = null;
    private static String pdfOutDir = null;
    private static String logFileDir = null;
    private static String logFileName = null;
    private static boolean paymentDocExams = false;
    private static boolean extractPDF = true;
    private static String imageServerURL = null;

    private static char forwardSlash = '/';
    private static char underScore = '_';

    public Main() {
    }

    /**
     * This program must be called with the following arguments in any order...
     *
     * DBUSER=devotlsystestplay353 - oracle DB User ID
     * DBPASSWORD=otl - oracle DB password
     * DBCONN=jdbc:oracle:thin:@PPXDB03:1521:PPXDEV04 - jdbc:oracle:drivertype:@hostname:port:instance/service
     * PDFOUTDIR=C:/PDFOUT - Root folder of the PDF extraction.
     * LOGFILEDIR=C:/PDFOUT - Root folder of the Report Log File. Is equal to PDFOUTDIR if left blank.
     * CUSTOMERID=CPGBCRICKET-ZL/A,CPUSBATS-ZL/A - Filter items by a single Customer ID or a with comma separated list of Customer IDs.
     * DATEFROM=01-JAN-2002 - Filter items by activities released after and including this date. Format dd-MMM-yyyy
     * DATETO=01-JAN-2007 - Filter items by activities released before and including this date. Format dd-MMM-yyyy
     * PRODUCTTYPES=IMP - Filter items by a single instrument product type or with a comma separated list of instrument product types
     * ACTIVITYTYPES=ISS,AMD,PAY - Filter items by a single activity type or with a comma separated list of activity types
     * PAYMENTDOCEXAMS=Y - Run query for Payment Doc Examination images (if left out or set to N, query will not run)
     * NOPDF=N - NOPDF=Y do not run PDF extraction only query and logging. NOPDF=N or missing, PDFs are extracted as well.  Good for testing.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        initialize(args);                   // Parse and validate arguments
        getPDFs(getDateAndTime());          // Run PDF extraction routine
        System.exit(1);                     // Exit program
    }

    private static void getPDFs(String runDateAndTime) {
        for (int i=0; i<customerIDList.size(); i++) {
            String currentCustID = (String)customerIDList.elementAt(i);
            currLogFileDir = logFileDir + "/" + runDateAndTime + "/" + currentCustID.replace(forwardSlash,underScore);
            currPdfOutDir = pdfOutDir + "/" + runDateAndTime + "/" + currentCustID.replace(forwardSlash,underScore);
            File logDirMaker = new File(currLogFileDir);
            if (!logDirMaker.mkdirs() && !logDirMaker.exists()) {
                System.out.println("[!ERROR!]-Creating Directory [" + currLogFileDir + "] failed. Exiting Application.");
                System.exit(1);
            }
            logFileName = currLogFileDir + "/" + currentCustID.replace(forwardSlash,underScore) + ".LOG";
            BufferedWriter out;
            try {
                out = new BufferedWriter(new FileWriter(logFileName, false));
                out.write("PDF EXTRACT FOR [" + currentCustID + "] - Run at [" + runDateAndTime + "]");
                out.newLine();
                out.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            long appStartTime = System.currentTimeMillis();
            queryCustomer(currentCustID);
            long appRunTime = System.currentTimeMillis() - appStartTime;
            DecimalFormat pattern = new DecimalFormat("#");
            String minutesToRun = pattern.format(Math.floor(appRunTime/60000F));
            String secondsToRun = String.valueOf(Integer.valueOf(pattern.format(Math.floor(appRunTime/1000))).intValue() - (Integer.valueOf(minutesToRun).intValue() * 60));
            logMessage("[INFO] - PDF Extraction Complete!... Time to run extraction = " + minutesToRun + " Minute(s) and " + secondsToRun + " Second(s).");
        }
    }

    private static void initialize(String args[]) {
        for (int i = 0; i < args.length; i++) {
            System.out.println("Argument " + i + " = " + args[i]);
            StringTokenizer argument = new StringTokenizer(args[i], "=");
            String argName = argument.nextToken();
            if (argName.toUpperCase().equals("DBUSER"))
                dbUser = argument.nextToken();
            else if (argName.toUpperCase().equals("DBPASSWORD"))
                dbPassword = argument.nextToken();
            else if (argName.toUpperCase().equals("DBCONN"))
                dbConn = argument.nextToken();
            else if (argName.toUpperCase().equals("PDFOUTDIR"))
                pdfOutDir = argument.nextToken();
            else if (argName.toUpperCase().equals("CUSTOMERID"))
                customerIDs = argument.nextToken();
            else if (argName.toUpperCase().equals("DATEFROM"))
                dateFrom = argument.nextToken();
            else if (argName.toUpperCase().equals("DATETO"))
                dateTo = argument.nextToken();
            else if (argName.toUpperCase().equals("PRODUCTTYPES"))
                productTypes = argument.nextToken();
            else if (argName.toUpperCase().equals("ACTIVITYTYPES"))
                activityTypes = argument.nextToken();
            else if (argName.toUpperCase().equals("LOGFILEDIR"))
                logFileDir = argument.nextToken();
            else if (argName.toUpperCase().equals("PAYMENTDOCEXAMS"))
                if (argument.nextToken().toUpperCase().equals("Y"))
                    paymentDocExams = true;
                else
                    paymentDocExams = false;
            else if (argName.toUpperCase().equals("NOPDF"))
                if (argument.nextToken().toUpperCase().equals("Y"))
                    extractPDF = false;
                else
                    extractPDF = true;
        }
        if (dbUser == null) {
            System.out.println("!ERROR!-Argument [DBUSER] not specified");
            printProgramUsage();
            System.exit(1);
        }
        if (dbPassword == null) {
            System.out.println("!ERROR!-Argument [DBPASSWORD] not specified");
            printProgramUsage();
            System.exit(1);
        }
        if (dbConn == null) {
            System.out.println("!ERROR!-Argument [DBCONN] not specified");
            printProgramUsage();
            System.exit(1);
        }
        if (customerIDs == null) {
            System.out.println("!ERROR!-Argument [CUSTOMERID] not specified");
            printProgramUsage();
            System.exit(1);
        } else {
            String customerTokens = customerIDs;
            StringTokenizer customers = new StringTokenizer(customerTokens, ",");
            int tokenCounter = customers.countTokens();
            if (tokenCounter > 0) {
                for (int i=0; i < tokenCounter; i++)
                    customerIDList.addElement(customers.nextToken());
            }
        }
        if (dateFrom == null) {
            System.out.println("!ERROR!-Argument [DATEFROM] not specified");
            printProgramUsage();
            System.exit(1);
        }
        if (dateTo == null) {
            System.out.println("!ERROR!-Argument [DATETO] not specified");
            printProgramUsage();
            System.exit(1);
        }
        if (productTypes == null) {
            System.out.println("!ERROR!-Argument [PRODUCTTYPES] not specified");
            printProgramUsage();
            System.exit(1);
        } else {
            String productTypeTokens = productTypes;
            StringTokenizer products = new StringTokenizer(productTypeTokens, ",");
            int tokenCounter = products.countTokens();
            productTypes = "";
            if (tokenCounter > 0) {
                for (int i=0; i < tokenCounter; i++) {
                    if (i < tokenCounter-1)
                        productTypes = productTypes + "\'" + products.nextToken() + "\',";
                    else
                        productTypes = productTypes + "\'" + products.nextToken() + "\'";
                }
            }
        }

        if (activityTypes == null) {
            System.out.println("!ERROR!-Argument [ACTIVITYTYPES] not specified");
            printProgramUsage();
            System.exit(1);
        } else {
            String activityTypeTokens = activityTypes;
            StringTokenizer activities = new StringTokenizer(activityTypeTokens, ",");
            int tokenCounter = activities.countTokens();
            activityTypes = "";
            if (tokenCounter > 0) {
                for (int i=0; i < tokenCounter; i++) {
                    if (i < tokenCounter-1)
                        activityTypes = activityTypes + "\'" + activities.nextToken() + "\',";
                    else
                        activityTypes = activityTypes + "\'" + activities.nextToken() + "\'";
                }
            }
        }
        if (paymentDocExams == false) {
            System.out.println("!WARNING!-Argument [PAYMENTDOCEXAMS] is missing or set to N. Payment Doc exams not will not be retrieved");
        }
        if (extractPDF == false) {
            System.out.println("!WARNING!-Argument [NOPDF] manually set to N. Query and log Only. PDFs will NOT be extracted.");
        }
        if (pdfOutDir == null) {
            pdfOutDir = System.getProperty("user.dir");
            System.out.println("!WARNING!-Argument [PDFOUTDIR] not specified. Using application path [" + pdfOutDir + "]");
        }
        if (logFileDir == null) {
            logFileDir = pdfOutDir;
            System.out.println("!WARNING!-Argument [LOGFILEDIR] not specified. Using PDFOUTDIR [" + pdfOutDir + "]");
        }
        System.out.println("[INFO]-All Arguments Parsed Successfully!");
    }

    private static String getDateAndTime() {

        TimeZone timeZone = TimeZone.getDefault();
        String timeZoneValue = timeZone.getDisplayName(true, TimeZone.SHORT);

        Calendar cal = new GregorianCalendar();
        // Get the components of the date
        int year = cal.get(Calendar.YEAR);
        // 2002
        int month = cal.get(Calendar.MONTH);           // 0=Jan, 1=Feb, ...
        String monthValue = null;
        if (month == 0) monthValue = "JAN";
        else if (month == 1) monthValue = "FEB";
        else if (month == 2) monthValue = "MAR";
        else if (month == 3) monthValue = "APR";
        else if (month == 4) monthValue = "MAY";
        else if (month == 5) monthValue = "JUN";
        else if (month == 6) monthValue = "JUL";
        else if (month == 7) monthValue = "AUG";
        else if (month == 8) monthValue = "SEP";
        else if (month == 9) monthValue = "OCT";
        else if (month == 10) monthValue = "NOV";
        else if (month == 11) monthValue = "DEC";

        int day = cal.get(Calendar.DAY_OF_MONTH);      // 1...
        String runDate =  String.valueOf(year) + "-" + monthValue + "-" + toLZ(day,2);
        // Get the components of the time
        int hour24 = cal.get(Calendar.HOUR_OF_DAY);     // 0..23
        int min = cal.get(Calendar.MINUTE);             // 0..59
        int sec = cal.get(Calendar.SECOND);             // 0..59
        String runTime = String.valueOf(toLZ(hour24,2) + "-" + toLZ(min,2) + "-" + toLZ(sec, 2));

        String runDateAndTime = "(" + runDate + " " + runTime + " " + timeZoneValue + ")";
        return runDateAndTime;
    }

    private static void queryCustomer(String customerID) {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection connection = DriverManager.getConnection(
                    dbConn,
                    dbUser,
                    dbPassword);

            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Get Image Server URL to connect to...
            if (imageServerURL == null) {
                Statement imageURLsql = connection.createStatement();
                String imageURLsqlString = "SELECT DISTINCT IMAGE_SERVER_URL FROM SETTINGS";
                imageURLsql.execute(imageURLsqlString);
                ResultSet imageURLresult = imageURLsql.getResultSet();
                while (imageURLresult.next()) {
                    imageServerURL = imageURLresult.getString("IMAGE_SERVER_URL");
                    if (imageServerURL != null) {
                        imageServerURL = imageServerURL + "/idmws/getPDFs.asp?ID=";
                        break;
                    }
                }
                imageURLsql.close();
                if (imageServerURL != null)
                    System.out.println("[INFO]-Image Server URL is [" + imageServerURL + "]");
                else {
                    System.out.println("[WARNING]-Image Server URL is null, images will not be extracted");
                    extractPDF = false;
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("######################################################################");
            System.out.println("[INFO]-Execute PDF Queries for Customer [" + customerID + "]");
            // Get all formatted documents for send to parties with an advice method of portal for specified customer, activities, product type, and release date range
            Statement sql = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String sqlString = "SELECT DISTINCT CUSTOMER.CUSTOMER_ID, ACTIVITY.A_ACTIVITY_TYPE, INSTRUMENT.INSTRUMENT_ID, ACTIVITY.SEQ_NUM_OF_ACTV_TY, TO_CHAR(ACTIVITY.RELEASE1_TIME+(ORGANIZATION.GMT_DIFF_HOURS/24),'dd-MON-yyyy') AS RELEASE1_TIME, TO_CHAR(ACTIVITY.RELEASE2_TIME+(ORGANIZATION.GMT_DIFF_HOURS/24), 'dd-MON-yyyy') AS RELEASE2_TIME, IMAGE_DOC_INDEX.IMAGE_DOC_ID, IMAGE_DOC_INDEX.NUM_PAGES " +
                    "FROM IMAGE_DOC_INDEX, FORMATTED_DOCUMENT, ACTIVITY, INSTRUMENT, CUSTOMER, PARTY, OUTGOING_DOC_PTY, ORGANIZATION " +
                    "WHERE FORMATTED_DOCUMENT.A_IMAGE_DOC_INDEX = IMAGE_DOC_INDEX.UOID AND " +
                    "FORMATTED_DOCUMENT.P_ACTIVITY = ACTIVITY.UOID AND " +
                    "FORMATTED_DOCUMENT.UOID = OUTGOING_DOC_PTY.P_FORM_DOC AND " +
                    "OUTGOING_DOC_PTY.A_OBJECT = PARTY.A_PARTY_TYPE AND " +
                    "PARTY.A_CUSTOMER = CUSTOMER.UOID AND " +
                    "PARTY.A_INSTRUMENT = INSTRUMENT.UOID AND " +
                    "ACTIVITY.P_INSTRUMENT = INSTRUMENT.UOID AND " +
                    "ORGANIZATION.UOID = INSTRUMENT.A_OPER_BK_ORG_ORIG AND " +
                    "ACTIVITY.A_ACTIVITY_TYPE IN (" + activityTypes + ") AND " +
                    "INSTRUMENT.A_PRODUCT_TYPE IN (" + productTypes + ") AND " +
                    "CUSTOMER.CUSTOMER_ID = '" + customerID + "' AND " +
                    "( (ACTIVITY.RELEASE2_TIME is null AND (ACTIVITY.RELEASE1_TIME >= '" + dateFrom + "' AND ACTIVITY.RELEASE1_TIME < TO_DATE('" + dateTo + "')+1) ) OR " +
                    "(ACTIVITY.RELEASE2_TIME is not null AND (ACTIVITY.RELEASE2_TIME >= '" + dateFrom + "' AND ACTIVITY.RELEASE2_TIME < TO_DATE('" + dateTo + "')+1)))" +
                    "ORDER BY  CUSTOMER.CUSTOMER_ID, ACTIVITY.A_ACTIVITY_TYPE, INSTRUMENT.INSTRUMENT_ID";
            System.out.println("[INFO]-Executing TradeForm Document Query [" + sqlString+ "]");
            System.out.println("[INFO]- ... This might take a few minutes ...");
            sql.execute(sqlString);
            ResultSet result = sql.getResultSet();
            result.last();
            int rowCount = result.getRow();
            result.beforeFirst();
            logMessage("Number of PDF Documents to Process = [" + String.valueOf(rowCount) + "] for TradeForm Document Query");
            logMessage("COUNT \t" + "CUSTOMER \t" + "ACTIVITY_TYPE \t" + "INSTRUMENT_ID \t" + "SEQ_NO \t" + "RELEASE_DATE \t" + "DOC_ID \t" + "PAGES \t" + "EXTRACTED");
            int counter = 1;
            while (result.next()) {
                String count = String.valueOf(counter);
                String custID = result.getString("CUSTOMER_ID");
                String activityType = result.getString("A_ACTIVITY_TYPE");
                if (activityType.equals("AMD"))
                    activityType = "Amendments";
                else if (activityType.equals("ISS"))
                    activityType = "Issuances";
                else if (activityType.equals("PAY"))
                    activityType = "Payments";
                String instrumentID = result.getString("INSTRUMENT_ID");
                String sequenceNo = result.getString("SEQ_NUM_OF_ACTV_TY");
                if (sequenceNo == null)
                    sequenceNo = "";
                String releaseDate = result.getString("RELEASE2_TIME");
                if (releaseDate == null)
                    releaseDate = result.getString("RELEASE1_TIME");
                String imageDocID = result.getString("IMAGE_DOC_ID");
                String numberOfPages = result.getString("NUM_PAGES");
                String extractSuccessful = null;
                if (extractPDF) {  // Try to pull the PDF up to 5 times before issuing an error on retrieve.
                    boolean pdfExtracted = false;
                    for (int i=0; i<5; i++) {
                        pdfExtracted = image2PDF(custID, activityType, instrumentID, sequenceNo, releaseDate, imageDocID, numberOfPages);
                        extractSuccessful = String.valueOf(pdfExtracted);
                        if (pdfExtracted) {
                            break;
                        } else {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                } else
                    extractSuccessful = "NOT REQUIRED";
                logMessage(count + "\t" +
                        custID + "\t" +
                        activityType + "\t" +
                        instrumentID + "\t" +
                        sequenceNo + "\t" +
                        releaseDate + "\t" +
                        imageDocID + "\t" +
                        numberOfPages + "\t"+
                        extractSuccessful);
                counter ++;
            }
            System.out.println("[INFO]-Completed TradeForm Document Query");
            result.close();
            sql.close();
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Get all scanned documents flagged as send to portal with the portal party that equals the customer id for all doc exams of payments released within the date range
            if (paymentDocExams) {
                sql = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                sqlString = "SELECT DISTINCT CUSTOMER.CUSTOMER_ID, ACTIVITY.A_ACTIVITY_TYPE, INSTRUMENT.INSTRUMENT_ID, ACTIVITY.SEQ_NUM_OF_ACTV_TY, TO_CHAR(ACTIVITY.RELEASE1_TIME+(ORGANIZATION.GMT_DIFF_HOURS/24),'dd-MON-yyyy') AS RELEASE1_TIME, TO_CHAR(ACTIVITY.RELEASE2_TIME+(ORGANIZATION.GMT_DIFF_HOURS/24), 'dd-MON-yyyy') AS RELEASE2_TIME, IMAGE_DOC_INDEX.IMAGE_DOC_ID, IMAGE_DOC_INDEX.NUM_PAGES " +
                        "FROM IMAGE_DOC_INDEX, ACTIVITY, INSTRUMENT, CUSTOMER, PARTY, WORK_ITEM, IMAGE_LINK, ORGANIZATION " +
                        "WHERE ACTIVITY.UOID IN (" +
                        "SELECT DISTINCT ACTIVITY.A_ACTV_EXAM_RESLTN " +
                        "FROM ACTIVITY " +
                        "WHERE ACTIVITY.A_ACTIVITY_TYPE = 'PAY' AND " +
                        "( (ACTIVITY.RELEASE2_TIME is null AND (ACTIVITY.RELEASE1_TIME >= '" + dateFrom + "' AND ACTIVITY.RELEASE1_TIME < TO_DATE('" + dateTo + "')+1) ) OR " +
                        "(ACTIVITY.RELEASE2_TIME is not null AND (ACTIVITY.RELEASE2_TIME >= '" + dateFrom + "' AND ACTIVITY.RELEASE2_TIME < TO_DATE('" + dateTo + "')+1)))) AND " +
                        "WORK_ITEM.A_PORTAL_PARTY_TYPE = PARTY.A_PARTY_TYPE AND " +
                        "PARTY.A_INSTRUMENT = INSTRUMENT.UOID AND " +
                        "PARTY.A_CUSTOMER = CUSTOMER.UOID AND " +
                        "INSTRUMENT.UOID = ACTIVITY.P_INSTRUMENT AND " +
                        "INSTRUMENT.A_PRODUCT_TYPE IN(" + productTypes + ") AND " +
                        "ORGANIZATION.UOID = INSTRUMENT.A_OPER_BK_ORG_ORIG AND " +
                        "WORK_ITEM.UOID = ACTIVITY.A_WORK_ITEM AND " +
                        "WORK_ITEM.UOID = IMAGE_LINK.P_OBJECT AND " +
                        "IMAGE_DOC_INDEX.UOID = IMAGE_LINK.A_IMAGE_INDEX AND " +
                        "IMAGE_DOC_INDEX.VIEW_VIA_PORTAL = 'Y' AND " +
                        "CUSTOMER.CUSTOMER_ID = '" + customerID + "' " +
                        "ORDER BY  CUSTOMER.CUSTOMER_ID, INSTRUMENT.INSTRUMENT_ID";

                System.out.println("[INFO]-Executing Portal Doc Exam Document Query [" + sqlString+ "]");
                System.out.println("[INFO]- ... This might take a few minutes ...");
                sql.execute(sqlString);
                result = sql.getResultSet();
                result.last();
                rowCount = result.getRow();
                result.beforeFirst();
                logMessage("Number of PDF Documents to Process = [" + String.valueOf(rowCount) + "] for Payment Doc Exam Query");
                logMessage("COUNT \t" + "CUSTOMER \t" + "ACTIVITY_TYPE \t" + "INSTRUMENT_ID \t" + "SEQ_NO \t" + "RELEASE_DATE \t" + "DOC_ID \t" + "PAGES \t" + "EXTRACTED");
                counter = 1;
                while (result.next()) {
                    String count = String.valueOf(counter);
                    String custID = result.getString("CUSTOMER_ID");
                    String activityType = result.getString("A_ACTIVITY_TYPE");
                    if (activityType.equals("EXM"))
                        activityType = "Payments";
                    String instrumentID = result.getString("INSTRUMENT_ID");
                    String sequenceNo = result.getString("SEQ_NUM_OF_ACTV_TY");
                    if (sequenceNo == null)
                        sequenceNo = "";
                    String releaseDate = result.getString("RELEASE2_TIME");
                    if (releaseDate == null)
                        releaseDate = result.getString("RELEASE1_TIME");
                    String imageDocID = result.getString("IMAGE_DOC_ID");
                    String numberOfPages = result.getString("NUM_PAGES");
                    String extractSuccessful = null;
                    if (extractPDF) {  // Try to pull the PDF up to 5 times before issuing an error on retrieve.
                        boolean pdfExtracted = false;
                        for (int i=0; i<5; i++) {
                            pdfExtracted = image2PDF(custID, activityType, instrumentID, sequenceNo, releaseDate, imageDocID, numberOfPages);
                            extractSuccessful = String.valueOf(pdfExtracted);
                            if (pdfExtracted) {
                                break;
                            } else {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    } else
                        extractSuccessful = "NOT REQUIRED";
                    logMessage(count + "\t" +
                            custID + "\t" +
                            activityType + "\t" +
                            instrumentID + "\t" +
                            sequenceNo + "\t" +
                            releaseDate + "\t" +
                            imageDocID + "\t" +
                            numberOfPages + "\t"+
                            extractSuccessful);
                    counter ++;
                }
                System.out.println("[INFO]-Completed Portal Doc Exam Document Query");
                System.out.println("[INFO]-Completed PDF Queries for Customer [" + customerID + "]");
                result.close();
                sql.close();
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

    }

    private static boolean image2PDF(String custID, String activityType, String instrumentID, String sequenceNo, String releaseDate, String imageDocID, String numberOfPages) {

        try {
            URL getPDFaspPage = new URL(imageServerURL + imageDocID);
            URLConnection connection = getPDFaspPage.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(true);
            InputStream pdfInputStream = connection.getInputStream();
            int fileLength = connection.getContentLength();
            byte[] bytes = new byte[ (int) fileLength];
            for (int counter=0; counter < fileLength; counter++) {
                bytes[counter] = (byte) pdfInputStream.read();
            }
            String checkForError = String.valueOf(bytes[0]) + String.valueOf(bytes[1]) + String.valueOf(bytes[2]) + String.valueOf(bytes[3]) +
                    String.valueOf(bytes[4]) + String.valueOf(bytes[5]) + String.valueOf(bytes[6]) + String.valueOf(bytes[7]);
            File rootDir = new File(currPdfOutDir + "/" + activityType);
            if(!rootDir.mkdirs() && !rootDir.exists()) {
                System.out.println("[!ERROR!]-Creating Directory [" + currPdfOutDir + "/" + activityType + "] failed. Exiting Application.");
                System.exit(1);
            }
            // need to put back cust id, product into sql; need to fix date, need to add doc exam, need to remove blank sequence numbers, need to log to file.
            if (!sequenceNo.equals("")) {
                sequenceNo = " - " + sequenceNo + " - ";
            } else {
                sequenceNo = " - ";
            }
            String pdfFileName = currPdfOutDir + "/" + activityType + "/" + instrumentID + sequenceNo + releaseDate + " - " + imageDocID + " - " + numberOfPages + ".PDF";
            // if (checkForError.equals("3780687045494651")) {
                FileOutputStream pdfOut = new FileOutputStream(pdfFileName);
                pdfOut.write(bytes);
                pdfOut.flush();
                pdfOut.close();
            // } else
            //     return false;
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            return false;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private static void logMessage(String msg) {

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(logFileName, true));
            System.out.println(msg);
            out.write(msg);
            out.newLine();
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static String toLZ( int i, int len ) {
        // converts integer to left-zero padded string, len  chars long.
        String s = Integer.toString(i);
        if ( s.length() > len ) return s.substring(0,len);
        else if ( s.length() < len ) // pad on left with zeros
            return "000000000000000000000000000".substring(0, len - s.length()) + s;
        else return s;
    }

    private static void  printProgramUsage() {
        System.out.println("################################PARAMETER EXAMPLE#####################################\r\n" +
            "DBUSER=devotlsystestplay353 - oracle DB User ID\r\n" +
            "DBPASSWORD=otl - oracle DB password\r\n" +
            "DBCONN=jdbc:oracle:thin:@PPXDB03:1521:PPXDEV04 - jdbc:oracle:drivertype:@hostname:port:instance/service\r\n" +
            "PDFOUTDIR=C:/PDFOUT - Root folder of the PDF extraction.\r\n" +
            "LOGFILEDIR=C:/PDFOUT - Root folder of the Report Log File. Is equal to PDFOUTDIR if left blank.\r\n" +
            "CUSTOMERID=CPGBCRICKET-ZL/A,CPUSBATS-ZL/A - Filter items by a single Customer ID or a with comma separated list of Customer IDs.\r\n" +
            "DATEFROM=01-JAN-2002 - Filter items by activities released after and including this date. Format dd-MMM-yyyy\r\n" +
            "DATETO=01-JAN-2007 - Filter items by activities released before and including this date. Format dd-MMM-yyyy\r\n" +
            "PRODUCTTYPES=IMP - Filter items by a single instrument product type or with a comma separated list of instrument product types\r\n" +
            "ACTIVITYTYPES=ISS,AMD,PAY - Filter items by a single activity type or with a comma separated list of activity types\r\n" +
            "PAYMENTDOCEXAMS=Y - Run query for Payment Doc Examination images (if left out or set to N, query will not run)\r\n" +
            "NOPDF=N - NOPDF=Y do not run PDF extraction only query and logging. NOPDF=N or missing, PDFs are extracted as well.  Good for testing.\r\n" +
            "################################PARAMETER EXAMPLE#####################################");
    }
}
