/*
 * The Working-Dogs.com License, Version 1.1
 *
 * Copyright (c) 1999 Working-Dogs.com.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:  
 *       "This product includes software developed by the 
 *        Working-Dogs.com <http://www.Working-Dogs.com/>."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "Working-Dogs.com" and "Village" must not be used to 
 *    endorse or promote products derived from this software without 
 *    prior written permission. For written permission, please contact 
 *    jon@working-dogs.com.
 *
 * 5. Products derived from this software may not be called 
 *    "Working-Dogs.com" nor may "Village" appear in their names 
 *    without prior written permission of Working-Dogs.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Working-Dogs.com.  For more
 * information on the Working-Dogs.com, please see
 * <http://www.Working-Dogs.com/>.
 */
/*
 *     Modifications are Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

package com.workingdogs.village;

import java.sql.*;
import java.util.*;

/**
This class is used for doing SQL select statements on the database. It
should not be used for doing modifications via update/delete/insert statements.
If you would like to perform those functions, please use a 
<a href="TableDataSet.html">TableDataSet</a>.
<P>
Here is some example code for using a QueryDataSet.
<PRE>
QueryDataSet qds = new QueryDataSet ( connection, "SELECT * from my_table" );
qds.fetchRecords(10); // fetch the first 10 records
for ( int i = 0; i < qds.size(); i++ )
{
  Record rec = qds.getRecord(i);
  int value = rec.getValue("column").asInt();
  System.out.println ( "The value is: " + value );
}
qds.close();
</PRE>

It is important to always remember to close() a QueryDataSet in order to
free the allocated resources.


@author Jon S. Stevens <A HREF="mailto:jon@working-dogs.com">jon@working-dogs.com</A>
@version $Revision:   1.0  $
*/
public class QueryDataSet extends DataSet
{
    /**
      * Private...does nothing.
      *
      * @exception   SQLException
      * @exception   DataSetException
      */
    public QueryDataSet() throws SQLException, DataSetException
    {
    }

    /**
      * Creates a new QueryDataSet based on a connection and a select string
      *
      * @param   conn
      * @param   selectStmt
      * @exception   SQLException
      * @exception   DataSetException
      */
    public QueryDataSet(Connection conn, String selectStmt) 
        throws SQLException, DataSetException
    {
        this.conn = conn;

        selectString = new StringBuffer(selectStmt);
        stmt = conn.createStatement();
        resultSet = stmt.executeQuery(selectStmt);
        schema = new Schema();
        schema.populate( resultSet.getMetaData(), null );
    }

    /**
      * Create a new QueryDataSet based on an existing resultSet
      *
      * @param   resultSet
      * @exception   SQLException
      * @exception   DataSetException
      */
    public QueryDataSet(ResultSet resultSet) throws SQLException,
    DataSetException
    {
        this.resultSet = resultSet;
        schema = new Schema();
        schema.populate (resultSet.getMetaData(),null);
    }

    /**
      * get the Select String that was used to create this QueryDataSet
      *
      * @return     a select string
      */
    public String getSelectString ()
    {
        return this.selectString.toString();
    }
}
