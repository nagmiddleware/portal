/*
 * The Working-Dogs.com License, Version 1.1
 *
 * Copyright (c) 1999 Working-Dogs.com.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:  
 *       "This product includes software developed by the 
 *        Working-Dogs.com <http://www.Working-Dogs.com/>."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "Working-Dogs.com" and "Village" must not be used to 
 *    endorse or promote products derived from this software without 
 *    prior written permission. For written permission, please contact 
 *    jon@working-dogs.com.
 *
 * 5. Products derived from this software may not be called 
 *    "Working-Dogs.com" nor may "Village" appear in their names 
 *    without prior written permission of Working-Dogs.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Working-Dogs.com.  For more
 * information on the Working-Dogs.com, please see
 * <http://www.Working-Dogs.com/>.
 */
/*
 *     Modifications are Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.workingdogs.village;

import java.sql.*;

/**
  This class is used for testing the functionality of this product. While creating
  this code, I have closed many potential bugs, but I'm sure that others still exist.
  Thus, if you find a bug in Village, please add to this test suite so that the bug
  will be sure to be fixed in future versions.
  <P>
  In order to do the testing, you will need to be able to connect via JDBC to
  your database. Since I use MySQL <A HREF="http://www.mysql.com/">http://www.mysql.com/</A>
  , this testing suite is best for that database. I also use the
  mm MySQL drivers <A HREF="http://www.worldserver.com/mm.mysql/">http://www.worldserver.com/mm.mysql/</A>
  because it is the best driver that I have found for MySQL.
  <P>
  Note that Village should work with <strong>any</strong> JDBC compliant driver.
  <P>
  Here is the schema that this test expects ( you should be able to copy and paste it into
  your MySQL database that you want to use ):

  <pre>
  CREATE TABLE test
  (
    a TINYINT null,
    b SMALLINT null,
    c MEDIUMINT null,
    d INT null,
    e INTEGER null,
    f BIGINT null,
    g REAL null,
    h DOUBLE null,
    i FLOAT null,
    j DECIMAL(8,1) null,
    k NUMERIC(8,1) null,
    l CHAR(255) null,
    m VARCHAR(255) null,
    n DATE null,
    o TIME null,
    p TIMESTAMP null,
    q DATETIME null,
    r TINYBLOB null,
    s BLOB null,
    t MEDIUMBLOB null,
    u LONGBLOB null,
    v TINYTEXT null,
    w TEXT null,
    x MEDIUMTEXT null
  );
  </pre>

@author Jon S. Stevens <A HREF="mailto:jon@working-dogs.com">jon@working-dogs.com</A>
@version $Revision:   1.0  $
*/

public class TestMySQL
{
    /** The database connection */
    static Connection conn;

    /** This is the name of the database. Created with mysqladmin create */
    private static String DB_NAME = "village";
    /** This is the name of the table in the DB_NAME */
    private static String DB_TABLE = "test";

    /** This is the name of the machine that is hosting the MySQL server */
    private static String DB_HOST = "localhost";
    /** This is the user to log into the database as. For this test, the user
    must have insert/update/delete access to the database. */
    private static String DB_USER = "";
    /** the password for the user */
    private static String DB_PASS = "";


    /** mm MySQL Driver setup */
    private static String DB_DRIVER = "org.gjt.mm.mysql.Driver";
    /** mm MySQL Driver setup */
    private static String DB_CONNECTION =
            "jdbc:mysql://" + DB_HOST + "/" + DB_NAME + "?user=" +
            DB_USER + "&password=" + DB_PASS;

    /** used for debugging */
    private static boolean debugging = true;
    /** used for debugging */
    private static int num = 1;
    /** used for debugging */
    private static int testCount = 1;
    /** used for debugging */
    private static int TDS = 1;
    /** used for debugging */
    private static int QDS = 2;
    /** used for debugging */
    private static int PASSED = 1;
    /** used for debugging */
    private static int FAILED = 2;

    public static void main (String argv[])
    {
        if (argv.length > 0 && argv.length < 5)
        {
            System.out.println(
                "Format: TestMySQL <DB_NAME> <DB_TABLE> <DB_HOST> <DB_USER> <DB_PASS>" );
            return;
        }
        else if ( argv.length == 5 )
        {
            DB_NAME  = argv[0];
            DB_TABLE = argv[1];
            DB_HOST  = argv[2];
            DB_USER  = argv[3];
            DB_PASS  = argv[4];
            DB_CONNECTION = 
             "jdbc:mysql://" + DB_HOST + "/" + DB_NAME + "?user=" +
                     DB_USER + "&password=" + DB_PASS;
        }

        getConnection();
//      testDeleteSomeRecords();
//      testTableDataSet();
//      testQueryDataSet();
//      testTableDataSet2();
//      testTableDataSet3();
//      testTableDataSet4();
//      testRemoveRecord();
    }

    /**
        This test verifies that deleting multiple records actually 
        works. after execution, there should be no more records in the 
        database.
    */
    public static void testDeleteSomeRecords()
    {
        try
        {
            KeyDef kd = new KeyDef().addAttrib("e");
            TableDataSet tds = new TableDataSet ( conn, DB_TABLE, kd );
            tds.where("e > 100");
            
            // add some records
            Record newRec = tds.addRecord();
            newRec.setValue ("e", "200");
            Record newRec2 = tds.addRecord();
            newRec2.setValue ("e", "300");
            tds.save();
            
            // get those records
            tds.fetchRecords();
            for (int i = 0; i < tds.size(); i++) 
            {
                Record rec = tds.getRecord(i);
                // delete those records
                rec.markToBeDeleted();
                System.out.println ( "here " + i + ": " + rec.toString() );
            }
            tds.save();
            tds.close();
        }
        catch (Exception e)
        {
            debug (TDS, e);
        }        
    }
    
    /**
        This test will throw a DataSetException. 
        The first getRecord will succeed and the second
        one will fail.
    */
    public static void testRemoveRecord()
    {
        try
        {
            TableDataSet tds = new TableDataSet ( conn, DB_TABLE );
            tds.addRecord();
            Record rec = tds.getRecord(0);
            tds.removeRecord(rec);
            Record foo = tds.getRecord(0);
            tds.close();
        }
        catch (Exception e)
        {
            debug (TDS, e);
        }
    }

    public static void testTableDataSet2()
    {
        try
        {
            TableDataSet tds = new TableDataSet ( conn, DB_TABLE );
            Record rec = tds.addRecord();
            rec.setValue ( "b", 2 );
            tds.save();
            tds.close();
        }
        catch (Exception e)
        {
            debug (TDS, e);
        }
    }

    public static void testTableDataSet3()
    {
        try
        {
            TableDataSet tds = new TableDataSet ( conn, DB_TABLE );
            Record rec = tds.addRecord();
            rec.setValue ( "b", 2 );
            rec.save();
            tds.close();
        }
        catch (Exception e)
        {
            debug (TDS, e);
        }
    }

    public static void testTableDataSet4()
    {
        try
        {
            KeyDef kd = new KeyDef().addAttrib("b");
            TableDataSet tds = new TableDataSet ( conn, DB_TABLE, kd );
            Record rec = tds.addRecord();
            rec.setValueNull ( "b" );
            System.out.println(rec.getSaveString());
            rec.save();
            rec.markToBeDeleted();
            System.out.println(rec.getSaveString());
            rec.save();
            tds.close();
        }
        catch (Exception e)
        {
            debug (TDS, e);
        }
    }

    public static void testTableDataSet()
    {
        try
        {
            KeyDef kd = new KeyDef().addAttrib("a");
            TableDataSet tds = new TableDataSet (conn, DB_TABLE, kd);
            tds.order ("a");
            tds.fetchRecords();
            int size = tds.size();

            debug (TDS, "size of fetchRecords", size);
            debug (TDS, "getSelectString()", tds.getSelectString());
            test (TDS, tds.getSelectString(), "SELECT * FROM test ORDER BY a");

            // add a new record
            Record addRec = tds.addRecord();
            addRec.setValue ("a", 1);
            addRec.setValue ("b", 2);
            addRec.setValue ("c", 2343);
            addRec.setValue ("d", 33333);
            addRec.setValue ("e", 22222);
            addRec.setValue ("f", 234324);
            addRec.setValue ("g", 3434);
            addRec.setValue ("h", 2343.30);
            addRec.setValue ("i", 2343.22);
            addRec.setValue ("j", 333.3);
            addRec.setValue ("k", 333.3);
            addRec.setValue ("l", "lskdfsd");
            addRec.setValue ("m", "lksdflkjsldf");
            addRec.setValue ("n", new java.util.Date());
            addRec.setValue ("o", new java.util.Date());
            addRec.setValue ("p", new java.util.Date());
            addRec.setValue ("q", new java.util.Date());
            addRec.setValue ("r", "lksdflkjsldf");
            addRec.setValue ("s", "lksdflkjsldf");
            addRec.setValue ("t", "lksdflkjsldf");
            addRec.setValue ("u", "lksdflkjsldf");
            addRec.setValue ("v", "lksdflkjsldf");
            addRec.setValue ("w", "lksdflkjsldf");
            addRec.setValue ("x", "lksdflkjsldf");

            debug (TDS, "getSaveString() for insert",
                    addRec.getSaveString());
            test (TDS, addRec.getSaveString(), "INSERT INTO test ( a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");

            // save it (causing an INSERT to happen)
            addRec.save();

            debug (TDS, "size of TDS after save()", tds.size());
            test (TDS, size + 1, tds.size());

            Record updateRec = tds.getRecord(0);
            updateRec.setValue ("b", 234);
            updateRec.setValue ("c", 4);
            updateRec.setValue ("d", 4);
            updateRec.setValue ("e", 5);
            updateRec.setValue ("f", 6);
            updateRec.setValue ("g", 3);
            updateRec.setValue ("h", 3.4);
            updateRec.setValue ("i", 33.44);
            updateRec.setValue ("j", 33.55);
            updateRec.setValue ("k", 3333.7);
            updateRec.setValue ("l", "qweqwe");
            updateRec.setValue ("m", "qweqwe");
            updateRec.setValue ("n", new java.util.Date());
            updateRec.setValue ("o", new java.util.Date());
            updateRec.setValue ("p", new java.util.Date());
            updateRec.setValue ("q", new java.util.Date());
            updateRec.setValue ("r", "qweqwe");
            updateRec.setValue ("s", "qweqwe");
            updateRec.setValue ("t", "qweqwe");
            updateRec.setValue ("u", "qweqwe");
            updateRec.setValue ("v", "qweqwe");
            updateRec.setValue ("w", "qweqwe");
            updateRec.setValue ("x", "qweqwe");

            debug (TDS, "updateRec.getRefreshQueryString()",
                    updateRec.getRefreshQueryString());

            debug (TDS, "updateRec.getSaveString() for update",
                    updateRec.getSaveString());
            test (TDS, updateRec.getSaveString(), "UPDATE test SET b = ?, c = ?, d = ?, e = ?, f = ?, g = ?, h = ?, i = ?, j = ?, k = ?, l = ?, m = ?, n = ?, o = ?, p = ?, q = ?, r = ?, s = ?, t = ?, u = ?, v = ?, w = ?, x = ? WHERE a = ?");

            updateRec.save();

            // mark it for deletion
            addRec.markToBeDeleted();

            debug (TDS, "addRec.getSaveString() for delete",
                    addRec.getSaveString());
            test (TDS, addRec.getSaveString(), "DELETE FROM test WHERE a = ?");

            // save it (causing a DELETE to happen and also remove the records from the TDS)
            addRec.save();
            test (TDS, tds.size(), 0);

            tds.close();

            // Start a new TableDataSet, this is to test the Record.refresh() method
            tds = new TableDataSet (conn, DB_TABLE, kd);
            tds.fetchRecords();
            addRec = tds.addRecord();
            addRec.setValue ("a", 1);
            addRec.save();

            tds = new TableDataSet (conn, DB_TABLE, kd);
            tds.fetchRecords();
            Record getRec = tds.getRecord(0);

            debug (TDS, "getRec.asString() 1a:",
                    getRec.getValue("a").asString());
            test (TDS, getRec.getValue("a").asString(), "1");
            debug (TDS, "getRec.asString() 1b:",
                    getRec.getValue("b").asString());
            test (TDS, getRec.getValue("b").asString(), "0");

            getRec.setValue ("b", 5);

            debug (TDS, "getRec.asString() 2b:",
                    getRec.getValue("b").asString());
            test (TDS, getRec.getValue("b").asString(), "5");

            getRec.refresh(conn);

            debug (TDS, "getRec.asString() 3b:",
                    getRec.getValue("b").asString());
            test (TDS, getRec.getValue("b").asString(), "0");
            debug (TDS, "getRec.asString() 2a:",
                    getRec.getValue("a").asString());
            test (TDS, getRec.getValue("a").asString(), "1");

            getRec.markToBeDeleted();
            getRec.save();

            System.out.println ( tds.toString() );
            System.out.println ( getRec.toString() );
            System.out.println ( tds.schema().toString() );

            tds.close();
        }
        catch (Exception e)
        {
            debug (TDS, e);
        }
    }


    public static void testQueryDataSet()
    {
        try
        {
            KeyDef kd = new KeyDef().addAttrib("a");
            TableDataSet tds = new TableDataSet (conn, DB_TABLE, kd);
            tds.fetchRecords();

            // add a new record
            Record addRec = tds.addRecord();
            addRec.setValue ("a", 1);
            addRec.setValue ("b", 2);
            debug ( TDS, "addRec.getSaveString()", addRec.getSaveString() );
            test ( TDS, addRec.getSaveString(), "INSERT INTO test ( a, b ) VALUES ( ?, ? )" );
            // save it (causing an INSERT to happen)
            addRec.save();
            tds.close();

            // get a QDS
            QueryDataSet qds = new QueryDataSet ( conn, 
                "SELECT * FROM " + DB_TABLE );
            qds.fetchRecords();

            debug ( QDS, "qds.getSelectString()", qds.getSelectString() );
            test ( QDS, qds.getSelectString(), "SELECT * FROM test" );

            debug ( QDS, "qds.size()", qds.size() ); // should be 1
            Record rec = qds.getRecord (0);
            debug ( QDS, "rec.size()", rec.size() ); // should be 24
            
            debug ( QDS, "rec.getValue(\"a\").asString()", rec.getValue("a").asString() );
            debug ( QDS, "rec.getValue(\"b\").asString()", rec.getValue("b").asString() );
            debug ( QDS, "rec.getValue(\"c\").asString()", rec.getValue("c").asString() );
            debug ( QDS, "rec.getValue(\"d\").asString()", rec.getValue("d").asString() );

            // this tests to make sure that "d" was assigned properly
            // there was a bug where wasNull() was being checked and this wasn't
            // being setup correctly.
            test ( QDS, rec.getValue("d").asString(), "0" );
            qds.close();

            // delete the record
            kd = new KeyDef().addAttrib("a");
            tds = new TableDataSet (conn, DB_TABLE, kd);
            tds.fetchRecords();
            Record getRec = tds.getRecord(0);
            getRec.markToBeDeleted();
            getRec.save();
            tds.close();
            
        }
        catch (Exception e)
        {
            debug (TDS, e);
        }
    }

    public static void getConnection()
    {
        try
        {
            Class.forName(DB_DRIVER);
            conn = DriverManager.getConnection(DB_CONNECTION);
        }
        catch (Exception e)
        {
            System.out.println("\n\nConnection failed : " + e.getMessage());
        }
    }

    public static void debug (int type, Exception e)
    {
        debug (TDS, e.getMessage());
        e.printStackTrace();
        System.out.println ("\n");
    }
    public static void debug(int type, String method)
    {
        debug (type, method, null);
    }

    public static void debug(int type, String method, int value)
    {
        debug (type, method, new String().valueOf(value));
    }

    public static void test (int type, int test, int value)
    {
        if (debugging)
        {
            String name = "";
            if (type == TDS)
                name = "TableDataSet";
            else
                name = "QueryDataSet";

            String val = "";
            if (test == value)
                val = "Passed";
            else
                val = "Failed";

            System.out.print ("[" + num++ + "] Test " + testCount++ +
                    " - " + val + "!\n");

            System.out.flush();
        }
    }

    public static void test (int type, String test, String value)
    {
        if (debugging)
        {
            String name = "";
            if (type == TDS)
                name = "TableDataSet";
            else
                name = "QueryDataSet";

            String val = "";
            if (test.equals (value))
                val = "Passed";
            else
                val = "Failed";

            System.out.print ("[" + num++ + "] Test " + testCount++ +
                    " - " + val + "!\n");

            System.out.flush();
        }
    }

    public static void debug(int type, String method, String value)
    {
        if (debugging)
        {
            String name = "";
            if (type == TDS)
                name = "TableDataSet";
            else
                name = "QueryDataSet";

            if (value != null)
                System.out.print ("[" + num++ + "] " + name + " - " +
                        method + " = " + value + "\n");
            else
                System.out.print ("[" + num++ + "] " + name + " - " +
                        method + "\n");

            System.out.flush();
        }
    }
}
