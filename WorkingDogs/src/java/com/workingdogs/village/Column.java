/*
 * The Working-Dogs.com License, Version 1.1
 *
 * Copyright (c) 1999 Working-Dogs.com.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:  
 *       "This product includes software developed by the 
 *        Working-Dogs.com <http://www.Working-Dogs.com/>."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "Working-Dogs.com" and "Village" must not be used to 
 *    endorse or promote products derived from this software without 
 *    prior written permission. For written permission, please contact 
 *    jon@working-dogs.com.
 *
 * 5. Products derived from this software may not be called 
 *    "Working-Dogs.com" nor may "Village" appear in their names 
 *    without prior written permission of Working-Dogs.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Working-Dogs.com.  For more
 * information on the Working-Dogs.com, please see
 * <http://www.Working-Dogs.com/>.
 */
/*
 *     Modifications are Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.workingdogs.village;

import java.sql.*;

/**
This class represents a Column in the database and its associated meta information. 
A <a href="Record.html">Record</A> is a collection of columns.

@author Jon S. Stevens <A HREF="mailto:jon@working-dogs.com">jon@working-dogs.com</A>
@version $Revision:   1.0.1.0  $
*/
public class Column
{
    /** column number in a schema object */
    private int columnNumber = -1;
    /** name of the column */
    private String name = "";
    /** example: this column is of type "String" */
    private String columnTypeName = "";
    /** what java.sql.Type is this column? */
    private int columnType = -1;
    /** name of table that this column belongs to */
    private String tableName = "";
    /** is null allowed for this column? */
    private boolean nullAllowed = false;
    /** is this an auto increment column? */
    private boolean autoIncrement = false;
    /** is this a read only column? */
    private boolean readOnly = false;
    /** is this a searchable column? */
    private boolean searchable = false;
    /** what is the scale of this column? */
    private int scale = -1;
    /** what is the precision of this column? */
    private int precision = -1;
    /** what is the length of this column? */
    private int length = -1;
    /** the column type resolved internally */
    private String type = "";
    private boolean isNCHAR = false;

    /** constructor */
    public Column()
    {
        this.columnNumber = -1;
        this.name = "";
        this.columnTypeName = "";
        this.tableName = "";
        this.columnType = -1;
        this.nullAllowed = false;
        this.autoIncrement = false;
        this.readOnly = false;
        this.searchable = false;
        this.scale = -1;
        this.precision = -1;
        this.length = -1;
        this.type = "";
    }

    /** internal package method for populating a Column instance */
    void populate (ResultSetMetaData rsmd, int colNum, String tableName) 
        throws SQLException
    {
        this.columnNumber = colNum;
        this.name = rsmd.getColumnName (columnNumber);

        // Workaround for Sybase jConnect 5.2 and older.
        try
        {
            this.tableName = rsmd.getTableName(columnNumber);

            // ResultSetMetaData may report table name as the empty
            // string when a database-specific function has been
            // called to generate a Column.
            if (this.tableName == null || this.tableName.equals(""))
            {
                if (tableName != null)
                    this.tableName = tableName;
                else
                    this.tableName = "";    
            }
        }
        catch (Exception e)
        {
            if (tableName != null)
            {
                this.tableName = tableName;
            }
            else
            {
                this.tableName = "";
            }
        }

        this.columnTypeName = rsmd.getColumnTypeName (columnNumber);

        this.columnType = rsmd.getColumnType (columnNumber);
        this.nullAllowed = rsmd.isNullable(columnNumber) == 1;
        this.autoIncrement = rsmd.isAutoIncrement(columnNumber);
        this.readOnly = rsmd.isReadOnly (columnNumber);
        this.searchable = rsmd.isSearchable (columnNumber);
        this.scale = rsmd.getScale (columnNumber);

        if(!type().equals("CLOB") && !type().equals("BLOB"))
            precision = rsmd.getPrecision(columnNumber);
         
        this.length = rsmd.getColumnDisplaySize (columnNumber);
        //CR-486            
        // Call the hidden method isNCHAR(int stmtNumber) on the underlying oracle.jdbc.driver.OraclePreparedStatement.
        // This is specific to Oracle.  Ignore errors if it fails (for example, it's on different DBMS)
        try { 
            java.lang.reflect.Method _isNCHAR = rsmd.getClass().getMethod("isNCHAR", new Class[] {int.class});
            isNCHAR = ((Boolean)_isNCHAR.invoke(rsmd, new Object[]{new Integer(columnNumber)})).booleanValue();
        }
        catch (Exception e) {
           // e.printStackTrace();
        	
        }
        
    }

    /**
       the name of the column
       @retuns the name of the column
     */
    public String name()
    {
        return this.name;
    }
    /**
       the data type of a column
       @returns the java.sql.Types String
     */
    public String dbType()
    {
        return this.columnTypeName;
    }
    
    /**
     * Whether the column is NCHAR datatype (NCHAR, NVARCHAR2, NCLOB).
     * @return
     */
    public boolean isNCHAR() 
    {
        return this.isNCHAR;
    }
    /**
       the data type of a column
       @returns the java.sql.Types enum
     */
    public int typeEnum()
    {
        return this.columnType;
    }
    /**
       does this column allow null?
       @returns whether or not the column has null Allowed
     */
    public boolean nullAllowed()
    {
        return this.nullAllowed;
    }
    /**
       does this column auto increment?
       @returns whether or not this column auto increments
     */
    public boolean autoIncrement()
    {
        return this.autoIncrement;
    }
    /**
       is this column read only?
       @returns whether or not this column is read only
     */
    public boolean readOnly()
    {
        return this.readOnly;
    }
    /**
       is this column searchable?
       @returns true if this column is searchable
     */
    public boolean searchable()
    {
        return this.searchable;
    }
    /**
       the scale of the column
       @returns the scale of the column
     */
    public int scale()
    {
        return this.scale;
    }
    /**
       the precision of the column
       @returns the precision of the column
     */
    public int precision()
    {
        return this.precision;
    }
    /**
       the storage length of a column
       @returns the storage length of a column
     */
    public int length()
    {
        return this.length;
    }
    /**
       the type of the column as a string
       @returns the type of the column as a string
     */
    public String type()
    {
        if (isBoolean())
            return "BOOLEAN";
        else if (isByte())
            return "BYTE";
        else if (isShort())
            return "SHORT";
        else if (isInt())
            return "INTEGER";
        else if (isLong())
            return "LONG";
        else if (isFloat())
            return "FLOAT";
        else if (isDouble())
            return "DOUBLE";
        else if (isBigDecimal())
            return "BIGDECIMAL";
        else if (isDate() || isTime() || isTimestamp())
            // Treat all date types as timestamps
            return "TIMESTAMP";
        else if (isString())
            return "STRING";
        else if (isBinary())
            return "BINARY";
        else if (isVarBinary())
            return "VARBINARY";
        else if (isLongVarBinary())
            return "LONGVARBINARY";
//Begin Clob Enhancemnt
        else if (isClob())
            return "CLOB";
//End Clob Enhancemnt

//Begin Blob Enhancemnt
        else if (isBlob())
            return "BLOB";
//End Blob Enhancemnt


        return "UNKNOWN TYPE: " + typeEnum();
    }

    /** column isBoolean: -7 */
    public boolean isBoolean()
    {
        if (this.typeEnum() == -7) 
            return true;
        else
            return false;
    }
    /** column isBigDecimal: 2 || 3 */
    public boolean isBigDecimal()
    {
        if (this.typeEnum() == 2 || this.typeEnum() == 3)
            return true;
        else
            return false;
    }
    /** column isBinary: -2 */
    public boolean isBinary()
    {
        if (this.typeEnum() == -2)
            return true;
        else
            return false;
    }
    /** column isByte: -6 */
    public boolean isByte()
    {
        if (this.typeEnum() == -6)
            return true;
        else
            return false;
    }
    /** column isBytes: -4 || -3 || -2 */
    public boolean isBytes()
    {
        if (this.typeEnum() == -4 || this.typeEnum() == -3 ||
                this.columnType == -2)
            return true;
        else
            return false;
    }
    /** column isBytes: 91 */
    public boolean isDate()
    {
        if (this.typeEnum() == 91)
            return true;
        else
            return false;
    }
    /** column isDouble: 6 || 8 */
    public boolean isDouble()
    {
        if (this.typeEnum() == 6 || this.typeEnum() == 8)
            return true;
        else
            return false;
    }
    /** column isFloat: 7 */
    public boolean isFloat()
    {
        if (this.typeEnum() == 7)
            return true;
        else
            return false;
    }
    /** column isInt: 4 */
    public boolean isInt()
    {
        if (this.typeEnum() == 4)
            return true;
        else
            return false;
    }
    /** column isLong: -5 */
    public boolean isLong()
    {
        if (this.typeEnum() == -5)
            return true;
        else
            return false;
    }
    /** column isShort: 5 */
    public boolean isShort()
    {
        if (this.typeEnum() == 5)
            return true;
        else
            return false;
    }
    /** column isString: -1 || -11 || 12 */
    public boolean isString()
    {
        if (this.typeEnum() == -1 || this.typeEnum() == 11 ||
                this.typeEnum() == 12)
            return true;
        else
            return false;
    }
    /** column isTime: 92 */
    public boolean isTime()
    {
        if (this.typeEnum() == 92)
            return true;
        else
            return false;
    }
    /** column isTimestamp: 93 */
    public boolean isTimestamp()
    {
        if (this.typeEnum() == 93)
            return true;
        else
            return false;
    }
    /** column isVarBinary: -3 */
    public boolean isVarBinary()
    {
        if (this.typeEnum() == -3)
            return true;
        else
            return false;
    }
    /** column isLongVarBinary: -4 */
    public boolean isLongVarBinary()
    {
        if (this.typeEnum() == -4)
            return true;
        else
            return false;
    }

//Begin Clob Enhancemnt
    /** column isClob: 2005 */
    public boolean isClob()
    {
        if (this.typeEnum() == 2005)
            return true;
        else
            return false;
    }
//End Clob Enhancemnt

//Begin Blob Enhancemnt
    /** column isBlob:  */
    public boolean isBlob()
    {
        if (this.typeEnum() == java.sql.Types.BLOB)
            return true;
        else
            return false;
    }
//End Blob Enhancemnt

    /** unknown use */
    public String dbKonaMethod() throws DataSetException
    {
        throw new DataSetException ("Method not implemented: Unknown use!");
    }
    /** unknown use */
    public String javaType() throws DataSetException
    {
        throw new DataSetException ("Method not implemented: Unknown use!");
    }
    /** unknown use */
    public final String preparedStatemntBindMethod() throws DataSetException
    {
        throw new DataSetException ("Method not implemented: Unknown use!");
    }
    /** unknown use */
    public final String resultSetMethod() throws DataSetException
    {
        throw new DataSetException ("Method not implemented: Unknown use!");
    }
    
    public String getTableName()
    {
        return tableName;
    }
}
