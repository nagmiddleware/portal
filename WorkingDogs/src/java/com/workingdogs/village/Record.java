/*
 * The Working-Dogs.com License, Version 1.1
 *
 * Copyright (c) 1999 Working-Dogs.com.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:  
 *       "This product includes software developed by the 
 *        Working-Dogs.com <http://www.Working-Dogs.com/>."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "Working-Dogs.com" and "Village" must not be used to 
 *    endorse or promote products derived from this software without 
 *    prior written permission. For written permission, please contact 
 *    jon@working-dogs.com.
 *
 * 5. Products derived from this software may not be called 
 *    "Working-Dogs.com" nor may "Village" appear in their names 
 *    without prior written permission of Working-Dogs.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Working-Dogs.com.  For more
 * information on the Working-Dogs.com, please see
 * <http://www.Working-Dogs.com/>.
 */
/*
 *     Modifications are Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.workingdogs.village;

import java.sql.*;
import java.math.*;
import java.util.*;
import java.io.*;

/**
A Record represents a row in the database. It contains a collection of 
<a href="Value.html">Values</A> which are the individual contents of each column in the row.

@author Jon S. Stevens <A HREF="mailto:jon@working-dogs.com">jon@working-dogs.com</A>
@version $Revision:   1.1.1.0.1.0  $
*/
public class Record
{
    /** an array of Value objects, this is 1 based*/
    private Value values[];
    /** a 1 To 1 relationship between Values and whether they are clean or not */
    private boolean isClean[];
    /** the parent DataSet for this Record */
    private DataSet parentDataSet;
    /** number of columns in this Record */
    private int numberOfColumns;
    /** this is the state of this record */
    private int saveType = 0;
    /** a saved copy of the schema for this Record */
    private Schema schema;
    /** the status of a record */
    private boolean status;
//Begin Enhancement    
    /** whether or not the record needs to take special action because of a clob */
    private boolean needsClobAction;
//End Clob Enhancement

//Begin Enhancement    
    /** whether or not the record needs to take special action because of a blob */
    private boolean needsBlobAction;
//End Clob Enhancement

    /**
      This isn't used and doesn't do anything.
    */
    public Record ()
    {
        // don't do anything
    }

    /**
       Creates a new Record and sets the parent dataset to the passed in value.
       This method also creates the Value objects which are associated with this 
       Record.
     */
    public Record(DataSet ds) throws DataSetException, SQLException
    {
        setParentDataSet (ds);
        initializeRecord();
        createValues(dataset().resultSet());
    }

    /**
        This is a special case method for Record. This case is really only 
        used when DataSet.addRecord() is called because we may not have an
        existing ResultSet so there will not be any values in the Value 
        objects that are created. Passing null to createValues forces 
        the Value object to be created, but no processing to be done within 
        the Value object constructor. 
        <P>
        This method is a package method only because it is really not useful
        outside of the package.
        
        @param ds the dataset
        @param addRecord whether or not this method is being called from DataSet.addRecord()
    */
    Record(DataSet ds, boolean addRecord) throws DataSetException, SQLException
    {
        setParentDataSet(ds);
        initializeRecord();
        createValues(null);
    }
    
    /**
       Performs initialization for this Record.
     */
    private void initializeRecord() throws DataSetException
    {
        this.schema = dataset().schema();
        this.numberOfColumns = schema.numberOfColumns();
        this.values = new Value[size() + 1];
        this.isClean = new boolean[size() + 1];
        this.needsClobAction = false;
        this.needsBlobAction = false;
        
        setSaveType(Enums.UNKNOWN);

        for (int i = 1; i <= size(); i++)
        {
            markValueClean(i);
            this.values[i] = null;
        }
    }
    /**
      *
      * Creates the value objects for this Record. It is 1 based
      *
      * @exception   DataSetException
      * @exception   SQLException
      */
    private void createValues(ResultSet rs) throws DataSetException,
    SQLException
    {
        for (int i = 1; i <= size(); i++)
        {
            Value val = new Value (rs, i, schema().column(i).typeEnum(), schema().column(i).isNCHAR()); //CR-486 add isNCHAR().
            this.values[i] = val;
        }
    }

    /**
       Saves the data in this Record to the database. Uses the parent dataset's connection.
       @return 1 if the save completed. 0 otherwise.
     */
    public int save() throws DataSetException, SQLException
    {
        return save (dataset().connection());
    }

    /**
       Saves the data in this Record to the database. Uses the connection passed into it.
       @return 1 if the save completed. 0 otherwise.
     */
    public int save (Connection connection) throws DataSetException,
    SQLException
    {
        int returnValue = 0;

        if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You cannot save a QueryDataSet. Please use a TableDataSet instead.");

        if (! needsToBeSaved())
            return returnValue;

        if (toBeSavedWithInsert())
            returnValue = saveWithInsert (connection);
        else if (toBeSavedWithUpdate())
            returnValue = saveWithUpdate (connection);
        else if (toBeSavedWithDelete())
            returnValue = saveWithDelete (connection);

        return returnValue;
    }

    /**
       Saves the data in this Record to the database with an DELETE statement
       @return SQL DELETE statement
     */
    private int saveWithDelete (Connection connection)
            throws DataSetException, SQLException
    {
        PreparedStatement stmt = null;
        try
        {
            stmt = connection.prepareStatement (getSaveString());
            int ps = 1;
            for (int i = 1; i <= dataset().keydef().size(); i++)
            {
                Value val = getValue (dataset().keydef().getAttrib(i));

                val.setPreparedStatementValue (stmt, ps++);
            }

            int ret = stmt.executeUpdate();

            // note that the actual deletion of the Record objects 
            // from the TDS is now in the save() method of the TDS 
            // instead of here. This fixes a bug where multiple 
            // records would not be deleted properly because they 
            // were being removed from here and the Records Vector 
            // was getting out of sync with reality. So, just 
            // mark them as needing to be removed here.
            setSaveType (Enums.ZOMBIE);

            if (ret > 1)
                throw new SQLException ("There were " + ret + " rows deleted with this records key value.");

            return ret;
        }
        catch (SQLException e1)
        {
            throw e1;
        }
        finally { try
            {
                if (stmt != null)
                    stmt.close();
            }
            catch (SQLException e2)
            {
                throw e2;
            }
        } }

    /**
       Saves the data in this Record to the database with an UPDATE statement
       @return SQL UPDATE statement
     */
    private int saveWithUpdate (Connection connection)
            throws DataSetException, SQLException
    {
        PreparedStatement stmt = null;
        try
        {
            String saveString = getSaveString();
            stmt = connection.prepareStatement (saveString);
            int ps = 1;
            int ret = 0;
            for (int i = 1; i <= size(); i++)
            {
                Value val = getValue (i);
                if (! valueIsClean(i) &&
                        ! schema().column(i).readOnly() &&
                            ! schema().column(i).isClob() &&
                            ! schema().column(i).isBlob())
                {
                    val.setPreparedStatementValue (stmt, ps++);
                }
            }
            
            for (int i = 1; i <= dataset().keydef().size(); i++)
            {
                Value val = getValue (dataset().keydef().getAttrib(i));
                val.setPreparedStatementValue (stmt, ps++);
            }
            ret = stmt.executeUpdate();
            
            if (this.needsClobAction) {
                this.saveClobValues(connection);
            }
            
            if (this.needsBlobAction) {
                this.saveBlobValues(connection);
            }
            

            if (((TableDataSet) dataset()).refreshOnSave())
            {
                refresh (dataset().connection());
            }
            else
            {
                // Marks all of the values clean since they have now been saved
                markRecordClean();
            }

            setSaveType (Enums.AFTERUPDATE);

            if (ret > 1)
                throw new SQLException ("There were " + ret + " rows updated with this records key value.");

            return ret;
        }
        catch (SQLException e1)
        {
            throw e1;
        }
        finally { try
            {
                if (stmt != null)
                    stmt.close();
            }
            catch (SQLException e2)
            {
                throw e2;
            }
        } }

    private void saveClobValues (Connection connection)
    {
      try(PreparedStatement stmt = connection.prepareStatement (getSelectClobColumnsString())) {
       
        
        int ps = 1;
        for (int i = 1; i <= dataset().keydef().size(); i++)
        {
            Value val = getValue (dataset().keydef().getAttrib(i));
            if (val.isNull()) {
                throw new DataSetException ("You cannot execute an update with a null value for a KeyDef.");
            }
            val.setPreparedStatementValue (stmt, ps++);
        }

        ResultSet rs = stmt.executeQuery();
        //IR-YAUI010956164 Krishna
        // rs.next(); is commented out and the same is wrapped into if condition
        //rs.next();
        //In some cases the ResultSet may not have any records.The code rs.next(); moves the cursor from 
        //initial position to a position where there are no records.So,the function call on ResultSet 
        //(rs.getClob(sche..) )  leads to the exception"java.sql.SQLException: Exhausted Resultset",
        //which inturn leads to Connection Leak.
       if(rs.next())
       {
        for (int i = 1; i <= size(); i++)
        {
            if (! valueIsClean(i) && ! schema().column(i).readOnly())
            {
                if (schema().column(i).isClob()) {
                    Clob myClob;
                    myClob = rs.getClob(schema().column(i).name());
                    if (myClob != null)
                    {
                        Writer os = ((weblogic.jdbc.vendor.oracle.OracleThinClob) myClob).getCharacterOutputStream();  
                        if (os != null)
                        {
                            String tmpStr = getValue(i).asString();
                            if (tmpStr != null)
                            {
                                os.write(tmpStr);
                                os.flush();
                                os.close();
                            }
                        }
                    }
                }
            }
        }
       }//if(..)
        rs.close();
        stmt.close();
        this.needsClobAction = false;
        
      } catch (Exception e) { e.printStackTrace(); }
    }
    
    
    //Begin blob enhancements
    
    private void saveBlobValues (Connection connection)
    {
     try{
    		String stmtStr=getSelectBlobColumnsString();
    	
       try(PreparedStatement stmt = connection.prepareStatement (stmtStr) ) {
        
        int ps = 1;
        for (int i = 1; i <= dataset().keydef().size(); i++)
        {
            Value val = getValue (dataset().keydef().getAttrib(i));
            if (val.isNull()) {
                throw new DataSetException ("You cannot execute an update with a null value for a KeyDef.");
            }
            val.setPreparedStatementValue (stmt, ps++);
        }

        ResultSet rs = stmt.executeQuery();
        //IR-YAUI010956164 Krishna
        // rs.next(); is commented out and the same is wrapped into if condition
        //rs.next();
        //In some cases the ResultSet may not have any records.The code rs.next(); moves the cursor from 
        //initial position to a position where there are no records.So, the function call on ResultSet
        //(rs.get..(..) )  leads to the exception"java.sql.SQLException: Exhausted Resultset".
        //which inturn leads to Connection Leak.
       if(rs.next())
       {
        for (int i = 1; i <= size(); i++)
        {
            if (! valueIsClean(i) && ! schema().column(i).readOnly())
            {
                if (schema().column(i).isBlob()) {
                    Blob myBlob;
                    myBlob = rs.getBlob(schema().column(i).name());
                    if (myBlob != null)
                    {
                        OutputStream os = ((weblogic.jdbc.vendor.oracle.OracleThinBlob) myBlob).getBinaryOutputStream();
                        
                        if (os != null)
                        {
                            byte[] tmpBlob = (byte[])getValue(i).getValue();
                            
                            if (tmpBlob != null)
                            {
                                os.write(tmpBlob);
                                os.flush();
                                os.close();
                            }
                                                        
                        }
                    }
                }
            }
        }
       } 
        rs.close();
        this.needsBlobAction = false;
      }
      } catch (Exception e) { e.printStackTrace(); }
    }
    
    //End blob enhancements  
        

    private int saveWithInsert (Connection connection)
            throws DataSetException, SQLException
    {
        PreparedStatement stmt = null;

        try
        {
            String myString = getSaveString();
            //System.out.println("saveWithInsert 1 "+myString);
//          stmt = connection.prepareStatement (getSaveString());
            stmt = connection.prepareStatement (myString);
            int ps = 1;
            for (int i = 1; i <= size(); i++)
            {
                Value val = getValue (i);
                if (! valueIsClean(i) &&
                        ! schema().column(i).readOnly() &&
                            ! schema().column(i).isClob() &&
                            ! schema().column(i).isBlob())
                {
                    val.setPreparedStatementValue (stmt, ps++);
                }
            }
            int ret = stmt.executeUpdate();

            if (this.needsClobAction) {
                this.saveClobValues(connection);
            }
            
            if (this.needsBlobAction) {
                this.saveBlobValues(connection);
            }
            
            if (((TableDataSet) dataset()).refreshOnSave())
            {
                refresh (dataset().connection());
            }
            else
            {
                // Marks all of the values clean since they have now been saved
                markRecordClean();
            }

            setSaveType (Enums.AFTERINSERT);

            if (ret > 1)
                throw new SQLException ("There were " + ret + " rows inserted with this records key value.");

            return ret;
        }
        catch (SQLException e1)
        {
            throw e1;
        }
        catch (Exception e)
        {
            throw new SQLException ();
        }
        finally
        {
            try
            {
                if (stmt != null)
                    stmt.close();
            }
            catch (SQLException e2)
            {
                throw e2;
            }
        }
    }

    /**
       Builds the SQL UPDATE statement for this Record
       @return SQL UPDATE statement
     */
    private String getUpdateSaveString() throws DataSetException
    {
        KeyDef kd = dataset().keydef();
        if (kd == null || kd.size() == 0)
            throw new DataSetException ("You must specify KeyDef attributes for this TableDataSet in order to create a Record for update.");
        else if (recordIsClean())
            throw new DataSetException ("You must Record.setValue() on a column before doing an update.");

        StringBuilder iss1 = new StringBuilder(256);
        StringBuilder iss2 = new StringBuilder(256);
        boolean comma = false;

        for (int i = 1; i <= size(); i++)
        {
             
            if (! valueIsClean(i) &&
                    ! schema().column(i).readOnly())
            {
                if (! comma)
                {
                    iss1.append (schema().column(i).name());

                    if ((! schema().column(i).isClob())&&(! schema().column(i).isBlob()))
                    {
                        iss1.append (" = ?");
                    }
                    else
                    {
                        if ( schema().column(i).isClob())
                        {
                            iss1.append ("= EMPTY_CLOB()");
                            this.needsClobAction = true;
                        }
                        else
                        {
                            iss1.append ("= EMPTY_BLOB()");
                            this.needsBlobAction = true;
                        }
                    }                    

                    comma = true;
                }
                else
                {
                    iss1.append (", ");
                    iss1.append (schema().column(i).name());
                    
                    if ((! schema().column(i).isClob())&&(! schema().column(i).isBlob()))
                    {
                        iss1.append (" = ?");
                    }
                    else
                    {
                        if ( schema().column(i).isClob())
                        {
                            iss1.append (" = EMPTY_CLOB()");
                            this.needsClobAction = true;
                        }
                        else
                        {
                            iss1.append (" = EMPTY_BLOB()");
                            this.needsBlobAction = true;
                        }
                    }                                                            
                }
            }
        }

        comma = false;

        for (int i = 1; i <= kd.size(); i++)
        {
            String attrib = kd.getAttrib(i);

            if (! valueIsClean (schema().index (attrib)))
                throw new DataSetException ("The value for column '" +
                        attrib + "' is a key value and cannot be updated.");

            if (! comma)
            {
                iss2.append (attrib);
                iss2.append (" = ?");
                comma = true;
            }
            else
            {
                iss2.append (" AND ");
                iss2.append (attrib);
                iss2.append (" = ?");
            }
        }
        
        TableDataSet tblDS;
        tblDS = (TableDataSet)parentDataSet;
        
        if (tblDS.optimisticLockingCol() != null)
        {
            // Release 8.2 CR-708B 2/18/13 W Zhu initialize opt_lock if it is empty.
            Value newIncrementedValue = getValue(tblDS.optimisticLockingCol());
            String newIncrementedValueStr = newIncrementedValue.getValue().toString();
            String copyOfStr = newIncrementedValueStr;
            Integer x;
            if (newIncrementedValueStr.length() > 0) {
                x = new Integer(Integer.parseInt(newIncrementedValueStr) +1);
            }
            else {
                x = new Integer(0);
            }
            newIncrementedValue.setValue(x);
            
            setValue(tblDS.optimisticLockingCol(), newIncrementedValue);
            if (! comma) {
                comma = true;
            }
            else {
                iss2.append (" AND ");                
            }
            iss2.append (tblDS.optimisticLockingCol());
            if (copyOfStr.length() > 0)
            {
                iss2.append(" = ").append(copyOfStr);
            }
            else
            {
                iss2.append (" is null ");
            }
        }
        return "UPDATE " + schema().tableName() + " SET " +
                iss1.toString() + " WHERE " + iss2.toString();
    }

    /**
       Builds the SQL DELETE statement for this Record
       @return SQL DELETE statement
     */
    private String getDeleteSaveString() throws DataSetException
    {
        KeyDef kd = dataset().keydef();
        if (kd == null || kd.size() == 0)
        {
            throw new DataSetException ("You must specify KeyDef attributes for this TableDataSet in order to delete a Record.");
        }

        StringBuilder iss1 = new StringBuilder(256);

        boolean comma = false;
        for (int i = 1; i <= kd.size(); i++)
        {
            if (! comma)
            {
                iss1.append (kd.getAttrib(i));
                iss1.append (" = ?");
                comma = true;
            }
            else
            {
                iss1.append (" AND ");
                iss1.append (kd.getAttrib(i));
                iss1.append (" = ? ");
            }
        }
        
        TableDataSet tblDS;
        tblDS = (TableDataSet) parentDataSet;
        
        if (tblDS.optimisticLockingCol() != null)
        {
            // Release 8.2 CR-708B 2/18/13 W Zhu initialize opt_lock if it is empty.
            Value optLockValue = getValue(tblDS.optimisticLockingCol());
            String optLockValueStr = optLockValue.getValue().toString();
            if (! comma) {
                comma = true;
            }
            else {
                iss1.append (" AND ");                
            }
            iss1.append (tblDS.optimisticLockingCol());
            if (optLockValueStr.length() > 0)
            {
                iss1.append(" = ").append(optLockValueStr);
            }
            else
            {
                iss1.append (" is null ");
            }
        }
        return "DELETE FROM " + schema().tableName() + " WHERE " +
                iss1.toString();
    }

    /**
       Builds the SQL INSERT statement for this Record
       @return SQL INSERT statement
     */
    private String getInsertSaveString() throws DataSetException
    {
        StringBuilder iss1 = new StringBuilder(256);
        StringBuilder iss2 = new StringBuilder(256);
        TableDataSet tblDS;
        
        boolean comma = false;
        for (int i = 1; i <= size(); i++)
        {
            Value val = getValue (i);
            if (! valueIsClean(i) &&
                    ! schema().column(i).readOnly())
            {
                if (! comma)
                {
                    comma = true;
                    iss1.append (schema().column(i).name());

                    if ((! schema().column(i).isClob())&&(! schema().column(i).isBlob()))
                    {
                        iss2.append ("?");
                    }
                    else
                    {
                        if ( schema().column(i).isClob())
                        {
                            iss2.append ("EMPTY_CLOB()");
                            this.needsClobAction = true;
                        }
                        else
                        {
                            iss2.append ("EMPTY_BLOB()");
                            this.needsBlobAction = true;
                        }
                    }                    
                }
                else
                {
                    iss1.append(", ").append(schema().column(i).name());
                    
                    if ((! schema().column(i).isClob())&&(! schema().column(i).isBlob()))
                    {
                        iss2.append (", ?");
                    }
                    else
                    {
                        if ( schema().column(i).isClob())
                        {
                            iss2.append (", EMPTY_CLOB()");
                            this.needsClobAction = true;
                        }
                        else
                        {
                            iss2.append (", EMPTY_BLOB()");
                            this.needsBlobAction = true;
                        }
                    }                    
                }
            }
        }
        tblDS = (TableDataSet)parentDataSet;
        if (tblDS.optimisticLockingCol() != null)
        {
            setValue(tblDS.optimisticLockingCol(), 1);
        }
        
        return "INSERT INTO " + schema().tableName() + " ( " +
                iss1.toString() + " ) VALUES ( " + iss2.toString() + " )";
    }

    /*
       private Hashtable getAffectedColumns()
         throws DataSetException
       {
       	Hashtable affectedColumns = new Hashtable ( size() );
       	for ( int i = 1; i <= size(); i++ )
         {
           if ( valueIsClean(i) == false )
             affectedColumns.put ( (Object) new Integer(i) , (Object) schema().getColumns()[i].name() );
         }
       	return affectedColumns;
       }
     */

    /**
      Gets the appropriate SQL string for this record.

      @return SQL string
    */
    public String getSaveString() throws DataSetException
    {
        if (toBeSavedWithInsert())
        {
            return getInsertSaveString();
        }
        else if (toBeSavedWithUpdate())
        {
            return getUpdateSaveString();
        }
        else if (toBeSavedWithDelete())
        {
            return getDeleteSaveString();
        }
        else
        {
            throw new DataSetException (
                    "Not able to return save string: " + this.saveType);
        }
    }

    /**
       gets the value at index i
       @return the Value object at index i
     */
    public Value getValue (int i) throws DataSetException
    {
        if (i == 0)
            throw new DataSetException ("Values are 1 based!");
        else if (i > size())
            throw new DataSetException ("Only " + size() + " columns exist!");
        else if (values[i] == null)
            throw new DataSetException ("No values for the requested column!");

        return values[i];
    }

    public Value getValue (String columnName) throws DataSetException
    {
        return getValue(schema().index (columnName));
    }

    /**
       the number of columns in this object
       @return the number of columns in this object
     */
    public int size()
    {
        return numberOfColumns;
    }
    /**
       whether or not this Record is to be saved with an SQL insert statement
       @return true if saved with insert
     */
    public boolean toBeSavedWithInsert()
    {
        return (this.saveType == Enums.INSERT) ? true : false;
    }
    /**
       whether or not this Record is to be saved with an SQL update statement
       @return true if saved with update
     */
    public boolean toBeSavedWithUpdate()
    {
        return (this.saveType == Enums.UPDATE) ? true : false;
    }
    /**
       whether or not this Record is to be saved with an SQL delete statement
       @return true if saved with delete
     */
    public boolean toBeSavedWithDelete()
    {
        return (this.saveType == Enums.DELETE) ? true : false;
    }

    /**
      * Marks all the values in this record as clean.
      *
      */
    public void markRecordClean() throws DataSetException
    {
        for (int i = 1; i <= size(); i++)
        {
            markValueClean(i);
        }
    }

    /**
       Marks this record to be inserted when a save is executed.
     */
    public void markForInsert() throws DataSetException
    {
        if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You cannot mark a record in a QueryDataSet for insert");

        setSaveType (Enums.INSERT);
    }
    /**
       Marks this record to be updated when a save is executed.
     */
    public void markForUpdate() throws DataSetException
    {
        if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You cannot mark a record in a QueryDataSet for update");

        setSaveType (Enums.UPDATE);
    }
    /**
       Marks this record to be deleted when a save is executed.
     */
    public Record markToBeDeleted() throws DataSetException
    {
        if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You cannot mark a record in a QueryDataSet for deletion");

        setSaveType (Enums.DELETE);
        return this;
    }

    /**
       Unmarks a record that has been marked for deletion.
       <P>
       WARNING: You must reset the save type before trying to save this record again.

       @see #markForUpdate()
       @see #markForInsert()
       @see #markToBeDeleted()
     */
    public Record unmarkToBeDeleted() throws DataSetException
    {
        if (this.saveType == Enums.ZOMBIE)
            throw new DataSetException ("This record has already been deleted!");

        setSaveType (Enums.UNKNOWN);
        return this;
    }

    /**
       marks a value at a given position as clean.
     */
    public void markValueClean (int pos) throws DataSetException
    {
        if (pos == 0)
            throw new DataSetException ("Value position must be greater than 0.");
        else if (pos > size())
            throw new DataSetException ("Value position is greater than number of values.");

        this.isClean[pos] = true;
    }

    /**
       marks a value with a given column name as clean.
     */
    public void markValueClean (String columnName) throws DataSetException
    {
        markValueClean (schema().index(columnName));
    }

    /**
       marks a value at a given position as dirty.
     */
    public void markValueDirty (int pos) throws DataSetException
    {
        if (pos == 0)
            throw new DataSetException ("Value position must be greater than 0.");
        else if (pos > size())
            throw new DataSetException ("Value position is greater than number of values.");

        this.isClean[pos] = false;
    }

    /**
       marks a value with a given column name as dirty.
     */
    public void markValueDirty (String columnName) throws DataSetException
    {
        markValueDirty (schema().index(columnName));
    }

    /**
       sets the internal save type as one of the defined privates (ie: ZOMBIE)
     */
    void setSaveType (int type)
    {
        this.saveType = type;
    }

    /**
       gets the internal save type as one of the defined privates (ie: ZOMBIE)
     */
    int getSaveType ()
    {
        return this.saveType;    
    }
    
    /**
       sets the value at pos with a BigDecimal
     */
    public Record setValue (int pos,
            BigDecimal value) throws DataSetException
    {
        this.values[pos].setValue (value);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a boolean
     */
    public Record setValue (int pos, boolean value) throws DataSetException
    {
        this.values[pos].setValue (Boolean.valueOf(value));
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a byte[]
     */
    public Record setValue (int pos, byte[] value) throws DataSetException
    {
        this.values[pos].setValue (value);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a java.util.Date
     */
    public Record setValue (int pos,
            java.util.Date value) throws DataSetException
    {
        this.values[pos].setValue (value);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a java.sql.Date
     */
    public Record setValue (int pos,
            java.sql.Date value) throws DataSetException
    {
        this.values[pos].setValue (value);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a double
     */
    public Record setValue (int pos, double value) throws DataSetException
    {
        this.values[pos].setValue (new Double (value));
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a float
     */
    public Record setValue (int pos, float value) throws DataSetException
    {
        this.values[pos].setValue (new Float (value));
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a int
     */
    public Record setValue (int pos, int value) throws DataSetException
    {
        this.values[pos].setValue (new Integer (value));
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a long
     */
    public Record setValue (int pos, long value) throws DataSetException
    {
        this.values[pos].setValue (new Long (value));
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a String
     */
    public Record setValue (int pos, String value) throws DataSetException
    {
        this.values[pos].setValue (value);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a java.sql.Time
     */
    public Record setValue (int pos,
            java.sql.Time value) throws DataSetException
    {
        this.values[pos].setValue (value);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a java.sql.Timestamp
     */
    public Record setValue (int pos,
            java.sql.Timestamp value) throws DataSetException
    {
        this.values[pos].setValue (value);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at pos with a Value
     */
    public Record setValue (int pos, Value value) throws DataSetException
    {
        this.values[pos].setValue (value.getValue());
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at column name with a BigDecimal
     */
    public Record setValue (String columnName,
            BigDecimal value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a boolean
     */
    public Record setValue (String columnName,
            boolean value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a byte[]
     */
    public Record setValue (String columnName,
            byte[] value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a java.util.Date
     */
    public Record setValue (String columnName,
            java.util.Date value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a java.sql.Date
     */
    public Record setValue (String columnName,
            java.sql.Date value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a double
     */
    public Record setValue (String columnName,
            double value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a float
     */
    public Record setValue (String columnName,
            float value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a int
     */
    public Record setValue (String columnName,
            int value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a long
     */
    public Record setValue (String columnName,
            long value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a String
     */
    public Record setValue (String columnName,
            String value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a java.sql.Time
     */
    public Record setValue (String columnName,
            java.sql.Time value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a java.sql.Timestamp
     */
    public Record setValue (String columnName,
            java.sql.Timestamp value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at column name with a Value
     */
    public Record setValue (String columnName,
            Value value) throws DataSetException
    {
        setValue (schema().index(columnName), value);
        return this;
    }

    /**
       sets the value at pos with a NULL
     */
    public Record setValueNull (int pos) throws DataSetException
    {
        if (pos == 0)
            throw new DataSetException ("Value position must be greater than 0.");
        else if (pos > size())
            throw new DataSetException ("Value position is greater than number of values.");

        this.values[pos].setValue (null);
        markValueDirty (pos);
        return this;
    }

    /**
       sets the value at column name with a NULL
     */
    public Record setValueNull (String columnName) throws DataSetException
    {
        if (columnName == null || columnName.length() == 0)
            throw new DataSetException ("You must specify a column name!");

        setValueNull(schema().index(columnName));
        return this;
    }

    /**
       Determines if this record is a Zombie. A Zombie is a record that has been deleted from the
       database, but not yet removed from the DataSet.

       @return a boolean
     */
    public boolean isAZombie()
    {
        return (this.saveType == Enums.ZOMBIE) ? true : false;
    }

    /**
       If the record is not clean, needs to be saved with an Update, Delete or Insert, it returns true.

       @return boolean
     */
    public boolean needsToBeSaved()
    {
        if (!isAZombie() || !recordIsClean() || toBeSavedWithUpdate() ||
                toBeSavedWithDelete() || toBeSavedWithInsert())
            return true;
        else
            return false;
    }

    /**
       Determines whether or not a value stored in the record is clean.

       @return true if clean
     */
    public boolean valueIsClean(int i)
    {
        return isClean[i];
    }

    /**
       Determines whether or not a value stored in the record is clean.

       @return true if clean
     */
    boolean valueIsClean(String column) throws DataSetException
    {
        return isClean[getValue (column).columnNumber()];
    }

    /**
       Goes through all the values in the record to determine if it is clean or not.

       @return true if clean
     */
    public boolean recordIsClean()
    {
        for (int i = 1; i <= size(); i++)
        {
            if (valueIsClean(i) == false)
                return false;
        }
        return true;
    }

    /**
      * This method refreshes this Record's Value's. It can only be performed on
      * a Record that has not been modified and has been created with a TableDataSet
      * and corresponding KeyDef.
      *
      * @param   connection
      * @exception   DataSetException
      * @exception   SQLException
      */
    public void refresh (Connection connection)
            throws DataSetException, SQLException
    {
        if (toBeSavedWithDelete())
            return;
        else if (toBeSavedWithInsert())
            throw new DataSetException ("There is no way to refresh a record which has been created with addRecord().");
        else if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You can only perform a refresh on Records created with a TableDataSet.");

        PreparedStatement stmt = null;
        try
        {
            stmt = connection.prepareStatement (getRefreshQueryString());
            int ps = 1;
            for (int i = 1; i <= dataset().keydef().size(); i++)
            {
                Value val = getValue (dataset().keydef().getAttrib(i));
                if (val.isNull())
                    throw new DataSetException ("You cannot execute an update with a null value for a KeyDef.");

                val.setPreparedStatementValue (stmt, ps++);
            }

            ResultSet rs = stmt.executeQuery();
            
            //IR-YAUI010956164 Krishna
            // rs.next(); is commented out and the same is wrapped into if condition
            //rs.next();
            //In some cases the ResultSet may not have any records.The code rs.next(); moves the cursor 
            //from initial position to a position where there are no records.So, the function using ResultSet rs 
            //leads to the exception"java.sql.SQLException: Exhausted Resultset".
            //which inturn leads to Connection Leak.
            initializeRecord();
            if(rs.next())
            {
            createValues(rs);
            }
        }
        catch (SQLException e1)
        {
            throw e1;
        }
        finally { try
            {
                if (stmt != null)
                    stmt.close();
            }
            catch (SQLException e2)
            {
                throw e2;
            }
        } }

//Begin Clob Enhancement
    public String getSelectClobColumnsString() throws DataSetException
    {
        if (dataset().keydef() == null || dataset().keydef().size() == 0)
            throw new DataSetException ("You can only perform a getRefreshQueryString on a TableDataSet that was created with a KeyDef.");
        else if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You can only perform a getRefreshQueryString on Records created with a TableDataSet.");

        StringBuilder iss1 = new StringBuilder(256);
        StringBuilder iss2 = new StringBuilder(256);
        boolean comma = false;

        for (int i = 1; i <= size(); i++)
        {
            if (schema().column(i).isClob())
            {
                if (! comma)
                {
                    iss1.append (schema().column(i).name());
                    comma = true;
                }
                else
                {
                    iss1.append (", ");
                    iss1.append (schema().column(i).name());
                }
            }
        }
        comma = false;

        for (int i = 1; i <= dataset().keydef().size(); i++)
        {
            String attrib = dataset().keydef().getAttrib(i);

            if (! comma)
            {
                iss2.append (attrib);
                iss2.append (" = ?");
                comma = true;
            }
            else
            {
                iss2.append (" AND ");
                iss2.append (attrib);
                iss2.append (" = ?");
            }
        }
        return "SELECT " + iss1.toString() + " FROM " +
                schema().tableName() + " WHERE " + iss2.toString() + " FOR UPDATE";
    }
//End Clob Enhancement


//Begin Blob Enhancement
    public String getSelectBlobColumnsString() throws DataSetException
    {
        if (dataset().keydef() == null || dataset().keydef().size() == 0)
            throw new DataSetException ("You can only perform a getRefreshQueryString on a TableDataSet that was created with a KeyDef.");
        else if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You can only perform a getRefreshQueryString on Records created with a TableDataSet.");

        StringBuilder iss1 = new StringBuilder(256);
        StringBuilder iss2 = new StringBuilder(256);
        boolean comma = false;

        for (int i = 1; i <= size(); i++)
        {
            if (schema().column(i).isBlob())
            {
                if (! comma)
                {
                    iss1.append (schema().column(i).name());
                    comma = true;
                }
                else
                {
                    iss1.append (", ");
                    iss1.append (schema().column(i).name());
                }
            }
        }
        comma = false;

        for (int i = 1; i <= dataset().keydef().size(); i++)
        {
            String attrib = dataset().keydef().getAttrib(i);

            if (! comma)
            {
                iss2.append (attrib);
                iss2.append (" = ?");
                comma = true;
            }
            else
            {
                iss2.append (" AND ");
                iss2.append (attrib);
                iss2.append (" = ?");
            }
        }
        String tempString = "SELECT " + iss1.toString() + " FROM " +
                schema().tableName() + " WHERE " + iss2.toString() + " FOR UPDATE";
        return tempString;
    }
//End Blob Enhancement




    /**
      * This builds the SELECT statement in order to refresh the contents of
      * this Record. It depends on a valid KeyDef to exist and it must have been
      * created with a TableDataSet.
      *
      * @return     the SELECT string
      * @exception   DataSetException
      */
    public String getRefreshQueryString() throws DataSetException
    {
        if (dataset().keydef() == null || dataset().keydef().size() == 0)
            throw new DataSetException ("You can only perform a getRefreshQueryString on a TableDataSet that was created with a KeyDef.");
        else if (dataset() instanceof QueryDataSet)
            throw new DataSetException ("You can only perform a getRefreshQueryString on Records created with a TableDataSet.");

        StringBuilder iss1 = new StringBuilder(256);
        StringBuilder iss2 = new StringBuilder(256);
        boolean comma = false;

        for (int i = 1; i <= size(); i++)
        {
            if (! comma)
            {
                iss1.append (schema().column(i).name());
                comma = true;
            }
            else
            {
                iss1.append (", ");
                iss1.append (schema().column(i).name());
            }
        }

        comma = false;

        for (int i = 1; i <= dataset().keydef().size(); i++)
        {
            String attrib = dataset().keydef().getAttrib(i);

            if (! valueIsClean(attrib))
                throw new DataSetException (
                        "You cannot do a refresh from the database if the value " +
                        "for a KeyDef column has been changed with a Record.setValue().");

            if (! comma)
            {
                iss2.append (attrib);
                iss2.append (" = ?");
                comma = true;
            }
            else
            {
                iss2.append (" AND ");
                iss2.append (attrib);
                iss2.append (" = ?");
            }
        }
        return "SELECT " + iss1.toString() + " FROM " +
                schema().tableName() + " WHERE " + iss2.toString();
    }

    public void saveWithoutStatusUpdate() throws DataSetException
    {
        throw new DataSetException ("Record.saveWithoutStatusUpdate() is not yet implemented.");
    }

    /**
       Gets the schema for the parent DataSet

       @return the schema for the parent DataSet
     */
    public Schema schema() throws DataSetException
    {
        if (dataset() != null)
            return this.schema;
        else
            throw new DataSetException ("Internal Error: Record DataSet is null");
    }

    /**
       Gets the DataSet for this Record

       @return the DataSet for this Record
     */
    public DataSet dataset()
    {
        return this.parentDataSet;
    }

    /**
       Sets the parent DataSet for this record.
     */
    void setParentDataSet(DataSet ds)
    {
        this.parentDataSet = ds;
    }

    /**
      * return the value of each column as a string.
      * Not yet implemented!
      *
      * @param   valueseparator
      * @param   maxwidths
      * @return     the formatted string
      * @exception   DataSetException
      */
    public String asFormattedString(String valueseparator,
            int maxwidths[]) throws DataSetException
    {
        throw new DataSetException ("Not yet implemented!");
    }

    /**
     * This returns a representation of this Record
     * @return java.lang.String
     */
    public String toString ()
    {
        try
        {
            ByteArrayOutputStream bout = new ByteArrayOutputStream ();
            PrintWriter out = new PrintWriter (bout);
            out.print ("{");
            for (int i = 1; i <= size (); i++)
            {
                out.print ("'" + getValue(i).asString() + "'");
                if (i < size ())
                    out.print (',');
            }
            out.print ("}");
            out.flush ();
            return bout.toString ();
        } catch (Exception e)
        {
            return "";
        }
    }

    /**
      * Updates the status of a Record after a saveWithoutStatusUpdate()
      *
      * @exception   SQLException
      * @exception   DataSetException
      */
    public void updateStatus() throws SQLException, DataSetException
    {
        this.status = true;
    }

}
