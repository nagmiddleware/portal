/*
 * The Working-Dogs.com License, Version 1.1
 *
 * Copyright (c) 1999 Working-Dogs.com.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:  
 *       "This product includes software developed by the 
 *        Working-Dogs.com <http://www.Working-Dogs.com/>."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "Working-Dogs.com" and "Village" must not be used to 
 *    endorse or promote products derived from this software without 
 *    prior written permission. For written permission, please contact 
 *    jon@working-dogs.com.
 *
 * 5. Products derived from this software may not be called 
 *    "Working-Dogs.com" nor may "Village" appear in their names 
 *    without prior written permission of Working-Dogs.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Working-Dogs.com.  For more
 * information on the Working-Dogs.com, please see
 * <http://www.Working-Dogs.com/>.
 */
/*
 *     Modifications are Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

package com.workingdogs.village;

import java.util.Vector;

/**
A KeyDef is a way to define the key columns in a table. The KeyDef is 
generally used in conjunction with a <a href="TableDataSet.html">TableDataSet</a>. 
Essentially a KeyDef is what forms the WHERE clause for an UPDATE or DELETE.
<P>
In order to use the KeyDef, you simply use it like this:
<PRE>
KeyDef kd = new KeyDef().addAttrib("key_column_a");
TableDataSet tds = new TableDataSet ( connection, "table", kd );
tds.fetchRecords();
Record rec = tds.getRecord(0);
rec.setValue("column_name", "new value" );
rec.save();
tds.close();
</PRE>
In the above example, Record 0 is retrieved from the database table 
and the following update statement is generated:
<P>
UPDATE table SET column_name=? WHERE key_column_a=?
<P>

@author Jon S. Stevens <A HREF="mailto:jon@working-dogs.com">jon@working-dogs.com</A>
@version $Revision:   1.0  $
*/
public class KeyDef
{
    private Vector mySelf = null;
    
    /**
        Constructor for KeyDef.

        Make sure to always initialize KeyDef with an initial element 
        because it is 1 based.
    */
    public KeyDef()
    {
        mySelf = new Vector();
        mySelf.addElement ("");        
    }

    /**
       Adds the named attribute to the KeyDef.

       @returns a copy of itself
     */
    public KeyDef addAttrib(String name)
    {
        mySelf.addElement (name);
        return this;
    }

    /**
       Determines if the KeyDef contains the requested Attribute.

       @returns true if the attribute has been defined. false otherwise.
     */
    public boolean containsAttrib (String name)
    {
        return (mySelf.indexOf ((Object) name) == -1) ? false : true;
    }

    /**
       getAttrib is 1 based. Setting pos to 0 will attempt to return pos 1.

       @returns value of Attribute at pos as String. null if value is not found.
     */
    public String getAttrib (int pos)
    {
        if (pos == 0)
            pos = 1;

        try
        {
            return (String) mySelf.elementAt (pos);
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            return null;
        }
    }

    /**
       KeyDef's are 1 based, returns size - 1

       @returns the number of elements in the KeyDef that were set by addAttrib()
       @see #addAttrib(java.lang.String)
     */
    public int size()
    {
        return mySelf.size() - 1;
    }
}
