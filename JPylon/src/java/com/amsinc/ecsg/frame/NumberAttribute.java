package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.util.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class NumberAttribute extends Attribute {

	public NumberAttribute ()
	 {
     }

	public NumberAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public NumberAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }

	// Validation logic for number attribute.
	public void validate(String attributeValue) throws AmsException{
		// Ensure that value is numeric
		if(!StringService.isDigit(attributeValue)){
			throw new InvalidAttributeValueException(AmsConstants.INVALID_NUMBER_ATTRIBUTE);
		}
	}
}
