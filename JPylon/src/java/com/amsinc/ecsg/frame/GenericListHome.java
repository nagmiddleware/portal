/*
 * @(#)GenericListHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import javax.ejb.*;

public interface GenericListHome extends EJBHome
{
  public GenericList create()
    throws CreateException, RemoteException;
  public GenericList create(ClientServerDataBridge csdb)
    throws CreateException, RemoteException;
}

