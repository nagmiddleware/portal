package com.amsinc.ecsg.frame;

/*
 * @(#)ListViewBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.rmi.*;
import com.workingdogs.village.*;
//import weblogic.db.jdbc.*;
import java.util.*;
import java.sql.*;

import javax.ejb.*;
import com.amsinc.ecsg.util.*;
import java.text.MessageFormat;
import javax.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.*;

/**
 * ListViewBean is the ancestor class provided to create SQL based views. This
 * class provides methods to build and excute simple to complex SQL statements.
 * In addition, methods are provided to navigate the result set that is
 * created by the view.
 *
 * @version 1.0 
 */
public class ListViewBean implements SessionBean
{
	private static final Logger LOG = LoggerFactory.getLogger(ListViewBean.class);
    // DBMS properties
    protected transient Connection dbmsConnection = null;
    protected transient QueryDataSet listViewData = null;
    protected transient String SQLStatement = "";
    protected transient Record currentRecord = null;
    protected transient int RecordCount = 0;

    protected transient Vector[] results = null;
    protected transient String[] columnNames = null;
    protected transient int rowPointer = 0;
    protected transient Hashtable columnHash = null;
    protected transient int returnCount = 0;
    protected transient Object[] sqlParams = null;
    
    /**
     * Builds the execution SQL Statement by substituting values supplied in the
     * sqlArguments argument into the SQL statement supplied in the
     * ListViewBean's deployment descriptor file.
     * 
     * @param sqlArguments The SQL substitution values
     * @param wrapArguments Boolean indicator as to if SQL arguments
     * are wrapped with single quote marks ('xxx').
     * @exception AmsException
     */
    protected String buildSQLStatement (String[] sqlArguments,
                                        boolean wrapArguments)
              throws AmsException
    {
        // Get the original SQL Statement
        if (this.SQLStatement == null)
        {
            throw new AmsException (
                "SQL Statement must be specified in Deployment Descriptor's " +
                "'sqlStatement' environment property");
        }
        // If no substitution arguments have been supplied, do not perform
        // message processing
        if (sqlArguments.length < 1)
        {
            // Return the basic SQL statement
            return this.SQLStatement;
        }
        else
        {
            if (wrapArguments)
            {
                int index = 0;

                // Wrap the parameters with '' (for DMBS processing)
                for (index = 0;index < sqlArguments.length;index++)
                {
                    sqlArguments[index] =
                        StringFunction.wrapString(sqlArguments[index], "'");
                }
            }
            // Return the processed SQL statement
            return MessageFormat.format (this.SQLStatement, (Object[])sqlArguments);
        }
    }
    // -------------------------------------------------//
    // DBMS Connect Methods                             //
    // -------------------------------------------------//
    
    /**
     * Connects the ListView to the DBMS using the DBMS parameters that
     * have been designated in the bean's desployment descriptor.
     * 

     */
    protected void connect() throws AmsException {
        
        String dbmsParm = JPylonProperties.getInstance().getString("dataSourceName");

        // Get a connection from the Server's Connection Pool
        try
        {
            DataSource ds = DataSourceFactory.getDataSource (dbmsParm);
            dbmsConnection = ds.getConnection ();
        }
        catch(SQLException e)
        {
            throw new AmsException (
                "Error on connecting to Datasource in DataObject using DBMS parms: " +
                dbmsParm + " Nested Exception: " + e.getMessage ());
        }
    }
    /**
     * Disconnects the ListView from the DBMS
     * 
     */
    protected void disconnect() throws AmsException
    {
        // Close the DBMS connection
        try
        {
            if (dbmsConnection != null)
                dbmsConnection.close ();
        }
        catch (SQLException e)
        {
            throw new AmsException (
                "Errors encountered during DBMS disconnect: " +
                " Nested Exception: " + e.getMessage ());
        }
    }
    public void ejbActivate ()
    {
    }
    // EJB Create Method
    public void ejbCreate() throws AmsException
    {
    }
    public void ejbPassivate ()
    {
    }
    
    // EJB Methods not fully implemented, but coded to adhere to the EJB
    // contract
    public void ejbRemove () throws RemoteException
    {
    }
    
    /**
     * Gets the number of records that have been retrieved based on the call
     * to the getRecords() method. Note that this is not neccessarily the
     * number of records returned to the user, this can be found by calling
     * getReturnCount. The numbers differ when we are chunking. If a query
     * retieved 100 rows, but we through chunking we specified we only wanted
     * rows 10 through 20, this method returns 100, while getReturnCount
     * would return 11.
     *

     * @return The number of records retrieved
     */
    public int getRecordCount () throws AmsException
    {
        return this.RecordCount;
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor.<p>
     * <p>
     * NOTE: This method assumes the SQL statement provided in the Deployment
     * Descriptor does not required substitution processing and is a valid SQL
     * statement.
     * 
     * @exception AmsException
     */
    public void getRecords ()
           throws AmsException
    {
        String[] emptyStringArray = new String[0];
        this.getRecords (emptyStringArray);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor.<p>
     * <p>
     * NOTE: This method assumes the SQL statement provided in the Deployment
     * Descriptor does not required substitution processing and is a valid SQL
     * statement.
     * 
     * @exception AmsException
     */
    public void getRecords (int maxRows)
           throws AmsException
    {
        String[] emptyStringArray = new String[0];
        this.getRecords (emptyStringArray, maxRows);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor.<p>
     * <p>
     * NOTE: This method assumes the SQL statement provided in the Deployment
     * Descriptor does not required substitution processing and is a valid SQL
     * statement.
     * 
     * @exception AmsException
     */
    public void getRecords (int firstRow, int maxRows)
           throws AmsException
    {
        String[] emptyStringArray = new String[0];
        this.getRecords (emptyStringArray, firstRow, maxRows);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor. Based on the argument array passed
     * in, the original SQL statement will be parameterized with the runtime
     * argument values.<p>
     *<p>
     * NOTE: This method will wrap all SQL arguments with single quotes " ' "
     * to form a valid SQL statement.
     * 
     * @param arguments array of argument values used in SQL statement
     * substitution
     * @exception AmsException
     */
    public void getRecords (String[] arguments) throws AmsException
    {
        this.getRecords (arguments, true);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor. Based on the argument array passed
     * in, the original SQL statement will be parameterized with the runtime
     * argument values.<p>
     *<p>
     * NOTE: This method will wrap all SQL arguments with single quotes " ' "
     * to form a valid SQL statement.
     * 
     * @param arguments array of argument values used in SQL statement
     * substitution
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public void getRecords (String[] arguments, int maxRows)
           throws AmsException
    {
        this.getRecords (arguments, true, maxRows);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor. Based on the argument array passed
     * in, the original SQL statement will be parameterized with the runtime
     * argument values.<p>
     *<p>
     * NOTE: This method will wrap all SQL arguments with single quotes " ' "
     * to form a valid SQL statement.
     * 
     * @param arguments array of argument values used in SQL statement
     * substitution
     * @param firstRow The first row retrieved by the query that we care about
     * This is a zero based value, i.e. 0 = 1st row, 1 = 2nd row, etc. This
     * allows the user to retrieve "chunks" of data.
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public void getRecords (String[] arguments, int firstRow, int maxRows)
           throws AmsException
    {
        this.getRecords (arguments, true, firstRow, maxRows);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor. Based on the argument array passed
     * in, the original SQL statement will be parameterized with the runtime
     * argument values.<p>
     * <p>
     * IMPORTANT: The behavior of this method relies on the implementation
     * of the TableDataSet class's fetchRecords (void) method. It mirrors
     * that method's logic, if that method should ever change, then this one
     * must also.
     * 
     * @param arguments String array of argument values used in SQL statement
     * substitution
     * @param wrapArguments A boolean indicator as to if SQL arguments are
     * wrapped with single quotes " ' " when building the SQL statement.
     * allows the user to retrieve "chunks" of data.
     * @exception AmsException
     */
    public void getRecords (String[] arguments, boolean wrapArguments)
           throws AmsException
    {
       getRecords (arguments, wrapArguments, -1);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor. Based on the argument array passed
     * in, the original SQL statement will be parameterized with the runtime
     * argument values.<p>
     * <p>
     * IMPORTANT: The behavior of this method relies on the implementation
     * of the TableDataSet class's fetchRecords (int max) method. It mirrors
     * that method's logic, if that method should ever change, then this one
     * must also.
     * 
     * @param arguments String array of argument values used in SQL statement
     * substitution
     * @param wrapArguments A boolean indicator as to if SQL arguments are
     * wrapped with single quotes " ' " when building the SQL statement.
     * allows the user to retrieve "chunks" of data.
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public void getRecords (String[] arguments, boolean wrapArguments,
                            int maxRows)
           throws AmsException
    {
       getRecords (arguments, wrapArguments, 0, maxRows);
    }
    /**
     * Connects to the Database and executes the SQL statement provided in the
     * ListViewBean's deployment descriptor. Based on the argument array passed
     * in, the original SQL statement will be parameterized with the runtime
     * argument values.
     * 
     * @param arguments String array of argument values used in SQL statement
     * substitution
     * @param wrapArguments A boolean indicator as to if SQL arguments are
     * wrapped with single quotes " ' " when building the SQL statement.
     * @param firstRow The first row retrieved by the query that we care about
     * This is a zero based value, i.e. 0 = 1st row, 1 = 2nd row, etc. This
     * allows the user to retrieve "chunks" of data.
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public void getRecords (String[] arguments, boolean wrapArguments,
                            int firstRow, int maxRows)
           throws AmsException
    {
        // Get the original SQL Statement
        String executionSQL = this.buildSQLStatement (arguments, wrapArguments);
        try
        {
            this.connect ();
           try(PreparedStatement statement = dbmsConnection.prepareStatement (executionSQL)){
            
          //now set the inputs!
                                                if(sqlParams!=null){
                                                for (int i=0; i<sqlParams.length; i++) {
                                                                if (sqlParams[i] != null) {
                                                                                statement.setObject(i + 1, sqlParams[i]);
                                                                } else {
                                                                                statement.setNull(i + 1, Types.NULL);
                                                                                }
                                                                }
                                                }
            
           try(ResultSet rs = statement.executeQuery ()){
            ResultSetMetaData rsmd = rs.getMetaData ();
            int cols = rsmd.getColumnCount ();
            results = new Vector[cols];
            columnNames = new String[cols];
            columnHash = new Hashtable ();
            for (int i = 1;i <= cols;i++)
            {
                columnNames[i - 1] = rsmd.getColumnLabel (i).toUpperCase ();
                columnHash.put (rsmd.getColumnLabel (i).toUpperCase (),
                                new Integer (i-1));
            }
            populateStructure (rs, firstRow, maxRows);       
           }
          }
        }
        catch (Exception e)
        {
            throw new AmsException (e.getMessage());
        }
        finally
        {
            try
            {
                this.disconnect();
            }
            catch (Exception ignore)
            {}
            
            sqlParams = null;
        }
    }
    /**
     * Same as getRecords, but immediately returns the result set as an XML
     * document.
     * 
     * @exception AmsException
     */
    public DocumentHandler getRecordsAsXml ()
           throws AmsException, RemoteException
    {
        return (this.getRecordsAsXml (-1));
    }
    /**
     * Same as getRecords, but immediately returns the result set as an XML
     * document.
     * 
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public DocumentHandler getRecordsAsXml (int maxRows)
           throws AmsException, RemoteException
    {
        return (this.getRecordsAsXml (0, maxRows));
    }
    /**
     * Same as getRecords, but immediately returns the result set as an XML
     * document.
     * 
     * @param firstRow The first row retrieved by the query that we care about
     * This is a zero based value, i.e. 0 = 1st row, 1 = 2nd row, etc. This
     * allows the user to retrieve "chunks" of data.
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public DocumentHandler getRecordsAsXml (int firstRow, int maxRows)
           throws AmsException, RemoteException
    {
        String[] emptyStringArray = new String[0];
        this.getRecords (emptyStringArray, firstRow, maxRows);
        return (this.getXmlResultSet ());
    }
    /**
     * Same as getRecords, but immediately returns the result set as an XML
     * document.
     * 
     * @param arguments String array of argument values used in SQL statement
     * substitution
     * @exception AmsException
     */
    public DocumentHandler getRecordsAsXml (String[] arguments)
           throws AmsException, RemoteException
    {
        return (this.getRecordsAsXml (arguments, -1));
    }
    /**
     * Same as getRecords, but immediately returns the result set as an XML
     * document.
     * 
     * @param arguments String array of argument values used in SQL statement
     * substitution
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public DocumentHandler getRecordsAsXml (String[] arguments, int maxRows)
           throws AmsException, RemoteException
    {
        return (this.getRecordsAsXml (arguments, 0, maxRows));    
    }
    /**
     * Same as getRecords, but immediately returns the result set as an XML
     * document.
     * 
     * @param arguments String array of argument values used in SQL statement
     * substitution
     * @param firstRow The first row retrieved by the query that we care about
     * This is a zero based value, i.e. 0 = 1st row, 1 = 2nd row, etc. This
     * allows the user to retrieve "chunks" of data.
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     * @exception AmsException
     */
    public DocumentHandler getRecordsAsXml (String[] arguments, int firstRow,
                                            int maxRows)
           throws AmsException, RemoteException
    {
        this.getRecords (arguments, firstRow, maxRows);
        return (this.getXmlResultSet ());    
    }
    /**
     * Based on the current record pointer, this method returns the value of
     * the specified record column. If the column isn't valid, an exception is
     * thrown.
     * 
     * @param columnName The column name in which the value is being requested.
     * @exception AmsException
     */
    public String getRecordValue (String columnName) throws AmsException
    {
        try
        {
            Integer colInt = (Integer)columnHash.get (columnName.toUpperCase());
            int col = colInt.intValue ();
            Object o = results[col].elementAt (rowPointer);
            String value = (o == null ? null : o.toString ());
            return value;
        }
        catch (Exception e)
        {
            throw new AmsException (
                "Error getting value from record in DataSet. Nested exception: " +
                e.getMessage ());
        }
    }
    /**
     * Specialized implementation of getRecordValue to return java.sql.Clob columns.
     * Based on the current record pointer, this method returns the value of
     * the specified record column. If the column isn't valid, an exception is
     * thrown.
     * 
     * @param columnName The column name in which the value is being requested.
     * @return String returns the value of the column if it is a Clob
     * @exception AmsException
     */
    public String getRecordValueAsClob (String columnName) throws AmsException
    {
        try
        {
            Integer colInt = (Integer)columnHash.get (columnName.toUpperCase());
            int col = colInt.intValue ();
            Object o = results[col].elementAt (rowPointer);
            
            if (o == null) {
                return null;
            }

            Clob c = (Clob)o;
            return c.getSubString(new Long(1).longValue(), new Long(c.length()).intValue());

        }
        catch (Exception e)
        {
            throw new AmsException (
                "Error getting CLOB value from record in DataSet. Nested exception: " +
                e.getMessage ());
        }
    }    
    
    /**
     * The number of rows we actually retrieved from the database.
     *
     * @return Number of rows retrieved.
     */
    public int getReturnCount() throws AmsException, RemoteException
    {
        return this.returnCount;
    }
    
    public DocumentHandler getXmlResultSet ()
           throws AmsException, RemoteException
    {
        DocumentHandler dh = null;
        StringBuilder sbuff = new StringBuilder("<DocRoot>");
        Object data = null;
        String stringData = null;
        char[] srsr = "<ResultSetRecord ID=\"".toCharArray();
        char[] ersr = "</ResultSetRecord>".toCharArray();
        try
        {
            int columnCount = columnNames.length;
            for (int rowIndex = 0;rowIndex < getReturnCount ();rowIndex++)
            {
                sbuff.append (srsr);
                sbuff.append (rowIndex);
                sbuff.append ('"');
                sbuff.append ('>');
                for (int columnIndex = 0;
                     columnIndex < columnCount;
                     columnIndex++)
                {
                    sbuff.append ('<');
                    sbuff.append (columnNames[columnIndex]);
                    sbuff.append ('>');
                    data = results[columnIndex].elementAt (rowIndex);
                    if (data == null)
                    {
                        stringData = "";
                    }
                    else
                    {
                        stringData = data.toString ();
                        stringData =
                            StringService.change (stringData, "&", "&amp;");
                        stringData =
                            StringService.change (stringData, ">", "&gt;");
                        stringData =
                            StringService.change (stringData, "<", "&lt;");
                    }
                    sbuff.append (stringData);
                    sbuff.append ('<');
                    sbuff.append ('/');
                    sbuff.append (columnNames[columnIndex]);
                    sbuff.append ('>');
                }
                sbuff.append (ersr);
            }
            sbuff.append ("</DocRoot>");
            dh = new DocumentHandler(sbuff.toString(),false);
            
            //for testing - caution large docs cause the toString method to fail
            //String xml = dh.toString();

            return dh;
        }
        catch (Exception e)
        {
            LOG.error("Exception in getXmlResultSet: " ,e);
            return null;
        }
    }



    /**
     * Moves data from a SQL result set to a Vector. Also implements chunking
     * by only moving the specified rows.
     *
     * @param rs The SQL result set to pull data from.
     * @param firstRow The first row of the SQL result set to save the data for
     * @param maxRows The maximum number of rows to pull from the SQL result set
     */
    private void populateStructure (ResultSet rs, int firstRow, int maxRows)
            throws SQLException
    {
        boolean containsRecords;
        
        try
        {
            int cols = columnNames.length;
            for (int j = 0;j < cols;j++)
            {
                results[j] = new Vector ();
            }
            int count = 0;
            int scrollCount =0;
            
            containsRecords = rs.next();
            while (scrollCount < firstRow && containsRecords)
            {
                count++;
                scrollCount++;
                containsRecords = rs.next();
            }
            int returnCount = 0;
            
            while ((returnCount < maxRows || maxRows == -1) && containsRecords)
            {
                for (int k = 0;k < cols;k++)
                {
                                                                                ResultSetMetaData resultsMetaData = rs.getMetaData ();
                                                                                Object rawColumnData = null;
                                                                                int sqlType = resultsMetaData.getColumnType(k + 1);
                                                                                rawColumnData = rs.getObject (k + 1);
                                                                                if(sqlType == Types.DATE && rawColumnData != null)
                                                                                {  
                                                                                                String date = rs.getDate(k + 1).toString();
                                                                                                String time = rs.getTime(k + 1).toString();
                                                                                                rawColumnData   = date+" "+time+".0";;
                                                                                }
                                                                                

                                                                                results[k].addElement (rawColumnData); 
                } 
                count++;
                returnCount++;
                containsRecords = rs.next();
            }
            while (containsRecords)
            {            
                count++;
                containsRecords = rs.next();
            }
            this.setReturnCount (returnCount);
            this.setRecordCount (count);
        }
        catch (Exception e)
        {
           LOG.error("Exception in populateStructure()", e);
        }
    }
    /**
     * Scrolls the ListView's current record pointer to the record index
     * supplied in the rowID argument. If the row is not valid, an exception
     * will be thrown.<p>
     *<p>
     * NOTE: This rowID index is a ZERO (0) based index.
     * 
     * @param rowID The row ID that is to be scrolled to.
     * @exception AmsException

     */
    public void scrollToRow (int rowID)
           throws AmsException
    {
        this.rowPointer = rowID;
    }
    /**
     * Set the RecordCount property based on the value of the "count" argument.
     *
     * @param count The number of records retrieved
     */
    protected void setRecordCount (int count)
    {
        this.RecordCount = count;
    }
    /**
     * Set the Return property based on the value of the "count" argument.
     *
     * @param count The number of records retrieved
     */
    protected void setReturnCount (int count)
    {
        this.returnCount = count;
    }
    // -------------------------------------------------//
    // EJB Methods                                      //
    // -------------------------------------------------//
    
    // Context Method
    public void setSessionContext (SessionContext pContext)
    {
        // Get the original SQL Statement
        String stringPropValue = null;
        Context ctx = null;
        
        // Check the EJBEnvironmentProperties to see if we can get the
        // cached values
        Hashtable ejbEnvironment =
            EJBEnvironmentProperties.getEnvironment (this.getClass().toString());
        
        if (ejbEnvironment != null)
        {
            this.SQLStatement = (String)ejbEnvironment.get ("sqlStatement");
        }
        else
        {
            ejbEnvironment = new Hashtable ();
            
            try
            {
                ctx = GenericInitialContextFactory.getInitialContext ();
            }
            catch (AmsException amsEx)
            {}
            try
            {
                stringPropValue =
                    (String) ctx.lookup ("java:/comp/env/sqlStatement");
                if (stringPropValue != null)
                {
                    this.SQLStatement = stringPropValue;
                    ejbEnvironment.put ("sqlStatement", stringPropValue);
                }
            }
            catch (Exception e)
            {
            	LOG.error("Exception in setSessionContext()", e);
            }
            EJBEnvironmentProperties.putEnvironment (
                this.getClass ().toString (), ejbEnvironment);
        }
    }
}
