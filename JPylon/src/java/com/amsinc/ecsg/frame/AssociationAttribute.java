package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class AssociationAttribute extends NumberAttribute{
    protected String associationObjectName;
    
	public AssociationAttribute ()
	 {
     }

	public AssociationAttribute (String name, String physicalName, String associationName)
	 {
	    super(name, physicalName);
	    setAssociationObjectName(associationName);
     }

	public AssociationAttribute (String name, String physicalName, String associationName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	    setAssociationObjectName(associationName);
	 }

    public void setAssociationObjectName(String objectName){
        this.associationObjectName = objectName;
    }
    
    public String getAssocationObjectName(){
        return this.associationObjectName;
    }
}   