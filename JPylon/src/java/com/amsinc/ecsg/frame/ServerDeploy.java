/*
 * @(#)ServerDeploy
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import javax.ejb.*;
import java.rmi.*;
import java.util.*;

/**
 * ServerDeploy is used as an initialization or '.ini' file.
 * Parameters that are needed to deploy the framework are put into the deployment descriptor
 * of this EJB component, and the corresponding framework components use this bean to
 * lookup those intialization parameters.  For example, the error management component
 * uses ServerDeploy to look up the name of the error code database table which should be
 * specified in the deployment descriptor of ServerDeploy.
 */
public interface ServerDeploy extends javax.ejb.EJBObject
{
  public String getInitializationParameter(String parmName)
            throws RemoteException;
  public void reloadReferenceData()
            throws RemoteException;
  public void init(String serverLocation)
            throws RemoteException;
  public Object updateCache(String updateableClass)
            throws RemoteException;
  public Date getServerDate() throws RemoteException, AmsException;
  public Date getServerDate(boolean override) throws RemoteException, AmsException;
 
}
