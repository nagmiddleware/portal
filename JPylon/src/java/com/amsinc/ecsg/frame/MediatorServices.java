/*
 * @(#)MediatorServices
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import javax.ejb.*;
import java.rmi.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import com.amsinc.ecsg.util.*;

/**
 * MediatorServices contains data and methods that are common to various portions of a Mediator implementation. Mediators are
 * stateless, so this class bundles a mediator's necessary components.
 */

public class MediatorServices implements java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(MediatorServices.class);
	/** Contains locale information that can be accessed by the mediator */
	public ClientServerDataBridge csdb = null;
	/** Contains error information for the mediator */
	public ResourceManager resMgr;
	public ErrorManager errorMgr;
	/** Contains the max error severity for the mediator */
	public int mediatorMaxErrorSeverity = 0;
	/** A vector of EJBs that are accessed by the mediator */
	public Vector ejbObjList;
	/** The Session Context for the Mediator */
	public SessionContext mContext;
	/** An XML document containing errors encountered in the mediator */
	public DocumentHandler errorDoc;
	public int errorId = 1;
	boolean isTransactional = false;
	boolean transSuccess = true;
	public boolean debuggingOn = false;
	public int debuggingLevel = 0;

	// The default constructor for this class
	public MediatorServices() {
		errorMgr = new ErrorManager();
		ejbObjList = new Vector();
		errorDoc = new DocumentHandler();
		errorDoc.setAttributeInt("/maxerrorseverity", this.mediatorMaxErrorSeverity);
	}

	/**
	 * This method checks if debugging has been turned on.
	 *
	 * @return A value of <code>true</code> if debugging is on, a value of <code>false</code> if it is not.
	 */
	public boolean isDebuggingOn() {
		return debuggingOn;
	}

	/**
	 * This method is the getter for Client Server Data Bridge
	 *
	 * @return The current csdb value
	 */
	public ClientServerDataBridge getCSDB() {
		return this.csdb;
	}

	/**
	 * Returns a handle to the resource manager for this business object
	 *
	 * @return ResourceManager
	 */
	public ResourceManager getResourceManager() throws RemoteException {
		if (this.resMgr == null) {
			this.resMgr = new ResourceManager(this.csdb);
		}
		return this.resMgr;
	}

	/**
	 * This method is the getter for Error Document
	 *
	 * @return The current Error Document value
	 */
	public ErrorManager getErrorManager() {
		return this.errorMgr;
	}

	/**
	 * This method is the getter for Max Error Severity
	 *
	 * @return The current MaxErrorSeverity value
	 */
	public int getMediatorMaxErrorSeverity() {
		return this.mediatorMaxErrorSeverity;
	}

	/**
	 * This method is the getter for isTransactional
	 *
	 * @return The current isTransactional value
	 */
	public boolean getIsTransactional() {
		return this.isTransactional;
	}

	/**
	 * This method is the getter for Transaction Success
	 *
	 * @return The current Transaction Success value
	 */
	public boolean getTranSuccess() {
		return this.transSuccess;
	}

	/**
	 * This method is the getter for Error Document
	 *
	 * @return The current Error Document value
	 */
	public DocumentHandler getErrorDoc() {
		return this.errorDoc;
	}

	/**
	 * This method is the setter for whether debugging is on or off.
	 *
	 * @param debug
	 *            A value of <code>true</code> indicates debugging is on, a value of <code>false</code> indicates debugging is off.
	 */
	public void setDebuggingOn(boolean debug) {
		debuggingOn = debug;
	}

	/**
	 * This method is the setter for the debugging level.
	 *
	 * @param level
	 *            The level to assign to the debugging level.
	 */
	public void setDebuggingLevel(int level) {
		debuggingLevel = level;
	}

	/**
	 * This method is the setter for Client Server Data Bridge.
	 *
	 * @param csdb
	 *            The transaction success value to be set
	 */
	public void setCSDB(ClientServerDataBridge csdb) {
		this.csdb = new ClientServerDataBridge(csdb);
	}

	/**
	 * This method is the setter for Error Manager
	 *
	 * @param errorMgr
	 *            The Error Manager value to be set
	 */
	public void setErrorMananger(ErrorManager errorMgr) {
		this.errorMgr = errorMgr;
	}

	/**
	 * This method is the setter for Max Error Severity
	 *
	 * @param mediatorMaxErrorSeverity
	 *            The Max Error Severity value to be set
	 */
	public void setMediatorMaxErrorSeverity(int mediatorMaxErrorSeverity) {
		this.mediatorMaxErrorSeverity = mediatorMaxErrorSeverity;
	}

	/**
	 * This method is the setter for Session Context
	 *
	 * @param context
	 *            The Session Context value to be set
	 */
	public void setSessionContext(SessionContext context) {
		this.mContext = context;
	}

	/**
	 * This method is the setter for isTransactional
	 *
	 * @param isTran
	 *            The transaction isTransactional to be set
	 */
	public void setIsTransactional(boolean isTran) {
		this.isTransactional = isTran;
	}

	/**
	 * This method is the setter for Transaction Success
	 *
	 * @param tranSuccess
	 *            The transaction success value to be set
	 */
	public void setTranSuccess(boolean tranSuccess) {
		this.transSuccess = tranSuccess;
	}

	/**
	 * This method adds the errors from EJBs found in error list to the error manager of the mediator. It then removes all of the
	 * EJBs.
	 *
	 */
	public void cleanUpEjbObjList() throws RemoteException {
		// Clean up registered business objects
		// This includes adding error information and removing the EJB
		Enumeration enumer = ejbObjList.elements();
		while (enumer.hasMoreElements()) {
			try {
				EJBObject ejbObj = (EJBObject) enumer.nextElement();
				if (ejbObj instanceof BusinessObject) {
					BusinessObject busObj = (BusinessObject) ejbObj;
					this.addErrorInfo(busObj.getIssuedErrors());
				}
				try {
					LOG.debug("Mediator removing EJB: {}", ejbObj.getClass());
					ejbObj.remove();
				} catch (RemoveException e) {
				}
			} catch (NoSuchObjectException nsoe) {
				// Catch this in case the bean has already been removed
				LOG.error("Exception in cleanUpEjbObjList: ", nsoe);
			}
		}
	}

	/**
	 * This method saves all of the EJBs allocated using this class
	 *
	 */
	public void saveAll() throws RemoteException, AmsException {
		// Clean up registered business objects
		// This includes adding error information and removing the EJB
		Enumeration enumer = ejbObjList.elements();
		while (enumer.hasMoreElements()) {
			try {
				EJBObject ejbObj = (EJBObject) enumer.nextElement();
				if (ejbObj instanceof BusinessObject) {
					BusinessObject busObj = (BusinessObject) ejbObj;
					busObj.save();
				}
			} catch (NoSuchObjectException nsoe) {
				LOG.error("Exception in saveALL: ", nsoe);
			}
		}
	}

	/**
	 * This method obtains a handle to an EJB business object component and automatically registers the business object with the
	 * Mediator This should be done so that the Mediator can, after the completion of the transaction, add the error information to
	 * the output document and remove the EJB component.
	 *
	 * @param objectName
	 *            The object name of the EJB to be created
	 */
	public EJBObject createServerEJB(String objectName) throws AmsException, RemoteException {
		EJBObject ejbObject = EJBObjectFactory.createServerEJB(this.mContext, objectName, this.getCSDB());
		ejbObjList.addElement(ejbObject);
		return ejbObject;
	}

	/**
	 * Remove EJB so that we can conserve the # of EJBs in the system. Add the error info before removing.
	 * 
	 * @param objectName
	 * @return
	 * @throws AmsException
	 * @throws RemoteException
	 */
	public void removeServerEJB(EJBObject ejbObject) throws AmsException, RemoteException {
		if (ejbObject instanceof BusinessObject) {
			BusinessObject busObj = (BusinessObject) ejbObject;
			this.addErrorInfo(busObj.getIssuedErrors());
		}
		try {
			ejbObject.remove();
		} catch (RemoveException e) {
		}
		ejbObjList.remove(ejbObject);

	}

	/**
	 * This method obtains a handle to an EJB business object component and automatically registers the business object with the
	 * Mediator This should be done so that the Mediator can, after the completion of the transaction, add the error information to
	 * the output document and remove the EJB component.
	 *
	 * @param objectName
	 *            The object name of the EJB to be created
	 * @param oid
	 *            The unique id of the EJB to return
	 */
	public EJBObject createServerEJB(String objectName, long oid) throws AmsException, RemoteException {
		EJBObject ejbObject = EJBObjectFactory.createServerEJB(this.mContext, objectName, oid, this.getCSDB());
		ejbObjList.addElement(ejbObject);
		return ejbObject;
	}

	/**
	 * This method adds the mediators list of errors and adds the information to the overall Mediator XML document to be returned to
	 * the client.
	 *
	 */
	public void addErrorInfo() {
		this.addErrorInfo(errorMgr.getIssuedErrorsList());

	}

	/**
	 * This method takes a list of errors and adds the information to the overall Mediator XML document to be returned to the
	 * client.
	 *
	 * @param list
	 *            A collection of IssuedError objects
	 */
	public void addErrorInfo(List<IssuedError> list) {
		int severity = 0;
		int maxErrorSeverity = 0;

		for (IssuedError err : list) {
			severity = err.getSeverity();
			if (severity > maxErrorSeverity)
				maxErrorSeverity = severity;

			String code = err.getErrorCode();
			String msg = err.getErrorText();

			// If this is an infrastructure message that signifies an update,
			// put the corresponding information in the XML document
			// This will notify the web server Java Bean infrastructure
			// that a bean has invalid data.
			if (code.equals("INF99")) {
			} else {
				errorDoc.setAttribute("/errorlist/error(" + errorId + ")/code", code);
				errorDoc.setAttribute("/errorlist/error(" + errorId + ")/message", msg);
				errorDoc.setAttributeInt("/errorlist/error(" + errorId + ")/severity", severity);
				errorId++;
			}
		}
		if (maxErrorSeverity > this.mediatorMaxErrorSeverity) {
			this.mediatorMaxErrorSeverity = maxErrorSeverity;
			errorDoc.setAttributeInt("/maxerrorseverity", maxErrorSeverity);
		}
	}

	/**
	 * This method logs debugging messages if the object is currently in the "Debug" mode. This mode is set in the Mediators's
	 * deployment descriptor file. The descriptor's environment variable "debugMode" indicates if debugging is on for the the EJB
	 * component.
	 *
	 * @param debugMessage
	 *            The debugger message that will be logged
	 */
	public void debug(String debugMessage) {
		if (this.debuggingOn) {

			// Call the Debugger's debug method
			LOG.debug(this.getClass().getName(), this.debuggingLevel, debugMessage);
		}
	}
}
