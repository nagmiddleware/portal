package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class ObjectIDAttribute extends NumberAttribute {
	public ObjectIDAttribute ()
	 {
     }

	public ObjectIDAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public ObjectIDAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }
}
