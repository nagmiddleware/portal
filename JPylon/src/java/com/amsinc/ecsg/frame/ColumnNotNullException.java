package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class ColumnNotNullException extends DataObjectException{
    public ColumnNotNullException(){
        super("Column Not Null Error encountered in DataObject Processing");
        this.vendorErrorCode = 1400;
    }
}