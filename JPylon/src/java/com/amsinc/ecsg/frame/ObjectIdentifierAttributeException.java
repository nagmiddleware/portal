package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class ObjectIdentifierAttributeException extends AmsException{
    public ObjectIdentifierAttributeException(){super("Attributes that are registered as ObjectIdentifier attributes cannot be set.");}
    public ObjectIdentifierAttributeException(String attributeName){super("The attribute "+attributeName+" has been registered as an ObjectIdentifier attribute and cannot be set");}
}
