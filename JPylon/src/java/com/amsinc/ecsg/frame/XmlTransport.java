package com.amsinc.ecsg.frame;

/*
 * @(#)XmlTransport
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.web.*;
import java.io.*;
import java.util.*;

import javax.servlet.http.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.*;
import java.text.MessageFormat;
import javax.crypto.SecretKey;

/**
 * XmlTransport encapsulates the logic required to package http parameters into an XML document and invoke an Enterprise Java Bean
 * component. This class is used primarily by AmsServlet for this purpose, but can be used by other EJB clients.
 */
public class XmlTransport {

	private static final Logger LOG = LoggerFactory.getLogger(XmlTransport.class);
	/**
	 * An indicator stating whether jsp:forward is being used. If yes, then the BeanManager is informed of the next page, and the
	 * JSP does a jsp:forward. If no, then this servlet does a sendRedirect method call.
	 */
	private boolean jspForwardOn = true;

	/**
	 * The default constructor. Used when no debugging output is desired.
	 *
	 */
	public XmlTransport() {

	}

	/**
	 * Constructor for use with the given debugger object and navigation turned on.
	 *
	 * @param debugger
	 *            the debugger object to use
	 * @param jspforwardOn
	 */
	public XmlTransport(boolean jspForwardOn) {

		this.jspForwardOn = jspForwardOn;
	}

	/**
	 * This method creates an XML document composed of the input parameters to the transaction and the servlet call. If the callEjb
	 * parameter is set to yes, the specified EJB component is invoked. The output document is then stored in the XML document
	 * cache.
	 *
	 * @param reqInfo
	 *            the infrastructure request information object
	 * @param session
	 *            the servlet session object for this user's session
	 * @param beanMgr
	 *            a reference to the BeanManager infrastructure component
	 * @param formMgr
	 *            a reference to the FormManager infrastructure component
	 * @param resMgr
	 *            a reference to the ResourceManager infrastructure component
	 * @param serverLocaiton
	 *            the EJB server location
	 * @param request
	 *            the servlet request object
	 * @param response
	 *            the servlet response object
	 * @param inputParmMap
	 *            the map of input parameters for the transaction
	 */
	public DocumentHandler callEjbComponent(AmsServletInvocation reqInfo, BeanManager beanMgr, FormManager formMgr,
			ResourceManager resMgr, String serverLocation, HttpServletRequest request, HttpServletResponse response,
			Hashtable inputParmMap) throws AmsException, RemoveException, IOException {

		boolean bErrorRedirect = false;

		// Create input document or get cached input document to send to EJB
		// This document will be particular to this user's session
		DocumentHandler inputDocument = formMgr.getFromDocCache(reqInfo.getDocCacheName());

		if (inputDocument == null) {
			LOG.debug("XmlTransport No input document exists yet.");
			inputDocument = new DocumentHandler();
		}

		if (!(reqInfo.isAddToInputCache())) {
			LOG.debug("XmlTransport Creating a new input document");
			inputDocument.removeAllChildren("/In");
		}

		// Add the input parameters to the document
		inputDocument = createDocument(request, inputParmMap, inputDocument, reqInfo, beanMgr, formMgr);

		// Declare the output document variable. Initialize to the input document
		// so the case where we do not call an ejb component is covered.
		DocumentHandler outputDocument = inputDocument;

		// Cache document for possible future use
		formMgr.storeInDocCache(reqInfo.getDocCacheName(), outputDocument);

		// Call the WebAction Modules
		if (reqInfo.getPreMedWebAction() != null) {
			// beansAsInput parameter can have multiple values seperated by a
			// backslash
			StringTokenizer tokenizer = new StringTokenizer(reqInfo.getPreMedWebAction(), "/");
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				LOG.debug("XmlTransport Pre Mediator Processing WebAction {} InputDoc==> {}", token, inputDocument);
				// Create a class instance and invoke
				try {
					Class webActionClass = Class.forName(token);
					WebAction webAction = (WebAction) webActionClass.newInstance();
					webAction.act(reqInfo, beanMgr, formMgr, resMgr, serverLocation, request, response, inputParmMap, inputDocument);
				} catch (Exception e) {
					LOG.error("Exception occured instantiating PreMedWebAction class",e);
				}
			}
		}

		// Determine if we should cache this data and return to client
		// This occurs in the case of a multiple part form the user must fill in.
		if (reqInfo.isCallEjb()) {

			if ((inputDocument.getAttribute("/Error/maxerrorseverity") != null)
					&& (Integer.parseInt(inputDocument.getAttribute("/Error/maxerrorseverity")) >= ErrorManager.ERROR_SEVERITY)) {
				outputDocument = inputDocument;
			} else {
				// Grab the CSDB from the ResourceManager if it exists.
				ClientServerDataBridge csdb = null;
				if (resMgr != null) {
					csdb = resMgr.getCSDB();
				}
				// Lookup the EJB component to call

				Mediator med = null;
				if (beanMgr.getUserLoginInfo() == null) {
					med = (Mediator) EJBObjectFactory.createClientEJB(serverLocation, reqInfo.getMediator());
				} else // we are using EJB security
				{
					med = (Mediator) EJBObjectFactory.createSecureClientEJB(serverLocation, reqInfo.getMediator(),
							beanMgr.getUserLoginInfo().getLoginName(), beanMgr.getUserLoginInfo().getLoginPassword());
				}

				// Invoke EJB component
				outputDocument = med.performService(inputDocument, csdb);

				// Free up EJB component for other users
				med.remove();
			}
			// Determine if an error occured and we should redirect this request
			// to an error display page.
			String errorSeverity = outputDocument.getAttribute("/Error/maxerrorseverity");
			if (errorSeverity != null) {
				if (Integer.parseInt(errorSeverity) >= ErrorManager.ERROR_SEVERITY) {
					// Redirect the web request to the specified error page
					bErrorRedirect = true;
					// If navigation management is in use, set the next page on the
					// BeanManager. The JSP will do a jsp:forward. Otherwise, call the servlet redirect API here.
					if (jspForwardOn) {
						if (reqInfo.getErrorPage() != null) {
							formMgr.setCurrPage(reqInfo.getErrorPage(), reqInfo.getAdvanceCurrentPage());
						} else {
							formMgr.setCurrPage(NavigationManager.getNavMan().getExceptionPage(formMgr.getOriginalPage()), true);
						}
					} else {
						if (reqInfo.getErrorPage() != null) {
							formMgr.setCurrPage(reqInfo.getErrorPage(), true);
							String physicalPage = NavigationManager.getNavMan().getPhysicalPage(reqInfo.getErrorPage(), request);
							response.sendRedirect(response.encodeRedirectUrl(physicalPage));
						} else {
							String nextPage = NavigationManager.getNavMan().getExceptionPage(formMgr.getOriginalPage());
							formMgr.setCurrPage(nextPage, true);
							String physicalPage = NavigationManager.getNavMan().getPhysicalPage(nextPage, request);
							response.sendRedirect(response.encodeRedirectUrl(physicalPage));
						}
					}
				}
			}
		}

		// Cache document for possible future use
		formMgr.storeInDocCache(reqInfo.getDocCacheName(), outputDocument);

		// Determine if there are web beans that we should populate from the input data
		if ((reqInfo.getBeansToPopulate() != null) && (!(reqInfo.isCallEjb())) && (beanMgr != null)) {
			DocumentHandler inputSection = inputDocument.getComponent("/In");
			try {
				StringTokenizer tokenizer = new StringTokenizer(reqInfo.getBeansToPopulate(), "/");
				while (tokenizer.hasMoreTokens()) {
					String token = tokenizer.nextToken();
					LOG.debug("XmlTransport Processing BeanToPopulate {}", token);
					AmsWebBean wb = (AmsWebBean) beanMgr.getBean(token);
					if (!(wb.populateFromXmlDoc(inputSection))) {
						wb.populateFromXmlDoc(inputSection, "/");
					}
				}
			} catch (Exception e) {
			}
		}

		// Call the WebAction Modules
		if (reqInfo.getPostMedWebAction() != null) {
			// beansAsInput parameter can have multiple values seperated by a backslash
			StringTokenizer tokenizer = new StringTokenizer(reqInfo.getPostMedWebAction(), "/");
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				LOG.debug("XmlTransport Post Mediator Processing WebAction {}", token);
				// Create a class instance and invoke
				try {
					Class webActionClass = Class.forName(token);
					WebAction webAction = (WebAction) webActionClass.newInstance();
					webAction.act(reqInfo, beanMgr, formMgr, resMgr, serverLocation, request, response, inputParmMap,
							inputDocument);
				} catch (Exception e) {
					LOG.error("Exception occured instantiating PostMedWebAction class",e);
				}
			}
		}

		// Determine if we should redirect this request
		if (!bErrorRedirect) {
			// If navigation management is in use, set the next page on the
			// BeanManager. The JSP will do a jsp:forward. Otherwise, call the servlet redirect API here.
			if (jspForwardOn) {
				if (reqInfo.getNextPage() != null) {
					formMgr.setCurrPage(reqInfo.getNextPage(), reqInfo.getAdvanceCurrentPage());
				} else {
					formMgr.setCurrPage(NavigationManager.getNavMan().getExceptionPage(formMgr.getOriginalPage()), true);
				}
			} else {
				if (reqInfo.getNextPage() != null) {
					formMgr.setCurrPage(reqInfo.getNextPage(), reqInfo.getAdvanceCurrentPage());
					String physicalPage = NavigationManager.getNavMan().getPhysicalPage(reqInfo.getNextPage(), request);
					response.sendRedirect(response.encodeRedirectUrl(physicalPage));
				} else {
					String nextPage = NavigationManager.getNavMan().getExceptionPage(formMgr.getOriginalPage());
					formMgr.setCurrPage(nextPage, true);
					String physicalPage = NavigationManager.getNavMan().getPhysicalPage(nextPage, request);
					response.sendRedirect(response.encodeRedirectUrl(physicalPage));
				}
			}
		}

		// Return the output document in case the client wants it
		return outputDocument;
	}

	/**
	 * Using the input parameter map, create an XML document from the input parameters.
	 *
	 * @param request
	 *            the servlet request object
	 * @param inputParmMap
	 *            the map of input parameters for the transaction
	 * @param masterDoc
	 *            the overall Mediator XML document
	 * @param reqInfo
	 *            the infrastructure request information object
	 * @param beanMgr
	 *            a reference to the BeanManager infrastructure component
	 * @param formMgr
	 *            a reference to the FormManager infrastructure component
	 */
	public DocumentHandler createDocument(HttpServletRequest request, Hashtable inputParmMap, DocumentHandler masterDoc,
			AmsServletInvocation reqInfo, BeanManager beanMgr, FormManager formMgr) throws AmsException {
		DocumentHandler inputDoc;

		// Check if the document has already been created. If not, create it.
		if (masterDoc == null) {
			masterDoc = new DocumentHandler();
		}

		// Check if the input section has already been created. If not, create it.
		if ((inputDoc = masterDoc.getComponent("/In")) == null) {
			inputDoc = new DocumentHandler();
		}

		// Determine if there are document cache entries we should get input data
		// from. If there are, copy their input and output sections into the
		// new document's input section.
		if ((reqInfo.getDocCacheAsInput() != null) && (formMgr != null)) {
			// docCacheAsInput parameter can have multiple values seperated by
			// a backslash
			StringTokenizer tokenizer = new StringTokenizer(reqInfo.getDocCacheAsInput(), "/");
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				LOG.debug("XmlTransport Processing docCacheAsInput {}", token);
				DocumentHandler doc = formMgr.getFromDocCache(token);
				if (doc != null) {
					inputDoc.mergeDocument("/", doc.getComponent("/In"));
					inputDoc.mergeDocument("/", doc.getComponent("/Out"));
				}
			}
		}

		// Determine if there are web beans we should get input data from
		if ((reqInfo.getBeansAsInput() != null) && (beanMgr != null)) {
			// beansAsInput parameter can have multiple values seperated by a backslash
			StringTokenizer tokenizer = new StringTokenizer(reqInfo.getBeansAsInput(), "/");
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				LOG.debug("XmlTransport Processing BeanAsInput {}", token);
				// Put the bean data into the input document
				AmsWebBean wb = (AmsWebBean) beanMgr.getBean(token);
				if (wb != null) {
					wb.populateXmlDoc(inputDoc);
				}
			}
		}

		putParmsIntoDoc(request, inputParmMap, inputDoc, reqInfo);

		// Determine if there are more input parameters to put into the document
		if (reqInfo.getMapFiles() != null) {
			// addlInput parameter can have multiple values seperated by a backslash
			StringTokenizer tokenizer = new StringTokenizer(reqInfo.getMapFiles(), "/");
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				LOG.debug("XmlTransport Processing Map File {}", token);
				// Get the input parameter information for this particular transaction
				MediatorInfoFactory fact = MediatorInfoFactory.getMediatorInfoFactory();
				MediatorInfo medInfo = fact.getMediatorInfo(token);
				Hashtable addlInputParmMap = medInfo.getInputParmMap().getParameterXmlPairs();
				putParmsIntoDoc(request, addlInputParmMap, inputDoc, reqInfo);
			}
		}

		// Set the input data as a document under the /In path in the master XML document
		masterDoc.setComponent("/In", inputDoc);
		LOG.debug("XmlTransport.createDocument() returns {}", inputDoc); // W Zhu 4/7/12 Rel 8.0 Add debugging
		return masterDoc;
	}

	/**
	 * This method actually does the work of creating the XML input document.
	 *
	 * @param request
	 *            the servlet request object
	 * @param inputParmMap
	 *            the map of input parameters for the transaction
	 * @param inputDoc
	 *            the input section of the overall Mediator XML document
	 * @param reqInfo
	 *            the infrastructure request information object
	 */
	public void putParmsIntoDoc(HttpServletRequest request, Hashtable inputParmMap, DocumentHandler inputDoc,
			AmsServletInvocation reqInfo) throws AmsException {
		// W Zhu 8/17/2012 Rel 8.1 T36000004579 add SecretKey parameter to allow different keys for each session for better security.
		Object userSession = request.getSession().getAttribute("userSession");
		SecretKey secretKey = null;
		try {
			java.lang.reflect.Method getSecretKey = userSession.getClass().getMethod("getSecretKey", new Class[] {});
			secretKey = ((SecretKey) getSecretKey.invoke(userSession, new Object[] {}));
		} catch (Exception e) {
			LOG.error("Exception getting secretkey from userSession",e);
		}

		// Iterate through the parameter list extracting each value out of
		// the request object and putting it into the appropriate path in the XML document
		Enumeration enumer = inputParmMap.keys();
		while (enumer.hasMoreElements()) {
			String parmName = (String) enumer.nextElement();
			DocumentMapRecord docMapRecord = (DocumentMapRecord) inputParmMap.get(parmName);
			String xmlPath = docMapRecord.getDocumentPath();
			String encryptionType = docMapRecord.getEncryptionType();
			if (xmlPath == null) {
				throw new AmsException("Internal hashtable problem.  Couldn't find " + parmName + " in hashtable.");
			}
			String[] parmValues = request.getParameterValues(parmName);
			if (parmValues == null) {
				// We do this case to handle what we think is a bug in the web
				// server. The getParameterValues method doesn't return anything
				// for a <SERVLET> PARAM tag, however, the singular getParameter
				// method does find something.
				String parmValue = request.getParameter(parmName);
				if (parmValue == null) {
					// Look in servlet invocation object for parameter values
					try {
						parmValue = reqInfo.getAddlParm(parmName);
						if (parmValue == null) {
							// Multiple objects can only be found on html form, so just
							// look for parameter values
							if (reqInfo.isMultipleObjects()) {
								// If there can be more than one object in the input, look
								// for attr1, attr2, ... where the name is attr
								int parmCount = 0;
								Object[] args = { parmCount };
								parmValue = request.getParameter(parmName.concat(args[0].toString()));
								// Mangle the given xmlPath to include an id value
								// We will take the second lowest directory path which we
								// assume is the name of the list component and add the
								// id to that
								int index = xmlPath.lastIndexOf('/');
								String xmlParmName = xmlPath.substring(index);
								StringBuilder msgFormatString = new StringBuilder(xmlPath.substring(0, index));
								msgFormatString.append("({0})");
								msgFormatString.append(xmlParmName);
								MessageFormat msgFormat = new MessageFormat(msgFormatString.toString());
								if (reqInfo.getNumberOfMultipleObjects() > 0 || reqInfo.getNumberOfMultipleObjects1() > 0
										|| reqInfo.getNumberOfMultipleObjects2() > 0 || reqInfo.getNumberOfMultipleObjects3() > 0
										|| reqInfo.getNumberOfMultipleObjects4() > 0) // Leelavathi - 10thDec2012 - Rel8200 CR-737
								{
									// Leelavathi - 10thDec2012 - Rel8200 CR-737 - Start
									String filterForNumOfMulObj1 = "|PaymentTemplateGroupOid|UserAuthorizedTemplateGroupOid|paymentReportingCode2Oid|report2Code|"
											+ "report2ShortDescription|report2Description|PmtTermTenorDtlOid|PmtTermPercent|PmtTermAmount|"
											+ "PmtTermTenorType|PmtTermNumDaysAfter|PmtTermDaysAfterType|PmtTermMaturityDate|"
											+ "SpMarginCurr|SpThresholdAmount|SpRateType|SpMargin|SpCustomerMarginRuleOid|";
									String filterForNumOfMulObj2 = "|AdditionalReqDocOid|AdditionalReqDocInd|AdditionalReqDocName|"
											+ "AdditionalReqDocOriginals|AdditionalReqDocCopies|AdditionalReqDocText|";
									String filterForNumOfMulObj3 = "ExternalBankOid|OperationalBankOid|CorporateOrgOid|InvPayInstructionInd|";
									String filterForNumOfMulObj4 = "SecAddressOid|SecName|SecAddressLine1|SecAddressLine2|SecCity|SecStateProvince|"
											+ "SecPostalCode|SecAddressSeqNum|SecCountry|SecOwnershipLevel|"; // Rel9.0 -
																												// numberOfMultipleObjects
																												// for Address
																												// fields

									int listCount = 0;
									if (filterForNumOfMulObj1.contains(parmName)) {
										listCount = reqInfo.getNumberOfMultipleObjects1();
									} else if (filterForNumOfMulObj2.contains(parmName)) {
										listCount = reqInfo.getNumberOfMultipleObjects2();
									} // Kyriba CR 268 External Bank
									else if (filterForNumOfMulObj3.contains(parmName)) {
										listCount = reqInfo.getNumberOfMultipleObjects3();
									} // Rel9.0 - numberOfMultipleObjects for Address fields
									else if (filterForNumOfMulObj4.contains(parmName)) {
										listCount = reqInfo.getNumberOfMultipleObjects4();
									}

									// Vshah - 10/19/2010 - IR#VIUK101252424 - [BEGIN]
									if (listCount > 0) {
										while (parmCount < listCount) {
											// Leelavathi - 10thDec2012 - Rel8200 CR-737 - End
											if (parmValue != null) {
												if (encryptionType != null) {
													inputDoc.setAttribute(msgFormat.format(args),
															EncryptDecrypt.decryptStringUsingTripleDes(parmValue, secretKey));
												} else {
													inputDoc.setAttribute(msgFormat.format(args), parmValue);
												}
											}
											parmCount++;
											args[0] = parmCount;
											parmValue = request.getParameter(parmName.concat(args[0].toString()));
										}
									} // Leelavathi IR#T36000017115 17/05/2013 Rel-8.2 Begin
									else if (parmCount < reqInfo.getNumberOfMultipleObjects()) {

										// Vshah - 10/19/2010 - IR#VIUK101252424 - [END]
										while (parmCount < reqInfo.getNumberOfMultipleObjects()) {
											if (parmValue != null) {
												if (encryptionType != null) {
													inputDoc.setAttribute(msgFormat.format(args),
															EncryptDecrypt.decryptStringUsingTripleDes(parmValue, secretKey));
												} else {
													inputDoc.setAttribute(msgFormat.format(args), parmValue);
												}
											}
											parmCount++;
											args[0] = parmCount;
											parmValue = request.getParameter(parmName.concat(args[0].toString()));
										}

									} else {
										while (parmValue != null) {
											if (encryptionType != null) {
												inputDoc.setAttribute(msgFormat.format(args),
														EncryptDecrypt.decryptStringUsingTripleDes(parmValue, secretKey));
											} else {
												inputDoc.setAttribute(msgFormat.format(args), parmValue);
											}
											parmCount++;
											args[0] = parmCount;
											parmValue = request.getParameter(parmName.concat(args[0].toString()));
										}
									}
								} // Leelavathi IR#T36000017115 17/05/2013 Rel-8.2 End
								else {
									while (parmValue != null) {
										if (encryptionType != null) {
											inputDoc.setAttribute(msgFormat.format(args),
													EncryptDecrypt.decryptStringUsingTripleDes(parmValue, secretKey));
										} else {
											inputDoc.setAttribute(msgFormat.format(args), parmValue);
										}
										parmCount++;
										args[0] = parmCount;
										parmValue = request.getParameter(parmName.concat(args[0].toString()));
									}
								}
							} else {
								LOG.debug("XmlTransport Didn't find parm {}, but that is okay ...", parmName);
							}
						} else {
							int delimeterIndex = parmValue.indexOf(FormManager.ADDL_PARM_DELIMETER);
							if (delimeterIndex == -1) {
								if (encryptionType != null) {
									inputDoc.setAttribute(xmlPath,
											EncryptDecrypt.decryptStringUsingTripleDes(parmValue, secretKey));
								} else {
									inputDoc.setAttribute(xmlPath, parmValue);
								}
							} else {
								// Parse multiple values for input parameter into vector
								StringTokenizer tokenizer = new StringTokenizer(parmValue, FormManager.ADDL_PARM_DELIMETER);
								List<String> multipleParmValues = new ArrayList<>();
								while (tokenizer.hasMoreTokens()) {
									String token = tokenizer.nextToken();
									if (encryptionType != null) {
										multipleParmValues.add(EncryptDecrypt.decryptStringUsingTripleDes(token, secretKey));
									} else {
										multipleParmValues.add(token);
									}
								}

								inputDoc.setAttributes(xmlPath, multipleParmValues);
							}
						}
					} catch (Exception e) {
						LOG.debug("XmlTransport Exception getting parm {} from additional parameters", parmName);
					}
				} else {
					if (encryptionType != null) {
						inputDoc.setAttribute(xmlPath, EncryptDecrypt.decryptStringUsingTripleDes(parmValue, secretKey));
					} else {
						inputDoc.setAttribute(xmlPath, parmValue);
					}
				}
			} else {
				if (parmValues.length == 1) {
					// [BEGIN] PPX-050 - jkok - Added call to convert HTML encoded special characters to literal characters
					if (encryptionType != null) {
						String decryptedValue = EncryptDecrypt.decryptStringUsingTripleDes(parmValues[0], secretKey);
						decryptedValue = StringFunction.xssHtmlToChars(decryptedValue);
						inputDoc.setAttribute(xmlPath, decryptedValue);
					} else {
						inputDoc.setAttribute(xmlPath, StringFunction.xssHtmlToChars(parmValues[0]));
					}
				} else {
					List<String> parmValuesList = new ArrayList<>();
					for (String parmValue : parmValues) {
						if (encryptionType != null) {
							String decryptedValue = EncryptDecrypt.decryptStringUsingTripleDes(parmValue, secretKey);
							decryptedValue = StringFunction.xssHtmlToChars(decryptedValue);
							parmValuesList.add(decryptedValue);
						} else {
							parmValuesList.add(StringFunction.xssHtmlToChars(parmValue));
						}
					}
					// [END] PPX-050 - jkok
					inputDoc.setAttributes(xmlPath, parmValuesList);
				}
			}
		}
	}

}
