package com.amsinc.ecsg.frame;

import java.util.*;

/**
 * This class contains staic attributes and methods that hold data about
 * particular business objects.   The data is used to make checking
 * associations more efficient.
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */
public class AssociationCheckCache
 {
    // Hold the values in a hashtable, the key is the EJB name
    private static Hashtable objectIdPhysicalNames;
    private static Hashtable dataTableNames;

    static
     {
        objectIdPhysicalNames = new Hashtable (40);
        dataTableNames =  new Hashtable (40);
     }

    /**
     * Checks to see if data has already been placed into the cache for a 
     * particular business object type.
     *
     * @param objectName - the business object whose presence is being checked for
     * @return boolean true if data exists, false otherwise
     */
    public static boolean entryExists(String objectName)
     {
        return (objectIdPhysicalNames.get(objectName) != null) &&
               (dataTableNames.get(objectName) != null);
     }

     /**
      * Retrieves the physical name of the object id attribute of the business
      * object type passed in.
      *
      * @param objectName - the business object whose presence is being checked for   
      * @return String the physical name of the object id attribute
      */
    public static String getObjectIdPhysicalName(String objectName)
     {
        return (String) objectIdPhysicalNames.get(objectName);
     }

     /**
      * Retrieves the table name of the business
      * object type passed in.
      *
      * @param objectName - the business object whose presence is being checked for  
      * @return String the data table name   
      */
    public static String getDataTableName(String objectName)
     {
        return (String) dataTableNames.get(objectName);
     }

      /**
      * Creates an entry in the cache for a particular business object type
      *
      * @param ejbName - Name of the EJB for which data is being created
      * @param dataTableName - table name
      * @param idAttributePhysicalName - physical name of the id attribute
      */
   public static void createEntry(String ejbName, String dataTableName, String idAttributePhysicalName)
     {
         objectIdPhysicalNames.put(ejbName, idAttributePhysicalName);
         dataTableNames.put(ejbName, dataTableName);
     }
 }