package com.amsinc.ecsg.frame;

/*
 * @(#)JPylonDBConnection
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.sql.*;
import javax.sql.*;
import com.amsinc.ecsg.util.JPylonProperties;

/**
 * This class is used to obtain a database connection.  It abstracts the differences
 * between application servers.
 *

 * @version 1.0 
 */
public class JPylonDBConnection {
 
    
    /**
     * Obtains a database connection using the jPylon specified username
     * and password.
     *
     * @exception AmsException

     */
    public static Connection getConnection(boolean onServer) 
        throws AmsException
    {
            String dbmsParm = null;
            try {
                dbmsParm = JPylonProperties.getInstance().getString("dataSourceName");
                DataSource ds = DataSourceFactory.getDataSource (dbmsParm, onServer);
                return ds.getConnection ();
            } catch(SQLException e) {
                throw new AmsException (
                  "Error on connecting to Datasource in DataObject using DBMS parms: " +
                  dbmsParm + " Nested Exception: " + e.getMessage ());
            }

    }
}
