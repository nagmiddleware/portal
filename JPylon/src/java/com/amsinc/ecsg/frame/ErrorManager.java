/*
 * @(#)ErrorManager
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.util.StringService;

/**
 * ErrorManager manages the errors for a business object. It stores the collection of errors that are issued. An instance of this
 * object is created for each business object. When a transaction ends, the getIssuedErrors method is called to get the entire list
 * and send it back to the client. The ErrorManager can be reinitialized for performance reasons so the object can remain in memory.
 * <p>
 * An example of the use of this object is shown below:<BR></BR>
 * Data in the properties file:<BR></BR>
 * ERR01 = 'Cannot withdraw funds. Account balance {0} is insufficient'
 *
 * <P></P>
 * Example Application Code in a business object:
 * <P></P>
 * // Check if there is money in the account before a withdrawal is made. <BR>
 * </BR>
 * if (getAttributeInt ("account_balance") < amountForWithdrawal)<BR></BR>
 * getErrorManager ().issueError ("Account", "ERR01", getAttribute ("account_balance"));
 * <P></P>
 * The message which will be presented to the user for this error if the account balance is $100.00 and the withdrawal amount is
 * $200.00: 'Cannot withdraw funds. Account balance $100.00 is insufficient'
 *
 * The message text is read from property resource bundles based on the specified locale. There is one properties file for each
 * severity (ErrorMessages, WarningMessages, InfoMessages).
 * 
 * The error messages are cached in a static Hashtable for quick retrieval
 * 
 *
 */
public class ErrorManager implements java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(ErrorManager.class);

	public static final int FATAL_ERROR_SEVERITY = 9;
	public static final int SEVERE_ERROR_SEVERITY = 7;
	public static final int ERROR_SEVERITY = 5;
	public static final int WARNING_ERROR_SEVERITY = 3;
	public static final int INFO_ERROR_SEVERITY = 1;

	private static final long serialVersionUID = 1L;
	private static final String ERROR_BUNDLE = "ErrorMessages";
	private static final String WARNING_BUNDLE = "WarningMessages";
	private static final String INFO_BUNDLE = "InfoMessages";

	private final List<IssuedError> error_issued = new ArrayList<>();
	private int highest_severity_level_error = 0;

	// Contains all error message that have been issued so
	// far for this JVM. Keyed by code+localeName
	private static final Map<String, IssuedError> cachedMessages = new HashMap<>(1000);

	private String localeName = "";

	/**
	 * This method issues an application error identified by errorCode. The ErrorManager stores all issued errors internally. The
	 * list of issued errors can be retrieved with the getIssuedErrors() method. Substitution values may optionally be provided
	 * based on the error message text for the given error code. For each placeholder in the message text, a substitution value
	 * should be provided so that the eventual text to be displayed to the user is logical and makes sense. This form of issueError
	 * contains an array ; This array parameter contains a value of 'true' or 'false' which determines if the element at the
	 * corresponding index in the substitutionValues array should be wrapped in quotes.
	 * 
	 *
	 * @param category
	 *            Debugging category the message is for. This is used to determine which file the log message is written to.
	 * @param errorCode
	 *            The error code to issue
	 * @param substitutionValues
	 *            The collection of string substitution values
	 * @param wrapSubstitutionValues
	 *            array of boolean values which determine if substitutionValues should be wrapped with quotes for the error text
	 *            message
	 */
	public void issueError(String category, String errorCode, String[] substitutionValues, boolean[] wrapSubstitutionValues)
			throws AmsException {

		// Look the error up in the catalog
		IssuedError error = getIssuedError(getLocaleName(), errorCode, substitutionValues, wrapSubstitutionValues);

		// check if this error has the greatest error severity
		if (error.getSeverity() > getMaxErrorSeverity())
			setMaxErrorSeverity(error.getSeverity());

		// add this error to our hashtable
		error_issued.add(error);

		// check if we should log this error
		if (error.getLogInd().equalsIgnoreCase("Y")) {
			LOG.info("Logging issuedError: {} {}", category + " default" + error.getErrorText() , error.getSeverity());
		}

		// check if this is a severe error, if so throw SevereErrorException
		if (error.getSeverity() == ErrorManager.SEVERE_ERROR_SEVERITY) {
			throw new FatalErrorException(error);
		}

		// check if this is a fatal error, if so throw SevereErrorException
		if (error.getSeverity() == ErrorManager.FATAL_ERROR_SEVERITY) {
			throw new SevereErrorException(error);
		}

	}

	/**
	 * This method issues an application error identified by errorCode. The ErrorManager stores all issued errors internally. The
	 * list of issued errors can be retrieved with the getIssuedErrors() method. Substitution values may optionally be provided
	 * based on the error message text for the given error code. For each placeholder in the message text, a substitution value
	 * should be provided so that the eventual text to be displayed to the user is logical and makes sense.
	 *
	 * @param category
	 *            Debugging category the message is for. This is used to determine which file the log message is written to.
	 * @param errorCode
	 *            The error code to issue
	 * @param substitutionValues
	 *            The collection of string substitution values for the error text message
	 */
	public void issueError(String category, String errorCode, String[] substitutionValues) throws AmsException {
		boolean[] emptyArray = null;
		issueError(category, errorCode, substitutionValues, emptyArray);
	}

	/**
	 * This method issues an application error identified by errorCode. The ErrorManager stores all issued errors internally. The
	 * list of issued errors can be retrieved with the getIssuedErrors() method. Substitution values may optionally be provided
	 * based on the error message text for the given error code. For each placeholder in the message text, a substitution value
	 * should be provided so that the eventual text to be displayed to the user is logical and makes sense.
	 *
	 * @param category
	 *            Debugging category the message is for. This is used to determine which file the log message is written to.
	 * @param errorCode
	 *            The error code to issue
	 * @param sub1
	 *            A substitution value for the error text message
	 * @param sub2
	 *            A substitution value for the error text message
	 * @param sub3
	 *            A substitution value for the error text message
	 * @param sub4
	 *            A substitution value for the error text message
	 */
	public void issueError(String category, String errorCode, String sub1, String sub2, String sub3, String sub4)
			throws AmsException {
		String[] substitutionValues = { sub1, sub2, sub3, sub4 };
		boolean[] emptyArray = null;
		issueError(category, errorCode, substitutionValues, emptyArray);
	}

	/**
	 * This method issues an application error identified by errorCode. The ErrorManager stores all issued errors internally. The
	 * list of issued errors can be retrieved with the getIssuedErrors() method. Substitution values may optionally be provided
	 * based on the error message text for the given error code. For each placeholder in the message text, a substitution value
	 * should be provided so that the eventual text to be displayed to the user is logical and makes sense.
	 *
	 * @param category
	 *            Debugging category the message is for. This is used to determine which file the log message is written to.
	 * @param errorCode
	 *            The error code to issue
	 * @param sub1
	 *            A substitution value for the error text message
	 * @param sub2
	 *            A substitution value for the error text message
	 * @param sub3
	 *            A substitution value for the error text message
	 */
	public void issueError(String category, String errorCode, String sub1, String sub2, String sub3) throws AmsException {
		String[] substitutionValues = { sub1, sub2, sub3 };
		boolean[] emptyArray = null;
		issueError(category, errorCode, substitutionValues, emptyArray);
	}

	/**
	 * This method issues an application error identified by errorCode. The ErrorManager stores all issued errors internally. The
	 * list of issued errors can be retrieved with the getIssuedErrors() method. Substitution values may optionally be provided
	 * based on the error message text for the given error code. For each placeholder in the message text, a substitution value
	 * should be provided so that the eventual text to be displayed to the user is logical and makes sense.
	 *
	 * @param category
	 *            Debugging category the message is for. This is used to determine which file the log message is written to.
	 * @param errorCode
	 *            The error code to issue
	 * @param sub1
	 *            A substitution value for the error text message
	 * @param sub2
	 *            A substitution value for the error text message
	 */
	public void issueError(String category, String errorCode, String sub1, String sub2) throws AmsException {
		String[] substitutionValues = { sub1, sub2 };
		boolean[] emptyArray = null;
		issueError(category, errorCode, substitutionValues, emptyArray);
	}

	/**
	 * This method issues an application error identified by errorCode. The ErrorManager stores all issued errors internally. The
	 * list of issued errors can be retrieved with the getIssuedErrors() method. Substitution values may optionally be provided
	 * based on the error message text for the given error code. For each placeholder in the message text, a substitution value
	 * should be provided so that the eventual text to be displayed to the user is logical and makes sense.
	 *
	 * @param category
	 *            Debugging category the message is for. This is used to determine which file the log message is written to.
	 * @param errorCode
	 *            The error code to issue
	 * @param sub1
	 *            A substitution value for the error text message
	 */
	public void issueError(String category, String errorCode, String sub1) throws AmsException {
		String[] substitutionValues = { sub1 };
		boolean[] emptyArray = null;
		issueError(category, errorCode, substitutionValues, emptyArray);
	}

	/**
	 * This method issues an application error identified by errorCode. The ErrorManager stores all issued errors internally. The
	 * list of issued errors can be retrieved with the getIssuedErrors() method.
	 *
	 * @param category
	 *            Debugging category the message is for. This is used to determine which file the log message is written to.
	 * @param errorCode
	 *            The error code to issue
	 */
	public void issueError(String category, String errorCode) throws AmsException {
		String[] emptyArray = null;
		boolean[] emptyArray1 = null;
		issueError(category, errorCode, emptyArray, emptyArray1);
	}

	/**
	 * This method returns the highest severity level for all the errors that have been issued to this point in time. Please refer
	 * to the static constants defined in this class for the different error severity levels.
	 *
	 * @return The highest error severity level.
	 */
	public int getMaxErrorSeverity() {
		return highest_severity_level_error;
	}

	/**
	 * This method sets the highest severity level attribute.
	 *
	 * @param highest_severity_level_error
	 *            The error severity level
	 */
	private void setMaxErrorSeverity(int highest_severity_level_error) {
		this.highest_severity_level_error = highest_severity_level_error;
	}

	/**
	 * This method returns the count of issued errors.
	 *
	 * @return The error count.
	 */
	public int getErrorCount() {
		return error_issued.size();
	}

	/**
	 * This method returns the collection of IssuedError objects for all of the errors that have been issued.
	 * 
	 * @deprecated
	 * @return The list of issued errors.
	 */
	public Vector<IssuedError> getIssuedErrors() {
		return new Vector<>(getIssuedErrorsList());
	}

	/**
	 * This method returns the collection of IssuedError objects for all of the errors that have been issued.
	 * 
	 * @return The list of issued errors.
	 */
	public List<IssuedError> getIssuedErrorsList() {
		return error_issued;
	}

	/**
	 * This method adds the errors contained in a Vector of errors to the error manager's list of current errors. This method is
	 * typically used when a business object needs to aggregate errors from other non-compositional business objects.
	 * 
	 * @deprecated
	 * @param additionalErrors
	 *            The list of additional errors to be added.
	 */
	public void addErrors(Vector<IssuedError> additionalErrors) {
		// Add the additional errors passed in to the error manager's vector
		// of errors.
		addErrors(new ArrayList<>(additionalErrors));
	}

	/**
	 * This method adds the errors contained in a Vector of errors to the error manager's list of current errors. This method is
	 * typically used when a business object needs to aggregate errors from other non-compositional business objects.
	 * 
	 * @param additionalErrors
	 *            The list of additional errors to be added.
	 */
	public void addErrors(List<IssuedError> additionalErrors) {
		// Add the additional errors passed in to the error manager's vector
		// of errors.
		error_issued.addAll(additionalErrors);
	}

	/**
	 * This method initializes the ErrorManager object.
	 *
	 */
	public void initialize() {
		error_issued.clear();
		highest_severity_level_error = 0;
	}

	/**
	 * Getter for the local name assocaited with the error manager. This is used in the internationalization of error codes.
	 *
	 * @return The locale name
	 */
	public String getLocaleName() {
		return this.localeName;
	}

	/**
	 * Setter for the local name assocaited with the error manager. This is used in the internationalization of error codes. Note
	 * that the locale name should match one of the locales define in the error code database table.
	 *
	 * @param localeName
	 *            Value to set the localeName to.
	 */
	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}

	/**
	 * This method creates an IssuedError object for the given errorCode, and formats the message text with the passed in
	 * substitution parms. If the error code does not exist (first for the locale, and then for the default), an "error" is created
	 * using the errorCode as both the code and text with ERROR_SEVERITY.
	 * 
	 * @return com.amsinc.ecsg.frame.IssuedError
	 * @param localeName
	 *            java.lang.String
	 * @param errorCode
	 *            java.lang.String
	 * @param substitutionValues
	 *            java.lang.String[]
	 * @param wrapSubstitutionValues
	 *            boolean[]
	 */
	public static IssuedError getIssuedError(String localeName, String errorCode, String[] substitutionValues,
			boolean[] wrapSubstitutionValues) {

		// Look the error up in the catalog
		IssuedError error = findErrorCode(errorCode, localeName);

		// check that error is not null, otherwise create a default error code
		if (error == null) {
			error = findErrorCode(errorCode, "");
			if (error == null) {
				error = new IssuedError(errorCode, errorCode, ErrorManager.ERROR_SEVERITY, "N");
			}
		}

		// Wrap each substituion parm in single quotes.
		if (substitutionValues != null) {
			boolean wrapSubstitutionValue = true;
			int length = substitutionValues.length;
			for (int i = 0; i < length; i++) {
				// [BEGIN] PPX-050 - jkok
				substitutionValues[i] = StringFunction.xssCharsToHtml(substitutionValues[i]);
				// [END] PPX-050 - jkok
				try {

					// Determine if we should wrap the substitution value in
					// quotes
					if (wrapSubstitutionValues != null)
						wrapSubstitutionValue = wrapSubstitutionValues[i];

					// CR-486 LRUK060947155 Don't check substitutionValues[i] for empty string since it could be null.
					// if (!substitutionValues[i].equals("") &&
					if (wrapSubstitutionValue) {
						substitutionValues[i] = "'" + substitutionValues[i] + "'";
					}

				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.error("Exception occured in getIssuedError()", e);
				}
			}
		}

		String errorText = error.getErrorText();

		// Single quotes early in the error text will cause MessageFormat to ignore the
		// substitution markers ({0}, {1}). Replace each single quote with
		// two single quotes (see java.text.MessageFormat javadoc for information)
		// MessageFormat changes the double single quote back into a single quote in the final output
		if (errorText.contains("'")) {
			// Replace each ' with ''
			errorText = StringService.change(errorText, "'", "''");
		}

		// put substitution values into error message text
		error.setErrorText(MessageFormat.format(errorText, substitutionValues));

		return error;
	}

	/**
	 * This method retrieves a given error code. An issued error object is returned which contains the error text message, severity,
	 * and log indicator.
	 * 
	 * @param code
	 *            the error code to lookup
	 * @param localeName
	 *            the locale to look it up for
	 * @return the error object for the given error code
	 */
	public static IssuedError findErrorCode(String code, String localeName) {
		// First, look for it in the cached messages
		IssuedError message = cachedMessages.get(code + localeName);

		// If it's found, return it
		if (message != null)
			// Return a copy since it might be changed later
			return new IssuedError(message);

		// Build a locale object from the passed in locale
		Locale locale = null;

		if ((localeName != null) && !localeName.isEmpty())
			locale = new Locale(localeName.substring(0, 2), localeName.substring(3, 5));

		// Look for the message in the error message bundle
		message = getMessage(ERROR_BUNDLE, code, locale, ERROR_SEVERITY);

		// If found, add the object to the hashtable
		if (message != null) {
			cachedMessages.put(code + localeName, message);
			return new IssuedError(message);
		}

		// Look for the message in the warning message bundle
		message = getMessage(WARNING_BUNDLE, code, locale, WARNING_ERROR_SEVERITY);

		// If found, add the object to the hashtable
		if (message != null) {
			cachedMessages.put(code + localeName, message);
			return new IssuedError(message);
		}

		// Look for the message in the information message bundle
		message = getMessage(INFO_BUNDLE, code, locale, INFO_ERROR_SEVERITY);

		// If found, add the object to the hashtable
		if (message != null) {
			cachedMessages.put(code + localeName, message);
			return new IssuedError(message);
		}

		// No error message found. Return null
		return null;
	}

	/**
	 * Looks for a message in the bundle file passed in for the specified locale.
	 *
	 * @param bundleName
	 *            - the property resource bundle where the message is expected to be
	 * @param code
	 *            - the code to determine which message to look up
	 * @param locale
	 *            - the locale to look it up for
	 * @param severity
	 *            - the type of message to create: error, warning, information
	 * @return IssuedError - the error object
	 */
	private static IssuedError getMessage(String bundleName, String code, Locale locale, int severity) {
		PropertyResourceBundle bundle;
		String messageText;

		// Get the bundle (passing in locale if appropriate)
		if (locale != null)
			bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle(bundleName, locale);
		else
			bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle(bundleName);

		// Attempt to get the string
		try {
			messageText = bundle.getString(code);
		} catch (MissingResourceException mre) {
			// Not found, return null
			return null;
		}

		// Found the message. Create an IssuedError object.
		// Default the log indicator to "N" for now.
		return new IssuedError(code, messageText, severity, "N");
	}

	
	/**
	 * This method takes a list of errors and adds the information to the overall Mediator XML document to be returned to the client.
	 *
	 * @param errorList
	 *            A collection of IssuedError objects
	 */
	public DocumentHandler getErrorDoc() {

		DocumentHandler errorDoc = new DocumentHandler();
		int maxErrorSeverity = 0;
		int errorId = 1;
		for (IssuedError err : error_issued) {
			int severity = err.getSeverity();
			if (severity > maxErrorSeverity)
				maxErrorSeverity = severity;

			String code = err.getErrorCode();
			String msg = err.getErrorText();

			// If this is an infrastructure message that signifies an update,
			// put the corresponding information in the XML document
			// This will notify the web server Java Bean infrastructure
			// that a bean has invalid data.
			errorDoc.setAttribute("/errorlist/error(" + errorId + ")/code", code);
			errorDoc.setAttribute("/errorlist/error(" + errorId + ")/message", msg);
			errorDoc.setAttributeInt("/errorlist/error(" + errorId + ")/severity", severity);
			errorId++;
		}

		errorDoc.setAttributeInt("/maxerrorseverity", maxErrorSeverity);

		return errorDoc;
	}
}