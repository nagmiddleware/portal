package com.amsinc.ecsg.frame;

/*
 * @(#)DecimalAttribute
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.math.BigDecimal;


/**
 * The DecimalAttribute class performs all validation required for attributes
 * that store decimal values (i.e. 8.93).
 *

 */
public class DecimalAttribute extends Attribute {
	public DecimalAttribute ()
	 {
     }

	public DecimalAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public DecimalAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }

	/**
	 * Override the ancestor's setAttributeValue method so the null/emtpy
	 * values are converted to "0" values.
	 * 
	 * @param String attributeValue The value being set
	 * @param boolean validateValue Boolean indicator as to if the value will 
	 * validate.
	 *

	 */
	public void setAttributeValue(String attributeValue,boolean validateValue) throws AmsException{
		// If the attribute value is null/empty, default the value to "0".
//		if ((attributeValue == null) || (attributeValue.equals ("")))
//		{
//			super.setAttributeValue("0",validateValue);
//		}else{
			// Simply set the attribute Value using the super class method
			super.setAttributeValue(attributeValue,validateValue);
//		}
	}
	/**
	 * Validate that the value of the attribute is a valid decimal 
	 * value. If the value is not numeric, an exception is thrown.
	 * 

	 */
	public void validate(String attributeValue) throws AmsException{
		// Ensure that the attribute value is a decimal value
		try{
		    if ((attributeValue != null)&&(!attributeValue.isEmpty())) {
			    BigDecimal value = new BigDecimal(attributeValue);
			}
		} catch (NumberFormatException nfex){
			throw new InvalidAttributeValueException(AmsConstants.INVALID_NUMBER_ATTRIBUTE);
		}
	}
}
