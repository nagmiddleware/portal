package com.amsinc.ecsg.frame;

import javax.ejb.*;
import java.rmi.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public interface SelectMediatorHome extends javax.ejb.EJBHome
{
  public SelectMediator create()
        throws CreateException, RemoteException;
}