package com.amsinc.ecsg.frame;

/*
 * @(#)GenericInitialContextFactory
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import javax.naming.*;
import java.util.Hashtable;
import java.util.PropertyResourceBundle;
import com.amsinc.ecsg.util.JPylonProperties;

/**
 * The GenericInitialContextFactory is a factory class that caches
 * InitialContexts. The price of getting InitialContexts is expensive.
 * This class will maintain an initial context that can be used by 
 * objects wishing to have a vanilla initial context (i.e. server side
 * components with no environment parameters).
 *

 * @version 1.0 
 */

public class GenericInitialContextFactory
{
    private static Context serverCtx;
    private static String jndiFactoryName;
    private static String defaultServerLocation;

    // Stores a cache of client contexts indexed by server locations
    private static Hashtable clientContexts;

    static
    {
        try {
            JPylonProperties jPylonProperties = JPylonProperties.getInstance();

            try
             {
               jndiFactoryName = jPylonProperties.getString ("jndiFactoryName"); 
             }
            catch(AmsException amse)
             {
                // Default to WebLogic's JNDI factory
                jndiFactoryName = "weblogic.jndi.WLInitialContextFactory";
             }
            defaultServerLocation = jPylonProperties.getString ("serverLocation");
        } catch (Exception e) {}
        
        try {
            //Create the server side initial context
            serverCtx = new InitialContext ();
        }
        catch (NamingException nEx)
        {
            serverCtx = null;
        }

      clientContexts = new Hashtable();
                
    }

    /**
     * The default creator is not available to the user as we only want
     * users calling the static methods.
     */
    private GenericInitialContextFactory ()
    {
    }

    /**
     * Gets the generic initial context. If we have already created it, then
     * the previously created one is returned, otherwise we create it and
     * return it.
     *
     * @return The generic initial context.
     */
    public static synchronized Context getInitialContext ()
           throws AmsException
    {
        return getInitialContext(true);
    }

    /**
     * Gets the generic initial context. If we have already created it, then
     * the previously created one is returned, otherwise we create it and
     * return it.
     *
     * @param onServer indicates whether or not the initial context is being
     *                 retrieved from the client or the server
     * @return The generic initial context.
     */
    public static synchronized Context getInitialContext (boolean onServer)
           throws AmsException
     {
        return getInitialContext(onServer, defaultServerLocation);
     }


    /**
     * Gets the generic initial context. If we have already created it, then
     * the previously created one is returned, otherwise we create it and
     * return it.
     *
     * @param onServer indicates whether or not the initial context is being
     *                 retrieved from the client or the server
     * @return The generic initial context.
     */
    public static synchronized Context getInitialContext (boolean onServer, String serverLocation)
           throws AmsException
    {
        if (!onServer) {
            InitialContext clientCtx = (InitialContext) clientContexts.get(serverLocation);

            if (clientCtx == null)
            {
                Hashtable props = new Hashtable ();
                props.put (Context.INITIAL_CONTEXT_FACTORY,jndiFactoryName);
                props.put (Context.PROVIDER_URL, serverLocation);
                try {
                    clientCtx = new InitialContext (props);

                    // Add the client context to the cache
                    clientContexts.put(serverLocation, clientCtx);
                }
                catch (NamingException nEx)
                {
                    throw new AmsException (
                    "Naming Exception thrown: Unable to create InitialContext: " + nEx.getMessage ());
                }
            }
            return clientCtx;
        } else {
            if (serverCtx == null)
            {
                try
                {
                    serverCtx = new InitialContext();
                }
                catch (NamingException nEx)
                {
                    throw new AmsException (
                    "Naming Exception thrown: Unable to create InitialContext: " + nEx.getMessage ());
                }
            }
            return serverCtx;
        }
    }
}
