package com.amsinc.ecsg.frame;

import java.io.IOException;
/*
 * @(#)NavigationManager
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DocumentHandler;

/**
 * NavigationManager is used to hold and provide all information related to the navigation from one web page to another. This
 * includes the knowledge of the next page to navigate to given the current page and user action, as well as the mapping between
 * logical and physical page names.
 */
public class NavigationManager implements java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(NavigationManager.class);

	/**
	 * The reference to the singleton static instance of the factory class
	 */
	private static NavigationManager instance = null;
	/**
	 * This hashtable is keyed by the concatenation of currentPage and userAction into one string. The values are the logical page
	 * names of the next page.
	 */
	private Hashtable navInfoList;
	/**
	 * A hashtable that maps between the logical page names and the physical page names.
	 */
	private Hashtable htLogicalPageToPhysical;
	/**
	 * A hashtable that maps between the logical page names and the physical page names.
	 */
	private Hashtable htExceptionMap;

	/**
	 * The default page to navigate to in case navigation information is not available
	 */
	private String defaultPage;

	/**
	 * This method provides static access to the error catalog instance. This implements the singleton pattern. NOTE: This getter
	 * should not be used on the very first access to the NavigationManager.
	 *
	 * @return NavigationManager the singleton instance
	 */
	public static NavigationManager getNavMan() {
		return instance;
	}

	/**
	 * This method provides static access to the navigation info factory instance. This implements the singleton pattern. NOTE: This
	 * getter only needs to be used once, i.e. the very first access to NavigationManager. All future access can be done through the
	 * standard getNavMan() method.
	 *
	 * @param String
	 *            baseDir The directory location of the XML configuration documents
	 * @param int
	 *            debugMode The debug mode to use within this class
	 * @return NavigationManager the singleton instance
	 */
	public static NavigationManager getNavMan(int debugMode) {
		if (instance == null) {
			synchronized (NavigationManager.class) {
				if (instance == null) {
					instance = new NavigationManager(debugMode);
				}
			}
		}
		return instance;
	}

	/**
	 * Check whether the session is valid on a servlet. Return 401 - Unauthroized Access if they are not. This blocks bookmarked
	 * access to the servlet.
	 * 
	 * @param request
	 * @param response
	 * @param servlet
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public static boolean bookmarkedAccessDenied(HttpServletRequest request, HttpServletResponse response, HttpServlet servlet)
			throws IOException {
		// T36000005819 W Zhu 9/24/12 Do not allow bookmarked access to the servlet.
		// TimeoutListener attribute is set on checkLogon.jsp
		HttpSession session = request.getSession(false); // don't create a new one, return null if none
		if (session == null || session.getAttribute("TimeoutListener") == null) {
			LOG.error("NavigationManager.bookmarkedAccessDenied: Direct acess to the servlet {} denied.", servlet.getClass());
			response.sendError(401); // Unauthorized Access

			return true;
		}
		return false;
	}

	/**
	 * Default constructor
	 *
	 */
	private NavigationManager() {
	}

	/**
	 * Constructor that specifies the directory location of the navigation information XML documents.
	 *
	 * @param int
	 *            debugMode The debug mode to use within this class
	 */
	private NavigationManager(int debugMode) {
		refresh();
	}

	/**
	 * This method reads in the XML configuration documents and populates the internal hashtable storage of the navigation
	 * information.
	 *
	 */
	public synchronized void refresh() {
		navInfoList = new Hashtable();
		htLogicalPageToPhysical = new Hashtable();
		htExceptionMap = new Hashtable();
		DocumentHandler doc = null;
		try {
			// Process navigation info xml configuration file
			doc = XmlConfigFileCache.readConfigFile("/Navigation.xml");

			defaultPage = doc.getAttribute("/defaultPage");
			if (defaultPage == null) {
				defaultPage = "";
				LOG.debug("NavigationManager Page Info: NO DEFAULT PAGE SPECIFIED");
			} else {
				LOG.debug("NavigationManager Page Info: The default page is {}", defaultPage);
			}
			Vector navigationDocs = doc.getFragments("/NavigationInfo");
			Enumeration enumer = navigationDocs.elements();
			while (enumer.hasMoreElements()) {
				DocumentHandler navDoc = (DocumentHandler) enumer.nextElement();
				// The key to determining the next page can be either:
				// a) the current page, b) the user action, or c) both the
				// current page and user action. An error is given if neither
				// is specified.
				String currPage = navDoc.getAttribute("/currentPage");
				String userAct = navDoc.getAttribute("/userAction");
				String nextPage = navDoc.getAttribute("/nextPage");
				String errorPage = navDoc.getAttribute("/errorPage");
				String intermediatePage = navDoc.getAttribute("/intermediatePage");
				String advanceCurrentPage = navDoc.getAttribute("/advanceCurrentPage");
				String htKey = "";
				if (currPage != null) {
					htKey = htKey.concat(currPage);
				}
				if (userAct != null) {
					htKey = htKey.concat(userAct);
				}
				if (nextPage == null) {
					LOG.debug("NavigationManager {} encountered an error.  No nextPage specified.  Ignoring this entry.", htKey);
				} else {
					LOG.debug("NavigationManager Page Info: {} = {}, " + errorPage, htKey, nextPage);
					NavigationInfo navInfo = new NavigationInfo(nextPage, errorPage, intermediatePage, !"N".equals(advanceCurrentPage));
					navInfoList.put(htKey, navInfo);
				}
			}

			// Process map file between logical and physical page names
			Vector pageDocs = doc.getFragments("/WebPage");
			Enumeration pageEnum = pageDocs.elements();
			while (pageEnum.hasMoreElements()) {
				DocumentHandler navDoc = (DocumentHandler) pageEnum.nextElement();
				String logicalName = navDoc.getAttribute("/logicalName");
				String physicalName = navDoc.getAttribute("/physicalName");
				LOG.debug("NavigationManager Page Map: {} = {}", logicalName, physicalName);
				htLogicalPageToPhysical.put(logicalName, physicalName);
			}
			// Process map file for exception cases
			Vector exceptionDocs = doc.getFragments("/NavigationException");
			Enumeration exceptionEnum = exceptionDocs.elements();
			while (exceptionEnum.hasMoreElements()) {
				DocumentHandler navDoc = (DocumentHandler) exceptionEnum.nextElement();
				String lastPageServed = navDoc.getAttribute("/lastPageServed");
				String theNextPage = navDoc.getAttribute("/nextPage");
				LOG.debug("NavigationManager Page Exception Map: {} => {}", lastPageServed, theNextPage);
				htExceptionMap.put(lastPageServed, theNextPage);
			}
		} catch (Exception e) {
			LOG.error("Error occured loading navigation xml", e);
		}
	}

	/**
	 * This method determines the next logical web page to go to in normal conditions given the current page in the application and
	 * the user action. Note that either of the two arguments could be null, because navigation can be based on either the current
	 * page, user action, or both.
	 *
	 * @param String
	 *            currentPage The current logical page name
	 * @param String
	 *            userAction The action taken by the user
	 * @return String The logical page name
	 */
	public String getNextPage(String currentPage, String userAction) throws AmsException {
		NavigationInfo navInfo = getNavInfo(currentPage, userAction);
		return navInfo.getNextPage();
	}

	/**
	 * This method determines the next logical web page to go to in an error condition given the current page in the application and
	 * the user action. Note that either of the two arguments could be null, because navigation can be based on either the current
	 * page, user action, or both.
	 *
	 * @param String
	 *            currentPage The current logical page name
	 * @param String
	 *            userAction The action taken by the user
	 * @return String The logical page name
	 */
	public String getErrorPage(String currentPage, String userAction) throws AmsException {
		NavigationInfo navInfo = getNavInfo(currentPage, userAction);
		return navInfo.getErrorPage();
	}

	/**
	 * This method returns a NavigationInfo object based on the current page in the application and the user action. Note that
	 * either of the two arguments could be null, because navigation can be based on either the current page, user action, or both.
	 *
	 * @param String
	 *            currentPage The current logical page name
	 * @param String
	 *            userAction The action taken by the user
	 * @return NavigationInfo Information object containing navigation information
	 */
	public NavigationInfo getNavInfo(String currentPage, String userAction) throws AmsException {
		String key;
		if (currentPage == null) {
			key = userAction;
		} else if (userAction == null) {
			key = currentPage;
		} else {
			key = currentPage.concat(userAction);
		}
		// debug("NavigationManager getNavigationInfo looking for " + key);// W Zhu 4/7/12 Rel 8.0 Remove excessive debugging
		NavigationInfo navInfo = null;
		try {
			navInfo = (NavigationInfo) navInfoList.get(key);
			// If we didn't find the navigation info, load with the default page
			if (navInfo == null) {
				LOG.debug("NavigationManager WARNING: Navigation information not found for current page and userAction. Using default page.");
				navInfo = new NavigationInfo(defaultPage, defaultPage, defaultPage, true);
			}
		} catch (NullPointerException ex) {
			String msg = "NavigationInfo: Didn't find ".concat(key);
			LOG.debug("NavigationManager " + msg);
			throw new AmsException(msg);
		}
		return navInfo;
	}

	/**
	 * This method returns the physical page name given the logical page name.
	 *
	 * @param String
	 *            logicalPage The logical page name
	 * @param HttpServletRequest
	 *            request The request object
	 * @return String The physical page name
	 */
	public String getPhysicalPage(String logicalPage, HttpServletRequest request) throws AmsException {
		LOG.debug("NavigationManager getPhysicalPage looking for {}", logicalPage);
		if (logicalPage == null) {
			throw new AmsException("Logical page is null.");
		}
		String physicalPage = null;
		String nextLogicalPage = null;
		int pageIndex = logicalPage.indexOf(NavigationInfo.PAGE_DELIMETER);
		// The presence of PAGE_DELIMETER indicates that the IntermediatePage should be
		// displayed prior to next page
		if (pageIndex != -1) {
			String nextPage = logicalPage.substring(pageIndex + NavigationInfo.PAGE_DELIMETER.length());
			String origPage = (String) request.getAttribute("originalPage");
			// If nextPage is found within the origPage string then we're navigating
			// the same page we came from, therefore don't forward user to the IntermediatePage
			// We check for the index of nextPage within origPage rather than checking if the
			// two strings are equal because the original page could include the IntermediatePage;
			// This situation is created by the method used to set the current page on FormManager
			// when the IntermediatePage is utilized
			if (origPage.indexOf(nextPage) == -1) {
				request.setAttribute("nextLogicalPage", nextPage);
				nextLogicalPage = "IntermediatePage";
			} else {
				nextLogicalPage = nextPage;
			}
		} else {
			nextLogicalPage = logicalPage;
		}
		physicalPage = (String) htLogicalPageToPhysical.get(nextLogicalPage);
		if (physicalPage == null) {
			LOG.debug("NavigationManager Logical page {} not found in page mapping.", logicalPage);
			return logicalPage;
		}
		return physicalPage;
	}

	/**
	 * This method returns the logical page to go to in an exception condition given the last page served.
	 *
	 * @param String
	 *            logicalPage The last logical page served to the client
	 * @return String The logical page to redirect to
	 */
	public String getExceptionPage(String logicalPage) throws AmsException {
		LOG.debug("NavigationManager getExceptionPage looking for {}", logicalPage);
		if (logicalPage == null) {
			throw new AmsException("Logical page is null.");
		}
		String nextPage = null;
		nextPage = (String) htExceptionMap.get(logicalPage);
		if (nextPage == null) {
			LOG.debug("NavigationManager Logical page {} not found in page mapping.", logicalPage);
			return logicalPage;
		}
		return nextPage;
	}

	/**
	 * This method returns the default page for the entire application.
	 *
	 * @return String The logical default page name
	 */
	public String getDefaultPage() {
		return defaultPage;
	}

}