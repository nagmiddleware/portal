/*
 * @(#)ListViewHome
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.*;
import javax.ejb.*;

public interface ListViewHome extends EJBHome
{
  public ListView create()
    throws RemoteException;
}

