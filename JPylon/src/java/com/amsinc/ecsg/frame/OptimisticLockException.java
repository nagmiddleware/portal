package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.frame.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OptimisticLockException extends AmsException {
    //Don't know what to output
    public OptimisticLockException(){super("The object has been altered and cannot be saved.");}
    //Don't know what to output
    public OptimisticLockException(String lockedObject){
        super("The object "+lockedObject+" has been altered and cannot be saved.");
    }
}