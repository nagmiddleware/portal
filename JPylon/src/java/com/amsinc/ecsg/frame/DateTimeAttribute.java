package com.amsinc.ecsg.frame;

/*
 * @(#)DateTimeAttribute
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.util.Date;
import com.amsinc.ecsg.util.DateTimeUtility;
import java.text.SimpleDateFormat;

/** 
 * The DateTimeAttribute class performs all validation required for DBMS
 * DateTime attributes.  The current implementation of the DateTimeAttribute class
 * validates that the attribute value is a valid SQL TimeStamp in the format
 * "MM/dd/yyyy hh:mm:ss SS".
 *

 */
public class DateTimeAttribute extends Attribute {
	public DateTimeAttribute ()
	 {
     }

	public DateTimeAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public DateTimeAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }

	/**
	 * Override the ancestor's getAttributeValue method so that the SQL
	 * Timestamp is converted to the standard DateTime format of "MM/dd/yyyy hh:mm:ss".
	 * 
	 * @return String The date value in the format "MM/dd/yyyy hh:mm:ss"
	 */
	public String getAttributeValue(){
		String superValue = super.getAttributeValue();
		// If the value is a pre-existing value (retrieved from the DB) then
		// perfrom the necessary processing to convert the value from a Timestamp
		// to the standard framework time format "MM/dd/yyyy HH:mm:ss".
		if(superValue.length() > 19){
			String value = "";
			try{
				value = DateTimeUtility.convertTimestampToDateTimeString(superValue);
			}catch(Exception e){}
			// return the processed value
			return value;
		}else{
			// return the unprocessed value
			return superValue;
		}
	}
	/**
	 * Override the ancestor's setAttributeValue method so that null/emtpy
	 * values are processed correctly.
	 * 
	 * @param String attributeValue The value being set
	 * @param boolean validateValue Boolean indicator as to if the value will 
	 * validate.
	 *

	 */
	public void setAttributeValue(String attributeValue,boolean validateValue) throws AmsException{
		// If the attribute value is null/empty, default the value to "".
		if ((attributeValue == null) || (attributeValue.isEmpty()))
		{
			super.setAttributeValue("",false);
		}else{
			// Simply set the attribute Value using the super class method
			super.setAttributeValue(attributeValue,validateValue);
		}
	}
	// Ensure that the attribute value being set is a valid Date
	public void validate(String attributeValue) throws AmsException{
		// Try to process the date-string as a SQL Date
    	Date parsedDate = null;
	    String newDate = "";

		try{
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		    parsedDate = formatter.parse(attributeValue);
		    newDate = formatter.format(parsedDate).trim();		
		}catch(Exception e){
			throw new InvalidAttributeValueException(AmsConstants.INVALID_DATE_ATTRIBUTE);
		}
	    if (!attributeValue.equals(newDate)) {
		    throw new InvalidAttributeValueException(AmsConstants.INVALID_DATE_ATTRIBUTE);
	    }
	}	
}
