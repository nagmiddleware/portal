/*
 * @(#)UpdateEvent
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import  com.amsinc.ecsg.util.DocumentHandler;


public class UpdateEvent implements java.io.Serializable {
    private String objectName;
    private String oid;
    private DocumentHandler cacheDocument;
    private String xmlPathLocation = null;
    private boolean deleteAction;

    public UpdateEvent(String objectName, String oid, DocumentHandler cacheDocument) {
        this(objectName, oid, false, cacheDocument);
    }

    public UpdateEvent(String objectName, String oid, boolean delete, DocumentHandler cacheDocument) {
        this.objectName = objectName;
        this.oid = oid;
        this.deleteAction = delete;
        this.cacheDocument = cacheDocument;
    }
    
    private UpdateEvent() {
    }
    
    public String getObjectName() {
        return objectName;
    }
    
    public String getOid() {
        return oid;
    }
    
    public boolean isDelete() {
        return deleteAction;
    }
    
    public DocumentHandler getCacheDocument() {
        return cacheDocument;
    }
    
    public String getXmlPathLocation() {
        return xmlPathLocation;
    }
    
    public void setXmlPathLocation(String xmlPathLocation) {
        this.xmlPathLocation = xmlPathLocation;
    }
}