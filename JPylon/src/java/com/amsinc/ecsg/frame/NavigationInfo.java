package com.amsinc.ecsg.frame;

/*
 * @(#)NavigationInfo
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.util.*;

public class NavigationInfo implements java.io.Serializable {

    // Class variables
    private String nextPage;
    private String errorPage;
    private String intermediatePage;
    private boolean advanceCurrentPage;
    public  static final String PAGE_DELIMETER = "=>";
    
    public NavigationInfo(String nextPage, String errorPage, String intermediatePage, boolean advanceCurrentPage) {
        this.nextPage = nextPage;
        this.errorPage = errorPage;
	this.intermediatePage = intermediatePage;
        this.advanceCurrentPage = advanceCurrentPage;
    }

    public String getNextPage() {
	// If this NavInfo object uses an intermediate page then return
	// the nextPage using the following format: "IntermediatePage=>nextPage"
        // This will ensure that the user is forwared to the IntermediatePage 
	// while the nextPage is loading
	if ((intermediatePage != null) && (intermediatePage.equalsIgnoreCase ("Y")))
        {
	  // Separate the IntermediatePage and nextPage with a string delimeter
          return "IntermediatePage" + PAGE_DELIMETER + nextPage;
        }
      	return nextPage;
    }

    public String getErrorPage() {
      return errorPage;
    }
    
    public boolean getAdvanceCurrentPage() {
        return advanceCurrentPage;
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder(" nextPage:");
        buffer.append(nextPage);
        buffer.append(" errorPage:");
        buffer.append(errorPage);
        return buffer.toString();
    }
}
