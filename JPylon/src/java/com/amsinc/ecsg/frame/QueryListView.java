/*
 * @(#)QueryListView
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.RemoteException;
import java.util.List;

public interface QueryListView extends ListView{ 
    public void setSQL(String sqlStatement)
        throws RemoteException;
    
    public void setSQL(String sqlStatement, Object... params)
    throws RemoteException;
    
    public void setSQL(String sqlStatement, List<Object> params)
    throws RemoteException;
}