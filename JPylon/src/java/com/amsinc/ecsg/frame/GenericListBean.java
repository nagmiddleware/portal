package com.amsinc.ecsg.frame;

/*
 * @(#)GenericListBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.util.*;
import java.util.Hashtable;
import com.workingdogs.village.*;
import java.sql.*;
import javax.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.*;
import java.rmi.*;
import java.util.*;
import java.text.MessageFormat;
import javax.naming.*;

/**
 * The GenericListBean provides basic listing services for individual business
 * object components.
 *
 * @version 1.0 
 */
public class GenericListBean implements SessionBean   {
	private static final Logger LOG = LoggerFactory.getLogger(GenericListBean.class);
    /** Empty KeyDef so we can use the DataSchemaFactory to get our database
        schemas and speed things up a bit */
    private static KeyDef dummyKey = new KeyDef ();
    // List Object properties
    protected transient String objectName;
    protected transient String objectTableName;
    protected transient String listTypeName;
    protected transient String[] ListParameters;
    protected transient String SQLWhereClause = "";
    protected SessionContext myContext;
    protected transient Hashtable<String, Attribute> registeredAttributes;
    // Record Properties
    protected transient TableDataSet listObjectData;
    protected transient Record currentRecord = null;
    protected transient int currentRecordIndex = 0;
    protected transient Hashtable<Long, Integer> objectRowIndex;
    // Object properties
    protected transient String IDAttributeName;
    protected transient boolean dataRetrieved = false;
    protected transient boolean removeBO = true;

    protected transient ClientServerDataBridge csdb;
    /** A vector of EJBs that are accessed by the list */
    public Vector<BusinessObject> ejbObjList = new Vector<> ();
    
        
    public GenericListBean()
    {
    }
    
    /**
     * Builds the where clause using for DBMS retrieval based on one or more 
     * retrieval parameters. If a retrieval parameter is null or empty, it
     * will be ignored.
     *
     */
    protected void buildWhereClause (boolean wrapArguments)
    {
   
        int index = 0;
        if (wrapArguments)
        {
            for (index = 0;index < this.ListParameters.length;index++)
            {
                // Wrap the parameters with '' (for DMBS processing)
                this.ListParameters[index] =
                    StringFunction.wrapString(this.ListParameters[index], "'");
            }
        }
        // Substitue the Values using the MessageFormat class
        this.SQLWhereClause = MessageFormat.format (this.SQLWhereClause, this.ListParameters);
    }
    public void ejbActivate ()
    {
    }
    // EJB Create Method
    public void ejbCreate ()
    {
    }
    public void ejbCreate (ClientServerDataBridge csdb)
    {
        this.csdb = new ClientServerDataBridge(csdb);
    }
    public void ejbPassivate ()
    {
    }
    
    // EJB Methods not fully implemented, but coded to adhere to the EJB
    // contract
	public void ejbRemove() {
		// Close the Listing DS
		try {
			this.listObjectData.close();
		} catch (Exception e) {
		}
		// Clean up registered business objects
		Enumeration<BusinessObject> enumer = ejbObjList.elements();
		while (enumer.hasMoreElements()) {
			EJBObject ejbObj = enumer.nextElement();
			try {
				ejbObj.remove();
			} catch (RemoveException rEx) {
				LOG.error("RemoveException in ejbRemove of GenericListBean: ", rEx);
			} catch (RemoteException rEx) {
				LOG.error("RemoteException in ejbRemove of GenericListBean: ", rEx);
			}
		}
	}
    
    /**
     * Gets the value of an attribute for the current object pointed to in the
     * list
     *
     * @param attributeName The attributeName being requested
     * @exception AmsException
     */
    public String getAttribute (String attributeName)
           throws RemoteException, AmsException
    {
        return this.getValue (attributeName);
    }
    /**
     * Gets multiple attributes for the current object pointed to in the list
     *
     * @param attributeNames The attributeNames being requested
     * @exception AmsException
     */
    public Hashtable<String, String> getAttributes (String[] attributeNames)
           throws RemoteException, AmsException
    {
        int i;
        int count = attributeNames.length;
        Hashtable<String, String> NameValuePair = new Hashtable<>(count);
        // For each requested attribute, store the name/value pair in the hash
        for (i = 0;i < count;i++)
        {
            NameValuePair.put (
                attributeNames[i], this.getValue (attributeNames[i]));
        }
        // Return the Hashtable
        return NameValuePair;
    }
    /**
     * This method returns the remote interface of an instantiated business
     * object based on the object that is currently active within the list.  
     * 
     * The active object is determined based on a call to the scrollToObject() 
     * or scrollToObjectByIndex() method.
     *
     * @return The remote interface of the Buisness Object being requested.
     * @exception AmsException
     */
    public BusinessObject getBusinessObject()
           throws AmsException, RemoteException
    {
        // Obtain the active object's objectID
        Long objectID =
            new Long (this.getAttribute (this.getIDAttributeName ()));
        // Get the business object
        return instantiateBusinessObject (objectID.longValue ());
    }
    /**
     * This method returns the remote interface of an instantiated business
     * object based on the objectID passed to the method.
     *
     * @param objectID The objectID of the business object being requested.
     * @return The remote interface of the Buisness Object being requested.
     * @exception AmsException
     */
    public BusinessObject getBusinessObject (long objectID)
           throws AmsException, RemoteException
    {
        return instantiateBusinessObject (objectID);
    }
    /**
     * This method loads the listing object's datasource with all instances of
     * business objects based on the the settings used in list preparation.<p>
     * <p>
     * IMPORTANT: The behavior of this method relies on the implementation
     * of the TableDataSet class's fetchRecords (void) method. It mirrors
     * that method's logic, if that method should ever change, then this one
     * must also.
     *
     */
    public void getData ()
           throws AmsException
    {
       getData (-1);
    }
    /**
     * This method loads the listing object's datasource with all instances of
     * business objects based on the the settings used in list preparation.<p>
     * <p>
     * IMPORTANT: The behavior of this method relies on the implementation
     * of the TableDataSet class's fetchRecords (int max) method. It mirrors
     * that method's logic, if that method should ever change, then this one
     * must also.
     *
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     */
    public void getData (int maxRows)
           throws AmsException
    {
       getData (0, maxRows);
    }
    /**
     * This method loads the listing object's datasource with all instances of
     * business objects based on the the settings used in list preparation.
     *
     * @param firstRow The first row retrieved by the query that we care about
     * This is a zero based value, i.e. 0 = 1st row, 1 = 2nd row, etc. This
     * allows the user to retrieve "chunks" of data.
     * @param maxRows The maximum number of rows to retrieve from the database.
     * This allows the caller to "chunk" data when making queries that return
     * lots of rows.
     */
    public void getData (int firstRow, int maxRows)
           throws AmsException
    {
        Connection conn = null;
        // Execute the Query
        String sql="";
        try
        {
            // Get the DBMS Connection
        	
            try {
                String dbmsConnectionPool = JPylonProperties.getInstance().getString("dataSourceName");
                DataSource ds = DataSourceFactory.getDataSource(dbmsConnectionPool);
                conn = ds.getConnection();
            } catch (Exception e) {
                LOG.error("Exception getting dataSourceName ", e);
            }
            // Create the DataSet
            this.listObjectData = new TableDataSet (
                conn,
                DataSchemaFactory.getSchema (conn, this.objectTableName),
                GenericListBean.dummyKey);
            // Add the where clause (if necessary)
            if (this.SQLWhereClause.length () > 0)
            {
                this.listObjectData.where (this.SQLWhereClause);
            }
            // Get the records
            sql=this.listObjectData.getSelectString();
            LOG.debug("Executing SQL => ", sql);
            this.listObjectData.fetchRecords (firstRow, maxRows);
            // If records are retrieved, then defualt to the first record and
            // establish the objectByID index
            if (listObjectData.size () > 0)
            {
                // Default to the first Record
                this.setCurrentRecord (0);
                // Load the Object IDs
                this.loadObjectRowIndex ();
            }
            // Indicate that the data has been retrieved for the listing object
            this.dataRetrieved = true;
        }
        catch (SQLException | DataSetException ex)
        {
        	LOG.error("Exception getting getData() - SQL ==> {} ", sql, ex);
            throw new AmsException (ex.getMessage (), ex);
        }
        finally
        {
            try
            {
                conn.close ();
            }catch(SQLException SQLEx){
                throw new AmsException ("Error on closing connection to DBMS in function: getData()", SQLEx);
            }
        }
    }
    /**
     * Return the attribute name of the object identifier field for items in
     * this list 
     *
     */
    public String getIDAttributeName ()
           throws AmsException, RemoteException
    {
        if (this.IDAttributeName == null)
            throw new AmsException (
                "Didn't get object information yet, so IDAttributeName is unknown.");
        return this.IDAttributeName;
    }  
    // List management methods
    
    /**
     * Returns the number entries found within the list once the getData method
     * has been called.
     *
     * @exception AmsException
     */
    public int getObjectCount ()
           throws AmsException
    {
        if (!isDataRetrieved ())
        {
            throw new AmsException (
                "The getData method must be called prior to calling getObjectCount.");
        }
        // Return the size of the listData dataSet
        return this.listObjectData.size ();
    }
    /**
     * Based on the unique object id passed in, this method gets the object's 
     * index within the listdata.
     *
     * @exception AmsException
     */
    public int getObjectIndex (long objectID)
           throws AmsException
    {
        Long lobjectID = new Long (objectID);
        // Ensure that the objectRowIndex contains an entry for the object id
        // passed in
        if (this.objectRowIndex.containsKey (lobjectID))
        {
            Integer i = this.objectRowIndex.get (lobjectID);
            return i.intValue ();
        }
        else
        {
            // The entry doesn't exist, throw an exception
            throw new InvalidObjectIdentifierException (
                "The objectID: " + objectID + " is not found in the list.");
        }
    }
    /**
     * Create's an instance of the listobject's business object and obtains
     * all information related to genric list processing.
     */
    public void getObjectInformation() throws AmsException
    {
        try
        {
            // Get a handle to the EJB component
            BusinessObject objectInterface = (BusinessObject)EJBObjectFactory.createServerEJB (
                    this.myContext, this.objectName, this.csdb);
            // Get the object's tablename        
            this.objectTableName = objectInterface.getDataTableName ();
            // Get listing information
            if (!this.listTypeName.equals (AmsConstants.STANDARD_LIST))
            {
                if (this.listTypeName.equals ("FOR_PARENT"))
                {
                    this.SQLWhereClause =
                        objectInterface.getParentIDAttributeName (true) +
                        " = {0}";
                }
                else
                {
                    this.SQLWhereClause =
                        objectInterface.getListCriteria (this.listTypeName);
                }
            }
            // Get the object's registered attributes
            this.registeredAttributes = this.removeNonSupportedAttributes (
                    objectInterface.getAttributeHash ());
            // Store the object's ID AttributeName
            this.IDAttributeName = objectInterface.getIDAttributeName ();
            // Remove the object
			//cquinton 5/24/2011 Rel 7.0.0 explicit removal of ejbs after use
            if ( objectInterface!=null ) {
                try {
                    objectInterface.remove();
                } catch (Exception ex) {}
            }
        } catch (RemoteException rEx)
        {
            throw new AmsException (rEx.getMessage ());
        } 
    }
    /**
     * This method returns the logical names of all attributes that have been
     * registered with the list object.  List object's do not support every 
     * attribute type registered with the list's business object (i.e.
     * DoubleDispatch) and therefore may differ from the attributes registered
     * within the business object.
     *
     * @return  An array of logical attribute names that have been registered
     * with the list.
     */
    protected String[] getRegisterdListAttributes ()
    {
        // Build a string array based on the size of the registered attributes
        String[] attributeNames = new String[this.registeredAttributes.size ()];
        // For each attribute, get the logical attribute name
        Enumeration<String> attributeKeys = this.registeredAttributes.keys ();
        int index = 0;
        while (attributeKeys.hasMoreElements ())
        {
            Attribute attributeObject = this.registeredAttributes.get(attributeKeys.nextElement ());
            attributeNames[index] = attributeObject.getAttributeName ();
            index = index + 1;
        }
        return attributeNames;
    }
    /**
     * Gets the value of a physical attribute based on the logical attribute
     * name supplied.  
     *
     * @param logicalAttributeName The logical name of an attribute
     * @return The value of the attribute
     * @exception AmsException
     */
    protected String getValue (String logicalAttributeName)
              throws AmsException
    {
        Attribute attribute = null;
        Value value = null;
        try
        {
            // Get a handle to the attribute object
            attribute = registeredAttributes.get (logicalAttributeName);
            // If the attribute isn't valid, then the attribute is not
            // registered
            if (attribute == null)
            {
                throw new AttributeNotRegisteredException (logicalAttributeName);
            }
            // If the attribute is DBMS based, get the value of the attribute
            // based on its physical name
            value = this.currentRecord.getValue(attribute.getPhysicalName ().toUpperCase ());
        }
        catch (DataSetException dsEx)
        {
        }
        // Return the value (as a string)
        if (value== null || value.asString () == null)
        {
            return "";
        }
        else
        {
            // Use the attribute object to process the DBMS value.
            // (Dates/Times may have special formatting occuring in the
            // attribute object itself.
            attribute.setAttributeValue (value.asString (), false);
            // Return the attribute processed value
            return attribute.getAttributeValue ();
        }
    }
    /**
     * Return the result set to the user as an XML document. 
     *
     */
    public DocumentHandler getXmlResultSet ()
           throws AmsException, RemoteException
    {
        DocumentHandler resultSetDoc = new DocumentHandler ();
        MessageFormat msgFormat = new MessageFormat ("/ResultSetRecord({0})/{1}");

        try
        {
          Object [] args = { "" , "" };
          // Iterate through the records in the result set
          for (int rowIndex = 0; rowIndex < this.getObjectCount (); rowIndex++)
          {
            this.scrollToObjectByIndex (rowIndex);
            args[0] = new Integer (rowIndex);

            int columnIndex = 0;
            Enumeration<String> enumer = registeredAttributes.keys();
            while (enumer.hasMoreElements ())
            {
              String logicalAttributeName = enumer.nextElement ();
              args[1] = logicalAttributeName;
              resultSetDoc.setAttribute (
                msgFormat.format (args),
                this.getAttribute (logicalAttributeName));
              columnIndex++;
            }

          }
        }
        catch (Exception e)
        {
          LOG.error("Exception occurred",e);
        }

        return resultSetDoc;
    }
    /**
     * For the specified ObjectID, this method instantiates (gets the
     * associated data) for an object that resides in the list. The method
     * returns the handle to the business object's remote interface.
     *

     * @param objectID The objectID of the object that is to be instantiated.
     * @return The remote interface of the business object that has
     *         been instantiated.
     * @exception AmsException
     */
    protected BusinessObject instantiateBusinessObject (long objectID)
              throws AmsException, RemoteException
    {
        // Create a server side object
        BusinessObject objectInterface = (BusinessObject)EJBObjectFactory.createServerEJB (
                myContext,this.objectName, this.csdb);

        // Add the EJB to the list of object's we own so we can clean them
        // up when we are removed
        ejbObjList.addElement (objectInterface);

        // Get all the registered attributes for this object
        String[] attributeNames = this.getRegisterdListAttributes ();
        String[] attributeValues = new String[attributeNames.length];
           

      
        int i = 0;
        // Scroll the Object to the correct ObjectID
        this.scrollToObject(objectID);

        // Declare an attribute hashtable to store the attributes
        Hashtable<String, String> attributes = null;
        // If this is a ComponentList, get the attributes using the
        // getListAttributes() method. Otherwise, process the attributes
        // normally.
        if (this instanceof ComponentListBean)
        {

            ComponentListBean componentList = (ComponentListBean)this;
            attributes = componentList.getListAttributes (attributeNames, new Long(objectID));
            // Mark the business object as a component
            objectInterface.setToComponent ();
        }
        else
        {
            attributes = this.getAttributes (attributeNames);
        }


        // Loop through the attributes and store them accordingly
        for (i = 0;i < attributes.size ();i++)
        {
            attributeValues[i] = attributes.get (attributeNames[i]);
        }

        // Set the Attributes on the object
        objectInterface.setAttributes (attributeNames, attributeValues);

        // Mark the object as populated from list
        objectInterface.populateObjectFromList (objectID);

        // Return the objectInterface
        return objectInterface;
    }
    /**
     * Determines if the data has been retrieved for the listing object
     */
    public boolean isDataRetrieved ()
    {
        return this.dataRetrieved;
    }
    /**
     * This method creates a hastTable that can be used as a cross-reference for
     * objects maintained within the list.  For each row retrieved by the list,
     * an entry is made that includes the rowIndex and the object's unique
     * object identifier.
     *

     */
	protected void loadObjectRowIndex() throws AmsException {
		int i = 0;
		int count = listObjectData.size();
		// Allocate the Hash Accordingly
		this.objectRowIndex = new Hashtable<>(count);
		// Store the objectid-dataset index for each record in the dataset
		for (i = 0; i < count; i++) {
			try {
				// Added logic to correctly get the physical name of the
				// ObjectID
				Attribute objectID = this.registeredAttributes.get(this.IDAttributeName);
				Value value = null;
				Record listRecord = this.listObjectData.getRecord(i);
				value = listRecord.getValue(objectID.getPhysicalName());
				this.objectRowIndex.put(new Long(value.asLong()), new Integer(i));
			} catch (Exception e) {
				throw new AmsException(
						"Errors encountered in loadObjectRowIndex(): Nested Exception: " + e.getMessage());
			}
		}
	}
    /**
     * Prepares the listing object so that all instances of the specified object
     * will be available to the listing object.
     *
     * This is the most basic form of list preparation.  No search criteria or
     * special list processing will be performed on the list retrieval using
     * this list preperation method.
     *
     * @param objectName The name of the business object that will be processed.
     */
	public void prepareList(String objectName) throws AmsException, RemoteException {
		String[] empty = new String[0];
		this.prepareList(objectName, AmsConstants.STANDARD_LIST, empty);
	}
    /**
     * Prepares the listing object so that a specified number of instances of
     * the specified object will be available to the listing object.
     *
     * This form of list prepartaion allows some special processing, without
     * runtime criteria to occur during list retrieval. Based on the listtype
     * name provided, the business object will be interrogated as to what
     * objects will appear in the list.
     *
     * @param objectName The name of the business object that will be processed.
     * @param listTypeName The name of the listType that will be used for list
     * processing.
     */
	public void prepareList(String objectName, String listTypeName) throws RemoteException, AmsException {
		String[] empty = new String[0];
		this.prepareList(objectName, listTypeName, empty);
	}
    /**
     * Prepares the listing object so that a specified number of instances of
     * the specified object will be available to the listing object based on a
     * listtype and criteria parameters provided.
     *
     * This form of list prepartaion allows the most specialization. Using both
     * a specified list name and an array of supplied criteria parameters, the
     * instances of objects made available to this list will be determined at
     * runtime.
     *
     * NOTE: This method processes SQL parameters and wraps them with single
     * quote marks " ' " to simplify development.
     *
     * @param objectName The name of the business object that will be processed.
     * @param listTypeName The name of the listType that will be used for list
     * processing.
     * @param listParameters An array of parameter values using for list
     * processing.
     */
	public void prepareList(String objectName, String listTypeName, String[] listParameters)
			throws AmsException, RemoteException {
		this.prepareList(objectName, listTypeName, listParameters, true);
	}
    /**
     * Prepares the listing object so that a specified number of instances of
     * the specified object will be available to the listing object based on a
     * listtype and criteria parameters provided.
     *
     * This form of list prepartaion allows the most specialization. Using
     * both a specified list name and an array of supplied criteria parameters,
     * the instances of objects made available to this list will be determined
     * at runtime.
     *
     * NOTE: Based on the boolean indicator "wrapArguments" the SQL parameters
     * will be wrapped with single quote marks " ' " to simplify development.
     *
     * @param objectName The name of the business object that will be processed.
     * @param listTypeName The name of the listType that will be used for list
     * processing.
     * @param listParameters An array of parameter values using for list
     * processing.
     * @param wrapArguments A boolean indicator as to if SQL arguments are
     * wrapped
     */
	public void prepareList(String objectName, String listTypeName, String[] listParameters, boolean wrapArguments)
			throws AmsException, RemoteException {
		// Set the List Object Parameters
		this.objectName = objectName;
		this.listTypeName = listTypeName;
		this.ListParameters = listParameters;
		// Create the EJB object
		this.getObjectInformation();
		// Build the SQL
		this.buildWhereClause(wrapArguments);
	}
    /**
     * This method identifies all attributes that have been registered with the
     * business object that are supported in list processing. Currently, List
     * processing does not support DoubleDispatched or Computed attributes.
     *
     * @param attributes A hastable of all attributes that have been registered
     * with the business object in which the list has been created for.
     * @return Hashtable The hashtable of all attributes that have been
     * registered with the business object that are supported in list
     * processing.
     *
     */
	protected Hashtable<String, Attribute> removeNonSupportedAttributes(Hashtable attributes) {
		// Create a new hashtable for list-only attributes
		Hashtable<String, Attribute> listAttributes = new Hashtable<>();
		Enumeration attributeKeys = attributes.keys();
		// Add only attributes that can be processed by lists (i.e.
		// non-DoubleDispatched etc.)
		while (attributeKeys.hasMoreElements()) {
			Attribute attributeObject = (Attribute) attributes.get(attributeKeys.nextElement());
			if (attributeObject instanceof DoubleDispatchAttribute) {
				// Do nothing
			} else if (attributeObject instanceof ComputedAttribute) {
				// Do nothing
			} else if (attributeObject instanceof LocalAttribute) {
				// Do nothing
			} else {
				// Add the attribute to the list attributes
				listAttributes.put(attributeObject.getAttributeName(), attributeObject);
			}
		}
		// Return the listAttribute hashtable
		return listAttributes;
	}
    /**
     * Based on the unique object id passed in, this method scrolls the current
     * object pointer to the row representing that object.
     *

     * @param objectID The unique ObjectID being scrolled to
     * @exception AmsException
     */
    public void scrollToObject (long objectID)
           throws AmsException
    {
        // Set the current record based on the Object's index
        this.setCurrentRecord (this.getObjectIndex (objectID));
    }
    /**
     * Based on the row index passed in, this method scrolls the current
     * object pointer to the row representing that object.
     *

     * @param index The row index being scrolled to
     * @exception AmsException
     */
    public void scrollToObjectByIndex (int index)
           throws AmsException
    {
        this.setCurrentRecord (index);
    }
    /**
     * Based on the row index passed in, this method sets the list object's 
     * current record using the supplied recordPointer.
     *

     * @param recordPointer the index of the record to be pointed to
     * @exception AmsException
     */
	protected void setCurrentRecord(int recordPointer) throws AmsException {
		// Get the Record based on the record pointer
		try {
			// If records are retrieved, then data exists
			if (this.listObjectData.size() > 0) {
				this.currentRecord = this.listObjectData.getRecord(recordPointer);
				this.currentRecordIndex = recordPointer;
			}
		} catch (DataSetException dsEx) {
			throw new AmsException(
					"Error on retrieving record from ListObjectData.  Nested Exception: " + dsEx.getMessage());

		}
	}
    // -------------------------------------------------//
    // EJB Methods                                      //
    // -------------------------------------------------//
    
    // Context Method
	public void setSessionContext(SessionContext pContext) {
		myContext = pContext;
		String stringPropValue = null;
		// Get Environment Properties
		Context ctx = null;

		// Check the EJBEnvironmentProperties to see if we can get the cached values
		Hashtable<String, String> ejbEnvironment = EJBEnvironmentProperties.getEnvironment(this.getClass().toString());

		if (ejbEnvironment != null) {

			stringPropValue = ejbEnvironment.get("EJBVendor");
			if (stringPropValue != null) {
				if (stringPropValue.equals("ATG")) {
					this.removeBO = false;
				}
			}
		} else {
			ejbEnvironment = new Hashtable<>();
			// Get the connection Pool Name (from the Deployment Descriptor)
			try {
				ctx = GenericInitialContextFactory.getInitialContext();

			} catch (AmsException amsEx) {
			}

			try {
				// Obtain the EJB Vendor for vendor specific processing
				if (ctx != null)
					stringPropValue = (String) ctx.lookup("java:/comp/env/EJBVendor");
				if (stringPropValue != null) {
					// If the EJB Vendor is ATG, connections should not be  closed
					if (stringPropValue.equals("ATG")) {
						this.removeBO = false;
					}
					ejbEnvironment.put("EJBVendor", stringPropValue);
				}
			} catch (NamingException e) {
			}
			// Add the properties to the Environment cache
			EJBEnvironmentProperties.putEnvironment(this.getClass().toString(), ejbEnvironment);
		}

	}

	/**
	 * Need to Add code here to ensure that comparisons are done on similar
	 * datatypes. Otherwise several SQL Errors are encountered.
	 *
	 * 
	 */
	protected void validateRetrievalArgument(String argument, String attributeName) throws AmsException {
	}

}
