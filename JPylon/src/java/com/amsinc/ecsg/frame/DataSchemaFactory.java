/*
 * @(#)DataSchemaFactory
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.util.*;
import java.sql.*;
import com.workingdogs.village.*;

/**
 * This class is used to cache dbKona (Working Dogs) table schemas. Table
 * schemas are used by the TableDataSet class to define the data types of
 * the columns in the TableDataSet. Rather than spend time creating the same
 * schemas every time a TableDataSet is allocated, we can create one the first
 * time and save it in this class for use by later TableDataSets that use the
 * same table. Since creating the schemas requires a database hit, it is a
 * relatively expensive operation.
 */
public class DataSchemaFactory
{
   /** The schema cache. The hash is keyed by the table name with the result
       being the schema */
   private static Hashtable schemas = new Hashtable ();

   /**
    * Checks the list of schemas we have cached for one with the specified
    * table name. If one is not found in the cache then one is created and
    * added to the cache.
    *
    * @param connection The database connection to use if we need to create
    * a new schema.
    * @param tableName The name of the table we are getting the schema for.
    * @return The schema we found or created for the specified table.
    */
   static public Schema getSchema (Connection connection, String tableName)
          throws SQLException, DataSetException
   {
      Schema returnVal;

      returnVal = (Schema)schemas.get (tableName);
      if (returnVal == null)
      {
         // We do not want multiple people updating the schema cache so we
         // synchronize this part. In case someone else adds the schema we need
         // while we are waiting, we check for it again before creating and
         // adding it ourselves.
         synchronized (schemas)
         {
            returnVal = (Schema)schemas.get (tableName);
            if (returnVal == null)
            {
               returnVal = (new Schema ()).schema (connection, tableName);
               schemas.put (tableName, returnVal);
            }
         }
      }
      return returnVal;
   }

   /**
    * Resets the hashtable. If the schemas (i.e. that database table columns)
    * change and the application server is not restarted, this method should
    * be called to ensure the schemas are refreshed.  
    *

    */
   public static void refreshCachedValues ()
   {
      synchronized (schemas)
      {
         schemas = new Hashtable ();
      }
   }
}
