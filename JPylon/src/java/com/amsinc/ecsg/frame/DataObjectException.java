package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class DataObjectException extends AmsException{
    protected int vendorErrorCode = 0;
    
    public DataObjectException(){super("Exception thrown in DataObject Processing");}
    public DataObjectException(String dbmsMessage){super(dbmsMessage);}
    
    // Sets the Vendor Error Code
    public DataObjectException(int vendorCode){
        super("Exception Thrown in DataObject Processing");
        this.vendorErrorCode = vendorCode;
    }
    
    // Gets the Vendor Error Code
    public int getErrorCode(){
        return this.vendorErrorCode;
    }
}


