/*
 * @(#)MediatorBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

import java.rmi.RemoteException;
import java.util.Hashtable;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DocumentHandler;

/**
 * MediatorBean is the base class for all application service objects in the eCSG Framework. The execute() method provides a common
 * interface for all subclasses to implement to perform the work of a transaction. The performService() method is what is called
 * from the client side. This method seperates the Mediator XML document into its sections, calls the execute() method, and then
 * puts error information into the overall document to be returned back to the client.
 */
public class MediatorBean implements SessionBean {
	private static final Logger LOG = LoggerFactory.getLogger(MediatorBean.class);
	/** EJB Contect variable */
	protected SessionContext mContext;

    // Required callbacks by EJB
    public void ejbActivate () {};
    public void ejbPassivate () {};
    public void ejbRemove () {};
	
	public void setSessionContext(SessionContext context) {
		mContext = context;
	}

	/**
	 * This method returns the EJB context object for this component. This is needed to pass to the EJBObjectFactory to create other
	 * server EJB components in the same transaction context.
	 *
	 */
	protected SessionContext getSessionContext() {
		return mContext;
	}

	public void ejbCreate() {

	}

	/**
	 * This method should be overriden by the subclass to perform the transaction specific functionality. This is where the work is
	 * done!
	 *
	 * @param inputDoc
	 *            The input XML document (&lt;In&gt; section of overall Mediator XML document)
	 * @param outputDoc
	 *            The output XML document (&lt;Out&gt; section of overall Mediator XML document)
	 * @param mediatorServices
	 *            Associated class that contains lots of the auxillary functionality for a mediator.
	 */
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices meditatorServices)
			throws RemoteException, AmsException {
		return outputDoc;
	}

	public String performService(String inputDocString, ClientServerDataBridge csdb) throws AmsException, RemoteException {
		DocumentHandler inputDoc = new DocumentHandler(inputDocString, false, true);
		DocumentHandler outputDoc = performService(inputDoc, csdb);
		if (outputDoc == null) {
			return null;
		} else {
			return outputDoc.toString();
		}
	}

	public DocumentHandler performService(DocumentHandler charDoc) throws AmsException, RemoteException {
		return this.performService(charDoc, null);
	}

	/**
	 * This method performs the work of the service. The subclass execute method is called after breaking up the overall XML
	 * document into input and output sections. Error information is collected and put into the XML document. Debug output is
	 * written if debug is turned on.
	 *
	 * @param doc
	 *            The Mediator XML document containing the input, output, and errors.
	 * @param csdb
	 */
	public DocumentHandler performService(DocumentHandler charDoc, ClientServerDataBridge csdb)
			throws AmsException, RemoteException {
		MediatorServices mediatorServices = new MediatorServices();
		mediatorServices.setSessionContext(this.mContext);

		// Convert the char [] back into the document handler
		DocumentHandler doc = charDoc;

		// ErrorManager for this Mediator
		if (csdb != null) {
			mediatorServices.setCSDB(csdb);
			if (csdb.getLocaleName() != null) {
				mediatorServices.getErrorManager().setLocaleName(csdb.getLocaleName());
			}
		}

		// Get Environment Properties
		String stringPropValue = null;
		Hashtable ejbEnvironment = EJBEnvironmentProperties.getEnvironment(getClass().toString());
		if (ejbEnvironment != null) {
			processProperties(mediatorServices, ejbEnvironment);
		} else {
			ejbEnvironment = new Hashtable();
			Context ctx = null;
			try {
				ctx = GenericInitialContextFactory.getInitialContext();
			} catch (AmsException amsEx) {
				LOG.error("Caught an AmsException with error text: ", amsEx);
			}
			try {
				stringPropValue = (String) ctx.lookup("java:/comp/env/isTransactional");
				if (stringPropValue != null) {
					ejbEnvironment.put("isTransactional", stringPropValue);
				}
			} catch (NamingException nEx) {
				LOG.warn("Caught a Naming Exception getting ejbEnvironment value with error: ", nEx);
			}
			try {
				stringPropValue = (String) ctx.lookup("java:/comp/env/debugMode");
				if (stringPropValue != null) {
					ejbEnvironment.put("debugMode", stringPropValue);
				}
			} catch (NamingException nEx) {
				LOG.warn("Caught a Naming Exception getting ejbEnvironment value with error: ", nEx);
			}
			processProperties(mediatorServices, ejbEnvironment);
		}

		// Handler to user transaction
		UserTransaction trans = null;
		// Get handle to input document
		DocumentHandler inputDoc = doc.getComponent("/In");

		// Instantiate return document
		DocumentHandler outputDoc = new DocumentHandler();

		try {
			// cquinton 12/6/2012 collect the errors BEFORE we write out the error.
			// note that this try block only includes a finally statement which does the error collection to mediatorServices
			try {
				// Bean Managed start of transaction
				if (mediatorServices.getIsTransactional()) {
					try {
						Context jndiContext = GenericInitialContextFactory.getInitialContext();
						trans = (UserTransaction) jndiContext.lookup("javax.transaction.UserTransaction");
					} catch (NamingException e) {
						LOG.error("Caught a Naming Exception looking up Initial Context or transaction: ", e);
					}
					trans.begin();
				}
				LOG.debug("Input Document passed to the MediatorBean is : {} ", inputDoc);
				// Call hook method for application specific logic
				outputDoc = execute(inputDoc, outputDoc, mediatorServices);
				// Rollback the transaction if necessary. If a business object voted
				// to rollback, this will be taken care of in the commit (i.e. it will know to rollback the transaction.)
				if (mediatorServices.getIsTransactional()) {
					mediatorServices.setTranSuccess(isTransSuccess(mediatorServices.getErrorManager()));
					if (mediatorServices.getErrorManager().getMaxErrorSeverity() < ErrorManager.ERROR_SEVERITY) {
						trans.commit();
					} else {
						trans.rollback();
					}
				}
			} catch (Exception e) {
				// W Zhu 3/25/2013 Need to rollback transaction before attempting to call getIssuedErrors and remove EJB in cleanUpEjbObjList()
				// Move the code from handleExceptionAndRollback to here. Rollback the transaction unless it is already no longer active
				if (mediatorServices.getIsTransactional()) {
					mediatorServices.setTranSuccess(false);
					try {
						if (trans != null && trans.getStatus() != Status.STATUS_NO_TRANSACTION) {
							trans.rollback();
						}
					} catch (SystemException sysex) {
						LOG.error("MediatorBean.performService caught a SystemException with error: ", sysex);
					}
				}
				throw e;
			} finally { // the important part - error collection
				// Add any error information from the Mediator itself
				mediatorServices.addErrorInfo();
				// add errors from the business objects (and clean them up)
				mediatorServices.cleanUpEjbObjList();
			}

		} catch (AmsException ex) {
			// W Zhu 3/25/2013 Need to rollback transaction before attempting to call getIssuedErrors and remove EJB
			// Move the code from handleExceptionAndRollback to above. So we only need to call handleException here.
			// handleExceptionAndRollback("Caught an AmsException", ex, trans, mediatorServices);
			handleException("Caught an AmsException", ex, mediatorServices);
		} catch (RemoteException ex) {
			handleException("Caught an RemoteException", ex, mediatorServices);
		} catch (NotSupportedException ex) {
			handleException("Caught an NotSupportedException", ex, mediatorServices);
		} catch (SystemException ex) {
			handleException("Caught an SystemException", ex, mediatorServices);
		} catch (HeuristicRollbackException ex) {
			handleException("Caught an HeuristicRollbackException", ex, mediatorServices);
		} catch (HeuristicMixedException ex) {
			handleException("Caught an HeuristicMixedException", ex, mediatorServices);
		} catch (RollbackException ex) {
			handleException("Caught a RollbackException", ex, mediatorServices);
		}
		// cquinton 12/6/2012 intercept all other miscellaneous exceptions
		catch (Exception ex) {
			handleException("Unexpected exception", ex, mediatorServices);
			throw new AmsException("Unexpected exception", ex);
		}

		// cquinton 12/6/2012 move collection of errors above, before they are written out

		// Set the return components
		doc.setComponent("/Out", outputDoc);
		doc.setComponent("/Error", mediatorServices.getErrorDoc());

		// Debug output - Write the output XML document
		LOG.debug("Output document returned from the MediatorBean is : {}", doc);
		// Return the output XML document
		return doc;
	}

	private void handleException(String errorMessage, Exception ex, MediatorServices mediatorServices) {

		// errors are already collected to mediatorServices - see if we have have collected an actual error
		int maxError = mediatorServices.getMediatorMaxErrorSeverity();
		if (maxError < ErrorManager.ERROR_SEVERITY) { // no error, just info, warnings

			// generate a server error number
			String serverErrorNumber = "SE" + System.currentTimeMillis();

			// ensure it is written to log
			LOG.error("{} Server error#" + serverErrorNumber + " " + " with error: ", errorMessage, ex);

			// add an issued error so ui knows something is wrong
			try {
				mediatorServices.getErrorManager().issueError(getClass().getName(), "E001", serverErrorNumber);
				mediatorServices.addErrorInfo(); // have to redo this
			} catch (Exception ex2) {
				LOG.error("Couldn't even issue an error for the UI, giving up.", ex2);
			}
		} else {
			// even if we already have an error to report to the user, note the current exception if debugging is on
			LOG.debug("{} with error: ", errorMessage, ex);
		}
	}

	/**
	 * Processes the mediator's deployment descriptor properties to determine whether the bean is transactional and the bean's
	 * debugging level.
	 *
	 * @param mediatorServices
	 *            Associated class that contains lots of the auxillary functionality for a mediator.
	 * @param properties
	 *            Hashtable of the properties defined for this mediator
	 */
	private void processProperties(MediatorServices mediatorServices, Hashtable properties) {
		String stringPropValue = null;

		stringPropValue = (String) properties.get("isTransactional");
		if (stringPropValue != null) {
			if (stringPropValue.equalsIgnoreCase("N")) {
				mediatorServices.setIsTransactional(false);
			} else {
				mediatorServices.setIsTransactional(true);
			}
		} else {
			mediatorServices.setIsTransactional(true);
		}
		stringPropValue = (String) properties.get("debugMode");
		if (stringPropValue != null) {
			try {
				mediatorServices.setDebuggingLevel(Integer.parseInt(stringPropValue));
				mediatorServices.setDebuggingOn(true);
			} catch (Exception ex) {
				mediatorServices.setDebuggingOn(false);
			}
		} else {
			mediatorServices.setDebuggingOn(false);
		}
	}

	/**
	 * This method returns an indicator stating whether the transaction was successful. This includes checking the Mediator's error
	 * manager as well as whether any business objects voted to rollback the transaction. This method can be called at any point
	 * during the execute() method.
	 *
	 * @param errorMgr
	 */
	public boolean isTransSuccess(ErrorManager errorMgr) {
		if (errorMgr.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
			return false;
		}
		UserTransaction trans = null;
		try {
			InitialContext jndiContext = new InitialContext();
			trans = (UserTransaction) jndiContext.lookup("javax.transaction.UserTransaction");
			if (trans.getStatus() == Status.STATUS_MARKED_ROLLBACK) {
				return false;
			}
		} catch (NamingException | SystemException e) {
			LOG.error("Exception occured while checking isTransSuccess", e);
			return false;
		}
		return true;
	}
}
