package com.amsinc.ecsg.frame;

/*
 * @(#)ComponentEntry
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import javax.ejb.*;

/**
 * Defines a component object. A business object can have 0 to many component
 * objects under it. An instance of this class represents one of those
 * component objects. This class contains key information about a component
 * object, including it's name, type, relationship attribute name, state, and
 * a reference to the remote interface of the actual component business object.
 */
public class ComponentEntry implements java.io.Serializable
{
    public String componentName;
    public String componentIdentifier;
    public String componentType;
    public String identifyingAttribute;
    public EJBObject componentInterface;
    public boolean instantiated = false;
    public boolean newObject = false;
    /**
     * Gets the remoteInterface (descendent of EJBObject) for the
     * component entry.
     *

     * @return The remote interface of the component object
     */
    public EJBObject getComponentInterface ()
    {
        return this.componentInterface;
    }
    // -------------------------------------------------//
    // Object Property Methods                          //
    // -------------------------------------------------//
    
    /**
     * Returns the name of the component as it was registered. This matches
     * the JNDI name of the EJB that was deployed for the component.
     *

     * @return The name of the component
     */
    public String getComponentName ()
    {
        return this.componentName;
    }
    /**
     * Gets the componentType (BusinessObject, ListingObject).
     *

     * @return String The componentType
     */
    public String getComponentType ()
    {
        return this.componentType;
    }
    /**
     * Gets the compenentIdentifier.
     *

     * @return String The componentIdentifier
     */
    public String getComponentIdentifier()
    {
        return this.componentIdentifier;
    }
    /**
     * Gets the identifying AttributeName for the component. In a one to many
     * relationship this is the logical name of the attribute registered as
     * the ObjectIDAttribute of the parent component. In a one to one
     * relationship this is the logical name of attribute in the parent
     * component whose value is the value of the child component's oid.
     *

     * @return The identifying AttributeName
     */
    public String getIdentifyingAttribute ()
    {
        return this.identifyingAttribute;
    }
    /**
     * Indicates if the component entry's component object has been
     * instantiated.
     *

     * @return True if the object has been instantiated, false if not.
     */
    public boolean isInstantiated ()
    {
        return this.instantiated;
    }
    /**
     * Indicates if the component entry's component is new (i.e. has not been
     * added to the persistent store).
     *

     * @return True if the component is new, false if not.
     */
    public boolean isNew ()
    {
        return this.newObject;
    }
    /**
     * Sets the remoteinterface of the component entry's component object. When
     * the remote interface has been set, the component is marked as
     * "instantiated" since the EJBObject has been created.
     *

     * @param componentInterface The remote interface of the component object
     */
    public void setComponentInterface (EJBObject componentInterface)
    {
        this.componentInterface = componentInterface;
        // If an interface has been created, the component has been instantiated
        this.instantiated = true;
    }

    public void removeComponentInterface ()
    {
        this.componentInterface = null;
        this.instantiated = false;
    }
    /**
     * Sets the component name as it was registered with the component manager
     *

     * @param componentName String The name of the component
     */
    public void setComponentName (String componentName)
    {
        this.componentName = componentName;
    }
    /**
     * Sets the componentType as it was registered with the component manager.
     *

     * @param componentType String the componentType (BusinessObject,
     * ListingObject)
     */
    public void setComponentType (String componentType)
    {
        this.componentType = componentType;
    }
    /**
     * Sets the identifying AttributeName as is was registered in the
     * component manager.
     *

     * @param identifyingAttribute The identifying AttributeName
     */
    public void setIdentifyingAttribute (String identifyingAttribute)
    {
        this.identifyingAttribute = identifyingAttribute;
    }
    /**
     * Marks the component entry as new.
     *

     */
    public void setNew ()
    {
        this.newObject = true;
    }

    /**
     * Sets the identifying Component Identifier as is was registered in the
     * component manager.
     *

     * @param identifyingAttribute The identifying AttributeName
     */
    public void setComponentIdentifier (String componentIdentifier)
    {
        this.componentIdentifier = componentIdentifier;
    }
}
