package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.frame.*;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
 

public class InstrumentLockException extends AmsException {
    String firstName;
    String lastName;
    String instrumentNumber;
    long userOid;
  //PPX-208 - Ravindra B - 03/18/2011 - Start
    String agentId;
  //PPX-208 - Ravindra B - 03/18/2011 - End
    
    public InstrumentLockException(){super("The Instrument is already locked and cannot be viewed");}
    public InstrumentLockException(String firstName,String lastName, String instrumentNumber, long userOid){
        this.firstName = firstName;
        this.lastName = lastName;
        this.instrumentNumber = instrumentNumber;
        this.userOid = userOid;
    }
    
  //PPX-208 - Ravindra B - 03/18/2011 - Start
    public InstrumentLockException(String instrumentNumber, String agentId){
    	this.instrumentNumber = instrumentNumber;
        this.agentId = agentId;
    }
  //PPX-208 - Ravindra B - 03/18/2011 - End
    
    public String getFirstName()
    {
        return firstName;   
    }
    public String getLastName()
    {
        return lastName;   
    }
    public String getInstrumentNumber()
    {
        return instrumentNumber;   
    }
    public long getUserOid()
    {
        return userOid;   
    }
    
  //PPX-208 - Ravindra B - 03/18/2011 - Start
    public String getAgentId()
    {
        return agentId;   
    }
  //PPX-208 - Ravindra B - 03/18/2011 - End
}