/*
 * @(#)SevereErrorException
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

//** SevereErrorException.java
//   This class extends AmsException. It embodies a severe error exception. It stores an
//   IssuedError object.

public class SevereErrorException extends AmsException
{
   public SevereErrorException(IssuedError iErr)
	{
		super(iErr);
	}
} // end "public class SevereErrorException extends AmsException"
