package com.amsinc.ecsg.frame;

/*
 * @(#)ObjectIDCache
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.JPylonProperties;

/**
 * The ObjectIDCache caches OIDs to prevent DB lookups
 * every time a new object is created. Only one of these
 * classes should appear per VM.
 *

 * @version 1.0 
 */
public class ObjectIDCache
{
	private static final Logger LOG = LoggerFactory.getLogger(ObjectIDCache.class);
	// Hashtable that will hold many object id instances eached keyed by
	// instanceName.  This allows us to create singletons of this class for
	// each instance name
	private static Hashtable objectIdInstanceCache = new Hashtable();

	private static String dbmsParm;

	private String instanceName;
	private long currentObjectIDCount = 0;
	private long maxObjectIDCount = 0;
	private Connection dbmsConnection;
	
	static
	{
		try {
			JPylonProperties jPylonProperties = JPylonProperties.getInstance();
			dbmsParm = jPylonProperties.getString ("dataSourceName"); 
		} catch (Exception e) {
			dbmsParm = "";
		}
	}

	private ObjectIDCache ()
	{
	}

	private ObjectIDCache (String instanceName)
	{
		this.instanceName = instanceName;
	}

	public static ObjectIDCache getInstance(String instanceName)
	{
		ObjectIDCache myInstance = (ObjectIDCache) objectIdInstanceCache.get(instanceName);
		if (myInstance == null)
		{
		   myInstance = new ObjectIDCache(instanceName);
		   objectIdInstanceCache.put(instanceName, myInstance);
		}
		return myInstance;
	}

	/**
	 * Retrieves a database connection from the data source factory. The
	 * connection will be used to access the sequence table.
	 */
	private void connect (boolean onServer) throws AmsException
	{
		try
		{
			DataSource ds = DataSourceFactory.getDataSource (dbmsParm, onServer);
			dbmsConnection = ds.getConnection ();
		}
		catch (SQLException e)
		{
			throw new AmsException (
				"Error on connecting to Datasource in DataObject using DBMS " +
				"parms: " + dbmsParm + " Nested Exception: " + e.getMessage ());
		}
	}

	/**
	 * Closes the database connection being used.
	 */
	private void disconnect () throws AmsException
	{
		try
		{
			if (dbmsConnection != null)
                         {
				dbmsConnection.close ();
                                dbmsConnection = null;
                         }
		}
		catch (SQLException e)
		{
			throw new AmsException (
				"Error on disconnecting DataObject " + e.getMessage());
		}
	}

	public synchronized long generateObjectID () throws AmsException
	{
		return generateObjectID(true);
	}
	/**
	 * Generates an object ID. Object IDs are generated using the 'sequence'
	 * table in the database. The table allows the application to grab a
	 * range of IDs so that the database does not need to be hit every time an
	 * ID is needed. Therefor we only hit the database the first time an ID
	 * is needed to 'prime' the value, and when we have used up the range of
	 * IDs we got from the database.
	 *
	 * @return The next available ID.
	 */
	public synchronized long generateObjectID (boolean onServer) throws AmsException
	{
		long returnObjectID;
		try
		{
			if ((currentObjectIDCount > maxObjectIDCount) ||
				(currentObjectIDCount == 0))
			{
                // W Zhu 3/27/09 RAUJ031657093
                // Suspend any transaction so that we can commit the changes to SEQUENCE table right now.
                TransactionManager tranMan = null;
                boolean transactionSuspended = false;
                Transaction tran = null;
                try
                {
                    Context jndiContext =
                        GenericInitialContextFactory.getInitialContext (onServer);
                    tranMan = (TransactionManager)jndiContext.lookup (
                        "javax.transaction.TransactionManager");
                }
                catch (NamingException e)
                {
                    LOG.error("Exception while looking up javax.transaction.TransactionManager",e);
                }
                if (tranMan != null) {
                    int status = tranMan.getStatus();
                    if (status != javax.transaction.Status.STATUS_NO_TRANSACTION) {
                        tran = tranMan.suspend();
                        transactionSuspended = true;
                    }
                }
				connect(onServer);
				dbmsConnection.setAutoCommit (false);
				try(Statement statement = dbmsConnection.createStatement ();
					ResultSet results = statement.executeQuery ("SELECT SEQUENCE_VALUE,SEQUENCE_INCREMENT FROM SEQUENCE WHERE SEQUENCE_NAME = '"+this.instanceName+"' FOR UPDATE")
						){
				results.next ();
				currentObjectIDCount = results.getLong ("SEQUENCE_VALUE");
				long tempIncrementID = results.getLong ("SEQUENCE_INCREMENT");
                //long tempIncrementID = 1; // test code 
				maxObjectIDCount = currentObjectIDCount + tempIncrementID;
				long tempID = maxObjectIDCount + 1;
				Long ltempID = new Long (tempID);
				statement.executeUpdate (
					"UPDATE SEQUENCE SET SEQUENCE_VALUE = " +
					ltempID.toString () + " WHERE SEQUENCE_NAME = '"+this.instanceName+"'");
				dbmsConnection.commit ();
                
                // RAUJ031657093 
                if (transactionSuspended) {
                    tranMan.resume(tran);
                }
				}
			}
			returnObjectID = currentObjectIDCount;
			currentObjectIDCount = currentObjectIDCount + 1;
		}
		catch (Exception e)
		{
			throw new AmsException (
				"Error generating new objectID. Nested Excpetion: " +
				e.getMessage (),e);
		}
		finally {
			disconnect ();
		}
			
		return returnObjectID;
	}
}