package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class InvalidAttributeValueException extends AmsException{
    public InvalidAttributeValueException(){super(AmsConstants.INVALID_ATTRIBUTE);}
    public InvalidAttributeValueException(String errorID){super(errorID);}
}
