package com.amsinc.ecsg.frame;

import com.amsinc.ecsg.util.*;
import javax.ejb.*;
import java.rmi.*;
import javax.naming.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.*;
import java.util.*;

/**
 *
 * Copyright � 2001 American Management Systems, Incorporated All rights reserved
 */
public class UpdateMediatorBean extends MediatorBean {
	private static final Logger LOG = LoggerFactory.getLogger(UpdateMediatorBean.class);

	public static final String INSERT_MODE = "I";
	public static final String UPDATE_MODE = "U";
	public static final String DELETE_MODE = "D";

	// Business methods
	public DocumentHandler execute(DocumentHandler inputDoc, DocumentHandler outputDoc, MediatorServices mediatorServices)
			throws RemoteException, AmsException {

		// Declare business object variables
		BusinessObject busObj = null;

		// Get input arguments
		String objectName = inputDoc.getAttribute("/Update/beanName");
		String oidAttributeName = inputDoc.getAttribute("/Update/oidFieldName");
		String notFoundError = inputDoc.getAttribute("/Update/notFoundError");
		String updateMode = inputDoc.getAttribute("/Update/mode");
		String updateButtonValue = null;
		String insertButtonValue = null;
		String deleteButtonValue = null;
		String updateButtonName = inputDoc.getAttribute("/Update/updateButtonName");
		if (updateButtonName != null)
			updateButtonValue = inputDoc.getAttribute(updateButtonName);
		String insertButtonName = inputDoc.getAttribute("/Update/insertButtonName");
		if (insertButtonName != null)
			insertButtonValue = inputDoc.getAttribute(insertButtonName);
		String deleteButtonName = inputDoc.getAttribute("/Update/deleteButtonName");
		if (deleteButtonName != null)
			deleteButtonValue = inputDoc.getAttribute(deleteButtonName);

		if (objectName == null)
			throw new AmsException("Need to send the objectName parameter.");

		if (updateButtonValue != null)
			updateMode = UpdateMediatorBean.UPDATE_MODE;

		if (insertButtonValue != null)
			updateMode = UpdateMediatorBean.INSERT_MODE;

		if (deleteButtonValue != null)
			updateMode = UpdateMediatorBean.DELETE_MODE;

		if (updateMode == null)
			throw new AmsException("Need to send the updateMode parameter.");

		if ((oidAttributeName == null) && (!(updateMode.equalsIgnoreCase(UpdateMediatorBean.INSERT_MODE))))
			throw new AmsException("Need to send the oidFieldName parameter.");

		if (notFoundError == null)
			notFoundError = "INF01";

		try {
			// Get a handle to an instance of the object
			busObj = (BusinessObject) mediatorServices.createServerEJB(objectName);

			if (updateMode.equals(UpdateMediatorBean.INSERT_MODE)) {
				busObj.newObject();
			} else {
				StringBuilder oidAttributeXmlPath = new StringBuilder();
				oidAttributeXmlPath.append("/");
				oidAttributeXmlPath.append(objectName);
				oidAttributeXmlPath.append("/");
				oidAttributeXmlPath.append(oidAttributeName);

				long lOid = inputDoc.getAttributeLong(oidAttributeXmlPath.toString());

				busObj.getData(lOid);
			}

			if (updateMode.equals(UpdateMediatorBean.DELETE_MODE)) {
				busObj.delete();
			} else {
				// Set object attributes equal to input arguments and save
				busObj.populateFromXmlDoc(inputDoc);

				busObj.save();
			}
		} catch (InvalidObjectIdentifierException ex) {

			// If the object does not exist, issue an error
			mediatorServices.getErrorManager().issueError(getClass().getName(), notFoundError, ex.getObjectId());

		} catch (Exception e) {
			LOG.error("General exception caught in UpdateMediator", e);
		} finally {

			// Populate output document
			outputDoc = busObj.populateXmlDoc(outputDoc);

			// Get error information from business object and put into
			// document to be returned to client
			mediatorServices.addErrorInfo(busObj.getIssuedErrors());

		}
		return outputDoc;
	}
}
