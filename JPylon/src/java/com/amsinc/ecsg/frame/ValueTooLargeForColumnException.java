package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class ValueTooLargeForColumnException extends DataObjectException{
    public ValueTooLargeForColumnException(){
        super("Value Too Large For Column in DataObject Processing");
        this.vendorErrorCode = 1401;
    }
}