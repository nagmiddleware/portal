package com.amsinc.ecsg.frame;

/*
 * @(#)BusinessObjectBean
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import java.math.BigDecimal;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.ejb.EJBObject;
import javax.ejb.RemoveException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.util.DateTimeUtility;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.DocumentNode;
import com.amsinc.ecsg.util.FrameworkDebugger;
import com.amsinc.ecsg.util.JPylonProperties;
import com.amsinc.ecsg.util.ResourceManager;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.util.StringService;
import com.amsinc.ecsg.util.XMLTagTranslator;
import com.workingdogs.village.DataSetException;
import com.workingdogs.village.KeyDef;
import com.workingdogs.village.Record;
import com.workingdogs.village.TableDataSet;
import com.workingdogs.village.Value;

/**
 * Ancestor business object for all singular business objects.
 *
 */
public abstract class BusinessObjectBean implements SessionBean {
	// Object level properties
	private static final Logger LOG = LoggerFactory.getLogger(BusinessObjectBean.class);
	
	protected boolean newComponentCalled = false;
	protected boolean componentObject = false;
	protected Hashtable<String, ComponentEntry> htRegisteredComponents;
	protected AttributeManager attributeMgr = new AttributeManager(this);
	protected boolean isNew;
	protected String dataTableName;
	protected Long objectID;
	protected ErrorManager errMgr;
	protected ResourceManager resMgr;
	protected ClientServerDataBridge csdb;
	protected Vector<EJBObject> additionalBusinessObjects = new Vector<>();
	protected boolean objectInstantiated = false;
	protected SessionContext myContext;
	protected boolean debuggingOn = false;
	protected FrameworkDebugger debuggerService;
	protected String ecsgObjectName;
	protected String ecsgDataName;
	protected int debugMode;
	// DataObject Methods
	// DBMS properties
	protected KeyDef objectKey;
	protected TableDataSet objectData;
	protected Record objectRecord;
	protected transient Connection dbmsConnection = null;
	protected Hashtable<String, String> DateAttributes = new Hashtable<>();
	protected Hashtable<String, String> DateTimeAttributes = new Hashtable<>();
	protected Hashtable<String, String> BlobAttributes = new Hashtable<>();

	protected boolean retryOptLock = false;

	/**
	 * This method obtains a handle to an EJB business object component and automatically registers the business object with the
	 * BusinessObject
	 *
	 * @param objectName
	 *            The object name of the EJB to be created
	 */
	public EJBObject createServerEJB(String objectName) throws AmsException, RemoteException {
		EJBObject ejbObject = EJBObjectFactory.createServerEJB(this.myContext, objectName, this.csdb);
		additionalBusinessObjects.addElement(ejbObject);
		return ejbObject;
	}

	/**
	 * This method obtains a handle to an EJB business object component and automatically registers the business object with the
	 * Business Object
	 *
	 * @param objectName
	 *            The object name of the EJB to be created
	 * @param oid
	 *            The unique id of the EJB to return

	 */
	public EJBObject createServerEJB(String objectName, long oid) throws AmsException, RemoteException {
		EJBObject ejbObject = EJBObjectFactory.createServerEJB(this.myContext, objectName, oid, this.csdb);
		additionalBusinessObjects.addElement(ejbObject);
		return ejbObject;
	}

	// -------------------------------------------------//
	// Association Processing Methods //
	// -------------------------------------------------//
	/**
	 * This method checks to see if the objectID parameter passed in is a valid objectID for this business object. Note that this is
	 * a somewhat expensive operation as the database is hit and the business object is completely populated.
	 *
	 * @param objectID
	 *            The objectID of object that is being associated to.
	 * @return A value of <code>true</code> if the objectID is valid, a value of <code>false</code> if the objectID is not valid.
	 * 
	 */
	public boolean checkAssociation(long objectID) throws RemoteException, AmsException {

		// Create a cache entry about this business object type. On subsequent asssociation checks, this
		// data can be used instead of creating an EJB.
		AssociationCheckCache.createEntry(ecsgObjectName, this.dataTableName, attributeMgr.getIDAttribute().getPhysicalName());

		boolean objectExists = false;
		// Try to instantiate the object
		try {
			// If the object exists, return true
			this.getData(objectID);
			objectExists = true;
		} catch (AmsException amsEx) {
			// If the object wasn't found, return false
			if (amsEx instanceof InvalidObjectIdentifierException) {
				objectExists = false;
			}
		}
		return objectExists;
	}

	/**
	 * The method ensures that a specified objectID exists for the business object specified by the objectName parameter.
	 *
	 * Checking can be done in one of two ways: 1. Create an EJB, check for the existence of an object with the OID specified, then
	 * remove the EJB. This is a very expensive process. 2. Use DatabaseQueryBean to perform a count(). This uses cached information
	 * about the EJB being checked to more efficiently check the association
	 *
	 * @param objectName
	 *            The ejb Name of the Business object, must match the JNDI name of the object.
	 * @param objectID
	 *            The objectID for the business object being checked
	 * @return A value of <code>true</code> if the objectID is valid for the specified objectName, a value of <code>false</code> if
	 *         the objectID is not valid for the specified objectName or the objectName is not valid.
	 * @exception AmsException
	 * @exception RemoteException
	 * 
	 */
	public boolean checkObjectAssociation(String objectName, long objectID) throws AmsException, RemoteException {
		boolean objectExists = false;

		// If data already exists about this object, use the more efficient way
		// Otherwise, create an EJB
		if (AssociationCheckCache.entryExists(objectName)) {
			// Data exists, so we can perform a more efficient getCount call
			// jgadela R91 IR T36000026319 - SQL INJECTION FIX
			int count = DatabaseQueryBean.getCount(AssociationCheckCache.getObjectIdPhysicalName(objectName),
					AssociationCheckCache.getDataTableName(objectName),
                    AssociationCheckCache.getObjectIdPhysicalName(objectName) + " = ?", true, new Object[]{String.valueOf(objectID)});

			objectExists = (count > 0);

		} else {
			// Since no data exists to check the association using DatabaseQueryBeam,
			// create the EJB and check the association that way.
			// Doing this will create the data in the AssociationCheckCache so that
			// next time DatabaseQueryBean can be user

			// Create the Business Object
            BusinessObject associatedObject = (BusinessObject) EJBObjectFactory.createServerEJB(
                            this.getSessionContext(), objectName, this.csdb);
			// Check to see if the object exists
			objectExists = associatedObject.checkAssociation(objectID);
			// Remove the Object
			try {
				associatedObject.remove();
			} catch (Exception e) {
				throw new AmsException("Error removing Business Object in checkObjectAssociation method");
			}
		}

		return objectExists;
	}

	/**
	 * Creates an instance of the specified component object. This method will determine the relationship between the composite and
	 * component object, create an instance of the component object, and retrieve its data (if the component relationship exists).
	 *
	 * @param component
	 *            A pointer to a valid component entry object
	 * @exception AmsException
	 * @exception RemoteException
	 */
	protected void componentCreateComponent(ComponentEntry component) throws AmsException, RemoteException {
		// Ensure that the component isn't already instantiated
		if (component.isInstantiated()) {
			// Return success, component exists
			return;
		}
		// Create the component
		String componentObjectName = component.getComponentName();
		String componentObjectType = component.getComponentType();
		// Process the component as a business object
		if (componentObjectType.equals(AmsConstants.BUSINESS_OBJECT)) {
			String componentOID = this.getAttribute(component.getIdentifyingAttribute());
			// If the component IS NOT NEW and the component relationship
			// hasn't been established, throw an exception
			if (componentOID.length() < 1 && !component.isNew()) {
				throw new ComponentNoRelationshipException(
						"No relationship established between composite object and component object");
			}
            BusinessObject componentInterface = (BusinessObject) EJBObjectFactory.createServerEJB(
                            this.myContext, componentObjectName, this.csdb);
			// If the component isn't new, get it's data
			if (!component.isNew()) {
				long oid = this.getAttributeLong(component.getIdentifyingAttribute());
				componentInterface.getData(oid);
			}
			// Mark the object as a component
			componentInterface.setToComponent();
			component.setComponentInterface(componentInterface);
		} else {
			// The component is a Listing Component. Prepare the
			// listing object with the standard "FOR_PARENT" setting. This
			// will ensure that the listing object is retrieved based on the
			// parent_id attribute evaluating to the parent object's ID
			// attributeValue.
			String parentObjectID = this.getAttribute(this.getIDAttributeName());
			String[] listParms = { parentObjectID };
			ComponentList componentInterface = (ComponentList) EJBObjectFactory.createServerEJB(this.myContext, "ComponentList",
					this.csdb);
			componentInterface.prepareList(StringService.remove(componentObjectName, componentObjectName.length() - 4, 4),
					"FOR_PARENT", listParms, false);
			componentInterface.setParentObjectID(parentObjectID);
			// Get the data so the developer doesn't have to call getData ()
			componentInterface.getData();
			component.setComponentInterface(componentInterface);
		}
	}

	/**
	 * Performs the BusinessObject Delete method on all component objects that are registered with the Component Manager service.
	 *
	 */
	protected int componentDelete() throws AmsException, RemoteException {
		boolean errorsFound = false;
		// Loop through all registered components, create the component,
		// and initiate delete processing. NOTE: The method deleteComponent is
		// passed "FALSE" to ensure that only delete logic is executed on the
		// component object.
		Enumeration<ComponentEntry> components = htRegisteredComponents.elements();
		while (components.hasMoreElements()) {
			ComponentEntry component = components.nextElement();
			if (componentDeleteComponent(component.getComponentIdentifier(), false) != 1) {
				errorsFound = true;
			}
		}
		// If errors found, return failure
		if (errorsFound) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * Deletes the component name based on the component id and removes the composite/component object relationship.
	 *
	 *
	 * @param componentIdentifier
	 *            The component identifier as it was registered
	 * @param preDeleteOnly
	 *            in the composite object
	 * @exception ComponentProcessingException
	 * @exception java.rmi.RemoteException
	 */
	protected int componentDeleteComponent(String componentIdentifier, boolean preDeleteOnly) throws AmsException, RemoteException {
		// Get a handle to the component entry
		ComponentEntry component = htRegisteredComponents.get(componentIdentifier);
		// Based on the entry type (BusinessObject or ListingObject), perform
		// the delete processing
		if (!component.isNew()) {
			if (component.getComponentType().equals(AmsConstants.BUSINESS_OBJECT)) {
				BusinessObject componentInterface = null;
				try {
					componentInterface = (BusinessObject) this.getComponentHandle(componentIdentifier);
				} catch (AmsException amsEx) {
					if (amsEx instanceof ComponentNoRelationshipException) {
						debug(this.getClass().getName() + "." + componentIdentifier + "-No Component relationship exists");
						return 1;
					}
				}
				if (!preDeleteOnly) {
					// Remove the object relationship and delete the component
					this.setAttribute(component.getIdentifyingAttribute(), "");
					component.removeComponentInterface();
					return componentInterface.delete();
				} else {
					// Only perform predelete processing on the component
					return componentInterface.preDelete();
				}
			} else {
				// Process the component as a listing object
				ComponentList componentInterface = (ComponentList) this.getComponentHandle(componentIdentifier);

				if (!preDeleteOnly) {
					component.removeComponentInterface();
					return componentInterface.delete();
				} else {
					return componentInterface.preDelete();
				}
			}
		} else {
			component.removeComponentInterface();
			return 0;
		}

	}

	/**
	 * Returns a Vector of component handles which are currently instantiated.<br>
	 *
	 * NOTE: Only the componentEntry is placed into the Vector, not the componentInterface.
	 */
	protected Vector<ComponentEntry> componentGetActiveComponentHandles() {
		Vector<ComponentEntry> activeComponentList = new Vector<>();
		ComponentEntry component = null;
		// Iterate through list of components to see which are active
		Enumeration<ComponentEntry> enumer = htRegisteredComponents.elements();
		while (enumer.hasMoreElements()) {
			component = enumer.nextElement();
			// If the component has been instantiated, add it to the collection
			if (component.isInstantiated()) {
				activeComponentList.addElement(component);
			}
		}
		// Return the component interface
		return activeComponentList;
	}

	/**
	 * Gets the specified attribute value from the specified component object.
	 *
	 * @param componentAttributePath
	 *            The component attribute path (i.e. "Account/account_id" or "AccountList(1)/account_id" )
	 * @return The value of the component attribute
	 * @exception AmsException
	 * @exception RemoteException
	 */
	protected String componentGetComponentAttribute(String componentAttributePath) throws AmsException, RemoteException {
		// Store the first level component name
		String componentIdentifier = StringFunction
				.parseNameFromComponentPath(StringFunction.getFirstComponentEntry(componentAttributePath));
		String componentType = this.componentGetComponentType(componentIdentifier);
		// Based on the component type, call the appropriate method and return
		// the results
		if (componentType.equals(AmsConstants.BUSINESS_OBJECT)) {
			// Call the BusinessObject's getAttribute
			BusinessObject componentInterface = (BusinessObject) this.componentGetComponentHandle(componentIdentifier);
			return componentInterface.getAttribute(StringFunction.removeFirstComponentEntry(componentAttributePath));
		} else {
			// Call the ComponentList's getAttribute
			ComponentList componentInterface = (ComponentList) this.componentGetComponentHandle(componentIdentifier);
			return componentInterface.getAttribute(componentAttributePath);
		}
	}

	/**
	 * Based on the specified componentname, returns a handle to the component entry that represents the component object.
	 *
	 * 
	 * @param componentName
	 *            The name of component as it was registered
	 * @return The ComponentEntry that represents the component object.
	 */
	protected ComponentEntry componentGetComponentEntry(String componentIdentifier) throws AmsException {
		// Parse the name of the component from the ComponentPath (for
		// Listing Components)
		String parsedName = StringFunction.parseNameFromComponentPath(componentIdentifier);
		// Get the component Entry
		if (htRegisteredComponents.containsKey(parsedName)) {
			ComponentEntry component = htRegisteredComponents.get(parsedName);
			return component;
		} else {
			throw new ComponentNotValidException(parsedName, this.getClass().toString());
		}
	}

	/**
	 * Based on the specified componentname, returns a handle to the component object's remote interface.
	 *
	 * @param componentName
	 *            The name of the component in which the remote interface is being reqeusted.
	 */
	protected EJBObject componentGetComponentHandle(String componentIdentifier) throws AmsException, RemoteException {
		// boolean noRelationship = false;
		// EJBObject componentInterface = null;
		ComponentEntry component = this.componentGetComponentEntry(componentIdentifier);
		// If the component hasn't been instantiated, create it
		if (!component.isInstantiated()) {
			this.componentCreateComponent(component);
		}
		// Return the component interface
		return component.getComponentInterface();
	}

	/**
	 * Based on the specified componentname, returns a string that identifies the type of component (BusinessObject/ListingObject)
	 *

	 * @param componentIdentifier
	 *            The identifier the of component as it was registered
	 * @return The Component Type
	 * @exception AmsException
	 */
	protected String componentGetComponentType(String componentIdentifier) throws AmsException {
		ComponentEntry component = this.componentGetComponentEntry(componentIdentifier);
		return component.getComponentType();
	}

	/**
	 * Returns a string array of names of all the components that have been registered with the component manager.
	 *
	 * @return An array of all registered Component Objects
	 */
	protected String[] componentGetRegisteredComponents() throws RemoteException {
		String names[] = new String[htRegisteredComponents.size()];
		int i = 0;
		// Store the keys as an enumeration
		Enumeration<String> components = htRegisteredComponents.keys();
		// For each key, get the name and value
		while (components.hasMoreElements()) {
			String component = components.nextElement();
			names[i] = component;
			i++;
		}
		return names;
	}
	// -------------------------------------------------//
	// Delegated Business Object Methods //
	// -------------------------------------------------//

	/**
	 * Peforms the newObject method on a specified, registered component.
	 *
	 * @param componentPath
	 *            The name of the component in which a new instance is being created
	 * @exception AmsException
	 * @exception RemoteException
	 */
	protected long componentNewComponent(String componentPath) throws AmsException, RemoteException {
		long newComponentID;
		// Determine the top-level and remaining component paths
		String topLevelComponent = StringFunction.getFirstComponentEntry(componentPath);
		String remainingComponentPath = StringFunction.removeFirstComponentEntry(componentPath);
		// Get a handle to the component entry. An exception is thrown if the
		// component isn't registered with the component manager
		ComponentEntry component = this.componentGetComponentEntry(topLevelComponent);
		String componentType = component.getComponentType();
		// Set the component as a new component if there is no remaining
		// component path
		if ((remainingComponentPath == null) || (remainingComponentPath.length() < 1)) {
			component.setNew();
		}
		// If the component is a business object, process accordingly
		if (componentType.equals(AmsConstants.BUSINESS_OBJECT)) {
			BusinessObject componentInterface = (BusinessObject) this.componentGetComponentHandle(topLevelComponent);
			// If there is a remaining component path, call new component on
			// the component object
			if (remainingComponentPath.length() > 0) {
				newComponentID = componentInterface.newComponent(remainingComponentPath);
			} else {
				// Create a new instance of the component
				newComponentID = componentInterface.newObject();
				// Set the object relationships
				this.setAttribute(component.getIdentifyingAttribute(), Long.toString(newComponentID));
			}
		} else {
			// The component is a listing object, process accordingly
			ComponentList componentInterface = (ComponentList) this
					.componentGetComponentHandle(StringFunction.parseNameFromComponentPath(topLevelComponent));
			newComponentID = componentInterface.newComponent(componentPath);
		}
		// Return the new object ID
		return newComponentID;
	}

	/**
	 * Performs the BusinessObject preDelete method on all component objects that are registered with the Component Manager service.
	 * The preDelete method ensures that no Delete process occurs if speciailizes preDelete logic has been implemented in objects
	 * that are maintained by the Component Manager.
	 *

	 */
	protected int componentPreDelete() throws AmsException, RemoteException {
		boolean errorsFound = false;
		// Loop through all registered components, create the component, and
		// initiate preDelete processing. NOTE: The method deleteComponent is
		// passed "TRUE" to ensure that only preDelete logic is executed on
		// the component object.
		Enumeration<ComponentEntry> components = htRegisteredComponents.elements();
		while (components.hasMoreElements()) {
			ComponentEntry component = components.nextElement();
			if (componentDeleteComponent(component.getComponentIdentifier(), true) != 1) {
				errorsFound = true;
			}
		}
		// If errors found, return failure
		if (errorsFound) {
			debug(this.getClass().getName() + " - Errors found in preDelete ()");
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * This method initiates validate/save logic (depending on the processing type specified) for all components that have been
	 * modified in the registry of the Component Manager. This method loops through all instantiated and modified components
	 * initiates the appropriate component processing method.
	 *
	 * @return 1 for Success, -1 for Failure
	 * @exception AmsException,
	 *                RemoteException
	 */
	protected int componentProcessComponents(int processType) throws RemoteException, AmsException {
		boolean errorsFound = false;
		boolean tempErrorsFound = false;
		Enumeration<ComponentEntry> components = htRegisteredComponents.elements();
		while (components.hasMoreElements()) {
			ComponentEntry component = components.nextElement();
			if (component.isInstantiated()) {
				// Process the component as a Listing Component
				if (component.getComponentType().equals(AmsConstants.LISTING_OBJECT)) {
					ComponentList componentInterface = (ComponentList) component.getComponentInterface();
					// Based on the processing type, call the appropriate
					// ComponentList method
					switch (processType) {
					case AmsConstants.SAVE:
						tempErrorsFound = (componentInterface.save() == -1);
						if (tempErrorsFound) {
							errorsFound = true;
						}
						break;
					case AmsConstants.VALIDATE:
						tempErrorsFound = (componentInterface.validate() == -1);
						if (tempErrorsFound) {
							errorsFound = true;
						}
						break;
					default:
						errorsFound = true;
					}
				} else {
					// Process the component as a business object
					BusinessObject componentInterface = (BusinessObject) component.getComponentInterface();
					switch (processType) {
					// Based on the processing type, call the
					// appropriate BusinessObject method
					case AmsConstants.SAVE:
						tempErrorsFound = (componentInterface.save() == -1);
						if (tempErrorsFound) {
							errorsFound = true;
						}
						break;
					case AmsConstants.VALIDATE:
						tempErrorsFound = (componentInterface.validate() == -1);
						if (tempErrorsFound) {
							errorsFound = true;
						}
						break;
					default:
						errorsFound = true;
					}
				}

				// Return here. Otherwise if multiple components
				// fail and all try to do setRollbackOnly (), it throws uncaught exception.
				if (processType == AmsConstants.SAVE && errorsFound) {
					break;
				}

			}
		}
		// If errors were encountered, return failure
		if (errorsFound) {
			return -1;
		} else {
			return 1;
		}
	}
	// -------------------------------------------------//
	// Component Management Methods //
	// -------------------------------------------------//

	/**
	 * Registers a component object with the component manager.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component being registered
	 * @param componentName
	 *            The name of the component being registered
	 * @param componentType
	 *            The type of component (BusinessObject, ListingObject)
	 * @param identifyingAttribute
	 *            The attribute which is used to identify a component instance
	 */
    protected void componentRegisterComponent(String componentIdentifier, String componentName,
            String componentType, String identifyingAttribute) {
		// Based on the parameters provided, create a new component entry
		ComponentEntry entry = new ComponentEntry();
		entry.setComponentIdentifier(componentIdentifier);
		entry.setComponentName(componentName);
		entry.setComponentType(componentType);
		entry.setIdentifyingAttribute(identifyingAttribute);
		// Place the new entry in the Hashtable
		htRegisteredComponents.put(componentIdentifier, entry);
	}

	/**
	 * Performs the BusinessObjectBean save method on all component objects that have been instantiated within the component
	 * manager.
	 *
	 */
	protected int componentSave() throws RemoteException, AmsException {
		return this.componentProcessComponents(AmsConstants.SAVE);
	}

	/**
	 * Sets the specified attribute value from the specified component object.
	 *
	 * @param componentAttributePath
	 *            The component attribute path (i.e. "Account/account_id" or "AccountList(1)/account_id" )
	 * @param attributeValue
	 * @return The value of the component attribute
	 * @exception ComponentProcessingException
	 * @exception AmsException
	 * @exception java.rmi.RemoteException
	 */
	protected void componentSetComponentAttribute(String componentAttributePath, String attributeValue)
			throws RemoteException, AmsException {
		// Ensure that the component path is valid (No occurrences of "//")
		if (!StringFunction.validateComponentPath(componentAttributePath)) {
			throw new ComponentProcessingException(
					"Component path: " + componentAttributePath + " is not valid for object: " + this.getClass());
		}
		// Store the first level component name
		String componentIdentifier = StringFunction
				.parseNameFromComponentPath(StringFunction.getFirstComponentEntry(componentAttributePath));
		String componentType = this.componentGetComponentType(componentIdentifier);
		// Based on the component type, call the appropriate method and return
		// the results
		if (componentType.equals(AmsConstants.BUSINESS_OBJECT)) {
			// If the no object relationship exists, assume that a component
			// object is to be created
			String idAttribute = this.componentGetComponentEntry(componentIdentifier).getIdentifyingAttribute();
			String componentOID = this.getAttribute(idAttribute);
			if ((componentOID == null) || (componentOID.length() < 1)) {
				// Create the component object
				this.newComponent(componentIdentifier);
			}
			// Call the BusinessObject's setAttribute
			BusinessObject componentInterface = (BusinessObject) this.componentGetComponentHandle(componentIdentifier);
			componentInterface.setAttribute(StringFunction.removeFirstComponentEntry(componentAttributePath), attributeValue);
		} else {
			// Call the ComponentList's getAttribute
			ComponentList componentInterface = (ComponentList) this.componentGetComponentHandle(componentIdentifier);
			componentInterface.setAttribute(componentAttributePath, attributeValue);
		}
	}

	/**
	 * Performs the BusinessObjectBean validate method on all component objects that have been instantiated within the component
	 * manager.
	 *
	 */
	protected int componentValidate() throws RemoteException, AmsException {
		return this.componentProcessComponents(AmsConstants.VALIDATE);
	}

	/**
	 * Creates an instance of the Business Object Component Manager service component.
	 *
	 */
	protected void createComponentManager() throws RemoteException, AmsException {
	}

	/**
	 * This method logs debugging messages if the object is currently in the "Debug" mode. This mode is set in the BusinessObject's
	 * deployment descriptor file. The descriptor's environment variable "DebugMode" indicates if debugging is on for the the EJB
	 * component.
	 *
	 * @param debugMessage
	 *            The debugger message that will be logged
	 */
	protected void debug(String debugMessage) {
		if (this.debuggingOn) {
			if (debuggerService == null) {
				debuggerService = new FrameworkDebugger();
				debuggerService.setDebugMode(this.debugMode); // W Zhu 4/6/12 Rel 8.0 Sys Test troubleshooting
			}
			// Call the Debugger's debug method
			debuggerService.debug(this.getClass().getName(), this.debugMode, this.ecsgObjectName + " " + debugMessage);
		}
	}

	/**
	 * Deletes the currently instantiated business object from its persistant storage. For registered component of the business
	 * object being deleted, the component business object is deleted as well.
	 *
	 * Prior to deleting the object, user-specified preDelete logic is executed. If errors are encountered during preDelete, the
	 * transaction is rolled back, and the object is not deleted.
	 *
	 */
	public int delete() throws RemoteException, AmsException {
		try {
			boolean errorsFound = false;
			// Throw an exception if the object hasn't been instantiated
			// (GetData called)
			if (!objectInstantiated) {
				throw new AmsException("Object must be instantiated before the delete method is invoked");
			}
			// Perform pre-delete processing
			if (!this.isComponent()) {
				errorsFound = (this.preDelete() == -1);
			}
			// Perform delete processing on the component manager (if appropriate)
			if (!errorsFound) {
				if (this.componentDelete() == -1) {
					debug(this.getClass().getName() + "-Error encountered during component Delete");
					errorsFound = true;
				}
			}
			// Delete the business object's data
			if (!errorsFound) {
				// Update the business object's data
				try {
					this.dataObjectDelete();
				} catch (AmsException amex) {
					errorsFound = true;
				}
			}
			// Return success/failure
			if (errorsFound) {
				LOG.error("{} - delete() Rolling back transaction.", this.getClass().getName());
				this.getSessionContext().setRollbackOnly();
				return -1;
			} else {
				// If this is the parent object, put a message in the error list
				// that this object was updated. This will notify the web
				// infrastructure of an update so JavaBeans can be refreshed if
				// necessary.
				try {
					String theName = this.ecsgObjectName;
					String theOid = attributeMgr.getIDAttribute().getAttributeName();
					String strOid = getAttribute(theOid);
				} catch (Exception ex) {
				}
			}
		} catch (AmsException amsEx) {
			LOG.error("{} - Rolling back transaction because of " ,this.getClass().getName(), amsEx);
			this.getSessionContext().setRollbackOnly();
		}
		return 1;
	}

	/**
	 * Based on the specified component name, this method deletes the component and any of it's component objects.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component
	 */
	public int deleteComponent(String componentIdentifier) throws AmsException, RemoteException {
		this.componentDeleteComponent(componentIdentifier, false);
		// Return Success
		return 1;
	}

	@Override
	public void ejbActivate() {
	}

	// Create
	public void ejbCreate() throws RemoteException, CreateException, AmsException {
		// Perform any post-creation processing
		this.initObject();
	}

	public void ejbCreate(ClientServerDataBridge csdb) throws RemoteException, CreateException, AmsException {
		// Perform any post-creation processing
		this.setClientServerDataBridge(csdb);
		this.initObject();
	}

	public void ejbCreate(long objectID) throws RemoteException, CreateException, AmsException {
		// Perform post-create prcoessing
		this.initObject();
		// Get the Data for this object
		this.getData(objectID);
	}

	@Override
	public void ejbPassivate() {
		// Clear out the objectData and objectKey so passivation works
		this.objectData = null;
		this.objectKey = null;
		this.objectRecord = null;
	}

	// EJB Methods
	@Override
	public void ejbRemove() throws RemoteException {
		dbmsConnection = null;
		Enumeration<ComponentEntry> components = htRegisteredComponents.elements();
		while (components.hasMoreElements()) {
			ComponentEntry component = components.nextElement();
			// Based on the component Type (Listing/Business Object)
			// remove it
			EJBObject componentInterface = component.getComponentInterface();
			// If the component has been instantiated, remove it's reference
			if (componentInterface != null) {
				try {
					componentInterface.remove();
				} catch (Exception e) {
				}
			}
		}

		Enumeration<EJBObject> additionalBOs = additionalBusinessObjects.elements();
		while (additionalBOs.hasMoreElements()) {
			EJBObject tempEJB = additionalBOs.nextElement();
			try {
				tempEJB.remove();
			} catch (Exception e) {
				LOG.error("Exception occured removing tempEjb", e);
			}
		}
	}

	/**
	 * This method "finds" and gets the data associated with an instance of the object based on a unique
	 * attributeName/attributeValue pair.
	 *
	 * If a unique object is found, the getData() method is called once the object identifying value (OID) is determined.
	 *
	 * @param attributeName
	 *            The name of the attribute being specified for the find
	 * @param attributeValue
	 *            The value of the attribute being specified for the find
	 */
	public void find(String attributeName, String attributeValue) throws AmsException, RemoteException {
		// Get the Physical attribute name for the parameter attribute
		String physicalAttributeName = attributeMgr.getPhysicalName(attributeName);
		// Get the ObjectID attribute name
		String idAttributeName;
		try {
			idAttributeName = this.getIDAttributeName(true);
		} catch (Exception e) {
			throw new AmsException("Errors encountered while getting the IDAttributeName", e);
		}
		// Build SQL to pass to the framework's QueryListView
		String sql = "SELECT " + idAttributeName + " FROM " + this.dataTableName 
		        + " WHERE " + physicalAttributeName + " = '" + attributeValue + "'";
		// Create a QueryListView and set the SQL
		QueryListView listView = (QueryListView) EJBObjectFactory.createServerEJB(getSessionContext(), "QueryListView");
		// Set the SQL and get the records from the list view
		listView.setSQL(sql);
		listView.getRecords();
		// Determine the number of records found, and process accordingly
		int recordCount = listView.getRecordCount();
		if (recordCount > 1 || recordCount == 0) {
			try {
				listView.remove();
			} catch (RemoveException reEx) {
			}
			// Indicate that errors were encountered in processing
			if (recordCount > 1) {
				throw new NameAndValueNotUniqueException(attributeName, attributeValue);
			} else {
				throw new ObjectNotFoundException(attributeName, attributeValue);
			}
		} else {
			listView.scrollToRow(0);
			long id = Long.parseLong(listView.getRecordValue(idAttributeName));
			try {
				listView.remove();
			} catch (RemoveException reEx) {
			}
			this.getData(id);
		}
	}

	/**
	 * Checks to see if the attribute has been modified
	 *
	 * @param attributeName
	 *            The name of the attribute being examined.
	 * @return True/False based on whether or not the attribute has been modified.
	 */
	public boolean isAttributeModified(String attributeName) throws RemoteException, AmsException {
		return attributeMgr.isAttributeModified(attributeName);
	}

	// -------------------------------------------------//
	// Attribute Management Methods //
	// -------------------------------------------------//
	/**
	 * Get the value of a specified attribute name that has been registered with the business object or one of it's components.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. first_name or /Account/account_id)
	 * @return The value of the attribute requested
	 */
	public String getAttribute(String attributePath) throws RemoteException, AmsException {
		String value;
		// Determine if the attribute path indicates component processing
		if (StringFunction.requiresComponentParsing(attributePath)) {
			// Get the component attribute value
			value = this.componentGetComponentAttribute(attributePath);
		} else {
			// Get the BusinessObject's attribute value
			value = attributeMgr.getAttributeValue(attributePath);
		}
		return value;
	}

	/**
	 * Get the original value of a specified attribute name that has been registered with the business object or one of it's
	 * components. The original value is the value that was read from the database.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. first_name or /Account/account_id)
	 * @return The value of the attribute requested
	 */
	public String getAttributeOriginalValue(String attributePath) throws RemoteException, AmsException {

		String value;
		// Determine if the attribute path indicates component processing
		if (StringFunction.requiresComponentParsing(attributePath)) {
			// Get the component attribute value
			value = this.componentGetComponentAttribute(attributePath);
		} else {
			// Get the BusinessObject's attribute value
			value = attributeMgr.getOriginalAttributeValue(attributePath);
		}
		return value;
	}

	/**
	 * Get the value of a specified attribute name that has been registered with the business object or one of it's components as a
	 * byte DataType.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. active_date or /Account/active_date)
	 * @return The byte[] value of the attribute requested
	 */
	public byte[] getAttributeBytes(String attributePath) throws RemoteException, AmsException {
		String attributeStr = this.getAttribute(attributePath);
		if (attributeStr != null && attributeStr.length() > 0) {
			return com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(attributeStr);
		}
		return null;
	}

	/**
	 * Get the value of a specified attribute name that has been registered with the business object or one of it's components as a
	 * Date DataType.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. active_date or /Account/active_date)
	 * @return The Date value of the attribute requested
	 */
	public Date getAttributeDate(String attributePath) throws AmsException, RemoteException {
		String value = this.getAttribute(attributePath);
		if (value == null || value.length() == 0) {
			return null;
		}
		return DateTimeUtility.convertStringDateToDate(value);
	}

	/**
	 * Get the value of a specified attribute name that has been registered with the business object or one of it's components as a
	 * Date DataType.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. active_date or /Account/active_date)
	 * @return The Date value of the attribute requested
	 */
	public Date getAttributeDateTime(String attributePath) throws AmsException, RemoteException {
		String value = this.getAttribute(attributePath);
		if (value == null || value.length() == 0) {
			return null;
		}
		return DateTimeUtility.convertStringDateTimeToDate(value);
	}

	/**
	 * Get the value of a specified attribute name that has been registered with the business object or one of it's components as a
	 * BigDecimal DataType.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. amount or /Account/amount_due)
	 * @return The value of the attribute requested
	 */
	public BigDecimal getAttributeDecimal(String attributePath) throws AmsException, RemoteException {
		String attributeValue = this.getAttribute(attributePath);
		// If the value isn't set, return a value of 0
		if (attributeValue == null || attributeValue.length() < 1) {
			return BigDecimal.ZERO;
		}
		// Convert the string attribute value to a BigDecimal. If an exception
		// is thrown catch it and throw an AMS Exception
		BigDecimal value = null;
		try {
			value = new BigDecimal(attributeValue);
		} catch (NumberFormatException ex) {
			throw new AmsException("The attribute: " + attributePath + " cannot be converted to a Decimal DataType", ex);
		}
		return value;
	}

	/**
	 * Gets a handle to the business object's attribute manager's registered attribute Hash.
	 *
	 * @return AttributeManager's registered attribute Hashtable
	 */
	public Hashtable<String, Attribute> getAttributeHash() {
		return this.attributeMgr.getAttributeHashtable();
	}

	/**
	 * Get the value of a specified attribute name that has been registered with the business object or one of it's components as a
	 * Long DataType.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. registry or /Account/registry)
	 * @return The value of the attribute requested
	 */
	public int getAttributeInteger(String attributePath) throws AmsException, RemoteException {
		int value;
		try {
			value = Integer.parseInt(this.getAttribute(attributePath));
		} catch (NumberFormatException ex) {
			throw new AmsException("The attribute: " + attributePath + " cannot be converted to a Integer DataType", ex);
		}
		return value;
	}

	/**
	 * Get the value of a specified attribute name that has been registered with the business object or one of it's components as a
	 * Long DataType.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. registry or /Account/registry)
	 * @return The value of the attribute requested
	 */
	public long getAttributeLong(String attributePath) throws AmsException, RemoteException {
		long value;
		try {
			value = Long.parseLong(this.getAttribute(attributePath));
		} catch (NumberFormatException ex) {
			throw new AmsException("The attribute: " + attributePath + " cannot be converted to a long DataType", ex);
		}
		return value;
	}

	/**
	 * Gets a NameValue Hashtable for each attribute that is requested.
	 *
	 * @param attributePaths
	 *            The String array of requested attributes
	 * @return A hashtable indexed by AttributeName that contains the corresponding values.
	 */
	public Hashtable<String, String> getAttributes(String[] attributePaths) throws RemoteException, AmsException {
		int i = 0;
		int count = attributePaths.length;
		Hashtable<String, String> htNameValue = new Hashtable<>(count);
		// For each attribute path requested, store the value in the hash
		for (i = 0; i < count; i++) {
			htNameValue.put(attributePaths[i], this.getAttribute(attributePaths[i]));
		}
		return htNameValue;
	}

	/**
	 *
	 */
	public ClientServerDataBridge getClientServerDataBridge() {
		return csdb;
	}
	// -------------------------------------------------//
	// Component Object Methods //
	// -------------------------------------------------//

	/**
	 * Based on the componentIdentifier supplied, returns the component's remote interface.
	 *
	 * @param componentIdentifier
	 *            The name of the component as it was registered in the business object's registerComponents() method.
	 */
	public EJBObject getComponentHandle(String componentIdentifier) throws RemoteException, AmsException {
		return this.componentGetComponentHandle(componentIdentifier);
	}

	/**
	 * Based on the list componentIdentifier supplied, returns the remote interface for the specified business object in the list.
	 * Note that this method is to be used for list components only.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component as it was registered in the business object's registerComponents() method.
	 * @param objectID
	 *            The OID of the component object being requested
	 * @return The remote interface for the object within the component list.
	 *
	 *
	 */
	public EJBObject getComponentHandle(String componentIdentifier, long objectId) throws RemoteException, AmsException {
		ComponentList compList = (ComponentList) this.componentGetComponentHandle(componentIdentifier);

		return compList.getComponentObject(objectId);
	}

	/**
	 * Based on the componentIdentifier supplied, returns the component type.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component as it was registered with the business object's registerComponents() method.
	 * @return The type of component (either listing or business object)
	 */
	public String getComponentType(String componentIdentifier) throws RemoteException, AmsException {
		return this.componentGetComponentType(componentIdentifier);
	}

	// -------------------------------------------------//
	// Object retrieval/load Methods //
	// -------------------------------------------------//
	/**
	 * For a given instance of the object, this method loads the corresponding object's data.
	 *
	 * The data for a particular instance of an object (usually identified by its object identifier) is loaded into the object's
	 * attribute manager. This data is then accessible via the object's get/set attribute methods. If the object identifier is not
	 * valid for the object, an exception is thrown.
	 *
	 * @param OID
	 *            The objectID of the object being instantiated.
	 */
	public void getData(long OID) throws RemoteException, AmsException {
		// Indicate that this is not a new instance of the object
		this.isNew = false;
		this.setObjectID(OID);
		// If the objectID has not been provided, return Failure
		if (this.getObjectID() == 0) {
			throw new InvalidObjectIdentifierException();
		}

		// Get the data for the object
		this.dataObjectGetData();
		// Indicate that the object has been instantiated
		this.objectInstantiated = true;

	}

	public String getDataTableName() {
		return this.dataTableName;
	}
	// -------------------------------------------------//
	// Error Management Methods //
	// -------------------------------------------------//

	/**
	 * Returns a handle to the resource manager for this business object This is the version called by the remote interface.
	 *
	 * @return ResourceManager
	 */
	public ResourceManager getResourceMgr() throws RemoteException {
		return this.getResourceManager();
	}

	/**
	 * Returns a handle to the resource manager for this business object
	 *
	 * @return ResourceManager
	 */
	public ResourceManager getResourceManager() {
		if (this.resMgr == null) {
			this.resMgr = new ResourceManager(this.csdb);
		}
		return this.resMgr;
	}

	/**
	 * Returns a handle to the error manager for this business object
	 *
	 * @return ErrorManager
	 */
	public ErrorManager getErrorManager() throws RemoteException {
		return errMgr;
	}

	/**
	 * Gets the logical name of the object Identifying attribute
	 *
	 * @return The logical name of the ObjectID Attribute
	 */
	public String getIDAttributeName() throws RemoteException, AmsException {
		return this.getIDAttributeName(false);
	}

	/**
	 * Based on the attributeType (logical/physical) indicator, get the name of the attribute registered as the object identifier.
	 *
	 * @return The name of the ObjectID Attribute
	 * @param physicalName
	 *            If true, the physical name is retrieved, otherwise the logical name is retrieved.
	 *
	 */
	protected String getIDAttributeName(boolean physicalName) throws RemoteException, AmsException {
		Attribute IDAttribute = attributeMgr.getIDAttribute();
		if (physicalName) {
			return IDAttribute.getPhysicalName();
		} else {
			return IDAttribute.getAttributeName();
		}
	}

	/**
	 * Returns the collection of errors which have been issued within this business object.
	 *
	 * @return Vector of IssuedError objects
	 */
	public List<IssuedError> getIssuedErrors() throws RemoteException {
		List<IssuedError> overallErrorList = getErrorManager().getIssuedErrorsList();

		// If the object registered any additional businessobject collect
		// the errors.
		Enumeration<EJBObject> additionalBOs = additionalBusinessObjects.elements();
		while (additionalBOs.hasMoreElements()) {
			try {
				BusinessObject tempEJB = (BusinessObject) additionalBOs.nextElement();
				overallErrorList.addAll(tempEJB.getIssuedErrors());

			} catch (NoSuchObjectException nsoe) {
				// Catch this in case the bean has already been removed
			}
		}

		// If the object has a component manager, get the errors issued for all
		// active components
		Enumeration<ComponentEntry> componentEnum = this.componentGetActiveComponentHandles().elements();
		while (componentEnum.hasMoreElements()) {
			// For each active component, add the list of issued errors
			// to the overall list
			ComponentEntry component = componentEnum.nextElement();
			if (component.getComponentType().equals(AmsConstants.BUSINESS_OBJECT)) {
				BusinessObject componentInterface = (BusinessObject) component.getComponentInterface();
				overallErrorList.addAll(componentInterface.getIssuedErrors());

			} else {
				ComponentList componentInterface = (ComponentList) component.getComponentInterface();
				overallErrorList.addAll(componentInterface.getIssuedErrors());

			}
		}
		// Return the error Vector
		return overallErrorList;
	}

	/**
	 * Descendent implemented method that identifies listing object criteria.
	 *
	 * Descendant objects will implement this method to identify one or more listTypes and list processing criteria.
	 *
	 * @param listTypeName
	 *            The name of the listtype (as specified by the developer)
	 * @return The qsuedo SQL used for specifying the list criteria.
	 *
	 */
	public String getListCriteria(String listTypeName) {
		return AmsConstants.STANDARD_LIST;
	}

	public long getObjectID() {
		return this.objectID.longValue();
	}

	/**
	 * Gets the logical or physical name of the parent object Identifying attribute.
	 *
	 * If the boolean argument physicalName is TRUE, then return the physical name of the ParentIDAttribute. Otherwise, return the
	 * logical name of the ParentIDAttribute.<br>
	 * NOTE: An object can have only 1 ParentIDAttribute registered.
	 *
	 * @return The name of the ParentID Attribute
	 */
	public String getParentIDAttributeName(boolean physicalName) throws RemoteException, AmsException {
		// Get a handle to the ParentID Attribute
		Attribute IDAttribute = attributeMgr.getParentIDAttribute();
		if (physicalName) {
			// Get the physicall name
			return IDAttribute.getPhysicalName();
		} else {
			// Get the logcialName
			return IDAttribute.getAttributeName();
		}
	}

	/**
	 * Gets the names of all attributes registered with the business object.
	 *
	 * @return A String array of logical attribute names that have been registered in the business object's registerAttributes()
	 *         method.
	 */
	public String[] getRegisteredAttributes() {
		return this.attributeMgr.getRegisteredAttributes();
	}

	/**
	 * This method returns the EJB context object for this component. This is needed to pass to the EJBObjectFactory to create other
	 * server EJB components in the same transaction context.
	 *
	 */
	protected SessionContext getSessionContext() {
		return myContext;
	}

	/**
	 * This method indicates if any changes are pending for business object.
	 *
	 * @return True if changes are pending, False if no changes are pending.
	 *
	 */
	public boolean hasChangesPending() {
		return attributeMgr.getModifiedCount() > 0 || newComponentCalled;
	}

	// -------------------------------------------------//
	// Object initialization Methods //
	// -------------------------------------------------//
	/**
	 * Performs all initialization functionality for the business object. Performs attribute registration, component registration,
	 * and general object setup.
	 *
	 * In addition, the userInit() method is triggered. The userInit() "hook" method is developed in descendants of
	 * BusinessObjectBean and any functionality developed within that method will be triggered as a result.
	 *
	 */
	protected void initObject() throws RemoteException, AmsException {
		// Set the locale on the attribute manager
		if (this.csdb != null) {
			attributeMgr.setLocaleName(csdb.getLocaleName());
		}
		// Register the attributes for the object
		registerAttributes();
		attributeMgr.resetModified();
		// Register the components for the object
		if (htRegisteredComponents == null) {
			htRegisteredComponents = new Hashtable<>();
		}
		// Register components for this instance of the object
		registerComponents();
		// Initialize error manager
		if (errMgr == null) {
			errMgr = new ErrorManager();
		}
		if (this.csdb != null) {
			if (csdb.getLocaleName() != null) {
				errMgr.setLocaleName(csdb.getLocaleName());
			}
		}
		errMgr.initialize();
		// Initialize instance variables
		this.isNew = false;
		this.newComponentCalled = false;
		this.componentObject = false;
		this.objectInstantiated = false;

		objectKey = new KeyDef();
		// W Zhu 8/27/12 Rel 8.1 PPX-173 allow an object not have ObjectIDAttribute defined. It cannot use open() but otherwise is
		// ok.
		Attribute idAttribute = attributeMgr.getIDAttribute();
		if (idAttribute != null) {
			objectKey.addAttrib(idAttribute.getAttributeName());
		}

		// Perform user Initialization
		userInitObject();
	}

	// Determines if the object is a component object

	public boolean isComponent() {
		return this.componentObject;
	}

	/**
	 * @return true if the object is new
	 *
	 */
	public boolean isNewObject() throws RemoteException {
		return this.isNew;
	}

	/**
	 * Based on the specified component identifier, this method sets the values for the specified attributename array on the
	 * component object.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component as it was registered in the object's registerComponents() method.
	 * @return The objectID for the newly created component.
	 *
	 */
	public long newComponent(String componentIdentifier) throws AmsException, RemoteException {
		// Throw an exception if the object hasn't been instantiated
		// (GetData/NewObject)
		if (!objectInstantiated) {
			throw new AmsException("Object must be instantiated before a component is created");
		}
		long newComponentID = 0;
		newComponentID = this.componentNewComponent(componentIdentifier);
		// Indicate that the object has changed
		this.newComponentCalled = true;
		return newComponentID;
	}
	// -------------------------------------------------//
	// Object Creation/Deletion //
	// -------------------------------------------------//

	/**
	 * Creates a new instance of the business object and generated a new unique object identifier.
	 *
	 * @return The newly generated object identifier.
	 */
	public long newObject() throws RemoteException, AmsException {
		try {
			// Indicate that the object has been instantiated
			this.objectInstantiated = true;
			// Indicate that this is a new instance of the object
			// (a new row will be created upon save)
			this.isNew = true;
			// Generate a new Object Identifier
			long objectID = this.generateObjectID();
			this.setObjectID(objectID);
			this.setAttribute(getIDAttributeName(), Long.toString(objectID));
			// Perform any user specific newObject functionality
			this.userNewObject();
			// Return the new Object ID
			return objectID;
		} catch (AmsException amsEx) {
			// If an exception was thrown, change the instantiation
			// status of the object
			this.objectInstantiated = false;
			throw amsEx;
		}
	}

	/**
	 * This method takes a document and populates this business object's attributes from the values in the document starting at the
	 * root level of the document. The subdocument under the business Object's name is used to populate this object. I.E. if this is
	 * the 'Account' business object, the subdocument '/Account/' is used to populate the object.
	 * <p>
	 * *-- / (root)<br>
	 * &nbsp;*-- Account/<br>
	 * &nbsp;&nbsp;*-- oid = 222<br>
	 * &nbsp;&nbsp;*-- name = Daddy Big Bucks<br>
	 *
	 * @param doc
	 *            The source DocumentHandler object that will be used to populated the object's attributes.
	 */
	public void populateFromXmlDoc(DocumentHandler doc) throws RemoteException, AmsException {
		this.populateFromXmlDoc(doc, "", null);
	}

	/**
	 * This method takes a document and populates this business object's attributes from the values in the document starting at the
	 * specified component path. Note that if the component path does not end with a slash '/', the object name is appended to it.
	 * Therefor a component path of '/Account' would become a component path of '/Account/ObjectName/', while '/Account/' would
	 * remain unchanged.
	 *
	 * @param doc
	 *            The source DocumentHandler object that will be used to populated the object's attributes.
	 * @param componentPath
	 *            The component path to go to to perform the business object population. See the method documentation for a note on
	 *            how the format of this variable is relevant.
	 */
	public void populateFromXmlDoc(DocumentHandler doc, String componentPath) throws RemoteException, AmsException {
		this.populateFromXmlDoc(doc, componentPath, null);
	}

	/**
	 * This method takes a document and populates this business object's attributes from the values in the document starting at the
	 * specified component path. Note that if the component path does not end with a slash '/', the object name is appended to it.
	 * Therefor a component path of '/Account' would become a component path of '/Account/ObjectName/', while '/Account/' would
	 * remain unchanged.
	 *
	 * @param doc
	 *            The source DocumentHandler object that will be used to populated the object's attributes.
	 * @param componentPath
	 *            The component path to go to to perform the business object population. See the method documentation for a note on
	 *            how the format of this variable is relevant.
	 * @param tagTranslator
	 *            XMLTagTranslater - an adaptor that translates the XML node tag name to Object attribute/component name, and tag
	 *            value to attribute valu. If the attribute name is null, this node is ignored.
	 */
	public void populateFromXmlDoc(DocumentHandler doc, String componentPath, XMLTagTranslator tagTranslator)
			throws RemoteException, AmsException {
		String ecsgObjectName = this.ecsgObjectName;
		if (!(componentPath.endsWith("/"))) {
			StringBuilder addlPath = new StringBuilder(componentPath);
			addlPath.append('/');
			addlPath.append(ecsgObjectName);
			componentPath = addlPath.toString();
		}
		Hashtable<String, Attribute> attrList = attributeMgr.getAttributeHashtable();
		DocumentNode node = doc.getDocumentNode(componentPath);
		if (node == null) {
		} else {
			Vector<String> processedListComponents = new Vector<>();
			node = node.getLeftChildNode();
			while (node != null) {

				String tagName, value, attributeComponentName;
				tagName = node.getTagName();
				attributeComponentName = tagName;
				value = node.getNodeValue(false);

				// tagTranslater is an adaptor class that translates the tagName to attributeName, and tag value to attributeValue.
				// If returned attributeName is null, ignore this node.
				// This is useful, for example, for agent unpackaging incoming messages.
				if (tagTranslator != null) {
					tagTranslator.translateTag(tagName, value);
					attributeComponentName = tagTranslator.getAttributeName();
					value = tagTranslator.getAttributeValue();
					if (attributeComponentName == null) {
						node = node.getRightSiblingNode(); // skip to the next node
						continue;
					}
				}

				// if this is an end node, set attributes on the object.
				if (node.getLeftChildNode() == null) {
					if (attrList.containsKey(attributeComponentName)) {
						if (!(attributeComponentName.equals(attributeMgr.getIDAttribute().getAttributeName()))) {
							setAttribute(attributeComponentName, value);
						}
					}
				} // If there are sub-nodes, this is a component. Set attributes on components.
				else {
					try {
						String type = getComponentType(attributeComponentName);
						if (type.equals(AmsConstants.BUSINESS_OBJECT)) {
							BusinessObject component = null;
							try {
								component = (BusinessObject) getComponentHandle(attributeComponentName);
							} catch (ComponentNoRelationshipException ex) {
								this.newComponent(attributeComponentName);
								component = (BusinessObject) getComponentHandle(attributeComponentName);
							}
							StringBuilder nextPath = new StringBuilder(componentPath);
							if (!(componentPath.endsWith("/"))) {
								nextPath.append('/');
							}
							nextPath.append(tagName);
							nextPath.append('/');
							component.populateFromXmlDoc(doc, nextPath.toString(), tagTranslator);
						} else if (type.equals(AmsConstants.LISTING_OBJECT)) {
							if (!(processedListComponents.contains(attributeComponentName))) {
								ComponentList component = null;
								try {
									component = (ComponentList) getComponentHandle(attributeComponentName);
								} catch (ComponentNoRelationshipException ex) {
									this.newComponent(attributeComponentName);
									component = (ComponentList) getComponentHandle(attributeComponentName);
								}
								StringBuilder nextPath = new StringBuilder(componentPath);
								if (!(componentPath.endsWith("/"))) {
									nextPath.append('/');
								}
								nextPath.append(tagName);
								nextPath.append('/');
								String tempPath = nextPath.toString();

								List<DocumentHandler> docList = doc.getFragmentsList(tempPath);
								for (DocumentHandler tempDoc : docList) {
									String IDAttributeName = component.getIDAttributeName();
									String strObjectId = tempDoc.getAttribute("/".concat(IDAttributeName));
									BusinessObject busObj = null;
									long objectId;
									try {
										objectId = Long.parseLong(strObjectId);
										// Get a handle to the appropriate business object
										busObj = (BusinessObject) this.getComponentHandle(attributeComponentName, objectId);
									} catch (InvalidObjectIdentifierException | NumberFormatException ex) {
										objectId = this.newComponent(attributeComponentName);
										busObj = (BusinessObject) this.getComponentHandle(attributeComponentName, objectId);
									}

									// Populate this business object from the document
									busObj.populateFromXmlDoc(tempDoc, "/", tagTranslator);

									processedListComponents.addElement(attributeComponentName);
								}
							}
						}
					} catch (AmsException ex) {
						LOG.error("Exception occured in populateFromXmlDoc() ", ex);
					}
				}
				node = node.getRightSiblingNode();
			}
		}
	}
	// -------------------------------------------------//
	// List object Population Methods //
	// -------------------------------------------------//

	/**
	 * This method populates a buisness object's state from data that has already been populated in a listing object (either
	 * ComponentList or GenericList).
	 *
	 * @param objectID
	 *            The objectID of the object that is to be created
	 *
	 */
	public void populateObjectFromList(long objectID) throws RemoteException, AmsException {
		// Reset the Modified Attributes
		attributeMgr.resetModified();
		// Indicate that the object has been instantiated
		this.objectInstantiated = true;
		// Set the object's Object Identifier
		this.setObjectID(objectID);
	}
	// -------------------------------------------------//
	// Document Integration Methods //
	// -------------------------------------------------//

	/**
	 * This method takes a document and puts this business object's attributes into the document starting at the specified component
	 * path. Currently, it will also put any component data into the document also.
	 *
	 * @return DocumentHandler
	 */
	public DocumentHandler populateXmlDoc(DocumentHandler doc) throws RemoteException, AmsException {
		return (this.populateXmlDoc(doc, ""));
	}

	public DocumentHandler populateXmlDoc(DocumentHandler doc, String componentPath) throws RemoteException, AmsException {
		if (!(componentPath.endsWith("/"))) {
			StringBuilder addlPath = new StringBuilder(componentPath);
			addlPath.append('/');
			addlPath.append(this.ecsgObjectName);
			addlPath.append('/');
			componentPath = addlPath.toString();
		}
		String[] attributes = getRegisteredAttributes();

		// For each attribute of this object, construct the path name for the
		// attribute, get it from the business object, and set the attribute in
		// the document
		for (String attribute : attributes) {
			String path = componentPath.concat(attribute);
			doc.setAttribute(path, getAttribute(attribute));
		}

		// For each component of this object, call the populateXmlDoc method
		// with the appropriate component path string
		Vector<ComponentEntry> components = this.componentGetActiveComponentHandles();
		for (int i = 0; i < components.size(); i++) {
			ComponentEntry compEntry = components.elementAt(i);
			String type = getComponentType(compEntry.getComponentIdentifier());
			if (type.equals(AmsConstants.BUSINESS_OBJECT)) {
				BusinessObject component = (BusinessObject) getComponentHandle(compEntry.getComponentIdentifier());
				StringBuilder nextPath = new StringBuilder(componentPath);
				nextPath.append('/');
				nextPath.append(compEntry.getComponentIdentifier());
				nextPath.append('/');
				component.populateXmlDoc(doc, nextPath.toString());
			} else if (type.equals(AmsConstants.LISTING_OBJECT)) {
				ComponentList component = (ComponentList) getComponentHandle(compEntry.getComponentIdentifier());
				StringBuilder nextPath = new StringBuilder(componentPath);
				nextPath.append('/');
				nextPath.append(compEntry.getComponentIdentifier());
				nextPath.append('/');
				component.populateXmlDoc(doc, nextPath.toString());
			}
		}

		return doc;
	}

	/**
	 * This method triggers any user-specified pre-delete processing that has been implemented both on the parent object (the one
	 * being deleted) and its component (children) objects. This processing is performed before any updates are triggered against
	 * the database to ensure data integrity.
	 *
	 *
	 */
	public int preDelete() throws AmsException, RemoteException {
		int returnCode = 1;
		this.userDelete();
		if (this.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
			returnCode = -1;
		}
		// Perform preDelete processing on component objects
		if (this.componentPreDelete() == -1) {
			returnCode = -1;
		}
		return returnCode;
		// If no errors were logged, return success
	}

	/**
	 * Performs any special "save" processing that is specific to the descendant of BusinessObject.
	 *
	 * This method is optinally coded in descendant objects as desired. Any pre-save processing should be coded within this method
	 * on descendants of BusinessObject. An example of this would be the setting of a last_updated attribute on the business object.
	 *
	 */
	protected void preSave() throws AmsException {
	}

	/**
	 * Descendent-implemented method used to register attributes for descendant business objects.
	 *
	 * Descendant objects will implement this method to register attributes for the business object.
	 *
	 */
	protected abstract void registerAttributes() throws AmsException;
	// -------------------------------------------------//
	// Component Management Methods //
	// -------------------------------------------------//

	/**
	 * Registers a component's vital information with the parent business object.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component
	 * @param componentName
	 *            The logical name of the component, this must match the JNDI name of the component.
	 * @param componentType
	 *            The component type, either AmsConstants.BUSINESS_OBJECT or AmsConstants.LISTING_OBJECT.
	 * @param idAttribute
	 *            The attribute which identifies the component with it's composite object.
	 * @deprecated Replaced by registerOneToManyComponent and registerOneToOneComponent.
	 */
	public void registerComponent(String componentIdentifier, String componentName, String componentType, String idAttribute)
			throws RemoteException, AmsException {
		this.componentRegisterComponent(componentIdentifier, componentName, componentType, idAttribute);
	}

	/**
	 * Descendent implemented method that initiates component registration.
	 *
	 * Descendant objects will implement this method to create the component manager and register all business object components.
	 *
	 */
	protected void registerComponents() throws RemoteException, AmsException {
	}

	/**
	 * This method registers a one-to-many (listing) component object with the business object's component manager.<br>
	 *
	 * NOTE: The default IDAttribute for one-to-many components is always the object's ObjectIDAttribute, therefore it is not
	 * necessary to specify the IDAttribute in this method.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component
	 * @param componentName
	 *            The logical name of the component, this must match the JNDI name of the component.
	 */
	protected void registerOneToManyComponent(String componentIdentifier, String componentName)
			throws RemoteException, AmsException {
		this.componentRegisterComponent(componentIdentifier, componentName, AmsConstants.LISTING_OBJECT, this.getIDAttributeName());
	}

	/**
	 * This method registers a one-to-one (businessObject) component object with the business object's component manager.
	 *
	 * @param componentIdentifier
	 *            The identifier of the component
	 * @param componentName
	 *            The logical name of the component, this must match the JNDI name of the component.
	 * @param idAttributeName
	 *            The logical name of the attribute which is used to establish the relationship between the composite (parent) and
	 *            component (child) object.
	 */
	protected void registerOneToOneComponent(String componentIdentifier, String componentName, String idAttributeName)
			throws RemoteException, AmsException {
		this.componentRegisterComponent(componentIdentifier, componentName, AmsConstants.BUSINESS_OBJECT, idAttributeName);
	}

	// Set's the object as a component object

	protected void resetNewComponentCalled() {
		this.newComponentCalled = false;
	}

	// -------------------------------------------------//
	// Object Save/Validation Methods //
	// -------------------------------------------------//

	public int save() throws RemoteException, AmsException {
		return this.save(true);
	}

	/**
	 * Saves all changes that have been made to the business object (since the last successful save).<br>
	 *
	 * This method method initially performs any save processing that may have been specified in descendant objects and then
	 * performs generic save processing which commits the changes that have been made on the business object to the DBMS.
	 *
	 * If the changes are successfully committed, the business object resets it's modified attribute/component processing
	 * indicators. These indicators are used to determine if any changes are pending for the business object.
	 *
	 */
	public int save(boolean shouldValidate) throws RemoteException, AmsException {
		try {
			ErrorManager errorManager = this.getErrorManager();
			boolean errorsFound = false;
			// Perform presave processing
			this.preSave();
			errorsFound = (errorManager.getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY);
			// If the object IS NOT a component, then initiate object
			// validation
			// NOTE: Component validation should only occur once. Components
			// are validated within the higher level object's validate method.
			if (!this.isComponent()) {
				if (shouldValidate) {
					if (this.validate() == -1) {
						errorsFound = true;
					}
				}
			}
			// Peform Component and BusinessObject updates
			// Perform save processing on the component manager(if appropriate)
			if (!errorsFound) {
				errorsFound = (this.componentSave() == -1);
			}
			// Save the business object's data
			if (!errorsFound) {
				try {
					errorsFound = (this.updateData() == -1);
				} catch (DataObjectException doe) {
					// A data object exception (or one of its subclasses) will result from various SQL errors (data too large for
					// column, etc)
					// Catch the exception and issue an error
					// IAZ IR-IEUK050167584 07/29/10 Begin: do not provide detailed information to the end-user for security reasons
					// Instead, write to the WebServer log on the server side
					errMgr.issueError(getClass().getName(), AmsConstants.SQL_ERROR, "", "Please contact your bank");

					// Set the error flag so that the transaction will be rolled back and error processed correctly
					errorsFound = true;
					LOG.error("Exception in BusinessObject::updateData()", doe);
				}
			}
			// If errors were encountered during processing,
			// rollback the transaction
			if (errorsFound) {
				this.getSessionContext().setRollbackOnly();
				LOG.error("{} - save() executed rollback, errors encountered", this.getClass().getName());
				return -1;
			} else {
				// If this is the parent object, put a message in the error
				// list that this object was updated. This will notify the web
				// infrastructure of an update so JavaBeans can be refreshed
				// if necessary.
				try {
					String theName = this.ecsgObjectName;
					String theOid = attributeMgr.getIDAttribute().getAttributeName();
					String strOid = getAttribute(theOid);
				} catch (Exception ex) {
				}
				// Clear the attribute manager's changes since they have been
				// saved
				attributeMgr.resetModified();
				// Reset the newComponentCalled indicator since the changes
				// have been saved
				resetNewComponentCalled();
				// Ensure that the object is not new
				this.isNew = false;
			}
		} catch (OptimisticLockException amsOptLockEx) {
			if (getRetryOptLock()) {
				return -2;
			}
            LOG.error(this.getClass().getName() + " - Rolling back transaction because of amsOptLockEx if !retryOptLock " + getRetryOptLock(), amsOptLockEx);
			this.getSessionContext().setRollbackOnly();
			return -1;
		} catch (AmsException amsEx) {
			LOG.error(this.getClass().getName() + " - Rolling back transaction because of " + amsEx.toString(), amsEx);
			this.getSessionContext().setRollbackOnly();
		}
		// Return Success
		return 1;
	}

	/**
	 * Set the value of an attribute with the passed in byte array
	 *
	 * 
	 * @param attributePath
	 *            The path for the requested attribute (i.e. first_name or /Account/account_id)
	 * @param attributeValue
	 *            The value of the attribute in bytes.
	 */
	public void setAttributeBytes(String attributePath, byte[] attributeValue) throws RemoteException, AmsException {

		String stringValue = null;
		if (attributeValue != null) {
			stringValue = com.amsinc.ecsg.util.EncryptDecrypt.bytesToBase64String(attributeValue);
		}

		this.setAttribute(attributePath, stringValue);
	}

	/**
	 * Set the value of an attribute that has been registered with the business object or one of its component objects.
	 *
	 * @param attributePath
	 *            The path for the requested attribute (i.e. first_name or /Account/account_id)
	 * @param attributeValue
	 *            The value that is to be set for the specified attribute path.
	 */
	public void setAttribute(String attributePath, String attributeValue) throws RemoteException, AmsException {
		try {
			// If no component string processing is required, set
			// the object's attribute
			if (!StringFunction.requiresComponentParsing(attributePath)) {
				attributeMgr.setAttributeValue(attributePath, attributeValue);
			} else {
				this.componentSetComponentAttribute(attributePath, attributeValue);
				this.newComponentCalled = true;
			}
			// If the Invalid Attribute exception has been thrown, issue a
			// business object error
		} catch (InvalidAttributeValueException attributeEx) {
			// Determine if an "Alias" has been registered on the attribute
			// manager. If an alias exists, issue the error using the alias
			// name, otherwise use the attribute name provided by the
			// attribute exception.
			String alias = attributeMgr.getAlias(attributePath);
			if (alias == null) {
				this.getErrorManager().issueError(getClass().getName(), attributeEx.getMessage(), attributeValue, attributePath);
			} else {
				this.getErrorManager().issueError(getClass().getName(), attributeEx.getMessage(), attributeValue, alias);
			}
		} catch (InvalidObjectAssociationException attributeEx) {
			this.getErrorManager().issueError(getClass().getName(), attributeEx.getErrorID(), attributeValue,
					attributeEx.getAssociationObjectName());
		}
	}

	/**
	 * For each specified attributePath/attributeValue pair, set the values accordingly.
	 *
	 * @param attributePaths
	 *            An array of attribute paths to be set.
	 * @param attributeValues
	 *            An array of attribute values that correspond to the specified attributePaths array.
	 */
	public void setAttributes(String[] attributePaths, String[] attributeValues) throws RemoteException, AmsException {
		int count = attributePaths.length;
		// For each attribute path requested, store the value in the hash
		for (int i = 0; i < count; i++) {
			this.setAttribute(attributePaths[i], attributeValues[i]);
		}
	}

	public void setClientServerDataBridge(ClientServerDataBridge csdb) throws RemoteException {
		this.csdb = new ClientServerDataBridge(csdb);
	}

	// Setter for data Table Name
	protected void setDataTableName(String tableName) {
		this.dataTableName = tableName;
	}
	// -------------------------------------------------//
	// Object property Methods //
	// -------------------------------------------------//

	// Setter for Object ID
	protected void setObjectID(long objectID) {
		this.objectID = new Long(objectID);
	}
	// -------------------------------------------------//
	// EJB Methods //
	// -------------------------------------------------//

	// Context Method
	public void setSessionContext(SessionContext pContext) {
		myContext = pContext;
		String stringPropValue = null;
		// Get Environment Properties
		Context ctx = null;

		// Check the EJBEnvironmentProperties to see if we can get the cached values
		Hashtable<String, String> ejbEnvironment = EJBEnvironmentProperties.getEnvironment(this.getClass().toString());

		// If we can get the hashed values then we should just read them
		if (ejbEnvironment != null) {
			this.ecsgObjectName = ejbEnvironment.get("ecsgObjectName");
			this.ecsgDataName = ejbEnvironment.get("ecsgDataName");
			this.dataTableName = ejbEnvironment.get("dataTableName");

			stringPropValue = ejbEnvironment.get("debugMode");
			if (stringPropValue.equals(AmsConstants.DEBUG_SYSTEM_OUT) || stringPropValue.equals(AmsConstants.DEBUG_DEBUG_FILE)) {
				this.debuggingOn = true;
				this.debugMode = Integer.parseInt(stringPropValue);
			}
		} else {
			// Otherwise we'll look up the properties through JNDI and populate our environment
			// cache
			ejbEnvironment = new Hashtable<>();
			try {
				ctx = GenericInitialContextFactory.getInitialContext();
			} catch (AmsException amsEx) {
			}

			try {
				// Set the ecsg object name (obtained from the Deployment Descriptor)
				if(ctx!=null)
				stringPropValue = (String) ctx.lookup("java:/comp/env/ecsgObjectName");
				if (stringPropValue != null) {
					this.ecsgObjectName = stringPropValue;
					ejbEnvironment.put("ecsgObjectName", stringPropValue);
				}
			} catch (NamingException e) {
			}
			try {
				// Set the data name (obtained from the Deployment Descriptor)
				if(ctx!=null)
				stringPropValue = (String) ctx.lookup("java:/comp/env/ecsgDataName");
				if (stringPropValue != null) {
					this.ecsgDataName = stringPropValue;
					ejbEnvironment.put("ecsgDataName", stringPropValue);
				}
			} catch (NamingException e) {
			}
			try {
				// Set the table name (obtained from the Deployment Descriptor)
				stringPropValue = (String) ctx.lookup("java:/comp/env/dataTableName");
				if (stringPropValue != null) {
					this.setDataTableName(stringPropValue);
					ejbEnvironment.put("dataTableName", stringPropValue);
				}
			} catch (NamingException e) {
			}

			try {
				// Determine if the Debugging Service has been enabled for this component
				stringPropValue = (String) ctx.lookup("java:/comp/env/debugMode");
				if (stringPropValue != null) {
					ejbEnvironment.put("debugMode", stringPropValue);
                    if (stringPropValue.equals(AmsConstants.DEBUG_SYSTEM_OUT) || stringPropValue.equals(AmsConstants.DEBUG_DEBUG_FILE)) {
						this.debuggingOn = true;
						this.debugMode = Integer.parseInt(stringPropValue);
					}
				}
			} catch (Exception e) {
			}
			// Add the read properties to the environment cache
			EJBEnvironmentProperties.putEnvironment(this.getClass().toString(), ejbEnvironment);
		}

	}

	// Set's the object as a component object

	public void setToComponent() {
		this.componentObject = true;
	}

	/**
	 * Updates any changes that have occured on the business object with the DBMS.
	 *
	 * @return int 1 for Success, -1 for Failure
	 */
	protected int updateData() throws RemoteException, AmsException {
		// Init the result to success
		return this.dataObjectUpdateData(attributeMgr.getChanges(), this.isNew);

	}

	/**
	 * This method is a hook method that is optionally implemented by descedent objects. This method is called in the business
	 * object method delete.
	 *
	 * Any processing that is necessary before an object is deleted should be placed within this method. If business object errors
	 * are issued within this method, delete processing will not continue and the delete transaction will be rolled back.
	 *
	 *
	 */
	protected void userDelete() throws AmsException, RemoteException {
		// SAMPLE CODE:
		// String name = this.getAttribute ("account_balance");
		// if (account_balance > 500)
		// {
		// issueError (BALANCE_TOO_HIGH);
		// }
		// END SAMPLE CODE:
	}

	/**
	 * Descendent implemented method to determine / calculate, and return the value of computed attributes. This method is called by
	 * the AttributeManager whenever the value of a computed attribute is requested. The logic to calculate the value of the
	 * attribute must be implemented in this method in the descendent class.
	 *
	 * @param attributeName
	 *            The name of the attribute to compute
	 * @return The value of the attribute
	 *
	 */
	public String userGetAttribute(String attributeName) throws RemoteException, AmsException {
		// SAMPLE CODE:
		// if (attributeName.equals ("HourlyWage")) {
		// BigDecimal hourlyWage = new BigDecimal (0);
		// BigDecimal hoursInYear = new BigDecimal (2000);
		// hourlyWage = getAttributeDecimal ("AnnualSalary").divide (
		// hoursInYear, BigDecimal.ROUND_HALF_UP);
		// return hourlyWage.toString ();
		// }
		// END SAMPLE CODE
		return "";
	}
	// -------------------------------------------------//
	// Descendant implemented Methods //
	// -------------------------------------------------//

	/**
	 * This method is a hook method that is optionally implemented by descedent objects. This method is called in the business
	 * object method initObject whtich is triggered each time the object is created. This method should be used only for processing
	 * that needs to be done every time the object is created.
	 *
	 */
	protected void userInitObject() throws AmsException, RemoteException {
	}

	/**
	 * This method is a hook method that is optionally implemented by descedent objects. This method is called in the business
	 * object method newObject.
	 *
	 */
	protected void userNewObject() throws AmsException, RemoteException {
		// SAMPLE CODE:
		// this.setAttribute ("account_balance","1000.00");
		// END SAMPLE CODE:
	}

	/**
	 * Performs any special "validate" processing that is specific to the descendant of BusinessObject.
	 *
	 * This method is optinally coded in descendant objects as desired. Any object specific validation should be coded within this
	 * method on descendants of BusinessObject.
	 *
	 */
	protected void userValidate() throws AmsException, RemoteException {
		// SAMPLE CODE:
		// String name = this.getAttribute ("first_name");
		// if (name.equals ("Bill"))
		// {
		// issueError (AmsConstants.INVALID_ATTRIBUTE, name, "first_name");
		// }
		// END SAMPLE CODE:
	}

	/**
	 * Performs basic object validation for the business object.
	 *
	 * This method method initially performs any validation processing that may have been specified in descendant objects
	 * (userValidate()) and then performs generic validation processing.
	 *
	 * @return int 1 for Success, -1 for Failure
	 */
	public int validate() throws RemoteException, AmsException {
		boolean errorsFound = false;
		// Validate Component Objects
		if (this.componentValidate() == -1) {
			errorsFound = true;
		}
		// Perform user-specific validation (USED TO HAVE RETURN CODE.
		// REMOVED RETURN CODE FOR SIMPLIFIED DEVELOPMENT.)
		userValidate();
		// Perform attribute validation
		for (String reqAttrib : attributeMgr.validateRequired()) {
			this.getErrorManager().issueError(getClass().getName(), AmsConstants.REQUIRED_ATTRIBUTE, reqAttrib);
		}

		// Return Success/Failure
		if (this.getErrorManager().getMaxErrorSeverity() >= ErrorManager.ERROR_SEVERITY) {
			errorsFound = true;
		}
		if (errorsFound) {
			return -1;
		} else {
			return 1;
		}
	}

	/**
	 * Performs basic object validation for the business object.
	 *
	 * This method method initially calls presave and performs any validation processing that may have been specified in descendant
	 * objects (userValidate()) and then performs generic validation processing.
	 *
	 * @return int 1 for Success, -1 for Failure
	 */
	public int validate(boolean callPreSave) throws RemoteException, AmsException {
		if (callPreSave) {
			this.preSave();
		}
		return this.validate();
	}

	/**
	 * This method builds 2 arrays that contain the column names and column values of each modified attribute.
	 *
	 * The column names and correspnding values are generated based on the ModifiedAttributes HashTable that is passed into the
	 * method. The arguments colNames and colValues are populated accordingly. The genereated string arrays are used to set values
	 * on the dbKona record object to facilitate DBMS updates.
	 *
	 * @param Hashtable
	 *            A Hashtable of attribute objects that need to be updated.
	 * @param String[]
	 *            colNames An array of strings that will be populated with all DBMS columns that have been modified.
	 * @param String[]
	 *            colValues An array of strings that will be populated with all DBMS column values that have been modified.
	 *
	 */
	private void dataObjectBuildParameters(Hashtable htModifiedAttributes, String[] colNames, String[] colValues)
			throws AmsException, RemoteException {
		Attribute attribute = null;
		int index = 0;
		Enumeration entries = htModifiedAttributes.keys();
		// Build the SQL name and value arrays
		while (entries.hasMoreElements()) {
			colNames[index] = (String) entries.nextElement();
			attribute = (Attribute) htModifiedAttributes.get(colNames[index]);
			colValues[index] = attribute.getAttributeValue();
			// IAZ IR-IEUK050167584 07/29/10 Begin: Prevet "<script>" constant from eevr being saved to db
			if ((colValues[index] != null) && (colValues[index].indexOf("<script>") != -1)) {
				this.getErrorManager().issueError(getClass().getName(), "ATT08", colValues[index], colNames[index]);
				throw (new AmsException());
			}
			// If the attribute is a date attribute, add it to the dateAttribute Hash
			if (attribute instanceof DateAttribute) {
				this.DateAttributes.put(colNames[index], "");
			}
			// If the attribute is a time attribute, add it to the timeAttribute hash
			if (attribute instanceof DateTimeAttribute) {
				this.DateTimeAttributes.put(colNames[index], "");
			}
			// If the attribute is a Blob attribute, add it to the blobAttribute hash
			if (attribute instanceof BlobAttribute) {
				this.BlobAttributes.put(colNames[index], "");
			}
			index++;
		}
	}
	// -------------------------------------------------//
	// DBMS Connect Methods //
	// -------------------------------------------------//

	/**
	 * Connects to the DBMS using the DBMS parameters that have been designated in the BusinessObject's environment properties.
	 *
	 * @exception An
	 *                AmsException is thrown if any SQL Exceptions are caught
	 * 
	 */
	private void connect() throws AmsException {
		String dbmsConnectionPool = null;
		try {
			dbmsConnectionPool = JPylonProperties.getInstance().getString("dataSourceName");
		} catch (AmsException e) {
		}

		// Get a connection from the Server's Connection Pool
		try {
			// Grab the datasource from the datasource factory using the
			// connection pool name from the BusinessObject's environment
			DataSource ds = DataSourceFactory.getDataSource(dbmsConnectionPool);
			dbmsConnection = ds.getConnection();
        } catch (SQLException e) {
            throw new AmsException("Error on connecting to Datasource in connect () using DBMS parms: " + dbmsConnectionPool + " Nested Exception: " + e.getMessage(),e);
		}
	}

	protected void issueOptimisticLockError() throws AmsException, RemoteException {

		if (!getRetryOptLock()) {
			this.getErrorManager().issueError(getClass().getName(), "ATT07");
		}
	}

	/**
	 * Deletes the object's data from the DBMS. Note, the getData method must be called before a delete will process.
	 *
	 * @exception AmsException
	 */
	private void dataObjectDelete() throws AmsException, RemoteException {
		try {
			this.connect();
			this.prepareTableDataSet();
			this.getObjectRecord();
			// If an optimistic lock column was registered, inform the table
			// data set and set the value automatically, regardless if the
			// client changed the value
			Attribute lockAttr = attributeMgr.getOptimisticLockAttribute();
			if (lockAttr != null) {
				this.objectData.setOptimisticLockingColumn(lockAttr.getPhysicalName());
				this.objectRecord.setValue(lockAttr.getPhysicalName(), lockAttr.getAttributeValue());
			}
			this.objectRecord.markToBeDeleted();
			int rowsChanged = this.objectData.save(this.dbmsConnection, true);
			if (rowsChanged <= 0) {
				this.issueOptimisticLockError();
				throw new OptimisticLockException(this.getClass().getName());
			}
			// Close the resources associated with the object Dataset
			this.objectData.close();
			// Disconnect from the DBMS
		} catch (SQLException | DataSetException ex) {
			// SQL Exceptions should never happen in normal use
			// If they do happen, something is very wrong, so write out to log
			LOG.error("Exception occured in dataObjectDelete",ex);
			throw new AmsException("Error Deleting in DataObject.  Nested Excpetion: " + ex.getMessage(),ex);
		} finally {
			this.disconnect();
		}
	}

	/**
	 * Disconnects the from the DBMS
	 *
	 * @exception An
	 *                AmsException is thrown if any SQL Exceptions are caught
	 */
	private void disconnect() throws AmsException {
		try {
			if (dbmsConnection != null) {
				dbmsConnection.close();
			}
		} catch (Exception e) {
			throw new AmsException("Error on disconnecting DataObject " + e.getMessage(), e);
		}
	}

	/**
	 * This method obtains the next valid object ID from the ObjectIDCache and returns it as a long.
	 *
	 * @return long The newly generated unique object identifier.
	 */
	private synchronized long generateObjectID() throws AmsException {
		ObjectIDCache oid = ObjectIDCache.getInstance(AmsConstants.OID);
		return oid.generateObjectID();
	}

	// -------------------------------------------------//
	// Data Retrieval Methods //
	// -------------------------------------------------//
	/**
	 * Gets the data associated with a singular or collection business object.
	 *
	 * The following steps are performed: - Establish a connection to the DBMS - Set the dbKona TableDataSet parms and fetch the
	 * records - Load the results to the business object's attribute manager
	 *
	 * @param Hashtable
	 *            The attribute manager's registered attributes
	 */
	private void dataObjectGetData() throws AmsException {

		try {
			this.connect();
			this.prepareTableDataSet();
			this.getObjectRecord();
			this.dataObjectLoadDatatoManager();
			try {
				this.objectData.close();
			} catch (Exception e) {
				throw new AmsException("Error in getting data. Nested exception is: " + e.getMessage(), e);
			}
		} catch (Exception e) {
			throw new AmsException("Error in getting data. Nested exception is: " + e.getMessage(), e);
		} finally {
			this.disconnect();
		}
	}

	/**
	 * This method sets the dbKona.TableDataSet parameters and fetches the business object's DBMS record.
	 *
	 * The following steps are performed: - Fetch the records into the TableDataSet - If no records found, throw an exception,
	 * otherwise, get the object's record
	 *
	 * @exception AmsException
	 */
	private void getObjectRecord() throws AmsException {
		try {
			// Get the object's Record
			this.objectData.fetchRecords();
			// If no rows are found, throw and exception
			if (this.objectData.size() < 1) {
				long objectID = this.getObjectID();
				throw new InvalidObjectIdentifierException(Long.toString(objectID));
			}
			this.objectRecord = this.objectData.getRecord(0);
		} catch (Exception ex) {
            throw new AmsException("Exceptions encountered during getObjectRecord() processing. Nested exception is: " + ex.getMessage(), ex);
		}
	}

	/**
	 * Loads the values from the dbKona Record object to the business object's Hashtable of registered attributes. If the DBMS value
	 * is null, the attribute value is NOT set.
	 *
	 * @param Hashtable
	 *            The Hash of registered attribute objects on the business object
	 */
	private void dataObjectLoadDatatoManager() throws AmsException {
		Hashtable<String, Attribute> registeredAttributes = attributeMgr.getAttributeHashtable();
		Value value = null;
		String physicalName;
		String attributeValue = null;
		// For each attribute, get the value from the object record and
		// set the attribute value accordingly
		Enumeration<Attribute> attributes = registeredAttributes.elements();
		while (attributes.hasMoreElements()) {
			Attribute attribute = attributes.nextElement();
			// If the attribute is a DoubleDispatchedAttribute, no processing is necessary
			if (attribute instanceof DoubleDispatchAttribute | attribute instanceof ComputedAttribute
					| attribute instanceof LocalAttribute) {
				// No processing necessary, process next attribute
			} else {
				// Process standard attributes
				physicalName = attribute.getPhysicalName().toUpperCase();
				try {
					value = this.objectRecord.getValue(physicalName);
				} catch (Exception e) {
					LOG.error("Errors encountered while getting data from record: ", e);
					throw new AmsException("Error occured in DataObject loadDatatoManager: Nested Exception: " + e.getMessage(),e);
				}
				// Only set the attribute value is the DBMS value is NOT null
				attributeValue = value.asString();
				if (attributeValue != null) {
					attribute.setAttributeValue(value.asString(), false);
				}
			}
		}
	}

	/**
	 * This method prepares the Dataobject's objectData "dbKona TableDataSet" for usage. This method is required for
	 * getData()/updateData() methods.
	 *
	 * The following steps are performed: - Establish a connection to the DBMS (using Dynamo DBMS Pooling services) - Set the dbKona
	 * TableDataSet parms and fetch the records
	 *
	 */
	protected void prepareTableDataSet() throws AmsException {
		try {
			String objectIdentifierAttribute = attributeMgr.getIDAttribute().getAttributeName();
            // Create a new dbKona TableDataSet using the DataSchemaFactory to save lookup time
            this.objectData = new TableDataSet(this.dbmsConnection, DataSchemaFactory.getSchema(this.dbmsConnection, this.dataTableName), objectKey);
            // Specify the "where" clause for the TableDataSet
			this.objectData.where(objectIdentifierAttribute + " = " + this.getObjectID());
		} catch (Exception e) {
			throw new AmsException("Errors encountered in preparing TableDataSet() method. Nested Exception: " + e.getMessage(), e);
		}
	}

	// -------------------------------------------------//
	// Data Update Methods //
	// -------------------------------------------------//
	/**
	 * Based on a the state of the object's attribute manager, this method performs updates for any DBMS related changes on the
	 * business object.
	 *
	 * @param Hashtable
	 *            The business object's registered attributes (passed from the buisness object)
	 * @param boolean
	 *            isNew An indicator as to if the object is new or existing.
	 * 
	 */
	public int dataObjectUpdateData(Hashtable htModifiedAttributes, boolean isNew) throws AmsException, RemoteException {
		int i = 0;
		int count = htModifiedAttributes.size();
		String colNames[] = new String[count];
		String colValues[] = new String[count];
		// If no attributes have been modified, no processing necessary
		if (count < 1) {
			return 0;
		}
		// Build the Parameters
		this.dataObjectBuildParameters(htModifiedAttributes, colNames, colValues);
		// Connect to the DBMS, and execute the update
		try {
			this.connect();
			this.prepareTableDataSet();
			// If the object is New, perform the appropriate processing
			if (isNew) {
				// Add a record to the object's TableDataSet
				this.objectRecord = this.objectData.addRecord();
			} else {
				// Get the object's data
				this.getObjectRecord();
			}
			// If an optimistic lock column was registered, inform the table
			// data set and set the value automatically, regardless if the
			// client changed the value
			Attribute lockAttr = attributeMgr.getOptimisticLockAttribute();
			if (lockAttr != null) {
				this.objectData.setOptimisticLockingColumn(lockAttr.getPhysicalName());
				this.objectRecord.setValue(lockAttr.getPhysicalName(), lockAttr.getAttributeValue());
			}
			// Set the values
			for (i = 0; i < count; i++) {
				// If the value is null or empty, set it accordingly
				if ((colValues[i] == null) || (colValues[i].isEmpty())) {
					this.objectRecord.setValueNull(colNames[i]);
				} else {
					// If the attribute is a date, convert the date accordingly
					if (DateAttributes.containsKey(colNames[i])) {
						try {
							java.util.Date dte = DateTimeUtility.convertStringDateToDate(colValues[i]);
							// Set the date attribute
							this.objectRecord.setValue(colNames[i], dte);
						} catch (Exception e) {
                            LOG.error(this.getClass().getName() + " - Cannot convert Date value \"" + colValues[i] + "\" for " + colNames[i], e);
						}
					} else if (DateTimeAttributes.containsKey(colNames[i])) {
						try {
							java.util.Date dte = DateTimeUtility.convertStringDateTimeToDate(colValues[i]);
							// Set the date attribute
							this.objectRecord.setValue(colNames[i], dte);
						} catch (Exception e) {
							LOG.error(this.getClass().getName() + " - Cannot convert DateTime value \"" + colValues[i] + "\" for " + colNames[i], e);
						}
					} else if (BlobAttributes.containsKey(colNames[i])) {
						try {
							byte[] byteArray = com.amsinc.ecsg.util.EncryptDecrypt.base64StringToBytes(colValues[i]);
							// Set the Blob attribute
							this.objectRecord.setValue(colNames[i], byteArray);
						} catch (Exception e) {
							LOG.error(this.getClass().getName() + " - Cannot convert Blob value for " + colNames[i], e);
						}
					} else {
						// Set the attribute
						this.objectRecord.setValue(colNames[i], colValues[i]);
					}
				}
			}
			// Clear the date/time Attributes Hash
			DateAttributes.clear();
			DateTimeAttributes.clear();
			// Save the Changes
			int rowsChanged = this.objectData.save(this.dbmsConnection, true);
			if (rowsChanged <= 0) {
				this.issueOptimisticLockError();
				throw new OptimisticLockException(this.getClass().getName());
			}

			// Close the dbKona Resources
			this.objectData.close();
		} catch (SQLException sqlEx) {
			// SQL Exceptions should never happen in normal use
			// If they do happen, something is very wrong, so write out to log
			LOG.error("SQLException in Update: " + sqlEx.getMessage() + " DBMS Vendor Code: " + sqlEx.getErrorCode(), sqlEx);
			// Based on the Vendor Code, throw the Appropriate Exception: NOTE no
			// brak necessary because an exception is being thrown
			switch (sqlEx.getErrorCode()) {
			case 1:
				throw new UniqueConstraintViolatedException();
			case 1400:
				throw new ColumnNotNullException();
			case 1401:
				throw new ValueTooLargeForColumnException();
			case 942:
				throw new InvalidTableViewException();
			default:
				throw new DataObjectException(sqlEx.getErrorCode());
			}
		} catch (DataSetException dsEx) {
			// DataSetExceptions should never happen in normal use
			// If they do happen, something is very wrong, so write out to log
			LOG.error("DataSet Exception in Update: " + dsEx.getMessage(), dsEx);
			throw new DataObjectException("Error Updating DataObject.  Nested Exception: " + dsEx.getMessage());
		} finally {
			// Close the DBMS connection
			this.disconnect();
		}

		return 1;
	}

	public boolean isAttributeRegistered(String attributeName) throws AmsException, RemoteException {
		return this.attributeMgr.isAttributeRegistered(attributeName);
	}

	public void setRetryOptLock(boolean retryOptLockOnEx) {
		retryOptLock = retryOptLockOnEx;
	}

	public boolean getRetryOptLock() {
		return this.retryOptLock;
	}

	/*
	 * This will get the max error severity of its componets. Get the max error severity of this business object and compare with
	 * components error severity and return highest severity. Added by Prateep Gedupudi IR#T36000019323
	 */
	public int getMaxErrorSeverity() throws RemoteException, AmsException {
		int maxErrorSev = getErrorManager().getMaxErrorSeverity();
		for (IssuedError err : getIssuedErrors()) {
			if (err.getSeverity() > maxErrorSev) {
				maxErrorSev = err.getSeverity();
			}
		}
		return maxErrorSev;
	}

}
