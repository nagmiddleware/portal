package com.amsinc.ecsg.frame;

/*
 * @(#)TableListView
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.frame.*;
import java.rmi.*;
import javax.ejb.*;

public interface TableListView extends ListView{ }
