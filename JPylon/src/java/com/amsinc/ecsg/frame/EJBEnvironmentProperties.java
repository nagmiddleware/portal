package com.amsinc.ecsg.frame;
/*
 * @(#)DataSourceFactory
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import javax.naming.*;
import java.util.*;
import javax.naming.*;
import javax.sql.*;
import java.util.PropertyResourceBundle;
import java.util.Hashtable;
import com.amsinc.ecsg.frame.GenericInitialContextFactory;

/**
 * The EJBEnvioronmentProperties is a class that caches environment
 * properties. Normally EJBs use JNDI to look up their environment
 * properties. Using the EJBEnvironmentProperties class provided a
 * performance boost over using JNDI. Only one of these
 * classes should appear per VM.
 *

 * @version 1.0 
 */
public class EJBEnvironmentProperties
{
    /** Hashtable to store hashtables of environment properties. One
        entry per ejb type will be stored in the hash. */
    private static Hashtable ejbEnvironmentProperties = new Hashtable ();

    private EJBEnvironmentProperties ()
    {
    }

    /**
     * Resets the hashtable. If the environment properties change for
     * and EJBs deployed, this method should be called to ensure the
     * environment properties are refreshed.  
     *

     */
    public static synchronized void refreshCachedValues ()
    {
        ejbEnvironmentProperties = new Hashtable ();
    }

    /**
     * Getter for environment properties with key being the JNDI name of
     * the ejb.
     *
     * @param key The name used to identify the environment properties to
     * be retreived
     * @return The hashtable of environment values corresponding to the key
     * value passed to this method

     */
    public static Hashtable getEnvironment (String key)
    {
        return (Hashtable)ejbEnvironmentProperties.get (key);
    }

    /**
     * Setter for environment properties with key being the JNDI name of
     * the ejb and value being a hashtable of environment properties.
     *
     * @param key The name used to identify the environment properties to
     * be stored
     * @param value The hashtable of environment values corresponding to 
     * the key
     * value passed to this method
     *

     */
    public static synchronized void putEnvironment (String key, Hashtable value)
    {
        ejbEnvironmentProperties.put (key,value);
    }
}
