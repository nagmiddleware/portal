package com.amsinc.ecsg.frame;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.amsinc.ecsg.util.*;

/**
 *  The class facilitate the reading of XML config files.   The files
 *  are read from the servlet context.  This allows 
 *  the XML to be located within the structure of a Web Application Resource 
 *  (WAR) file.  This class explicitly DOES NOT cache config files.
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */
public class XmlConfigFile {

    // The servlet context of AmsServlet (set from AmsServlet.init())
    private static ServletContext context;

    /**
     * This method sets the servlet context of the cache.   The servlet
     * context is used to look up the configuration files.   This must 
     * be set prior to making any other method calls.
     *
     * @param ServletContext theContext - the servlet context to read from
     */
    public static void init(ServletContext theContext) {
        context = theContext;               
    }

    /**
      * Obtains data from a configuration file.   The file may have been cached
     * already.   If it is not already cached, the file is read from the servlet
     * context.  
     *
     * The init() method must have already been called prior to calling this method.
     *
     *
     * @param path String - the path to the file
     * @return DocumentHandler - the XML data requested
     */
    public static DocumentHandler readConfigFile(String path) {
        //Look it up using the servlet context
        DocumentHandler xml;
        try {
            // XML files are under the /WEB-INF/configxml directory
            String fullPath = "/WEB-INF/configxml"+path;
 
            if(context == null) {
                System.out.println("Could not read XML configuration file because ServletContext is null!!");
            }
            
            // Use the ServletContext to retrieve the XML data
  	        BufferedReader in = new BufferedReader(new InputStreamReader(
                context.getResourceAsStream(fullPath)));
		
		    StringBuilder fileContent = new StringBuilder();
	        String line;
		
		    while ((line = in.readLine()) != null) {
		        fileContent.append(line);
            }
	
	        in.close();

            // Create a DocumentHandler from the XML
            xml = new DocumentHandler(fileContent.toString(), false);

            return xml;
        }
        catch(Exception e) {
            return new DocumentHandler();
        }
    }
}