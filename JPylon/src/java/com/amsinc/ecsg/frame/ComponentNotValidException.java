package com.amsinc.ecsg.frame;/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class ComponentNotValidException extends ComponentProcessingException{    public ComponentNotValidException(){super();}    public ComponentNotValidException(String componentName,String parentObject){super(componentName + " is not a registered component for object: " + parentObject);}    }