package com.amsinc.ecsg.frame;

/*
 * @(#)Mediator
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import javax.ejb.*;
import java.rmi.*;
import com.amsinc.ecsg.util.*;

public interface Mediator extends javax.ejb.EJBObject
{
  public DocumentHandler performService (DocumentHandler inputDoc) 
		 throws AmsException,RemoteException; 
  public DocumentHandler performService (DocumentHandler inputDoc, ClientServerDataBridge csdb)
		 throws AmsException, RemoteException; 
  public String performService(String inputDocString, ClientServerDataBridge csdb)
          throws AmsException, RemoteException;
}
