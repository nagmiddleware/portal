/*
 * @(#)FatalExceptionError
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.frame;

//** FatalErrorException.java
//   This class extends AmsException. It embodies a fatal error exception.
//   It stores an IssuedError object.

public class FatalErrorException extends AmsException
{
	public FatalErrorException(IssuedError iErr)
	{
		super(iErr);
	}
} // end "public class FatalErrorException extends AmsException"
