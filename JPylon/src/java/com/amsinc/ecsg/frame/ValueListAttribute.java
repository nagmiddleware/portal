package com.amsinc.ecsg.frame;

/**
 * ValueListAttribute class.  This class is a descendant of the
 * Business Object framework Attribute class.  It performs validation
 * on the attribute value to ensure the value is a valid based on a designated
 * list of valid values.
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *

 * @version 1.0
 */
public class ValueListAttribute extends Attribute {
	private String[] validValues;
	
	public ValueListAttribute ()
	 {
     }

	public ValueListAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public ValueListAttribute (String name, String physicalName,String[] values)
	 {
	    super(name, physicalName);
	    setValueList(values);
    }
	
	public ValueListAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }

	/**
	 * Sets the list of valid attribute values for this attribute object.
	 * @param String[] values A string array that contains all valid attribute values.

	 * @version 1.0
	 */
	protected void setValueList(String[] values)
	{
		validValues = values;
	}
	/**
	 * Ensures that the attribute value being set is valid based on a set of valid
	 * attribute values designated in the descendants of this class.
	 *
	 * @param String attributeValue The value of the attribute being set.

	 * @version 1.0
	 */
	public int validateValue(String attributeValue)
	{
		int count = this.validValues.length;
		int i = 0;
		for(i=0;i<count;i++){
			if(attributeValue.equals(validValues[i])){
				// Return Success
				return 1;
			}
		}
		// Return Failure (not found)
		return -1;
	}
}
