package com.amsinc.ecsg.frame;

/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class OptimisticLockAttribute extends NumberAttribute {
    // Validation logic for OptimisticLockAttribute being integer is in superclass
	public OptimisticLockAttribute ()
	 {
     }

	public OptimisticLockAttribute (String name, String physicalName)
	 {
	    super(name, physicalName);
     }

	public OptimisticLockAttribute (String name, String physicalName, boolean required, String alias)
	 {
	    super(name, physicalName, required, alias);
	 }
}