package com.amsinc.ecsg.frame;
/**
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
class AttributeNotRegisteredException extends AmsException{
    public AttributeNotRegisteredException(){super("The attribute being accessed is not registered");}
    public AttributeNotRegisteredException(String attributeName){super("The attribute "+attributeName+" is not registered");}
}

