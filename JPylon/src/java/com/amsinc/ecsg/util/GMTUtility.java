package com.amsinc.ecsg.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

/*
 * @(#)ObjectIDCache
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.DataSourceFactory;

/**
 * The ObjectIDCache caches OIDs to prevent DB lookups
 * every time a new object is created. Only one of these
 * classes should appear per VM.
 *

 * @version 1.0 
 */
public class GMTUtility
{
    // DBMS properties
    private static String dbmsParm;
    private static Connection dbmsConnection;
    
    static
    {
        try {
            JPylonProperties jPylonProperties = JPylonProperties.getInstance();
            dbmsParm = jPylonProperties.getString ("dataSourceName"); 
        } catch (Exception e) {
            dbmsParm = "";
        }
    }

    private GMTUtility ()
    {
    }

    /**
     * Retrieves a database connection from the data source factory. The
     * connection will be used to access the sequence table.
     */
    private static void connect (boolean onServer) throws AmsException
    {
        try
        {
            DataSource ds = DataSourceFactory.getDataSource (dbmsParm, onServer);
            dbmsConnection = ds.getConnection ();
        }
        catch (SQLException e)
        {
            throw new AmsException (
                "Error on connecting to Datasource in DataObject using DBMS " +
                "parms: " + dbmsParm + " Nested Exception: " + e.getMessage ());
        }
    }

    /**
     * Closes the database connection being used.
     */
    private static void disconnect () throws AmsException
    {
        try
        {
            if (dbmsConnection != null)
                dbmsConnection.close ();
        }
        catch (SQLException e)
        {
            throw new AmsException (
                "Error on disconnecting DataObject " + e.getMessage());
        }
    }


    public static synchronized java.util.Date getGMTDateTime () throws AmsException
    {
        return getGMTDateTime(true, true);
    }

    public static synchronized java.util.Date getGMTDateTime (boolean onServer) throws AmsException
    {
        return getGMTDateTime(onServer, true);
    }
    
    public static synchronized java.util.Date getGMTDateTime (boolean onServer, boolean overrideDateBasedOnFile) throws AmsException
    {
        java.util.Date gmtDate;
        ResultSet results = null;
        Statement statement = null;
        try
        {
  
            JPylonProperties jPylonProperties = JPylonProperties.getInstance();
            String overrideSystemDateFilePath = null;
            String overrideDateFlag = null;
            try {
                overrideSystemDateFilePath = jPylonProperties.getString("configFilePath");
		overrideDateFlag = jPylonProperties.getString("overrideDate");


            } catch (Exception e){}

            if(overrideDateBasedOnFile && (overrideDateFlag  != null) && (overrideDateFlag.equalsIgnoreCase("Y")))
            {
                overrideSystemDateFilePath += "/overrideDate.xml"; 

                // Pull the date from the file specified
                // It would be more efficient to cache this date and not read from the
                // file each time.   However, this feature will only ever be used in 
                // testing environments where performance is not a concern.
                String overrideDate =  KeyValueXmlParser.parse(overrideSystemDateFilePath).get("overrideDate");

                SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");  

                return iso.parse(overrideDate);                
            } 
            else if (jPylonProperties.getString("getDateTimeFromDatabase").equals("y"))
            {

              
              connect(onServer);
              statement = dbmsConnection.createStatement ();
              results = statement.executeQuery ("SELECT TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') AS RIGHTNOW FROM DUAL");
              results.next ();

              String formattedDate = results.getString ("RIGHTNOW");

              SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");  
              gmtDate = sdf.parse(formattedDate);   

              statement.close ();
              dbmsConnection.commit ();
            } else {
              return new java.util.Date();
            }
        }
        catch (Exception e)
        {

            throw new AmsException (
                "Error getting date from Oracle. Nested Excpetion: " +
                e.getMessage ());
        } finally {
        	
        	if(results != null){
				try {
					results.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
        	if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
        	}
            disconnect ();
        }

        return gmtDate;
    }
}
