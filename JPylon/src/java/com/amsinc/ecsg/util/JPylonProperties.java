/*
 * @(#)JPylonProperties
 *
 *
 *     Copyright  © 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
 
package com.amsinc.ecsg.util;
 
import com.amsinc.ecsg.frame.AmsException;
import java.util.PropertyResourceBundle;

import java.util.MissingResourceException;



/**
 * Reads the Jpylon configuration in the form of a PropertyResourceBundle.
 * The name of the properties file is set as a constant in this class.
 * <BR>
 * Sample .properties file:
 * <BR>
 * EJBVendor=Weblogic<BR>
 * serverLocation=t3://localhost:7001/<BR>
 * configFileDir=d:/jpylon/mapFiles/<BR>
 * debugMode=1<BR>
 * xslFileLocation=d:/weblogic5/myserver/public_html<BR>
 * pageExpirationCount=10<BR>
 * jndiFactoryName=weblogic.jndi.WLInitialContextFactory<BR>
 * dbmsParm=database.TXoraclePool<BR>
 * dbmsDriverClass=weblogic.jdbc.jts.Driver<BR>
 * dbmsConnectionPool=jdbc:weblogic:pool:oraclePool<BR>
 * # <BR>
 * # <BR>
 * # <BR>
 * servletLocation=/jPylon/JPylonServlet<BR>
 * servletJSPLocation=/jPylon/JPylonServlet.jsp<BR>
 * # <BR>
 * refDataTableName=refdata <BR>
 * # <BR>
 * # <BR>
 * # <BR>
 * loggingPort=4000 <BR>
 * loggingServer=127.0.0.1 <BR>
 * loggingConfigurationFile=./Logging.xml <BR>
 * loggingThreads=21 <BR>
 * loggingTCPNoDelay=y <BR>
 * loggingClientRetryCount=3 <BR>
 * loggingMaxFileLength=1000000 <BR>
 * <BR>

 */
 
public class JPylonProperties
{

   protected static JPylonProperties jpylonPropertiesInstance = null;
   protected static PropertyResourceBundle jpylonProperties;
   protected static final String JPYLON_BUNDLE_NAME = "jPylon";
   protected static String serverLocation = null;

   /** 
   * Default constructor.  Reads the properties and validates the JPylon license.
   *
   * @throws AmsException
   */
   public JPylonProperties() throws AmsException
   {
      jpylonPropertiesInstance = this;
      
      refresh();
      
      if (jpylonProperties == null)
      {
         System.out.println ("JPylon configuration file not found");
      }      
   }

   /**
    * This function should be called if the properties are every updated 
    *
    * @throws AmsException
    */
   public synchronized void refresh() throws AmsException
   {
      try 
      {
         jpylonProperties = (PropertyResourceBundle)PropertyResourceBundle.getBundle(JPYLON_BUNDLE_NAME);
         
         if (jpylonProperties == null)
         {
             throw new AmsException("Error getting " + JPYLON_BUNDLE_NAME + " file.");
         }
      }
      catch(MissingResourceException e)
      {
         throw new AmsException("Error getting " + JPYLON_BUNDLE_NAME + " file.");
      }

   }
   
   /**
    * This function retrieves a property from the file
    *
    * @param prop - The property to be retrieved.
    * @throws AmsException
    */
   public String getString(String prop) throws AmsException
   {
      String propValue = null; 
    
      if (jpylonProperties == null)
      {
         throw new AmsException("jpylonProperties not found");
      }
    
      if (prop == null)
      {
         throw new AmsException("NULL argument passed");
      }
      
      try
      {
         if(prop.equalsIgnoreCase("serverLocation") && (serverLocation != null))
             propValue = JPylonProperties.serverLocation;
         else
             propValue = jpylonProperties.getString(prop);
         
         if (propValue == null)
         {
            throw new AmsException(); 
         }
      }
      catch (Exception e)
      {
         throw new AmsException("could not find requested property" + e.getMessage());
         
      }
      return propValue;
   }

   /**
    * Sets an override to the server location
    *
    */
   public static void setServerLocation(String location)
    {
           serverLocation = location;
    }
   
   

  /**
   * This methods obtains an instance of the JPylonProperties class
   *
   * @returns An instance of this class
   * @throws AmsException
   */ 
   public static JPylonProperties getInstance() throws AmsException
   {
      if (jpylonPropertiesInstance == null) 
      {
         synchronized(JPylonProperties.class)
         {
            if (jpylonPropertiesInstance == null)
	        {
	           jpylonPropertiesInstance = new JPylonProperties();
               return jpylonPropertiesInstance;
            }
         }
      }
      return jpylonPropertiesInstance;  
   }//end method
     
}