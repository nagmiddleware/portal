package com.amsinc.ecsg.util;

/*
 * @(#)DocumentParser
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import java.io.*;

/* This class is used to read in data from a Reader and provide access
   to the data.  
*/
public class DocumentParser implements java.io.Serializable
{
   //An optimal number of bytes to read ahead. 
   private static final int BUFFER_SIZE = 16384;

   //A character array to hold data read from the reader
   char[] charBuffer = new char[BUFFER_SIZE];
   private int charBufferIndex = 0;

   //A character array to hold data that will be returned to the calling
   //class.  1000 is an arbitrary number and should probably be changed to
   //be BUFFER_SIZE.  If this happens, we should cache some char arrays of this
   //size, because they are expensive to create.
   char[] returnCharBuffer = new char[BUFFER_SIZE];
   private int returnCharBufferIndex = 0;
   
   //The local document reader.
   private Reader documentReader; 
   //Number of characters that have been read from the reader.
   private int readCharacters = 0;

   // Flag to indicate whether or not newline characters will be left in
   // or stripped by the parser
   private boolean keepNewLines;

   /**  
    * Create a new instance of DocumentParser by setting the Reader and
    * getting the first data from the reader.
    *
    * @param Reader A valid reader.
    * @return A new DocumentParser instance.
    */        
   public DocumentParser(Reader reader, boolean keepNewLines)
   {
        if (reader != null)
        {
            documentReader = reader;
            getNextDataChunk();
        }

       this.keepNewLines = keepNewLines;
       
   }

   /**  
    * Grabs the next chunk of data from the reader and places the data into
    * the character array.
    *
    */        
   private void getNextDataChunk()
   {
        if (readCharacters != -1)
        {
           charBuffer = new char[BUFFER_SIZE];
           try {
              readCharacters = documentReader.read(charBuffer, 0, BUFFER_SIZE);
           } catch (IOException ioEx) {
              System.out.println("IOException in getNextDataChunk");
           }
           charBufferIndex = 0;
        }
   }
   
   /**  
    * Reads a group of characters up to the delimeter or end of file
    * and places the characters into a char array.  The car array is 
    * returned if there are characters.  If there are no characters to
    * return, null is returned.
    *
    * @param char The delimeter to which data should be read.
    * @return char[] An array of characters.
    */        
   public char[] getNextToken(char delimeter)   
   {
       int  delimeterInt = (int)delimeter;
        returnCharBufferIndex = -1;

       int charInt = this.getBufferCharacter();
       if (charInt == -1)
            return null;
           
        do {
            if( (charInt != delimeterInt) &&
                (keepNewLines || ((charInt != 10) && (charInt != 13))) )
            {
                returnCharBufferIndex++;
                // Grow the buffer by doubling its size if needed.
                // DocumentParser object is expected to be short-lived.  So don't need to shrink it.
				if (returnCharBufferIndex >= returnCharBuffer.length) {
					char[] newReturnCharBuffer = new char[2 * returnCharBuffer.length];
					System.arraycopy(returnCharBuffer, 0, newReturnCharBuffer, 0, returnCharBuffer.length);
					returnCharBuffer = newReturnCharBuffer;
				}
				returnCharBuffer[returnCharBufferIndex] = (char)charInt;
            }
            charInt = this.getBufferCharacter();                        
        } while ((charInt != -1)&&(charInt != delimeterInt));

        return returnCharBuffer;
   }
   
   /**  
    * Returns the index of the return char buffer array.  This will enable
    * us to use the same char array and to avoid having to clear out the array.
    *
    * @return int The index of the last entry in the char array.
    */        
   public int getReturnArrayBufferIndex()
   {
        return this.returnCharBufferIndex;
   }
   
   /**  
    * Returns the next character in the buffer
    *
    * @return int The next character in the buffer.
    */        
   private int getBufferCharacter()
   {
        if (charBufferIndex >= readCharacters)
        {
           if (readCharacters == -1)
              return -1;
           else
              getNextDataChunk();
        }
        
        int returnChar;
        if (charBufferIndex < readCharacters)
        {
           returnChar = (int)charBuffer[charBufferIndex];
           charBufferIndex++;
        } 
        else 
        {
           returnChar = -1; 
        }
        return returnChar;
   }
}