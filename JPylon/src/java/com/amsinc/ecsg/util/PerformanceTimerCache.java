package com.amsinc.ecsg.util;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.*;

/**
 *  This class caches timer data and configuration data related to 
 *  performance timers.   The contents of the configuration XML file
 *  are cached here, but other classes may cache information about
 *  it contents.
 *
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PerformanceTimerCache 
{
	private static final Logger LOG = LoggerFactory.getLogger(PerformanceTimerCache.class);
   // List of timers to be saved
   private static HashSet cachedTimers;
   
   // The maximum size of the cache of timer data (read from XML)
   private static int cacheSize = 50;
   
   // Indicates whether or not timer data is being collected (read from XML)
   // This would be set to false for environments that don't
   // care about performance timers
   private static boolean collectTimerData = false;
   
   // The XML file that contains configuration information
   // about performance timers
   private static DocumentHandler configXml;
   
   // A timestamp of the last time when the XML file was read from the database
   private static long xmlLastUpdateTime;
   
   static
    {       
      // Read in the configuration XML file
      readConfigXmlDoc();

      cachedTimers = new HashSet(cacheSize);  
    }
   
    /**
     * Clears out the hashtables so that certs will be read again from files
     * instead of from the cached files
     *
     * This method is here so that it can be updated using ServerDeploy
     */
    public static String update()
     {
        LOG.info("Updating cached Performance Timer Configuration");

        readConfigXmlDoc();

        return "Done";
     }


   /**
    * Reads in the PerformanceTimer.xml file and stores it in this class.
    * It also records a timestamp of when the file was last read into memory.
    * This timestamp is used by other classes to determine if they can rely
    * on data they have cached about the XML config file or whether they need
    * to reload.
    *
    */
   private static synchronized void readConfigXmlDoc()
   {
      String configFilePath = null;
      
      try
      {
         // Determine the path to the file from the configuration file
         JPylonProperties jPylonProperties = JPylonProperties.getInstance();
         configFilePath = jPylonProperties.getString ("configFilePath");
      }
      catch(AmsException e)
      {  
      }
      
      if(configFilePath != null)
      {
         // Read in the file
         configXml = new DocumentHandler(configFilePath+"/PerformanceTimer.xml");
         
         if(configXml != null)
         {
             // Update the timestamp
             xmlLastUpdateTime = System.currentTimeMillis();            
            
             String cacheSizeStr = configXml.getAttribute("/CacheSize");
             if(cacheSizeStr != null)
                 cacheSize = Integer.parseInt(cacheSizeStr);
             else
                 cacheSize = 50;
                    
             String collectTimerDataStr = configXml.getAttribute("/CollectData");
             if((collectTimerDataStr != null) && (collectTimerDataStr.equalsIgnoreCase("Y")))
                  collectTimerData = true;
             else
                  collectTimerData = false;                 
          }
      }
   }
   
   /**
    * Adds a performance timer to the cache to be saved.   If the
    * cache size has been exceeded, a new thread will be created to 
    * save the data.
    *
    * @param timer - PerformanceTimer - the timer to be added to the cache
    */
   public static synchronized void add(PerformanceTimer timer)
   {
       // Add the timer to the cache
       cachedTimers.add(timer);
              
       // If the maximum cache size has been reached, create a new thread
       if(cachedTimers.size() >= cacheSize)
       { 
          // Create the new thread, passing in the timers to be saved
          Thread saveThread = new Thread(new PerformanceTimerSaver(cachedTimers));
             
          // Now that the timers to be saved have been passed to the thread,
          // create a new, empty cache of timers.
          cachedTimers = new HashSet(cacheSize); 
          
          // Start the thread to save the timers
          saveThread.start();  
       }
   }   
   
   /**
    * Returns true if the system is configured to collect timer data
    *
    * @return flag indicating whether or not to collect timer data
    */
   public static boolean collectTimerData()
   {
      return collectTimerData;
   }   
   
   /**
    * Returns the configuration XML that has been read in
    * 
    * @return the config XML
    */
   public static DocumentHandler getConfigXml()
   {
      return configXml;  
   }
 
   /**
    * Returns the date/time stamp that the configuration XML was last read in
    * 
    * @return the config XML last update time stamp
    */   
   public static long getXmlLastUpdateTime()
   {
     return xmlLastUpdateTime;
   }
}
