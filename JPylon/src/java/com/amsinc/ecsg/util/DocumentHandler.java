package com.amsinc.ecsg.util;

/*
 * @(#)DocumentHandler
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;

/**
 * The interface that defines how a document can be accessed. Implementations
 * of this interface would allow underlying document sources such as XML to
 * be handled via a naming and directory-like interface. The interface provides
 * for a document model independent implementation.
 * <br>
 * Note on supported XPath notation: <br>
 * This version of DocumentHandler supports the directory naming structure and
 * ID specification portions of XPath. Other query notations available within
 * XPath are not currently supported here.
 *

 * @version 1.0, 05/20/2001
 * @since   JDK1.1
 */

public class DocumentHandler implements java.io.Serializable
{
	private static final Logger LOG = LoggerFactory.getLogger(DocumentHandler.class);
    //The top of the document tree
    private DocumentNode    root;
    //The hashtable contains references to different nodes in the document
    //tree.  Every thread that accesses the hash gets it own entry to ensure
    //relative path names can be used by different accessing classes.
    private Hashtable<String,DocumentNode>   currNodeHT;

   /**  
    * Construct a blank document
    *
    * @return A new DocumentHandler instance.
    */       
   public DocumentHandler()
   {
        root = new DocumentNode("DocRoot");
        setCurrNode(root);
   }
   
   /**  
    * Construct a document from where the node passed in is the root node
    * of the new document handler
    *
    * @param  node The root node of the new document handler
    * @return A new DocumentHandler instance.
    */       
   public DocumentHandler(DocumentNode node)
   {
        root = node;
        setCurrNode(root);
   }
   
   /**  
    * Construct a document from a file that is pointed to by the filename
    * string.
    *
    * @param  filename The file name of the document
    * @return A new DocumentHandler instance.
    */       
   public DocumentHandler (String filename)
   {
        try {
            FileReader fileReader = new FileReader(filename);
            this.createDocumentHandler(fileReader);        
        } catch (FileNotFoundException fnfEx) {
        }
   }
   

   /**  
    * Construct a document from a string containg XML.
    * All newline characters (ASCII 10 and 13) will be stripped out
    *
    * @param  xml The string containing the xml
    * @param  bAddDocRoot A boolean stating whether or not to add
    *         <DocRoot></DocRoot> as the outermost XML node.
    * @return A new DocumentHandler instance.
    */       
   public DocumentHandler (String xml, boolean bAddDocRoot)
   {
      this(xml, bAddDocRoot, false);
   }

   /**  
    * Construct a document from a string containg XML.
    *
    * @param  xml The string containing the xml
    * @param  bAddDocRoot A boolean stating whether or not to add
    *         <DocRoot></DocRoot> as the outermost XML node.
    * @param  keepNewLines - if true, newlines will be left in by the parser. 
    *         if false, they will be ignored
    * @return A new DocumentHandler instance.
    */       
   public DocumentHandler (String xml, boolean bAddDocRoot, boolean keepNewLines)
   {
        StringBuilder xmlString = new StringBuilder();

        if (bAddDocRoot) {
          xmlString.append ("<DocRoot>");
        }
        xmlString.append (xml);
        if (bAddDocRoot) {
          xmlString.append ("</DocRoot>");
        }
        StringReader stringReader = new StringReader (xmlString.toString ());
        this.createDocumentHandler(stringReader, keepNewLines);        
   }


   /**  
    * Create the document handler using the Reader as input.
    *
    * @param  reader The reader that contains the xml to be build.
    * @return A new DocumentHandler instance.
    */    
   private void createDocumentHandler (Reader reader)
    { 
       // Default behavior is to strip out new lines
       createDocumentHandler(reader, false);
    }

   
   /**  
    * Create the document handler using the Reader as input.
    *
    * @param  reader The reader that contains the xml to be build.
    * @param  keepNewLines - if true, newlines will be processed by the parser, otherwise they will be stripped out
    * @return A new DocumentHandler instance.
    */       
   private void createDocumentHandler (Reader reader, boolean keepNewLines)
   {    
        DocumentParser documentParser = new DocumentParser(reader, keepNewLines);
        int tokenArrayLength = 0;
        DocumentNode currentNode = null;
            
        //token will now contain all xml up until the first '<'
        char[] token = documentParser.getNextToken('<');
        //repeat until there are no more tokens in the file.
        while (token != null)
        {
            tokenArrayLength = documentParser.getReturnArrayBufferIndex();
            tokenArrayLength++;           
            //process the currentToken.
            currentNode = processToken(token, tokenArrayLength, currentNode);
            //get the next token
            token = documentParser.getNextToken('<');
        }        
        //set the root node to the last item returned from processToken
        this.root = currentNode;
        //set the current node to the root node.
        setCurrNode(root);        
   }
   
   /**  
    * Process a token contained in a character array.
    *
    * @param  charBuffer The character buffer that contains the xml text.
    * @param  tokenArrayLength The length of the character buffer.
    * @param  node The node currently being pointed to.
    * @return The new current node.
    */       
   private DocumentNode processToken (char[] charBuffer, int tokenArrayLength, DocumentNode node)
   {
        DocumentNode docNode;
        String tagName = null;
        String idValue = null;
        String value = null;
        
        //Read the data into a String
        String bufferString = new String(charBuffer,0,tokenArrayLength);
        bufferString = bufferString.trim();
 
        //If the bufferString starts with a ! then it must be a comment so skip
        //the entire token.  This should work as long as their comment does not
        //contain a <. 
        if (bufferString.charAt(0) == '!')
           return node;    
           
        //If I've found a closing tag   
        if (bufferString.charAt(0) == '/')
        {
           //grab the tag name
           tagName = bufferString.substring(1, bufferString.length()-1);
           //if the closing tag matches the opening tage we're on
           if (node != null && node.getTagName().equals(tagName))
           {
             //if we've got a parent then return the parent, otherwise
             //the node is the root, so return.
             if (node.getParentNode() != null)
                return node.getParentNode();
             else
                return node;
           }
           else
           {
             //if the closing tag does not match the opening tag print an error
             //to standard out, and return the current node.
            LOG.debug("Mismatched Document Tags in XML = ", node);
             return node;
           }
        }
        
        //if i've found a short circuit open/close tag (Ex. <money/> instead of 
        //<money></money> then...
        int checkIndex = bufferString.indexOf("/>");
        if (checkIndex != -1)
        {
           //grab the tagName
           tagName = bufferString.substring(0,checkIndex);
           //create a new document node with a blank value
           docNode = new DocumentNode(tagName,idValue,null,false);
           //if we're not the root node then
           if (node != null)
           {
              //append the new node to the current node and return the current
              //node because we will not be looking for a close tag
              node.appendChildToRight(docNode);
              return node;
           } else
           {
              //The short circuit open/close tage cannot be used at the root
              //level
        	   LOG.debug("Document cannot have a root node that starts and finishes");
           }
        }
        else
        {
           //I'm dealing with an opending tag.           
           if (!bufferString.isEmpty())
           {
              //Grab the value out of the string.  The value will be everything
              //found after the > in the remaining string.
              int valueIndex = bufferString.indexOf(">");
              if (valueIndex != -1)
              {
                if (valueIndex != bufferString.length()-1)
                {
                    
                   value = bufferString.substring(valueIndex+1);
                   bufferString = bufferString.substring(0,valueIndex);
                }
                else
                {
                   value = null;
                   bufferString = bufferString.substring(0,valueIndex);
                }
              }
              else
              {
                System.out.println("XML Token does not contain > "); 
              }
              
              //if and ID= is found in the string then grab the id and tag names 
              int idIndex = bufferString.indexOf("ID=");
              if (idIndex != -1)
              {
                 int tagIndex = idIndex-1;
                 tagName = bufferString.substring(0, tagIndex );
                 
                 if (idIndex != -1)
                 {
                    idValue = bufferString.substring(idIndex+4, bufferString.length()-1);
                 }
              }
              else
              {
                 tagName = bufferString.substring(0, bufferString.length()) ;
              }
           }
           docNode = new DocumentNode(tagName,idValue,value,true);
           if (node != null)
           {
              node.appendChildToRight(docNode);
           }
           return docNode;
        }
        return node;
   }
   
   /**  
    * Add a new component to the document tree without affecting the any 
    * existing nodes.
    *
    * @param  xpath The location in the XML document where the component
    *               should be added.
    * @param  doc DocumentHandler containing the component to be added.
    */       
   public void addComponent (String xpath, DocumentHandler doc)
   {
       //Find the location to add the component
       DocumentNode parentNode = getDocumentNode (xpath, true);
       //Append the document to the parent node starting with the left child of
       //of the root.
       if (parentNode != null) {
            parentNode.appendChildToLeft(this.makeComponent (doc.getRootNode().getLeftChildNode(), parentNode));
       }
   }   

   /**  
    * Get an attribute from the document specified by the xpath
    * existing nodes.
    *
    * @param  xpath The location in the XML document where the attribute
    *               should be retreived.
    */       
   public String getAttribute (String xpath)
   {
       DocumentNode node = getDocumentNode (xpath);
       if (node == null)
       {
           return null;
       }
       return getNodeText (node);
   }

   /**  
    * Get an attribute from the document specified by the xpath
    * existing nodes.
    *
    * @param  attributePath The location in the XML document where the attribute
    *               should be retreived.
    */       
   public BigDecimal getAttributeDecimal (String attributePath)
          throws AmsException
   {
       BigDecimal value = null;
       try
       {
           value = new BigDecimal (this.getAttribute (attributePath));
       }
       catch (NumberFormatException ex)
       {
           throw new AmsException (
               "The attribute: " + attributePath +
               " cannot be converted to a BigDecimal DataType");
       }
       return value;
   }

   /**  
    * Get an attribute from the document specified by the xpath
    * existing nodes.
    *
    * @param  xpath The location in the XML document where the attribute
    *               should be retreived.
    * @param  int containing the value at the specified xpath.
    */       
   public int getAttributeInt (String attributePath) throws AmsException
   {
       int value = 0;
       try
       {
           value = Integer.parseInt(this.getAttribute (attributePath));
       }
       catch (NumberFormatException ex)
       {
           throw new AmsException (
               "The attribute: " + attributePath +
               " cannot be converted to a long DataType");
       }
       return value;
   }

   /**  
    * Get an attribute from the document specified by the xpath
    * existing nodes.
    *
    * @param  xpath The location in the XML document where the attribute
    *               should be retreived.
    * @param  long containing the value at the specified xpath.
    */       
   public long getAttributeLong (String attributePath) throws AmsException
   {
       Long value = null;
       try
       {
           value = Long.valueOf(this.getAttribute (attributePath));
       }
       catch(NumberFormatException ex)
       {
           throw new AmsException (
               "The attribute: " + attributePath +
               " cannot be converted to a long DataType");
       }
       return value;
   }
   
   

   /**  
    * Gets attributes from the document specified by the xpath
    * existing nodes.
    *
    * @param  xpath The location in the XML document where the attribute
    *               should be retreived.
    */       
   public List<String> getAttributes (String xpath)
   {
       DocumentNode node = getDocumentNode (xpath);
       List<String> resultList = new ArrayList<> ();
       if (node == null) {
           return resultList;
       }

       resultList.add(getNodeText (node));
       DocumentNode nextNode = node.getRightSiblingNode();
       String tagName = node.getTagName();
       while (nextNode != null) {
           if (nextNode.getTagName().equals (tagName)) {
               resultList.add(getNodeText (nextNode));
           }
           nextNode = nextNode.getRightSiblingNode();
       }
       return resultList;
   }

   
   /**  
    * Sets a component removing any existing data at the xpath
    *
    * @param  xpath The location in the XML document where the component
    *               should be added.
    * @param  Doc containing the component at the specified xpath.
    */       
   public void setComponent (String xpath, DocumentHandler doc)
   {
       DocumentNode parentNode = getDocumentNode (xpath, true);
       if (parentNode != null)
       {
            this.removeAllChildren (parentNode);
            parentNode.appendChildToLeft(this.makeComponent(doc.getRootNode().getLeftChildNode(), parentNode));       
       }
   }   

   /**  
    * Gets a component from the specified xpath
    *
    * @param  xpath The location in the XML document from where the component
    *               should be retreived.
    * @param  Doc containing the component at the specified xpath.
    */       
   public DocumentHandler getComponent (String xpath)
   {
       DocumentNode parentNode = getDocumentNode (xpath);
       if (parentNode == null)
       {
          return null;
       }
       DocumentHandler doc = new DocumentHandler ();
       DocumentNode docNode = doc.getRootNode();
       
       docNode.appendChildToLeft(doc.makeComponent (parentNode.getLeftChildNode(), doc.getRootNode ()));
       return doc; 
   }

   /**  
    * Gets the document node at the specified xpath
    *
    * @param  xpath The location in the XML document from where the node
    *               should be retreived.
    * @param  DocumentNode at the specified xpath.
    */       
   public DocumentNode getDocumentNode (String xpath)
   {
       return getDocumentNode(xpath,false);
   }
   
    /**  
    * Finds the document node at the specified xpath
    *
    * @param  xpath The location in the XML document from where the node
    *               should be retreived.
    * @param  boolean Indicates whether or not to create nodes if the xpath
    *                 does not exist.
    */       
  public DocumentNode getDocumentNode (String xpath, boolean createPath)
   {
       DocumentNode locateNode = null;
       if (xpath.startsWith ("/"))
          //using absolute paths
          locateNode = root;
       else
          //using relative paths
          locateNode = getCurrNode();

       if (locateNode == null)
          return null;
       //parse the xpath strings
       StringTokenizer parser = new StringTokenizer (xpath, "/");
        
       //continue until the node is found
       while (parser.hasMoreTokens ())
       {
          String token = parser.nextToken ();
          String tagName = getPathTagName (token);
          String id = getPathId (token);
          //ShilpaR CR710 start
          String attribute = getAttrNameVal(token);
          if(null!=attribute && !attribute.isEmpty()){
       	   int index = tagName.indexOf ('[');
              if (index != -1)
              {
                  tagName = tagName.substring (0, index);
              }
          }
        //ShilpaR CR710 end
          //if .. move up a level in the tree hierarchy
          if (tagName.equals (".."))
              locateNode = locateNode.getParentNode ();
          else
          {
              if (tagName.equals ("."))
              {
                 // do nothing
              }
              else
              {
                 //find the child node that contains the tagname and id
                 DocumentNode tempNode = getNode (locateNode, tagName, id);
                 //if the node is not found then...
                 if (tempNode == null)
                 {
                   //if the node is not found and we don't want to create the
                   //path, then return null
                   if (!createPath)
                      return null;
                   //otherwise create a new node
                   DocumentNode newDocNode = null;
                 //ShilpaR CR710 start
                   if(null!=attribute && !attribute.isEmpty()){
                	  /* int index = tagName.indexOf ('[');
                       if (index != -1)
                       {
                           tagName = tagName.substring (0, index);
                       }*/

                	    newDocNode = new DocumentNode(tagName, id,attribute, "",false);
                   }
                 //ShilpaR CR710 end
                   else{
                    newDocNode = new DocumentNode(tagName, id, "",false);
                   }
                   //newDocNode.appendChildToRight(childNode)
                   //attach the new node to the current node
                   locateNode.appendChildToRight (newDocNode);
                   tempNode = newDocNode;
                 }
                 locateNode = tempNode;
               }
           }
        }
        //set the current node to the node found and send back a reference to
        //this node.
        setCurrNode (locateNode);

        return getCurrNode();
    }

    /**  
    * Looks one level deep from the node passed in for a node containing
    * the nodeName specified
    *
    * @param  DocumentNode The node where to start the search
    * @param  nodeName The name of the node we're lookin for.
    */       
   public DocumentNode getNode (DocumentNode locateNode, String nodeName)
   {
       return getNode (locateNode, nodeName, null);
   }

    /**  
    * Looks one level deep from the node passed in for a node containing
    * the nodeName specified with the id specified.
    *
    * @param  DocumentNode The node where to start the search
    * @param  nodeName The name of the node we're lookin for.
    * @param  id The idof the node we're lookin for.
    */       
   public DocumentNode getNode (DocumentNode locateNode, String nodeName, String id)
   {
        //start with the first child
        DocumentNode tempNode = locateNode.getLeftChildNode();
        
        //look through all the children for the node specified.
        while (tempNode != null)
        {
            //if our tag names match
            if (tempNode.getTagName().equals(nodeName))
            {
                //if the id was not specified in by the calling method we're done
                if (id == null)
                    return tempNode;
                
                //if they specified a tag name and it matches then we're done
                if ((tempNode.getIdName() != null) && (tempNode.getIdName().equals(id)))
                    return tempNode;
            }
            //look at the next node
            tempNode = tempNode.getRightSiblingNode();
        }
        //if we've gotten here we're done
        return null;        
   }

    /**  
    * Merges two documents together at the xpath specified
    *
    * @param  xpath The path that we want to place our new document
    * @param  doc The document to add.
    */       
   public void mergeDocument (String xpath, DocumentHandler doc)
   {              
       DocumentNode tempNode = this.getDocumentNode(xpath, true);
       if (tempNode != null) {
            tempNode.appendChildToRight(doc.getRootNode());       
       }
   }

    /**  
    * Returns a new documenthandler whose root points to the specified
    * xpath.  Note that this is not a copy, but instead references the same
    * nodes as in the original document.  Therefore a change to the original
    * document may effect the new document.
    *
    * @param  xpath The path that from which we want to retrieve our new 
    *               document
    * @return DocumentHandler whose root points to the specified xpath.
    */       
   public DocumentHandler getFragment (String xpath)
   {
       DocumentNode node = getDocumentNode (xpath);
       if (node == null)
       {
           return null;
       }
       DocumentHandler doc = new DocumentHandler (node);
       return doc;
   }

    /**  
    * Returns a vector of documenthandlers whose roots points to the 
    * children found at the specified xpath.
    * Note that this is not a copy, but instead references the same
    * nodes as in the original document.  Therefore a change to the original
    * document may effect the new document.
    * @deprecated
    * @param  xpath The path that from which we want to retrieve our new 
    *               document
    * @return Vector of dochandlers whose roots point to the children
    *         found at the specified xpath.
    */       
   public Vector<DocumentHandler> getFragments (String xpath)
   {
	   
	   return new Vector<>(getFragmentsList(xpath));
   }
   
   /**  
    * Returns a list of documenthandlers whose roots points to the 
    * children found at the specified xpath.
    * Note that this is not a copy, but instead references the same
    * nodes as in the original document.  Therefore a change to the original
    * document may effect the new document.
    * 
    * @param  xpath The path that from which we want to retrieve our new 
    *               document
    * @return List of dochandlers whose roots point to the children
    *         found at the specified xpath.
    */       
   public List<DocumentHandler> getFragmentsList (String xpath)
   {
       List<DocumentHandler> docList = new ArrayList<> ();
       //find the node at the xpath
       DocumentNode parentNode = getDocumentNode (xpath);
       if (parentNode == null)
       {
           return docList;
       }

       DocumentHandler doc = new DocumentHandler (parentNode);
       docList.add(doc);

       DocumentNode nextNode = parentNode.getRightSiblingNode();
       String tagName = parentNode.getTagName();
       while (nextNode != null)
       {
           if (nextNode.getTagName ().equals (tagName))
           {
               DocumentHandler tempDoc = new DocumentHandler (nextNode);
               docList.add(tempDoc);
           }
           nextNode = nextNode.getRightSiblingNode ();
       }
       return docList;
   }

    /**  
    * Removes all the children at the specified xpath
    *
    * @param  xpath The path that we want to remove all the child nodes
    */       
   public void removeAllChildren (String xpath)
   {
       DocumentNode node = getDocumentNode (xpath);
       if (node == null)
       {
           return;
       }
       this.removeAllChildren (node);
   }

    /**  
    * Removes all the children at the specified xpath
    *
    * @param  DocumentNode The node at which we want to remove all children
    */       
   public void removeAllChildren (DocumentNode node)
   {
       DocumentNode tempNode = node.getLeftChildNode();
       node.setLeftChildNode(null);
       
       while (tempNode != null)
       {
         tempNode.setParentNode(null);
         tempNode = tempNode.getRightSiblingNode();
       }
   }

    /**  
    * Sets the attribute value at the path specified.
    *
    * @param  xpath The path at which we want to set the specified value
    * @param  value The value to be set 
    */       
   public void setAttribute (String xpath, String value)
   {
       DocumentNode node = getDocumentNode (xpath, true);   
       this.setNodeText (node, value);
   }

    /**  
    * Sets the attribute value at the path specified.
    *
    * @param  xpath The path at which we want to set the specified value
    * @param  value The value to be set 
    */       
   public void setAttributeDecimal (String attributePath, BigDecimal value)
   {
       this.setAttribute (attributePath, value.toString ());
   }

    /**  
    * Sets the attribute value at the path specified.
    *
    * @param  attributePath The path at which we want to set the specified value
    * @param  value The value to be set 
    */       
   public void setAttributeInt (String attributePath, int value)
   {
       this.setAttribute (attributePath, Integer.toString(value));
   }

    /**  
    * Sets the attribute value at the path specified.
    *
    * @param  attributePath The path at which we want to set the specified value
    * @param  value The value to be set 
    */       
   public void setAttributeLong (String attributePath, long value)
   {
       Long longValue = value;
       this.setAttribute (attributePath, longValue.toString ());
       //return;
   }

   

   /**  
   * Sets the attribute value sat the path specified.
   *
   * @param  xpath The path at which we want to set the specified values
   * @param  values The values to be set
   */       
  public void setAttributes (String xpath, List<String> values)
  {
      if (values == null)     return;
      
      int numValues = values.size ();
      if (numValues == 0)     return;
      
      
      DocumentNode node = getDocumentNode (xpath, true);
      this.setNodeText (node, values.get(0));
      DocumentNode parentNode = node.getParentNode ();
      for (int loop = 1; loop < numValues; loop++)
      {
          parentNode.appendChildToRight (new DocumentNode(node.getTagName(),values.get(loop),false));
      }
  }

   
   
   private void setNodeText (DocumentNode node, String text)
   {
       if (node != null)
       {
          node.setNodeValue(text,false);
       }
   }

   public void removeComponent (String path)
   {
       DocumentNode node = getDocumentNode (path);
       if (node == null)
       {
           return;
       }
       DocumentNode parentNode = node.getParentNode();
       if (parentNode != null)
       {
           parentNode.removeChild(node);
       }
    }

   public String toString ()
   {
       return toString(false);
   }   

   public String toString(boolean formatted)
   {
       int depth = 0;
       StringBuffer strBuffer = new StringBuffer();
       strBuffer = this.toString(root,strBuffer,formatted,depth);
       return strBuffer.toString();       
   }
   
   public StringBuffer toString (DocumentNode node, StringBuffer strBuffer, boolean formatted, int depth)
   {
       // W Zhu 16 Jun 2015 Rel 9.3 T36000031888 use loop instead of recursion for siblings to avoid stack overflow
       while(node != null) {
           if (formatted)
           {
               for (int i=0;i<=depth;i++)
               {
                  strBuffer.append(" ");
               }
           }
           strBuffer.append("<");
           strBuffer.append(node.getTagName());
           if (node.getIdName() != null)
           {
               strBuffer.append(" ID=\"");
               strBuffer.append(node.getIdName());
               strBuffer.append("\"");
           }
           //ShilpaR CR710 start
           if (node.getAttr()!= null && !node.getAttr().isEmpty())
           {
               String name = node.getAttr();
               String[] values = name.split("=");
               strBuffer.append(" ").append(values[0]).append("=\"");
               strBuffer.append(values[1]);
               strBuffer.append("\"");
           }
           //ShilpaR CR710 end
           strBuffer.append(">");

           if (node.getNodeValue(false) != null)
                strBuffer.append(node.getNodeValue(true));
           if (node.getLeftChildNode() != null)
           {           
               if (formatted)
               {
                    strBuffer.append("\n");
                    depth++;
               }
               this.toString(node.getLeftChildNode(),strBuffer, formatted, depth);
               if (formatted)
               {
                    depth--;
                    strBuffer.append("\n");
                    for (int i=0;i<=depth;i++)
                    {
                      strBuffer.append(" ");
                    }
               }
           }

           strBuffer.append("</");
           strBuffer.append(node.getTagName());
           strBuffer.append(">");
       
           if ((node != this.root)&&(node.getRightSiblingNode() != null))
           {
              if (formatted)
              {
                   strBuffer.append("\n");
              }
              // W Zhu 16 Jun 2015 Rel 9.3 T36000031888 use loop instead of recursion for siblings to avoid stack overflow
              //this.toString(node.getRightSiblingNode(),strBuffer, formatted, depth);
              node = node.getRightSiblingNode();
           }
           else {
               node = null;
           }
       }
       
       return strBuffer;
   }   

   private DocumentNode makeComponent (DocumentNode fromNode, DocumentNode toNode)
   {
       if (toNode != null) 
       {
         if (fromNode != null)
         {           
            DocumentNode newNode = new DocumentNode(fromNode.getTagName(),
                                                    fromNode.getIdName(),
                                                    fromNode.getAttr(), //BSL IR RSUM041136929 04/12/2012 - include attribute
                                                    fromNode.getNodeValue(false),false);
            if (fromNode.getLeftChildNode() != null)
            {       
                newNode.appendChildToLeft(this.makeComponent(fromNode.getLeftChildNode(),newNode));
            }
            
            if (fromNode.getRightSiblingNode() != null)
            {
                toNode.appendChildToLeft(this.makeComponent(fromNode.getRightSiblingNode(), toNode));
            }
            
            return newNode;
         }
       }
       return null;
   }

   private String getNodeText (DocumentNode node)
   {
       return node.getNodeValue(false);
   }

   private String getPathId (String xpathToken)
   {
       int startIndex = xpathToken.indexOf ('(');
       int endIndex = xpathToken.indexOf (')');
       if (startIndex != -1)
       {
           xpathToken = xpathToken.substring (startIndex + 1, endIndex);
           return xpathToken;
       }
       return null;
   }
   
   private String getAttrNameVal (String xpathToken)
   {
       int startIndex = xpathToken.indexOf ('[');
       int endIndex = xpathToken.indexOf (']');
       if (startIndex != -1)
       {
           xpathToken = xpathToken.substring (startIndex + 1, endIndex);
           return xpathToken;
       }
       return null;
   }
   

   private String getPathTagName (String xpathToken)
   {
       int index = xpathToken.indexOf ('(');
       if (index != -1)
       {
           xpathToken = xpathToken.substring (0, index);
       }
       return xpathToken;
   }

   private DocumentNode getCurrNode ()
   {
      if (currNodeHT == null)
           currNodeHT = new Hashtable<> ();

      DocumentNode returnNode =
              currNodeHT.get (Thread.currentThread ().getName ());
      
      if (returnNode == null) 
           returnNode = root;
 
      return returnNode;
   }
   
   public DocumentNode getRootNode ()
   {
      return this.root;
   }

   private void setCurrNode (DocumentNode nodePointer)
   {
       if (currNodeHT == null)
           currNodeHT = new Hashtable<> ();
       currNodeHT.put (Thread.currentThread ().getName (), nodePointer);
   }
   
}