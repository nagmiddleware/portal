package com.amsinc.ecsg.util;

import java.io.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

import org.apache.soap.util.mime.ByteArrayDataSource;

import com.amsinc.ecsg.frame.*;

/**
 * This class encapsulates sending an e-mail using SMTP servers specified 
 * in an EmailConfig.xml file. 
 */
public class EmailSender
 {
 	// The number of times to skip over an SMTP server that has been marked as 
 	// invalid before trying to reconnect
	private static int MAX_SKIPS = 20;
	
	// List of all available SMTP servers
	private Vector smtpSessions;	
	
	// Keeps track of which session was last used
	private int lastSessionUsed = -1;
	
	// The singelton instance of this class
	private static EmailSender emailSenderInstance;
	
	// Messages that are passed back with exceptions

	// This indicates that the SMTP server could not be connected to
	public static String SMTP_CONNECTION_ERROR = "SMTPConnectionError";

        // This flag is set to true if all SMTP servers are down
        private boolean allSmtpServersAreDown = false;
	
	// This indicates that there was a problem with sending the message 
	// This is not likely to be recovered from without changing the contents
	public static String MESSAGE_SEND_ERROR = "MessageSendError";

	public static final String PDF_FILE_TYPE = "application/octet-stream"; //SHILPAR CR-597
 	public static final String DEFAULT_PDF_ATTACHMENT_NAME = "Attachment.pdf"; //SHILPAR CR-597

	/*
	 * Factory method to be used to create an instance of EmailSender
	 * that can be used to send e-mail.
	 * 
	 */
	public static EmailSender getInstance() throws AmsException
	 {
	   if (emailSenderInstance == null) 
	   {
		  synchronized(EmailSender.class)
		  {
			 if (emailSenderInstance == null)
			 {
				emailSenderInstance = new EmailSender();
				return emailSenderInstance;
			 }
		  }
	   }
	   
	   return emailSenderInstance;  				
	}

	/*
	 * Constructor for this class.   It's private so that you can only get to it
	 * through the getInstance() method.  
	 * 
	 */
   	private EmailSender() throws AmsException
	 {
     		refresh();
         }
    
	/*
	 * Constructor for this class.   It's private so that you can only get to it
	 * through the getInstance() method.  This method reads data from the
	 * EmailConfig.xml file and places the SMTP information into a list of servers.
	 * 
	 */    
	public void refresh() throws AmsException
         {
		String configFilePath = JPylonProperties.getInstance().getString("configFilePath");
		String pathToEmailConfig = configFilePath + "/EmailConfig.xml";
		DocumentHandler emailConfigXml = new DocumentHandler(pathToEmailConfig);

		Vector v = emailConfigXml.getFragments("SmtpServer");

		Enumeration enumer = v.elements();		

		smtpSessions = new Vector(v.size());

		// Loop through each of the SMTP servers in the list
		while (enumer.hasMoreElements())
		 {
			DocumentHandler smtpConfigXml =
				(DocumentHandler) enumer.nextElement();

			String host = smtpConfigXml.getAttribute("host");
			String port = smtpConfigXml.getAttribute("../port");
			String login = smtpConfigXml.getAttribute("../login");
			String password = smtpConfigXml.getAttribute("../password");
			String debug = smtpConfigXml.getAttribute("../debug");

			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);

                        if( (debug != null) && (debug.equals("true")))
   			    props.put("mail.debug", debug);
			else
   			    props.put("mail.debug", "false");

			// Create a session object that corresponds to this SMTP server
			Session smtpSession;

			if (login != null && password != null
				&& !login.isEmpty() && !password.equals(""))
			 {
				props.put("mail.smtp.auth", "true");
				smtpSession =
					Session.getInstance(props, new SmtpPasswordAuthenticator(login, password));
			 }
		    else
				smtpSession = Session.getDefaultInstance(props, null);
			
			// Create an EmailSenderSmtpSession object for this SMTP server
			// and add it to the list
			EmailSenderSmtpSession emailSenderSession = new EmailSenderSmtpSession();
			emailSenderSession.session = smtpSession;
			emailSenderSession.isActive = true;
			
			try
			{
				emailSenderSession.transport = smtpSession.getTransport("smtp");
 				smtpSessions.addElement(emailSenderSession);
			}
			catch(MessagingException me)
			 {
			 	// If there was a problem, still add it to the list
				// It will just be marked as a "bad" SMTP server
			 	me.printStackTrace();
			 }
		 }
  	 }

	/**
	 * Returns a valid Session object.  This method will look through the 
	 * list of SMTP servers until it finds a valid one.  If no valid ones
	 * are found, AmsException will be thrown.
	 * 
         * This method is synchronized because it controls access to the session,
         * which is shared by anyone calling a method on this class.
         *
	 * @return Session - a valid Session object where its transport object has
	 * connected
	 * @throws AmsException
	 */
        private synchronized EmailSenderSmtpSession getNextSession() throws AmsException
         {
			 int sessionsTried = 0;

		do
		 {
			int indexOfSessionToUse;

			if ((lastSessionUsed + 1) == smtpSessions.size())
				indexOfSessionToUse = 0;		  
	    		else
				indexOfSessionToUse = (++lastSessionUsed);

			// Get the next session from the list (in round-robin order)
			 EmailSenderSmtpSession theSession = (EmailSenderSmtpSession) smtpSessions.elementAt(indexOfSessionToUse);

			 // If the session is marked as active, return it
			if (theSession.isActive)
			 {
			 	boolean connectedOK;
			 	
			 	// Try to connect using that session
			 	try
			 	 {
					if(!theSession.transport.isConnected())
						  theSession.transport.connect();
					connectedOK = true;			 	 	
			 	 }
			 	catch(MessagingException me)
			 	 {
			 	 	// If connection is unsuccesful, mark the session as invalid
			 	 	theSession.isActive = false;
			 	 	connectedOK = false;
			 	 }
					
				if(connectedOK)
                                 {
                                        allSmtpServersAreDown = false;
					return theSession;
                                 }
			 } 
			
			// Session was marked as being inactive
                        // If all SMTP servers are down, try right away
                        // MAX_SKIPS was put in to prevent a major slowdown if one server out of two or three was down
			// Only try to reconnect every MAX_SKIPS number of times
			// That way, we're not trying to connect every time
			if (allSmtpServersAreDown || theSession.invalidSkippedCount == MAX_SKIPS)
			 {
				// Reset the invalid skipped count
				theSession.invalidSkippedCount = 0;
				try
				 {
					theSession.transport.connect();

					// If no exception was thrown, we could reconnect,
					// so mark it as active and return the session
					theSession.isActive = true;
                                        allSmtpServersAreDown = false;
					return theSession;
				 }
				catch (MessagingException me)
				 {
                                        me.printStackTrace();

					// Connecting to the SMTP server failed, so increase
					// the count of times that we've skipped it
					theSession.invalidSkippedCount++;
				 }
			 }
			else
			  // Since we're just skipping this server, increment the count 
			  theSession.invalidSkippedCount++;			 	 	

			sessionsTried++;
		 }
		// Only do this as many times as the number of sessions there are
		// No point in looping through more than once
		while (sessionsTried < smtpSessions.size());

		// If no sessions were found to return, ALL of the SMTP servers are down
		// so throw exception to let the calling method handle it
                allSmtpServersAreDown = true;
		throw new AmsException(SMTP_CONNECTION_ERROR);   
    	 }


	/**
	 * Sends an e-mail using one of the SMTP servers specified
	 * in EmailConfig.xml.  This method allows passing an array of strings when
	 * there are more than one recipient.
	 * 
	 * @param to - array of e-mail address strings for the To: line
	 * @param cc - array of e-mail address strings for the CC: line
	 * @param bcc - array of e-mail address strings for the BCC: line
	 * @param from - e-mail address for the FROM: line
	 * @param fromPersonalName - personal name that corresponds to the FROM: line
	 * @param subject - the subject of the e-mail
	 * @param body - the body of the e-mail
	 * @throws AddressException
	 */
	public void sendEmail(String[] to, String[] cc, String[] bcc,
		String from, String fromPersonalName, String subject,
		String body,String attachment,String attachmentName) throws AmsException
	 {
	  	 try
	 	  {
		 	// Arrays of strings need to be converted to arrays of
		 	// InternetAddress objects

			  if (to != null) {
				InternetAddress[] toAddresses = new InternetAddress[to.length];

				for (int i = 0; i < to.length; i++)
					toAddresses[0] = new InternetAddress(to[0]);
			}
	
			if (cc != null) {
				InternetAddress[] ccAddresses = new InternetAddress[cc.length];

				for (int i = 0; i < cc.length; i++)
					ccAddresses[0] = new InternetAddress(cc[0]);
			}
	
			if (bcc != null) {
				InternetAddress[] bccAddresses = new InternetAddress[bcc.length];

				for (int i = 0; i < bcc.length; i++)
					bccAddresses[0] = new InternetAddress(bcc[0]);
			}
		  }
		 catch (AddressException ae)
		  {
			 // Output actual exception to the agent log????
			 throw new AmsException(MESSAGE_SEND_ERROR);
		  }		

		// Pass the arguments along to the method that actually does the work
		sendEmail(to, cc, bcc, from, fromPersonalName, subject,	body,attachment,attachmentName);
	 }
	/**
	 * Sends an e-mail without attachment using one of the SMTP servers specified
	 * in EmailConfig.xml.  This method allows passing an array of strings when
	 * there are more than one recipient.
	 *
	 * @param to - array of e-mail address strings for the To: line
	 * @param cc - array of e-mail address strings for the CC: line
	 * @param bcc - array of e-mail address strings for the BCC: line
	 * @param from - e-mail address for the FROM: line
	 * @param fromPersonalName - personal name that corresponds to the FROM: line
	 * @param subject - the subject of the e-mail
	 * @param body - the body of the e-mail
	 * @throws AddressException
	 */
	public void sendEmail(String[] to, String[] cc, String[] bcc,
		String from, String fromPersonalName, String subject,
		String body) throws AmsException
	 {
	  	 // Pass the arguments along to the method that actually does the work
		sendEmail(to, cc, bcc, from, fromPersonalName, subject,	body,null,null);
	 }
	/**
	 * Sends an e-mail without attachments using one of the SMTP servers specified
	 * in EmailConfig.xml.  This method allows passing a comma separated list of
	 * e-mail addresses
	 * 
	 * @param to - e-mail address(es) for the To: line
	 * @param cc - e-mail address(es) for the CC: line
	 * @param bcc - e-mail address(es) for the BCC: line
	 * @param from - e-mail address for the FROM: line
	 * @param fromPersonalName - personal name that corresponds to the FROM: line
	 * @param subject - the subject of the e-mail
	 * @param body - the body of the e-mail
	 * @throws AddressException
	 */
	public void sendEmail(String to, String cc, String bcc, String from,
		String fromPersonalName, String subject, String body) throws AmsException
	 {

		sendEmail(to, cc,	bcc, from,	fromPersonalName, subject, body,null,null);
	 }

	/**
	 *
	 *
	 * Sends an e-mail using one of the SMTP servers specified
	 * in EmailConfig.xml.  This method allows passing a comma separated list of
	 * e-mail addresses
	 * SHILPAR CR-597 changed the the to send attachment also
	 *
	 * @param to - e-mail address(es) for the To: line
	 * @param cc - e-mail address(es) for the CC: line
	 * @param bcc - e-mail address(es) for the BCC: line
	 * @param from - e-mail address for the FROM: line
	 * @param fromPersonalName - personal name that corresponds to the FROM: line
	 * @param subject - the subject of the e-mail
	 * @param body - the body of the e-mail
	 * @param attachmentName - the filename of the attachment
	 * @param attachmentBytes - the binary contents of the attachment
	 * @throws AddressException
	 */
	//BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
	//public void sendEmail(String to, String cc, String bcc, String from, String fromPersonalName, String subject, String body,String attachment,String attachmentName) throws AmsException
	public void sendEmail(String to, String cc, String bcc, String from,
			String fromPersonalName, String subject, String body,
			String attachmentName, byte[] attachmentBytes) throws AmsException
	//BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
	 {
		InternetAddress[] toAddresses = null;
		InternetAddress[] ccAddresses = null;
		InternetAddress[] bccAddresses = null;

     	        // Parse through each string passed in to create an array of addresses
      	        // This allows for the strings to be comma separated lists
		try
		 {
			if (to != null)
				toAddresses = InternetAddress.parse(to, false);
	
			if (cc != null)
				ccAddresses = InternetAddress.parse(cc, false);
	
			if (bcc != null)
				bccAddresses = InternetAddress.parse(bcc, false);
		 }
		catch (AddressException ae)
		 {
			// Output actual exception to the agent log????
			throw new AmsException(MESSAGE_SEND_ERROR);
		 }
		//BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
		//sendEmail(toAddresses, ccAddresses,	bccAddresses, from,	fromPersonalName, subject, body,attachment,attachmentName);
		sendEmail(toAddresses, ccAddresses,	bccAddresses,
				from, fromPersonalName, subject, body,
				attachmentName, attachmentBytes);
		//BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
	}


	/**
	 * Sends an e-mail using one of the SMTP servers specified
	 * in EmailConfig.xml.  This method actually does the work of sending the
         * e-mail, unlike its overloads above.   
	 * 
	 * @param to - e-mail address(es) for the To: line
	 * @param cc - e-mail address(es) for the CC: line
	 * @param bcc - e-mail address(es) for the BCC: line
	 * @param from - e-mail address for the FROM: line
	 * @param fromPersonalName - personal name that corresponds to the FROM: line
	 * @param subject - the subject of the e-mail
	 * @param body - the body of the e-mail
	 * @param attachmentName - the filename of the attachment
	 * @param attachmentBytes - the binary contents of the attachment
	 * @throws AddressException
	 */
	//BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
	//private void sendEmail(InternetAddress[] to, InternetAddress[] cc, InternetAddress[] bcc, String from, String fromPersonalName, String subject, String body, String attachment, String attachmentName) throws AmsException
	private void sendEmail(InternetAddress[] to, InternetAddress[] cc,
			InternetAddress[] bcc, String from, String fromPersonalName,
			String subject, String body, String attachmentName,
			byte[] attachmentBytes) throws AmsException
	//BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
	 {

		 // First, try to get a session to connect to
		 	// If no sessions are available, AmsException will be thrown by
		 	// the getNextSession() method
		 	EmailSenderSmtpSession sessionFromList = getNextSession();
		 Session smtpSession = sessionFromList.session;

		 // Get the transport and connect to the session
		 Transport transport = sessionFromList.transport;

		 jPylonMimeMessage message = new jPylonMimeMessage(smtpSession);
			try
			 {
			 	// Set the fields of the e-mail message
				message.setFrom(new InternetAddress(from, fromPersonalName));
				message.setRecipients(Message.RecipientType.TO, to);
				message.setRecipients(Message.RecipientType.CC, cc);
				message.setRecipients(Message.RecipientType.BCC, bcc);
				message.setSubject(subject);
				message.setSentDate(GMTUtility.getGMTDateTime());
				//SHILPAR start CR-597 add attachment part
				MimeBodyPart messagePart = new MimeBodyPart();
				messagePart.setText(body);
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messagePart);
                // If there are attachments, create different body parts
				//BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
//				if(!"".equals(attachment) && null!=attachment){
//					MimeBodyPart attachmentPart = new MimeBodyPart();
//					ByteArrayDataSource ds = new ByteArrayDataSource(attachment,PDF_FILE_TYPE);
				if(attachmentBytes != null && attachmentBytes.length > 0) {
					MimeBodyPart attachmentPart = new MimeBodyPart();
					ByteArrayDataSource ds = new ByteArrayDataSource(attachmentBytes, PDF_FILE_TYPE);
					//BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
					attachmentPart.setDataHandler(new DataHandler(ds));
					attachmentName= (!"".equals(attachmentName) && null!=attachmentName)?attachmentName:DEFAULT_PDF_ATTACHMENT_NAME;
					attachmentPart.setFileName(attachmentName);
					multipart.addBodyPart(attachmentPart);
				}
				message.setContent(multipart);
				//SHILPAR CR-597 end
			 }
		        catch (MessagingException me)
		         {
				// If an exception occurs building the message object, mark this as unsendable
				me.printStackTrace();
				throw new AmsException(MESSAGE_SEND_ERROR);
			 }
			catch (UnsupportedEncodingException uee)
			 {
				// Can't happen
		         }

                        try
                         {
			//SHILPAR CR-597 changed from transport.sendMessage() to transport.send() as sendMessage will consider attachment as the message body
               transport.send(message, message.getAllRecipients());
			 }
		        catch (SendFailedException sfe)
		         {
				// The SMTP server was contacted and the message couldn't be sent.  Mark as unsendable
				sfe.printStackTrace();
				throw new AmsException(MESSAGE_SEND_ERROR);
			 }
		        catch (MessagingException me)
		         {
				// There was a problem sending the message
                                // MessagingExceptions can be caused by lots of things 
                                // See the stack trace or run with SMTP debug on for more details if exception is being thrown
                                // It's likely that error is due to problem with connecting to SMTP server, so 
                                // mark it as such.
				me.printStackTrace();
				throw new AmsException(SMTP_CONNECTION_ERROR);
			 }

   	 }

       public static void main(String argc[])
        {
           try
            {
                   EmailSender sender = EmailSender.getInstance();

	           if((argc == null) || (argc.length < 5))
	            {
	               System.out.println("There was a problem with the parameters.  Parameters that are multiple words need to be placed in quotes.");
	               System.out.println("Please enter them as follows:");
	               System.out.println("  1.) TO: address");
	               System.out.println("  2.) FROM: address");
	               System.out.println("  3.) FROM: personal name");
	               System.out.println("  4.) subject");
	               System.out.println("  5.) content");
	               System.out.println("  6.) attachmentName");

                       return;
	            }         
	
	           String toAddress = argc[0];
	           String fromAddress = argc[1];
	           String fromPersonalName = argc[2]; 
	           String subject = argc[3];
	           String content = argc[4];
	           String attachment = argc[5];
	           String attachmentName = argc[6];
	           
	           //BSL Cocoon Upgrade 10/11/11 Rel 7.0 BEGIN
	           //sender.sendEmail(toAddress, null, null, fromAddress, fromPersonalName, subject, content,attachment,attachmentName);
	           byte[] attachmentBytes = null;
	           if (attachment != null && attachment.length() > 0) {
	        	   attachmentBytes = attachment.getBytes();
	           }

	           sender.sendEmail(toAddress, null, null, fromAddress,
	        		   fromPersonalName, subject, content,
	        		   attachmentName, attachmentBytes);
	           //BSL Cocoon Upgrade 10/11/11 Rel 7.0 END
	           System.out.println("Email sent");
            }
           catch(Exception e)
            {
              e.printStackTrace();
            }
        }

	/**
	 * Subclass of MimeMessage that allows us to override how the
         * Message-ID is set.
	 * 
	 */
        private class jPylonMimeMessage extends MimeMessage
         {
            /**
             * Constructor
             */
            protected jPylonMimeMessage(Session session)
             {
                super(session);
             }


            /**
             * Override of superclass.
             * 
             * Sets the Message-ID field in the header.  JavaMail's default is to
             * include the user ID and machine name in the Message-ID.  We don't 
             * want this for security reasons.
             */
            protected void updateHeaders () throws MessagingException
             {
               // Let the superclass set all of the header fields
               super.updateHeaders ();
               
               // Get the millisecond that the message is being sent
               String timeString = String.valueOf(new Date().getTime());

               // Put a random number on there too just to be safe
               String random     = String.valueOf((int)(1000000 * Math.random()));

               // Concatenate it together
               // Include in <> and include '@' so that it will follow standards
               String messageId = "<" + random+"."+timeString + ".JavaMail.jPylon@jPylon>";

               // Set the field
               super.setHeader ("Message-ID", messageId);
             }

         }


	/**
	 * This static inner class encapsulates an SMTP server.
	 * 
	 */
	private static class EmailSenderSmtpSession
	 {
		protected boolean isActive = false;
		protected Session session = null;
		protected int invalidSkippedCount = 0;  
		protected Transport transport = null;  	
	 }

        /**
         * This static inner class is required to support authentication with
         * an SMTP server.
         * 
	 */
	private static class SmtpPasswordAuthenticator extends Authenticator
	 {
		String user;
		String pw;

		public SmtpPasswordAuthenticator(String username, String password)
		 {
			super();
			this.user = username;
			this.pw = password;
		 }

		public PasswordAuthentication getPasswordAuthentication()
		 {
			return new PasswordAuthentication(user, pw);
		 }
	 }
}