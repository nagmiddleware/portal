package com.amsinc.ecsg.util;

/*
 * @(#)FullDocumentHandler
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import org.w3c.dom.Document;
import org.w3c.dom.*;
import java.util.*;
import org.apache.xerces.parsers.DOMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import org.xml.sax.*;
import com.amsinc.ecsg.frame.*;

/**
 * The interface that defines how a document can be accessed. Implementations
 * of this interface would allow underlying document sources such as XML to
 * be handled via a naming and directory-like interface. The interface provides
 * for a document model independent implementation.
 * <br>
 * Note on supported XPath notation: <br>
 * This version of DocumentHandler supports the directory naming structure and
 * ID specification portions of XPath. Other query notations available within
 * XPath are not currently supported here.
 *

 * @version 2.0, 03/24/2000
 * @since   JDK1.1
 */
public class FullDocumentHandler implements java.io.Serializable
{
	private static final Logger LOG = LoggerFactory.getLogger(FullDocumentHandler.class);
    /**
     * The document root node
     */
    private Document document;
    /**
     * The document root node referenced as an element
     */
    private Element root;
    /**
     * Holds a reference to the current node; allows for relative path names.
     * We are using a hash to support multithreading. Each thread name can
     * have a different current node pointer.
     */
    private transient Hashtable currNodeHT = new Hashtable();

   /**  
    * Default constructor.
    *
    * @return  A new DocumentHandler instance
    */       
   public FullDocumentHandler ()
   {
        try
        {
            DOMParser parser = new DOMParser ();
            StringReader stream = new StringReader ("<DocRoot></DocRoot>");
            InputSource is = new InputSource (stream);
            parser.parse (is);
            document = parser.getDocument ();
            root = document.getDocumentElement ();
            setCurrNode (root);
        } 
        catch (Exception e)
        {
            LOG.error ("Exception occured instanting FullDocumentHandler",e);
        }
    }
   /**  
    * Create a new instance of DocumentHandler by parsing in the 
    * XML file called filename.
    *
    * @param filename The name of the file to be parsed into myself.
    * @return A new DocumentHandler instance.
    */        
    public FullDocumentHandler (String filename)
    {
        try
        {
            DOMParser parser = new DOMParser ();
            FileReader fileReader = new FileReader(filename);
            InputSource is = new InputSource (fileReader);
            parser.parse (is);
            document = parser.getDocument ();
            root = document.getDocumentElement ();
            setCurrNode (root);
        } 
        catch (org.xml.sax.SAXParseException ex)
        {
            LOG.error ("Exception occured instanting FullDocumentHandler",ex);
        }       
        catch (Exception e)
        {
        }

    }

   /**  
    * Construct a document from a string of XML.
    *
    * @param  xml A string of XML text
    * @param  bAddDocRoot Indicator of whether XML should be wrapped with
    * <DocRoot> tag 
    * @return A new FullDocumentHandler instance.
    */       
    public FullDocumentHandler (String xml, boolean bAddDocRoot)
    {
      
        try
        {
            DOMParser parser = new DOMParser ();
            StringBuilder xmlString = new StringBuilder();
            int index = xml.indexOf ("<?xml");
            int fromIndex = 0;
            if (index != -1)
            {
                fromIndex = xml.indexOf ("?>", index);
                if (fromIndex == -1)
                {
                    throw new AmsException (
                        "XML version tag could not be parsed.");
                }
                fromIndex = fromIndex + 2;
                xmlString.append (xml.substring (0, fromIndex));
            }
            if (bAddDocRoot)
            {
              xmlString.append ("<DocRoot>");
            }
            xmlString.append (xml.substring (fromIndex));
            if (bAddDocRoot)
            {
              xmlString.append ("</DocRoot>");
            }
            StringReader stream = new StringReader (xmlString.toString ());
            InputSource is = new InputSource (stream);
            parser.parse (is);
            document = parser.getDocument ();
            root = document.getDocumentElement ();
            setCurrNode (root);
        } 
        catch (Exception e)
        {
        	LOG.error ("Exception occured instanting FullDocumentHandler",e);
        }
    }
    
   /**  
    * Construct a document fragment.
    *
    * @param document node to use as root node for document fragment
    * @param node A dummy parameter to differentiate this constructor 
    * @return A new FullDocumentHandler instance.
    */       
    public FullDocumentHandler (Document document, Node node)
    {
        try
        {
            this.document = document;
            root = (Element)node;
            setCurrNode (root);
        } 
        catch (Exception e)
        {
            LOG.error ("Exception occured instanting FullDocumentHandler",e);
        }
    }

   /**  
    * Insert the document at the specified path, not affecting any other
    * nodes currently at the specified path.
    *
    * @param path The document path of the component to match.
    * @param doc FullDocumentHandler instance to be set at the path.
    */
   public void addComponent (String path, FullDocumentHandler doc)
   {
       Node parentNode = getDocumentNode (path, true);
       this.makeComponent (doc.getRoot(), parentNode);
   }   
   /**  
    *  Clone a node source node and return the new node.
    *
    *  @param  sourceNode The node to be cloned. 
    *  @return The copy or clone of sourceNode.
    */ 
   public Node copyNode (Node sourceNode)
   {
        Element newElement =
            (document.createElement (sourceNode.getNodeName ()));
        // Copy text node
        String text = this.getNodeText (sourceNode);
        Text newText = document.createTextNode (text);
        newElement.appendChild (newText);
        
        // Copy attribute nodes
        NamedNodeMap nodeMap = ((Element)sourceNode).getAttributes();
        for (int j = 0; j < nodeMap.getLength ();j++)
        {
            Attr newAttr = ((Attr)(nodeMap.item (j)));
            if (!(newAttr.getValue().isEmpty()))
                newElement.setAttribute (
                    newAttr.getName (), newAttr.getValue ());
        }
        return newElement;
   }   
   /**  
    * This method gets the value of a String attribute located in the specified
    * document path. Also, validate to check that the user has not entered an
    * id value at the end of the attribute path. If not, an exception is thrown.
    * <br>
    * Example: to retrieve the value of an Account balance, in an Account
    * document with the following tree structure:<br>
    *
    *      Account<br>
    *          *-Summary<br>
    *              *-Balance = 300.00<br>
    *              *-MinDue = 1999/08/10<br>
    *          *-Details<br>
    *
    * the invokation would look like:<br>
    *
    *  String balance = FullDocumentHandler.getAttribute ("Summary/Balance");<br>
    *  Another example: see the Test Document below.<br>
    *   *-root<br>
    *       *-parameter1<br>
    *           *-person ID="id1"<br>
    *               *-firstname = Darren<br>
    *               *-middlename = Michael<br>
    *               *-lastName = Broemmer<br>
    *       *-parameter2<br>
    *           *-person ID="id2"<br>
    *               *-firstname = Mehreen<br>
    *               *-middlename = A<br>
    *               *-lastName = Nanjiani<br>
    *               *-cars = sentra<br>
    *               *-cars = civic<br>
    *      
    * If the attribute does not exist, return a null pointer exception. e.g.
    * FullDocumentHandler.getAttribute ("Summary/Balance");<br>
    * If an incorrect id is provided in the path, return null e.g.
    * FullDocumentHandler.getAttribute ("/parameter2/person(id55)/middlename");<br>
    * If no id is provided when there is an id present in the document in the
    * path, return null. e.g.
    * FullDocumentHandler.getAttribute ("/parameter1/person");<br>
    *
    * @param  inputPath The document path of an attribute to match.
    * @return The String value of the attribute.  
    */
    public String getAttribute (String xpath)
    {
        Node node = getDocumentNode (xpath);
        if (node == null)
        {
            return null;
        }
        return getNodeText (node);
    }
    /**
     * Get the value of a specified attribute name as a BigDecimal DataType.
     *

     * @param  attributePath (i.e. first_name or /Account/account_id)
     * @return The value of the attribute requested
     * @exception AmsException Indicates attribute is not found.
     */
    public BigDecimal getAttributeDecimal (String attributePath)
           throws AmsException
    {
        BigDecimal value = null;
        try
        {	
        	 value = new BigDecimal (this.getAttribute (attributePath));
        }
        catch (NumberFormatException ex)
        {
            throw new AmsException (
                "The attribute: " + attributePath +
                " cannot be converted to a BigDecimal DataType", ex);
        }
        // Return the value
        return value;
    }
    /**
     * Get the value of a specified attribute name as an Integer DataType.
     *

     * @param attributePath (i.e. first_name or /Account/account_id)
     * @return The value of the attribute requested
     */
    public int getAttributeInt (String attributePath) throws AmsException
    {
        int value = 0;
        try
        {
            value = Integer.parseInt(this.getAttribute (attributePath));
        }
        catch (NumberFormatException ex)
        {
            throw new AmsException (
                "The attribute: " + attributePath +
                " cannot be converted to a long DataType", ex);
        }
        // Return the value
        return value;
    }
    /**
     * Get the value of a specified attribute name as a Long DataType.
     *

     * @param   String attributePath (i.e. first_name or /Account/account_id)
     * @return  The value of the attribute requested
     * @exception AmsException Indicates attribute is not found.
     */
    public long getAttributeLong (String attributePath) throws AmsException
    {
        Long value = null;
        try
        {
            value = Long.valueOf(this.getAttribute (attributePath));
        }
        catch(NumberFormatException ex)
        {
            throw new AmsException (
                "The attribute: " + attributePath +
                " cannot be converted to a long DataType", ex);
        }
        // Return the value
        return value;
    }
   /**  
    * This method gets a vector of String attributes that match the specified 
    * document path. Example, see the Test Document below:<br>
    *   *-root<br>
    * &nbsp;*-parameter1<br>
    * &nbsp;&nbsp;*-person ID="id1"<br>
    * &nbsp;&nbsp;&nbsp;*-firstname = Darren<br>
    * &nbsp;&nbsp;&nbsp;*-middlename = Michael<br>
    * &nbsp;&nbsp;&nbsp;*-lastName = Broemmer<br>
    * &nbsp;*-parameter2<br>
    * &nbsp;&nbsp;*-person ID="id2"<br>
    * &nbsp;&nbsp;&nbsp;*-firstname = Mehreen<br>
    * &nbsp;&nbsp;&nbsp;*-middlename = A<br>
    * &nbsp;&nbsp;&nbsp;*-lastName = Nanjiani<br>
    * &nbsp;&nbsp;&nbsp;*-cars = sentra<br>
    * &nbsp;&nbsp;&nbsp;*-cars = civic<br>
    *      
    * If the attribute does not exist, return a null pointer exception. e.g.
    * FullDocumentHandler.getAttributes("Summary/Balance");<br>
    * If an incorrect id is provided in the path, throw a null pointer
    * exception e.g.
    * FullDocumentHandler.getAttributes("/parameter2/person(id55)/middlename");<br>
    * If no id is provided when there is an id present in the document in the
    * path, return nothing if more than one node exists e.g.
    * FullDocumentHandler.getAttributes("/parameter1/person"); otherwise if one
    * node exists return that node's attribute value;<br>
    * If an id is provided when one does not exist in the path, throw a "Could
    * not find id" exception. e.g.
    * FullDocumentHandler.getAttributes("/parameter1(id11)/person(id1)/firstname");
    * <br>
    * If an id is provided at the end of the path, throw a "Null pointer"
    * exception e.g.
    * FullDocumentHandler.getAttributes("/parameter2/person(id2)/middlename(id5)");
    * <br>
    * If more than one attribute exists, return all the values in a vector e.g.
    * FullDocumentHandler.getAttributes("/parameter2/person(id2)/cars");
    *
    * @param inputPath The document path of the attributes to match.
    * @return Vector of the String values of the attributes. If the attribute
    * does not exist, return a null pointer exception.      
    */
    public Vector getAttributes (String xpath)
    {
        Node node = getDocumentNode (xpath);
        Vector resultVector = new Vector ();
        if (node == null)
        {
            return resultVector;
        }
        resultVector.addElement (getNodeText (node));
        Node nextNode = node.getNextSibling ();
        String tagName = node.getNodeName ();
        while (nextNode != null)
        {
            if (nextNode.getNodeName().equals (tagName))
            {
                resultVector.addElement (getNodeText (nextNode));
            }
            nextNode = nextNode.getNextSibling ();
        }
        return resultVector;
    }

   /**  
    * Parse the attrPath into an array of strings.  
    * Find the corresponding node at the attrPath in myself.
    * If the node is retrieved successfully, assign that node 
    * as the root of a new instance of FullDocumentHandler and return 
    * this new instance.<br>
    * Example: Below is a document<br>
    *   *-root<br>
    * &nbsp;*-parameter1<br>
    * &nbsp;&nbsp;*-person ID="id1"<br>
    * &nbsp;&nbsp;&nbsp;*-firstname = Darren<br>
    * &nbsp;&nbsp;&nbsp;*-middlename = Michael<br>
    * &nbsp;&nbsp;&nbsp;*-lastName = Broemmer<br>
    * &nbsp;*-parameter2<br>
    * &nbsp;&nbsp;*-person ID="id2"<br>
    * &nbsp;&nbsp;&nbsp;*-firstname = Mehreen<br>
    * &nbsp;&nbsp;&nbsp;*-middlename = A<br>
    * &nbsp;&nbsp;&nbsp;*-lastName = Nanjiani<br>
    * &nbsp;&nbsp;&nbsp;*-cars = sentra<br>
    * &nbsp;&nbsp;&nbsp;*-cars = civic<br>
    * getComponent (parameter/person(id1)) will return an instance with the
    * following structure:<br>
    *   *-root<br>
    * &nbsp;*-firstname = Darren<br>
    * &nbsp;*-middlename = Michael<br>
    * &nbsp;*-lastName = Broemmer<br>
    * @param path The document path of the component to match.
    * @return The new FullDocumentHandler instance found at the attrPath.  
    */
   public FullDocumentHandler getComponent (String path)
   {
       Node parentNode = getDocumentNode (path);
       if (parentNode == null)
       {
          return null;
       }
       FullDocumentHandler doc = new FullDocumentHandler ();
       doc.makeComponent (parentNode, doc.getRoot ());
       return doc; 
   }

   /**  
    * Parse the attrPath into an array of strings.  
    * Find the corresponding nodes at the attrPath in myself.
    * If the nodes are retrieved successfully, assign that nodes 
    * as the root of a new instances of FullDocumentHandler and return 
    * the vector of new FullDocumentHandler instances.<br>
    * Example: Below is a document<br>
    *   *-root<br>
    * &nbsp;*-parameter<br>
    * &nbsp;&nbsp;*-person ID="id1"<br>
    * &nbsp;&nbsp;&nbsp;*-firstname = Darren<br>
    * &nbsp;&nbsp;&nbsp;*-middlename = Michael<br>
    * &nbsp;&nbsp;&nbsp;*-lastName = Broemmer<br>
    * &nbsp;*-parameter<br>
    * &nbsp;&nbsp;*-person ID="id2"<br>
    * &nbsp;&nbsp;&nbsp;*-firstname = Mehreen<br>
    * &nbsp;&nbsp;&nbsp;*-middlename = A<br>
    * &nbsp;&nbsp;&nbsp;*-lastName = Nanjiani<br>
    * &nbsp;&nbsp;&nbsp;*-cars = sentra<br>
    * &nbsp;&nbsp;&nbsp;*-cars = civic<br>
    *  getComponents(parameter) will return an instances with the following 
    *  structures:<br>
    *   *-root<br>
    * &nbsp;*-person ID="id1"<br>
    * &nbsp;&nbsp;  *-firstname = Darren<br>
    * &nbsp;&nbsp;  *-middlename = Michael<br>
    * &nbsp;&nbsp;  *-lastName = Broemmer<br>
    *   *-root<br>
    * &nbsp;*-person ID="id2"<br>
    * &nbsp;&nbsp;  *-firstname = Mehreen<br>
    * &nbsp;&nbsp;  *-middlename = A<br>
    * &nbsp;&nbsp;  *-lastName = Nanjiani<br>
    * &nbsp;&nbsp;  *-cars = sentra<br>
    * &nbsp;&nbsp;  *-cars = civic<br>
    * @param attrPath  The document path of the components to match.
    * @return Vector of FullDocumentHandler instances found at attrPath.
    */
   public Vector getComponents (String path)
   {
       Vector docList = new Vector ();
       Node parentNode = getDocumentNode (path);
       if (parentNode == null)
       {
           return docList;
       }
       FullDocumentHandler doc = new FullDocumentHandler ();
       doc.makeComponent (parentNode, doc.getRoot ());
       docList.addElement (doc);
       Node nextNode = parentNode.getNextSibling ();
       String tagName = parentNode.getNodeName ();
       while (nextNode != null)
       {
           if (nextNode.getNodeName ().equals (tagName))
           {
               FullDocumentHandler tempDoc = new FullDocumentHandler ();
               tempDoc.makeComponent (nextNode, tempDoc.getRoot ());
               docList.addElement (tempDoc);
           }
           nextNode = nextNode.getNextSibling ();
       }
       return docList; 
   }

    /**
     * Merges the specified document into the current document at the specified
     * path. Given the current document:<p>
     * *-root<br>
     * &nbsp;*-person ID="id1"<br>
     * &nbsp;&nbsp;*-firstname = Darren<br>
     * <br>
     * and the document to merge:<p>
     * *-root<br>
     * &nbsp;*-person ID="id1"<br>
     * &nbsp;&nbsp;*-lastname = Broemmer<br>
     * &nbsp;*-person ID="id2"<br>
     * &nbsp;&nbsp;*-firstname = Bill<br>
     * <br>
     * the resulting document would be:<p>
     * *-root<br>
     * &nbsp;*-person ID="id1"<br>
     * &nbsp;&nbsp;*-firstname = Darren<br>
     * &nbsp;&nbsp;*-lastname = Broemmer<br>
     * &nbsp;*-person ID="id2"<br>
     * &nbsp;&nbsp;*-firstname = Bill<br>
     *
     * @param path Path within the current document to perform the merge at.
     * @param doc The document to merge into the current document at the
     * specified path.
     */
    public void mergeDocument (String path, FullDocumentHandler doc)
    {
        int    count = 0;
        String attribute = null,
               component = null,
               value = null;
        Vector components = doc.getComponentNames ("/");
        Hashtable attributes = doc.getAttributeNamesValues ("/");
        Enumeration enumer = attributes.keys ();

        while (enumer.hasMoreElements ())
        {
            attribute = (String)enumer.nextElement ();
            value = (String)attributes.get (attribute);
            setAttribute ((path + attribute), value);
        }
        for (count = 0;count < components.size ();count++)
        {
            component = (String)components.elementAt (count);
            mergeDocument (
                path + component + "/",
                doc.getComponent ("/" + component + "/"));
        }
    }

    /**
     * Creates a list of all the components under the specified path's XML
     * node. We define components as leaves without values or non-leaves.
     *
     * @param path The path within this document to begin the search for
     * components from.
     * @return A vector of strings, where each string is a component name.
     */
    public Vector getComponentNames (String path)
    {
        Vector nameList = new Vector ();
        Node   parentNode = getDocumentNode (path);
        long   numComp = 0;

        if (parentNode == null)
        {
            return nameList;
        }
        NodeList children = parentNode.getChildNodes ();
        if (children != null)
        {
            for (int i = 0;i < children.getLength ();i++)
            {
                Node childNode = children.item (i);
                // Do our best to identify the node's use (jPylon business
                // object or an attribute) as we only want the business objects.
                // We are saying if I have multiple child nodes (there is
                // always at least one) or my child node has no value then I
                // am a business object.
                if ((childNode.getChildNodes ().getLength () > 1) ||
                    ((childNode.hasChildNodes ()) &&
                    (childNode.getFirstChild ().getNodeValue ().length () == 0)))
                {

                    Element e = (Element)childNode;
                    String idAttr = e.getAttribute ("ID");
                    if ((idAttr != null) && (idAttr.trim ().length () > 0))
                    {
                       nameList.addElement (
                          childNode.getNodeName () + "(" + idAttr + ")");
                    }
                    else
                    {
                       nameList.addElement (childNode.getNodeName ());
                    }
                }
            }
        }
        return nameList;
    }

    /**
     * Creates a list of all the attributes under the specified path's XML
     * node. We define attributes as leaves.
     *
     * @param path The path within this document to begin the search for
     * attributes from.
     * @return A vector of strings, where each string is an attribute name.
     */
    public Vector getAttributeNames (String path)
    {
        Vector nameList = new Vector ();
        Node parentNode = getDocumentNode (path);

        if (parentNode == null)
        {
            return nameList;
        }
        NodeList children = parentNode.getChildNodes ();
        if (children != null)
        {
            for (int i = 0;i < children.getLength ();i++)
            {
                Node childNode = children.item (i);
                // Do our best to identify the node's use (jPylon business
                // object or an attribute) as we only want the business objects.
                // We are saying if I have one child node and it has a value,
                // then I am an attribute.
                if ((childNode.getChildNodes ().getLength () == 1) &&
                    (childNode.getFirstChild ().getNodeValue ().length () > 0))
                {
                    nameList.addElement (childNode.getNodeName ());
                }
            }
        }
        return nameList;
    }

    /**
     * Creates a list of all the attributes under the specified path's XML
     * node and their values. We define attributes as leaves.
     *
     * @param path The path within this document to begin the search for
     * attributes from.
     * @return A hashtable where the key is an attribute name and the result is
     * the attribute value.
     */
    public Hashtable getAttributeNamesValues (String path)
    {
        Hashtable namesValues = new Hashtable ();
        Node parentNode = getDocumentNode (path);

        if (parentNode == null)
        {
            return namesValues;
        }
        NodeList children = parentNode.getChildNodes ();
        if (children != null)
        {
            for (int i = 0;i < children.getLength ();i++)
            {
                Node childNode = children.item (i);
                // Do our best to identify the node's use (jPylon business
                // object or an attribute) as we only want the business objects.
                // We are saying if I have one child node and it has a value,
                // then I am an attribute.
                if ((childNode.getChildNodes ().getLength () == 1) &&
                    (childNode.getFirstChild ().getNodeValue ().length () > 0))
                {
                    namesValues.put (
                        childNode.getNodeName (),
                        childNode.getFirstChild ().getNodeValue ());
                }
            }
        }
        return namesValues;
    }
    /**
     * Determines the current XML DOM node being pointed to by the current
     * thread name.
     *
     * @return The current node for the current thread name.
     */
    private Node getCurrNode ()
    {
        if (currNodeHT == null)
            currNodeHT = new Hashtable ();
        Node returnNode =
            (Node)currNodeHT.get (Thread.currentThread ().getName ());
        if (returnNode == null) 
        {   
            returnNode = root;
        }
        return returnNode;
    }

    /**  
     * @return My document instance variable.
     */   
    public Document getDocument ()
    {
        return document;
    }
    /**
     * Retrieve the Node object within the XML DOM at the given path
     * Do not create the path if it does not exist, return null in this case.
     *

     * @param xpath The xpath string to search for (ex. /Customer/first_name)
     */  
    public Node getDocumentNode (String xpath)
    {
        return (getDocumentNode (xpath, false));    
    }
    /**
     * Retrieve the Node object within the XML DOM at the given path
     * If the createPath indicator is true, create the path if it does not
     * exist. If the createPath indicator is false, return null if the path is
     * not found. An important "side effect" to note is that the currNode class
     * variable is modified by this method to point to the xpath specified.
     *

     * @param xpath The xpath string to search for (ex. /Customer/first_name)
     * @param createPath Indicates whether to create a path if not found
     */  
    public Node getDocumentNode (String xpath, boolean createPath)
    {
        if (document == null)
        {
            return null;
        }
        Node locateNode = null;
        if (xpath.startsWith ("/"))
        {
            locateNode = root;
        }
        else
        {
            locateNode = getCurrNode();
        }
        StringTokenizer parser = new StringTokenizer (xpath, "/");
        while (parser.hasMoreTokens ())
        {
            String token = parser.nextToken ();
            String tagName = getPathTagName (token);
            String id = getPathId (token);
            if (tagName.equals (".."))
            {
                locateNode = locateNode.getParentNode ();
            }
            else
            {
                if (tagName.equals ("."))
                {
                   // do nothing
                }
                else
                {
                    Node tempNode = getNode (locateNode, tagName, id);
                    if (tempNode == null)
                    {
                        // If lookup, return null
                        if (!createPath)
                        {
                          return null;
                        }
                        // If set attribute, create tag
                        Element newElement = makeElement (tagName, id, "");
                        locateNode.appendChild (newElement);
                        tempNode = newElement;
                    }
                    locateNode = tempNode;
                }
            }
        }
        setCurrNode (locateNode);
        return getCurrNode();
    }
    /**
     * Given an xpath, return a FullDocumentHandler that represents a fragment
     * of the existing document
     *

     * @param path The document path of the component to match.
     */
    public FullDocumentHandler getFragment (String path)
    {
        Node node = getDocumentNode (path);
        if (node == null)
        {
            return null;
        }
        FullDocumentHandler doc = new FullDocumentHandler (document, node);
        return doc;
    }
    /**
     * Given an xpath, return a vector of FullDocumentHandlers that represents
     * fragments of the existing document
     *

     * @param path The document path of the component to match.
     */
    public Vector getFragments (String path)
    {
        Vector docList = new Vector ();
        Node parentNode = getDocumentNode (path);
        if (parentNode == null)
        {
            return docList;
        }
        FullDocumentHandler doc = new FullDocumentHandler (document, parentNode);
        docList.addElement (doc);
        Node nextNode = parentNode.getNextSibling ();
        String tagName = parentNode.getNodeName ();
        while (nextNode != null)
        {
            if (nextNode.getNodeName ().equals (tagName))
            {
                FullDocumentHandler tempDoc =
                    new FullDocumentHandler (document, nextNode);
                docList.addElement (tempDoc);
            }
            nextNode = nextNode.getNextSibling ();
        }
        return docList;
    }
    /**
     * Retrieve the Node object with the given name, looking only at children
     * of the specified locateNode
     *

     * @param locateNode the starting point for the search
     * @param nodeNode the tag name to search for
     */    
    public Node getNode (Node locateNode, String nodeName)
    {
        return getNode (locateNode, nodeName, null);
    }
    /**
     * Retrieve the Node object with the given name, looking only at children
     * of the specified locateNode, and looking for the specified ID
     *

     * @param locateNode the starting point for the search
     * @param nodeName the tag name to search for
     * @param id the ID value to search for
     * @return Node the Node if found, else null
     */ 
    public Node getNode (Node locateNode, String nodeName, String id)
    {
        
        NodeList nodeList = locateNode.getChildNodes ();
        for (int i = 0; i < nodeList.getLength (); i++)
        {
            Node tempNode = nodeList.item (i);
            String name = tempNode.getNodeName ();
            short nodeType = tempNode.getNodeType ();
            if (name.equals (nodeName))
            {
                Element e = (Element)tempNode;
                if (id == null)
                {
                    return tempNode;
                }
                String idAttr = e.getAttribute ("ID");
                if (idAttr != null)
                {
                    if (idAttr.equals (id))
                    {
                        return tempNode;
                    }
                }
            }
        }
        return null;
    }
    /**
     * Get the text of a given Element Node
     *

     * @param node The Element node to modify
     * @return The text value of the Element
     */
    private String getNodeText (Node node)
    {
        StringBuilder buffer = new StringBuilder();
        NodeList nl = node.getChildNodes ();
        for (int loop = 0; loop < nl.getLength (); loop++)
        {
            Node tempNode = nl.item (loop);
            if (tempNode.getNodeType () == Node.TEXT_NODE)
            {
                buffer.append (tempNode.getNodeValue ());
            }
        }
        return buffer.toString ();
    }
    /**
     * Given a path token, return the ID value if there is one, else return
     * null.
     *

     * @param xpathToken A token in the XPath string
     * @return The ID value, or null if it does not exist
     */ 
    private String getPathId (String xpathToken)
    {
        int startIndex = xpathToken.indexOf ('(');
        int endIndex = xpathToken.indexOf (')');
        if (startIndex != -1)
        {
            xpathToken = xpathToken.substring (startIndex + 1, endIndex);
            return xpathToken;
        }
        return null;
    }
   //
   //
   // Private Helper methods
   //
   //
   
    /**
     * Given a path token, return the tag name
     *

     * @param xpathToken a token in the XPath string
     * @return The tag name
     */  
    private String getPathTagName (String xpathToken)
    {
        int index = xpathToken.indexOf ('(');
        if (index != -1)
        {
            xpathToken = xpathToken.substring (0, index);
        }
        return xpathToken;
    }
   /**  
    *  @return My root element.
    */   
    public Element getRoot ()
    {        
        return root;
    }
   /**  
    * This method returns the value of an XML attribute at the given path
    * (i.e. element).
    *
    * @param xpath The document path of the attributes to match.
    * @param attributeName The name of the XML attribute.
    * @return The value of the XML attribute.
    */
    public String getXmlAttribute (String xpath, String attributeName)
    {
        Node node = getDocumentNode (xpath);
        if (node == null)
        {
            return "";
        }
        short nodeType = node.getNodeType ();
        if (nodeType == Node.ELEMENT_NODE)
        {
            Element elem = (Element)node;
            return elem.getAttribute (attributeName);
        }
        return "";
    }
    /**
     * Make a new component from an existing one. This method copies all of the
     * children of the fromNode to the toNode.
     *

     * @param fromNode A DOM node to copy
     * @param toNode What Node to copy to in this document
     * @return void
     */
    private void makeComponent (Node fromNode, Node toNode)
    {
        // copy all children recursively
        NodeList children = fromNode.getChildNodes ();
        if (children != null)
        {
            for (int i = 0; i < children.getLength (); i++)
            {
                Node oldChildNode = children.item (i);
                if (oldChildNode.getNodeType () == Node.ELEMENT_NODE)
                {
                  Node newChildNode = copyNode (oldChildNode);
                  toNode.appendChild (newChildNode);
                  this.makeComponent (oldChildNode, newChildNode);
                }
            }
        }
    }
    /**  
     *  This method creates an XML element.
     *
     *  @param tagName The name of the element
     *  @param attId The value of the ID attribute
     *  @param text The text value of the element
     *  @return Newly created XML element for this document
     */
    public Element makeElement (String tagName, String attId, String text)
    {
        Element newElement = document.createElement (tagName);
        if (attId != null)
        {
            if (!(attId.isEmpty()))
            {
                newElement.setAttribute ("ID", attId);
            }
        }
        Text textNode = document.createTextNode (text);
        newElement.appendChild (textNode);
        return newElement;
    }

    /**  
     * Read in the Document instance and return a string format of the 
     * XML file.  NOTE: This method is deprecated.
     * @return The XML string.
     */          
    public String printString ()
    {
        return this.toString ();
    }

    /**
     * Given a document path, remove all of its children
     *

     * @param node the Node to remove all children from
     */
    public void removeAllChildren (String path)
    {
        Node node = getDocumentNode (path);
        if (node == null)
        {
            return;
        }
        this.removeAllChildren (node);
    }
    /**
     * Given a node, remove all of its children
     *

     * @param node The Node to remove all children from
     */
    public void removeAllChildren (Node node)
    {
        // Remove all current children
        NodeList children = node.getChildNodes ();
        if (children != null)
        {
            int len = children.getLength ();
            for (int i = 0; i < len; i++)
            {
                Node tempNode = children.item (i);
                if (tempNode.getNodeType () == Node.ELEMENT_NODE)
                {
                    node.removeChild (tempNode);
                    i--;
                    len--;
                }
            }
        }
    }
    /**
     * Remove the specified component.
     *

     * @param path The xml path
     */    
    public void removeComponent (String path)
    {
        Node node = getDocumentNode (path);
        if (node == null)
        {
            return;
        }
        Node parentNode = node.getParentNode ();
        parentNode.removeChild (node);
    }
   /**  
    *  This method sets the value of the attribute at the given path.
    *  If the attribute does not exist, then create it.
    *
    *  @param xpath The document path of the attribute to match.
    *  @param value String value of the attribute.
    */ 
    public void setAttribute (String xpath, String value)
    {
        Node node = getDocumentNode (xpath, true);
        this.setNodeText (node, value);
    }
    /**
     * Set the value of a specified attribute name with a BigDecimal DataType.
     *

     * @param  attributePath (i.e. first_name or /Account/account_id)
     */
    public void setAttributeDecimal (String attributePath, BigDecimal value)
    {
        this.setAttribute (attributePath, value.toString ());
    }
    /**
     * Set the value of a specified attribute name with a Long DataType.
     *

     * @param attributePath (i.e. first_name or /Account/account_id)
     */
    public void setAttributeInt (String attributePath, int value)
    {
        this.setAttribute (attributePath, Integer.toString(value));
    }
    /**
     * Set the value of a specified attribute name with a Long DataType.
     *

     * @param attributePath (i.e. first_name or /Account/account_id)
     */
    public void setAttributeLong (String attributePath, long value)
    {
        Long longValue = value;
        this.setAttribute (attributePath, longValue.toString ());
    }
   /**  
    * This method sets the values of the attributes at the given path.
    * If the attribute does not exist, then create it.
    *
    * @param xpath The document path of the attributes to match.
    * @param values Vector of String values of the attributes.
    */  
    public void setAttributes (String xpath, Vector values)
    {
        if (values == null)
            return;
        int numValues = values.size ();
        if (numValues == 0)
            return;
        Node node = getDocumentNode (xpath, true);
        this.setNodeText (node, (String) values.elementAt (0));
        Node parentNode = node.getParentNode ();
        for (int loop = 1; loop < numValues; loop++)
        {
            parentNode.appendChild (
                this.makeElement (
                    node.getNodeName (), "", (String)values.elementAt (loop)));
        }
    }
   /**  
    * Insert the document at the specified path, removing what was there.
    *
    * @param path The document path of the component to match.
    * @param doc FullDocumentHandler instance to be set at the path.
    */
   public void setComponent (String path, FullDocumentHandler doc)
   {
       Node parentNode = getDocumentNode (path, true);
       this.removeAllChildren (parentNode);
       this.makeComponent (doc.getRoot (), parentNode);
   }   
   /**  
    * Insert the vector of documents at the specified path, removing what was
    * there.
    *
    * @param path The document path of the components
    * @param docList Vector of FullDocumentHandler instances to be set at the path.
    */ 
   public void setComponents (String path, Vector docList)
   {
       Node parentNode = getDocumentNode (path, true);
       this.removeAllChildren (parentNode);
       for (int loop = 0; loop < docList.size(); loop++)
       {
          FullDocumentHandler tempDoc = (FullDocumentHandler)docList.elementAt (loop);
          this.makeComponent (tempDoc.getRoot (), parentNode);
       }
   }   

   /**
    * Sets the current node pointer for this thread.
    *
    * @param nodePointer The node to set the current node pointer to.
    */
    private void setCurrNode (Node nodePointer)
    {
        if (currNodeHT == null)
            currNodeHT = new Hashtable ();
        currNodeHT.put (Thread.currentThread ().getName (), nodePointer);
    }

   /**  
    * @param doc The instance of Document class to be set as my document
    * instance variable.
    */  
    public void setDocument (Document doc)
    {
        document = doc;
    }
    /**
     * Set the text of a given Element Node
     *

     * @param node The Element node to modify
     * @param text The text value to give the Element
     */
    private void setNodeText (Node node, String text)
    {
        Node firstChild = node.getFirstChild ();
        if (firstChild == null)
        {
            node.appendChild (document.createTextNode (text));
            return;
        }
        firstChild.setNodeValue (text);
    }
   /**  
    * @param e The instance of Element class to be set as my root.
    * @return Void
    */ 
    public void setRoot(Element e)
    {
        root = e;
    }
   /**  
    * This method creates an XML attribute at the given path (i.e. element).
    *
    * @param xpath The document path of the attributes to match.
    * @param attributeName The name of the XML attribute.
    * @param value The value of the XML attribute.
    */  
    public void setXmlAttribute (String xpath,
                                 String attributeName,
                                 String value)
    {
        Node node = getDocumentNode (xpath, true);
        short nodeType = node.getNodeType ();
        if (nodeType == Node.ELEMENT_NODE)
        {
            Element element = (Element)node;
            element.setAttribute (attributeName, value);
        }
    }
   
}
