package com.amsinc.ecsg.util;

import java.util.*;

/**
 *  This class represents a thread that saves a list
 *  of performance timer objects to the database.
 *   
 *     Copyright  � 2003                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class PerformanceTimerSaver implements Runnable
 {  
   // Timers that this thread will save to the database
   private Collection timersToBeSaved;
   
   /**
    * Constructor for this class.   Accepts a list of timers
    * to be saved.  Once the thread is started, these timers
    * will be saved to the database.
    *
    * @param timersToBeSaved - Collection - timers to be saved.
    */
   public PerformanceTimerSaver(Collection timersToBeSaved)
    {
      this.timersToBeSaved = timersToBeSaved;   
    }
   
   /**
    * This method is called when the thread represented by this class
    * is started.   It iterates through all timers in the list, and then
    * saves each one to the database.
    */
   public void run()
    {
      Iterator iter = timersToBeSaved.iterator();
      
      // Iterate through all of the timers
      while(iter.hasNext())
       {
         PerformanceTimer timer = (PerformanceTimer) iter.next();
         
         try
          {
             timer.save();   
          }
         catch(Exception e)
          {  
             // Do nothing - could not save stat
          }
        }
    }  
}
