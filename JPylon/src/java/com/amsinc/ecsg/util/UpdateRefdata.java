package com.amsinc.ecsg.util;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import com.amsinc.ecsg.frame.*;

/*
 * UpdateRefdata
 *
 *  This class is used to update the REFDATA table and the corresponding
 *  reference data information that is cached in memory.  
 *
 *     Copyright  � 2002                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
public class UpdateRefdata
 {
	private static final Logger LOG = LoggerFactory.getLogger(UpdateRefdata.class);
    // Get all supported locales as specified in the SupportedLocales.xml file
    // The hashtable has the locale logical name as its key and
    // a Locale object as its data
    private Hashtable supportedLocales = ResourceManager.getSupportedLocales();
          
    // Used to hold all languages used in the supported locales
    private Vector languages = new Vector(10);
          
    // All language / country combination used in supported locales
    private Vector languageAndCountryCombinations = new Vector(15);
    
    // The refdata manager class
    private ReferenceDataManager refMgr = ReferenceDataManager.getRefDataMgr();

    // The values passed in from the file
    private String mode;
    private String tableType;
    private String code;
    private String addlValue;
    private String defaultDescr;

    // The locale-specific descriptions passed in from the file
    private Hashtable localeSpecificDescr = new Hashtable();

    // Mapping of which descriptions will be used for each locale
    // (This includes any defaulting)
    private Hashtable descrForLocale;

    // Flag indicating whether or not the database will be updated
    // When updating refdata without restarting the servers,
    // the update method must be run against all app server instances
    // However, the database must only be updated once.
    private boolean updateDatabase;

    /**
     * Main driver method for parsing the contents of a file and 
     * updating the database and current memory according to the
     * contents.   See called methods for more details.
     *
     * @param fileContents - contents of file to be parsed
     * @param updatingDatabase - indicates whether or not to update the database
     * @return String - result message
     */
    public String update(String fileContents, boolean updatingDatabase)
     {
        LOG.info("Updating cached refdata");

        // Determine what needs to be done from the file
        String errors = readParametersFromFile(fileContents); 

        // Errors in the file are returned to the user
        if(errors != null)
          return errors;

        // Set the flag from what is passed in
        updateDatabase = updatingDatabase;        

        // Get information about the supported locales
        getLocaleInformation();

        // For each locale, determine the locale that should be 
        // used.  This is where the locale defaulting is done
        determineDescrForLocales();

        // Perform the action requested in the data passed in
        if(mode.equalsIgnoreCase("Delete"))
         {
             processDelete();
         }
        else if(mode.equalsIgnoreCase("Insert"))
         {
             processInsert();
         }
        else if(mode.equalsIgnoreCase("Update"))
         {
             processUpdate();
         }
        else
         {
             return "Invalid mode specified: must be Insert, Delete, or Update";
         }
       
       return "Success";
     }

     /**
      * Generate SQL and update memory for updating a refdata code
      */
     private void processUpdate()
      {
             // Build an update statement and call refMgr.updateCode for all locales
             // This guarantees that data for all locales will be updated
             Enumeration localeNames = descrForLocale.keys();
     
             while(localeNames.hasMoreElements())
              {
                     String localeName = (String) localeNames.nextElement();

                     // Get the description for this locale
                     String descrForThisLocale = (String) descrForLocale.get(localeName);

	             // Update the reference data description and additional value in memory
	             if(!localeName.equals("NULL"))
			refMgr.updateCode(tableType, code, descrForThisLocale, addlValue, localeName);
	             else
			refMgr.updateCode(tableType, code, descrForThisLocale, addlValue, "");

                     if(updateDatabase)
                      {
                    	 List<Object> sqlParams = new ArrayList<>();
	                  StringBuilder sql = new StringBuilder();
                          sql.append("update refdata set ");         
	                  sql.append("descr = ?");
                          sql.append(", addl_value = ?");
                          sql.append(" where table_type =? ");
	                  sql.append(" and code =? ");                     
	                  sqlParams.add(descrForThisLocale);
	                  sqlParams.add(addlValue);
	                  sqlParams.add(tableType);
	                  sqlParams.add(code);
                          if(!localeName.equals("NULL"))
                           {
  	                     sql.append(" and locale_name =? ");  
	                     sqlParams.add(localeName);
                           }
                          else
                           {
  	                     sql.append(" and locale_name is null");  
                           }

                          try
                           {
                              // Update the reference data in the datbase
                              DatabaseQueryBean.executeUpdate(sql.toString(), false, sqlParams);
                           }
                          catch(Exception e)
                           {
                               LOG.error("Exception occured in processUpdate",e);
                           }
                       }
              }
       }

     /**
      * Generate SQL and update memory for deleting a refdata code
      */
     private void processDelete()
      {
             // Remove the code from memory
             refMgr.removeCode(tableType, code);

             if(updateDatabase)
              {
                 // Build SQL to delete
                 String sql = "delete from refdata where table_type =  ? and code = ? ";

                 try
                  {
   	             DatabaseQueryBean.executeUpdate(sql, false, tableType, code);
                  }
                 catch(Exception e)
                  {
                	 LOG.error("Exception occured in processDelete",e);
                  }
               }
       }

     /**
      * Generate SQL and update memory for inserting a refdata code
      */
     private void processInsert()
      {
             // Build an insert statement and call refMgr.updateCode for all locales
             // This guarantees that data for all locales will be updated
             Enumeration localeNames = descrForLocale.keys();

             while(localeNames.hasMoreElements())
              {
                     String localeName = (String) localeNames.nextElement();

                     // Get the description for this locale
                     String descrForThisLocale = (String) descrForLocale.get(localeName);

                     // Update the reference data description and additional value in memory
                     if(!localeName.equals("NULL"))
		         refMgr.updateCode(tableType, code, descrForThisLocale, addlValue, localeName);
                     else
		         refMgr.updateCode(tableType, code, descrForThisLocale, addlValue, "");

	             if(updateDatabase)
                      {
	            	 List<Object> sqlParams = new ArrayList<>();
 	                 StringBuilder sql = new StringBuilder();
	                 sql.append("insert into refdata (table_type, code, descr, addl_value, locale_name) values (?, ?, ?, ? ");
	                 sqlParams.add(tableType);
	                 sqlParams.add(code);
	                 sqlParams.add(descrForThisLocale);
	                 sqlParams.add(addlValue);
                         if(!localeName.equals("NULL"))
                          {
  	                    sql.append(",?)");  
	                    sqlParams.add(localeName);
                          }
                         else
                          {
  	                    sql.append(", NULL)");  
                          }

                         try
                          {
	 	    	      DatabaseQueryBean.executeUpdate(sql.toString(), false, sqlParams);
                          }
                         catch(Exception e)
                          {
                        	 LOG.error("Exception occured in processInsert",e);
                          }
                       }
              }
      }

     /**
      * Analyze the supported locale information stored in ResourceManager
      * to determine the list of language/country combinations and also
      * the list of countries used.  This is used to properly default the
      * descriptions.
      */
     private void getLocaleInformation()
      {
          Enumeration enumer;          
          
          // Build list of the languages that are used in the supported locales
          // The two character code for the language will be added to the hashtable
          enumer = supportedLocales.elements();
          while(enumer.hasMoreElements())
           {
             Locale theLocale = (Locale) enumer.nextElement(); 
             // If the locale's language isn't already in the list, add it
             if(!languages.contains(theLocale.getLanguage()))
                 languages.addElement(theLocale.getLanguage());
           }
          
          // Build list of language / country combinations that are used in the supported locales
          // A string containing both the language and the country will be added
          // to the hashtable (i.e. en_US)
          enumer = supportedLocales.elements();
          while(enumer.hasMoreElements())
           {
             Locale theLocale = (Locale) enumer.nextElement(); 
             
             // If no country is specified, we don't need to go any further
             if( (theLocale.getCountry() == null) ||
                 theLocale.getCountry().equals("") )
               continue;
             
             // Create the string containing both language and country
             String languageCountry = theLocale.getLanguage() + "_" + theLocale.getCountry();
             
             // If the locale's language/country isn't already in the list, add it             
             if(!languageAndCountryCombinations.contains(languageCountry))
                 languageAndCountryCombinations.addElement(languageCountry);
           }   
      }


     /**
      * Determines which description will be used for each supported locale.
      * There must be a description in the database for each locale, but there
      * does not need to be a description specified in the input file for each
      * locale.  Descriptions are defaulted according to the locale or part of
      * the locale that is specified.  For example, if "fr" (indicating the French
      * language) is included in the input file, its description must be applied 
      * to all supported locales using the French language.   If "fr_FR" (indicating
      * France French) is included, its description will only be applied to the
      * fr_FR locale.  The more specific the locale information in the file will 
      * override the more generic.
      * 
      */
     private void determineDescrForLocales()
      {
          // Hashtable to populate with the data
          descrForLocale = new Hashtable();     

          Enumeration allLocales = supportedLocales.keys();
             
          // First, populate the description for the null locale with the default description
          descrForLocale.put("NULL", defaultDescr);


          // Second, populate data for all non-null locales with the default descr
          // This way, all locales start by being populated with the default description
          while(allLocales.hasMoreElements())
           {
                  String localeName = (String) allLocales.nextElement();
                  descrForLocale.put(localeName, defaultDescr);
           }


          // Third, handle the case when only a language is given.  Data for all
          // locales that use that language must be updated.

          // Loop through each language used by a supported locale
          Enumeration allLanguages = languages.elements();         
          while(allLanguages.hasMoreElements())
           {
             String language = (String) allLanguages.nextElement();

             // Loop through all supported locales to find the ones that
             // use this language
             Enumeration locales = supportedLocales.keys();

             while(locales.hasMoreElements())
              {        
               String localeLogicalName = (String) locales.nextElement();

               Locale theLocale = (Locale) supportedLocales.get(localeLogicalName);
               
               String localeLanguage = theLocale.getLanguage();
               String descrForLanguage = (String) localeSpecificDescr.get(localeLanguage);      

               // If this locale uses the language, and we have data for that language,
               // update the hashtable
               if(theLocale.getLanguage().equals(language) &&
                  (descrForLanguage != null) )
                {
                    descrForLocale.put(localeLogicalName, descrForLanguage);
                }
               }
            }
 

          // Fourth, handle the case when a language and country is given.  Data for all
          // supported locales that use that language must be updated.

          // Loop through each language and country combination used by a supported locale
          Enumeration allCombinations = languageAndCountryCombinations.elements();         
          while(allCombinations.hasMoreElements())
           {
             String languageCountry = (String) allCombinations.nextElement();

             // Loop through all supported locales to find the ones that
             // use this language / country combination
             Enumeration locales = supportedLocales.keys();

             while(locales.hasMoreElements())
              {        
               String localeLogicalName = (String) locales.nextElement();

                Locale theLocale = (Locale) supportedLocales.get(localeLogicalName);
                String language = languageCountry.substring(0, 2);
                String country = languageCountry.substring(3,5);
                
                String descrForLanguageCountry = (String) localeSpecificDescr.get(languageCountry);

                // If the suppored locale uses the language and country, and
                // we have data for it, add it             
                if( theLocale.getLanguage().equals(language) &&
                    theLocale.getCountry().equals(country)   &&
                    (localeSpecificDescr.get(languageCountry) != null) )
                 {
                   descrForLocale.put(localeLogicalName, descrForLanguageCountry);                  
                 }
               }
            }
      }

     /**
      * Parses through the data from a file to determine the parameters of the
      * reference data update.  An example file is:
      * #Mode: Insert, Delete, Update
      * Mode=Update
      * 
      * # The table type
      * # Required for all modes
      * TableType=CURRENCY_CODE
      * 
      * # The code value for the refdata being updated
      * # Required for all modes
      * Code=DONUT
      * 
      * AddlValue=7
      * 
      * # Description to use for default and English locales
      * # Required for modes: Insert, UpdateDescr
      * Descr=Donuts
      * 
      * # Description to use for French users
      * # If no translation is available, prepend description with !!!ENGLISH:
      * #   For example: !!!ENGLISH:Accounts Receivable
      * # This will mark it as needing translation.  (see DOC-748 in the Project Workbook)
      * # If no French description is provided, the default description (above) will be used
      * Descr.fr=!!!!Frenchie French
      *  
      * # Description to use for French users
      * # If no translation is available, prepend description with !!!ENGLISH:
      * #   For example: !!!ENGLISH:Accounts Receivable
      * # This will mark it as needing translation.  (see DOC-748 in the Project Workbook)
      * # If no French description is provided, the default description (above) will be used
      * Descr.en_CA=!!!Donuts, eh?
      * 
      * @param fileContents - the contents of the file to be parsed
      * @return String - result message
      */
     private String readParametersFromFile(String fileContents)
      {
       try
        {
           ByteArrayInputStream bais = new ByteArrayInputStream(fileContents.getBytes() );

           PropertyResourceBundle changeFile = new PropertyResourceBundle(bais);

           mode = changeFile.getString("Mode");
           tableType = changeFile.getString("TableType");
           code = changeFile.getString("Code");
           try
            {
               addlValue = changeFile.getString("AddlValue");
            }
           catch(MissingResourceException mre)
            {
               addlValue = "";
            } 

           defaultDescr = changeFile.getString("Descr");

           if(mode.equals(""))
            {
               return "Mode cannot be blank";
            }

           if(code.equals(""))
            {
               return "Code cannot be blank";
            }

           if(tableType.equals(""))
            {
               return "Table Type cannot be blank";
            }

           Enumeration keys = changeFile.getKeys();
        
           while(keys.hasMoreElements())
            {
               String key = (String) keys.nextElement();
 
               if(key.startsWith("Descr."))
                {
                   int indexOfDot = key.indexOf('.');
                   String locale = key.substring(indexOfDot+1).trim();
                   localeSpecificDescr.put(locale, changeFile.getString(key));
                }
            } 
        }
       catch(Exception e)
        {         
          LOG.error("Exception occured in readParametersFromFile ", e);
          return "Exception occurred in readParametersFromFile: "+e.getMessage();
        }  

       // No errors, so return null
       return null;
      }


}

























