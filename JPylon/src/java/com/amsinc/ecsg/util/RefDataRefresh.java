/*
 * @(#)RefDataRefresh
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.util;

import com.amsinc.ecsg.frame.*;
import javax.naming.*;
import javax.ejb.*;
import java.rmi.*;

public class RefDataRefresh
{
 
 public static void main (String[] argv) throws RemoteException, RemoveException, AmsException
 {
  if (argv.length == 0) {
     System.out.println("Usage: java com.amsinc.ecsg.util.RefDataRefresh [serverLocation]");
     System.out.println("serverLocation is a required parameter. The server:port location of your EJB server");
  } else {
    String serverLocation = argv[0];
    ServerDeploy serverDeploy = (ServerDeploy)EJBObjectFactory.createClientEJB(serverLocation,
                                                                               "ServerDeploy");
                                                                               
    serverDeploy.reloadReferenceData();
    serverDeploy.remove();
  }
 }
}
