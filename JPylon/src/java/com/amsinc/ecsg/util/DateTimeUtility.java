/*
 * @(#)DateTimeUtility
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.util;

import com.amsinc.ecsg.frame.AmsException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.Timestamp;

public class DateTimeUtility{
    
    // This object has no constructor
    protected DateTimeUtility(){}

    public static String getGMTDateTime() throws AmsException{
        return getGMTDateTime(true, true);
    }
    
    public static String getGMTDateTime(boolean onServer) throws AmsException
     {
        return getGMTDateTime(onServer, true);
     }

    public static String getGMTDateTime(boolean onServer, boolean override) throws AmsException {

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));//BSL IR REUM041350118 04/18/2012 ADD

        Date gmtDate = GMTUtility.getGMTDateTime(onServer, override);

        return formatter.format(gmtDate);
    }
    
    /**
     * Returns the current system date in a string format of "mm-dd-yyyy". This
     * format is consistent with the Framework's DateAttribute format.
     *
     * @return String The Current Date in format "mm-dd-yyyy".

     */
    public static String getCurrentDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        return formatter.format(new Date());
    }
    
    /**
     * Returns the current system date/time in a string format of "mm-dd-yyyy". This
     * format is consistent with the Framework's DateAttribute format.
     *
     * @return String The Current Date in format "mm-dd-yyyy".

     */
    public static String getCurrentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return formatter.format(new Date());
    }
    
    /**
     * Returns the current system time in a string format of "HH:mm:ss". This
     * format is consistent with the Framework's DateAttribute format.
     *
     * @return String The Current time in format "HH:mm:ss".

     */
    public static String getCurrentTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        return formatter.format(new Date());
    }
    
    /**
     * Converts the supplied date object into the Framework's standard String date
     * format of "MM/dd/yyyy".
     *
     * @param Date A valid date object
     * @return String The date supplied in the dateObject Date argument formatted as
     * "MM/dd/yyyy".

     */
    public static String convertDateToDateString(Date dateObject){
        if (dateObject == null) return null;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        return formatter.format(dateObject);
    }
    
    /**
     * Converts the supplied date object into the Framework's standard String time
     * format of "HH:mm:ss".
     *
     * @param Date A valid date object
     * @return String The time supplied in the dateObject Date argument formatted as
     * "hh:mm:ss".

     */
    public static String convertDateToTimeString(Date dateObject){
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
        return formatter.format(dateObject);
    }
    
    /**
     * Converts the supplied date object into the Framework's standard String time
     * format of "MM/dd/yyyy HH:mm:ss".
     *
     * @param Date A valid date object
     * @return String The time supplied in the dateObject Date argument formatted as
     * "MM/dd/yyyy HH:mm:ss".

     */
    public static String convertDateToDateTimeString(Date dateObject){
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return formatter.format(dateObject);
    }

    /**
     * Converts passed in date object into GMT date time string
     * @param date
     * @return
     */
    public static String convertDateToGMTDateTimeString(Date date){
    	
    	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));//BSL IR REUM041350118 04/18/2012 ADD

        return formatter.format(date);	
    }
    
    /**
     * Converts passed in date object into GMT date time 
     * @param date
     * @return
     * @throws AmsException 
     */
    public static Date convertDateToGMTDateTime(Date date) throws AmsException{
    	
    	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));//BSL IR REUM041350118 04/18/2012 ADD
        String formattedDate = formatter.format(date);
        try {
			return formatter.parse(formattedDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			 throw new AmsException("Error converting date: " + formattedDate + " to java.util.Date object. Nested exception: " + e.getMessage());
		}	
    }
    /**
     * Converts the supplied date string (in the format of "MM/dd/yyyy") into a Date
     * object.
     *
     * @param String dateString The date String in the format of "MM/dd/yyyy"
     * @return Date the Date object which represents the date parsed from the supplied
     * dateString argument.
     *

     */
    public static Date convertStringDateToDate(String stringDate) throws AmsException{
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            return formatter.parse(stringDate);
        }catch(Exception e){
            throw new AmsException("Error converting date: " + stringDate + " to java.util.Date object. Nested exception: " + e.getMessage());
        }
    }
    
    /**
     * Converts the supplied DateTime string (in the format of "MM/dd/yyyy HH:mm:ss") 
     * into a Date object.
     *
     * @param String timeString The time String in the format of "MM/dd/yyyy HH:mm:ss"
     * @return Date the Date object which represents the date parsed from the supplied
     * timeString argument.
     *

     */
    public static Date convertStringDateTimeToDate(String stringTime) throws AmsException{
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            return formatter.parse(stringTime);
        }catch(Exception e){
            throw new AmsException("Error converting DateTime: " + stringTime + " to java.util.Date object. Nested exception: " + e.getMessage());
        }
    }
    
    /**
     * Converts the supplied DateTime String (in the format of "MM/dd/yyyy HH:mm:ss") 
     * into a Timestamp object.
     *
     * @param String timeString The time String in the format of "MM/dd/yyyy HH:mm:ss"
     * @return Timestamp the Timestamp object which represents the date parsed from the supplied
     * timeString argument.
     *

     */
    public static Timestamp convertStringDateTimeToTimestamp(String stringTime) throws AmsException{
        try{
            SimpleDateFormat timestampFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date dateValue = dateFormatter.parse(stringTime);
            return Timestamp.valueOf(timestampFormatter.format(dateValue));
        }catch(Exception e){
            throw new AmsException("Error converting DateTime: " + stringTime + " to java.sql.Timestamp object. Nested exception: " + e.getMessage());
        }
    }
    
    /**
     * Converts the supplied Date String (in the format of "MM/dd/yyyy") 
     * into a Timestamp object.
     *
     * @param String timeString The time String in the format of "MM/dd/yyyy"
     * @return Timestamp the Timestamp object which represents the date parsed from the supplied
     * timeString argument.
     *

     */
    public static Timestamp convertStringDateToTimestamp(String stringTime) throws AmsException{
        try{
            SimpleDateFormat timestampFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
            Date dateValue = dateFormatter.parse(stringTime);
            return Timestamp.valueOf(timestampFormatter.format(dateValue));
        }catch(Exception e){
            throw new AmsException("Error converting Date: " + stringTime + " to java.sql.Timestamp object. Nested exception: " + e.getMessage());
        }
    }
    
    /**
     * Converts a SQL Timestamp to the standard Framework Date string.  This
     * date String has the format of "MM/dd/yyyy".
     *
     * @param String TimestampString The SQL Timestamp string retrieved from the DBMS
     * @return String String object which represents the date in the format "MM/dd/yyyy"
     *

     */
    public static String convertTimestampToDateString(String TimestampString) throws AmsException{
        Date dateValue = Timestamp.valueOf(TimestampString);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        return formatter.format(dateValue);
    }

    /**
     * Converts a SQL Timestamp to the standard Framework Date string.  This
     * date String has the format of "MM/dd/yyyy".
     *
     * @param String TimestampString The SQL Timestamp string retrieved from the DBMS
     * @param String dateFormat format the date should be converted to
     * @return String String object which represents the date according to the dateFormat argument
     *

     */
    public static String convertTimestampToDateString(String TimestampString, String dateFormat) throws AmsException{
        Date dateValue = Timestamp.valueOf(TimestampString);
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(dateValue);
    }
    
    /**
     * Converts given date to given format
     *
     * @param Date aDate 
     * @param String dateFormat format the date should be converted to
     * @return String String object which represents the given date according to the dateFormat argument
     */
    public static String formatDate(Date aDate, String dateFormat) throws AmsException{
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(aDate);
    }
    
    /**
     * Converts a SQL Timestamp to the standard Framework DateTime string.  This
     * time String has the format of "MM/dd/yyyy HH:mm:ss".
     *
     * @param String TimestampString The SQL Timestamp string retrieved from the DBMS
     * @return String String object which represents the time in the format "MM/dd/yyyy hh:mm:ss"
     *

     */
    public static String convertTimestampToDateTimeString(String TimestampString) throws AmsException{
        Date dateValue = Timestamp.valueOf(TimestampString);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return formatter.format(dateValue);
    }
    
    /**
     * Returns the current system date in a string format that is specified by the user. 
     *
     * @return String The Current Date in format specified by user. If error, default to MM/dd/yyyy HH:mm:ss

     */
    public static String getCurrentDate(String format){
    	//default format
    	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    	try{
    		 formatter = new SimpleDateFormat(format);
    	}catch(Exception e){
    		System.out.println("Exception getting Current Date with user supplied format: "+e);
    	}
        return formatter.format(new Date());
    }
    
    
    /**
     * Returns the date in a string format after adding minutes specified by the user to the current time. 
     *
     * @return String Add minutes to Current Date. 

     */
    public static String addMinutesToGMTTime(int minutes) throws AmsException, ParseException{
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    	GregorianCalendar cal = (GregorianCalendar) GregorianCalendar.getInstance();
    	// Calendar cal = Calendar.getInstance();
    	cal.setTime(dateFormatter.parse(getGMTDateTime(true, false)));
    	cal.add(Calendar.MINUTE, minutes);
    	return dateFormatter.format(cal.getTime());

    }
    
    /**
     * convert date to simple date format
     * 
     * @param dateFormat string of the date
     * @return SimpleDateFormat date
     */   
    public static String getCurrentDateTime( final String dateFormat ) 
    {
        final Calendar cal = Calendar.getInstance();
        final SimpleDateFormat sdf = new SimpleDateFormat( dateFormat, new Locale( "en", "US" ) );

        return sdf.format( cal.getTime() );
    }
    
    
}