/*
 * @(#)LoggerCategoryInfo
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.logging;

import java.util.*;
import org.w3c.dom.*;
import org.apache.xerces.parsers.DOMParser;

/**
 * Class for storing information about the logger categories that have been
 * defined for use in the logging process. This includes the catgory's name
 * and the various severity/logical log file name relationship's defined for
 * the category. All the logger category information is read from the Logger.xml
 * file.
 */
public class LoggerCategoryInfo
{
   private String _categoryName;
   /** Hashtable where key/result is severity/logical_log_file_name */
   private Hashtable _entries;

   /**
    * Parses an XML node containing logger category information.
    *
    * @param node The XML node to be parsed.

    */
   public LoggerCategoryInfo (Node node)
   {
      int      count = 0;
      Node     subNode = null;
      NodeList nodeList = null;

      _entries = new Hashtable ();
      if (!node.hasChildNodes ())
      {
         return;
      }
      nodeList = node.getChildNodes ();
      for (count = 0;count < nodeList.getLength ();count++)
      {
         subNode = nodeList.item (count);
         if (subNode.getNodeName ().equals (LoggerServer.ELEMENT_CATEGORYNAME))
         {
            if ((subNode.getChildNodes () != null) &&
                (subNode.getChildNodes ().item (0) != null))
               _categoryName =
                  subNode.getChildNodes ().item (0).getNodeValue ();
         }
         else if (subNode.getNodeName ().equals (
            LoggerServer.SECTION_CATEGORYENTRY))
         {
            parseEntry (subNode);
         }
      }
   }

   /**
    * Getter for the category's name.
    *
    * @return The category's name.

    */
   public String getCategoryName ()
   {
      return _categoryName;
   }

   /**
    * Determines the logical file name for the specified severity level. The
    * file name can then be used to access the file handle associated with it.
    *
    * @param severity The severity to get the logical file name for.
    * @return The logical file name for the specified severity, or <code>
    * null</code> if no file name is found for the severity.

    */
   public String getLogFileNameForSeverity (int severity)
   {
      return (String)_entries.get ("" + severity);
   }
   
   //IValavala IR POUM080661913. Removing this method. Not necessary or used.
   //IValavala IR POUM080661913 add process name to file name. 
  /* public String getLogFileNameForSeverity (int severity, String processName)
   {
      return (String)_entries.get ("" + severity)+ "_" + processName;
   }
   */

   /**
    * Parses an category entry section from the XML file. The category entry
    * section contains two tags, one for the error level and one for the logical
    * error file name.
    *
    * @param node The XML node containing the category entry section data.

    */
   private void parseEntry (Node node)
   {
      int      count = 0;
      Node     subNode = null;
      NodeList nodeList = null;
      String   nodeValue = null,
               categoryLevel = null,
               categoryFile = null;

      if (!node.hasChildNodes ())
      {
         return;
      }
      nodeList = node.getChildNodes ();
      for (count = 0;count < nodeList.getLength ();count++)
      {
         subNode = nodeList.item (count);
         if ((subNode.getChildNodes () != null) &&
             (subNode.getChildNodes ().item (0) != null))
         {
            nodeValue = subNode.getChildNodes ().item (0).getNodeValue ();
         }
         else
         {
            nodeValue = null;
         }
         if (subNode.getNodeName ().equals (LoggerServer.ELEMENT_CATEGORYLEVEL))
         {
            categoryLevel = nodeValue;
         }
         else if (subNode.getNodeName ().equals (
            LoggerServer.ELEMENT_CATEGORYFILE))
         {
            categoryFile = nodeValue;
         }
      }
      if ((categoryLevel != null) && (categoryFile != null))
      {
         _entries.put (categoryLevel, categoryFile);
      }
   }

   /**
    * Creates a String version of the class instance. This involves 'dumping'
    * all the class's data into a formatted string.
    *
    * @return The string representation of the class instance.

    */
   public String toString ()
   {
      Enumeration enumer = null;
      String returnVal = "",
             level = null;

      returnVal += "Name: " + _categoryName;
      if (_entries.isEmpty ())
      {
         returnVal += " Entries: (null)";
      }
      else
      {
         returnVal += " Entries: ";
         for (enumer = _entries.keys ();enumer.hasMoreElements ();)
         {
            level = (String)enumer.nextElement ();
            returnVal += " '" + level + "::" +
                         (String)_entries.get (level) + "'";
         }
      }
      return returnVal;
   }
}
