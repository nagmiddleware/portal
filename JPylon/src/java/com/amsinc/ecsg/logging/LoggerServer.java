/*
 * @(#)LoggerServer
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
package com.amsinc.ecsg.logging;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.*;
import org.w3c.dom.*;
import org.apache.xerces.parsers.DOMParser;

/*
 * Potential enhancements:
 * 1) Multiple entries with the same logical name so that a logical files can
 * have multiple physical files.
 * 2) Multiple log files allowed for a class/level pair
 * 3) Add some sort of session information to the log message so that a
 * particular session can be followed through the log. This would have to be
 * done on the sender's side.
 * 4) Allow the user to customize the format of the message written to the log
 * to include only the data they care about; date, category, session id (when/if
 * it exists), and/or the message.
 *
 * Potential validation checks:
 * 1) File name specified for a category does not match any defined logical
 *    file names.
 * 2) Default category name must exist.
 *
 * Limits:
 * 1) In the creator for the ServerSocket we are limiting the server socket's
 * backlog to 99,999,999.
 *
 */
/**
 * This is the server portion of persistent logging. It monitors a specified
 * port at a specified IP location for socket connects. The connections are
 * used to send messages that are to be logged persistently. The message must
 * be in a specific format:<br>
 * &lt;category name&gt;$$&lt;severity&gt;$$&lt;message&gt;<br>
 * as we will be parsing it to get the different parts out. Using the
 * category name and severity, we then determine where the message will be
 * logged and log it. We are using a thread pool where we assign a thread to
 * each client socket connection for processing that socket's messages. The
 * use of a pool makes us more responsive to incomming client socket
 * connections.
 */
public class LoggerServer
{
   /** Default maximum number of bytes to write to a log file */
   public static int DEFAULT_MAXFILELENGTH = 1000000;
   /** Default port to listen to if none is specified in the properties file */
   public static int DEFAULT_PORT = 4000;
   /** Default number of threads for the thread pool */
   public static int DEFAULT_THREADS = 20;
   /** Default server to listen to if none is specified in the properties
       file */
   public static String DEFAULT_SERVER = "127.0.0.1";
   /** Default value of the TCP no delay setting to be used when none is
       specified in the properties file */
   public static boolean DEFAULT_TCPNODELAY = false;
   /** Name of the default category that is used when a specific category does
       not have logging information defined */
   public static String DEFAULT_CATEGORYNAME = "default";
   /** Logging.xml file entry specifying we are defining logging info */
   public static String SECTION_LOGGING = "Logging";
   /** Logging.xml file entry specifying we are defining a log file */
   public static String SECTION_LOGFILE = "LogFile";
   /** Logging.xml file entry specifying we are defining an category's
       logging information */
   public static String SECTION_CATEGORY = "Category";
   /** Logging.xml file entry specifying we are defining an category's
       level/file information */
   public static String SECTION_CATEGORYENTRY = "CategoryEntry";
   /** Logging.xml file entry specifying we are defining a log file's
       logical name */
   public static String ELEMENT_LOGICALNAME = "logicalName";
   /** Logging.xml file entry specifying we are defining a log file's
       physical name */
   public static String ELEMENT_PHYSICALNAME = "physicalName";
   /** Logging.xml file entry specifying we are defining an category's name */
   public static String ELEMENT_CATEGORYNAME = "categoryName";
   /** Logging.xml file entry specifying we are defining an error level
       for an category's logging */
   public static String ELEMENT_CATEGORYLEVEL = "categoryLevel";
   /** Logging.xml file entry specifying we are defining a logical file name
       for an category's logging */
   public static String ELEMENT_CATEGORYFILE = "categoryFile";
   /** String used in data written to the logger port to delimit the entries in
       the lines of data */
   public static String MESSAGE_DELIMITER = "$$";
   /** String used as a logical file name to indicate that we do not wish to
       write these log messages to a file */
   public static String DO_NOT_LOG = "null";

   /** Hash where key/result are category_name/LoggerCategoryInfo_instance. */
   private Hashtable _logCategories = null;
   /** Hash where key/result are logical_name/file_handle. */
   private Hashtable _logFiles = null;
   /** Hash where key/result are logical_name/physical_name. */
   private Hashtable _logFileNames = null;
   /** Server socket we are listening for data on */
   private ServerSocket serverSocket = null;
   /** List of all the threads available for client socket processing */
   private Vector _threadPool;
   /** Use to track which threads are currently being used */
   private boolean[] _threadsInUse;
   /** Port number we are listening for data on */
   private int    _loggingPort = LoggerServer.DEFAULT_PORT;
   /** Number of threads in the thread pool */
   private int    _loggingThreads = LoggerServer.DEFAULT_THREADS;
   /** Name of the server we are listening for data on */
   private String _loggingServer = DEFAULT_SERVER;
   /** Indicates if a socket should be set to TCP no delay. Setting this value
    to <code>true</code> means written data to the network is not buffered
    pending acknowledgement of previously written data. Therefor it
    potentially sacrifices accuracy for speed. */
   private boolean _TCPNoDelay = DEFAULT_TCPNODELAY;
   /** The maximum length log files are allowed to grow to. When this length
       is exceeded, the files are "rolled" */
   private int _maxFileLength = DEFAULT_MAXFILELENGTH;

   /**
    * Creates a new logger instance. This includes the following:<br>
    * 1) Reading the port number, server name, and the logger XML configuration
    * file name from the LoggerServer properties bundle.<br>
    * 2) Initializing class instance variables.<br>
    * 3) Parsing the logger XML configuration file.<br>
    * 4) Opening the specified log files in preparation for writing to them.<br>
    * 5) Initializing the thread pool.<br>
    * 6) Attaching to the specified port to get log messages from it.<br>
    *

    */
   public LoggerServer ()
          throws Exception
   {
      PropertyResourceBundle myProps = null;
      DOMParser parser = null;
      String  propValue = null;

      myProps = (PropertyResourceBundle)PropertyResourceBundle.getBundle (
         "LoggerServer");
      propValue = getPropertyValue (myProps, "loggingServer");
      if (propValue != null)
      {
         _loggingServer = propValue;
      }
      propValue = getPropertyValue (myProps, "loggingTCPNoDelay");
      if ((propValue != null) && (propValue.compareToIgnoreCase ("y") == 0))
      {
         _TCPNoDelay = true;
      }
      System.out.println ("Logging Server: " + _loggingServer);
      propValue = getPropertyValue (myProps, "loggingPort");
      if (propValue != null)
      {
         try
         {
            _loggingPort = Integer.parseInt (propValue);
         }
         catch (NumberFormatException nfEx)
         {  // use the default port
         }
      }
      System.out.println ("Logging Port: " + _loggingPort);
      propValue = getPropertyValue (myProps, "loggingMaxFileLength");
      if (propValue != null)
      {
         try
         {
            _maxFileLength = Integer.parseInt (propValue);
         }
         catch (NumberFormatException nfEx2)
         {  // use the default maximum length
         }
      }
      System.out.println ("Max File Length: " + _maxFileLength);
      propValue = getPropertyValue (myProps, "loggingThreads");
      if (propValue != null)
      {
         try
         {
            _loggingThreads = Integer.parseInt (propValue);
         }
         catch (NumberFormatException nfEx2)
         {  // use the default thread number
         }
      }
      System.out.println ("Number of threads in pool: " + _loggingThreads);
      propValue = getPropertyValue (myProps, "loggingConfigurationFile");
      if (propValue != null)
      {
         System.out.println ("Config File: " + propValue);
      }
      else
      {
         throw new Exception (
            "Need to supply a loggingConfigurationFile entry in the LoggerServer.properties file");
      }
      _logCategories = new Hashtable ();
      _logFiles = new Hashtable ();
      _logFileNames = new Hashtable ();
      parser = new DOMParser ();
      parser.parse (propValue);
      parseXML (parser.getDocument ());
      openLogFiles ();
      buildThreadPool (_loggingThreads);
      attachToPort (_loggingServer, _loggingPort);
   }

   /**
    * Returns the maximum size we should let files grow to, in bytes. Once a
    * file exceeds this size we are going to try to "roll" our logging to a
    * new file by renaming the current file and opening a new file handle to
    * a file by the original name of the renamed oversized file.
    *
    * @return The maximum file size.

    */
   public int getMaxFileLength ()
   {
      return _maxFileLength;
   }

   /**
    * Assign a client socket to be read to one of our threads.
    *
    * @param socket The client socket to be read by this thread.

    */
   public synchronized void startLoggerThread (Socket socket)
   {
      int count = 0;
      LoggerServerLogic thread = null;

      // Synchronize of the list of threads in use so we do not assign the
      // same thread to multiple sockets
      synchronized (_threadsInUse)
      {
         for (count = 0;count < _loggingThreads;count++)
         {
            if (!_threadsInUse[count])
            {
               _threadsInUse[count] = true;
               thread = (LoggerServerLogic)_threadPool.get (count);
               synchronized (thread)
               {
                  thread.setClientSocket (socket);
               }
               break;
            }
         }
      }
   }

   /**
    * Puts a thread back in the available pool so that is can be used again
    * by a future client socket.
    *
    * @param threadNumber Which thread we are putting back in the available
    * thread pool.

    */
   public void releaseLoggerThread (int threadNumber)
   {
      _threadsInUse[threadNumber] = false;
   }

   /**
    * Creates the pool of threads that we will use to process data from client
    * sockets. We are using a pool in order to speed up processing. Without a
    * pool we would create the threads on demand when we get a client socket
    * connection. Since creating the threads is a relatively expensive
    * operation, we can save some time by creating them during initialization
    * and then reusing them.
    *
    * @param numThreads The number of threads to create for the thread pool.

    */
   private void buildThreadPool (int numThreads)
   {
      LoggerServerLogic newThread = null;
      int count = 0;

      _threadPool = new Vector (numThreads);
      _threadsInUse = new boolean[numThreads];
      for (count = 0;count < numThreads;count++)
      {
         newThread = new LoggerServerLogic (this, count);
         newThread.start ();
         _threadPool.add (newThread);
         _threadsInUse[count] = false;
      }
   }
   /**
    * Controls the parsing of an XML document in order to populate the
    * logger information that has been defined.
    *
    * @param document The document to be processed.

    */
   private void parseXML (Document document)
           throws Exception
   {
      NodeList nodeList = null;
      Node     node = null;
      int      count = 0;

      if (document == null)
         return;
      nodeList = document.getElementsByTagName (LoggerServer.SECTION_LOGGING);
      if ((nodeList == null) || (nodeList.getLength () == 0))
      {
         throw new Exception (
            "Logging configuration file is missing required " +
            LoggerServer.SECTION_LOGGING + " tag");
      }
      for (count = 0;count < nodeList.getLength ();count++)
      {
         node = nodeList.item (count);
         parseSubXML (node);
      }
   }

   /**
    * Controls the parsing of a specfic node of an XML document. Looks for
    * high level tags we are interested in, such as LogFile and Category,
    * and calls additional methods to pull the information we need out of those
    * sections.
    *
    * @param node The XML node to be examined.

    */
   private void parseSubXML (Node node)
   {
      LoggerCategoryInfo categoryInfo = null;
      NodeList nodeList = null;
      Node     subNode = null;
      int      count = 0;

      if (node.getNodeName ().equals (LoggerServer.SECTION_LOGGING))
      {
         if (node.hasChildNodes ())
         {
            nodeList = node.getChildNodes ();
            for (count = 0;count < nodeList.getLength ();count++)
            {
               subNode = nodeList.item (count);
               parseSubXML (subNode);
            }
         }
      }
      else if (node.getNodeName ().equals (LoggerServer.SECTION_LOGFILE))
      {
         parseLogFile (node);
      }
      else if (node.getNodeName ().equals (LoggerServer.SECTION_CATEGORY))
      {
         categoryInfo = new LoggerCategoryInfo (node);
         _logCategories.put (categoryInfo.getCategoryName (), categoryInfo);
      }
   }

   /**
    * Parses a log file section from the XML file. The log file section
    * contains two tags, one for the logical file name and one for the
    * physical file name.
    *
    * @param node The XML node containing the log file section data.

    */
   private void parseLogFile (Node node)
   {
      int      count = 0;
      Node     subNode = null;
      NodeList nodeList = null;
      String   nodeValue = null,
               logicalName = null,
               physicalName = null;

      if (!node.hasChildNodes ())
      {
         return;
      }
      nodeList = node.getChildNodes ();
      for (count = 0;count < nodeList.getLength ();count++)
      {
         subNode = nodeList.item (count);
         if ((subNode.getChildNodes () != null) &&
             (subNode.getChildNodes ().item (0) != null))
         {
            nodeValue = subNode.getChildNodes ().item (0).getNodeValue ();
         }
         else
         {
            nodeValue = null;
         }
         if (subNode.getNodeName ().equals (LoggerServer.ELEMENT_LOGICALNAME))
         {
            logicalName = nodeValue;
         }
         else if (subNode.getNodeName ().equals (
            LoggerServer.ELEMENT_PHYSICALNAME))
         {
            physicalName = nodeValue;
         }
      }
      if ((logicalName != null) && (physicalName != null))
      {
         _logFileNames.put (logicalName, physicalName);
      }
      else if (logicalName == null)
      {
         if (physicalName != null)
         {
            System.out.println ("Log with physical name: " + physicalName +
                                " has no logical name");
         }
         else
         {
            System.out.println (
               "Log file entry with no logical or physical name was found");
         }
      }
      else
      {
         System.out.println ("Log with logical name: " + logicalName +
                             " has no physical name");
      }
   }

   /**
    * Wrapper aroung the PropertyResourceBundle class's getString method. If
    * a MissingResourceException is caught, we return <code>null</code>,
    * otherwise we return the getString method's return value.
    *
    * @param myProps The resource bundle to query for a value.
    * @param key The resource bundle key to find the value of.
    * @return The key's value if one is found, otherwise <code>null</code>.

    */
   private String getPropertyValue (PropertyResourceBundle myProps, String key)
   {
      try
      {
         return myProps.getString (key);
      }
      catch (MissingResourceException mrEx)
      {
         return null;
      }
   }

   /**
    * Determines the LoggerCategoryInfo instance that is associated with a
    * given category name. Used when we are trying to get information about the
    * logs being used by a specific category.
    *
    * @param categoryName Name of the category to get information for.
    * @return The LoggerCategoryInfo instance that was found for the category
    * name or <code>null</code> if no instance was found for the specified name.

    */
   public LoggerCategoryInfo getLoggerCategoryForCategoryName (String categoryName)
   {
      return (LoggerCategoryInfo)_logCategories.get (categoryName);
   }

   /**
    * Determines the PrintWriter instance that is associated with a given
    * logical file name. Note that if the file name matches the DO_NOT_LOG
    * string constant we return a <code>null</code> to indicate not to perform
    * logging.
    *
    * @param logicalName Name of the file to get information for.
    * @return The PrintWriter instance that was found for the file name
    * or <code>null</code> if no instance was found or the file name matches
    * the DO_NOT_LOG string constant.

    */
   public PrintWriter getWriterForLogicalName (String logicalName)
   {
      if (logicalName.equals (LoggerServer.DO_NOT_LOG))
         return null;
      return (PrintWriter)_logFiles.get (logicalName);
   }
   
   //IValavala IR POUM080661913. Added param processName
   public PrintWriter getWriterForLogicalName (String logicalName, String processName)
   {
	   PrintWriter printWriter = null;
      if (logicalName.equals (LoggerServer.DO_NOT_LOG))
         return null;
      printWriter = (PrintWriter)_logFiles.get (logicalName+processName);
      //IValavala
      //System.out.println("LoggerServer.getWriterForLogicalName:" );
      if (printWriter==null){
    	  openLogFile(logicalName, processName, _logFiles);
    	  printWriter = (PrintWriter)_logFiles.get (logicalName+processName);
      }
     return printWriter;
   }
   
 //IValavala IR POUM080661913. Open a single log file
   private void openLogFile (String logicalName, String processName, Hashtable _logFiles)
   {
      PrintWriter writer = null;
      Enumeration enumer = null;
      String      physicalName = null;

      if (_logFileNames.isEmpty ())
         return;
         physicalName = (String)_logFileNames.get (logicalName);
         try
         {
            // Create the PrintWriter with autoflush on
            writer =     new PrintWriter (new FileWriter (physicalName+ "_"+processName + ".log", true), true);
            _logFiles.put (logicalName+processName, writer);
         }
         catch (IOException ioEx)
         {
            System.out.println ("Could not open output file: " + physicalName+ "_"+processName);
         }
      
   }
  

   /**
    * Replaces the entry in the list of logical file names and their associated
    * PrintWriter with the specified logical name and PrintWriter. This is
    * typically done as part of the file "rolling" that occurs when a file
    * exceeds it's maximum size.
    *
    * @param logicalName Logical name of the entry to be replaced.
    * @param writer The PrintWriter to be used in the replacement entry.

    */
 //IValavala Oct 6 2012. IR#MMUM092039389. Added callingProcess
   public void replaceWriterForLogicalName (String logicalName, String processName, PrintWriter writer)
   {
      _logFiles.remove (logicalName+processName);
      _logFiles.put (logicalName+processName, writer);
   }

   /**
    * Determines the physical file name that is associated with a given
    * logical file name.
    *
    * @param logicalName Name of the file to get information for.
    * @return The physical file name that was found for the logical file name.

    */
   public String getPhysicalForLogicalName (String logicalName)
   {
      return (String)_logFileNames.get (logicalName);
   }

   /**
    * Loops through all the logical/physical files names we read in from the
    * XML file and attempts to open the files for writing. All files are opened
    * in append mode. If a file cannot be opened this is written to standard
    * out. When all the file names have been processed the _logFileNames
    * Hashtable contains logical/actual_file as its key/result.
    *

    */
   private void openLogFiles ()
   {
      Hashtable   withFiles = new Hashtable ();
      PrintWriter writer = null;
      Enumeration enumer = null;
      String      logicalName = null,
                  physicalName = null;

      if (_logFileNames.isEmpty ())
         return;
      for (enumer = _logFileNames.keys ();enumer.hasMoreElements ();)
      {
         logicalName = (String)enumer.nextElement ();
         physicalName = (String)_logFileNames.get (logicalName);
         try
         {
            // Create the PrintWriter with autoflush on
            writer =
               new PrintWriter (new FileWriter (physicalName, true), true);
            withFiles.put (logicalName, writer);
         }
         catch (IOException ioEx)
         {
            System.out.println ("Could not open output file: " + physicalName);
         }
      }
      _logFiles = withFiles;
   }

   /**
    * Establish our connection to the port we are listening on.
    *
    * @param server The name of the server we are listing to.
    * @param port The number of the port we are listening on.

    */
   private void attachToPort (String server, int port)
           throws UnknownHostException, BindException, SocketException,
                  IOException
   {
      InetAddress serverAddr = null;

      try
      {
         serverAddr = InetAddress.getByName (server);
      }
      catch (UnknownHostException uhEx)
      {
         System.out.println ("Server lookup for: " + server + " failed");
         throw uhEx;
      }
      try
      {
         serverSocket = new ServerSocket (port, 99999999, serverAddr);
      }
      catch (BindException bEx)
      {
         System.out.println ("Bind to port: " + port + " failed");
         throw bEx;
      }
      catch (SocketException sEx)
      {
         System.out.println ("Bind to port: " + port + " failed");
         throw sEx;
      }
   }

   /**
    * 'Listens' for client connections to the port we are monitoring. When
    * a client connects, a connection socket is created and a thread to process
    * it is created and run. We then wait for the next client connection so we
    * can repeat the process.
    *

    */
   public void process ()
   {
      Socket clientSocket = null;

      for (;;)
      {
         try
         {
            clientSocket = serverSocket.accept ();
            clientSocket.setTcpNoDelay (_TCPNoDelay);
            startLoggerThread (clientSocket);
         }
         catch (IOException ioEx)
         {
            System.out.println ("Could not open client socket" +
                                ioEx.toString ());
         }
      }
   }

   /**
    * Creates a String version of the class instance. This involves 'dumping'
    * all the class's data into a formatted string.
    *
    * @return The string representation of the class instance.

    */
   public String toString ()
   {
      LoggerCategoryInfo logCategory = null;
      Enumeration enumer = null;
      String returnVal = null,
             logFile = null;

      returnVal = "Logging Server: " + _loggingServer;
      returnVal += " Logging Port: " + _loggingPort;
      if (_logFiles.isEmpty ())
      {
         returnVal += " Log Files: (none)";
      }
      else
      {
         returnVal += " Log Files:";
         for (enumer = _logFiles.keys ();enumer.hasMoreElements ();)
         {
            logFile = (String)enumer.nextElement ();
            returnVal += " '" + logFile + "'";
         }
      }
      if (_logCategories.isEmpty ())
      {
         returnVal += " Log Categories: (none)";
      }
      else
      {
         returnVal += " Log Categories: ";
         for (enumer = _logCategories.elements ();enumer.hasMoreElements ();)
         {
            logCategory = (LoggerCategoryInfo)enumer.nextElement ();
            returnVal += " " + logCategory.toString ();
         }
      }
      return returnVal;
   }

   /**
    * Starts up the LoggerServer application. This includes instantiating a
    * LoggerServer category and telling it to begin processing.
    *

    */
   public static void main (String argv[])
   {
      LoggerServer logger = null;

      try
      {
         logger = new LoggerServer ();
      }
      catch (Exception ex)
      {
         System.out.println ("Exception starting logger: " + ex.toString ());
         ex.printStackTrace ();
      }
      if (logger != null)
      {
         System.out.println (logger.toString ());
         logger.process ();
      }
   }
}
