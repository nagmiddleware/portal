package com.amsinc.ecsg.web;

/*
 * @(#)ClientRequestInfo
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import java.util.*;

public class ClientRequestInfo implements java.io.Serializable {

    // Class variables
    private String nextPage;
    private String errorPage;
    private String formName;
    private boolean advanceCurrentPage;
    /**
     * A hashtable of additional parameters, usually application specific, to send to the
     * AmsServlet call.
     */
    private Hashtable addlParms;
  
    public ClientRequestInfo(String nextPage, String errorPage, String formName, boolean advanceCurrentPage) {
        this.nextPage = nextPage;
        this.errorPage = errorPage;
        this.formName = formName;
        this.advanceCurrentPage = advanceCurrentPage;
    }

    public String getNextPage() {
      return nextPage;
    }

    public String getErrorPage() {
      return errorPage;
    }

    public void setNextPage(String nextPage) {
      this.nextPage = nextPage;
    }

    public void setErrorPage(String errorPage) {
      this.errorPage = errorPage;
    }

    public String getFormName() {
      return formName;
    }
    
    public boolean getAdvanceCurrentPage() {
        return advanceCurrentPage;
    }

    public void setAddlParm(String parmName, String parmValue) {
        getAddlParms().put(parmName, parmValue);
    }

    public Hashtable getAddlParms() {
        if (addlParms == null) {
            addlParms = new Hashtable();
        }
        return addlParms;
    }
    /**
     * Produces a string version of this class instance's data.
     *
     * @return The string representaion of the class's data.
     */
    public String toString()
    {
         StringBuilder sbuff = new StringBuilder("ClientRequestInfo [formName=");
         sbuff.append (formName);
         sbuff.append (", nextPage=");
         sbuff.append (nextPage);
         sbuff.append (", errorPage=");
         sbuff.append (errorPage);
         sbuff.append ("]");
         return sbuff.toString ();
    }
}
