package com.amsinc.ecsg.web;

/*
 * @(#)QueryBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.util.*;
import com.amsinc.ecsg.frame.*;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * QueryBean is an infrastructure web bean that acts as an interface to the
 * query services located on the EJB application server. There are methods to
 * execute generic list queries (lists of objects) and list views (handwritten
 * SQL queries). QueryBean stores the result set as an XML document until
 * another query is executed. If desired, the XML document containing a result
 * set can be saved in the document cache before this occurs.
 */
public class QueryBean extends AmsWebBean {
	private static final Logger LOG = LoggerFactory.getLogger(QueryBean.class);
    /**
     * The XML result set document.
     */
    private DocumentHandler xmlDoc;

    /**
     * An indicator to determine whether to wrap the arguments to a query with
     * tic marks
     */
    private boolean bWrapArguments;

    public QueryBean() {
        super();
        xmlDoc = null;
        bWrapArguments = true;
    }

    /**
     * This method runs the specified generic list query.
     *
     *

     * @param objectName The name of the business object that will be processed.
     * @return The result set document
     */
    public DocumentHandler runQuery(String objectName) {
        return (this.runQuery(objectName, null, new ArrayList<String>()));
    }

    /**
     * This method runs the specified generic list query.
     *

     * @param objectName The name of the business object that will be processed.
     * @param queryName The name of the listType that will be used for list
     * processing.
     * @return The result set document
     */
    public DocumentHandler runQuery(String objectName, String queryName) {
        return (this.runQuery(objectName, queryName, new ArrayList<String>()));
    }

    /**
     * This method runs the specified generic list query given the query
     * argument.
     *
     * @param objectName The name of the business object that will be processed.
     * @param queryName The name of the listType that will be used for list
     * processing.
     * @param queryParm A parameter value used for list processing.

     * @return DocumentHandler The result set document
     */
    public DocumentHandler runQuery(String objectName,
            String queryName,
            String queryParm) {
        List<String> queryParms = new ArrayList<>();
        queryParms.add(queryParm);
        return (this.runQuery(objectName, queryName, queryParms));
    }

    /**
     * This method runs the specified generic list query given the query
     * arguments.
     *

     * @param objectName The name of the business object that will be processed.
     * @param queryName The name of the listType that will be used for listi
     * processing.
     * @param queryParm1 A parameter value used for list processing.
     * @param queryParm2 A parameter value used for list processing.
     * @return DocumentHandler The result set document
     */
    public DocumentHandler runQuery(String objectName, String queryName,
            String queryParm1, String queryParm2) {
        List<String> queryParms = new ArrayList<>();
        queryParms.add(queryParm1);
        queryParms.add(queryParm2);
        return (this.runQuery(objectName, queryName, queryParms));
    }

    /**
     * This method runs the specified generic list query given the query
     * arguments.
     *

     * @param objectName The name of the business object that will be processed.
     * @param queryName The name of the listType that will be used for list
     * processing.
     * @param queryParms A collection of string parameter values used for list
     * processing.
     * @return The result set document
     */
    public DocumentHandler runQuery(String objectName,
            String queryName,
            List<String> queryParms) {
        // The remote interface to the QueryMediator EJB component.

        DocumentHandler inputDoc = new DocumentHandler();

        inputDoc.setAttribute("/In/Query/objectname", objectName);
        if (queryName != null) {
            inputDoc.setAttribute("/In/Query/querylisttype", queryName);
            if (queryParms.size() > 0) {
                inputDoc.setAttributes("/In/Query/parameters", queryParms);
            }
        }

        if (!bWrapArguments) {
            inputDoc.setAttribute("/In/Query/wrapargs", "N");
        }

        try {
            QueryMediator queryMed;
            if (beanMgr.getUserLoginInfo() == null) {
                queryMed = (QueryMediator) EJBObjectFactory.createClientEJB(
                        getServerLocation(), "QueryMediator");
            } else {
                queryMed = (QueryMediator) EJBObjectFactory.createSecureClientEJB(
                        getServerLocation(), "QueryMediator",
                        beanMgr.getUserLoginInfo().getLoginName(),
                        beanMgr.getUserLoginInfo().getLoginPassword());
            }

            DocumentHandler outputDoc = queryMed.performService(inputDoc);
            setXmlDoc(outputDoc.getComponent("/Out"));

            queryMed.remove();
        } catch (Exception ex) {
            LOG.error("Exception in runQuery()", ex);
        }

        return getXmlDoc();
    }

    /**
     * This method runs the specified view list query.
     *

     * @param viewName The name of the EJB view list object that will be
     * processed.
     * @return The result set document
     */
    public DocumentHandler runViewQuery(String viewName) {
        List<String> queryParms = new ArrayList<>();
        return (this.runViewQuery(viewName, queryParms));
    }

    /**
     * This method runs the specified view list query.
     *

     * @param viewName The name of the EJB view list object that will be
     * processed.
     * @param queryParm A parameter value used for list processing.
     * @return The result set document
     */
    public DocumentHandler runViewQuery(String viewName, String queryParm) {
        List<String> queryParms = new ArrayList<>();
        queryParms.add(queryParm);
        return (this.runViewQuery(viewName, queryParms));
    }

    /**
     * This method runs the specified view list query.
     *

     * @param viewName The name of the EJB view list object that will be
     * processed.
     * @param queryParm1 A parameter value used for list processing.
     * @param queryParm2 A parameter value used for list processing.
     * @return The result set document
     */
    public DocumentHandler runViewQuery(String viewName,
            String queryParm1,
            String queryParm2) {
        List<String> queryParms = new ArrayList<>();
        queryParms.add(queryParm1);
        queryParms.add(queryParm2);
        return (this.runViewQuery(viewName, queryParms));
    }

    /**
     * This method runs the specified view list query.
     *

     * @param viewName The name of the EJB view list object that will be
     * processed.
     * @param queryParm1 A parameter value used for list processing.
     * @param queryParm2 A parameter value used for list processing.
     * @param queryParm3 A parameter value used for list processing.
     * @return The result set document
     */
    public DocumentHandler runViewQuery(String viewName, String queryParm1,
            String queryParm2, String queryParm3) {
        List<String> queryParms = new ArrayList<>();
        queryParms.add(queryParm1);
        queryParms.add(queryParm2);
        queryParms.add(queryParm3);
        return (this.runViewQuery(viewName, queryParms));
    }

    /**
     * This method runs the specified view list query.
     *

     * @param viewName The name of the EJB view list object that will be
     * processed.
     * @param queryParms A collection of string parameter values used for list
     * processing.
     * @return The result set document
     */
    public DocumentHandler runViewQuery(String viewName, List<String> queryParms) {
        // The remote interface to the ListView EJB component.
        try {
            String args[] = new String[queryParms.size()];
            args = queryParms.toArray(args);
            ListView listView = (ListView) EJBObjectFactory.createClientEJB(getServerLocation(), viewName);
            setXmlDoc(listView.getRecordsAsXml(args));
            listView.remove();
        } catch (Exception ex) {
        	LOG.error("Exception in runViewQuery()", ex);
        }

        return getXmlDoc();
    }

    /**
     * This method sets the XML result set document.
     *

     * @param xmlDoc the XML result set document
     */
    public void setXmlDoc(DocumentHandler xmlDoc) {
        if (xmlDoc == null) {
            xmlDoc = new DocumentHandler();
        }
        this.xmlDoc = xmlDoc;
    }

    /**
     * This method returns the XML result set document.
     *

     * @return The result set document
     */
    public DocumentHandler getXmlDoc() {
        return xmlDoc;
    }

    /**
     * This method sets the wrap arguments indicator.
     *

     * @param bWrapArguments the wrap arguments indicator
     */
    public void setWrapArguments(boolean bWrapArguments) {
        this.bWrapArguments = bWrapArguments;
    }

    /**
     * This method runs the specified SQL query.
     *

     * @param sql The sql string to execute
     * @return The result set document
     */
    public DocumentHandler runSQL(String sql) {
        try {
            QueryListView qlv = (QueryListView) EJBObjectFactory.createClientEJB(
                    getServerLocation(), "QueryListView");
            qlv.setSQL(sql);
            setXmlDoc(qlv.getRecordsAsXml());
            qlv.remove();
        } catch (Exception ex) {
        	LOG.error("Exception in runSQL()", ex);
        }

        return getXmlDoc();
    }
}
