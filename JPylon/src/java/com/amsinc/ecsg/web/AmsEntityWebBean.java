package com.amsinc.ecsg.web;

/*
 * @(#)AmsEntityWebBean
 *
 *     Copyright  � 2001
 *     American Management Systems, Incorporated
 *     All rights reserved
 */
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amsinc.ecsg.frame.AmsException;
import com.amsinc.ecsg.frame.AmsServlet;
import com.amsinc.ecsg.frame.BusinessObject;
import com.amsinc.ecsg.frame.DataSchemaFactory;
import com.amsinc.ecsg.frame.DatabaseQueryBean;
import com.amsinc.ecsg.frame.EJBEnvironmentProperties;
import com.amsinc.ecsg.frame.EJBObjectFactory;
import com.amsinc.ecsg.frame.JPylonDBConnection;
import com.amsinc.ecsg.util.DocumentHandler;
import com.amsinc.ecsg.util.DocumentNode;
import com.amsinc.ecsg.util.StringFunction;
import com.amsinc.ecsg.util.StringService;
import com.workingdogs.village.KeyDef;
import com.workingdogs.village.Record;
import com.workingdogs.village.TableDataSet;

/**
 * AmsEntityWebBean is the base class for all entity Java Beans components on
 * the web server in the eCSG architecture. It stores the list of registered
 * attributes, provides methods to transport the object into and out of XML
 * documents, and can refresh its data from the application server. Typically,
 * entity web beans correspond to either single business objects, a business
 * objects and its aggregated objects data, or a result set record from a list
 * view query. An entity web bean does not need to register all of the
 * attributes of a corresponding business object or result set, only those
 * fields that are relevant for presentation purposes within the application.
 */
public class AmsEntityWebBean extends AmsWebBean {
	private static final Logger LOG = LoggerFactory.getLogger(AmsEntityWebBean.class);
    /**
     * This hashtable stores all of the attributes of this object and their
     * corresponding values.
     */
    protected Hashtable htRegisteredAttributes;
    /**
     * This Vector stores a list of the components stored in this object.
     */
    protected Vector componentList;
    /**
     * This is the name of the attribute which stores the object identifier.
     */
    private String oidFieldName;
    private String dataTableName;

    public boolean isValid = false;

    public AmsEntityWebBean() {
        super();
      // Single entities should be valid from initialization. A list component
        // will override this if necessary.
        htRegisteredAttributes = new Hashtable();
        componentList = new Vector();
        oidFieldName = null;
        registerComponents();
    }

    @Override
    public void init(BeanManager beanMgr) {
        registerAttributes();
        this.beanMgr = beanMgr;
    }

    public boolean isValid() {
        return this.isValid;
    }

    private String getDataTableName() {
        if (this.dataTableName == null) {
            Hashtable ejbEnvironment = EJBEnvironmentProperties.getEnvironment(this.getClass().toString());
            if (ejbEnvironment != null) {
                dataTableName = (String) ejbEnvironment.get("dataTableName");
            } else {
                ejbEnvironment = new Hashtable();
                try {
                    BusinessObject busObj = (BusinessObject) EJBObjectFactory.createClientEJB(
                            getServerLocation(), this.getEcsgObjectName(), this.getClientServerDataBridge());
                    dataTableName = busObj.getDataTableName();
                } catch (Exception e) {
                	LOG.error("Exception occured in getDataTableName", e);
                }
                ejbEnvironment.put("dataTableName", dataTableName);
                EJBEnvironmentProperties.putEnvironment(this.getClass().toString(), ejbEnvironment);
            }
        }
        return dataTableName;
    }

    public void getDataFromAppServer() {
        //Only if there is OID then get the data
        if (StringFunction.isNotBlank(this.getOid())) {
            getDataFromAppServerFromCondtion(oidFieldName + " = '" + StringService.filterSQLParam(this.getOid()) + "'");// T36000041885 RKAZI REL 9.3 07/22/2015 - Add
        }
    }

    public void getDataFromAppServerFromCondtion(String whereClause) {
       
        KeyDef objectKey = new KeyDef();

        try(Connection dbmsConnection = JPylonDBConnection.getConnection(false)) {
            
            TableDataSet objectData = new TableDataSet(dbmsConnection, DataSchemaFactory.getSchema(dbmsConnection, this.getDataTableName()), objectKey);
            //objectData.where(oidFieldName + " = " + this.getOid());
            objectData.where(whereClause);
            objectData.fetchRecords();

            if (objectData.size() > 0) {
                this.isValid = true;
                Record objectRecord = objectData.getRecord(0);
                Enumeration enumer = (Enumeration) htRegisteredAttributes.elements();
                while (enumer.hasMoreElements()) {
                    AmsEntityWebBeanInfo info = (AmsEntityWebBeanInfo) enumer.nextElement();

                // Do not try to read data for attribute types where
                    // data does not get stored in the database
                    if (!info.attributeType.equals("LocalAttribute")
                            && !info.attributeType.equals("ComputedAttribute")) {
                        String physicalName = info.physicalName;
                        try {
                            String attributeValue = objectRecord.getValue(physicalName).asString();

                            if (attributeValue == null) {
                                attributeValue = "";
                            }
                            this.setAttribute(info.logicalName, attributeValue);
                        } catch (Exception e) {
                            LOG.error("Error in getDataFromAppServer while retrieving {}" , info.logicalName, e);
                            this.isValid = false;
                        }
                    }
                }
            } else {
                this.isValid = false;
            }
        } catch (Exception e) {
        	LOG.error("General Error in getDataFromAppServer", e);
            this.isValid = false;
        } 

    }

    /**
     * This method is called upon initialization to register the attributes for
     * this web bean. This method should be overridden by the specific subclass.
     *

     */
    public void registerAttributes() {
    }

    /**
     * This method is called upon initialization to register the components for
     * this web bean. This method should be overridden by the specific subclass.
     *

     */
    public void registerComponents() {
    }

    /**
     * Registers an attribute with the attribute manager.
     *
     * This method creates a new entry in the RegisteredAttributes Hashtable and
     * initializes its value.
     *

     * @param attributeName The logical name of the attribute
     * @param physicalName The physicalName (DBName) of the attribute
     * @param attributeType The type of attribute (STRING, Integer, etc..)
     */
    public void registerAttribute(String attributeName,
            String physicalName,
            String attributeType) {
        AmsEntityWebBeanInfo info = new AmsEntityWebBeanInfo();
        info.logicalName = attributeName;
        info.physicalName = physicalName;
        info.attributeType = attributeType;
        info.value = "";

        htRegisteredAttributes.put(attributeName, info);
        if (attributeType.equals("ObjectIDAttribute")) {
            oidFieldName = attributeName;
        }
    }

    /**
     * Registers an attribute with the attribute manager.
     *
     * This method creates a new entry in the RegisteredAttributes Hashtable and
     * initializes its value. The attributes is assumed to be a string, non
     * object identifier field.
     *

     * @param attributeName The logical name of the attribute
     * @param physicalName The physicalName (DBName) of the attribute
     */
    public void registerAttribute(String attributeName, String physicalName) {
        registerAttribute(attributeName, physicalName, "");
    }

    /**
     * Registers a component with the web bean.
     *
     * @param componentIdentifier The identifier of the component
     * @param componentName The logical name of the component
     * @param componentType The component type (BusinessObject, ListingObject,
     * etc..)
     * @param idAttribute The attribute which identifies the component with it's
     * composite object.
     */
    public void registerComponent(String componentIdentifier,
            String componentName,
            String componentType,
            String idAttribute) {
        componentList.addElement(componentIdentifier);
    }

    /**
     * This method registers a one-to-many (listing) component object.
     *
     * NOTE: The default IDAttribute for one-to-many components is always the
     * object's ObjectIDAttribute, therefore it is not necessary to specify the
     * IDAttribute in this method.
     *
     * @param componentName The logical name of the component
     */
    protected void registerOneToManyComponent(String componentIdentifier,
            String componentName) {
        componentList.addElement(componentIdentifier);
    }

    /**
     * This method registers a one-to-one (businessObject) component object.
     *
     * @param componentIdentifier The identifier of the component
     * @param componentName The logical name of the component
     * @param idAttributeName The logical name of the attribute which is used to
     * establish the relationship between the composite (parent) and component
     * (child) object.
     */
    protected void registerOneToOneComponent(String componentIdentifier,
            String componentName,
            String idAttributeName) {
        componentList.addElement(componentIdentifier);
    }

    
    /**
     * This method gets an attribute value from a web bean.
     *

     * @param attributeName the name of the attribute, i.e. first_name
     * @return attribute value
     */
    public String getAttributeWithoutHtmlChars(String attributeName) {
        AmsEntityWebBeanInfo info = (AmsEntityWebBeanInfo) htRegisteredAttributes.get(attributeName);

        if (info != null) {
            return info.value;
        } else {
            return null;
        }
    }
    
    /**
     * This method gets an attribute value from a web bean.
     *

     * @param attributeName the name of the attribute, i.e. first_name
     * @return attribute value
     */
    public String getAttribute(String attributeName) {
        AmsEntityWebBeanInfo info = (AmsEntityWebBeanInfo) htRegisteredAttributes.get(attributeName);

        if (info != null) {
        	
            if ("DateAttribute".equals(info.attributeType) ||
                    "DateTimeAttribute".equals(info.attributeType) ) {
            	  return StringFunction.validatedDateOutputString(info.value);
            	          	
            }  else {
            	 return StringFunction.xssCharsToHtml(info.value);
            }           
        } else {
            return null;
        }
    }

    /**
     * This method sets an attribute value on a web bean.
     *

     * @param attributeName the name of the attribute, i.e. first_name
     * @param attributeValue the attribute is set to this value
     */
    public void setAttribute(String attributeName, String attributeValue) {
        AmsEntityWebBeanInfo info = (AmsEntityWebBeanInfo) htRegisteredAttributes.get(attributeName);
        if (info != null) {
            info.value = StringFunction.xssHtmlToChars(attributeValue);
            htRegisteredAttributes.put(attributeName, info);
        }
    }

    /**
     * For each specified attributePath/attributeValue pair, set the values
     * accordingly.
     *

     * @param attributePaths An array of attribute paths to be set.
     * @param attributeValues An array of attribute values that correspond to
     * the specified attributePaths array.
     */
    public void setAttributes(String[] attributePaths, String[] attributeValues) {

        int count = attributePaths.length;
        // For each attribute path requested, store the value in the hash
        for (int i = 0; i < count; i++) {
            this.setAttribute(attributePaths[i], StringFunction.xssHtmlToChars(attributeValues[i]));
        }
    }

    /**
     * This method returns the name of the object identifier attribute.
     *

     * @return name of object identifier attribute
     */
    public String getOidFieldName() {
        return oidFieldName;
    }

    /**
     * This method sets the name of the object identifier attribute.
     *
     * @param oidFieldName The name of the object identifier attribute

     */
    public void setOidFieldName(String oidFieldName) {
        this.oidFieldName = oidFieldName;
    }

    /**
     * This method returns the value of the object identifier attribute.
     *

     * @return name of object identifier attribute
     */
    public String getOid() {
        AmsEntityWebBeanInfo info = (AmsEntityWebBeanInfo) htRegisteredAttributes.get(oidFieldName);
        return info.value;
    }

    /**
     * This method takes an object identifier and populates this bean with the
     * appropriate data from the application server.
     *

     * @param oid
     */
    public void getById(String oid) {
        setAttribute(oidFieldName, oid);
        getDataFromAppServer();
    }

    /**
     * This method takes an XML document and puts this bean's attributes into
     * the document.
     *

     * @param doc The XML document object
     */
    @Override
    public void populateXmlDoc(DocumentHandler doc) {
        populateXmlDoc(doc, "");
    }

    /**
     * This method takes a document and puts this bean's attributes into the
     * document starting at the specified component path. If a '/' character
     * terminates the component path, that will be the absolute path name used,
     * otherwise, the eCSG object name will be appended.
     *

     * @param doc The XML document object
     * @param componentPath The path in the XML document to put the data
     */
    @Override
    public void populateXmlDoc(DocumentHandler doc, String componentPath) {
        componentPath = createXmlPath(componentPath);
        Enumeration enumer = htRegisteredAttributes.keys();
        while (enumer.hasMoreElements()) {
            String attrName = (String) enumer.nextElement();
            String path = componentPath.concat(attrName);
            String attrValue = ((AmsEntityWebBeanInfo) htRegisteredAttributes.get(attrName)).value;
            doc.setAttribute(path, attrValue);
        }
    }

    /**
     * This method takes an XML document and populates this bean's attributes
     * from the values in the document starting at the specified component path.
     *
     * @param doc The XML document object

     * @return true if object was found in document, otherwise false
     */
    @Override
    public boolean populateFromXmlDoc(DocumentHandler doc) {
        return (populateFromXmlDoc(doc, ""));
    }

    /**
     * This method takes an XML document and populates this bean's attributes
     * from the values in the document starting at the specified component path.
     *
     * @param doc The XML document object
     * @param String The path in the XML document to look for the data

     * @return true if object was found in document, otherwise false
     */
    @Override
    public boolean populateFromXmlDoc(DocumentHandler doc, String componentPath) {
        return (populateFromXmlDoc(doc, componentPath, true));
    }

    public boolean populateFromXmlDoc(DocumentHandler doc, String componentPath, boolean createComponentPath) {
        if (createComponentPath) {
            componentPath = createXmlPath(componentPath);
        }

        DocumentNode node = doc.getDocumentNode(componentPath);
        if (node == null) {
            this.isValid = false;
            return false;
        } else {
            node = node.getLeftChildNode();
            while (node != null) {
                String name, value;
                name = node.getTagName();
                value = node.getNodeValue(false);

                if (node.getLeftChildNode() == null) {

                    if (htRegisteredAttributes.containsKey(name)) {
                        this.setAttribute(name, value);
                    }
                }

                node = node.getRightSiblingNode();
            }
        }
        this.isValid = true;
        return true;
    }

    /**
     * This method creates the XML path location for the object in a document
     * based on if the user specified a location, if the path ends with a '/',
     * and if the object name should be added on.
     *

     * @param componentPath The current xml path location
     */
    protected String createXmlPath(String componentPath) {
        if (!(componentPath.endsWith("/"))) {
            StringBuilder addlPath = new StringBuilder(componentPath);
            addlPath.append('/');
            addlPath.append(ecsgObjectName);
            addlPath.append('/');
            componentPath = addlPath.toString();
        }
        return componentPath;
    }

    /**
     *
     * This method takes a hastable of registered attributes and returns a array
     * of the attribute names and an array of their values. Those arrays are
     * stored in in a vector and returned. This method is specifically designed
     * to facilitate the copying of business objects. Associations, oids, etc.
     * are not processed.
     *
     * @return java.util.Vector
     */
    public Vector getAttributeNameValues() {
        //get an Enumeration of the keys
        Enumeration attributeList = htRegisteredAttributes.elements();
        Vector nameVector = new Vector();
        Vector valVector = new Vector();

    //Cycle through the key Enumeration storing the key and element in
        //the proper arrays.
        while (attributeList.hasMoreElements()) {
            //get the next attribute
            AmsEntityWebBeanInfo info = (AmsEntityWebBeanInfo) attributeList.nextElement();

            //Only process data attributes
            if (!info.attributeType.equals("ObjectIDAttribute")
                    && !info.attributeType.equals("ParentIDAttribute")
                    && !info.attributeType.equals("LocalAttribute")
                    && !info.attributeType.equals("ComputedAttribute")) {
        //we use vectors here because the size is dynamic and at
                //this point we don't know how many DATA attributes there are.
                nameVector.addElement(info.logicalName);
                valVector.addElement(info.value);
            }
        }

        String[] name = new String[nameVector.size()];
        String[] value = new String[valVector.size()];

        nameVector.copyInto(name);
        valVector.copyInto(value);

        //Store the arrays in a vector to be returned.
        Vector returnVector = new Vector();
        returnVector.addElement(name);
        returnVector.addElement(value);

        return returnVector;
    }

    public List<AmsEntityWebBean> getChildren(String componentIdentifier, String idAttribute, String parentIDAttribute) throws AmsException {
        Class c;
		try {
			c = Class.forName("com.ams.tradeportal.busobj.webbean." + componentIdentifier + "WebBean");
		} catch (ClassNotFoundException e) {
			throw new AmsException("Invalid componentIdentifier, " + componentIdentifier + " not found", e);
		}
		AmsEntityWebBean tempAmsBean = (AmsEntityWebBean) beanMgr.createBean(c, componentIdentifier);

        List<AmsEntityWebBean> result = new ArrayList<>();

        String objectOid = this.getOid();
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(idAttribute);
        sb.append(" OID FROM ");
        sb.append(tempAmsBean.getDataTableName());
        sb.append(" WHERE ");
        sb.append(parentIDAttribute);
        sb.append(" = ? ");

        sb.append(" order by 1");
        DocumentHandler childrenDoc = DatabaseQueryBean.getXmlResultSet(sb.toString(), true, new Object[]{objectOid});
        if (childrenDoc != null) {
            List<DocumentHandler> chidrenList = childrenDoc.getFragmentsList("/ResultSetRecord");
            for (DocumentHandler childDoc : chidrenList) {
                String childOid = childDoc.getAttribute("/OID");
                AmsEntityWebBean amsBean = (AmsEntityWebBean) beanMgr.createBean(c, componentIdentifier);
                amsBean.setAttribute(idAttribute, childOid);
                amsBean.getDataFromAppServer();
                result.add(amsBean);
            }
        }
        return result;
    }
}

class AmsEntityWebBeanInfo implements java.io.Serializable {

    public String logicalName;
    public String physicalName;
    public String attributeType;
    public String value;
}
