package com.amsinc.ecsg.web;

/*
 * @(#)UserLoginManager.java
 *
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 *
 */

/**
 * This class contains user login information, specifically a user name (ID) and
 * password. It is currently used to enforce EJB level security. If the
 * application requires EJB level security then before these EJBs are accessed
 * a call should be made to the FormManager class's updateUserLoginInfo
 * method to set up the user login information for the current HTTP session.
 */
public class UserLoginManager implements java.io.Serializable
{
   /** The login name of the user */
   private String _loginName;
   /** The login password of the user */
   private String _loginPassword;

   /**
    * Creates a new class instance populating it with the information passed
    * in.
    *

    * @param name The user's login name.
    * @param password The user's login password.
    */
   public UserLoginManager (String name, String password)
   {
      _loginName = name;
      _loginPassword = password;
   }

   /**
    * Sets the user's login name to the specified value.
    *

    * @param name Value to set the user's login name to.
    */
   public void setLoginName (String name)
   {
      _loginName = name;
   }

   /**
    * Retrieves the login name of the user.
    *

    * @return The user's login name.
    */
   public String getLoginName ()
   {
      return _loginName;
   }

   /**
    * Sets the user's login password to the specified value.
    *

    * @param password Value to set the user's password name to.
    */
   public void setLoginPassword (String password)
   {
      _loginPassword = password;
   }

   /**
    * Retrieves the login password of the user.
    *

    * @return The user's password name.
    */
   public String getLoginPassword ()
   {
      return _loginPassword;
   }
}
