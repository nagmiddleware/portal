package com.amsinc.ecsg.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * @(#)AmsWebBean
 *
 *     Copyright  � 2001                         
 *     American Management Systems, Incorporated 
 *     All rights reserved
 */
import com.amsinc.ecsg.frame.*;
import com.amsinc.ecsg.util.*;

/**
 * AmsWebBean is the base class for all Java Beans components on the web server
 * in the eCSG architecture.  It stores some basic information common to all
 * web beans such as the EJB server location
 */
public class AmsWebBean implements java.io.Serializable {
	private static final Logger LOG = LoggerFactory.getLogger(AmsWebBean.class);
    protected ClientServerDataBridge csdb;
    /**
     * The Enterprise Java Bean server location as a string (IP:port).
     * This location is used for all interaction with EJB components.
     */
    private String serverLocation;
    /**
     * The eCSG object name is used as the tag name for the object in an XML document,
     * which should be the same as the corresponding EJB business object name (if there is one).
     * For view objects, the name can be any appropriate name.  Note that this is always
     * the name of the individual object, not the name of the list in the case of a list web bean.
     */
    protected String ecsgObjectName;

    /**
     * The name of the list component this list corresponds to.  This is the logical name
     * that the list was registered under.  This will be applicable only to list web beans.
     */
    private String ecsgListName;
    
    protected BeanManager beanMgr;
    
    public AmsWebBean(){
        ecsgObjectName = "";
        beanMgr = null;
        try {
            setServerLocation(JPylonProperties.getInstance().getString("serverLocation"));
        } catch (Exception e){
            LOG.error("Exception getting serverLocation",e);
        }
    }

    /**
     * This method initializes the web bean.
     *
     * @param   Bean Manager The BeanManager object for this session     

     * @return  void
     */
    public void init(BeanManager beanMgr) {
        this.beanMgr = beanMgr;
    }

    /**
     * This method returns a handle to the ClientServerDataBridge.
     *

     * @return  ClientServerDataBridge
     */
    public ClientServerDataBridge getClientServerDataBridge() {
        return csdb;
    }

    /**
     * This method sets the ClientServerDataBridge.
     *

     * @param ClientServerDataBridge
     */
    public void setClientServerDataBridge(ClientServerDataBridge csdb)
     {
        this.csdb = new ClientServerDataBridge(csdb);
    }

    /**
     * This method returns a handle to the BeanManager object for this session.
     *

     * @return  BeanManager
     */
    public BeanManager getBeanManager() {
        return beanMgr;
    }

    /**
     * This method returns the name of this list component.  This name should be the
     * same as the component list object of the corresponding business object, if there is one.
     *

     * @return  String
     */
    public String getEcsgListName() {
      return ecsgListName;
    }

    /**
     * This method sets the location of the object in the output XML document.  This will
     * only be set if the location will be different than the ecsgObjectName.  This will also be
     * used for list web beans if the items in this list correspond to a list component of
     * a parent business object.  This name should be the same as that parent business object.
     * This name is used to look for items in an XML document if it has a value.
     *
     * @param String the location of the object in the output XML document

     * @return  void
     */
    public void setEcsgListName(String ecsgListName) {
      this.ecsgListName = ecsgListName;
    }

    /**
     * This method returns the Enterprise Java Bean server location as a string.
     * This location is used for all interaction with EJB components.
     *

     * @return  String EJB server location
     */
    public String getServerLocation() {
        return serverLocation;
    }

    /**
     * This method sets the Enterprise Java Bean server location as a string.
     * This location is used for all interaction with EJB components.
     *
     * @param   String The EJB server location (IP:port)     

     * @return  void
     */
    public void setServerLocation(String serverLocation) {
        this.serverLocation = serverLocation;
    }

    /**
     * This method gets the eCSG object name.  This name is used as the
     * tag name for the object in an XML document.  It should be the same
     * as the corresponding EJB business object (if there is one).
     * For view objects, the name can be any appropriate name.
     *
     * @param   String the eCSG object name  

     * @return  void
     */
    public String getEcsgObjectName() {
        return ecsgObjectName;
    }

    /**
     * This method sets the eCSG object name.  This name is used as the
     * tag name for the object in an XML document.  It should be the same
     * as the corresponding EJB business object (if there is one).
     * For view objects, the name can be any appropriate name.
     *
     * @param   String the eCSG object name  

     * @return  void
     */
    public void setEcsgObjectName(String ecsgObjectName) {
        this.ecsgObjectName = ecsgObjectName;
    }

    /**
     * This method takes an XML document and returns a vector of the web beans
     * from the values in the document starting at the root level of the document.
     *

     * @return  boolean true if object was found in document, otherwise false
     */
    public boolean populateFromXmlDoc(DocumentHandler doc) {
        return(populateFromXmlDoc(doc, ""));
    }

    /**
     * This method takes an XML document and returns a vector of the web beans
     * from the values in the document starting at the specified component path.
     * If a '/' character terminates the component path, that will
     * be the absolute path name used, otherwise, the eCSG object
     * name will be appended.
     *

     * @return  boolean true if object was found in document, otherwise false
     */
    public boolean populateFromXmlDoc(DocumentHandler doc, String componentPath) {
        return false;
    }

    /**
     * This method takes an XML document and puts this bean's attributes
     * into the document.
     *
     * @param   DocumentHandler The XML document object

     * @return  void
     */
    public void populateXmlDoc(DocumentHandler doc) {
        populateXmlDoc(doc, "");
    }

    /**
     * This method takes a document and puts this bean's attributes
     * into the document starting at the specified component path.
     * If a '/' character terminates the component path, that will
     * be the absolute path name used, otherwise, the eCSG object
     * name will be appended.
     *
     * @param   DocumentHandler The XML document object
     * @param   String The path in the XML document to put the data

     * @return  void
     */
    public void populateXmlDoc(DocumentHandler doc, String componentPath) {
    }
}
